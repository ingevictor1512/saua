/*
 Navicat Premium Data Transfer

 Source Server         : Local
 Source Server Type    : MySQL
 Source Server Version : 50728
 Source Host           : localhost:3306
 Source Schema         : saua_db

 Target Server Type    : MySQL
 Target Server Version : 50728
 File Encoding         : 65001

 Date: 03/02/2021 17:23:26
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for respuestas
-- ----------------------------
DROP TABLE IF EXISTS `respuestas`;
CREATE TABLE `respuestas` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_pregunta` bigint(255) NOT NULL,
  `texto` varchar(150) DEFAULT NULL,
  `checked` tinyint(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_pregunta` (`id_pregunta`),
  CONSTRAINT `fk_pregunta` FOREIGN KEY (`id_pregunta`) REFERENCES `preguntas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of respuestas
-- ----------------------------
BEGIN;
INSERT INTO `respuestas` VALUES (1, 3, 'En el mismo lugar de origen', 1);
INSERT INTO `respuestas` VALUES (2, 3, 'En otro lugar(indique)', 0);
INSERT INTO `respuestas` VALUES (3, 4, 'Computadora', 1);
INSERT INTO `respuestas` VALUES (4, 4, 'Tablet', 0);
INSERT INTO `respuestas` VALUES (5, 4, 'Celular', 0);
INSERT INTO `respuestas` VALUES (6, 4, 'Ninguno de los anteriores', 0);
INSERT INTO `respuestas` VALUES (7, 5, 'Excelente', 0);
INSERT INTO `respuestas` VALUES (8, 5, 'Buena', 1);
INSERT INTO `respuestas` VALUES (9, 5, 'Intermitente', 0);
INSERT INTO `respuestas` VALUES (10, 5, 'Sin conexión', 0);
INSERT INTO `respuestas` VALUES (11, 8, 'Si(especifique)', 0);
INSERT INTO `respuestas` VALUES (12, 8, 'No', 1);
INSERT INTO `respuestas` VALUES (13, 9, 'Si(especifique)', 0);
INSERT INTO `respuestas` VALUES (14, 9, 'No', 1);
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
