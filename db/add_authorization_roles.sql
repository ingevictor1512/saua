/*Se actualiza tabla documentos: se agrega roles autorizados.*/
DROP TABLE IF EXISTS `documentos`;
CREATE TABLE `documentos` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `nombre_real` varchar(80) NOT NULL,
  `nombre_mostrar` varchar(80) NOT NULL,
  `descripcion` varchar(100) NOT NULL,
  `roles_aut` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;