/*
 Navicat Premium Data Transfer

 Source Server         : Local
 Source Server Type    : MySQL
 Source Server Version : 50728
 Source Host           : localhost:3306
 Source Schema         : saua_db

 Target Server Type    : MySQL
 Target Server Version : 50728
 File Encoding         : 65001

 Date: 03/02/2021 17:23:42
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for usuario_respuestas
-- ----------------------------
DROP TABLE IF EXISTS `usuario_respuestas`;
CREATE TABLE `usuario_respuestas` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_respuesta` bigint(255) DEFAULT NULL,
  `id_usuario` int(255) NOT NULL,
  `resp_personalizada` varchar(150) DEFAULT NULL,
  `id_pregunta` bigint(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_pregunta_respuesta` (`id_respuesta`),
  KEY `fk_usuario` (`id_usuario`),
  CONSTRAINT `fk_pregunta_respuesta` FOREIGN KEY (`id_respuesta`) REFERENCES `respuestas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_usuario` FOREIGN KEY (`id_usuario`) REFERENCES `usuario` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=60 DEFAULT CHARSET=latin1;

SET FOREIGN_KEY_CHECKS = 1;
