CREATE DATABASE  IF NOT EXISTS `saua_db` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `saua_db`;
-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: saua_db
-- ------------------------------------------------------
-- Server version	5.7.20

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `alumno`
--

DROP TABLE IF EXISTS `alumno`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alumno` (
  `matricula` varchar(13) NOT NULL,
  `paterno` varchar(40) NOT NULL,
  `materno` varchar(40) NOT NULL,
  `nombre` varchar(40) NOT NULL,
  `curp` varchar(18) DEFAULT NULL,
  `semestre` int(11) NOT NULL,
  `generacion` varchar(40) NOT NULL,
  `genero` tinyint(4) NOT NULL,
  `grupo_id` int(11) DEFAULT NULL,
  `f_registro` datetime NOT NULL,
  `f_modif` datetime NOT NULL,
  `pe_CVE_PLAN_ALUMNO` varchar(10) DEFAULT NULL,
  `tipo_ingreso_id` int(11) DEFAULT NULL,
  `status_alumno_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`matricula`),
  KEY `fk_alumno_prog_educativo1_idx` (`pe_CVE_PLAN_ALUMNO`),
  KEY `fk_alumno_tipo_ingreso1_idx` (`tipo_ingreso_id`),
  KEY `fk_alumno_status_alumno1_idx` (`status_alumno_id`),
  CONSTRAINT `fk_alumno_prog_educativo1` FOREIGN KEY (`pe_CVE_PLAN_ALUMNO`) REFERENCES `prog_educativo` (`CVE_PLAN_ALUMNO`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_alumno_status_alumno1` FOREIGN KEY (`status_alumno_id`) REFERENCES `status_alumno` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_alumno_tipo_ingreso1` FOREIGN KEY (`tipo_ingreso_id`) REFERENCES `tipo_ingreso` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alumno`
--

LOCK TABLES `alumno` WRITE;
/*!40000 ALTER TABLE `alumno` DISABLE KEYS */;
INSERT INTO `alumno` VALUES ('07046511','Morales','Ramirez','Benito','MORB890213HGRRMN05',6,'2007 - 2013',1,NULL,'2020-12-27 20:21:46','2020-12-27 20:21:46',NULL,NULL,NULL);
/*!40000 ALTER TABLE `alumno` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `alumno_grupo`
--

DROP TABLE IF EXISTS `alumno_grupo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alumno_grupo` (
  `grupo_id` int(10) unsigned NOT NULL,
  `alumno_matricula` varchar(13) NOT NULL,
  `f_registro` varchar(45) DEFAULT NULL,
  `observaciones` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`grupo_id`,`alumno_matricula`),
  KEY `fk_alumno_grupo_grupo1_idx` (`grupo_id`),
  KEY `fk_alumno_grupo_alumno1_idx` (`alumno_matricula`),
  CONSTRAINT `fk_alumno_grupo_alumno1` FOREIGN KEY (`alumno_matricula`) REFERENCES `alumno` (`matricula`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_alumno_grupo_grupo1` FOREIGN KEY (`grupo_id`) REFERENCES `grupo` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alumno_grupo`
--

LOCK TABLES `alumno_grupo` WRITE;
/*!40000 ALTER TABLE `alumno_grupo` DISABLE KEYS */;
/*!40000 ALTER TABLE `alumno_grupo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `autorizacion_carga`
--

DROP TABLE IF EXISTS `autorizacion_carga`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `autorizacion_carga` (
  `alumno_matricula` varchar(13) NOT NULL,
  `periodo_id` int(10) unsigned NOT NULL,
  `clave_aut` varchar(100) NOT NULL,
  `autorizacion` tinyint(1) NOT NULL,
  `observaciones` varchar(100) DEFAULT NULL,
  `f_registro` datetime DEFAULT NULL,
  `f_modif` datetime DEFAULT NULL,
  KEY `fk_autorizacion_carga_alumno1_idx` (`alumno_matricula`),
  KEY `fk_autorizacion_carga_periodo1_idx` (`periodo_id`),
  CONSTRAINT `fk_autorizacion_carga_alumno1` FOREIGN KEY (`alumno_matricula`) REFERENCES `alumno` (`matricula`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_autorizacion_carga_periodo1` FOREIGN KEY (`periodo_id`) REFERENCES `periodo` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `autorizacion_carga`
--

LOCK TABLES `autorizacion_carga` WRITE;
/*!40000 ALTER TABLE `autorizacion_carga` DISABLE KEYS */;
/*!40000 ALTER TABLE `autorizacion_carga` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `autorizacion_notificaciones`
--

DROP TABLE IF EXISTS `autorizacion_notificaciones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `autorizacion_notificaciones` (
  `id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `autorizacion_notificaciones`
--

LOCK TABLES `autorizacion_notificaciones` WRITE;
/*!40000 ALTER TABLE `autorizacion_notificaciones` DISABLE KEYS */;
/*!40000 ALTER TABLE `autorizacion_notificaciones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `baja_tutoria`
--

DROP TABLE IF EXISTS `baja_tutoria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `baja_tutoria` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tutoria_id` bigint(11) unsigned NOT NULL,
  `observaciones` varchar(60) DEFAULT NULL,
  `f_registro` datetime DEFAULT NULL,
  `f_modif` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_baja_tutoria_tutoria1_idx` (`tutoria_id`),
  CONSTRAINT `fk_baja_tutoria_tutoria1` FOREIGN KEY (`tutoria_id`) REFERENCES `tutoria` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `baja_tutoria`
--

LOCK TABLES `baja_tutoria` WRITE;
/*!40000 ALTER TABLE `baja_tutoria` DISABLE KEYS */;
/*!40000 ALTER TABLE `baja_tutoria` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `carga_alumno`
--

DROP TABLE IF EXISTS `carga_alumno`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `carga_alumno` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `alumno_matricula` varchar(13) NOT NULL,
  `periodo_id` int(10) unsigned NOT NULL,
  `clase_ofertada_id` int(10) unsigned NOT NULL,
  `f_carga` datetime NOT NULL,
  `cve_autorizacion` varchar(10) DEFAULT NULL,
  `comun` int(11) DEFAULT NULL,
  `f_modif` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_carga_alumno_alumno1_idx` (`alumno_matricula`),
  KEY `fk_carga_alumno_periodo1_idx` (`periodo_id`),
  KEY `fk_carga_alumno_clase_ofertada1_idx` (`clase_ofertada_id`),
  CONSTRAINT `fk_carga_alumno_alumno1` FOREIGN KEY (`alumno_matricula`) REFERENCES `alumno` (`matricula`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_carga_alumno_clase_ofertada1` FOREIGN KEY (`clase_ofertada_id`) REFERENCES `clase_ofertada` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_carga_alumno_periodo1` FOREIGN KEY (`periodo_id`) REFERENCES `periodo` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `carga_alumno`
--

LOCK TABLES `carga_alumno` WRITE;
/*!40000 ALTER TABLE `carga_alumno` DISABLE KEYS */;
/*!40000 ALTER TABLE `carga_alumno` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categoria_profesor`
--

DROP TABLE IF EXISTS `categoria_profesor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categoria_profesor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(60) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categoria_profesor`
--

LOCK TABLES `categoria_profesor` WRITE;
/*!40000 ALTER TABLE `categoria_profesor` DISABLE KEYS */;
INSERT INTO `categoria_profesor` VALUES (1,'CONTRATO'),(2,'INVITADO'),(3,'INTERINO'),(4,'BASE');
/*!40000 ALTER TABLE `categoria_profesor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clase_ofertada`
--

DROP TABLE IF EXISTS `clase_ofertada`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clase_ofertada` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `periodo_id` int(10) unsigned NOT NULL,
  `profesor_id` bigint(11) NOT NULL,
  `carga_uap` bigint(11) DEFAULT NULL,
  `semestre` int(11) NOT NULL,
  `carga_maxima` int(11) DEFAULT NULL,
  `carga_minima` int(11) DEFAULT NULL,
  `uap_clave` varchar(10) NOT NULL,
  `grupo_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_clase_ofertada_periodo1_idx` (`periodo_id`),
  KEY `fk_clase_ofertada_profesor1_idx` (`profesor_id`),
  KEY `fk_clase_ofertada_uap1_idx` (`uap_clave`),
  KEY `fk_clase_ofertada_grupo1_idx` (`grupo_id`),
  CONSTRAINT `fk_clase_ofertada_grupo1` FOREIGN KEY (`grupo_id`) REFERENCES `grupo` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_clase_ofertada_periodo1` FOREIGN KEY (`periodo_id`) REFERENCES `periodo` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_clase_ofertada_profesor1` FOREIGN KEY (`profesor_id`) REFERENCES `profesor` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_clase_ofertada_uap1` FOREIGN KEY (`uap_clave`) REFERENCES `uap` (`clave`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clase_ofertada`
--

LOCK TABLES `clase_ofertada` WRITE;
/*!40000 ALTER TABLE `clase_ofertada` DISABLE KEYS */;
/*!40000 ALTER TABLE `clase_ofertada` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `config`
--

DROP TABLE IF EXISTS `config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(45) NOT NULL,
  `value` varchar(100) NOT NULL,
  `desc` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `config`
--

LOCK TABLES `config` WRITE;
/*!40000 ALTER TABLE `config` DISABLE KEYS */;
INSERT INTO `config` VALUES (1,'URL_SITE','http://localhost:8080/saua',NULL);
/*!40000 ALTER TABLE `config` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contacto`
--

DROP TABLE IF EXISTS `contacto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contacto` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tipo` enum('EMAIL','FACEBOOK','CELULAR') NOT NULL,
  `valor` varchar(100) NOT NULL,
  `alumno_matricula` varchar(13) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_contacto_alumno_idx` (`alumno_matricula`),
  CONSTRAINT `fk_contacto_alumno` FOREIGN KEY (`alumno_matricula`) REFERENCES `alumno` (`matricula`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contacto`
--

LOCK TABLES `contacto` WRITE;
/*!40000 ALTER TABLE `contacto` DISABLE KEYS */;
/*!40000 ALTER TABLE `contacto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `continuidad`
--

DROP TABLE IF EXISTS `continuidad`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `continuidad` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uap_clave` varchar(10) NOT NULL,
  `uap` varchar(10) NOT NULL,
  `tipo` enum('ANTECEDENTE','CONSECUENTE') NOT NULL,
  `observaciones` varchar(100) DEFAULT NULL,
  `f_registro` datetime DEFAULT NULL,
  `f_modif` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_continuidad_uap3_idx` (`uap_clave`),
  CONSTRAINT `fk_continuidad_uap3` FOREIGN KEY (`uap_clave`) REFERENCES `uap` (`clave`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `continuidad`
--

LOCK TABLES `continuidad` WRITE;
/*!40000 ALTER TABLE `continuidad` DISABLE KEYS */;
/*!40000 ALTER TABLE `continuidad` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `escuela_proc`
--

DROP TABLE IF EXISTS `escuela_proc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `escuela_proc` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(60) NOT NULL COMMENT '			',
  `direccion` varchar(100) DEFAULT NULL,
  `telefono` varchar(10) DEFAULT NULL,
  `mail` varchar(45) DEFAULT NULL,
  `f_registro` datetime DEFAULT NULL,
  `f_ult_modif` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `escuela_proc`
--

LOCK TABLES `escuela_proc` WRITE;
/*!40000 ALTER TABLE `escuela_proc` DISABLE KEYS */;
INSERT INTO `escuela_proc` VALUES (1,'FACULTAD DE INGENIERÍA','AV LAZARO CARDENAS S/N','7474730487','ingenieria@uagro.mx','2020-12-30 00:08:58','2020-12-30 00:08:58'),(2,'FACULTAD DE MATEMATICAS','AV LAZARO CARDENAS','7561300688','matematicas@uagro.mx','2020-12-30 00:09:11','2020-12-30 00:09:11');
/*!40000 ALTER TABLE `escuela_proc` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `etapas_uap`
--

DROP TABLE IF EXISTS `etapas_uap`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `etapas_uap` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(60) NOT NULL,
  `abr` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `etapas_uap`
--

LOCK TABLES `etapas_uap` WRITE;
/*!40000 ALTER TABLE `etapas_uap` DISABLE KEYS */;
INSERT INTO `etapas_uap` VALUES (1,'ETAPA DE FORMACION INSTITUCIONAL','EFI'),(2,'NUCLEO FORMACIÓN BASICO','NFBAD'),(3,'NIVEL FORMACIÓN PROFESIONAL','NFPE'),(4,'ESPECIALIZACIÓN','EIV');
/*!40000 ALTER TABLE `etapas_uap` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `grupo`
--

DROP TABLE IF EXISTS `grupo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `grupo` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) DEFAULT NULL,
  `jefe_grupo` varchar(13) NOT NULL,
  `ubicacion` varchar(45) DEFAULT NULL,
  `turno` enum('MATUTINO','VESPERTINO') NOT NULL,
  `tipo_grupo` enum('NORMAL','REPITE','MIXTO') NOT NULL,
  `abr` varchar(10) NOT NULL,
  `prog_educativo_CVE_PLAN_ALUMNO` varchar(10) NOT NULL,
  `idPeriodo` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_grupo_alumno1_idx` (`jefe_grupo`),
  KEY `fk_grupo_prog_educativo1_idx` (`prog_educativo_CVE_PLAN_ALUMNO`),
  KEY `´fk_id_periodo_grupo´` (`idPeriodo`),
  CONSTRAINT `fk_grupo_alumno1` FOREIGN KEY (`jefe_grupo`) REFERENCES `alumno` (`matricula`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_grupo_prog_educativo1` FOREIGN KEY (`prog_educativo_CVE_PLAN_ALUMNO`) REFERENCES `prog_educativo` (`CVE_PLAN_ALUMNO`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `´fk_id_periodo_grupo´` FOREIGN KEY (`idPeriodo`) REFERENCES `periodo` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `grupo`
--

LOCK TABLES `grupo` WRITE;
/*!40000 ALTER TABLE `grupo` DISABLE KEYS */;
/*!40000 ALTER TABLE `grupo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `periodo`
--

DROP TABLE IF EXISTS `periodo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `periodo` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `f_inicial` date NOT NULL,
  `f_final` date NOT NULL,
  `periodo_lectivo` varchar(45) NOT NULL,
  `periodo_activo` int(11) DEFAULT NULL,
  `periodo_activo_admin` int(11) DEFAULT NULL,
  `ciclo_escolar` varchar(45) NOT NULL,
  `f_registro` datetime DEFAULT NULL,
  `f_modif` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `periodo`
--

LOCK TABLES `periodo` WRITE;
/*!40000 ALTER TABLE `periodo` DISABLE KEYS */;
/*!40000 ALTER TABLE `periodo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `periodo_registro`
--

DROP TABLE IF EXISTS `periodo_registro`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `periodo_registro` (
  `int` int(11) NOT NULL AUTO_INCREMENT,
  `f_inicio` date NOT NULL,
  `f_final` date NOT NULL,
  `idPeriodo` int(10) unsigned NOT NULL,
  PRIMARY KEY (`int`),
  KEY `´fk_id_periodo´` (`idPeriodo`),
  CONSTRAINT `´fk_id_periodo´` FOREIGN KEY (`idPeriodo`) REFERENCES `periodo` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `periodo_registro`
--

LOCK TABLES `periodo_registro` WRITE;
/*!40000 ALTER TABLE `periodo_registro` DISABLE KEYS */;
/*!40000 ALTER TABLE `periodo_registro` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `profesor`
--

DROP TABLE IF EXISTS `profesor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `profesor` (
  `id` bigint(11) NOT NULL,
  `paterno` varchar(45) NOT NULL,
  `materno` varchar(45) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `cubiculo` varchar(45) DEFAULT NULL,
  `categoria_profesor_id` int(11) NOT NULL,
  `f_registro` datetime DEFAULT NULL,
  `f_modif` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_profesor_categoria_profesor1_idx` (`categoria_profesor_id`),
  CONSTRAINT `fk_profesor_categoria_profesor1` FOREIGN KEY (`categoria_profesor_id`) REFERENCES `categoria_profesor` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `profesor`
--

LOCK TABLES `profesor` WRITE;
/*!40000 ALTER TABLE `profesor` DISABLE KEYS */;
/*!40000 ALTER TABLE `profesor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prog_educativo`
--

DROP TABLE IF EXISTS `prog_educativo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `prog_educativo` (
  `CVE_PLAN_ALUMNO` varchar(10) NOT NULL,
  `NOMBRE_PLAN_ALUMNO` varchar(45) NOT NULL,
  `VRS_PLAN_ALUMNO` varchar(8) DEFAULT NULL,
  `abr` varchar(13) NOT NULL,
  `observaciones` varchar(45) DEFAULT NULL,
  `escuela_proc_id` int(10) unsigned NOT NULL,
  `f_registro` datetime DEFAULT NULL,
  `f_modif` datetime DEFAULT NULL,
  PRIMARY KEY (`CVE_PLAN_ALUMNO`),
  KEY `fk_prog_educativo_escuela_proc1_idx` (`escuela_proc_id`),
  CONSTRAINT `fk_prog_educativo_escuela_proc1` FOREIGN KEY (`escuela_proc_id`) REFERENCES `escuela_proc` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prog_educativo`
--

LOCK TABLES `prog_educativo` WRITE;
/*!40000 ALTER TABLE `prog_educativo` DISABLE KEYS */;
/*!40000 ALTER TABLE `prog_educativo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reset_password`
--

DROP TABLE IF EXISTS `reset_password`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reset_password` (
  `id` bigint(11) unsigned NOT NULL AUTO_INCREMENT,
  `usuario_id` int(11) NOT NULL,
  `f_registro` datetime NOT NULL,
  `hash_value` varchar(100) NOT NULL,
  `uso` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_reset_password_usuario1_idx` (`usuario_id`),
  CONSTRAINT `fk_reset_password_usuario1` FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reset_password`
--

LOCK TABLES `reset_password` WRITE;
/*!40000 ALTER TABLE `reset_password` DISABLE KEYS */;
/*!40000 ALTER TABLE `reset_password` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rol`
--

DROP TABLE IF EXISTS `rol`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rol` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario_id` int(11) NOT NULL,
  `nombre` varchar(60) NOT NULL,
  `f_registro` datetime DEFAULT NULL,
  `f_modif` datetime DEFAULT NULL,
  PRIMARY KEY (`id`,`usuario_id`),
  KEY `fk_rol_usuario1_idx` (`usuario_id`),
  CONSTRAINT `fk_rol_usuario1` FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rol`
--

LOCK TABLES `rol` WRITE;
/*!40000 ALTER TABLE `rol` DISABLE KEYS */;
INSERT INTO `rol` VALUES (14,14,'STUDENT','2020-12-27 00:00:00','2020-12-27 00:00:00');
/*!40000 ALTER TABLE `rol` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `status_alumno`
--

DROP TABLE IF EXISTS `status_alumno`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `status_alumno` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(60) NOT NULL,
  `observaciones` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `status_alumno`
--

LOCK TABLES `status_alumno` WRITE;
/*!40000 ALTER TABLE `status_alumno` DISABLE KEYS */;
/*!40000 ALTER TABLE `status_alumno` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_ingreso`
--

DROP TABLE IF EXISTS `tipo_ingreso`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_ingreso` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_ingreso`
--

LOCK TABLES `tipo_ingreso` WRITE;
/*!40000 ALTER TABLE `tipo_ingreso` DISABLE KEYS */;
INSERT INTO `tipo_ingreso` VALUES (1,'NORMAL'),(2,'EQUIVALENCIA'),(3,'HOMOLOGACIÓN');
/*!40000 ALTER TABLE `tipo_ingreso` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_uap`
--

DROP TABLE IF EXISTS `tipo_uap`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_uap` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `categoria_optativa` varchar(62) NOT NULL,
  `semestre` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_uap`
--

LOCK TABLES `tipo_uap` WRITE;
/*!40000 ALTER TABLE `tipo_uap` DISABLE KEYS */;
INSERT INTO `tipo_uap` VALUES (1,'ORIENTADA A ESTRUCTURAS',5),(2,'ORIENTADA A GEOTECNICA',5),(3,'ORIENTADA A LA CONSTRUCCION',5),(4,'ORIENTADA A HIDRAULICA',5),(5,'CONSTRUCCION DE INFRAESTRUCTURA URBANA',5),(6,'EDIFICACION',5),(7,'ORIENTACION EN SISTEMAS DE INFORMACION GEOGRAFICA',5),(8,'ORIENTACIÓN EN GEORREFERENCIACION',5),(9,'ORIENTACION EN SISTEMAS E INFORMATICA EDUCATIVA',5),(10,'ORIENTACION EN TECNOLOGIAS DE LA INFORMACION Y COMUNICACIONES',5),(11,'REDES',5),(12,'ARQUITECTURA DE COMPUTADORAS',5),(13,'OBLIGATORIA',5),(14,'ELECTIVA',5);
/*!40000 ALTER TABLE `tipo_uap` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tutoria`
--

DROP TABLE IF EXISTS `tutoria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tutoria` (
  `id` bigint(11) unsigned NOT NULL AUTO_INCREMENT,
  `alumno_matricula` varchar(13) NOT NULL,
  `profesor_id` bigint(11) NOT NULL,
  `observaciones` varchar(45) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `f_registro` datetime DEFAULT NULL,
  `f_modif` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tutoria_alumno1_idx` (`alumno_matricula`),
  KEY `fk_tutoria_profesor1_idx` (`profesor_id`),
  CONSTRAINT `fk_tutoria_alumno1` FOREIGN KEY (`alumno_matricula`) REFERENCES `alumno` (`matricula`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tutoria_profesor1` FOREIGN KEY (`profesor_id`) REFERENCES `profesor` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tutoria`
--

LOCK TABLES `tutoria` WRITE;
/*!40000 ALTER TABLE `tutoria` DISABLE KEYS */;
/*!40000 ALTER TABLE `tutoria` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `uap`
--

DROP TABLE IF EXISTS `uap`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `uap` (
  `clave` varchar(10) NOT NULL,
  `nombre` varchar(60) NOT NULL,
  `creditos` int(11) DEFAULT NULL,
  `codigo_comun` varchar(45) DEFAULT NULL,
  `pe_CVE_PLAN_ALUMNO` varchar(10) NOT NULL,
  `tipo_uap_id` int(11) NOT NULL,
  `etapas_uap_id` int(11) NOT NULL,
  `f_registro` datetime DEFAULT NULL COMMENT '					',
  `f_modif` datetime DEFAULT NULL,
  PRIMARY KEY (`clave`),
  KEY `fk_UAP_prog_educativo1_idx` (`pe_CVE_PLAN_ALUMNO`),
  KEY `fk_UAP_tipo_uap1_idx` (`tipo_uap_id`),
  KEY `fk_UAP_etapas_uap1_idx` (`etapas_uap_id`),
  CONSTRAINT `fk_UAP_etapas_uap1` FOREIGN KEY (`etapas_uap_id`) REFERENCES `etapas_uap` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_UAP_prog_educativo1` FOREIGN KEY (`pe_CVE_PLAN_ALUMNO`) REFERENCES `prog_educativo` (`CVE_PLAN_ALUMNO`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_UAP_tipo_uap1` FOREIGN KEY (`tipo_uap_id`) REFERENCES `tipo_uap` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `uap`
--

LOCK TABLES `uap` WRITE;
/*!40000 ALTER TABLE `uap` DISABLE KEYS */;
/*!40000 ALTER TABLE `uap` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuario`
--

DROP TABLE IF EXISTS `usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) NOT NULL,
  `password` varchar(100) NOT NULL,
  `profesor_id` bigint(11) DEFAULT NULL,
  `alumno_matricula` varchar(13) DEFAULT NULL,
  `bloqueo` tinyint(255) NOT NULL,
  `activo` tinyint(255) NOT NULL,
  `primer_login` tinyint(255) NOT NULL,
  `f_expiracion` date DEFAULT NULL,
  `f_registro` datetime DEFAULT NULL,
  `f_modif` datetime DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `status` tinyint(255) DEFAULT '0',
  `ultimo_login` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_usuario_profesor1_idx` (`profesor_id`),
  KEY `fk_usuario_alumno1_idx` (`alumno_matricula`),
  CONSTRAINT `fk_usuario_alumno1` FOREIGN KEY (`alumno_matricula`) REFERENCES `alumno` (`matricula`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_usuario_profesor1` FOREIGN KEY (`profesor_id`) REFERENCES `profesor` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuario`
--

LOCK TABLES `usuario` WRITE;
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
INSERT INTO `usuario` VALUES (14,'07046511','$2a$10$txR1oWx6vJ2rVWbDWInOBOG.SihtTU86jb4Lg1BMUs6u3LUAzMm4i',NULL,'07046511',0,1,0,NULL,'2020-12-27 20:21:46','2020-12-27 20:21:46','bmorales.dev@gmail.com',1,NULL);
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuario_notificaciones`
--

DROP TABLE IF EXISTS `usuario_notificaciones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuario_notificaciones` (
  `id` bigint(11) unsigned NOT NULL AUTO_INCREMENT,
  `nombre_destinatario` varchar(45) NOT NULL,
  `email_destinatario` varchar(60) NOT NULL,
  `plain_password` varchar(45) NOT NULL,
  `usuario_id` int(11) DEFAULT NULL,
  `status` enum('REGISTRADA','ENVIADA') NOT NULL,
  `f_expiracion` date DEFAULT NULL,
  `tipo_notificacion` varchar(50) NOT NULL,
  `visto` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_noficaciones_usuario1_idx` (`usuario_id`),
  CONSTRAINT `fk_noficaciones_usuario1` FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuario_notificaciones`
--

LOCK TABLES `usuario_notificaciones` WRITE;
/*!40000 ALTER TABLE `usuario_notificaciones` DISABLE KEYS */;
/*!40000 ALTER TABLE `usuario_notificaciones` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-12-30 10:52:59
