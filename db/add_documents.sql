/*Agregar path de donde se guardaran los documentos*/
INSERT INTO `saua_db`.`config`(`config_id`, `config_key`, `config_value`, `config_desc`, `config_type`) VALUES (10, 'DOCUMENT_PATH', '/users/bny/Documents/Proyectos/', 'Path donde se guardarán formatos/archivos', 1);

/*Agregar tabla documentos*/
DROP TABLE IF EXISTS `documentos`;
CREATE TABLE `documentos` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `nombre_real` varchar(80) NOT NULL,
  `nombre_mostrar` varchar(80) NOT NULL,
  `descripcion` varchar(100) NOT NULL,
  `status` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;