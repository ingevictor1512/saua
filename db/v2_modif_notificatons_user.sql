ALTER TABLE `saua_db`.`usuario_notificaciones` CHANGE COLUMN `nombre` `nombre_destinatario` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL AFTER `id`

ALTER TABLE `saua_db`.`usuario_notificaciones` CHANGE COLUMN `email` `email_destinatario` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL AFTER `nombre_destinatario`

ALTER TABLE `saua_db`.`reset_password` MODIFY COLUMN `hash_value` varchar(100) NOT NULL AFTER `f_registro`;

CREATE TABLE `saua_db`.`config` (`id` int(0) NOT NULL AUTO_INCREMENT, `key` varchar(45) NOT NULL,`value` varchar(100) NOT NULL,`desc` varchar(100) NULL,PRIMARY KEY (`id`));

INSERT INTO `config` VALUES (1, 'URL_SITE', 'http://localhost:8080/saua', NULL);

ALTER TABLE `saua_db`.`alumno` MODIFY COLUMN `grupo_id` int(11) NULL AFTER `genero`;

ALTER TABLE `saua_db`.`alumno` MODIFY COLUMN `pe_CVE_PLAN_ALUMNO` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL AFTER `f_modif`, MODIFY COLUMN `tipo_ingreso_id` int(11) NULL AFTER `pe_CVE_PLAN_ALUMNO`, MODIFY COLUMN `status_alumno_id` int(11) NULL AFTER `tipo_ingreso_id`;