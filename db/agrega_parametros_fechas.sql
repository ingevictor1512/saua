DELETE FROM config;
/*Se agrega tipo de dato, para saber que tipo de dato contendrá value*/
ALTER TABLE `saua_db`.`config` ADD COLUMN `config_type` int(255) NOT NULL AFTER `config_desc`;

BEGIN;
INSERT INTO `config` VALUES (1, 'URL_SITE', 'http://localhost:8080/saua', 'URL del portal', 1);
INSERT INTO `config` VALUES (2, 'EXPIRATION_DAY', '2', 'Tiempo de epiración para reseteo de password', 2);
INSERT INTO `config` VALUES (3, 'REGISTRY_DAY', '1', 'Numero de días para proceso de registro de UAP\'s', 2);
INSERT INTO `config` VALUES (4, 'INQUEST_INIT', '05/02/2021', 'Fecha de inicio de encuesta', 3);
INSERT INTO `config` VALUES (5, 'INQUEST_END', '07/02/2021', 'Fecha de término de encuesta', 3);
INSERT INTO `config` VALUES (6, 'REGISTRY_SUBJECTS_INIT', '08/02/2021', 'Fecha de inicio de alta de UAP\'s', 3);
INSERT INTO `config` VALUES (7, 'REGISTRY_SUBJECTS_END', '15/02/2021', 'Fecha de término de registro de UAP\'s', 3);
INSERT INTO `config` VALUES (8, 'AUTHORIZATION_SUBJECTS_INIT', '16/02/2021', 'Fecha de inicio de autorización de UAP\'s', 3);
INSERT INTO `config` VALUES (9, 'AUTHORIZATION_SUBJECTS_END', '20/02/2021', 'Fecha de término de autorización de UAP\'s', 3);
COMMIT;