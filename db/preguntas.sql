/*
 Navicat Premium Data Transfer

 Source Server         : Local
 Source Server Type    : MySQL
 Source Server Version : 50728
 Source Host           : localhost:3306
 Source Schema         : saua_db

 Target Server Type    : MySQL
 Target Server Version : 50728
 File Encoding         : 65001

 Date: 03/02/2021 17:22:52
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for preguntas
-- ----------------------------
DROP TABLE IF EXISTS `preguntas`;
CREATE TABLE `preguntas` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `texto` varchar(255) DEFAULT NULL,
  `cuerpo` text,
  `tipo` enum('ABIERTA','OPCIONES','FALSO_VERDADERO','OPCION') DEFAULT NULL,
  `place_holder` varchar(100) DEFAULT NULL,
  `status` tinyint(255) DEFAULT NULL,
  `f_creacion` date DEFAULT NULL,
  `f_modif` date DEFAULT NULL,
  `tipo_cuerpo` enum('TEXTO','IMAGEN','NINGUNO','CODIGO') DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of preguntas
-- ----------------------------
BEGIN;
INSERT INTO `preguntas` VALUES (1, 'Lugar de origen:', '', 'ABIERTA', 'Chilpancingo de los Bravo, Guerrero', 1, '2021-02-01', '2021-02-01', 'TEXTO');
INSERT INTO `preguntas` VALUES (2, 'Domicilio:', '', 'ABIERTA', 'Calle No. Colonia Localidad Municipio Región Estado País', 1, '2021-02-01', '2021-02-01', 'TEXTO');
INSERT INTO `preguntas` VALUES (3, 'Lugar donde tomará los cursos virtuales del próximo semestre?', NULL, 'OPCIONES', NULL, 1, '2021-02-01', '2021-02-01', 'TEXTO');
INSERT INTO `preguntas` VALUES (4, 'Para su conexión a sus cursos virtuales cuenta con?', NULL, 'OPCIONES', NULL, 1, '2021-02-01', '2021-02-01', 'TEXTO');
INSERT INTO `preguntas` VALUES (5, 'La conexión a internet para sus cursos virtuales es?', NULL, 'OPCIONES', NULL, 1, '2021-02-01', '2021-02-01', 'TEXTO');
INSERT INTO `preguntas` VALUES (6, 'Correo Institucional oficial ( matricula@uagro.mx).', '', 'ABIERTA', 'matricula@uagro.mx', 1, '2021-02-01', '2021-02-01', 'TEXTO');
INSERT INTO `preguntas` VALUES (7, 'Numero de celular actual (Nota: en caso de cambiar número de celular, notifique\n a la dirección).', '', 'ABIERTA', '7471010101', 1, '2021-02-01', '2021-02-01', 'TEXTO');
INSERT INTO `preguntas` VALUES (8, 'Tiene Alguna Discapacidad?', NULL, 'OPCIONES', NULL, 1, '2021-02-01', '2021-02-01', 'TEXTO');
INSERT INTO `preguntas` VALUES (9, 'Habla alguna lengua materna originaria?', NULL, 'OPCIONES', NULL, 1, '2021-02-01', '2021-02-01', 'TEXTO');
INSERT INTO `preguntas` VALUES (10, 'Eres afrodescendiente directo?', NULL, 'OPCION', NULL, 1, '2021-02-01', '2021-02-01', 'TEXTO');
INSERT INTO `preguntas` VALUES (11, 'Perteneces al grupo LGBT?', NULL, 'OPCION', NULL, 0, '2021-02-01', '2021-02-01', 'TEXTO');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
