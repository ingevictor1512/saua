CREATE TABLE `saua_db`.`categoria`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(60) NOT NULL,
  `descripcion` varchar(100) NULL,
  PRIMARY KEY (`id`)
);

INSERT INTO `saua_db`.`categoria`(`id`, `nombre`, `descripcion`) VALUES (1, 'DEFAULT', 'DEFAULT');

ALTER TABLE `saua_db`.`preguntas` 
ADD COLUMN `id_categoria` int(11) NULL AFTER `tipo_cuerpo`,
ADD CONSTRAINT `categoria_fk` FOREIGN KEY (`id_categoria`) REFERENCES `saua_db`.`categoria` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

update preguntas set id_categoria = 1;