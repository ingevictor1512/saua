-- CAMBIOS A REALIZAR PARA LAS ENCUESTAS POR PERIODO
    ALTER TABLE `saua_db`.`usuario_respuestas` ADD COLUMN `id_periodo` int(0) NULL AFTER `orden`;
    ALTER TABLE `saua_db`.`encuesta_usuario` ADD COLUMN `periodo_id` int(0) NULL AFTER `current_question_id`;
    update usuario_respuestas eu set eu.id_periodo = 2 where eu.id_periodo is null;
    update encuesta_usuario  set encuesta_usuario.periodo_id=2 whereencuesta_usuario.periodo_id is null;