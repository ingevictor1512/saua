/*
 Navicat Premium Data Transfer

 Source Server         : Local
 Source Server Type    : MySQL
 Source Server Version : 50728
 Source Host           : localhost:3306
 Source Schema         : saua_db

 Target Server Type    : MySQL
 Target Server Version : 50728
 File Encoding         : 65001

 Date: 03/01/2021 12:56:07
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for alumno
-- ----------------------------
DROP TABLE IF EXISTS `alumno`;
CREATE TABLE `alumno` (
  `matricula` varchar(13) NOT NULL,
  `paterno` varchar(40) NOT NULL,
  `materno` varchar(40) NOT NULL,
  `nombre` varchar(40) NOT NULL,
  `curp` varchar(18) DEFAULT NULL,
  `semestre` int(11) NOT NULL,
  `generacion` varchar(40) NOT NULL,
  `genero` tinyint(4) NOT NULL,
  `grupo_id` int(11) DEFAULT NULL,
  `f_registro` datetime NOT NULL,
  `f_modif` datetime NOT NULL,
  `pe_CVE_PLAN_ALUMNO` varchar(10) DEFAULT NULL,
  `tipo_ingreso_id` int(11) DEFAULT NULL,
  `status_alumno_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`matricula`),
  KEY `fk_alumno_prog_educativo1_idx` (`pe_CVE_PLAN_ALUMNO`),
  KEY `fk_alumno_tipo_ingreso1_idx` (`tipo_ingreso_id`),
  KEY `fk_alumno_status_alumno1_idx` (`status_alumno_id`),
  CONSTRAINT `fk_alumno_prog_educativo1` FOREIGN KEY (`pe_CVE_PLAN_ALUMNO`) REFERENCES `prog_educativo` (`CVE_PLAN_ALUMNO`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_alumno_status_alumno1` FOREIGN KEY (`status_alumno_id`) REFERENCES `status_alumno` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_alumno_tipo_ingreso1` FOREIGN KEY (`tipo_ingreso_id`) REFERENCES `tipo_ingreso` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of alumno
-- ----------------------------
BEGIN;
INSERT INTO `alumno` VALUES ('07046511', 'Morales', 'Ramirez', 'Benito', 'MORB890213HGRRMN05', 6, '2007 - 2013', 1, NULL, '2020-12-27 20:21:46', '2021-01-02 12:15:31', NULL, 3, NULL);
COMMIT;

-- ----------------------------
-- Table structure for alumno_grupo
-- ----------------------------
DROP TABLE IF EXISTS `alumno_grupo`;
CREATE TABLE `alumno_grupo` (
  `grupo_id` int(10) unsigned NOT NULL,
  `alumno_matricula` varchar(13) NOT NULL,
  `f_registro` varchar(45) DEFAULT NULL,
  `observaciones` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`grupo_id`,`alumno_matricula`),
  KEY `fk_alumno_grupo_grupo1_idx` (`grupo_id`),
  KEY `fk_alumno_grupo_alumno1_idx` (`alumno_matricula`),
  CONSTRAINT `fk_alumno_grupo_alumno1` FOREIGN KEY (`alumno_matricula`) REFERENCES `alumno` (`matricula`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_alumno_grupo_grupo1` FOREIGN KEY (`grupo_id`) REFERENCES `grupo` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for autorizacion_carga
-- ----------------------------
DROP TABLE IF EXISTS `autorizacion_carga`;
CREATE TABLE `autorizacion_carga` (
  `alumno_matricula` varchar(13) NOT NULL,
  `periodo_id` int(10) unsigned NOT NULL,
  `clave_aut` varchar(100) NOT NULL,
  `autorizacion` tinyint(1) NOT NULL,
  `observaciones` varchar(100) DEFAULT NULL,
  `f_registro` datetime DEFAULT NULL,
  `f_modif` datetime DEFAULT NULL,
  KEY `fk_autorizacion_carga_alumno1_idx` (`alumno_matricula`),
  KEY `fk_autorizacion_carga_periodo1_idx` (`periodo_id`),
  CONSTRAINT `fk_autorizacion_carga_alumno1` FOREIGN KEY (`alumno_matricula`) REFERENCES `alumno` (`matricula`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_autorizacion_carga_periodo1` FOREIGN KEY (`periodo_id`) REFERENCES `periodo` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for autorizacion_notificaciones
-- ----------------------------
DROP TABLE IF EXISTS `autorizacion_notificaciones`;
CREATE TABLE `autorizacion_notificaciones` (
  `id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for baja_tutoria
-- ----------------------------
DROP TABLE IF EXISTS `baja_tutoria`;
CREATE TABLE `baja_tutoria` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tutoria_id` bigint(11) unsigned NOT NULL,
  `observaciones` varchar(60) DEFAULT NULL,
  `f_registro` datetime DEFAULT NULL,
  `f_modif` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_baja_tutoria_tutoria1_idx` (`tutoria_id`),
  CONSTRAINT `fk_baja_tutoria_tutoria1` FOREIGN KEY (`tutoria_id`) REFERENCES `tutoria` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for carga_alumno
-- ----------------------------
DROP TABLE IF EXISTS `carga_alumno`;
CREATE TABLE `carga_alumno` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `alumno_matricula` varchar(13) NOT NULL,
  `periodo_id` int(10) unsigned NOT NULL,
  `clase_ofertada_id` int(10) unsigned NOT NULL,
  `f_carga` datetime NOT NULL,
  `cve_autorizacion` varchar(10) DEFAULT NULL,
  `comun` int(11) DEFAULT NULL,
  `f_modif` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_carga_alumno_alumno1_idx` (`alumno_matricula`),
  KEY `fk_carga_alumno_periodo1_idx` (`periodo_id`),
  KEY `fk_carga_alumno_clase_ofertada1_idx` (`clase_ofertada_id`),
  CONSTRAINT `fk_carga_alumno_alumno1` FOREIGN KEY (`alumno_matricula`) REFERENCES `alumno` (`matricula`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_carga_alumno_clase_ofertada1` FOREIGN KEY (`clase_ofertada_id`) REFERENCES `clase_ofertada` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_carga_alumno_periodo1` FOREIGN KEY (`periodo_id`) REFERENCES `periodo` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for categoria_profesor
-- ----------------------------
DROP TABLE IF EXISTS `categoria_profesor`;
CREATE TABLE `categoria_profesor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(60) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of categoria_profesor
-- ----------------------------
BEGIN;
INSERT INTO `categoria_profesor` VALUES (1, 'CONTRATO');
INSERT INTO `categoria_profesor` VALUES (2, 'INVITADO');
INSERT INTO `categoria_profesor` VALUES (3, 'INTERINO');
INSERT INTO `categoria_profesor` VALUES (4, 'BASE');
COMMIT;

-- ----------------------------
-- Table structure for clase_ofertada
-- ----------------------------
DROP TABLE IF EXISTS `clase_ofertada`;
CREATE TABLE `clase_ofertada` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `periodo_id` int(10) unsigned NOT NULL,
  `profesor_id` bigint(11) NOT NULL,
  `carga_uap` bigint(11) DEFAULT NULL,
  `semestre` int(11) NOT NULL,
  `carga_maxima` int(11) DEFAULT NULL,
  `carga_minima` int(11) DEFAULT NULL,
  `uap_clave` varchar(10) NOT NULL,
  `grupo_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_clase_ofertada_periodo1_idx` (`periodo_id`),
  KEY `fk_clase_ofertada_profesor1_idx` (`profesor_id`),
  KEY `fk_clase_ofertada_uap1_idx` (`uap_clave`),
  KEY `fk_clase_ofertada_grupo1_idx` (`grupo_id`),
  CONSTRAINT `fk_clase_ofertada_grupo1` FOREIGN KEY (`grupo_id`) REFERENCES `grupo` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_clase_ofertada_periodo1` FOREIGN KEY (`periodo_id`) REFERENCES `periodo` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_clase_ofertada_profesor1` FOREIGN KEY (`profesor_id`) REFERENCES `profesor` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_clase_ofertada_uap1` FOREIGN KEY (`uap_clave`) REFERENCES `uap` (`clave`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for config
-- ----------------------------
DROP TABLE IF EXISTS `config`;
CREATE TABLE `config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(45) NOT NULL,
  `value` varchar(100) NOT NULL,
  `desc` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of config
-- ----------------------------
BEGIN;
INSERT INTO `config` VALUES (1, 'URL_SITE', 'http://localhost:8080/saua', NULL);
COMMIT;

-- ----------------------------
-- Table structure for contacto
-- ----------------------------
DROP TABLE IF EXISTS `contacto`;
CREATE TABLE `contacto` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tipo` enum('EMAIL','FACEBOOK','CELULAR') NOT NULL,
  `valor` varchar(100) NOT NULL,
  `alumno_matricula` varchar(13) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_contacto_alumno_idx` (`alumno_matricula`),
  CONSTRAINT `fk_contacto_alumno` FOREIGN KEY (`alumno_matricula`) REFERENCES `alumno` (`matricula`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for continuidad
-- ----------------------------
DROP TABLE IF EXISTS `continuidad`;
CREATE TABLE `continuidad` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uap_clave` varchar(10) NOT NULL,
  `uap` varchar(10) NOT NULL,
  `tipo` enum('ANTECEDENTE','CONSECUENTE') NOT NULL,
  `observaciones` varchar(100) DEFAULT NULL,
  `f_registro` datetime DEFAULT NULL,
  `f_modif` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_continuidad_uap3_idx` (`uap_clave`),
  CONSTRAINT `fk_continuidad_uap3` FOREIGN KEY (`uap_clave`) REFERENCES `uap` (`clave`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for escuela_proc
-- ----------------------------
DROP TABLE IF EXISTS `escuela_proc`;
CREATE TABLE `escuela_proc` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(60) NOT NULL COMMENT '			',
  `direccion` varchar(100) DEFAULT NULL,
  `telefono` varchar(10) DEFAULT NULL,
  `mail` varchar(45) DEFAULT NULL,
  `f_registro` datetime DEFAULT NULL,
  `f_ult_modif` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of escuela_proc
-- ----------------------------
BEGIN;
INSERT INTO `escuela_proc` VALUES (1, 'FACULTAD DE INGENIERÍA', 'AV LAZARO CARDENAS S/N', '7474730487', 'ingenieria@uagro.mx', '2020-12-30 00:08:58', '2020-12-30 00:08:58');
INSERT INTO `escuela_proc` VALUES (2, 'FACULTAD DE MATEMATICAS', 'AV LAZARO CARDENAS', '7561300688', 'matematicas@uagro.mx', '2020-12-30 00:09:11', '2020-12-30 00:09:11');
COMMIT;

-- ----------------------------
-- Table structure for etapas_uap
-- ----------------------------
DROP TABLE IF EXISTS `etapas_uap`;
CREATE TABLE `etapas_uap` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(60) NOT NULL,
  `abr` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of etapas_uap
-- ----------------------------
BEGIN;
INSERT INTO `etapas_uap` VALUES (1, 'ETAPA DE FORMACION INSTITUCIONAL', 'EFI');
INSERT INTO `etapas_uap` VALUES (2, 'NUCLEO FORMACIÓN BASICO', 'NFBAD');
INSERT INTO `etapas_uap` VALUES (3, 'NIVEL FORMACIÓN PROFESIONAL', 'NFPE');
INSERT INTO `etapas_uap` VALUES (4, 'ESPECIALIZACIÓN', 'EIV');
COMMIT;

-- ----------------------------
-- Table structure for grupo
-- ----------------------------
DROP TABLE IF EXISTS `grupo`;
CREATE TABLE `grupo` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) DEFAULT NULL,
  `jefe_grupo` varchar(13) NOT NULL,
  `ubicacion` varchar(45) DEFAULT NULL,
  `turno` enum('MATUTINO','VESPERTINO') NOT NULL,
  `tipo_grupo` enum('NORMAL','REPITE','MIXTO') NOT NULL,
  `abr` varchar(10) NOT NULL,
  `prog_educativo_CVE_PLAN_ALUMNO` varchar(10) NOT NULL,
  `id_periodo` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_grupo_alumno1_idx` (`jefe_grupo`),
  KEY `fk_grupo_prog_educativo1_idx` (`prog_educativo_CVE_PLAN_ALUMNO`),
  KEY `´fk_id_periodo_grupo´` (`id_periodo`),
  CONSTRAINT `fk_grupo_alumno1` FOREIGN KEY (`jefe_grupo`) REFERENCES `alumno` (`matricula`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_grupo_periodo` FOREIGN KEY (`id_periodo`) REFERENCES `periodo` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_grupo_prog_educativo1` FOREIGN KEY (`prog_educativo_CVE_PLAN_ALUMNO`) REFERENCES `prog_educativo` (`CVE_PLAN_ALUMNO`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of grupo
-- ----------------------------
BEGIN;
INSERT INTO `grupo` VALUES (14, '101', '07046511', 'Edificio A', 'MATUTINO', 'NORMAL', '101', '000001', 1);
COMMIT;

-- ----------------------------
-- Table structure for periodo
-- ----------------------------
DROP TABLE IF EXISTS `periodo`;
CREATE TABLE `periodo` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `f_inicial` date NOT NULL,
  `f_final` date NOT NULL,
  `periodo_lectivo` varchar(45) NOT NULL,
  `periodo_activo` int(11) DEFAULT NULL,
  `periodo_activo_admin` int(11) DEFAULT NULL,
  `ciclo_escolar` varchar(45) NOT NULL,
  `f_registro` datetime DEFAULT NULL,
  `f_modif` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of periodo
-- ----------------------------
BEGIN;
INSERT INTO `periodo` VALUES (1, '2021-10-12', '2021-10-12', 'Febrero 2021 - Julio 2021', 1, 1, 'Febrero 2021 - Julio 2021', '2021-01-02 18:57:47', '2021-01-02 18:57:47');
COMMIT;

-- ----------------------------
-- Table structure for periodo_registro
-- ----------------------------
DROP TABLE IF EXISTS `periodo_registro`;
CREATE TABLE `periodo_registro` (
  `int` int(11) NOT NULL AUTO_INCREMENT,
  `f_inicio` date NOT NULL,
  `f_final` date NOT NULL,
  `idPeriodo` int(10) unsigned NOT NULL,
  PRIMARY KEY (`int`),
  KEY `´fk_id_periodo´` (`idPeriodo`),
  CONSTRAINT `´fk_id_periodo´` FOREIGN KEY (`idPeriodo`) REFERENCES `periodo` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for profesor
-- ----------------------------
DROP TABLE IF EXISTS `profesor`;
CREATE TABLE `profesor` (
  `id` bigint(11) NOT NULL,
  `paterno` varchar(45) NOT NULL,
  `materno` varchar(45) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `cubiculo` varchar(45) DEFAULT NULL,
  `categoria_profesor_id` int(11) NOT NULL,
  `f_registro` datetime DEFAULT NULL,
  `f_modif` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_profesor_categoria_profesor1_idx` (`categoria_profesor_id`),
  CONSTRAINT `fk_profesor_categoria_profesor1` FOREIGN KEY (`categoria_profesor_id`) REFERENCES `categoria_profesor` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for prog_educativo
-- ----------------------------
DROP TABLE IF EXISTS `prog_educativo`;
CREATE TABLE `prog_educativo` (
  `CVE_PLAN_ALUMNO` varchar(10) NOT NULL,
  `NOMBRE_PLAN_ALUMNO` varchar(45) NOT NULL,
  `VRS_PLAN_ALUMNO` varchar(8) DEFAULT NULL,
  `abr` varchar(13) NOT NULL,
  `observaciones` varchar(45) DEFAULT NULL,
  `escuela_proc_id` int(10) unsigned NOT NULL,
  `f_registro` datetime DEFAULT NULL,
  `f_modif` datetime DEFAULT NULL,
  PRIMARY KEY (`CVE_PLAN_ALUMNO`),
  KEY `fk_prog_educativo_escuela_proc1_idx` (`escuela_proc_id`),
  CONSTRAINT `fk_prog_educativo_escuela_proc1` FOREIGN KEY (`escuela_proc_id`) REFERENCES `escuela_proc` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of prog_educativo
-- ----------------------------
BEGIN;
INSERT INTO `prog_educativo` VALUES ('000001', 'INGENIERO EN COMPUTACION', 'NNNNN', 'COMP', NULL, 1, '2021-01-03 09:49:34', '2021-01-03 09:49:39');
COMMIT;

-- ----------------------------
-- Table structure for reset_password
-- ----------------------------
DROP TABLE IF EXISTS `reset_password`;
CREATE TABLE `reset_password` (
  `id` bigint(11) unsigned NOT NULL AUTO_INCREMENT,
  `usuario_id` int(11) NOT NULL,
  `f_registro` datetime NOT NULL,
  `hash_value` varchar(100) NOT NULL,
  `uso` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_reset_password_usuario1_idx` (`usuario_id`),
  CONSTRAINT `fk_reset_password_usuario1` FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for rol
-- ----------------------------
DROP TABLE IF EXISTS `rol`;
CREATE TABLE `rol` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario_id` int(11) NOT NULL,
  `nombre` varchar(60) NOT NULL,
  `f_registro` datetime DEFAULT NULL,
  `f_modif` datetime DEFAULT NULL,
  PRIMARY KEY (`id`,`usuario_id`),
  KEY `fk_rol_usuario1_idx` (`usuario_id`),
  CONSTRAINT `fk_rol_usuario1` FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rol
-- ----------------------------
BEGIN;
INSERT INTO `rol` VALUES (14, 14, 'ADMIN', '2020-12-27 00:00:00', '2020-12-27 00:00:00');
COMMIT;

-- ----------------------------
-- Table structure for status_alumno
-- ----------------------------
DROP TABLE IF EXISTS `status_alumno`;
CREATE TABLE `status_alumno` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(60) NOT NULL,
  `observaciones` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of status_alumno
-- ----------------------------
BEGIN;
INSERT INTO `status_alumno` VALUES (1, 'REGISTRADO', NULL);
INSERT INTO `status_alumno` VALUES (2, 'NO REGISTRADO', NULL);
COMMIT;

-- ----------------------------
-- Table structure for tipo_ingreso
-- ----------------------------
DROP TABLE IF EXISTS `tipo_ingreso`;
CREATE TABLE `tipo_ingreso` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tipo_ingreso
-- ----------------------------
BEGIN;
INSERT INTO `tipo_ingreso` VALUES (1, 'NORMAL');
INSERT INTO `tipo_ingreso` VALUES (2, 'EQUIVALENCIA');
INSERT INTO `tipo_ingreso` VALUES (3, 'HOMOLOGACIÓN');
COMMIT;

-- ----------------------------
-- Table structure for tipo_uap
-- ----------------------------
DROP TABLE IF EXISTS `tipo_uap`;
CREATE TABLE `tipo_uap` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `categoria_optativa` varchar(62) NOT NULL,
  `semestre` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tipo_uap
-- ----------------------------
BEGIN;
INSERT INTO `tipo_uap` VALUES (1, 'ORIENTADA A ESTRUCTURAS', 5);
INSERT INTO `tipo_uap` VALUES (2, 'ORIENTADA A GEOTECNICA', 5);
INSERT INTO `tipo_uap` VALUES (3, 'ORIENTADA A LA CONSTRUCCION', 5);
INSERT INTO `tipo_uap` VALUES (4, 'ORIENTADA A HIDRAULICA', 5);
INSERT INTO `tipo_uap` VALUES (5, 'CONSTRUCCION DE INFRAESTRUCTURA URBANA', 5);
INSERT INTO `tipo_uap` VALUES (6, 'EDIFICACION', 5);
INSERT INTO `tipo_uap` VALUES (7, 'ORIENTACION EN SISTEMAS DE INFORMACION GEOGRAFICA', 5);
INSERT INTO `tipo_uap` VALUES (8, 'ORIENTACIÓN EN GEORREFERENCIACION', 5);
INSERT INTO `tipo_uap` VALUES (9, 'ORIENTACION EN SISTEMAS E INFORMATICA EDUCATIVA', 5);
INSERT INTO `tipo_uap` VALUES (10, 'ORIENTACION EN TECNOLOGIAS DE LA INFORMACION Y COMUNICACIONES', 5);
INSERT INTO `tipo_uap` VALUES (11, 'REDES', 5);
INSERT INTO `tipo_uap` VALUES (12, 'ARQUITECTURA DE COMPUTADORAS', 5);
INSERT INTO `tipo_uap` VALUES (13, 'OBLIGATORIA', 5);
INSERT INTO `tipo_uap` VALUES (14, 'ELECTIVA', 5);
COMMIT;

-- ----------------------------
-- Table structure for tutoria
-- ----------------------------
DROP TABLE IF EXISTS `tutoria`;
CREATE TABLE `tutoria` (
  `id` bigint(11) unsigned NOT NULL AUTO_INCREMENT,
  `alumno_matricula` varchar(13) NOT NULL,
  `profesor_id` bigint(11) NOT NULL,
  `observaciones` varchar(45) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `f_registro` datetime DEFAULT NULL,
  `f_modif` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tutoria_alumno1_idx` (`alumno_matricula`),
  KEY `fk_tutoria_profesor1_idx` (`profesor_id`),
  CONSTRAINT `fk_tutoria_alumno1` FOREIGN KEY (`alumno_matricula`) REFERENCES `alumno` (`matricula`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tutoria_profesor1` FOREIGN KEY (`profesor_id`) REFERENCES `profesor` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for uap
-- ----------------------------
DROP TABLE IF EXISTS `uap`;
CREATE TABLE `uap` (
  `clave` varchar(10) NOT NULL,
  `nombre` varchar(60) NOT NULL,
  `creditos` int(11) DEFAULT NULL,
  `codigo_comun` varchar(45) DEFAULT NULL,
  `pe_CVE_PLAN_ALUMNO` varchar(10) NOT NULL,
  `tipo_uap_id` int(11) NOT NULL,
  `etapas_uap_id` int(11) NOT NULL,
  `f_registro` datetime DEFAULT NULL COMMENT '					',
  `f_modif` datetime DEFAULT NULL,
  PRIMARY KEY (`clave`),
  KEY `fk_UAP_prog_educativo1_idx` (`pe_CVE_PLAN_ALUMNO`),
  KEY `fk_UAP_tipo_uap1_idx` (`tipo_uap_id`),
  KEY `fk_UAP_etapas_uap1_idx` (`etapas_uap_id`),
  CONSTRAINT `fk_UAP_etapas_uap1` FOREIGN KEY (`etapas_uap_id`) REFERENCES `etapas_uap` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_UAP_prog_educativo1` FOREIGN KEY (`pe_CVE_PLAN_ALUMNO`) REFERENCES `prog_educativo` (`CVE_PLAN_ALUMNO`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_UAP_tipo_uap1` FOREIGN KEY (`tipo_uap_id`) REFERENCES `tipo_uap` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for usuario
-- ----------------------------
DROP TABLE IF EXISTS `usuario`;
CREATE TABLE `usuario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) NOT NULL,
  `password` varchar(100) NOT NULL,
  `profesor_id` bigint(11) DEFAULT NULL,
  `alumno_matricula` varchar(13) DEFAULT NULL,
  `bloqueo` tinyint(255) NOT NULL,
  `activo` tinyint(255) NOT NULL,
  `primer_login` tinyint(255) NOT NULL,
  `f_expiracion` date DEFAULT NULL,
  `f_registro` datetime DEFAULT NULL,
  `f_modif` datetime DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `status` tinyint(255) DEFAULT '0',
  `ultimo_login` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_usuario_profesor1_idx` (`profesor_id`),
  KEY `fk_usuario_alumno1_idx` (`alumno_matricula`),
  CONSTRAINT `fk_usuario_alumno1` FOREIGN KEY (`alumno_matricula`) REFERENCES `alumno` (`matricula`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_usuario_profesor1` FOREIGN KEY (`profesor_id`) REFERENCES `profesor` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of usuario
-- ----------------------------
BEGIN;
INSERT INTO `usuario` VALUES (14, '07046511', '$2a$10$txR1oWx6vJ2rVWbDWInOBOG.SihtTU86jb4Lg1BMUs6u3LUAzMm4i', NULL, '07046511', 0, 1, 0, NULL, '2020-12-27 20:21:46', '2020-12-31 13:27:42', 'bmorales.dev@gmail.com', 1, NULL);
COMMIT;

-- ----------------------------
-- Table structure for usuario_notificaciones
-- ----------------------------
DROP TABLE IF EXISTS `usuario_notificaciones`;
CREATE TABLE `usuario_notificaciones` (
  `id` bigint(11) unsigned NOT NULL AUTO_INCREMENT,
  `nombre_destinatario` varchar(45) NOT NULL,
  `email_destinatario` varchar(60) NOT NULL,
  `plain_password` varchar(45) NOT NULL,
  `usuario_id` int(11) DEFAULT NULL,
  `status` enum('REGISTRADA','ENVIADA') NOT NULL,
  `f_expiracion` date DEFAULT NULL,
  `tipo_notificacion` varchar(50) NOT NULL,
  `visto` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_noficaciones_usuario1_idx` (`usuario_id`),
  CONSTRAINT `fk_noficaciones_usuario1` FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

SET FOREIGN_KEY_CHECKS = 1;
