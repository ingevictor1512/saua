/*Actualiza columnas de la tabla config*/
ALTER TABLE `saua_db`.`config` 
CHANGE COLUMN `id` `config_id` int(11) NOT NULL AUTO_INCREMENT FIRST,
CHANGE COLUMN `key` `config_key` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL AFTER `config_id`,
CHANGE COLUMN `value` `config_value` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL AFTER `config_key`,
CHANGE COLUMN `description` `config_desc` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL AFTER `config_value`,
DROP PRIMARY KEY,
ADD PRIMARY KEY (`config_id`) USING BTREE;