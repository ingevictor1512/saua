package com.mx.saua.controller;

import com.mx.saua.constant.MessagesConstant;
import com.mx.saua.constant.ViewConstant;
import com.mx.saua.dto.CoffeeShopDto;
import com.mx.saua.exception.ServiceException;
import com.mx.saua.service.CoffeeShopService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 * Controlador para la administracion de las cafeterias
 * @author Victor Francisco Garcia
 * @version 1.0
 */
@Controller
@RequestMapping("admin/coffeeshop")
public class CoffeeShopController {

    @Autowired
    private CoffeeShopService coffeeShopService;

    @GetMapping(value = { "", "/", "/list"})
    public String list(Model model) {
        model.addAttribute("coffeeshops", coffeeShopService.find());
        return ViewConstant.COFFEESHOP_LIST;
    }

    @GetMapping(value = "/add")
    public String add(Model model) {
        model.addAttribute("coffeeshops", new CoffeeShopDto());
        return ViewConstant.ADD_COFFEESHOP;
    }

    @PostMapping(value = "/edit")
    public String edit(Model model, @RequestParam(name = "id", required = true) int id) {
        CoffeeShopDto coffeeShopDto = coffeeShopService.getById(id);
        model.addAttribute("coffeeshops", coffeeShopDto);
        return ViewConstant.ADD_COFFEESHOP;
    }

    @PostMapping(value = "/delete")
    public String delete(Model model, CoffeeShopDto form, RedirectAttributes redirAttrs) {
        coffeeShopService.delete(form.getId());
        redirAttrs.addFlashAttribute(ViewConstant.MESSAGE, MessagesConstant.DELETE_SUCCESS_MESSAGE);
        return ViewConstant.REDIRECT_COFFEESHOP_LIST;

    }

    @PostMapping(value = "/save")
    public String save(Model model, CoffeeShopDto form, RedirectAttributes redirAttrs) throws ServiceException {

        if(form.getId() > 0) {
            coffeeShopService.save(form);
            redirAttrs.addFlashAttribute(ViewConstant.MESSAGE, MessagesConstant.EDIT_SUCCESS_MESSAGE);
        } else {
            coffeeShopService.save(form);
            redirAttrs.addFlashAttribute(ViewConstant.MESSAGE, MessagesConstant.ADD_SUCCESS_MESSAGE);
        }
        return ViewConstant.REDIRECT_COFFEESHOP_LIST;
    }
}
