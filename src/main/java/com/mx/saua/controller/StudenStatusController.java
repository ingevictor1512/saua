package com.mx.saua.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.mx.saua.constant.MessagesConstant;
import com.mx.saua.constant.ViewConstant;
import com.mx.saua.dto.StudentStatusDto;
import com.mx.saua.exception.ServiceException;
import com.mx.saua.service.StudentStatusService;
import com.mx.saua.service.impl.UserSecurityService;

/**
 * Controller que responde a peticiones de status de alumno
 * @author Victor Francisco
 * @version 1.0
 *
 */
@Controller
@RequestMapping("admin/statusstudent")
public class StudenStatusController {
	
	/**
	 * Inyeccion de StudentStatusService
	 */
	@Autowired
	private StudentStatusService studentStatusService;
	
	/**
	 * Logger de la clase
	 */
	private Logger LOGGER = LoggerFactory.getLogger(StudentStatusService.class);
	
	/**
	 * Inyección de UserSecurityService
	 */
	@Autowired
	private UserSecurityService userSecurityService;
	
	/**
	 * Responde a peticion list
	 * @param model
	 * @return
	 */
	@GetMapping(value = { "", "/" })
	public String list(Model model) {
		model.addAttribute("studentstatus", studentStatusService.find());
		return ViewConstant.STUDENTSTATUS_LIST;
	}
	
	@GetMapping(value = "/add")
	public String add(Model model) {
		LOGGER.info("{} - Ingresando a agregar status de alumno.", userSecurityService.getIdSession());
		model.addAttribute("studentstatus", new StudentStatusDto());
		return ViewConstant.ADD_STUDENTSTATUS;
	}
	
	@PostMapping(value = "/edit")
	public String edit(Model model, @RequestParam(name = "id", required = true) Long id) {
		StudentStatusDto studentStatusDto = studentStatusService.getById(id);
		model.addAttribute("studentstatus", studentStatusDto);
		return ViewConstant.ADD_STUDENTSTATUS;
	}
	
	@PostMapping(value = "/delete")
	public String delete(Model model, StudentStatusDto form, RedirectAttributes redirAttrs) {
		studentStatusService.delete(form.getId());
		redirAttrs.addFlashAttribute(ViewConstant.MESSAGE, MessagesConstant.DELETE_SUCCESS_MESSAGE);
		return ViewConstant.REDIRECT_STUDENTSTATUS_LIST;
	}
	
	@PostMapping(value = "/save")
	public String save(Model model, StudentStatusDto form, RedirectAttributes redirAttrs) throws ServiceException {
		if(form.getId() > 0) {
			studentStatusService.save(form);
			redirAttrs.addFlashAttribute(ViewConstant.MESSAGE, MessagesConstant.EDIT_SUCCESS_MESSAGE);
		} else {
			studentStatusService.save(form);
			redirAttrs.addFlashAttribute(ViewConstant.MESSAGE, MessagesConstant.ADD_SUCCESS_MESSAGE);
		}
		return ViewConstant.REDIRECT_STUDENTSTATUS_LIST;
	}

}
