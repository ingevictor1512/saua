package com.mx.saua.controller;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.commons.codec.digest.DigestUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MaxUploadSizeExceededException;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.mx.saua.constant.ErrorConstant;
import com.mx.saua.constant.MessagesConstant;
import com.mx.saua.constant.ViewConstant;
import com.mx.saua.dto.DocumentDto;
import com.mx.saua.dto.GroupDto;
import com.mx.saua.dto.TeacherDto;
import com.mx.saua.entity.Document;
import com.mx.saua.enums.AuthorityEnum;
import com.mx.saua.enums.ConfigurationEnum;
import com.mx.saua.exception.ServiceException;
import com.mx.saua.repository.ConfigurationRepository;
import com.mx.saua.repository.DocumentRepository;
import com.mx.saua.service.DocumentService;
import com.mx.saua.service.GroupService;
import com.mx.saua.service.TeacherService;
import com.mx.saua.service.impl.UserSecurityService;

/**
 * Controller que responde a las peticiones para el manejo de documentos
 * @author Benito Morales
 * @version 1.0
 *
 */
@Controller
@ControllerAdvice
@RequestMapping("admin/documents")
public class DocumentsController {
	
	/**
	 * Logger de la clase
	 */
	private Logger logger = LoggerFactory.getLogger(DocumentsController.class);

	/**
	 * Inyección de ConfigurationRepository
	 */
	@Autowired
	private ConfigurationRepository configurationRepository;
	
	/**
	 * Inyección de DocumentRepository
	 */
	@Autowired
	private DocumentRepository documentRepository;
	
	/**
	 * Inyección de DocumentService
	 */
	@Autowired
	private DocumentService documentService;
	
	@Autowired
	private GroupService groupService;
	
	@Autowired
	private TeacherService teacherService;
	
	/**
	 * Inyección de UserSecurityService
	 */
	@Autowired
	private UserSecurityService userSecurityService;
	
	
	@GetMapping(value = { "", "/", "/list" })
	public String list(Model model) {
		model.addAttribute("documentDto", documentService.find(new DocumentDto()));
		return ViewConstant.DOCUMENTS_LIST;
	}
	
	@GetMapping(value = "/add")
	public String add(Model model) {
		model.addAttribute("documentDto", new DocumentDto());
		return ViewConstant.ADD_DOCUMENT;
	}
	
	@PostMapping("/save")
	public String upload(@RequestParam("file") MultipartFile documentFile, Model model, 
			DocumentDto form, RedirectAttributes redirAttrs)  throws ServiceException, NoSuchFileException {
		logger.info("documentDto:{}", form.toString());
		logger.info("MULTIPART-ContentType:{}", documentFile.getContentType());
		logger.info("MULTIPART-OriginalFilename:{}", documentFile.getOriginalFilename());
		if(!documentFile.getOriginalFilename().contains("docx") && !documentFile.getOriginalFilename().contains("doc") && !documentFile.getOriginalFilename().contains("xlsx") 
				&& !documentFile.getOriginalFilename().contains("xls") && !documentFile.getOriginalFilename().contains("csv") && !documentFile.getOriginalFilename().contains("pdf")) {
			redirAttrs.addFlashAttribute(ErrorConstant.ERROR_MESSAGE, MessagesConstant.INVALID_FILE_MESSAGE);
			model.addAttribute("documentDto", form);
			return ViewConstant.REDIRECT_ADD_DOCUMENT;
		}
		
		try {
			String[] splitname = documentFile.getOriginalFilename().split("\\.");
			String message = String.format("%s", new java.util.Date());
			String name = DigestUtils.sha1Hex(message)+"."+splitname[1];
			
			Path path = Paths.get(configurationRepository.findByKey(ConfigurationEnum.DOCUMENT_PATH.toString()).getValue(),name);
			byte[] bytes = documentFile.getBytes();
			Files.write(path, bytes);
			
			String roles = "";
			roles = form.isAdminRole()?roles+"|"+AuthorityEnum.ADMIN:roles;
			roles = form.isStudentRole()?roles+"|"+AuthorityEnum.STUDENT:roles;
			roles = form.isCoordinatorRole()?roles+"|"+AuthorityEnum.TUTORING_COORDINATOR:roles;
			roles = form.isTutorRole()?roles+"|"+AuthorityEnum.TUTOR:roles;
			roles = form.isTeacherRole()?roles +"|"+AuthorityEnum.TEACHER:roles;
			
			//Si se escribió el archivo correctamente.
			Document document = new Document();
			document.setDescription(form.getDescription());
			document.setRealName(name);
			document.setViewName(form.getViewName());
			document.setAuthorizationsRole(roles);
			document.setTypeDocument(form.getTypeDocument());
			documentRepository.save(document);
		} catch (IOException e) {
			logger.error("Ocurrio un error al subir archivo:{}",e.getMessage());
			redirAttrs.addFlashAttribute(ErrorConstant.ERROR_MESSAGE, "Ocurri\u00F3 un error al subir archivo.");
			model.addAttribute("documentDto", form);
			return ViewConstant.REDIRECT_ADD_DOCUMENT;
		}
		
		redirAttrs.addFlashAttribute(ViewConstant.MESSAGE, "Se guard\u00F3 el documento correctamente.");
		return ViewConstant.REDIRECT_DOCUMENTS_LIST;
	}
	
	@ExceptionHandler(MaxUploadSizeExceededException.class)
	public String handleFileUploadError(RedirectAttributes redirAttrs){
		redirAttrs.addFlashAttribute(ErrorConstant.ERROR_MESSAGE, "El archivo excede el tama\u00F1o m\u00E1ximo permitido(5MB).");
		return ViewConstant.REDIRECT_DOCUMENTS_LIST;
	}
	
	@PostMapping(value = "/delete")
	public String delete(Model model, DocumentDto form, RedirectAttributes redirAttrs) {
		logger.info("{} - DocumentController - Ingresando a eliminar documento:{}", userSecurityService.getIdSession(), form.toString());
		try {
			documentService.delete(form);
			redirAttrs.addFlashAttribute(ViewConstant.MESSAGE, "Se elimin\u00F3 el documento correctamente.");
			model.addAttribute("documentDto",documentService.find(documentService.find(new DocumentDto())));
			logger.info("{} - DocumentController - Documento eliminado correctamente", userSecurityService.getIdSession());
			return ViewConstant.REDIRECT_DOCUMENTS_LIST;
		} catch (ServiceException se) {
			redirAttrs.addFlashAttribute(ErrorConstant.ERROR_MESSAGE, se.getMessage());
			model.addAttribute("documentDto",documentService.find(documentService.find(new DocumentDto())));
			return ViewConstant.REDIRECT_DOCUMENTS_LIST;
		}
	}
	
	@GetMapping("/exportListExcelByGroupId")
	public ResponseEntity<InputStreamResource> exportListExcelByGroupId(@RequestParam(name = "id", required = true) Long id) throws Exception{
		logger.info("{} - DocumentsController - ID de grupo recibido:{}", userSecurityService.getIdSession(), id);
		GroupDto grupo= groupService.getGroupById(id);
		ByteArrayInputStream  stream = documentService.exportListExcelByGroupId(id);
		
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Disposition", "attachment; filename=Listas_del_grupo_"+grupo.getName()+"_"+grupo.getEducationalProgramDto().getAbr()+"_"+grupo.getAbbreviation()+".xls");
		
		return ResponseEntity.ok().headers(headers).body(new InputStreamResource(stream));
	}
	
	@GetMapping("/exportListExcelByTeacherId")
	public ResponseEntity<InputStreamResource> exportListExcelByTeacherId(@RequestParam(name = "id", required = true) String id) throws Exception{
		logger.info("{} - DocumentsController - ID de docente recibido:{}", userSecurityService.getIdSession(), id);
		/*Obtenemos el profesor recibido */
		TeacherDto teacher = teacherService.getTeacherByRegister(id.toString());
		ByteArrayInputStream  stream = documentService.exportListExcelByTeacherId(id);
		
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Disposition", "attachment; filename=LISTAS_DE_UAP_DE_"+teacher.getName()+"_"+teacher.getLastname()+"_"+teacher.getSecondLastname()+".xls");
		
		return ResponseEntity.ok().headers(headers).body(new InputStreamResource(stream));
	}
	@GetMapping("/exportStudentsListExcelByGroupId")
	public ResponseEntity<InputStreamResource> exportStudentsListExcelByGroupId(@RequestParam(name = "id", required = true) Long id) throws Exception{
		logger.info("{} - DocumentsController - ID de grupo recibido:{}", userSecurityService.getIdSession(), id);
		GroupDto grupo= groupService.getGroupById(id);
		//ByteArrayInputStream  stream = documentService.exportListExcelByGroupId(id);
		ByteArrayInputStream  stream = documentService.exportStudentsListExcelByGroupId(id);
		
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Disposition", "attachment; filename=Listas_del_grupo_"+grupo.getName()+"_"+grupo.getEducationalProgramDto().getAbr()+"_"+grupo.getAbbreviation()+".xls");
		
		return ResponseEntity.ok().headers(headers).body(new InputStreamResource(stream));
	}
}
