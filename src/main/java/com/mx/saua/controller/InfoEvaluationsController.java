package com.mx.saua.controller;

import com.mx.saua.constant.ErrorConstant;
import com.mx.saua.constant.MessagesConstant;
import com.mx.saua.constant.ViewConstant;
import com.mx.saua.dto.*;
import com.mx.saua.entity.Configuration;
import com.mx.saua.entity.Tutorship;
import com.mx.saua.enums.ConfigurationEnum;
import com.mx.saua.exception.ServiceException;
import com.mx.saua.repository.ConfigurationRepository;
import com.mx.saua.repository.TutorshipRepository;
import com.mx.saua.service.*;
import com.mx.saua.service.impl.UserSecurityService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;


/**
 * Controller que responde a la petcion de informacion de evaluaciones
 *
 * @author Victor Francisco
 *
 */
@Controller
@RequestMapping("admin/infoeval")
public class InfoEvaluationsController {
    /**
     * Logger de la clase
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(InfoEvaluationsController.class);

    @Autowired
    private UserSecurityService userSecurityService;
    @Autowired
    private InfoEvaluationService infoEvaluationService;
    @Autowired
    private UserService userService;
    @Autowired
    private StudentService studentService;
    @Autowired
    private ConfigurationRepository configurationRepository;
    @Autowired
    private TutorshipRepository tutorshipRepository;
    @Autowired
    private UserAnswerEvalService userAnswerEvalService;

    @GetMapping(value = { "", "/", "/list"})
    public String list(Model model) {
        LOGGER.info("{} - Inicia carga de informacion de evaluaciones", userSecurityService.getIdSession());
        model.addAttribute("infoEvaluationDto", infoEvaluationService.find(new InfoEvaluationDto()));
        LOGGER.info("{} - Termina carga de informacion de evaluaciones", userSecurityService.getIdSession());
        return ViewConstant.INFOEVAL_LIST;
    }
    @GetMapping(value = "/add")
    public String add(Model model) {
        model.addAttribute("infoEvaluationDto", new InfoEvaluationDto());
        return ViewConstant.ADD_INFOEVAL;
    }
    @PostMapping(value = "/edit")
    public String edit(Model model, @RequestParam(name = "id", required = true) long idUsuario) {
        LOGGER.info("{} - Ingresando a editar el total de las evaluaciones del usuario:{}", userSecurityService.getIdSession(), idUsuario);
        Configuration previosPeriod= configurationRepository.findByKey(ConfigurationEnum.PREVIOUS_PERIOD.toString());
            model.addAttribute("infoEvaluationDto", infoEvaluationService.findByIdInfoEvaluationUserAndIdInfoEvaluationPeriod(idUsuario,Long.parseLong(previosPeriod.getValue())));
            return ViewConstant.EDIT_INFOEVAL;

    }
    @PostMapping(value = "/save")
    public String save(Model model, InfoEvaluationDto form, RedirectAttributes redirAttrs) {
        LOGGER.info("{} - Ingresando a guardar la modificacion de evaluaciones contestadas: {}",userSecurityService.getIdSession(),form.toString());
        try {
            if(form.getIdUsuario()>0) {
                infoEvaluationService.save(form);
                LOGGER.info("{} - Totales modificados correctamente", userSecurityService.getIdSession());
                redirAttrs.addFlashAttribute(ViewConstant.MESSAGE, MessagesConstant.EDIT_SUCCESS_MESSAGE);
            }else {
                infoEvaluationService.save(form);
                LOGGER.info("{} - Toales agregados correctamente", userSecurityService.getIdSession());
                redirAttrs.addFlashAttribute(ViewConstant.MESSAGE, MessagesConstant.ADD_SUCCESS_MESSAGE);
            }
            return ViewConstant.REDIRECT_INFOEVAL_LIST;
        } catch (ServiceException se) {
            model.addAttribute(ErrorConstant.ERROR_MESSAGE, se.getMessage());
            return ViewConstant.REDIRECT_INFOEVAL_LIST;
        }
    }
    @PostMapping(value = "/show")
    public String show(Model model, @RequestParam(name = "id", required = true) long idUsuario) {
        LOGGER.info("{} - Ingresando a revisar las evaluaciones contestadas del IdUsuario:{}", userSecurityService.getIdSession(), idUsuario);
        UserDto user = userService.getById(idUsuario);
        StudentChargeDto studentChargeDto = studentService.findChargeByRegisterByPeriodoId(user.getStudentDto().getRegister());
        LOGGER.info("{} - Ingresando a mostrar el total de las evaluaciones del usuario:{}", userSecurityService.getIdSession(), idUsuario);
        List<Tutorship> tutor = tutorshipRepository.findByStudentRegister(user.getStudentDto().getRegister());
        studentChargeDto.setStudentRegister(user.getStudentDto().getRegister());
        studentChargeDto = userAnswerEvalService.getStatusEvaluations(studentChargeDto);
        if (tutor != null && tutor.size() > 0) {
            studentChargeDto.setTutorName(
                    tutor.get(0).getTeacher().getName() + " " + tutor.get(0).getTeacher().getLastname() + " "
                            + tutor.get(0).getTeacher().getSecondLastname());
        } else {
            studentChargeDto.setTutorName("Sin tutor asignado");
        }
        model.addAttribute("studentChargeDto", studentChargeDto);
        //model.addAttribute("studentChargeDto", infoEvaluationService.findByIdInfoEvaluationUserAndIdInfoEvaluationPeriod(idUsuario,2L));
        return ViewConstant.INFOEVAL_LIST_REPORT;

    }

    @PostMapping(value = "/activateEval")
    public String active(Model model, RedirectAttributes redirAttrs, @RequestParam(name = "id", required = true) Long id) {
        LOGGER.info("Activando las evaluaciones del id:{}",id);
        try{
            infoEvaluationService.activateEvaluations(id);
            redirAttrs.addFlashAttribute(ViewConstant.MESSAGE, "Se activar\u00F3n las evaluaciones correctamente.");
        }catch(ServiceException se) {
            redirAttrs.addFlashAttribute(ViewConstant.MESSAGE, se.getMessage());
        }
        model.addAttribute("infoEvaluationDto", infoEvaluationService.find(new InfoEvaluationDto()));
        return ViewConstant.REDIRECT_INFOEVAL_LIST;
    }

    @PostMapping(value = "/desactivateEval")
    public String desactive(Model model, RedirectAttributes redirAttrs, @RequestParam(name = "id", required = true) Long id) {
        LOGGER.info("Desactivando las evaluaciones del id:{}",id);
        try{
            infoEvaluationService.desactivateEvaluations(id);
            redirAttrs.addFlashAttribute(ViewConstant.MESSAGE, "Se desactivar\u00F3n las evaluaciones correctamente.");
        }catch(ServiceException se) {
            redirAttrs.addFlashAttribute(ViewConstant.MESSAGE, se.getMessage());
        }
        model.addAttribute("infoEvaluationDto", infoEvaluationService.find(new InfoEvaluationDto()));
        return ViewConstant.REDIRECT_INFOEVAL_LIST;
    }

}
