package com.mx.saua.controller;

import java.io.ByteArrayInputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.mx.saua.constant.ErrorConstant;
import com.mx.saua.constant.MessagesConstant;
import com.mx.saua.constant.ViewConstant;
import com.mx.saua.dto.GrantDto;
import com.mx.saua.exception.ServiceException;
import com.mx.saua.service.GrantService;
import com.mx.saua.service.impl.UserSecurityService;

/**
 * Controller que responde a peticiones de Becas
 * @author Victor Francisco
 * @version 1.0
 *
 */
@Controller
@RequestMapping("admin/grant")
public class GrantController {
    /**
	 * Logger de la clase
	 */
	private Logger log = LoggerFactory.getLogger(GrantController.class);

    /**
	 * Inyección de GrantService
	 */
	@Autowired
	private GrantService grantService;
    @Autowired
	private UserSecurityService userSecurityService;

    @GetMapping(value = { "", "/", "/list" })
	public String list(Model model) {
		log.info("{} - GrantController - Generando el listado de becas", userSecurityService.getIdSession());
		model.addAttribute("grantDtos",grantService.find());
		log.info("{} - GrantController - Listado de becas generado correctamente", userSecurityService.getIdSession());
		return ViewConstant.GRANT_LIST;
	}
	@GetMapping(value = "/add")
	public String add(Model model) {
		model.addAttribute("grantDto", new GrantDto());
		return ViewConstant.ADD_GRANT;
	}

    @PostMapping(value = "/edit")
	public String edit(Model model, @RequestParam(name = "id", required = true) Long id) {
		GrantDto grantDto = grantService.getById(id);
		model.addAttribute("grantDto", grantDto);
		return ViewConstant.ADD_GRANT;
	}
    @PostMapping(value = "/save")
	public String save(Model model, GrantDto form, RedirectAttributes redirAttrs) throws ServiceException {
		if(form.getId() > 0) {
			grantService.save(form);
			redirAttrs.addFlashAttribute(ViewConstant.MESSAGE, MessagesConstant.EDIT_SUCCESS_MESSAGE);
		} else {
			grantService.save(form);
			redirAttrs.addFlashAttribute(ViewConstant.MESSAGE, MessagesConstant.ADD_SUCCESS_MESSAGE);
		}
		return ViewConstant.REDIRECT_GRANT_LIST;
	}  

    @PostMapping(value = "/delete")
	public String delete(Model model, GrantDto form, RedirectAttributes redirAttrs) {
		try {
			grantService.delete(form.getId());
			redirAttrs.addFlashAttribute(ViewConstant.MESSAGE, MessagesConstant.DELETE_SUCCESS_MESSAGE);
		} catch (ServiceException se) {
			redirAttrs.addFlashAttribute(ErrorConstant.ERROR_MESSAGE, se.getMessage());
		}
		return ViewConstant.REDIRECT_GRANT_LIST;
	}
	@GetMapping("/export")
	public ResponseEntity<InputStreamResource> exportAllGrants() throws Exception{
		ByteArrayInputStream stream = grantService.exportAllGrants();

		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Disposition", "attachment; filename=Cat_Becas.xls");

		return ResponseEntity.ok().headers(headers).body(new InputStreamResource(stream));
	}
	/**
	 * Rest para descarga de los becarios por beca
	 *
	 * @param request
	 * @return Listado de becarios
	 */
	/*@GetMapping("/exportbecarios/{idInquest}")
	public ResponseEntity<Resource> papeleta(@PathVariable(value = "idInquest") long idInquest, HttpServletRequest request) {
		log.info("{} - Inicia peticion para descarga de reporte de encuesta:{}", userSecurityService.getIdSession(), idInquest);
		GrantDto grantDto = grantService.getById(idInquest);
		//InquestDto inquestDto = inquestService.findInquestById(idInquest);
		Resource resource = null;
		try {
			resource = grantService.exportBecarios(idInquest);
		} catch (ServiceException se) {
			return ResponseEntity.status(500).body(null);
		}
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddkkMMss");
		log.info("{} - Termina peticion para descarga de reporte de encuesta:{}", userSecurityService.getIdSession(), idInquest);
		return ResponseEntity.ok().contentType(MediaType.parseMediaType("application/vnd.ms-excel")).header(
						HttpHeaders.CONTENT_DISPOSITION,
						"attachment; filename=reporte_de_encuesta_" + grantDto.getGrant() +"_"+sdf.format(new Date())+".xls")
				.body(resource);
	}*/
}
