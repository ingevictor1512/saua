package com.mx.saua.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.mx.saua.constant.MessagesConstant;
import com.mx.saua.constant.ViewConstant;
import com.mx.saua.dto.TutoringLowDto;
import com.mx.saua.exception.ServiceException;
import com.mx.saua.service.TutoringLowService;

/**
 * Cnotroller TutoringLowController
 * @author Victor Francisco
 * @version 1.0
 *
 */
@Controller
@RequestMapping("admin/tutoringlows")
public class TutoringLowController {
	
	@Autowired
	private TutoringLowService tutoringLowService;
	
	@GetMapping(value = { "", "/", "/list"})
	public String list(Model model) {
		model.addAttribute("tutoringLows", tutoringLowService.find());
		return ViewConstant.TUTORINGLOW_LIST;
	}
	
	@GetMapping(value = "/add")
	public String add(Model model) {
		model.addAttribute("tutoringLows", new TutoringLowDto());
		return ViewConstant.ADD_TUTORINGLOW;
	}
	
	@PostMapping(value = "/edit")
	public String edit(Model model, @RequestParam(name = "id", required = true) Long id) {
		TutoringLowDto tutoringLowDto = tutoringLowService.getById(id);
		model.addAttribute("tutoringLows", tutoringLowDto);
		return ViewConstant.ADD_TUTORINGLOW;
	}
	
	@PostMapping(value = "/delete")
	public String delete(Model model, TutoringLowDto form, RedirectAttributes redirAttrs) {
		tutoringLowService.delete(form.getId());
		redirAttrs.addFlashAttribute(ViewConstant.MESSAGE, MessagesConstant.DELETE_SUCCESS_MESSAGE);
		return ViewConstant.REDIRECT_TUTORINGLOW_LIST;
	
	}
	
	@PostMapping(value = "/save")
	public String save(Model model, TutoringLowDto form, RedirectAttributes redirAttrs) throws ServiceException {
		if(form.getId() > 0) {
			tutoringLowService.save(form);
			redirAttrs.addFlashAttribute(ViewConstant.MESSAGE, MessagesConstant.EDIT_SUCCESS_MESSAGE);
		} else {
			tutoringLowService.save(form);
			redirAttrs.addFlashAttribute(ViewConstant.MESSAGE, MessagesConstant.ADD_SUCCESS_MESSAGE);
		}
		return ViewConstant.REDIRECT_TUTORINGLOW_LIST;
	}

}
