package com.mx.saua.controller;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.mx.saua.constant.ErrorConstant;
import com.mx.saua.constant.MessagesConstant;
import com.mx.saua.constant.ViewConstant;
import com.mx.saua.dto.ResetPasswordDto;
import com.mx.saua.exception.ServiceException;
import com.mx.saua.service.ResetPasswordService;
import com.mx.saua.service.impl.UserSecurityService;

/**
 * Controller que responde a la solicitud de listado reseteos de contraseña.
 * 
 * @author Benito Morales
 *
 */
@Controller
@RequestMapping("admin/resetpassword")
public class ResetPasswordViewController {

	/**
	 * Logger de la clase
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(ResetPasswordViewController.class);

	/**
	 * Inyección de UserSecurityService
	 */
	@Autowired
	private UserSecurityService userSecurityService;

	/**
	 * Inyecciòn de ResetPasswordService
	 */
	@Autowired
	private ResetPasswordService resetPasswordService;

	
//	@RequestMapping(value = { "", "/", "/list"}, method = GET)
//	public String list(Model model) {
//		LOGGER.info("Cargando resetPassword.");
//		model.addAttribute("resetDto", resetPasswordService.find(new ResetPasswordDto()));
//		return ViewConstant.RESET_PASSWORD_LIST;
//	}
	
	@GetMapping(value = { "", "/", "/list"})
	public String list(Model model) {
		LOGGER.info("Cargando resetPassword.");
		ResetPasswordDto dto =  resetPasswordService.findPageable(new ResetPasswordDto());
		List<String> combo = new ArrayList<String>();
		combo.add("");
		combo.add("Usuario");
		combo.add("F. Registro");
		combo.add("Email");
		combo.add("Usado");
		dto.setComboSearch(combo);
		model.addAttribute("resetDto",dto);
		return ViewConstant.RESET_PASSWORD_LIST;
	}

	@PostMapping(value = "/edit")
	public String edit(Model model, @RequestParam(name = "id", required = true) long idReset) {
		LOGGER.info("{} - Ingresando a editar reset_password:{}", userSecurityService.getIdSession(), idReset);
		model.addAttribute("resetDto", resetPasswordService.getOne(idReset));
		return ViewConstant.RESET_PASSWORD_EDIT;
	}

	@PostMapping(value = "/save")
	public String save(Model model, ResetPasswordDto form, RedirectAttributes redirAttrs) {
		LOGGER.info("{} - Ingresando a guardar solicitud de reseteo:{}", userSecurityService.getIdSession(), form.getEmail());
		try {
			resetPasswordService.save(form);
			LOGGER.info("{} - Solicitud de reseteo modificada correctamente", userSecurityService.getIdSession());
			redirAttrs.addFlashAttribute(ViewConstant.MESSAGE, MessagesConstant.EDIT_SUCCESS_MESSAGE);
			model.addAttribute("resetDto", resetPasswordService.find(new ResetPasswordDto()));
			return ViewConstant.REDIRECT_RESET_PASSWORD_LIST;
		} catch (ServiceException se) {
			model.addAttribute(ErrorConstant.ERROR_MESSAGE, se.getMessage());
			model.addAttribute("resetDto", resetPasswordService.find(new ResetPasswordDto()));
			return ViewConstant.REDIRECT_RESET_PASSWORD_LIST;
		}
	}

}
