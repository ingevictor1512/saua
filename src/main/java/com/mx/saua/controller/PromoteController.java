package com.mx.saua.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.mx.saua.constant.ViewConstant;
import com.mx.saua.dto.GroupDto;
import com.mx.saua.service.GroupService;
import com.mx.saua.service.impl.UserSecurityService;

/**
 * Controlador para promover alumnos al siguiente grupo
 * @author Benito Morales
 * @version 1.0
 *
 */
@Controller
@RequestMapping("admin/promote")
public class PromoteController {

	/**
	 * Logger de la clase
	 */
	private Logger LOGGER = LoggerFactory.getLogger(GroupController.class);
	
	/**
	 * Inyección de groupService
	 */
	@Autowired
	private GroupService groupService;
	
	/**
	 * Inyección de UserSecurityService
	 */
	@Autowired
	private UserSecurityService userSecurityService;
	
	@GetMapping(value = "")
	public String promote(Model model) {
		LOGGER.info("{} - Generando el listado de grupos", userSecurityService.getIdSession());
		GroupDto groupDto = groupService.findGroupsInPreviuosPeriod(new GroupDto());
		model.addAttribute("groupDto",groupDto);
		LOGGER.info("{} - Listado de grupos generando correctamente", userSecurityService.getIdSession());
		return ViewConstant.PROMOTE;
	}
}
