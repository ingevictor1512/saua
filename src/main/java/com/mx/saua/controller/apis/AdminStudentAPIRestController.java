package com.mx.saua.controller.apis;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mx.saua.dto.StudentChargeDto;
import com.mx.saua.dto.StudentDto;
import com.mx.saua.entity.Student;
import com.mx.saua.exception.ServiceException;
import com.mx.saua.service.StudentService;
import com.mx.saua.service.impl.UserSecurityService;

/**
 * RestController que alberga todos los endPoints que a los que tiene acceso el Administrador
 * para el manejo de los alumnos
 * @author Benito Morales
 * @version 1.0
 *
 */
@RestController
@RequestMapping("/api/admin/student")
public class AdminStudentAPIRestController {

	/**
	 * Logger de la clase
	 */
	private Logger LOGGER = LoggerFactory.getLogger(AdminStudentAPIRestController.class);
	
	/**
	 * Inyeccion de studentService
	 */
	@Autowired
	private StudentService studentService;
	
	/**
	 * Inyeccion de userSecurityService
	 */
	@Autowired
	private UserSecurityService userSecurityService;

	@RequestMapping("/page")
	public Object[][] page(@RequestBody StudentDto studentDto)throws ServiceException {
		StudentDto dto = studentService.findPageable(studentDto);
		Object[][] datos = new Object[dto.getStudentList().size()][7];
		int i = 0;
		for(Student student : dto.getStudentList()) {
			datos[i][0] = student.getRegister();
        	datos[i][1] = student.getName()+" "+student.getLastname()+" "+student.getSecondLastname();
        	datos[i][2] = student.getGroup().getName()+" "+student.getGroup().getTurn()+" ["+student.getGroup().getEducationalProgram().getAbr()+"]";
        	datos[i][3] = student.getGroup().getEducationalProgram().getStudentPlanKey();
        	datos[i][4] = studentService.getStudentByRegister(student.getRegister()).getEmail();
        	datos[i][5] = student.getTotalAutoEval();
        	datos[i][6] = student.getTotalTeacherEval();
        	i++;
		}
		return datos;
	}
	
	@RequestMapping("/page/rest")
	public ResponseEntity<StudentDto> studentPageable(@RequestBody StudentDto studentDto) {
		LOGGER.info("Consumiendo /api/admin/student/page{}", studentDto);
		return new ResponseEntity<>(studentService.findPageable(studentDto), null, HttpStatus.OK);
	}
	
	/**
	 * Responde a la petición Rest, para la busqueda de las coincidencias de la matricula buscada
	 * @param program matricula a buscar
	 * @return Lista con las coincidencias
	 * @throws ServiceException 
	 */
	@RequestMapping("/findbymatricula/{register}")
	public StudentDto findByMatricula(@PathVariable(value = "register") String register) throws ServiceException {
		LOGGER.info("{} - Inicia rest findbymatricula[{}].", userSecurityService.getIdSession(), register);
		StudentDto dto = studentService.getStudentByRegister(register);
		LOGGER.info("{} - Termina rest findbymatricula[{}].", userSecurityService.getIdSession(), register);
		return dto;
	}
	
	/**
	 * 
	 * @param studentChargeDto Dto con los datos a agregar
	 * @return Resultado de la transaccion
	 * @author Benito Morales
	 * @throws ServiceException
	 */
	@RequestMapping("/savecharge")
	public String saveCharge(@RequestBody StudentChargeDto studentChargeDto)throws ServiceException {
		LOGGER.info("{} - Inicia rest savecharge[{}] - [Datos:{}].", userSecurityService.getIdSession(), studentChargeDto.getStudentRegister(),
				"idProgram:"+studentChargeDto.getIdProgram()+" idGroup:"+studentChargeDto.getIdGroup()+" idUap:"+studentChargeDto.getIdUap());
		if(studentChargeDto.getIdGroup().trim().equals("")||studentChargeDto.getIdUap().trim().equals("")||studentChargeDto.getIdProgram().trim().equals("")) {
			LOGGER.error("{} - Datos invalidos.", userSecurityService.getIdSession());
			return "Ocurri\u00F3 un problema al procesar su solicitud, favor de intentarlo de nuevo.";
		}
		try{
			studentService.saveCharge(studentChargeDto);
			LOGGER.info("{} - Termina rest savecharge[{}].", userSecurityService.getIdSession(), studentChargeDto.getStudentRegister());
			return "{\"result\":\"ok\"}";
		}catch(ServiceException se) {
			LOGGER.info("{} - Error:{}.", userSecurityService.getIdSession(), se.getMessage());
			return se.getMessage();
		}
	}
}
