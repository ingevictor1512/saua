package com.mx.saua.controller.apis;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mx.saua.exception.ServiceException;
import com.mx.saua.service.StatisticsService;
import com.mx.saua.service.StudentService;
import com.mx.saua.service.impl.UserSecurityService;

/**
 * RestController que alberga todos los endPoints que a los que tiene acceso el Tutor
 * @author Benito Morales
 * @version 1.0
 *
 */
@RestController
@RequestMapping("/api/tutor")
public class TutorApiRestController {
	
	private Logger LOGGER = LoggerFactory.getLogger(TutorApiRestController.class);
	
	/**
	 * Inyeccion de studentService
	 */
	@Autowired
	private StudentService studentService;
	
	@Autowired
	private UserSecurityService userSecurityService;

	/**
	 * Inyección de UserSecurityService
	 */
	@Autowired
	private StatisticsService statisticsService;
	
	
	/**
	 * Rest para descarga de papeleta de materia de alumno
	 * 
	 * @param request
	 * @return Papeleta
	 */
	@GetMapping("/download/papeleta/{register}")
	public ResponseEntity<Resource> papeletaByStudent(@PathVariable String register, HttpServletRequest request) {
		LOGGER.info("{} - Inicia peticion para descarga de papeleta por alumno:{}", userSecurityService.getIdSession(),
				register);
		Resource resource = null;
		try {
			resource = studentService.loadPapeletaAsResourceAdmin(register);
			//resource = studentService.loadPapeletaAsResourceAdminMasivo("","");
		} catch (ServiceException se) {
			switch (se.getMessage()) {
			case "501":
				// El usuario no es un alumno.
				return ResponseEntity.status(501).body(null);
			case "502":
				// El usuario no esta autorizado para imprimir papeleta.
				return ResponseEntity.status(502).body(null);
			case "503":
				// Carga academica no autorizada.
				return ResponseEntity.status(503).body(null);
			default:
				LOGGER.error(se.getMessage());
				return ResponseEntity.status(500).body(null);
			}

		}
		LOGGER.info("{} - Termina peticion para descarga de papeleta.", userSecurityService.getIdSession());
		return ResponseEntity.ok().contentType(MediaType.parseMediaType("application/pdf"))
				.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=papeleta_" + register + ".pdf")
				.body(resource);
	}
	
	@RequestMapping("/statistics/tutors")
	public Object[][] findLoads() {
		return statisticsService.findStatisticsForTutor();
	}
}
