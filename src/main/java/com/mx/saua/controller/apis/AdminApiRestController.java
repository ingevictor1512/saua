package com.mx.saua.controller.apis;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mx.saua.constant.FormatsConstant;
import com.mx.saua.dto.*;
import com.mx.saua.service.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import com.mx.saua.entity.OfferedClass;
import com.mx.saua.entity.Question;
import com.mx.saua.entity.ResetPassword;
import com.mx.saua.exception.ServiceException;
import com.mx.saua.service.impl.UserSecurityService;

/**
 * RestController que alberga todos los endPoints que a los que tiene acceso el Administrador
 * @author Benito Morales
 * @version 1.0
 *
 */
@RestController
@RequestMapping("/api/admin")
public class AdminApiRestController {
	
	private Logger LOGGER = LoggerFactory.getLogger(AdminApiRestController.class);
	
	/**
	 * Inyección del servicio
	 */
	@Autowired
	private TeacherService teacherService;
	
	@Autowired
	private UserSecurityService userSecurityService;
	
	@Autowired
	private QuestionService questionService;
	
	@Autowired
	private UapService uapService;
	
	/**
	 * Inyección de GroupService
	 */
	@Autowired
	private GroupService groupService;
	
	/**
	 * Inyección de ProgramService
	 */
	@Autowired
	private ProgramService programService;
	
	/**
	 * Inyeccion de studentService
	 */
	@Autowired
	private StudentService studentService;
	
	/**
	 * Inyección de ListService
	 */
	@Autowired
	private ListService listService;
	
	/**
	 * Inyección de OfferedClassService
	 */
	@Autowired
	private OfferedClassService offeredClassService;
	
	@Autowired
	private TutorshipService tutorshipService;
	
	/**
	 * Inyección de UserSecurityService
	 */
	@Autowired
	private StatisticsService statisticsService;
	
	@Autowired
	private OfferedClassService offeredClassServive;
	
	@Autowired
	private ResetPasswordService resetPasswordService;

	@Autowired
	private ApplicantService applicantService;
	
	@Autowired
	private ApplicantCoffeeShopService applicantCoffeeShopService;
	
	@Autowired
	private UserService userService;
	
	@RequestMapping("/exportEvalsTeachersAll/{periodId}")
	public ResponseEntity<Resource> findStudent(@PathVariable(value = "periodId") long periodId) {
		LOGGER.info("Consumiendo api/teachers/exportEvalsTeachersAll/{}", periodId);
		Resource resource = null;
		try {
			resource = teacherService.exportEvalsTeachersAll(periodId);
		} catch (ServiceException se) {
			return ResponseEntity.status(500).body(null);
		}
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddkkMMss");
		LOGGER.info("{} - Termina peticion para descarga de reporte de evaluaciones de profesores:{}", userSecurityService.getIdSession(), periodId);
		return ResponseEntity.ok().contentType(MediaType.parseMediaType("application/vnd.ms-excel")).header(
				HttpHeaders.CONTENT_DISPOSITION,
				"attachment; filename=reporte_de_evaluaciones_de_profesores_"+sdf.format(new Date())+".xls")
				.body(resource);
	}
	
	/**
	 * Rest para descarga de la evaluacion docente
	 * 
	 * @param request
	 * @return Evaluacion docente
	 */
	@GetMapping("/evalDocente/{register}")
	public ResponseEntity<Resource> evalByTeacher(@PathVariable String register, HttpServletRequest request) {
		LOGGER.info("{} - Inicia peticion para descarga de la evaluacion del docente:{}", userSecurityService.getIdSession(),
				register);
		Resource resource = null;
		try {
			resource = teacherService.loadEvaluacionTeacherAsResource(register);
			//resource = studentService.loadPapeletaAsResourceAdminMasivo("","");
		} catch (ServiceException se) {
			switch (se.getMessage()) {
			case "501":
				// El usuario no es un alumno.
				return ResponseEntity.status(501).body(null);
			case "502":
				// El usuario no esta autorizado para imprimir papeleta.
				return ResponseEntity.status(502).body(null);
			case "503":
				// Carga academica no autorizada.
				return ResponseEntity.status(503).body(null);
			default:
				LOGGER.error(se.getMessage());
				return ResponseEntity.status(500).body(null);
			}

		}
		LOGGER.info("{} - Termina peticion para descarga de la evaluacion del docente.", userSecurityService.getIdSession());
		return ResponseEntity.ok().contentType(MediaType.parseMediaType("application/pdf"))
				.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=Evaluacion_Docente_de_" + register + ".pdf")
				.body(resource);
	}
	
	/**
	 * Lista las preguntas, que se pueden seleccionar para una encuesta
	 * @param questionDto Objeto con información de la encuesta
	 * @return ResponseEntity con la información de preguntas
	 */
	@PostMapping("/questions/list-page")
    public ResponseEntity<?> page(@RequestBody QuestionDto questionDto) {
		LOGGER.info("{} - Preguntas seleccionadas:{}",userSecurityService.getIdSession(), questionDto.getIdsQuestionsSelected().toString());
        QuestionAjaxResponseBodyDTO result = new QuestionAjaxResponseBodyDTO();
		QuestionDto dto = questionService.findPageable(questionDto);
		
		for (Question question : dto.getQuestionList()) {
			question.setBody("");
		}
		
		for (Question question : dto.getQuestionList()) {
			if(questionsIsIn(question, questionDto.getIdsQuestionsSelected())) {
				question.setBody("checked");
			}
		}
		
		
		result.setResult(dto.getQuestionList());
		result.setTotalPages(dto.getTotalPages());
        result.setMsg("");
        result.setIdsQuestions(questionDto.getIdsQuestionsSelected());
        return ResponseEntity.ok(result);

    }
	
	private boolean questionsIsIn(Question question, List<String> idsQuestionsSelected) {
		for (String id : idsQuestionsSelected) {
			if(String.valueOf(question.getId()).equals(id)) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Rest para descarga de papeleta de materia de alumno
	 * 
	 * @param request
	 * @return Papeleta
	 */
	@GetMapping("/download/statistics/uaps")
	public ResponseEntity<Resource> reportStatisticsByUAP(HttpServletRequest request) {
		LOGGER.info("{} - Inicia peticion para descarga de reporte estadistico de UAP's.", userSecurityService.getIdSession());
		Resource resource = null;
		try {
			resource = uapService.loadStatisticsUAPsAsResource();
		} catch (ServiceException se) {
			switch (se.getMessage()) {
			case "502":
				LOGGER.error("El usuario no esta autorizado para imprimir este reporte.");
				return ResponseEntity.status(502).body(null);
			default:
				LOGGER.error(se.getMessage());
				return ResponseEntity.status(500).body(null);
			}

		}
		LOGGER.info("{} - Inicia peticion para descarga de reporte estadistico de UAP's.", userSecurityService.getIdSession());
		return ResponseEntity.ok().contentType(MediaType.parseMediaType("application/pdf"))
				.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=Estadisticas_UAPs.pdf")
				.body(resource);
	}
	
	/**
	 * Responde a peticion rest, para asignar las uaps de un grupo a los alumnos del mismo grupo
	 * @param idGroup - Id del grupo
	 * @return Resultado de la transaccion
	 */
	@RequestMapping("/groups/assinguapsallgroup/{idgroup}")
	public String assinguapsallgroup(@PathVariable(value = "idgroup") long idGroup) throws ServiceException{
		LOGGER.info("{} - Consumiendo rest /groups/assinguapsallgroup:{}", userSecurityService.getIdSession(), idGroup);
		int result = groupService.setAcademicChargeByGroup(idGroup);
		LOGGER.info("{} - Termina rest /groups/assinguapsallgroup:{}", userSecurityService.getIdSession(), idGroup);
		if(result == 1) {
			return "{\"result\":\"ok\"}";
		} else {
			throw new ServiceException("Ocurri\u00F3 un error al asignar las uaps a los alumnos.");
		}
	}
	
	
	/**
	 * Responde a la petición Rest, para asignar las materias al programa educativo que se recibe
	 * @param studentPlanKey id del plan de estudios
	 * @return Resultado de la transaccion
	 * @throws ServiceException 
	 */
	@RequestMapping("/programs/uapsassign/{studentPlanKey}")
	public String uapsAssign(@PathVariable(value = "studentPlanKey") String studentPlanKey) throws ServiceException {
		LOGGER.info("{} - Inicia rest uapsassign[{}].", userSecurityService.getIdSession(), studentPlanKey);
		int result = programService.setAcademicChargeByProgram(studentPlanKey);
		LOGGER.info("{} - Termina rest uapsassign[{}].", userSecurityService.getIdSession(), studentPlanKey);
		if(result == 1) {
			return "{\"result\":\"ok\"}";
		} else {
			throw new ServiceException("Ocurri\u00F3 un error al asignar las uaps a los alumnos.");
		}
	}
	
	/**
	 * Rest para descarga de papeletas masivas por turno matutino
	 * 
	 * @param request
	 * @return Papeletas masivas
	 */
	@GetMapping("/download/papeletasMasVesp/{studentPlanKey}")
	public ResponseEntity<Resource> papeletasMasVesp(@PathVariable String studentPlanKey, HttpServletRequest request) {
		LOGGER.info("{} - Inicia peticion para descarga masiva de papeletas turno vespertino:{}", userSecurityService.getIdSession(),
				studentPlanKey);
		Resource resource = null;
		try {
			resource = studentService.loadPapeletaAsResourceAdminMasivo(studentPlanKey,"VESP");
		} catch (ServiceException se) {
			switch (se.getMessage()) {
			case "501":
				// El usuario no es un alumno.
				return ResponseEntity.status(501).body(null);
			case "502":
				// El usuario no esta autorizado para imprimir papeleta.
				return ResponseEntity.status(502).body(null);
			case "503":
				// Carga academica no autorizada.
				return ResponseEntity.status(503).body(null);
			default:
				LOGGER.error(se.getMessage());
				return ResponseEntity.status(500).body(null);
			}

		}
		LOGGER.info("{} - Termina peticion para descarga de papeleta.", userSecurityService.getIdSession());
		return ResponseEntity.ok().contentType(MediaType.parseMediaType("application/pdf"))
				.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=papeleta_" + studentPlanKey + ".pdf")
				.body(resource);
	}
	
	/**
	 * Rest para descarga de papeletas masivas por turno matutino
	 * 
	 * @param request
	 * @return Papeletas masivas
	 */
	@GetMapping("/papeletasMasMat/{studentPlanKey}")
	public ResponseEntity<Resource> papeletaByStudentMasMat(@PathVariable String studentPlanKey, HttpServletRequest request) {
		LOGGER.info("{} - Inicia peticion para descarga masiva de papeletas turno matutino:{}", userSecurityService.getIdSession(),
				studentPlanKey);
		Resource resource = null;
		try {
			resource = studentService.loadPapeletaAsResourceAdminMasivo(studentPlanKey,"MAT");
		} catch (ServiceException se) {
			switch (se.getMessage()) {
			case "501":
				// El usuario no es un alumno.
				return ResponseEntity.status(501).body(null);
			case "502":
				// El usuario no esta autorizado para imprimir papeleta.
				return ResponseEntity.status(502).body(null);
			case "503":
				// Carga academica no autorizada.
				return ResponseEntity.status(503).body(null);
			default:
				LOGGER.error(se.getMessage());
				return ResponseEntity.status(500).body(null);
			}

		}
		LOGGER.info("{} - Termina peticion para descarga de papeleta.", userSecurityService.getIdSession());
		return ResponseEntity.ok().contentType(MediaType.parseMediaType("application/pdf"))
				.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=papeleta_" + studentPlanKey + ".pdf")
				.body(resource);
	}
	

	/**
	 * Rest para descarga de papeleta de materia de alumno
	 * 
	 * @param request
	 * @return Papeleta
	 */
	@GetMapping("/download/papeleta/{register}")
	public ResponseEntity<Resource> papeletaByStudent(@PathVariable String register, HttpServletRequest request) {
		LOGGER.info("{} - Inicia peticion para descarga de papeleta por alumno desde el admin:{}", userSecurityService.getIdSession(),
				register);
		Resource resource = null;
		try {
			resource = studentService.loadPapeletaAsResourceAdmin(register);
			//resource = studentService.loadPapeletaAsResourceAdminMasivo("","");
		} catch (ServiceException se) {
			switch (se.getMessage()) {
			case "501":
				// El usuario no es un alumno.
				return ResponseEntity.status(501).body(null);
			case "502":
				// El usuario no esta autorizado para imprimir papeleta.
				return ResponseEntity.status(502).body(null);
			case "503":
				// Carga academica no autorizada.
				return ResponseEntity.status(503).body(null);
			default:
				LOGGER.error(se.getMessage());
				return ResponseEntity.status(500).body(null);
			}

		}
		LOGGER.info("{} - Termina peticion para descarga de papeleta.", userSecurityService.getIdSession());
		return ResponseEntity.ok().contentType(MediaType.parseMediaType("application/pdf"))
				.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=papeleta_" + register + ".pdf")
				.body(resource);
	}
	
	/**
	 * 
	 * @param GroupDto dto con los datos a agregar
	 * @return Resultado de la transaccion
	 * @author Benito Morales
	 * @throws ServiceException
	 */
	@RequestMapping(value = "/lists/studentsbyuap", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<StreamingResponseBody> download(final HttpServletResponse response,
			@RequestBody GroupDto groupDto) throws ServiceException {
		LOGGER.info("{} - ListsRestController - Generando la lista de la uap:{}", userSecurityService.getIdSession(),groupDto.getIdUap());

		try {
			byte[] pdfReport = listService.studentListByGroupUap(groupDto);

			String mimeType = "application/pdf";
			response.setContentType(mimeType);
			response.setHeader("Content-Disposition",
					String.format("attachment; filename=\"%s\"", groupDto.getFilename()+".pdf"));

			response.setContentLength(pdfReport.length);

			ByteArrayInputStream inStream = new ByteArrayInputStream(pdfReport);

			try {
				FileCopyUtils.copy(inStream, response.getOutputStream());
			} catch (IOException e) {
				LOGGER.error(e.getMessage());
			}

			StreamingResponseBody responseBody = outputStream -> {

				int numberOfBytesToWrite;
				byte[] data = new byte[1024];
				while ((numberOfBytesToWrite = inStream.read(data, 0, data.length)) != -1) {
					outputStream.write(data, 0, numberOfBytesToWrite);
				}

				inStream.close();
			};
			LOGGER.info("{} - ListsRestController - Se generó la lista correctamente", userSecurityService.getIdSession());
			return ResponseEntity.ok()
					.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=Lista de grupo.pdf")
					.contentType(MediaType.APPLICATION_PDF).body(responseBody);
		} catch (ServiceException e) {
			LOGGER.error(e.getMessage());
			return ResponseEntity.status(500)
					.contentType(MediaType.APPLICATION_PDF).body(null);
		}
	}
	
	/**
	 * 
	 * @param studentChargeDto Dto con los datos a agregar
	 * @return Resultado de la transaccion
	 * @author Benito Morales
	 * @throws ServiceException
	 */
	@RequestMapping("/groups/promote")
	public String saveCharge(@RequestBody GroupDto groupDto)throws ServiceException {
		LOGGER.info("{} - Consumiendo rest /promote:{}", groupDto.toString(), userSecurityService.getIdSession());
		try{
			groupService.promote(groupDto);
			return "{\"result\":\"ok\"}";
		}catch(ServiceException se) {
			return se.getMessage();
		}
	}
	
	/**
	 * Responde a peticion rest, para busqueda de clases ofertadas de un grupo
	 * @param idGroup - Id del grupo
	 * @return Lista de objetos, con los resultados
	 * @throws ServiceException 
	 */
	@RequestMapping("/groups/findbypromote/{idgroup}")
	public Object[][] findbypromote(@PathVariable(value = "idgroup") long idGroup) throws ServiceException{
		LOGGER.info("{} - Consumiendo rest /groups/findbypromote:{}", userSecurityService.getIdSession(), idGroup);
		return groupService.findObjectByGrouPromote(idGroup);
	}
	
	/**
	 * Responde a la petición rest, para la busqueda de las coincidencias de la UAP buscada
	 * @param program programa del cual se obtendran los profesores
	 * @return Lista con las coincidencias
	 */
	@RequestMapping("/uaps/findbygroup/{group}")
	public Object [][] findUapByGroup (@PathVariable(value = "group") Long group) {
		return offeredClassService.findByUapsForGroup(group);
	}
	
	/**
	 * Responde a peticion rest, para busqueda de clases obligatorias, ofertadas de un grupo
	 * @param idGroup - Id del grupo
	 * @return Lista de objetos, con los resultados
	 */
	@RequestMapping("/groups/offeredclassrequired/find/{idgroup}")
	public Object[][] findOfferedClassesRequiredByGroup(@PathVariable(value = "idgroup") long idGroup){
		LOGGER.info("{} - Consumiendo rest offeredclassrequired/find:{}", userSecurityService.getIdSession(), idGroup);
		return offeredClassService.getUapsRequiredByGroup(idGroup);
	}
	
	@RequestMapping("/tutorship/saveTutortoGroup")
	public String savePrueba(@RequestBody TutorshipDto tutorshipDto)throws ServiceException {
		LOGGER.info("Información a guardar en saveTutortoGroup:{}",tutorshipDto.toString());
		try{
			tutorshipService.saveTutortoGroup(tutorshipDto);
			return "{\"result\":\"ko\"}";
		}catch(ServiceException se) {
			return se.getMessage();
		}
	}
	
	/**
	 * Responde a peticion rest, para busqueda de clases obligatorias, ofertadas de un grupo
	 * @param idGroup - Id del grupo
	 * @return Lista de objetos, con los resultados
	 */
	@PostMapping("/groups/offeredclass/count")
	public String count(@RequestBody CountDto countDto) throws ServiceException {
		LOGGER.info("{} - Consumiendo rest offeredclassrequired/count:{}", userSecurityService.getIdSession(), countDto.getIdGroup());
		return offeredClassService.countStudentsByGroup(countDto);
	}
	
	/**
	 * Responde a peticion rest, para busqueda de clases ofertadas de un grupo
	 * @param idGroup - Id del grupo
	 * @return Lista de objetos, con los resultados
	 */
	@RequestMapping("/groups/offeredclass/find/{idgroup}")
	public Object[][] findOfferedClassesByGroup(@PathVariable(value = "idgroup") long idGroup){
		LOGGER.info("{} - Consumiendo rest api/find/idGroup:{}", userSecurityService.getIdSession(), idGroup);
		return offeredClassService.getUapsByGroup(idGroup);
	}
	
	@RequestMapping("/teachers/find")
	public Object[][] find(){
		return teacherService.findObject();
	}
	
	/**
	 * Responde a la petición rest, para la busqueda de las coincidencias de la UAP buscada
	 * @param program programa del cual se obtendran los profesores
	 * @return Lista con las coincidencias
	 */
	@RequestMapping("/uaps/findbyprogram/{program}")
	public Object [][] findByProgrDtos (@PathVariable(value = "program") String program) {
		return uapService.findByProgramForCatalog(program);
	}
	
	@RequestMapping("/statistics/admin")
	public Object[][] findStatisticsForAdmin() {
		return statisticsService.findStatisticsForAdmin();
	}
	
	@RequestMapping("/offeredclass/page")
	public Object[][] page(@RequestBody OfferedClassDto offeredClassDto)throws ServiceException {
		OfferedClassDto dto = offeredClassServive.findPageable(offeredClassDto);
		Object[][] datos = new Object[dto.getOfferedClassList().size()][10];
		int i = 0;
		for(OfferedClass entity : dto.getOfferedClassList()) {
			datos[i][0] = entity.getUap().getKey();
        	datos[i][1] = entity.getUap().getName();
        	datos[i][2] = entity.getTeacher().getName()+" "+entity.getTeacher().getLastname()+" "+entity.getTeacher().getSecondLastname();
        	datos[i][3] = entity.getGroup().getName()+" "+entity.getGroup().getTurn();
        	datos[i][4] = entity.getGroup().getEducationalProgram().getAbr();
        	datos[i][5] = entity.getSemester();
        	datos[i][6] = entity.getUapCharge();
        	datos[i][7] = entity.getMinCharge();
        	datos[i][8] = entity.getMaxCharge();
        	datos[i][9] = entity.getId();
        	i++;
		}
		return datos;
	}
	
	@RequestMapping("/resetpassword/page")
	public Object[][] page(@RequestBody ResetPasswordDto studentDto)throws ServiceException{
		LOGGER.info("{} - Consumiendo rest api/students/page:{}", userSecurityService.getIdSession(), studentDto.getPage());
		ResetPasswordDto dto = resetPasswordService.findPageable(studentDto);
		Object[][] datos = new Object[dto.getResetPasswordList().size()][7];
		int i = 0;
		for(ResetPassword student : dto.getResetPasswordList()) {
			datos[i][0] = student.getIdReset();
        	datos[i][1] = student.getUser().getUsername();
        	datos[i][2] = FormatsConstant.SDF_DDMMYYYY_KKMMSS.format(student.getCreated());
        	datos[i][3] = student.getHashValue();
			datos[i][4] = student.getUser().getEmail();//Cambiar al correo que viene en la notificación
			datos[i][5] = (student.isUsed())?"badge badge-primary badge-pill" : "badge badge-warning badge-pill";
			datos[i][6] = (student.isUsed())?"Si":"No";
        	i++;
		}
		return datos;
	}
	
	/**
	 * Obtiene una lista de preguntas, recibiendo como parametro sus ids
	 * 
	 * @param questionDto Información de la encuesta recibida desde el front
	 * @return Arreglo de objetos con las preguntas buscadas
	 * @throws ServiceException Excepción de servicio
	 */
	@RequestMapping("/questions/listfromlist")
	public Object[][] list(@RequestBody QuestionDto questionDto)throws ServiceException {
		LOGGER.info("{} - Obteniendo preguntas seleccionadas:{}", userSecurityService.getIdSession(), questionDto.getIdsQuestionsSelectedString());
		return questionService.findByIds(questionDto);
	}
	
	@RequestMapping("/questions/save")
	public String saveQuestion(@RequestBody QuestionDto questionDto)throws ServiceException {
		try{
			questionService.save(questionDto);
			return "{\"result\":\"ok\"}";
		}catch(ServiceException se) {
			return se.getMessage();
		}
	}
	
	@RequestMapping("/uap/delete/{list}")
	public String deleteByIds(@PathVariable(value = "list") String list)throws ServiceException {
		LOGGER.info("{} - Inicia eliminar uaps multiplw[{}].", userSecurityService.getIdSession(), list);
		try{
			uapService.deleteFromList(list);
			LOGGER.info("{} - Termina eliminar uaps multiple[{}].", userSecurityService.getIdSession(), list);
			return "{\"result\":\"ok\"}";
		}catch(ServiceException se) {
			return se.getMessage();
		}
	}
	
	@RequestMapping("/applicant/save")
	public String saveApplicants(@RequestBody ApplicantDto applicantDto)throws ServiceException {
		LOGGER.info("{} - Matriculas recibidas en el controller[{}].", userSecurityService.getIdSession(), applicantDto.getIdsStudentSelectedString());
		LOGGER.info("{} - Id beca recibida en el controller[{}].", userSecurityService.getIdSession(), applicantDto.getGrantId());

		try{
			applicantService.save(applicantDto);
			return "{\"result\":\"ok\"}";
		}catch(ServiceException se) {
			return se.getMessage();
		}
	}
	
	@RequestMapping("/applicant/assign/save")
	public String saveApplicantsAssign(@RequestBody ApplicantCoffeeShopDto applicantCoffeeShopDto)throws ServiceException {
		try{
			applicantCoffeeShopService.save(applicantCoffeeShopDto);
			return "{\"result\":\"ok\"}";
		}catch(ServiceException se) {
			LOGGER.error("Error al guardar fechas:{}", se.getMessage());
			return se.getMessage();
		}
	}
	
	@RequestMapping("/applicant/getcoffeeshops/{register}")
	public List<ApplicantCoffeeShopDto> getApplicantCoffeeShop(@PathVariable(value = "register") String register){
		LOGGER.info("Consumiendo /applicant/getcoffeeshops/{}", register);
		List<ApplicantCoffeeShopDto> dtos = applicantCoffeeShopService.getApplicantCoffeeShop(register);
		return dtos;
	}
	
	@RequestMapping("/applicant/assign/delete")
	public String saveApplicantsAssignDelete(@RequestBody ApplicantCoffeeShopDto applicantCoffeeShopDto)throws ServiceException {
		try{
			applicantCoffeeShopService.delete(applicantCoffeeShopDto);
			return "{\"result\":\"ok\"}";
		}catch(ServiceException se) {
			LOGGER.error("Error al guardar fechas:{}", se.getMessage());
			return se.getMessage();
		}
	}
	
	@GetMapping("/coffeeshop/user/search")
	public ResponseEntity<String> doAutoComplete(@RequestParam("q") final String input) {
		List<String> strings = userService.doAutoCompleteUsername(input);

		ObjectMapper mapper = new ObjectMapper();
		String resp = "";

		try {
			resp = mapper.writeValueAsString(strings);
		} catch (JsonProcessingException e) {
		}

		return new ResponseEntity<String>(resp, HttpStatus.OK);
	}
}
