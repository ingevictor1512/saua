package com.mx.saua.controller.apis;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mx.saua.dto.StudentDto;
import com.mx.saua.dto.StudentResponseDto;
import com.mx.saua.exception.ServiceException;
import com.mx.saua.service.StudentService;
import com.mx.saua.service.impl.UserSecurityService;

/**
 * RestController que alberga todos los endPoints que a los que tiene acceso el cajero
 * @author Benito Morales
 * @version 1.0
 *
 */
@RestController
@RequestMapping("/api/cashier")
public class CashierApiRestController {
	
	/**
	 * Logger de la clase
	 */
	private Logger LOGGER = LoggerFactory.getLogger(CashierApiRestController.class);
	
	@Autowired
	private StudentService studentService;
	
	/**
	 * Inyección de UserSecurityService
	 */
	@Autowired
	private UserSecurityService userSecurityService;

	@RequestMapping("/findstudent/{register}")
	public ResponseEntity<StudentResponseDto> findStudent(@PathVariable(value = "register") String register) {
		LOGGER.info("Consumiendo /api/cashier/findstudent/{}", register);
		ResponseEntity<StudentResponseDto> responseEntity = null;
		try {
			StudentResponseDto dto = studentService.getStudentResponseByRegister(register);
			responseEntity = new ResponseEntity<>(dto, null, dto.getErrorCode());
		}catch(ServiceException se){
			LOGGER.error("Error al obtener alumno:{}", se.getMessage());
			StudentResponseDto ex = new StudentResponseDto();
			ex.setErrorCode(HttpStatus.INTERNAL_SERVER_ERROR);
			ex.setErrorText(se.getMessage());
			return new ResponseEntity<>(ex, null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return responseEntity;
	}
	
	@PostMapping("/registervisit")
	public ResponseEntity<String> save(@RequestBody StudentDto studentDto)throws ServiceException {
		LOGGER.info("{} - Consumiendo rest api/cashier/registervisit - register:{}|coffeeShopId:{}", userSecurityService.getIdSession(), studentDto.getRegister(), studentDto.getCoffeeShopId());
		try{
			if(studentDto.getRegister().contains("G")) {
				studentService.registerVisitGuest(studentDto);
			}else {
				studentService.registerVisitStudent(studentDto);
			}
			LOGGER.info("Return Ok del registro de la visita.");
		}catch(ServiceException se) {
			LOGGER.error("Error al registrar la visita:{}", se.getMessage());
			return new ResponseEntity<>(se.getMessage(), null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<>("{\"result\":\"ok\"}", null, HttpStatus.OK);
	}
}
