package com.mx.saua.controller.apis;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mx.saua.dto.StudentDto;
import com.mx.saua.dto.TutorshipDto;
import com.mx.saua.exception.ServiceException;
import com.mx.saua.service.StudentService;
import com.mx.saua.service.TeacherService;
import com.mx.saua.service.TutorshipService;
import com.mx.saua.service.impl.UserSecurityService;

/**
 * RestController que alberga todos los endPoints que a los que tiene acceso el Tutor
 * @author Benito Morales
 * @version 1.0
 *
 */
@RestController
@RequestMapping("/api/coordinator")
public class CoordinatorApiRestController {
	
	/**
	 * Logger de la clase
	 */
	private Logger LOGGER = LoggerFactory.getLogger(CoordinatorApiRestController.class);
	
	@Autowired
	private TutorshipService tutorshipService;
	
	/**
	 * Inyección del servicio
	 */
	@Autowired
	private TeacherService teacherService;
	
	/**
	 * Inyeccion de studentService
	 */
	@Autowired
	private StudentService studentService;
	
	@Autowired
	private UserSecurityService userSecurityService;

	@RequestMapping("/tutorship/saveStudentsToTutor")
	public String saveStudentsToTutor(@RequestBody TutorshipDto tutorshipDto)throws ServiceException {
		LOGGER.info("Información a guardar en saveStudentsToTutor:{}",tutorshipDto.toString());
		String[] parts = tutorshipDto.getRegister().split(",");
		LOGGER.info("Separando el register recibido a un array:{}",parts.length);
		LOGGER.info("Imprimiendo las matriculas");
		for(String x: parts) {
			LOGGER.info("Matricula:{}",x);
		}
		try{
			tutorshipService.saveStudentsToTutor(tutorshipDto);
			return "{\"result\":\"ko\"}";
		}catch(ServiceException se) {
			return se.getMessage();
		}
	}
	
	@RequestMapping("/tutorship/saveTutortoGroup")
	public String savePrueba(@RequestBody TutorshipDto tutorshipDto)throws ServiceException {
		LOGGER.info("Información a guardar en saveTutortoGroup:{}",tutorshipDto.toString());
		try{
			tutorshipService.saveTutortoGroup(tutorshipDto);
			return "{\"result\":\"ko\"}";
		}catch(ServiceException se) {
			return se.getMessage();
		}
	}
	
	@RequestMapping("/teachers/find")
	public Object[][] find(){
		return teacherService.findObject();
	}
	
	/**
	 * Responde a la petición Rest, para la busqueda de las coincidencias de la matricula buscada
	 * @param program matricula a buscar
	 * @return Lista con las coincidencias
	 * @throws ServiceException 
	 */
	@RequestMapping("/students/findbymatricula/{register}")
	public StudentDto findByMatricula(@PathVariable(value = "register") String register) throws ServiceException {
		LOGGER.info("{} - Inicia rest findbymatricula[{}].", userSecurityService.getIdSession(), register);
		StudentDto dto = studentService.getStudentByRegister(register);
		LOGGER.info("{} - Termina rest findbymatricula[{}].", userSecurityService.getIdSession(), register);
		return dto;
	}
}
