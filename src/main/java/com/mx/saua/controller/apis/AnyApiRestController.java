package com.mx.saua.controller.apis;

import java.io.IOException;
import java.security.Principal;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mx.saua.controller.DocumentsController;
import com.mx.saua.dto.QuestionDto;
import com.mx.saua.exception.ServiceException;
import com.mx.saua.service.DocumentService;
import com.mx.saua.service.GroupService;
import com.mx.saua.service.InquestService;
import com.mx.saua.service.ProgramService;
import com.mx.saua.service.QuestionService;
import com.mx.saua.service.StudentService;
import com.mx.saua.service.impl.UserSecurityService;

/**
 * RestController que alberga todos los endPoints que a los que tiene acceso el alumno
 * @author Benito Morales
 * @version 1.0
 *
 */
@RestController
@RequestMapping("/api/any")
public class AnyApiRestController {
	
	/**
	 * Logger de la clase
	 */
	private Logger LOGGER = LoggerFactory.getLogger(AnyApiRestController.class);
	
	/**
	 * Inyeccion de studentService
	 */
	@Autowired
	private StudentService studentService;
	
	/**
	 * Inyeccion de userSecurityService
	 */
	@Autowired
	private UserSecurityService userSecurityService;
	
	/**
	 * Inyeccion de documentService
	 */
	@Autowired
	private DocumentService documentService;
	
	/**
	 * Inyección de QuestionService
	 */
	@Autowired
	private QuestionService questionService;
	
	/**
	 * Inyección de GroupService
	 */
	@Autowired
	private GroupService groupService;
	
	/**
	 * Inyección de ProgramService
	 */
	@Autowired
	private ProgramService programService;
	
	/**
	 * Rest para descarga de papeleta de materia de alumno
	 * 
	 * @param request
	 * @return Papeleta
	 */
	@GetMapping("/download/evaluationvoucher")
	public ResponseEntity<Resource> evaluationVoucher(HttpServletRequest request) {
		LOGGER.info("{} - Inicia peticion para descarga de comprobante de evaluacion.", userSecurityService.getIdSession());
		Resource resource = null;
		try {
			resource = studentService.loadEvaluationVoucherAsResource(userSecurityService.getUserCurrentDetails().getUsername());
		} catch (ServiceException se) {
			return ResponseEntity.status(500).body(null);
		}
		LOGGER.info("{} - Termina peticion para descarga de comprobante de evaluacion.", userSecurityService.getIdSession());
		return ResponseEntity.ok().contentType(MediaType.parseMediaType("application/pdf")).header(
				HttpHeaders.CONTENT_DISPOSITION,
				"attachment; filename=comprobante_evaluación_" + userSecurityService.getUserCurrentDetails().getUsername() + ".pdf")
				.body(resource);
	}
	
	/**
	 * Rest para descarga de papeleta de materia de alumno
	 * 
	 * @param request
	 * @return Papeleta
	 */
	@GetMapping("/download/papeleta")
	public ResponseEntity<Resource> papeleta(HttpServletRequest request) {
		LOGGER.info("{} - Inicia peticion para descarga de papeleta.", userSecurityService.getIdSession());
		Resource resource = null;
		try {
			resource = studentService.loadPapeletaAsResource(userSecurityService.getUserCurrentDetails().getUsername());
		} catch (ServiceException se) {
			return ResponseEntity.status(500).body(null);
		}
		LOGGER.info("{} - Termina peticion para descarga de papeleta.", userSecurityService.getIdSession());
		return ResponseEntity.ok().contentType(MediaType.parseMediaType("application/pdf")).header(
				HttpHeaders.CONTENT_DISPOSITION,
				"attachment; filename=papeleta_" + userSecurityService.getUserCurrentDetails().getUsername() + ".pdf")
				.body(resource);
	}
	
	/**
	 * Rest para descarga de documentos/formatos del Saua
	 * 
	 * @param id      del registro a descargar
	 * @param request
	 * @return Documento
	 */
	@GetMapping("/download/document/{id}")
	public ResponseEntity<Resource> download(@PathVariable String id, HttpServletRequest request) {
		LOGGER.info("{} - Inicia peticion para descarga de formato[{}].", userSecurityService.getIdSession(), id);
		Resource resource = null;
		try {
			resource = documentService.loadFileAsResource(id, userSecurityService.getUserCurrentDetails());
		} catch (ServiceException se) {
			LOGGER.error(se.getMessage());
			return ResponseEntity.status(500).body(null);
		}
		String contentType = null;
		try {
			contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
		} catch (IOException ex) {
			LOGGER.error("No se pudo determinar el tipo de archivo.");
			return ResponseEntity.status(500).body(null);
		}

		if (contentType == null) {
			contentType = "application/octet-stream";
		}
		LOGGER.info("{} - Termina peticion para descarga de formato[{}].", userSecurityService.getIdSession(), id);
		return ResponseEntity.ok().contentType(MediaType.parseMediaType(contentType))
				.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
				.body(resource);
	}

	@PostMapping("/inquest/save")
	public String save(@RequestBody QuestionDto questionDto)throws ServiceException {
		LOGGER.info("{} - Consumiendo rest api/inquest/save:{}", userSecurityService.getIdSession());
		try{
			questionService.saveFromApiRest(questionDto);
			LOGGER.info("Return Ok del guardado de respuestas de encuesta.");
			return "{\"result\":\"ok\"}";
		}catch(ServiceException se) {
			return se.getMessage();
		}
	}
	
	/**
	 * Responde a la petición rest, para la busqueda de las coincidencias de la UAP buscada
	 * @param program programa del cual se obtendran los profesores
	 * @return Lista con las coincidencias
	 */
	@RequestMapping("/groups/find/{idprogram}")
	public Object[][] find(@PathVariable(value = "idprogram") String idprogram, Principal principal) {
		LOGGER.info("{} - Consumiendo rest api/find/idProgram:{}", userSecurityService.getIdSession(), idprogram);
		return groupService.findObjectByProgram(idprogram);
	}
	
	/**
	 * Responde a la petición Rest, para la busqueda de todos los programas educativos
	 * @param 
	 * @return Listado de objetos con los programas educativos
	 * @throws ServiceException 
	 */
	@RequestMapping("/programs/find")
	public Object[][] find() {
		return programService.findObject();
	}

}
