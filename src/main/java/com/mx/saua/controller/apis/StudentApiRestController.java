package com.mx.saua.controller.apis;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mx.saua.dto.QuestionDto;
import com.mx.saua.dto.StudentChargeDto;
import com.mx.saua.dto.StudentDto;
import com.mx.saua.entity.Student;
import com.mx.saua.exception.ServiceException;
import com.mx.saua.service.OfferedClassService;
import com.mx.saua.service.QuestionService;
import com.mx.saua.service.StudentService;
import com.mx.saua.service.UapService;
import com.mx.saua.service.impl.UserSecurityService;

/**
 * RestController que alberga todos los endPoints que a los que tiene acceso el alumno
 * @author Benito Morales
 * @version 1.0
 *
 */
@RestController
@RequestMapping("/api/student")
public class StudentApiRestController {
	
	/**
	 * Logger de la clase
	 */
	private Logger LOGGER = LoggerFactory.getLogger(StudentApiRestController.class);
	
	/**
	 * Inyección de QuestionService
	 */
	@Autowired
	private QuestionService questionServce;
	
	/**
	 * Inyección de UserSecurityService
	 */
	@Autowired
	private UserSecurityService userSecurityService;
	
	/**
	 * Inyeccion de studentService
	 */
	@Autowired
	private StudentService studentService;
	
	/**
	 * Inyección de OfferedClassService
	 */
	@Autowired
	private OfferedClassService offeredClassService;
	
	/**
	 * Inyección de uapService
	 */
	@Autowired
	private UapService uapService;
	
	
	@PostMapping("/inquest/saveteachereval")
	public String saveDocentEvaluation(@RequestBody QuestionDto questionDto)throws ServiceException {
		LOGGER.info("{} - Consumiendo rest api/inquest/saveeval", userSecurityService.getIdSession());
		LOGGER.info("{} - QuestionDto.register:{}", userSecurityService.getIdSession(), questionDto.getStudentRegister());
		LOGGER.info("{} - QuestionDto.answerL:{}", userSecurityService.getIdSession(), questionDto.getAnswersl());
		LOGGER.info("{} - QuestionDto.studentChargeId:{}", userSecurityService.getIdSession(), questionDto.getStudentChargeId());
		try{
			questionServce.saveTeacherEvalFromApiRest(questionDto);
			LOGGER.info("Return Ok del guardado de respuestas de la evaluación docente.");
			return "{\"result\":\"ok\"}";
		}catch(ServiceException se) {
			return se.getMessage();
		}
		//return "{\"result\":\"ok\"}";
	}
	
	/**
	 * 
	 * @param studentChargeDto Dto con los datos a agregar
	 * @return Resultado de la transaccion
	 * @author Benito Morales
	 * @throws ServiceException
	 */
	@RequestMapping("/students/savecharge")
	public String saveCharge(@RequestBody StudentChargeDto studentChargeDto)throws ServiceException {
		LOGGER.info("{} - Inicia rest savecharge[{}] - [Datos:{}].", userSecurityService.getIdSession(), studentChargeDto.getStudentRegister(),
				"idProgram:"+studentChargeDto.getIdProgram()+" idGroup:"+studentChargeDto.getIdGroup()+" idUap:"+studentChargeDto.getIdUap());
		if(studentChargeDto.getIdGroup().trim().equals("")||studentChargeDto.getIdUap().trim().equals("")||studentChargeDto.getIdProgram().trim().equals("")) {
			LOGGER.error("{} - Datos invalidos.", userSecurityService.getIdSession());
			return "Ocurri\u00F3 un problema al procesar su solicitud, favor de intentarlo de nuevo.";
		}
		try{
			studentService.saveCharge(studentChargeDto);
			LOGGER.info("{} - Termina rest savecharge[{}].", userSecurityService.getIdSession(), studentChargeDto.getStudentRegister());
			return "{\"result\":\"ok\"}";
		}catch(ServiceException se) {
			LOGGER.info("{} - Error:{}.", userSecurityService.getIdSession(), se.getMessage());
			return se.getMessage();
		}
	}

	/**
	 * Responde a peticion rest, para busqueda de clases ofertadas de un grupo
	 * @param idGroup - Id del grupo
	 * @return Lista de objetos, con los resultados
	 */
	@RequestMapping("/groups/offeredclass/find/{idgroup}")
	public Object[][] findOfferedClassesByGroup(@PathVariable(value = "idgroup") long idGroup){
		LOGGER.info("{} - Consumiendo rest api/find/idGroup:{}", userSecurityService.getIdSession(), idGroup);
		return offeredClassService.getUapsByGroup(idGroup);
	}
	
	@RequestMapping("/students/page")
	public Object[][] page(@RequestBody StudentDto studentDto)throws ServiceException {
		StudentDto dto = studentService.findPageable(studentDto);
		Object[][] datos = new Object[dto.getStudentList().size()][7];
		int i = 0;
		for(Student student : dto.getStudentList()) {
			datos[i][0] = student.getRegister();
        	datos[i][1] = student.getName()+" "+student.getLastname()+" "+student.getSecondLastname();
        	datos[i][2] = student.getGroup().getName()+" "+student.getGroup().getTurn()+" ["+student.getGroup().getEducationalProgram().getAbr()+"]";
        	datos[i][3] = student.getGroup().getEducationalProgram().getStudentPlanKey();
        	datos[i][4] = studentService.getStudentByRegister(student.getRegister()).getEmail();
        	datos[i][5] = student.getTotalAutoEval();
        	datos[i][6] = student.getTotalTeacherEval();
        	i++;
		}
		return datos;
	}
	
	@RequestMapping("/students/savechargeall")
	public String saveChargeByAdmin(@RequestBody StudentChargeDto studentChargeDto)throws ServiceException {
		LOGGER.info("{} - Inicia rest savechargeall[{}] - [Datos:{}].", userSecurityService.getIdSession(), studentChargeDto.getStudentRegister(),
				"idGroup:"+studentChargeDto.getIdGroup()+" studentRegister:"+studentChargeDto.getStudentRegister());
		if(studentChargeDto.getIdGroup().trim().equals("")||studentChargeDto.getStudentRegister().isEmpty()) {
			LOGGER.error("{} - Datos invalidos.", userSecurityService.getIdSession());
			return "Ocurri\u00F3 un problema al procesar su solicitud, favor de intentarlo de nuevo.";
		}
		try{
			studentService.saveChargeAdmin(studentChargeDto);
			LOGGER.info("{} - Termina rest savechargeall[{}].", userSecurityService.getIdSession(), studentChargeDto.getStudentRegister());
			return "{\"result\":\"ok\"}";
		}catch(ServiceException se) {
			LOGGER.info("{} - Error:{}.", userSecurityService.getIdSession(), se.getMessage());
			return se.getMessage();
		}
	}
	
	@RequestMapping("/uap/delete/{list}")
	public String deleteByIds(@PathVariable(value = "list") String list)throws ServiceException {
		LOGGER.info("{} - Inicia eliminar uaps multiplw[{}].", userSecurityService.getIdSession(), list);
		try{
			uapService.deleteFromList(list);
			LOGGER.info("{} - Termina eliminar uaps multiple[{}].", userSecurityService.getIdSession(), list);
			return "{\"result\":\"ok\"}";
		}catch(ServiceException se) {
			return se.getMessage();
		}
	}
}
