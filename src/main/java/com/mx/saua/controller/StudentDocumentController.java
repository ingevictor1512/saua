package com.mx.saua.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.mx.saua.constant.ViewConstant;
import com.mx.saua.dto.DocumentDto;
import com.mx.saua.dto.KardexDto;
import com.mx.saua.enums.AuthorityEnum;
import com.mx.saua.service.DocumentService;
import com.mx.saua.service.KardexService;
import com.mx.saua.service.impl.UserSecurityService;

/**
 * Controller que responde al crud de documentos
 * @author Benito Morales
 * @version 1.0
 *
 */
@Controller
@RequestMapping("student/documents")
public class StudentDocumentController {

	/**
	 * Inyección de servicio
	 */
	@Autowired
	private DocumentService documentService;
	
	/**
	 * Inyección de KardexService
	 */
	@Autowired
	private KardexService kardexService;
	
	/**
	 * Inyección de UserSecurityService
	 */
	@Autowired
	private UserSecurityService userSecurityService;
	
	@GetMapping(value =  "/list")
	public String listDocuemnts(Model model) {
		DocumentDto documentDto = new DocumentDto();
		UserDetails userDetails = userSecurityService.getUserCurrentDetails();
		KardexDto kardexDto = kardexService.find(userDetails.getUsername());
		documentDto.setAfterPeriod(kardexDto.isAfterPeriod());
		documentDto.setBeforePeriod(kardexDto.isBeforePeriod());
		model.addAttribute("documentDto", documentService.findByRole(documentDto, AuthorityEnum.STUDENT));
		return ViewConstant.STUDENT_DOCUMENTS_LIST;
	}
}
