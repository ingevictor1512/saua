package com.mx.saua.controller;

import com.mx.saua.constant.ViewConstant;
import com.mx.saua.dto.ApplicantCoffeeShopDto;
import com.mx.saua.dto.ApplicantDto;
import com.mx.saua.dto.UserDto;
import com.mx.saua.service.ApplicantCoffeeShopService;
import com.mx.saua.service.ApplicantService;
import com.mx.saua.service.UserService;
import com.mx.saua.service.impl.UserSecurityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

/**
 * Controller que responde al crud de listado de dias de beca
 * @author Victor Francisco
 * @version 1.0
 *
 */
@Controller
@RequestMapping("student/daysgrant")
public class StudentGrantController {
    /**
     * Inyección de UserSecurityService
     */
    @Autowired
    private UserSecurityService userSecurityService;

    @Autowired
    private ApplicantService applicantService;

    @Autowired
    private UserService userService;

    @Autowired
    private ApplicantCoffeeShopService applicantCoffeeShopService;

    @GetMapping(value =  "/list")
    public String listDaysgrant(Model model) {
        UserDetails userDetails = userSecurityService.getUserCurrentDetails();
        UserDto usr=userService.findByUsername(userDetails.getUsername());
        System.out.println("Valor de getIdUser: "+usr.getIdUser());
        ApplicantDto applicantDto = applicantService.findByUser(usr);
        if(applicantDto == null){
            model.addAttribute("notInitGrant", "Actualmente no tienes el beneficio de la beca alimenticia, si la necesitas comunicate a la direcci\u00F3n escolar.");
        } else {
            model.addAttribute("notInitGrant", "Actualmente tienes el beneficio de la beca alimenticia");
            List<ApplicantCoffeeShopDto> applicantCoffeeShopDto = applicantCoffeeShopService.getByUser(usr);
            model.addAttribute("applicantCoffeeShopDto",applicantCoffeeShopDto);
        }
        //applicantService.
        //model.addAttribute("notInitGrant", "Actualmente no tienes el beneficio de la beca alimenticia, si la necesitas comunicate a la direcci\u00F3n escolar.El periodo de alta de UAP's inicia el " + dto.getInitialPeriod()
          //      + ". Para mayor informaci\u00F3n, comunicate a la direcci\u00F3n escolar.");
        return ViewConstant.STUDENT_DAYSGRANT_LIST;

    }
}
