/**
 * 
 */
package com.mx.saua.controller;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mx.saua.dto.InquestDto;
import com.mx.saua.exception.ServiceException;
import com.mx.saua.service.InquestService;
import com.mx.saua.service.QuestionService;
import com.mx.saua.service.UserAnswerService;
import com.mx.saua.service.impl.UserSecurityService;

/**
 * API para el manejo de preguntas de encuestas
 * @author Benito Morales
 * @version 1.0
 */
@RestController
@RequestMapping("/api/inquest")
public class InquestRestController {
	/**
	 * Logger de la clase
	 */
	private Logger LOGGER = LoggerFactory.getLogger(InquestRestController.class);
	
	/**
	 * Inyección de QuestionService
	 */
	@Autowired
	private QuestionService questionServce;
	
	/**
	 * Inyección de UserSecurityService
	 */
	@Autowired
	private UserSecurityService userSecurityService;
	
	/**
	 * Inyección de InquestService
	 */
	@Autowired
	private InquestService inquestService;
	

	@Autowired
	private UserAnswerService userAnswerService;
	
	/**
	 * Responde a peticion rest, para busqueda preguntas de una categoria en especifico
	 * @param idCategory - Id de la categoria
	 * @return Lista de objetos, con los resultados
	 */
	@RequestMapping("/find/{idCategory}")
	public Object[][] findOfferedClassesByGroup(@PathVariable(value = "idCategory") int idCategory){
		LOGGER.info("{} - Consumiendo rest api/inquest/find/idCategory:{}", userSecurityService.getIdSession(), idCategory);
		return questionServce.findByCategory(idCategory);
	}
	
	@PostMapping("/saveinquest")
	public String saveInquest(@RequestBody InquestDto inquestDto)throws ServiceException {
		LOGGER.info("{} - Consumiendo rest api/saveinquest:{}", userSecurityService.getIdSession());
		LOGGER.info("{} - inquestDto:{}", userSecurityService.getIdSession(), inquestDto.toString());
		try{
			if(inquestDto.isNewRegistry()) {
				inquestService.save(inquestDto);
				LOGGER.info("{} - Return Ok del guardado de encuesta.", userSecurityService.getIdSession());
			}else {
				inquestService.update(inquestDto);
				LOGGER.info("{} - Return Ok de la actualización de encuesta.", userSecurityService.getIdSession());
			}
			return "{\"result\":\"ok\"}";
		}catch(ServiceException se) {
			return se.getMessage();
		}
	}
	
	/**
	 * Rest para descarga de papeleta de materia de alumno
	 * 
	 * @param request
	 * @return Papeleta
	 */
	@GetMapping("/exportans/{idInquest}")
	public ResponseEntity<Resource> papeleta(@PathVariable(value = "idInquest") long idInquest, HttpServletRequest request) {
		LOGGER.info("{} - Inicia peticion para descarga de reporte de encuesta:{}", userSecurityService.getIdSession(), idInquest);
		InquestDto inquestDto = inquestService.findInquestById(idInquest);
		Resource resource = null;
		try {
			resource = userAnswerService.export(idInquest);
		} catch (ServiceException se) {
			return ResponseEntity.status(500).body(null);
		}
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddkkMMss");
		LOGGER.info("{} - Termina peticion para descarga de reporte de encuesta:{}", userSecurityService.getIdSession(), idInquest);
		return ResponseEntity.ok().contentType(MediaType.parseMediaType("application/vnd.ms-excel")).header(
				HttpHeaders.CONTENT_DISPOSITION,
				"attachment; filename=reporte_de_encuesta_" + inquestDto.getName() +"_"+sdf.format(new Date())+".xls")
				.body(resource);
	}
}
