package com.mx.saua.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.mx.saua.constant.ErrorConstant;
import com.mx.saua.constant.MessagesConstant;
import com.mx.saua.constant.ViewConstant;
import com.mx.saua.dto.ProgramDto;
import com.mx.saua.exception.ServiceException;
import com.mx.saua.service.ProgramService;
import com.mx.saua.service.impl.UserSecurityService;

/**
 * Controlador de programas educativos
 * @author Vic
 * @version 1.0
 *
 */
@Controller
@RequestMapping("admin/programs")
public class ProgramController {
	
	/**
	 * Logger de la clase
	 */
	private Logger log = LoggerFactory.getLogger(ProgramController.class);
	
	/**
	 * Inyección de ProgramService
	 */
	@Autowired
	private ProgramService programService;
	
	/**
	 * Iyección de UserSecurityService
	 */
	@Autowired
	private UserSecurityService userSecurityService;
	
	@GetMapping(value = { "", "/", "/list" })
	public String list(Model model) {
		log.info("{} - ProgramController - Generando el listado de programas educativos", userSecurityService.getIdSession());
		ProgramDto programDto = programService.find(new ProgramDto());
		model.addAttribute("programDto",programDto);
		log.info("{} - ProgramController - Listado generando correctamente", userSecurityService.getIdSession());
		return ViewConstant.PROGRAM_LIST;
	}
	
	@GetMapping(value = "/add")
	public String add(Model model) {
		
		model.addAttribute("programDto", programService.prepareDto(new ProgramDto()));
		return ViewConstant.ADD_PROGRAM;
	}
	
	@PostMapping(value = "/save")
	public String save(Model model, ProgramDto form, RedirectAttributes redirAttrs) {
		
		try {
			if(form.isNewRegister()) {
				log.info("{} - ProgramController - Se agregara nuevo registro", userSecurityService.getIdSession());
				programService.save(form);
				log.info("{} - ProgramController - Se agrego nuevo registro", userSecurityService.getIdSession());
				redirAttrs.addFlashAttribute(ViewConstant.MESSAGE, MessagesConstant.ADD_SUCCESS_MESSAGE);
				
			}else {
				log.info("{} - ProgramController - Se actualizara el registro", userSecurityService.getIdSession());
				programService.save(form);
				log.info("{} - ProgramController - Se actualizo el registro", userSecurityService.getIdSession());
				redirAttrs.addFlashAttribute(ViewConstant.MESSAGE, MessagesConstant.EDIT_SUCCESS_MESSAGE);
			}
			model.addAttribute("programDto", programService.prepareDto(form));
			return ViewConstant.REDIRECT_PROGRAM_LIST;
		} catch (ServiceException se) {
			model.addAttribute(ErrorConstant.ERROR_MESSAGE, se.getMessage());
			model.addAttribute("programDto", programService.prepareDto(form));
			return ViewConstant.ADD_PROGRAM;
		}
	}
	
	@PostMapping(value = "/edit")
	public String edit(Model model, @RequestParam(name = "studentPlanKey", required = true) String studentPlanKey) {
		ProgramDto programDto;
		try {
			log.info("{} - ProgramController - Se editara el registro: {}", userSecurityService.getIdSession(),studentPlanKey);
			programDto = programService.getProgramByKey(studentPlanKey);
			programDto.setNewRegister(false);
			model.addAttribute("programDto",programService.prepareDto(programDto));
			return ViewConstant.ADD_PROGRAM;
		} catch (ServiceException se) {
			model.addAttribute(ErrorConstant.ERROR_MESSAGE, se.getMessage());
			return ViewConstant.ADD_PROGRAM;
		}
		
	}
	
	@PostMapping(value = "/delete")
	public String delete(Model model, ProgramDto form, RedirectAttributes redirAttrs) {
		try {
			log.info("{} - ProgramController - Se eliminara el registro: {}", userSecurityService.getIdSession(),form.getStudentPlanKey()+" "+form.getStudentPlanName());
			programService.delete(form);
			redirAttrs.addFlashAttribute(ViewConstant.MESSAGE, MessagesConstant.DELETE_SUCCESS_MESSAGE);
			model.addAttribute("programDto",programService.find(new ProgramDto()));
			log.info("{} - ProgramController - Se elimino el registro correctamente", userSecurityService.getIdSession());
			return ViewConstant.REDIRECT_PROGRAM_LIST;
		} catch (ServiceException se) {
			model.addAttribute(ErrorConstant.ERROR_MESSAGE, se.getMessage());
			return ViewConstant.REDIRECT_PROGRAM_LIST;
		}
	}

}
