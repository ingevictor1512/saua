package com.mx.saua.controller;

import java.text.SimpleDateFormat;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.mx.saua.constant.MessagesConstant;
import com.mx.saua.constant.ViewConstant;
import com.mx.saua.dto.PeriodDto;
import com.mx.saua.exception.ServiceException;
import com.mx.saua.service.PeriodService;

/**
 * Controller que responde a peticiones de periodo
 * @author Benito Morales
 * @version 1.0
 *
 */
@Controller
@RequestMapping("admin/period")
public class PeriodController {
	
	/**
	 * Logger de la clase
	 */
	private Logger log = LoggerFactory.getLogger(PeriodController.class);
	
	/**
	 * Inyección de PeriodService
	 */
	@Autowired
	private PeriodService periodService;
	
	@GetMapping(value = { "", "/", "/list"})
	public String list(Model model) {
		model.addAttribute("periods", periodService.find());
		return ViewConstant.PERIOD_LIST;
	}
	
	@GetMapping(value = "/add")
	public String add(Model model) {
		model.addAttribute("periods", new PeriodDto());
		return ViewConstant.ADD_PERIOD;
	}
	
	@PostMapping(value = "/edit")
	public String edit(Model model, @RequestParam(name = "id", required = true) Long id) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		PeriodDto periodDto = periodService.getById(id);
		periodDto.setInitialDateString(sdf.format(periodDto.getInitialDate()));
		periodDto.setFinalDateString(sdf.format(periodDto.getFinalDate()));
		model.addAttribute("periods", periodDto);
		return ViewConstant.ADD_PERIOD;
	}
	
	@PostMapping(value = "/delete")
	public String delete(Model model, PeriodDto form, RedirectAttributes redirAttrs) {
		try{
			periodService.delete(form.getId());
			redirAttrs.addFlashAttribute(ViewConstant.MESSAGE, MessagesConstant.DELETE_SUCCESS_MESSAGE);
		}catch(ServiceException se) {
			redirAttrs.addFlashAttribute(ViewConstant.MESSAGE, se.getMessage());
		}
		model.addAttribute("periods", periodService.find());
		return ViewConstant.REDIRECT_PERIOD_LIST;
	}
	
	@PostMapping(value = "/save")
	public String save(Model model, PeriodDto form, RedirectAttributes redirAttrs) throws ServiceException {
		if(form.getId() > 0) {
			periodService.save(form);
			redirAttrs.addFlashAttribute(ViewConstant.MESSAGE, MessagesConstant.EDIT_SUCCESS_MESSAGE);
		} else {
			periodService.save(form);
			redirAttrs.addFlashAttribute(ViewConstant.MESSAGE, MessagesConstant.ADD_SUCCESS_MESSAGE);
		}
		return ViewConstant.REDIRECT_PERIOD_LIST;
	}
	
	@PostMapping(value = "/active")
	public String active(Model model, RedirectAttributes redirAttrs, @RequestParam(name = "id", required = true) Long id) {
		log.info("Activando periodo:{}",id);
		try{
			periodService.activePeriod(id);
			redirAttrs.addFlashAttribute(ViewConstant.MESSAGE, "Se activ\u00F3 el periodo correctamente.");
		}catch(ServiceException se) {
			redirAttrs.addFlashAttribute(ViewConstant.MESSAGE, se.getMessage());
		}
		model.addAttribute("periods", periodService.find());
		return ViewConstant.REDIRECT_PERIOD_LIST;
	}
}
