package com.mx.saua.controller;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.commons.codec.digest.DigestUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MaxUploadSizeExceededException;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.mx.saua.constant.ErrorConstant;
import com.mx.saua.constant.MessagesConstant;
import com.mx.saua.constant.ViewConstant;
import com.mx.saua.dto.DocumentDto;
import com.mx.saua.dto.TutorshipDto;
import com.mx.saua.entity.Document;
import com.mx.saua.enums.AuthorityEnum;
import com.mx.saua.enums.ConfigurationEnum;
import com.mx.saua.exception.ServiceException;
import com.mx.saua.repository.ConfigurationRepository;
import com.mx.saua.repository.DocumentRepository;
import com.mx.saua.service.DocumentService;
import com.mx.saua.service.StudentService;
import com.mx.saua.service.TutorshipService;
import com.mx.saua.service.impl.UserSecurityService;


/**
 * Controlador de Coordinador de tutorias
 * @author Vic
 * @version 1.0
 *
 */
@Controller
@ControllerAdvice
@RequestMapping("coordinator/tutorships")
public class CoordinatorController {
	/* Variable de Logger */
	private static final Logger LOGGER = LoggerFactory.getLogger(CoordinatorController.class);
	
	/**
	 * Inyección de Services
	 */
	@Autowired
	private TutorshipService tutorshipService;
	
	@Autowired
	private UserSecurityService userSecurityService;
	
	@Autowired
	private ConfigurationRepository configurationRepository;
	
	@Autowired
	private DocumentRepository documentRepository;
	
	@Autowired
	private DocumentService documentService;
	
	@Autowired
	private StudentService studentService;
	
	private String TUTORSHIP_DTO = "tutorshipDto";
	
	@GetMapping(value = { "", "/", "/list" })
	public String list(Model model) {
		LOGGER.info("Ingresando a tutorias desde el coordinador de tutorias");
		UserDetails userDetails = userSecurityService.getUserCurrentDetails();
		TutorshipDto tutorshipDto = tutorshipService.find(new TutorshipDto());
		tutorshipDto.setAuthority(userDetails.getAuthorities().iterator().next().getAuthority());
		model.addAttribute(TUTORSHIP_DTO,tutorshipDto);
		return ViewConstant.TUTORSHIP_COORDINATOR_LIST;
	}
	
	@GetMapping(value = "/add")
	public String add(Model model) {
		LOGGER.info("Agregando tutor desde coordinador de tutorias");
		TutorshipDto tutorshipDto = tutorshipService.prepareDto(new TutorshipDto());
		UserDetails userDetails = userSecurityService.getUserCurrentDetails();
		tutorshipDto.setAuthority(userDetails.getAuthorities().iterator().next().getAuthority());
		model.addAttribute(TUTORSHIP_DTO, tutorshipDto);
		return ViewConstant.ADD_TUTORSHIP_COORDINATOR;
	}
	
	@PostMapping(value = "/save")
	public String save(Model model, TutorshipDto form, RedirectAttributes redirAttrs) {
		LOGGER.info("Guardando tutoria:{}",form.toString());		
		try {
			if(form.getId() > 0) {
					tutorshipService.save(form);
					redirAttrs.addFlashAttribute(ViewConstant.MESSAGE, MessagesConstant.EDIT_SUCCESS_MESSAGE);
							
				}else {
					tutorshipService.save(form);
					redirAttrs.addFlashAttribute(ViewConstant.MESSAGE, MessagesConstant.ADD_SUCCESS_MESSAGE);
				}
			model.addAttribute(TUTORSHIP_DTO, tutorshipService.prepareDto(form));
			return ViewConstant.REDIRECT_TUTORSHIP_LIST_COORDINATOR;
		} catch (ServiceException se) {
			model.addAttribute(ErrorConstant.ERROR_MESSAGE, se.getMessage());
			model.addAttribute(TUTORSHIP_DTO, tutorshipService.prepareDto(form));
			return ViewConstant.ADD_TUTORSHIP_COORDINATOR;
		}
	}
	
	@PostMapping(value = "/edit")
	public String edit(Model model, @RequestParam(name = "id", required = true) Long id) {

		TutorshipDto tutorshipDto;
		try {
			UserDetails userDetails = userSecurityService.getUserCurrentDetails();
			tutorshipDto = tutorshipService.getTutorshipByKey(id);
			tutorshipDto.setAuthority(userDetails.getAuthorities().iterator().next().getAuthority());
			model.addAttribute(TUTORSHIP_DTO,tutorshipService.prepareDto(tutorshipDto));
			return ViewConstant.ADD_TUTORSHIP_COORDINATOR;
		} catch (ServiceException se) {
			model.addAttribute(ErrorConstant.ERROR_MESSAGE, se.getMessage());
			return ViewConstant.ADD_TUTORSHIP_COORDINATOR;
		}
		
	}
	
	@PostMapping(value = "/delete")
	public String delete(Model model, TutorshipDto form, RedirectAttributes redirAttrs) {
		LOGGER.info("Eliminando la tutoria con id:{}",form.getId());
		try {
			tutorshipService.delete(form);
			redirAttrs.addFlashAttribute(ViewConstant.MESSAGE, MessagesConstant.DELETE_SUCCESS_MESSAGE);
			model.addAttribute(TUTORSHIP_DTO,tutorshipService.find(new TutorshipDto()));
			return ViewConstant.REDIRECT_TUTORSHIP_LIST_COORDINATOR;
		} catch (ServiceException se) {
			model.addAttribute(ErrorConstant.ERROR_MESSAGE, se.getMessage());
			return ViewConstant.REDIRECT_TUTORSHIP_LIST_COORDINATOR;
		}
	}
	
	@PostMapping(value = "/authorizationcharge")
	public String authorizationcharge(Model model, TutorshipDto form, @RequestParam(name = "id", required = true) Long id, RedirectAttributes redirAttrs) {
		LOGGER.info("Si llego al metodo authorizationcharge:{}", form.toString());
		
		try {
			UserDetails userDetails = userSecurityService.getUserCurrentDetails();
			TutorshipDto tutorshipDto = tutorshipService.getTutoredsByTeacher(new TutorshipDto());
			TutorshipDto tutorshipDtoRegister =tutorshipService.getTutorshipByKey(form.getId());
			LOGGER.info("Dto Recuperado:{}", tutorshipDtoRegister.getId());

			tutorshipDto.setAuthority(userDetails.getAuthorities().iterator().next().getAuthority());
			tutorshipService.loadAuthorization(tutorshipDtoRegister.getStudent().getRegister());
			redirAttrs.addFlashAttribute(ViewConstant.MESSAGE, MessagesConstant.AUTHORIZATION_CHARGE_SUCCESS_MESSAGE);	
			model.addAttribute(TUTORSHIP_DTO,tutorshipDto);
			return ViewConstant.REDIRECT_TUTORSHIP_LIST_COORDINATOR;
		} catch (ServiceException se) {
			model.addAttribute(ErrorConstant.ERROR_MESSAGE, se.getMessage());
			return ViewConstant.REDIRECT_TUTORSHIP_LIST_COORDINATOR;
		} 
		
	}
	
	@GetMapping("/export")
	public ResponseEntity<InputStreamResource> exportAllTutors() throws Exception{
		ByteArrayInputStream  stream = tutorshipService.exportAllTutorships();
		
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Disposition", "attachment; filename=Tutorias.xls");
		
		return ResponseEntity.ok().headers(headers).body(new InputStreamResource(stream));
	}

	@GetMapping(value =  "/listdocuments")
	public String listDocuemnts(Model model) {
		model.addAttribute("documentDto", documentService.find(new DocumentDto()));
		return ViewConstant.TUTORSHIP_COORDINATOR_DOCUMENTS_LIST;
	}
	
	@GetMapping(value = "/adddocument")
	public String addDocument(Model model) {
		model.addAttribute("documentDto", new DocumentDto());
		return ViewConstant.TUTORSHIP_COORDINATOR_ADD_DOCUMENT;
	}
	
	@PostMapping("/uploaddocument")
	public String upload(@RequestParam("file") MultipartFile documentFile, Model model, 
			DocumentDto form, RedirectAttributes redirAttrs)  throws ServiceException {
		LOGGER.info("documentDto:{}", form.toString());
		LOGGER.info("MULTIPART-ContentType:{}", documentFile.getContentType());
		LOGGER.info("MULTIPART-OriginalFilename:{}", documentFile.getOriginalFilename());
		if(!documentFile.getOriginalFilename().contains("docx") && !documentFile.getOriginalFilename().contains("doc") && !documentFile.getOriginalFilename().contains("xlsx") 
				&& !documentFile.getOriginalFilename().contains("xls") && !documentFile.getOriginalFilename().contains("csv") && !documentFile.getOriginalFilename().contains("pdf")) {
			redirAttrs.addFlashAttribute(ErrorConstant.ERROR_MESSAGE, MessagesConstant.INVALID_FILE_MESSAGE);
			model.addAttribute("documentDto", form);
			return ViewConstant.TUTORSHIP_COORDINATOR_REDIRECT_ADD_DOCUMENT;
		}
		
		try {
			String[] splitname = documentFile.getOriginalFilename().split("\\.");
			String message = String.format("%s", new java.util.Date());
			String name = DigestUtils.sha1Hex(message)+"."+splitname[1];
			
			Path path = Paths.get(configurationRepository.findByKey(ConfigurationEnum.DOCUMENT_PATH.toString()).getValue(),name);
			byte[] bytes = documentFile.getBytes();
			Files.write(path, bytes);
			
			String roles = "";
			roles = form.isAdminRole()?roles+"|"+AuthorityEnum.ADMIN:roles;
			roles = form.isStudentRole()?roles+"|"+AuthorityEnum.STUDENT:roles;
			roles = form.isCoordinatorRole()?roles+"|"+AuthorityEnum.TUTORING_COORDINATOR:roles;
			roles = form.isTutorRole()?roles+"|"+AuthorityEnum.TUTOR:roles;
			roles = form.isTeacherRole()?roles +"|"+AuthorityEnum.TEACHER:roles;
			
			//Si se escribió el archivo correctamente.
			Document document = new Document();
			document.setDescription(form.getDescription());
			document.setRealName(name);
			document.setViewName(form.getViewName());
			document.setAuthorizationsRole(roles);
			document.setTypeDocument(form.getTypeDocument());
			documentRepository.save(document);
		} catch (IOException e) {
			redirAttrs.addFlashAttribute(ErrorConstant.ERROR_MESSAGE, "Ocurri\u00F3 un error al subir archivo.");
			model.addAttribute("documentDto", form);
			return ViewConstant.TUTORSHIP_COORDINATOR_REDIRECT_LIST_DOCUMENTS;
		}
		
		redirAttrs.addFlashAttribute(ViewConstant.MESSAGE, "Se guard\u00F3 el documento correctamente.");
		return ViewConstant.TUTORSHIP_COORDINATOR_REDIRECT_LIST_DOCUMENTS;
	}
	
	@ExceptionHandler(MaxUploadSizeExceededException.class)
	public String handleFileUploadError(RedirectAttributes redirAttrs){
		redirAttrs.addFlashAttribute(ErrorConstant.ERROR_MESSAGE, "El archivo excede el tama\u00F1o m\u00E1ximo permitido(5MB).");
		return ViewConstant.TUTORSHIP_COORDINATOR_REDIRECT_LIST_DOCUMENTS;
	}
	
	@PostMapping(value = "/deletedocument")
	public String delete(Model model, DocumentDto form, RedirectAttributes redirAttrs) {
		LOGGER.info("{} - DocumentController - Ingresando desde coordinador a eliminar documento:{}", userSecurityService.getIdSession(), form.toString());
		try {
			documentService.delete(form);
			redirAttrs.addFlashAttribute(ViewConstant.MESSAGE, "Se elimin\u00F3 el documento correctamente.");
			model.addAttribute("documentDto",documentService.find(documentService.find(new DocumentDto())));
			LOGGER.info("{} - DocumentController - Documento eliminado correctamente", userSecurityService.getIdSession());
			return ViewConstant.TUTORSHIP_COORDINATOR_REDIRECT_LIST_DOCUMENTS;
		} catch (ServiceException se) {
			redirAttrs.addFlashAttribute(ErrorConstant.ERROR_MESSAGE, se.getMessage());
			model.addAttribute("documentDto",documentService.find(documentService.find(new DocumentDto())));
			return ViewConstant.TUTORSHIP_COORDINATOR_REDIRECT_LIST_DOCUMENTS;
		}
	}
	
	/* Listado de tutorados por tutor
	 * 
	 */
	@GetMapping("/exportByTutor")
	public ResponseEntity<InputStreamResource> exportAllTutoredsByTutor(@RequestParam(name = "id", required = true) Long id) throws Exception{
		LOGGER.info("{} - CoordinatorController - ID profesor recibido:{}", userSecurityService.getIdSession(), id);
		ByteArrayInputStream  stream = tutorshipService.exportTutoredsByIdTutor(id);
		
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Disposition", "attachment; filename=Listado_tutorados_"+id+".xls");
		
		return ResponseEntity.ok().headers(headers).body(new InputStreamResource(stream));
	}
	
	@GetMapping("/exportStudentsWithUaps")
	public ResponseEntity<InputStreamResource> exportStudentsWithUaps() throws Exception{
		ByteArrayInputStream  stream = tutorshipService.exportStudentsWithUaps();
		
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Disposition", "attachment; filename=Listado_Alumnos_con_uaps.xls");
		
		return ResponseEntity.ok().headers(headers).body(new InputStreamResource(stream));
	}
	/* Función para mostrar la carga del alumnoListado de tutorados por tutor
	 * 
	 */
	@PostMapping(value = "/show")
	public String show(Model model, @RequestParam(name = "id", required = true) Long id) {
		LOGGER.info("{} - CoordinatorController - Register recibido: {}", userSecurityService.getIdSession(), id);
		TutorshipDto tutorshipDto;
		try { 
			UserDetails userDetails = userSecurityService.getUserCurrentDetails();
			tutorshipDto = tutorshipService.getTutorshipByKey(id);
			tutorshipDto.setAuthority(userDetails.getAuthorities().iterator().next().getAuthority());

			model.addAttribute("studentChargeDto",studentService.findChargeByRegister(tutorshipDto.getStudent().getRegister())); 
			return ViewConstant.REDIRECT_STUDENTCHARGE_LIST;
		} catch (ServiceException se) {
			model.addAttribute(ErrorConstant.ERROR_MESSAGE, se.getMessage());
			return ViewConstant.REDIRECT_AUTHORIZATION_LIST;
		}
	}
	@PostMapping(value = "/desauthorized")
	public String desauthorized(Model model, @RequestParam(name = "id", required = true) Long id, RedirectAttributes redirAttrs) {
		LOGGER.info("ID recibido:{}",id);
		try {
			tutorshipService.deleteloadAuthorization(id);
			//model.addAttribute("studentChargeDto",studentService.findChargeByRegister(tutorshipDto.getStudent().getRegister()));
			LOGGER.info("{} - CoordinatorController - Se desautorizo la carga academica correctamente.", userSecurityService.getIdSession());
			redirAttrs.addFlashAttribute(ViewConstant.MESSAGE, MessagesConstant.AUTHORIZATION_REMOVE_SUCCESS_MESSAGE);
			return ViewConstant.REDIRECT_TUTORSHIP_LIST_COORDINATOR;
		} catch (ServiceException se) {
			model.addAttribute(ErrorConstant.ERROR_MESSAGE, se.getMessage());
			return ViewConstant.REDIRECT_TUTORSHIP_LIST_COORDINATOR;
		}

	}
}
