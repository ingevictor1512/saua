package com.mx.saua.controller;

import java.io.ByteArrayInputStream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.mx.saua.constant.ErrorConstant;
import com.mx.saua.constant.MessagesConstant;
import com.mx.saua.constant.ViewConstant;
import com.mx.saua.dto.InquestDto;
import com.mx.saua.dto.UserAnswerDto;
import com.mx.saua.exception.ServiceException;
import com.mx.saua.service.Arqsrv;
import com.mx.saua.service.CategoryQuestionService;
import com.mx.saua.service.InquestService;
import com.mx.saua.service.UserAnswerService;

/**
 * Controller que responde a la peticion de encuestas
 * 
 * @author Benito Morales
 *
 */
@Controller
@RequestMapping("admin/inquest")
public class InquestController extends Arqsrv {

	/**
	 * Inyección de InquestService
	 */
	@Autowired
	private InquestService inquestService;

	/**
	 * Inyección de UserAnswerService
	 */
	@Autowired
	private UserAnswerService userAnswerService;

	/**
	 * Inyección de CategoryQuestionService
	 */
	@Autowired
	private CategoryQuestionService categoryQuestionService;

	@GetMapping(value = { "", "/", "/list" })
	public String list(Model model) {
		LOGGER.info("{} - Inicia carga de encuestas.", userSecurityService.getIdSession());
		model.addAttribute("inquestDto", inquestService.find(new InquestDto()));
		LOGGER.info("{} - Termina carga de encuestas.", userSecurityService.getIdSession());
		return ViewConstant.INQUEST_LIST;
	}

	@GetMapping(value = "/add")
	public String add(Model model) {
		model.addAttribute("inquestDto", inquestService.prepare(new InquestDto()));
		return ViewConstant.ADD_INQUEST;
	}

	@PostMapping(value = "/save")
	public String save(Model model, InquestDto form, RedirectAttributes redirAttrs,
			@RequestParam(name = "selection", required = true) String selection) {
		LOGGER.info("INQUEST_DTO:{}", form.toString());
		LOGGER.info("{} - Inicia aguardado de encuesta:{}", userSecurityService.getIdSession(), form.toString());
		// inquestService.save(form, selection);
		if (form.getId() > 0) {
			// Edición de Encuesta
			redirAttrs.addFlashAttribute(ViewConstant.MESSAGE, MessagesConstant.ADD_SUCCESS_MESSAGE);
		} else {
			// Nueva encuesta
			redirAttrs.addFlashAttribute(ViewConstant.MESSAGE, MessagesConstant.EDIT_SUCCESS_MESSAGE);
		}
		redirAttrs.addFlashAttribute(ViewConstant.MESSAGE, "Regs");
		LOGGER.info("{} - Termina guardado de encuesta.", userSecurityService.getIdSession());
		return ViewConstant.REDIRECT_INQUEST_LIST;

	}

	@PostMapping(value = "/edit")
	public String edit(Model model, @RequestParam(name = "id", required = true) long id) {
		LOGGER.info("{} - InquestController - Inicia editar Encuesta[{}],", userSecurityService.getIdSession(), id);
		InquestDto dto = inquestService.findInquestById(id);
		dto.setCategoryQuestionList(categoryQuestionService.find());
		dto.setNewRegistry(false);
		model.addAttribute("inquestDto", dto);
		LOGGER.info("{} - InquestController - Termina editar Encuesta[{}],", userSecurityService.getIdSession(), id);
		return ViewConstant.ADD_INQUEST;
	}

	@PostMapping(value = "/delete")
	public String delete(Model model, InquestDto form, RedirectAttributes redirAttrs) {
		LOGGER.info("{} - InquestController - Inicia eliminar encuesta[{}].", userSecurityService.getIdSession(),
				form.getId());
		try {
			inquestService.delete(form);
			redirAttrs.addFlashAttribute(ViewConstant.MESSAGE, MessagesConstant.DELETE_SUCCESS_MESSAGE);
			model.addAttribute("inquestDto", inquestService.find(new InquestDto()));
			LOGGER.info("{} - InquestController - Termina agregar encuesta[{}].", userSecurityService.getIdSession(),
					form.getId());
			return ViewConstant.REDIRECT_INQUEST_LIST;
		} catch (ServiceException se) {
			redirAttrs.addFlashAttribute(ErrorConstant.ERROR_MESSAGE, se.getMessage());
			model.addAttribute("studentDto", inquestService.find(new InquestDto()));
			return ViewConstant.REDIRECT_INQUEST_LIST;
		}
	}

	@GetMapping(value = "/results")
	public String results(Model model) {
		LOGGER.info("Cargando resultados de encuesta.");
		model.addAttribute("userAnswerDto", new UserAnswerDto());
		return ViewConstant.INQUEST_RESULT;
	}

	/*
	 * Exportar todas las respuestas de la encuesta
	 * 
	 * @param sin parametros
	 */
	@GetMapping("/exportAnswersAll")
	public ResponseEntity<InputStreamResource> exportAnswersAll() throws Exception {
		LOGGER.info("{} - InquestController - Se inicia exportacion de respuestas", userSecurityService.getIdSession());
		ByteArrayInputStream stream = userAnswerService.exportAnswersAll();

		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Disposition", "attachment; filename=Concentrado_Estudio_BackToSchool.xls");

		return ResponseEntity.ok().headers(headers).body(new InputStreamResource(stream));
	}

	/*
	 * Exportar todas las respuestas de la encuesta de regreso a clases
	 * 
	 * @param sin parametros
	 */
	@GetMapping("/exportAnswersBackToSchool")
	public ResponseEntity<InputStreamResource> exportAnswersBackToSchool() throws Exception {
		LOGGER.info("{} - InquestController - Se inicia exportacion de respuestas de regreso a clases",
				userSecurityService.getIdSession());
		ByteArrayInputStream stream = userAnswerService.exportAnswersBackToSchoolP();

		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Disposition", "attachment; filename=Concentrado_Estudio_BackToSchool.xls");

		return ResponseEntity.ok().headers(headers).body(new InputStreamResource(stream));
	}
}
