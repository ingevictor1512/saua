package com.mx.saua.controller;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

import java.security.Principal;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.mx.saua.constant.ViewConstant;
import com.mx.saua.dto.DataAdminDto;
import com.mx.saua.dto.InquestDto;
import com.mx.saua.dto.InquestQuestionDto;
import com.mx.saua.dto.KardexDto;
import com.mx.saua.dto.TeacherDto;
import com.mx.saua.dto.TutorshipDto;
import com.mx.saua.dto.UserDto;
import com.mx.saua.entity.Inquest;
import com.mx.saua.entity.Period;
import com.mx.saua.entity.UserInquest;
import com.mx.saua.exception.ServiceException;
import com.mx.saua.repository.InquestRepository;
import com.mx.saua.repository.PeriodRepository;
import com.mx.saua.repository.UserInquestRepository;
import com.mx.saua.service.GroupService;
import com.mx.saua.service.InquestQuestionService;
import com.mx.saua.service.KardexService;
import com.mx.saua.service.StudentService;
import com.mx.saua.service.TutorshipService;
import com.mx.saua.service.UserAnswerEvalService;
import com.mx.saua.service.UserAnswerService;
import com.mx.saua.service.UserService;
import com.mx.saua.service.impl.UserSecurityService;

/**
 * Controlador principal de la aplicación
 * 
 * @author Benito Morales
 * @version 1.0
 *
 */
@Controller
public class MainController {

	/**
	 * Logger de la clase
	 */
	private Logger LOGGER = LoggerFactory.getLogger(MainController.class);

	/**
	 * Inyección de UserService
	 */
	@Autowired
	private UserService userService;

	/**
	 * Inyección de UserSecurityService
	 */
	@Autowired
	private UserSecurityService userSecurityService;

	/**
	 * Inyección de KardexService
	 */
	@Autowired
	private KardexService kardexService;

	/**
	 * Inyección de GroupService
	 */
	@Autowired
	private GroupService groupService;

	/**
	 * Inyección de UserService
	 */
	@Autowired
	private TutorshipService tutorshipService;

	/**
	 * Inyección de StudentService
	 */
	@Autowired
	private StudentService studentService;
	/**
	 * Inyección de AnswerUser
	 */
	@Autowired
	private UserAnswerService userAnswerService;

	/**
	 * Inyección de UserAnswerEvalService
	 */
	@Autowired
	private UserAnswerEvalService userAnswerEvalService;

	@Autowired
	private InquestQuestionService inquestQuestionService;

	@Autowired
	private UserInquestRepository userInquestRepository;

	@Autowired
	private InquestRepository inquestRepository;

	@Autowired
	private PeriodRepository periodRepository;

	@GetMapping({ "/", "/home" })
	public String index(Model model, Principal principal) {
		UserDto user = userService.findByUsername(principal.getName());
		LOGGER.info("{} - Ingresando a home - USER:{}", userSecurityService.getIdSession(), user.getUsername());
		if (user.isFirstLogin()) {
			model.addAttribute("user", user);
			return ViewConstant.CHANGE_PASSWORD_VIEW;
		} else if (user.isInquest()) {
			model.addAttribute("userDto", user);
			return ViewConstant.INQUEST_LIST_USER;
		} else {
			return redirectDashboardByAuthority(user, model, principal);
		}
	}

	private String redirectDashboardByAuthority(UserDto user, Model model, Principal principal) {
		switch (user.getAuthority()) {
		case "ADMIN":
			LOGGER.info("{} - Redireccionando a dashboard de administrador.", userSecurityService.getIdSession());
			model.addAttribute("dataAdminDto",
					DataAdminDto.builder().surveyAnswered(userAnswerService.totalSurveyAnswered())
							.totalStudents(studentService.totalStudentsLoadAutorizedExist())
							.totalAutosurveyAnswered(userAnswerEvalService.totalAutoSurveyAnswered())
							.totalsurveyDocentAnswered(userAnswerEvalService.totalSurveyDocentAnswered()).build());
			return ViewConstant.ADMIN_DASHBOARD;
		case "STUDENT":
			LOGGER.info("{} - Redireccionando a dashboard de alumno.", userSecurityService.getIdSession());
			UserDetails userDetails = userSecurityService.getUserCurrentDetails();
			KardexDto dto = kardexService.find(userDetails.getUsername());
			model.addAttribute("kardexDto", dto);
			if (dto.isBeforePeriod()) {
				model.addAttribute("notInitMsg", "El periodo de alta de UAP's inicia el " + dto.getInitialPeriod()
						+ ". Para mayor informaci\u00F3n, comunicate a la direcci\u00F3n escolar.");
			}
			if (dto.isAfterPeriod()) {
				model.addAttribute("endMsg", "El periodo de alta de UAP's termin\u00F3 el " + dto.getEndPeriod()
						+ ". Para mayor informaci\u00F3n, comunicate a la direcci\u00F3n escolar.");
			}
			return ViewConstant.STUDENT_DASHBOARD;
		case "TUTORING_COORDINATOR":
			LOGGER.info("{} - Redireccionando a dashboard de coordinador de tutorias.",
					userSecurityService.getIdSession());
			try {
				// model.addAttribute("tutorshipDto",tutorshipService.findAll());
				List<TeacherDto> xDtos = tutorshipService.findAllTutors();

				TutorshipDto tutorshipDto = new TutorshipDto();
				tutorshipDto.setTeachersDto(xDtos);
				model.addAttribute("tutorshipDto", tutorshipDto);

			} catch (ServiceException e) {
				e.printStackTrace();
			}
			return ViewConstant.TUTORING_COORDINATOR_DASHBOARD;
		case "TUTOR":
			LOGGER.info("{} - Redireccionando a dashboard de tutor.", userSecurityService.getIdSession());
			return ViewConstant.TUTOR_DASHBOARD;
		case "TEACHER":
			LOGGER.info("{} - Redireccionando a dashboard de profesor.", userSecurityService.getIdSession());
			return ViewConstant.TUTOR_DASHBOARD;
		case "CASHIER":
			LOGGER.info("{} - Redireccionando a dashboard de cajero.", userSecurityService.getIdSession());
			model.addAttribute("user", user);
			return ViewConstant.CASHIER_DASHBOARD;
		default:
			return ViewConstant.NO_ENCONTRADA;
		}
	}

	// @GetMapping("/register")
	public String register() {
		return ViewConstant.REGISTER;
	}

	@GetMapping("/faq")
	public String faq() {
		return ViewConstant.FAQ;
	}

	@PostMapping(value = "/answerinquest")
	public String answer(Model model, Principal principal, @RequestParam(name = "id", required = true) Long idInquest) {
		InquestQuestionDto inqQDto = new InquestQuestionDto();
		InquestDto inquestDto = new InquestDto();
		inquestDto.setId(idInquest);
		inqQDto.setInquest(inquestDto);
		inqQDto.setIdInquest(idInquest);
		inqQDto.setPage(0);
		InquestQuestionDto inqDto = null;
		Period period = periodRepository.getOneByActivePeriod(true);

		Inquest inquest = inquestRepository.getOne(idInquest);
		UserDetails userDetail = userSecurityService.getUserCurrentDetails();
		// Verifica que la encuesta no se halla iniciado antes
		UserInquest usrinq = userInquestRepository.findByUserAndInquestAndIdPeriod(
				userService.findUserByUsername(userDetail.getUsername()), inquest, period.getId());

		if (usrinq != null) {
			// LOGGER.info("{} - Encontró la encuesta
			// pendiente:{}",userSecurityService.getIdSession(),inqQDto.getId());
			inqDto = inquestQuestionService.findPageable(inqQDto);

			// La encuesta ya se ha inciado antes
			while (questionIsInCurrentQuestionsId(usrinq.getCurrentQuestionId(), inqDto)) {
				// LOGGER.info("Pregunta ya contestada:{}",
				// inqDto.getInquestQuestionList().iterator().next().getQuestion().getText());
				inqDto.setPage(inqDto.getCurrentPage() + 1);
				inqDto.setIdInquest(idInquest);
				inqDto = inquestQuestionService.findPageable(inqDto);
				inqDto.setCurrentPage(inqDto.getPage());
				// LOGGER.info("Pregunta siguiente:{}",
				// inqDto.getInquestQuestionList().iterator().next().getQuestion().getText());
			}
			model.addAttribute("inqDto", inqDto);
			return ViewConstant.GENERAL_INQUEST;
		} else {
			// La encuesta no se ha iniciado
			inqQDto.setPage(inqQDto.getCurrentPage());
		}
		inqDto = inquestQuestionService.findPageable(inqQDto);
		model.addAttribute("inqDto", inqDto);
		return ViewConstant.GENERAL_INQUEST;
	}

	/**
	 * Guarda las respuestas de una pregunta, que se está contestando desde el front
	 * 
	 * @param dto        Datos a guardar
	 * @param model      Model
	 * @param principal  Datos del usuario que está guardando las respuestas
	 * @param redirAttrs Datos de retorno
	 * @return
	 */
	@GetMapping(value = "/saveinquest")
	public String saveInquest(InquestQuestionDto dto, Model model, Principal principal, RedirectAttributes redirAttrs) {
		/*
		 * LOGGER.info("CURRENT_PAGE:{}", dto.getCurrentPage());
		 * LOGGER.info("TOTAL_PAGES:{}", dto.getTotalPages());
		 * LOGGER.info("ID_INQUEST:{}",dto.getIdInquest());
		 * LOGGER.info("ANSW_RATING:{}",dto.getRating());
		 * LOGGER.info("TYPE:{}",dto.getQtype()); LOGGER.info("IDQ:{}",dto.getIdq());
		 */

		try {
			/**
			 * Verificando que la pregunta no se halla contestados antes
			 */
			if (!userAnswerService.verifySaveAnswers(dto)) {
				/**
				 * Guarda las respuestas de la pregunta
				 */
				InquestQuestionDto inqDto = userAnswerService.saveAnswer(dto);
				if (inqDto != null) {
					model.addAttribute("inqDto", inqDto);
				} else {
					redirAttrs.addFlashAttribute(ViewConstant.MESSAGE, "Encuesta contestada correctamente.");
					return ViewConstant.REDIRECT_HOME;
				}
			} else {
				/**
				 * Si ya se contestó, va por la siguiente pregunta
				 */
				if (dto.getTotalPages() < dto.getCurrentPage()) {
					dto.setPage(dto.getCurrentPage() + 1);
					InquestQuestionDto inqDtoo = inquestQuestionService.findPageable(dto);
					model.addAttribute("inqDto", inqDtoo);
				} else {
					return ViewConstant.REDIRECT_HOME;
				}
			}
		} catch (ServiceException e) {
			LOGGER.error(e.getMessage());
		}
		return ViewConstant.GENERAL_INQUEST;
	}

	@GetMapping(value = "/profile")
	public String profile(Model model, Principal principal) {
		// LOGGER.info("{} - MainController - Inicia carga de perfil.",
		// userSecurityService.getIdSession());
		UserDto userDto = userService.findByUsername(principal.getName());
		userDto.translateAutorities();
		if (userDto.getStudentDto() != null) {
			try {
				if (!userDto.getStudentDto().getName().equals("") && !userDto.getStudentDto().getLastname().equals("")
						&& !userDto.getStudentDto().getSecondLastname().equals("")) {
					userDto.getStudentDto()
							.setGroupDto(groupService.getGroupById(userDto.getStudentDto().getGroupDto().getId()));
					TutorshipDto tutor = tutorshipService.findByStudent(userDto.getStudentDto());
					userDto.setTutor(tutor.getTeacher());
				} else {
					userDto.setStudentDto(null);
				}
			} catch (ServiceException e) {
				LOGGER.error("Ocurrio un error al cargar el perfil:{}", e.getMessage());
			}
		}
		model.addAttribute("userDto", userDto);
		// LOGGER.info("{} - MainController - Termina carga de perfil.",
		// userSecurityService.getIdSession());
		return ViewConstant.PROFILE;
	}

	@GetMapping("/changepassword")
	public String userEdit(Model model, Principal principal) {
		LOGGER.info("{} - Inicia cambio de contraseña.", userSecurityService.getIdSession());
		UserDto userDto = userService.findByUsername(principal.getName());
		model.addAttribute("user", userDto);
		LOGGER.info("{} - Termina cambio de contraseña[EXITOSO].", userSecurityService.getIdSession());
		return ViewConstant.CHANGE_PASSWORD_VIEW;
	}

	@RequestMapping(value = "/editpassword", method = { GET, POST })
	public String passwordEdit(Model model, Principal principal) {
		LOGGER.info("{} - Inicia editar contraseña, desde el perfil.", userSecurityService.getIdSession());
		UserDto userDto = userService.findByUsername(principal.getName());
		userDto.setHash("EDIT");
		model.addAttribute("user", userDto);
		return ViewConstant.CHANGE_PASSWORD_VIEW;
	}

	@GetMapping(value = "/logoutcontroller")
	public String logout(HttpServletRequest request) {
		HttpSession httpSession = request.getSession();
		httpSession.invalidate();
		LOGGER.info("{} - Logout.", userSecurityService.getIdSession());
		return "redirect:/login";
	}

	private boolean questionIsInCurrentQuestionsId(String currentQuestionId, InquestQuestionDto dto) {
		// LOGGER.info("Buscando pregunta {} en las ya contestadas [{}].",
		// dto.getInquestQuestionList().iterator().next().getQuestion().getId(),
		// currentQuestionId);
		String[] idsQ = currentQuestionId.split("\\,");
		for (String idq : idsQ) {
			if (String.valueOf(dto.getInquestQuestionList().iterator().next().getQuestion().getId()).equals(idq)) {
				// LOGGER.info("Pregunta ya contestada, return true.");
				return true;
			}
		}
		// LOGGER.info("Pregunta no contestada, return false.");
		return false;
	}
}
