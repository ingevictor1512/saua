/**
 * 
 */
package com.mx.saua.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.mx.saua.constant.ErrorConstant;
import com.mx.saua.constant.MessagesConstant;
import com.mx.saua.constant.ViewConstant;
import com.mx.saua.dto.ProgramDto;
import com.mx.saua.dto.StudentChargeDto;
import com.mx.saua.dto.UserDto;
import com.mx.saua.entity.Configuration;
import com.mx.saua.entity.Group;
import com.mx.saua.entity.LoadAuthorization;
import com.mx.saua.entity.Tutorship;
import com.mx.saua.enums.ConfigurationEnum;
import com.mx.saua.exception.ServiceException;
import com.mx.saua.repository.ConfigurationRepository;
import com.mx.saua.repository.GroupRepository;
import com.mx.saua.repository.LoadAuthorizationRepository;
import com.mx.saua.repository.TutorshipRepository;
import com.mx.saua.service.ProgramService;
import com.mx.saua.service.StudentService;
import com.mx.saua.service.UserService;
import com.mx.saua.service.impl.UserSecurityService;

/**
 * Controller para el manejo de la carga academica
 * 
 * @author Benito Morales
 * @version 1.0
 *
 */

@Controller
@RequestMapping("/student/academiccharge")
public class StudentChargeController {

	/**
	 * Logger de la clase
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(StudentChargeController.class);

	/**
	 * Inyeccion de userSecurityService
	 */
	@Autowired
	private UserSecurityService userSecurityService;

	/**
	 * Inyeccion de userService
	 */
	@Autowired
	private UserService userService;

	/**
	 * Inyeccion de programService
	 */
	@Autowired
	private ProgramService programService;

	/**
	 * Inyeccion de studentService
	 */
	@Autowired
	private StudentService studentService;

	/**
	 * Inyeccion de configurationRepository
	 */
	@Autowired
	private ConfigurationRepository configurationRepository;

	/**
	 * Inyeccion de loadAuthorizationRepository
	 */
	@Autowired
	private LoadAuthorizationRepository loadAuthorizationRepository;

	/**
	 * Inyeccion de tutor
	 */
	@Autowired
	private TutorshipRepository tutorshipRepository;

	@Autowired
	private GroupRepository groupRepository;

	@GetMapping(value = { "", "/", "/list" })
	public String list(Model model, RedirectAttributes redirAttrs) {
		LOGGER.info("{} - StudentChargeController - Inicia carga academica.", userSecurityService.getIdSession());
		Configuration registryDay = configurationRepository.findByKey(ConfigurationEnum.REGISTRY_DAY.toString());
		//LOGGER.info("REGISTRY_DAY:{}",registryDay.getValue());
		Configuration registrySubjectsInit = configurationRepository
				.findByKey(ConfigurationEnum.REGISTRY_SUBJECTS_INIT.toString());
		Configuration registrySubjectsEnd = configurationRepository
				.findByKey(ConfigurationEnum.REGISTRY_SUBJECTS_END.toString());
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy kk:mm");

		Calendar initialCalendar = null;
		Calendar endCalendar = null;
		try {
			initialCalendar = Calendar.getInstance();
			Date initialDate = sdf.parse(registrySubjectsInit.getValue());
			initialCalendar.setTime(initialDate);
			initialCalendar.add(Calendar.DATE, Integer.valueOf(registryDay.getValue()));

			endCalendar = Calendar.getInstance();
			Date endDate = sdf.parse(registrySubjectsEnd.getValue());
			endCalendar.setTime(endDate);
			endCalendar.add(Calendar.DATE, Integer.valueOf(registryDay.getValue()));
		} catch (ParseException e) {
			LOGGER.error("{} - ERROR:{}", userSecurityService.getIdSession(), e.getMessage());
			redirAttrs.addFlashAttribute(ErrorConstant.ERROR_MESSAGE,
					"Ocurri\u00F3 un error al la procesar petici\u00F3n.");
			return ViewConstant.REDIRECT_HOME;
		}
		Date now = new Date();
		if (now.before(initialCalendar.getTime())) {
			redirAttrs.addFlashAttribute(ViewConstant.ERROR_MESSAGE, MessagesConstant.NOT_INIT_PROCESS_MESSAGE);
			return ViewConstant.REDIRECT_HOME;
		}
		if (now.after(endCalendar.getTime())) {
			redirAttrs.addFlashAttribute(ViewConstant.MESSAGE, MessagesConstant.FINISH_PROCESS_MESSAGE);
			return ViewConstant.REDIRECT_HOME;
		}

		try {
			UserDetails userDetails = userSecurityService.getUserCurrentDetails();
			UserDto user = userService.findByUsername(userDetails.getUsername());

			// Revisar si la carga academica ya se encuentra autorizada
			LoadAuthorization auth = null;
			auth = loadAuthorizationRepository.findByStudentAndPeriod(user.getStudentDto().getRegister(), true);

			LOGGER.info("{} - Cargando carga acad\u00E9mica de {}.", userSecurityService.getIdSession(), user.getStudentDto().getRegister());
			StudentChargeDto studentChargeDto = studentService.findChargeByRegister(user.getStudentDto().getRegister());
			
			studentChargeDto.setPrograms(programService.find());
			studentChargeDto.setStudent(studentService.getStudentByRegister(user.getStudentDto().getRegister()));
			studentChargeDto.setAuthorization((auth != null) ? true : false);
			List<Tutorship> tutor = tutorshipRepository.findByStudentRegister(user.getStudentDto().getRegister());
			studentChargeDto.setStudentRegister(user.getStudentDto().getRegister());
			if (tutor != null && tutor.size() > 0) {
				studentChargeDto.setTutorName(
						tutor.get(0).getTeacher().getName() + " " + tutor.get(0).getTeacher().getLastname() + " "
								+ tutor.get(0).getTeacher().getSecondLastname());
			} else {
				studentChargeDto.setTutorName("");
			}
			model.addAttribute("studentChargeDto", studentChargeDto);
			Group group = groupRepository.getOne(studentChargeDto.getStudent().getGroupDto().getId());
			studentChargeDto.getStudent().getGroupDto()
					.setTurn(group.getEducationalProgram().getAbr() + " - " + group.getTurn());
			if(!studentChargeDto.isEvaluationsComplete()) {
				model.addAttribute("studentChargeDto", studentChargeDto);
				model.addAttribute(ErrorConstant.ERROR_MESSAGE, "No has terminado de contestar las evaluaciones.");
			}
			LOGGER.info("{} - StudentController - Termina carga academica.", userSecurityService.getIdSession());
			return ViewConstant.STUDENT_ADD_CHARGE;
		} catch (ServiceException se) {
			model.addAttribute(ErrorConstant.ERROR_MESSAGE, se.getMessage());
			return ViewConstant.STUDENT_ADD_CHARGE;
		}
	}

	@PostMapping(value = "/delete")
	public String deleteCharge(Model model, StudentChargeDto form, @RequestParam(name = "id", required = true) Long id,
			@RequestParam(name = "studentRegister", required = true) String register, RedirectAttributes redirAttrs) {
		LOGGER.info("{} - StudentChargeController - Inicia eliminar carga academica[{} - {}].",
				userSecurityService.getIdSession(), register, id);
		Configuration registryDay = configurationRepository.findByKey(ConfigurationEnum.REGISTRY_DAY.toString());
		Configuration registrySubjectsInit = configurationRepository
				.findByKey(ConfigurationEnum.REGISTRY_SUBJECTS_INIT.toString());
		Configuration registrySubjectsEnd = configurationRepository
				.findByKey(ConfigurationEnum.REGISTRY_SUBJECTS_END.toString());
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy kk:mm");

		Calendar initialCalendar = null;
		Calendar endCalendar = null;
		try {
			initialCalendar = Calendar.getInstance();
			Date initialDate = sdf.parse(registrySubjectsInit.getValue());
			initialCalendar.setTime(initialDate);
			initialCalendar.add(Calendar.DATE, Integer.valueOf(registryDay.getValue()));

			endCalendar = Calendar.getInstance();
			Date endDate = sdf.parse(registrySubjectsEnd.getValue());
			endCalendar.setTime(endDate);
			endCalendar.add(Calendar.DATE, Integer.valueOf(registryDay.getValue()));
		} catch (ParseException e) {
			LOGGER.error("{} - ERROR:{}", userSecurityService.getIdSession(), e.getMessage());
			redirAttrs.addFlashAttribute(ErrorConstant.ERROR_MESSAGE,
					"Ocurri\u00F3 un error al la procesar petici\u00F3n.");
			return ViewConstant.REDIRECT_STUDENT_CHARGE;
		}
		Date now = new Date();
		if (now.before(initialCalendar.getTime())) {
			redirAttrs.addFlashAttribute(ViewConstant.ERROR_MESSAGE, MessagesConstant.NOT_INIT_PROCESS_MESSAGE);
			return ViewConstant.REDIRECT_HOME;
		}
		if (now.after(endCalendar.getTime())) {
			redirAttrs.addFlashAttribute(ViewConstant.MESSAGE, MessagesConstant.FINISH_PROCESS_MESSAGE);
			return ViewConstant.REDIRECT_HOME;
		}

		try {
			studentService.delete(id);
			List<ProgramDto> programs = programService.find();
			form.setPrograms(programs);
			form.setStudent(studentService.getStudentByRegister(register));
			redirAttrs.addFlashAttribute(ViewConstant.MESSAGE, "Se elimin\u00F3 la clase correctamente.");
			model.addAttribute("studentChargeDto", studentService.findCharge(form));
			LOGGER.info("{} - StudentChargeController - Termina eliminar carga academica[{} - {}].",
					userSecurityService.getIdSession(), register, id);
			return ViewConstant.REDIRECT_STUDENT_CHARGE;
		} catch (ServiceException se) {
			LOGGER.error("{} - Error: {}", userSecurityService.getIdSession(), se.getMessage());
			redirAttrs.addFlashAttribute(ErrorConstant.ERROR_MESSAGE, se.getMessage());
			model.addAttribute("studentChargeDto", studentService.findCharge(form));
			return ViewConstant.REDIRECT_STUDENT_CHARGE;
		}

	}
}
