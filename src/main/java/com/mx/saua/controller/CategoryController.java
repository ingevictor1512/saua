package com.mx.saua.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.mx.saua.constant.MessagesConstant;
import com.mx.saua.constant.ViewConstant;
import com.mx.saua.dto.CategoryDto;
import com.mx.saua.exception.ServiceException;
import com.mx.saua.service.CategoryService;
import com.mx.saua.service.impl.UserSecurityService;

/**
 * Controlador para la administracion de las categorias
 * @author Benito Morales
 * @version 1.0
 *
 */
@Controller
@RequestMapping("admin/category")
public class CategoryController {
	
	/**
	 * Logger de la clase
	 */
	private Logger LOGGER = LoggerFactory.getLogger(CategoryController.class);
	
	/**
	 * Inyección de CategoryService
	 */
	@Autowired
	private CategoryService categoryService;
	
	/**
	 * Inyección de UserSecurityService
	 */
	@Autowired
	private UserSecurityService userSecurityService;
	
	@GetMapping(value = { "", "/", "/list"})
	public String list(Model model) {
		LOGGER.info("{} - Inicia generar el listado de las categrorias.", userSecurityService.getIdSession());
		model.addAttribute("categories", categoryService.find());
		LOGGER.info("{} - Termina generar listado de categorias.", userSecurityService.getIdSession());
		return ViewConstant.CATEGORY_LIST;
	}
	
	@GetMapping(value = "/add")
	public String add(Model model) {
		LOGGER.info("{} - Inicia agregar categoria.", userSecurityService.getIdSession());
		model.addAttribute("categories", new CategoryDto());
		LOGGER.info("{} - Termina agregar categoria.", userSecurityService.getIdSession());
		return ViewConstant.ADD_CATEGORY;
	}
	
	@PostMapping(value = "/edit")
	public String edit(Model model, @RequestParam(name = "id", required = true) Long id) {
		LOGGER.info("{} - Inicia editar categoria:{}", userSecurityService.getIdSession(),id);
		CategoryDto categoryDto = categoryService.getById(id);
		model.addAttribute("categories", categoryDto);
		LOGGER.info("{} - Termina editar categoria.", userSecurityService.getIdSession());
		return ViewConstant.ADD_CATEGORY;
	}
	
	@PostMapping(value = "/delete")
	public String delete(Model model, CategoryDto form, RedirectAttributes redirAttrs) {
		LOGGER.info("{} - Inicia eliminar categoria:{}", userSecurityService.getIdSession(), form.getId());
		categoryService.delete(form.getId());
		redirAttrs.addFlashAttribute(ViewConstant.MESSAGE, MessagesConstant.DELETE_SUCCESS_MESSAGE);
		LOGGER.info("{} - Termina eliminar categoria.", userSecurityService.getIdSession());
		return ViewConstant.REDIRECT_CATEGORY_LIST;
	
	}
	
	@PostMapping(value = "/save")
	public String save(Model model, CategoryDto form, RedirectAttributes redirAttrs) throws ServiceException {

		LOGGER.info("Inicia guardar categoria:{}", form.toString());
		if(form.getId() > 0) {
			LOGGER.info("{} - Ingresando a modificar la Categoria: {}", userSecurityService.getIdSession(), form.toString());
			categoryService.save(form);
			LOGGER.info("{} - Categoria modificada correctamente", userSecurityService.getIdSession());
			redirAttrs.addFlashAttribute(ViewConstant.MESSAGE, MessagesConstant.EDIT_SUCCESS_MESSAGE);
		} else {
			LOGGER.info("{} - Ingresando a crear la Categoria: {}", userSecurityService.getIdSession(), form.toString());
			categoryService.save(form);
			LOGGER.info("{} - Categoria creada correctamente", userSecurityService.getIdSession());
			redirAttrs.addFlashAttribute(ViewConstant.MESSAGE, MessagesConstant.ADD_SUCCESS_MESSAGE);
		}
		LOGGER.info("{} - Termina guardar categoria.", userSecurityService.getIdSession());
		return ViewConstant.REDIRECT_CATEGORY_LIST;
	}

}
