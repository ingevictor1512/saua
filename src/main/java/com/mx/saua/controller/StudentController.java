package com.mx.saua.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.mx.saua.constant.ErrorConstant;
import com.mx.saua.constant.MessagesConstant;
import com.mx.saua.constant.ViewConstant;
import com.mx.saua.dto.ProgramDto;
import com.mx.saua.dto.StudentChargeDto;
import com.mx.saua.dto.StudentDto;
import com.mx.saua.exception.ServiceException;
import com.mx.saua.service.Arqsrv;
import com.mx.saua.service.ProgramService;
import com.mx.saua.service.StudentService;

/**
 * Controller que responde a petciones del crud de alumnos
 * @author Benito Morales
 * @version 1.0
 *
 */
@Controller
@RequestMapping("admin/students")
public class StudentController extends Arqsrv {

	/**
	 * Inyeccion de studentservice
	 */
	@Autowired
	private StudentService studentService;
	
	/**
	 * Inyeccion de programService
	 */
	@Autowired
	private ProgramService programService;

	/*@GetMapping(value = { "", "/", "/list" })
	public String list(Model model) {
		LOGGER.info("{} - StudentController - Inicia carga de alumnos.", userSecurityService.getIdSession());
		StudentDto dto = studentService.find(new StudentDto());
		model.addAttribute("studentDto",dto);
		LOGGER.info("{} - StudentController - Termina carga de alumnos.", userSecurityService.getIdSession());
		return ViewConstant.STUDENT_LIST;
	}*/
	
	@RequestMapping(value = { "", "/", "/list"}, method = { RequestMethod.POST, RequestMethod.GET })
	public String listForPagination(Model model, StudentDto studentDto) {
		LOGGER.info("{} - StudentController - Inicia la carga de alumnos.", userSecurityService.getIdSession());
		StudentDto dto = studentService.findPageable(studentDto);
		List<String> combo = new ArrayList<String>();
		combo.add("Matricula");
		combo.add("Nombre");
		combo.add("Grupo");
		combo.add("E-mail");
		combo.add("Tipo de ingreso");
		dto.setComboSearch(combo);
		model.addAttribute("studentDto",dto);
		LOGGER.info("{} - StudentController - Termina la carga de alumnos.", userSecurityService.getIdSession());
		return ViewConstant.STUDENT_LIST_PAGE;
	}
	
	/*@RequestMapping(value = { "/list-pagination" }, method = { POST, GET })
	public String listForPagination(Model model) {
		LOGGER.info("{} - StudentController - Inicia carga de alumnos.", userSecurityService.getIdSession());
		StudentDto dto = studentService.find(new StudentDto());
		model.addAttribute("studentDto",dto);
		LOGGER.info("{} - StudentController - Termina carga de alumnos.", userSecurityService.getIdSession());
		return ViewConstant.STUDENT_LIST;
	}*/
	
	@GetMapping(value = "/add")
	public String add(Model model) {
		LOGGER.info("{} - StudentController - Inicia agregar alumno.", userSecurityService.getIdSession());
		model.addAttribute("studentDto", studentService.prepareDto(new StudentDto(), "ADD"));
		LOGGER.info("{} - StudentController - Termina agregar alumno.", userSecurityService.getIdSession());
		return ViewConstant.ADD_STUDENT;
	}
	
	@PostMapping(value = "/save")
	public String save(Model model, StudentDto form, RedirectAttributes redirAttrs) {
		LOGGER.info("{} - StudentController - Inicia guardar alumno[{}],", userSecurityService.getIdSession(), form.getRegister());
		try {
			if(form.isNewRegister()) {
				studentService.save(form);
				redirAttrs.addFlashAttribute(ViewConstant.MESSAGE, "Se agreg\u00F3 el alumno correctamente");
			}else {
				studentService.save(form);
				redirAttrs.addFlashAttribute(ViewConstant.MESSAGE, "Se modific\u00F3 el alumno correctamente");
			}
			//model.addAttribute("studentDto", studentService.prepareDto(form,"SAVE"));
			//model.addAttribute("studentDto", studentService.find(new StudentDto()));
			
			
			StudentDto dto = studentService.findPageable(new StudentDto());
			List<String> combo = new ArrayList();
			combo.add("Matricula");
			combo.add("Nombre");
			combo.add("Grupo");
			combo.add("E-mail");
			dto.setComboSearch(combo);
			model.addAttribute("studentDto",dto);
			
			LOGGER.info("{} - StudentController - Termina guardar alumno[{}],", userSecurityService.getIdSession(), form.getRegister());
			return ViewConstant.REDIRECT_STUDENT_LIST;
		} catch (ServiceException se) {
			model.addAttribute(ErrorConstant.ERROR_MESSAGE, se.getMessage());
			//model.addAttribute("studentDto", studentService.prepareDto(form, "SAVE"));
			model.addAttribute("studentDto", studentService.find(new StudentDto()));
			return ViewConstant.ADD_STUDENT;
		}
	}
	
	@PostMapping(value = "/edit")
	public String edit(Model model, @RequestParam(name = "register", required = true) String register) {
		StudentDto studentDto;
		LOGGER.info("{} - StudentController - Inicia editar alumno[{}],", userSecurityService.getIdSession(), register);
		try {
			studentDto = studentService.getStudentByRegister(register);
			studentDto.setNewRegister(false);
			model.addAttribute("studentDto", studentService.prepareDto(studentDto, "EDIT"));
			LOGGER.info("{} - StudentController - Termina editar alumno[{}],", userSecurityService.getIdSession(), register);
			return ViewConstant.ADD_STUDENT;
		} catch (ServiceException se) {
			model.addAttribute(ErrorConstant.ERROR_MESSAGE, se.getMessage());
			return ViewConstant.ADD_STUDENT;
		}
	}
	
	@PostMapping(value = "/delete")
	public String delete(Model model, StudentDto form, RedirectAttributes redirAttrs) {
		LOGGER.info("{} - StudentController - Inicia eliminar alumno[{}].", userSecurityService.getIdSession(), form.getRegister());
		try {
			studentService.delete(form);
			redirAttrs.addFlashAttribute(ViewConstant.MESSAGE, MessagesConstant.DELETE_SUCCESS_MESSAGE);
			model.addAttribute("studentDto",studentService.find(new StudentDto()));
			LOGGER.info("{} - StudentController - Termina agregar alumno[{}].", userSecurityService.getIdSession(), form.getRegister());
			return ViewConstant.REDIRECT_STUDENT_LIST;
		} catch (ServiceException se) {
			redirAttrs.addFlashAttribute(ErrorConstant.ERROR_MESSAGE, se.getMessage());
			model.addAttribute("studentDto",studentService.find(new StudentDto()));
			return ViewConstant.REDIRECT_STUDENT_LIST;
		}
	}
	
	@PostMapping(value = "/charge")
	public String addCharge(Model model, @RequestParam(name = "register", required = true) String register) {
		LOGGER.info("{} - StudentController - Inicia carga de materias.", userSecurityService.getIdSession());
		try {
			StudentChargeDto studentChargeDto = studentService.findChargeByRegister(register);
			
			List<ProgramDto> programs = programService.find();
			studentChargeDto.setPrograms(programs);
			studentChargeDto.setStudent(studentService.getStudentByRegister(register));
			model.addAttribute("studentChargeDto", studentChargeDto);
			LOGGER.info("{} - StudentController - Termina carga de materias.", userSecurityService.getIdSession());
			return ViewConstant.ADD_CHARGE;
		} catch (ServiceException se) {
			model.addAttribute(ErrorConstant.ERROR_MESSAGE, se.getMessage());
			return ViewConstant.ADD_CHARGE;
		}
	}
	
	@PostMapping(value = "/deletecharge")
	public String deleteCharge(Model model, StudentChargeDto form, @RequestParam(name = "id", required = true) Long id, 
			@RequestParam(name = "studentRegister", required = true) String register, RedirectAttributes redirAttrs) {
		//LOGGER.info("{} - StudentController - Inicia eliminar carga academica[{}].", userSecurityService.getIdSession(), id);
		try {
			studentService.delete(id);
			List<ProgramDto> programs = programService.find();
			form.setPrograms(programs);
			form.setStudent(studentService.getStudentByRegister(register));
			redirAttrs.addFlashAttribute(ViewConstant.MESSAGE, MessagesConstant.DELETE_SUCCESS_MESSAGE);
			model.addAttribute("studentChargeDto", studentService.findCharge(form));
			LOGGER.info("{} - StudentController - Termina eliminar carga academica[{}].", userSecurityService.getIdSession(), id);
			return ViewConstant.REDIRECT_ADD_CHARGE;
			//return "redirect:/admin/students/charge";
		} catch (ServiceException se) {
			model.addAttribute(ErrorConstant.ERROR_MESSAGE, se.getMessage());
			return ViewConstant.REDIRECT_ADD_CHARGE;
		}
	}
}
