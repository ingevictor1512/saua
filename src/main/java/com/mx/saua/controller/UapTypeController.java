package com.mx.saua.controller;

import org.hibernate.service.spi.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.mx.saua.constant.ErrorConstant;
import com.mx.saua.constant.MessagesConstant;
import com.mx.saua.constant.ViewConstant;
import com.mx.saua.dto.UapTypeDto;
import com.mx.saua.service.UapTypeService;

/**
 * Controlador que responde a peticiones del CRUD Type
 * @author Victor Francisco
 * @version 1.0
 *
 */
@Controller
@RequestMapping("admin/types")
public class UapTypeController {
	
	/**
	 * Inyección de UapTypeService
	 */
	@Autowired
	private UapTypeService uapTypeService;
	
	@GetMapping(value = { "", "/", "/list"})
	public String list(Model model) {
		model.addAttribute("types",uapTypeService.find());	
		return ViewConstant.TYPES_LIST;
	}
	
	@GetMapping(value = "/add")
	public String add(Model model) {
		model.addAttribute("types", new UapTypeDto());
		return ViewConstant.ADD_TYPES;
	}
	
	@PostMapping(value = "/edit")
	public String edit(Model model, @RequestParam(name = "id", required = true) Long id) {
		UapTypeDto uapTypeDto = uapTypeService.getById(id);
		model.addAttribute("types", uapTypeDto);
		return ViewConstant.ADD_TYPES;
	}
	
	@PostMapping(value = "/delete")
	public String delete(Model model, UapTypeDto form, RedirectAttributes redirAttrs) {
		try {
			uapTypeService.delete(form.getId());
			redirAttrs.addFlashAttribute(ViewConstant.MESSAGE, MessagesConstant.DELETE_SUCCESS_MESSAGE);
		} catch (ServiceException se) {
			redirAttrs.addFlashAttribute(ErrorConstant.ERROR_MESSAGE, se.getMessage());
		}
		return ViewConstant.REDIRECT_TYPES_LIST;
	}
	
	@PostMapping(value = "/save")
	public String save(Model model, UapTypeDto form, RedirectAttributes redirAttrs) {
		try {
			if(form.getId() > 0) {
				uapTypeService.save(form);
				redirAttrs.addFlashAttribute(ViewConstant.MESSAGE, MessagesConstant.EDIT_SUCCESS_MESSAGE);
			}else {
				uapTypeService.save(form);
				redirAttrs.addFlashAttribute(ViewConstant.MESSAGE, MessagesConstant.ADD_SUCCESS_MESSAGE);
			}
			return ViewConstant.REDIRECT_TYPES_LIST;
		} catch (ServiceException se) {
			model.addAttribute(ErrorConstant.ERROR_MESSAGE, se.getMessage());
			model.addAttribute("types", form);
			return ViewConstant.ADD_TYPES;
		}
	}

}
