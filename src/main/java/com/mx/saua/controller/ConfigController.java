package com.mx.saua.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.mx.saua.constant.MessagesConstant;
import com.mx.saua.constant.ViewConstant;
import com.mx.saua.dto.ConfigurationDto;
import com.mx.saua.exception.ServiceException;
import com.mx.saua.service.ConfigService;
import com.mx.saua.service.impl.UserSecurityService;

/**
 * Controller que responde a peticiones para configuración de parametros
 * @author Benito Morales
 * @version 1.0
 *
 */
@Controller
@RequestMapping("admin/config")
public class ConfigController {

	/**
	 * Inyección de ConfigService
	 */
	@Autowired
	private ConfigService configService;
	
	/**
	 * Logger de la clase
	 */
	private Logger LOGGER = LoggerFactory.getLogger(ConfigController.class);
	
	/**
	 * Inyección de UserSecurityService
	 */
	@Autowired
	private UserSecurityService userSecurityService;
	
	@GetMapping(value = { "", "/", "/list"})
	public String list(Model model) {
		LOGGER.info("{} - ConfigController - Inicia generar el listado de las categrorias.", userSecurityService.getIdSession());
		model.addAttribute("configs", configService.find());
		return ViewConstant.CONFIG_LIST;
	}
	
	@PostMapping(value = "/edit")
	public String edit(Model model, @RequestParam(name = "id", required = true) Long id) {
		model.addAttribute("configurationDto", configService.getById(id));
		return ViewConstant.EDIT_CONFIG;
	}
	
	@PostMapping(value = "/save")
	public String save(Model model, ConfigurationDto form, RedirectAttributes redirAttrs) throws ServiceException {
		if(form.getIdConfiguration() > 0) {
			configService.save(form);
			redirAttrs.addFlashAttribute(ViewConstant.MESSAGE, MessagesConstant.EDIT_SUCCESS_MESSAGE);
		}
		return ViewConstant.REDIRECT_CONFIG_LIST;
	}
}
