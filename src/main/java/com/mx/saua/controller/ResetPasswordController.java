package com.mx.saua.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.mx.saua.constant.ErrorConstant;
import com.mx.saua.constant.MessagesConstant;
import com.mx.saua.constant.ViewConstant;
import com.mx.saua.dto.UserDto;
import com.mx.saua.exception.ServiceException;
import com.mx.saua.service.UserService;
import com.mx.saua.service.impl.UserSecurityService;

/**
 * Controller que responde a la petición de reseteo de contraseñas
 * @author Benito Morales
 * @version 1.0
 *
 */
@Controller
public class ResetPasswordController {

	/**
	 * Logger de la clase
	 */
	Logger LOGGER = LoggerFactory.getLogger(ResetPasswordController.class);

	@Autowired
	private UserService userService;
	
	/**
	 * Inyección de UserSecurityService
	 */
	@Autowired
	private UserSecurityService userSecurityService;
	
	@GetMapping(value = "/resetp")
	public String resetRedirect(Model model) {
		LOGGER.info("Inicia reseteo de contraseña desde el login.");
		model.addAttribute("userDto", new UserDto());
		return ViewConstant.RESET_PASSWORD_VIEW;
	}

	@PostMapping(value = "/resetPassword")
	public String resetPassword(Model model, UserDto form) {
		if (form.getHash()!=null && form.getHash().equals("EDIT")) {
			LOGGER.info("{} - Inicia resetPassword.", userSecurityService.getIdSession());
			try {
				userService.updatePassword(form);
				model.addAttribute(ViewConstant.MESSAGE, "Contraseña actualizada correctamente.");
				model.addAttribute("userDto", form);
				LOGGER.info("{} - Termina resetPassword[OK].", userSecurityService.getIdSession());
				return ViewConstant.RESET_PASSWORD_VIEW;
			} catch (ServiceException e) {
				model.addAttribute(ViewConstant.ERROR_MESSAGE, e.getMessage());
				model.addAttribute("userDto", form);
				LOGGER.info("{} - Termina resetPassword[NOK].", userSecurityService.getIdSession());
				return ViewConstant.RESET_PASSWORD_VIEW;
			}
		} else {
			try {
				LOGGER.info("Inicia resetPassword Mail.");
				userService.resetPasswordForMail(form);
				LOGGER.info("Termina resetPassword Mail.");
			} catch (ServiceException se) {
				LOGGER.error(se.getMessage());
				model.addAttribute("errorMessage", se.getMessage());
				model.addAttribute("userDto", form);
				return ViewConstant.RESET_PASSWORD_VIEW;
			}
			model.addAttribute("message", MessagesConstant.RESET_PASSWORD_SUCCESS_MESSAGE);
			form.setReset(true);
			model.addAttribute("userDto", form);
			return ViewConstant.RESET_PASSWORD_VIEW;
		}
	}

	@GetMapping(value = "/chkhash")
	public String resetChkPassword(Model model, @RequestParam("h") String hash, RedirectAttributes redirAttrs) {
		try {
			LOGGER.info("Inicia verificaci-on de hash[{}].", hash);
			userService.compareHash(hash);
			model.addAttribute("h", hash);
			LOGGER.info("Termina verificación de hash[{}] - Verificación exitosa.", hash);
			model.addAttribute("userDto", new UserDto());
			return ViewConstant.NEW_PASSWORD_VIEW;
		} catch (ServiceException se) {
			LOGGER.error("Error al validar hash:{}", se.getMessage());
			redirAttrs.addFlashAttribute(ViewConstant.ERROR_MESSAGE, se.getMessage());
			return ViewConstant.REDIRECT_LOGIN;
		}
	}

	@PostMapping(value = "/newpassword")
	public String changePassw(Model model, @ModelAttribute("user") UserDto form, RedirectAttributes redirAttrs) {
		try {
			LOGGER.info("{} -  Inicia cambio de contraseña obligatorio.",userSecurityService.getIdSession());
			userService.updatePassword(form);
			model.addAttribute(ViewConstant.MESSAGE, MessagesConstant.CHANGE_PASSWORD_SUCCESS_MESSAGE);
			LOGGER.info("{} -  Termina cambio de contraseña obligatorio.",userSecurityService.getIdSession());
			return "redirect:/logoutcontroller";
		} catch (ServiceException se) {
			redirAttrs.addFlashAttribute(ViewConstant.ERROR_MESSAGE, se.getMessage());
			LOGGER.error("{} -  Error:{}",userSecurityService.getIdSession(), se.getMessage());
			return ViewConstant.REDIRECT_HOME;
		}
	}

	@PostMapping(value = "/changepassword")
	public String resetNewPassword(Model model, UserDto form, RedirectAttributes redirAttrs) {
		try {
			userService.updatePasswordForReset(form);
			model.addAttribute(ViewConstant.MESSAGE, MessagesConstant.CHANGE_PASSWORD_SUCCESS_MESSAGE);
			form.setReset(true);
			LOGGER.info("Termina reseteo de contraseña.");
			return ViewConstant.NEW_PASSWORD_VIEW;
		} catch (ServiceException se) {
			redirAttrs.addFlashAttribute(ErrorConstant.ERROR_MESSAGE, se.getMessage());
			return ViewConstant.REDIRECT_RESET_PASSWORD+form.getHash();
		}
	}
}
