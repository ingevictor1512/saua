package com.mx.saua.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.mx.saua.constant.ViewConstant;
import com.mx.saua.dto.ProgramDto;
import com.mx.saua.dto.StudentDto;
import com.mx.saua.service.ProgramService;
import com.mx.saua.service.StudentService;
import com.mx.saua.service.impl.UserSecurityService;

/**
 * Controller que responde a las peticiones para generar
 * papeletas de alumnos, desde la session de administrador
 * @author Benito Morales
 * @version 1.0
 *
 */
@Controller
@RequestMapping("admin/papeletas")
public class StudentPapeletaController {
	
	/**
	 * Logger de la clase
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(StudentController.class);
	
	/**
	 * Inyeccion de studentservice
	 */
	@Autowired
	private StudentService studentService;
	/**
	 * Inyección de ProgramService
	 */
	@Autowired
	private ProgramService programService;
	
	/**
	 * Inyección de UserSecurityService
	 */
	@Autowired
	private UserSecurityService userSecurityService;
	
	@GetMapping(value = "/generate")
	public String print(Model model) {
		LOGGER.info("{} - StudentController - Inicia carga de alumnos - papeletas.", userSecurityService.getIdSession());
		StudentDto dto = studentService.find(new StudentDto());
		model.addAttribute("studentDto",dto);
		LOGGER.info("{} - StudentController - Termina carga de alumnos - papeletas.", userSecurityService.getIdSession());
		return ViewConstant.STUDENT_LIST_PAPELETAS;
	}
	
	@GetMapping(value = "/generatePapeletas")
	public String printPapeletas(Model model) {
		LOGGER.info("{} - ProgramController - Generando el listado de programas educativos", userSecurityService.getIdSession());
		ProgramDto programDto = programService.find(new ProgramDto());
		model.addAttribute("programDto",programDto);
		LOGGER.info("{} - ProgramController - Listado generando correctamente", userSecurityService.getIdSession());
		return ViewConstant.STUDENT_LIST_PAPELETASMASIVO;
	}
}
