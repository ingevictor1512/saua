package com.mx.saua.controller;

import java.io.ByteArrayInputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.mx.saua.constant.ErrorConstant;
import com.mx.saua.constant.MessagesConstant;
import com.mx.saua.constant.ViewConstant;
import com.mx.saua.dto.StudentChargeDto;
import com.mx.saua.dto.TutorshipDto;
import com.mx.saua.exception.ServiceException;
import com.mx.saua.service.KardexService;
import com.mx.saua.service.StudentService;
import com.mx.saua.service.TutorshipService;
import com.mx.saua.service.impl.UserSecurityService;

/**
 * Controlador de tutorias
 * 
 * @author Vic
 * @version 1.0
 *
 */
@Controller
@RequestMapping("tutor/tutoreds")
public class TutoredController {

	/**
	 * Logger de la clase
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(TutoredController.class);

	/**
	 * Inyección de Services
	 */
	@Autowired
	private TutorshipService tutorshipService;

	/**
	 * Inyección de UserSecurityService
	 */
	@Autowired
	private UserSecurityService userSecurityService;

	/**
	 * Inyección de KardexService
	 */
	@Autowired
	private KardexService kardexService;

	/**
	 * Inyección de StudentService
	 */
	@Autowired
	private StudentService studentService;

	@GetMapping(value = { "", "/", "/list" })
	public String list(Model model) {
		UserDetails userDetails = userSecurityService.getUserCurrentDetails();
		TutorshipDto tutorshipDto = tutorshipService.getTutoredsByTeacher(new TutorshipDto());
		tutorshipDto.setAuthority(userDetails.getAuthorities().iterator().next().getAuthority());
		model.addAttribute("tutorshipDto", tutorshipDto);
		return ViewConstant.TUTORED_LIST;
	}

	@PostMapping(value = "/kardex")
	public String kardex(Model model, @RequestParam(name = "id", required = true) Long id) {
		TutorshipDto tutorshipDto;
		try {
			UserDetails userDetails = userSecurityService.getUserCurrentDetails();
			tutorshipDto = tutorshipService.getTutorshipByKey(id);
			tutorshipDto.setAuthority(userDetails.getAuthorities().iterator().next().getAuthority());
			// List<Kardex> kardex =
			// kardexRepository.findByRegister(tutorshipDto.getStudent().getRegister());
			// LOGGER.info("##################### Kardex:{}",kardex.size());

			model.addAttribute("kardexDto", kardexService.find(tutorshipDto.getStudent().getRegister()));
			return ViewConstant.REDIRECT_KARDEX_TUTORED;
		} catch (ServiceException se) {
			model.addAttribute(ErrorConstant.ERROR_MESSAGE, se.getMessage());
			return ViewConstant.REDIRECT_KARDEX_TUTORED;
		}

	}

	@PostMapping(value = "/load")
	public String load(Model model, @RequestParam(name = "id", required = true) Long id) {
		TutorshipDto tutorshipDto;
		try {
			UserDetails userDetails = userSecurityService.getUserCurrentDetails();
			tutorshipDto = tutorshipService.getTutorshipByKey(id);
			tutorshipDto.setAuthority(userDetails.getAuthorities().iterator().next().getAuthority());
			model.addAttribute("studentChargeDto",
					studentService.findChargeByRegister(tutorshipDto.getStudent().getRegister()));
			return ViewConstant.REDIRECT_LOAD_TUTORED;
		} catch (ServiceException se) {
			model.addAttribute(ErrorConstant.ERROR_MESSAGE, se.getMessage());
			return ViewConstant.REDIRECT_LOAD_TUTORED;
		}

	}

	@PostMapping(value = "/authorized")
	public String authorized(Model model, @RequestParam(name = "id", required = true) Long id) {
		TutorshipDto tutorshipDto;
		try {
			UserDetails userDetails = userSecurityService.getUserCurrentDetails();
			tutorshipDto = tutorshipService.getTutorshipByKey(id);
			tutorshipDto.setAuthority(userDetails.getAuthorities().iterator().next().getAuthority());
			// List<Kardex> kardex =
			// kardexRepository.findByRegister(tutorshipDto.getStudent().getRegister());
			// LOGGER.info("##################### Kardex:{}",kardex.size());

			model.addAttribute("studentChargeDto",
					studentService.findChargeByRegister(tutorshipDto.getStudent().getRegister()));
			return ViewConstant.REDIRECT_AUTHORIZATION_LIST;
		} catch (ServiceException se) {
			model.addAttribute(ErrorConstant.ERROR_MESSAGE, se.getMessage());
			return ViewConstant.REDIRECT_AUTHORIZATION_LIST;
		}

	}

	@PostMapping(value = "/save")
	public String save(Model model, TutorshipDto form, RedirectAttributes redirAttrs) {

		try {
			if (form.getId() > 0) {
				tutorshipService.saveTutored(form);
				redirAttrs.addFlashAttribute(ViewConstant.MESSAGE, MessagesConstant.DELETE_SUCCESS_MESSAGE);

			} else {
				tutorshipService.saveTutored(form);
				redirAttrs.addFlashAttribute(ViewConstant.MESSAGE, MessagesConstant.ADD_SUCCESS_MESSAGE);
			}
			model.addAttribute("tutorshipDto", tutorshipService.prepareDto(form));
			return ViewConstant.REDIRECT_TUTORED_LIST;
		} catch (ServiceException se) {
			model.addAttribute(ErrorConstant.ERROR_MESSAGE, se.getMessage());
			model.addAttribute("tutorshipDto", tutorshipService.prepareDto(form));
			return ViewConstant.ADD_TUTORED;
		}
	}

	@PostMapping(value = "/edit")
	public String edit(Model model, @RequestParam(name = "id", required = true) Long id) {
		TutorshipDto tutorshipDto;
		try {
			UserDetails userDetails = userSecurityService.getUserCurrentDetails();
			tutorshipDto = tutorshipService.getTutorshipByKey(id);
			tutorshipDto.setAuthority(userDetails.getAuthorities().iterator().next().getAuthority());
			model.addAttribute("tutorshipDto", tutorshipService.prepareDto(tutorshipDto));
			return ViewConstant.ADD_TUTORED;
		} catch (ServiceException se) {
			model.addAttribute(ErrorConstant.ERROR_MESSAGE, se.getMessage());
			return ViewConstant.ADD_TUTORED;
		}

	}

	@PostMapping(value = "/deletecharge")
	public String deleteCharge(Model model, StudentChargeDto form, @RequestParam(name = "id", required = true) Long id,
			@RequestParam(name = "studentRegister", required = true) String register, RedirectAttributes redirAttrs) {
		try {
			studentService.delete(id);
			form.setStudent(studentService.getStudentByRegister(register));
			redirAttrs.addFlashAttribute(ViewConstant.MESSAGE, MessagesConstant.DELETE_SUCCESS_MESSAGE);
			model.addAttribute("studentChargeDto", studentService.findChargeByRegister(register));
			return ViewConstant.REDIRECT_LOAD_TUTORED;
		} catch (ServiceException se) {
			model.addAttribute(ErrorConstant.ERROR_MESSAGE, se.getMessage());
			return ViewConstant.REDIRECT_LOAD_TUTORED;
		}
	}

	@PostMapping(value = "/authorizationcharge")
	public String authorizationcharge(Model model, StudentChargeDto form,
			@RequestParam(name = "id", required = true) Long id,
			@RequestParam(name = "studentRegister", required = true) String register, RedirectAttributes redirAttrs) {
		LOGGER.info("Si llego al metodo authorizationcharge:{}", id);

		try {
			UserDetails userDetails = userSecurityService.getUserCurrentDetails();
			TutorshipDto tutorshipDto = tutorshipService.getTutoredsByTeacher(new TutorshipDto());
			tutorshipDto.setAuthority(userDetails.getAuthorities().iterator().next().getAuthority());
			tutorshipService.loadAuthorization(register);
			redirAttrs.addFlashAttribute(ViewConstant.MESSAGE, MessagesConstant.AUTHORIZATION_CHARGE_SUCCESS_MESSAGE);
			model.addAttribute("tutorshipDto", tutorshipDto);
			return ViewConstant.REDIRECT_TUTORED_LIST;
		} catch (ServiceException se) {
			model.addAttribute(ErrorConstant.ERROR_MESSAGE, se.getMessage());
			return ViewConstant.REDIRECT_TUTORED_LIST;
		}
	}

	@GetMapping("/exportTutoreds")
	public ResponseEntity<InputStreamResource> exportTutoreds() throws Exception {
		LOGGER.info("{} - TutoredController - Generando excel de tutorados asignados a un tutor.",
				userSecurityService.getIdSession());
		UserDetails userDetails = userSecurityService.getUserCurrentDetails();
		ByteArrayInputStream stream = tutorshipService.exportTutoredsByTutors(userDetails.getUsername());

		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Disposition", "attachment; filename=Tutorados.xls");
		LOGGER.info("{} - TutoredController - Se genero excel de tutorados asignados a un tutor.",
				userSecurityService.getIdSession());
		return ResponseEntity.ok().headers(headers).body(new InputStreamResource(stream));
	}

	/**
	 * Desautorizar una carga academica
	 * 
	 * @param model
	 * @param id
	 * @return vista tutorados
	 */
	@PostMapping(value = "/desauthorized")
	public String desauthorized(Model model, @RequestParam(name = "id", required = true) Long id,
			RedirectAttributes redirAttrs) {
		LOGGER.info("ID recibido:{}", id);
		try {
			tutorshipService.deleteloadAuthorization(id);
			redirAttrs.addFlashAttribute(ViewConstant.MESSAGE, MessagesConstant.AUTHORIZATION_REMOVE_SUCCESS_MESSAGE);
		} catch (ServiceException se) {
			model.addAttribute(ErrorConstant.ERROR_MESSAGE, se.getMessage());
			return ViewConstant.REDIRECT_TUTORED_LIST;
		}
		return ViewConstant.REDIRECT_TUTORED_LIST;
	}

}
