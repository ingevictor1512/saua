package com.mx.saua.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.mx.saua.constant.ViewConstant;
import com.mx.saua.dto.DocumentDto;
import com.mx.saua.enums.AuthorityEnum;
import com.mx.saua.service.DocumentService;

/**
 * Controller que responde a la vista de documentos desde la sesion del tutor
 * @author Benito Morales
 * @version 1.0
 *
 */
@Controller
@RequestMapping("tutor/documents")
public class TutorDocumentController {

	/**
	 * Inyección del servicio
	 */
	@Autowired
	private DocumentService documentService;
	
	@GetMapping(value =  "/list")
	public String listDocuemnts(Model model) {
		model.addAttribute("documentDto", documentService.findByRole(new DocumentDto(), AuthorityEnum.TUTOR));
		return ViewConstant.TUTOR_DOCUMENT_LIST;
	}
}
