package com.mx.saua.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.mx.saua.constant.MessagesConstant;
import com.mx.saua.constant.ViewConstant;
import com.mx.saua.dto.IncomeDto;
import com.mx.saua.exception.ServiceException;
import com.mx.saua.service.IncomeService;
import com.mx.saua.service.impl.UserSecurityService;

/**
 * Controller que responde a peticiones de tipos de ingreso
 * @author Victor Francisco
 * @version 1.0
 *
 */
@Controller
@RequestMapping("admin/income")
public class IncomeController {
	
	/**
	 * Logger de la clase
	 */
	private Logger LOGGER = LoggerFactory.getLogger(IncomeController.class);
	
	/**
	 * Inyección de IncomeService
	 */
	@Autowired
	private IncomeService incomeService;
	
	/**
	 * Iyección de UserSecurityService
	 */
	@Autowired
	private UserSecurityService userSecurityService;
	
	
	@GetMapping(value = { "", "/", "/list"})
	public String list(Model model) {
		LOGGER.info("{} - IncomeController - Generando el listado de tipos de ingresos", userSecurityService.getIdSession());
		model.addAttribute("incomes", incomeService.find());
		LOGGER.info("{} - IncomeController - Listado generando correctamente", userSecurityService.getIdSession());
		return ViewConstant.INCOME_LIST;
	}
	
	@GetMapping(value = "/add")
	public String add(Model model) {
		LOGGER.info("{} - IncomeController - Ingresando a agregar tipo de ingreso", userSecurityService.getIdSession());
		model.addAttribute("incomes", new IncomeDto());
		return ViewConstant.ADD_INCOME;
	}
	
	@PostMapping(value = "/edit")
	public String edit(Model model, @RequestParam(name = "id", required = true) Long id) {
		LOGGER.info("{} - IncomeController - Ingresando a editar tipo de ingreso:{}", userSecurityService.getIdSession(), id);
		IncomeDto incomeDto = incomeService.getById(id);
		model.addAttribute("incomes", incomeDto);
		return ViewConstant.ADD_INCOME;
	}
	
	@PostMapping(value = "/delete")
	public String delete(Model model, IncomeDto form, RedirectAttributes redirAttrs) {
		LOGGER.info("{} - IncomeController - Ingresando a eliminar tipo de ingreso:{}", userSecurityService.getIdSession(), form.toString());
		incomeService.delete(form.getId());
		LOGGER.info("{} - IncomeController - Registro eliminado correctamente.", userSecurityService.getIdSession());
		redirAttrs.addFlashAttribute(ViewConstant.MESSAGE, MessagesConstant.DELETE_SUCCESS_MESSAGE);
		return ViewConstant.REDIRECT_INCOME_LIST;
	
	}
	
	@PostMapping(value = "/save")
	public String save(Model model, IncomeDto form, RedirectAttributes redirAttrs) throws ServiceException {
		if(form.getId() > 0) {
			incomeService.save(form);
			LOGGER.info("{} - IncomeController - Tipo de ingreso modificado correctamente", userSecurityService.getIdSession());
			redirAttrs.addFlashAttribute(ViewConstant.MESSAGE, MessagesConstant.EDIT_SUCCESS_MESSAGE);
		} else {
			incomeService.save(form);
			LOGGER.info("{} - IncomeController - Tipo de ingreso agregado correctamente", userSecurityService.getIdSession());
			redirAttrs.addFlashAttribute(ViewConstant.MESSAGE, MessagesConstant.ADD_SUCCESS_MESSAGE);
		}
		return ViewConstant.REDIRECT_INCOME_LIST;
	}

}
