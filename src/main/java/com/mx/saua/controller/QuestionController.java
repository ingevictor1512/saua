/**
 * 
 */
package com.mx.saua.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.mx.saua.constant.ErrorConstant;
import com.mx.saua.constant.MessagesConstant;
import com.mx.saua.constant.ViewConstant;
import com.mx.saua.dto.QuestionDto;
import com.mx.saua.exception.ServiceException;
import com.mx.saua.service.Arqsrv;
import com.mx.saua.service.QuestionService;

/**
 * Controller que responde a las peticiones del crud de preguntas
 * @author Benito Morales
 *
 */
@Controller
@RequestMapping("admin/question")
public class QuestionController extends Arqsrv{
	
	@Autowired
	private QuestionService questionService;

	@GetMapping(value = { "", "/", "/list" })
	public String asks(Model model) {
		LOGGER.info("{} - Inicia generacion del listado de las preguntas.", userSecurityService.getIdSession());
		model.addAttribute("askDto", questionService.findAll(new QuestionDto()));
		LOGGER.info("{} - Termina generar listado de preguntas.", userSecurityService.getIdSession());
		return ViewConstant.QUESTION_LIST;
	}
	
	@GetMapping(value = "/add")
	public String add(Model model) {
		model.addAttribute("askDto", new QuestionDto());
		return ViewConstant.ADD_QUESTION;
	}
	
	@PostMapping(value = "/edit")
	public String edit(Model model, @RequestParam(name = "id", required = true) Long id) {
		model.addAttribute("askDto", new QuestionDto());
		return ViewConstant.ADD_QUESTION;
	}
	
	@PostMapping(value = "/delete")
	public String delete(Model model, QuestionDto form, RedirectAttributes redirAttrs) {
		LOGGER.info("{} - QuestionController - Ingresando a eliminar pregunta:{}", userSecurityService.getIdSession(), form.toString());
		try {
			questionService.delete(form);
			redirAttrs.addFlashAttribute(ViewConstant.MESSAGE, MessagesConstant.DELETE_SUCCESS_MESSAGE);
			model.addAttribute("askDto",questionService.findAll(new QuestionDto()));
			LOGGER.info("{} - QuestionController - Pregunta eliminado correctamente", userSecurityService.getIdSession());
			return ViewConstant.REDIRECT_QUESTION_LIST;
		} catch (ServiceException se) {
			model.addAttribute(ErrorConstant.ERROR_MESSAGE, se.getMessage());
			model.addAttribute("askDto",questionService.findAll(new QuestionDto()));
			return ViewConstant.QUESTION_LIST;
		}
	}
}
