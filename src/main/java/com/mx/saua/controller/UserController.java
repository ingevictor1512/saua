package com.mx.saua.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.mx.saua.constant.ErrorConstant;
import com.mx.saua.constant.MessagesConstant;
import com.mx.saua.constant.ViewConstant;
import com.mx.saua.dto.UserDto;
import com.mx.saua.entity.User;
import com.mx.saua.exception.ServiceException;
import com.mx.saua.repository.UserRepository;
import com.mx.saua.service.UserService;
import com.mx.saua.service.impl.UserSecurityService;

/**
 * Controller que responde a peticiones del crud usuarios
 * @author Benito Morales
 * @version 1.0
 *
 */
@Controller
@RequestMapping("admin/users")
public class UserController {
	
	/**
	 * Logger de la clase
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);
	
	/**
	 * Inyección de UserSecurityService
	 */
	@Autowired
	private UserSecurityService userSecurityService;
	
	@Autowired
	private UserRepository userRepository;
	
	/**
	 * Inyeccion de passwordEncoder
	 */
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	/**
	 * Inyección de UserService
	 */
	@Autowired
	private UserService userService;
	
	@GetMapping(value = { "", "/", "/list"})
	public String list(Model model) {
		model.addAttribute("user",userService.find(new UserDto()));
		return ViewConstant.USER_LIST;
	}
	
	@GetMapping(value = "/add")
	public String add(Model model) {
		model.addAttribute("user", new UserDto());
		return ViewConstant.ADD_USER;
	}
	
	@PostMapping(value = "/save")
	public String save(Model model, UserDto form, RedirectAttributes redirAttrs) {
		try {
			if(form.getIdUser() > 0) {
				userService.save(form);
				redirAttrs.addFlashAttribute(ViewConstant.MESSAGE, MessagesConstant.EDIT_SUCCESS_MESSAGE);
			}else {
				userService.save(form);
				redirAttrs.addFlashAttribute(ViewConstant.MESSAGE, MessagesConstant.ADD_SUCCESS_MESSAGE);
			}
			return ViewConstant.REDIRECT_USERS_LIST;
		} catch (ServiceException se) {
			model.addAttribute(ErrorConstant.ERROR_MESSAGE, se.getMessage());
			model.addAttribute("user", form);
			return ViewConstant.ADD_USER;
		}
	}
	
	@PostMapping(value = "/edit")
	public String edit(Model model, @RequestParam(name = "idUser", required = true) Long idUser) {
		UserDto userDto = userService.getById(idUser);
		model.addAttribute("user", userDto);
		return ViewConstant.ADD_USER;
	}
	
	@PostMapping(value = "/delete")
	public String delete(Model model, UserDto form, RedirectAttributes redirAttrs) {
		try {
			userService.delete(form.getIdUser());
			redirAttrs.addFlashAttribute(ViewConstant.MESSAGE, MessagesConstant.DELETE_SUCCESS_MESSAGE);
			model.addAttribute("username", userSecurityService.getUserCurrentDetails().getUsername());
		} catch (ServiceException se) {
			redirAttrs.addFlashAttribute(ErrorConstant.ERROR_MESSAGE, se.getMessage());
		}
		return ViewConstant.REDIRECT_USERS_LIST;
	}
	
	@PostMapping(value = "/resetpassword")
	public String resetPassword(Model model, @RequestParam(name = "idUser", required = true) Long idUser) {
		LOGGER.info("{} - StudentController - Inicia reseteo de contraseña[{}],", userSecurityService.getIdSession(), idUser);
		model.addAttribute("user", userService.getById(idUser));
		LOGGER.info("{} - StudentController - Termina reseteo de contraseña[{}],", userSecurityService.getIdSession(), idUser);
		return ViewConstant.RESET_PASSWORD;
	}
	
	@PostMapping(value = "/savepassword")
	public String savePassword(Model model, UserDto form, RedirectAttributes redirAttrs) {
		User user = userRepository.getOne(form.getIdUser());
		user.setPassword(passwordEncoder.encode(form.getVerifyPassword()));
		userRepository.save(user);
		redirAttrs.addFlashAttribute(ViewConstant.MESSAGE, MessagesConstant.ADD_SUCCESS_MESSAGE);
		return ViewConstant.REDIRECT_USERS_LIST;
	}
}
