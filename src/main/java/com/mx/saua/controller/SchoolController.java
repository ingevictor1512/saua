package com.mx.saua.controller;

import org.hibernate.service.spi.ServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.mx.saua.constant.ErrorConstant;
import com.mx.saua.constant.MessagesConstant;
import com.mx.saua.constant.ViewConstant;
import com.mx.saua.dto.SchoolDto;
import com.mx.saua.service.SchoolService;
import com.mx.saua.service.impl.UserSecurityService;

/**
 * Controller que responde a peticiones de crud escuelas
 * @author Victor Francisco
 * @version 1.0
 *
 */
@Controller
@RequestMapping("admin/school")
public class SchoolController {
	
	/**
	 * Logger de la clse
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(SchoolController.class);
	
	/**
	 * inyección del servicio
	 */
	@Autowired
	private SchoolService schoolService;
	
	/**
	 * Inyección de UserSecurityService
	 */
	@Autowired
	private UserSecurityService userSecurityService;
	
	@GetMapping(value = { "", "/", "/list"})
	public String list(Model model) {
		LOGGER.info("{} - SchoolController - Listando las escuelas registradas.", userSecurityService.getIdSession());
		model.addAttribute("school",schoolService.find());	
		LOGGER.info("{} - SchoolController - Listado de escuelas generado correctamente.", userSecurityService.getIdSession());
		return ViewConstant.SCHOOL_LIST;
	}
	
	@GetMapping(value = "/add")
	public String add(Model model) {
		model.addAttribute("school", new SchoolDto());
		return ViewConstant.ADD_SCHOOL;
	}
	
	@PostMapping(value = "/edit")
	public String edit(Model model, @RequestParam(name = "id", required = true) Long id) {
		SchoolDto schoolDto = schoolService.getById(id);
		model.addAttribute("school", schoolDto);
		return ViewConstant.ADD_SCHOOL;
	}
	
	@PostMapping(value = "/delete")
	public String delete(Model model, SchoolDto form, RedirectAttributes redirAttrs) {
		try {
			schoolService.delete(form.getId());
			redirAttrs.addFlashAttribute(ViewConstant.MESSAGE, MessagesConstant.DELETE_SUCCESS_MESSAGE);
		} catch (ServiceException se) {
			redirAttrs.addFlashAttribute(ErrorConstant.ERROR_MESSAGE, se.getMessage());
		}
		return ViewConstant.REDIRECT_SCHOOL_LIST;
	}
	
	@PostMapping(value = "/save")
	public String save(Model model, SchoolDto form, RedirectAttributes redirAttrs) {
		try {
			if(form.getId() > 0) {
				schoolService.save(form);
				redirAttrs.addFlashAttribute(ViewConstant.MESSAGE, MessagesConstant.EDIT_SUCCESS_MESSAGE);
			}else {
				schoolService.save(form);
				redirAttrs.addFlashAttribute(ViewConstant.MESSAGE, MessagesConstant.ADD_SUCCESS_MESSAGE);
			}
			return ViewConstant.REDIRECT_SCHOOL_LIST;
		} catch (ServiceException se) {
			model.addAttribute(ErrorConstant.ERROR_MESSAGE, se.getMessage());
			model.addAttribute("school", form);
			return ViewConstant.ADD_SCHOOL;
		}
	}

}
