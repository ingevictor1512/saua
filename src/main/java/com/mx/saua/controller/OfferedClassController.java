package com.mx.saua.controller;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.mx.saua.constant.ErrorConstant;
import com.mx.saua.constant.MessagesConstant;
import com.mx.saua.constant.ViewConstant;
import com.mx.saua.dto.OfferedClassDto;
import com.mx.saua.exception.ServiceException;
import com.mx.saua.service.OfferedClassService;
import com.mx.saua.service.impl.UserSecurityService;

/**
 * Controller que responde a las peticiones de las clases ofertadas
 * @author Benito Morales
 *
 */
@Controller
@RequestMapping("admin/offeredclass")
public class OfferedClassController {

	/**
	 * Logger de la clase
	 */
	private Logger LOGGER = LoggerFactory.getLogger(OfferedClassController.class);
	
	/**
	 * Inyección de OfferedClassService
	 */
	@Autowired
	private OfferedClassService offeredClassService;
	
	/**
	 * Inyección de UserSecurityService
	 */
	@Autowired
	private UserSecurityService userSecurityService;
	
	/*@RequestMapping(value = { "", "/", "/list" }, method = { POST, GET })
	public String list(Model model) {
		LOGGER.info("Ingresando clases ofertadas.");
		OfferedClassDto offeredClassDto = offeredClassService.find(new OfferedClassDto());
		model.addAttribute("offeredClassDto",offeredClassDto);		
		return ViewConstant.OFFEREDCLASS_LIST;

	}*/
	
	@RequestMapping(value = { "", "/", "/list" }, method = {GET,POST})
	public String listPage(Model model, OfferedClassDto offeredClassDto) {
		LOGGER.info("{} - Ingresando clases ofertadas /list-pagination:{}.", offeredClassDto.getSearchByValue());
		OfferedClassDto dto = offeredClassService.findPageable(offeredClassDto);
		List<String> combo = new ArrayList<String>();
		combo.add("Clave");
		combo.add("UAP");
		combo.add("Docente");
		combo.add("Grupo");
		combo.add("P. Educativo");
		combo.add("Semestre");
		dto.setComboSearch(combo);
		model.addAttribute("offeredClassDto",dto);
		return ViewConstant.OFFEREDCLASS_LIST_PAGE;

	}
	
	@GetMapping(value = "/add")
	public String add(Model model) {
		LOGGER.info("{} - Inicia agregar clase ofertada.", userSecurityService.getIdSession());
		model.addAttribute("offeredClassDto", offeredClassService.prepareDto(new OfferedClassDto()));
		LOGGER.info("{} - Termina agregar clase ofertada.", userSecurityService.getIdSession());
		return ViewConstant.ADD_OFFEREDCLASS;
	}
	
	@PostMapping(value = "/save")
	public String save(Model model, OfferedClassDto form, RedirectAttributes redirAttrs) {
		LOGGER.info("{} - Inicia guardado de clase ofertada.", 
				userSecurityService.getIdSession());
		try {
			offeredClassService.save(form);
			if(form.getId() > 0) {
				redirAttrs.addFlashAttribute(ViewConstant.MESSAGE, MessagesConstant.EDIT_SUCCESS_MESSAGE);
			}else {
				redirAttrs.addFlashAttribute(ViewConstant.MESSAGE, MessagesConstant.ADD_SUCCESS_MESSAGE);
			}
			model.addAttribute("offeredClassDto", form);
			LOGGER.info("{} - Termina guardado de clase ofertada.", userSecurityService.getIdSession());
			return ViewConstant.REDIRECT_OFFEREDCLASS_LIST;
		} catch (ServiceException se) {
			model.addAttribute(ErrorConstant.ERROR_MESSAGE, se.getMessage());
			model.addAttribute("offeredClassDto", form);
			LOGGER.info("{} - Termina guardado con error de clase ofertada.", userSecurityService.getIdSession(), form.getUapCharge());
			return ViewConstant.OFFEREDCLASS_LIST;
		}
	}
	
	@PostMapping(value = "/edit")
	public String edit(Model model, @RequestParam(name = "id", required = true) long id) {
		LOGGER.info("{} - Inicia carga de vista de edicion  de clase ofertada.", userSecurityService.getIdSession());
		OfferedClassDto offeredClassDto;
		try {
			offeredClassDto = offeredClassService.getOfferedClassById(id);
			model.addAttribute("offeredClassDto", offeredClassDto);
			LOGGER.info("{} - Termina carga exitosa, de vista de edicion  de clase ofertada - uapCharge:{}", 
					userSecurityService.getIdSession(), offeredClassDto.getUapCharge());
			return ViewConstant.ADD_OFFEREDCLASS;
		} catch (ServiceException se) {
			model.addAttribute(ErrorConstant.ERROR_MESSAGE, se.getMessage());
			LOGGER.info("{} - Termina con error, carga de vista de edicion  de clase ofertada.", 
					userSecurityService.getIdSession());
			return ViewConstant.ADD_OFFEREDCLASS;
		}
	}
	
	@PostMapping(value = "/delete")
	public String delete(Model model, OfferedClassDto form, RedirectAttributes redirAttrs) {
		LOGGER.info("{} - Inicia eliminacion de clase ofertada:{}", userSecurityService.getIdSession(), form.getId());
		try {
			offeredClassService.delete(form);
			redirAttrs.addFlashAttribute(ViewConstant.MESSAGE, MessagesConstant.DELETE_SUCCESS_MESSAGE);
			LOGGER.info("{} - Termina eliminacion de clase ofertada:{}", userSecurityService.getIdSession(), form.getId());
			model.addAttribute("offeredClassDto", offeredClassService.find(new OfferedClassDto()));
			return ViewConstant.REDIRECT_OFFEREDCLASS_LIST;
		} catch (ServiceException se) {
			model.addAttribute(ErrorConstant.ERROR_MESSAGE, se.getMessage());
			LOGGER.info("{} - Terminacon error, eliminacion de clase ofertada:{}", userSecurityService.getIdSession(), form.getId());
			return ViewConstant.OFFEREDCLASS_LIST;
		}
	}
}
