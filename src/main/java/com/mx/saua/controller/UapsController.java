package com.mx.saua.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.mx.saua.constant.ErrorConstant;
import com.mx.saua.constant.MessagesConstant;
import com.mx.saua.constant.ViewConstant;
import com.mx.saua.dto.UapDto;
import com.mx.saua.exception.ServiceException;
import com.mx.saua.service.UapService;

/**
 * Controlador de uaps
 * @author Vic
 * @version 1.0
 *
 */
@Controller
@RequestMapping("admin/uaps")
public class UapsController {
	/**
	 * Logger de la clase
	 */
	private Logger log = LoggerFactory.getLogger(UapsController.class);
	
	/**
	 * Inyección de uapService
	 */
	@Autowired
	private UapService uapService;
	
	@GetMapping(value = { "", "/", "/list" })
	public String list(Model model) {
		UapDto uapDto = uapService.find(new UapDto());
		model.addAttribute("uapDto",uapDto);
		return ViewConstant.UAPS_LIST;
	}
	
	@GetMapping(value = "/add")
	public String add(Model model) {
		model.addAttribute("uapDto", uapService.prepareDto(new UapDto()));
		return ViewConstant.ADD_UAPS;
	}
	
	@PostMapping(value = "/save")
	public String save(Model model, UapDto form, RedirectAttributes redirAttrs) {
		log.info("Se entra al metodo save");
		try {
			if(form.isNewRegister()) {
				uapService.save(form);
				redirAttrs.addFlashAttribute(ViewConstant.MESSAGE, MessagesConstant.ADD_SUCCESS_MESSAGE);
				
			}else {
				uapService.save(form);
				redirAttrs.addFlashAttribute(ViewConstant.MESSAGE, MessagesConstant.EDIT_SUCCESS_MESSAGE);
			}
			model.addAttribute("uapDto", uapService.prepareDto(form));
			return ViewConstant.REDIRECT_UAPS_LIST;
		} catch (ServiceException se) {
			model.addAttribute(ErrorConstant.ERROR_MESSAGE, se.getMessage());
			model.addAttribute("uapDto", uapService.prepareDto(form));
			return ViewConstant.ADD_UAPS;
		}
	}
	
	@PostMapping(value = "/edit")
	public String edit(Model model, @RequestParam(name = "key", required = true) String key) {
		UapDto uapDto;
		try {
			uapDto = uapService.getUapByKey(key);
			uapDto.setNewRegister(false);
			model.addAttribute("uapDto",uapService.prepareDto(uapDto));
			return ViewConstant.ADD_UAPS;
		} catch (ServiceException se) {
			model.addAttribute(ErrorConstant.ERROR_MESSAGE, se.getMessage());
			return ViewConstant.ADD_UAPS;
		}
		
	}
	
	@PostMapping(value = "/delete")
	public String delete(Model model, UapDto form, RedirectAttributes redirAttrs) {
		try {
			uapService.delete(form);
			redirAttrs.addFlashAttribute(ViewConstant.MESSAGE, MessagesConstant.DELETE_SUCCESS_MESSAGE);
			model.addAttribute("uapDto",uapService.find(new UapDto()));
			return ViewConstant.REDIRECT_UAPS_LIST;
		} catch (ServiceException se) {
			redirAttrs.addFlashAttribute(ErrorConstant.ERROR_MESSAGE, se.getMessage());
			model.addAttribute("uapDto",uapService.find(new UapDto()));
			return ViewConstant.REDIRECT_UAPS_LIST;
		}
	}
	
}
