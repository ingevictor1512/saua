package com.mx.saua.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.mx.saua.constant.ViewConstant;
import com.mx.saua.dto.ProgramDto;
import com.mx.saua.service.ProgramService;
import com.mx.saua.service.impl.UserSecurityService;

/**
 * Controller UapsAssignController
 * @author Victor Francisco
 * @version 1.0
 *
 */
@Controller
@RequestMapping("admin/uapsassign")
public class UapsAssignController {
	
	/**
	 * Logger de la clase
	 */
	private Logger LOGGER = LoggerFactory.getLogger(UapsAssignController.class);
	
	/**
	 * Inyección de UserSecurityService
	 */
	@Autowired
	private UserSecurityService userSecurityService;
	
	/**
	 * Inyección de ProgramService
	 */
	@Autowired
	private ProgramService programService;
	
	@GetMapping(value = { "", "/", "/list"})
	public String list(Model model) {
		LOGGER.info("{} - ProgramController - Generando el listado de programas educativos", userSecurityService.getIdSession());
		ProgramDto programDto = programService.find(new ProgramDto());
		model.addAttribute("programDto",programDto);
		LOGGER.info("{} - ProgramController - Listado generando correctamente", userSecurityService.getIdSession());
		return ViewConstant.ASSIGN_LIST;
	}

}
