package com.mx.saua.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.mx.saua.constant.ErrorConstant;
import com.mx.saua.constant.MessagesConstant;
import com.mx.saua.constant.ViewConstant;
import com.mx.saua.dto.GroupDto;
import com.mx.saua.exception.ServiceException;
import com.mx.saua.service.GroupService;
import com.mx.saua.service.impl.UserSecurityService;

/**
 * Controlador grupos
 * @author Benito Morales
 * @version 1.0
 *
 */
@Controller
@RequestMapping("admin/groups")
public class GroupController {

	/**
	 * Logger de la clase
	 */
	private Logger LOGGER = LoggerFactory.getLogger(GroupController.class);
	
	/**
	 * Inyección de groupService
	 */
	@Autowired
	private GroupService groupService;
	
	/**
	 * Inyección de UserSecurityService
	 */
	@Autowired
	private UserSecurityService userSecurityService;
	
	@GetMapping(value = { "", "/", "/list" })
	public String list(Model model) {
		LOGGER.info("{} - GroupController - Generando el listado de grupos", userSecurityService.getIdSession());
		GroupDto groupDto = groupService.find(new GroupDto());
		model.addAttribute("groupDto",groupDto);
		LOGGER.info("{} - GroupController - Listado de grupos generando correctamente", userSecurityService.getIdSession());
		return ViewConstant.GROUP_LIST;
	}

	@GetMapping(value = "/add")
	public String add(Model model) {
		LOGGER.info("{} - GroupController - Ingresando a agregar grupo", userSecurityService.getIdSession());
		model.addAttribute("groupDto", groupService.prepareDto(new GroupDto()));
		return ViewConstant.ADD_GROUP;
	}
	
	@PostMapping(value = "/save")
	public String save(Model model, GroupDto form, RedirectAttributes redirAttrs) {
		LOGGER.info("Ingresando a guardar grupo:{}", form.toString());
		try {
			if(form.getId()>0) {
				groupService.save(form);
				LOGGER.info("{} - GroupController - Grupo modificado correctamente", userSecurityService.getIdSession());
				redirAttrs.addFlashAttribute(ViewConstant.MESSAGE, MessagesConstant.EDIT_SUCCESS_MESSAGE);
			}else {
				groupService.save(form);
				LOGGER.info("{} - GroupController - Grupo agregado correctamente", userSecurityService.getIdSession());
				redirAttrs.addFlashAttribute(ViewConstant.MESSAGE, MessagesConstant.ADD_SUCCESS_MESSAGE);
			}
			model.addAttribute("groupDto", groupService.prepareDto(form));
			return ViewConstant.REDIRECT_GROUP_LIST;
		} catch (ServiceException se) {
			model.addAttribute(ErrorConstant.ERROR_MESSAGE, se.getMessage());
			model.addAttribute("groupDto", groupService.prepareDto(form));
			return ViewConstant.ADD_GROUP;
		}
	}
	
	@PostMapping(value = "/edit")
	public String edit(Model model, @RequestParam(name = "id", required = true) long idGroup) {
		LOGGER.info("{} - GroupController - Ingresando a editar grupo:{}", userSecurityService.getIdSession(), idGroup);
		try {
			model.addAttribute("groupDto", groupService.prepareDto(groupService.getGroupById(idGroup)));
			return ViewConstant.ADD_GROUP;
		} catch (ServiceException se) {
			model.addAttribute(ErrorConstant.ERROR_MESSAGE, se.getMessage());
			return ViewConstant.ADD_GROUP;
		}
	}
	
	@PostMapping(value = "/delete")
	public String delete(Model model, GroupDto form, RedirectAttributes redirAttrs) {
		LOGGER.info("{} - GroupController - Ingresando a eliminar grupo:{}", userSecurityService.getIdSession(), form.toString());
		try {
			groupService.delete(form);
			redirAttrs.addFlashAttribute(ViewConstant.MESSAGE, MessagesConstant.DELETE_SUCCESS_MESSAGE);
			model.addAttribute("groupDto",groupService.find(groupService.prepareDto(new GroupDto())));
			LOGGER.info("{} - GroupController - Grupo eliminado correctamente", userSecurityService.getIdSession());
			return ViewConstant.REDIRECT_GROUP_LIST;
		} catch (ServiceException se) {
			model.addAttribute(ErrorConstant.ERROR_MESSAGE, se.getMessage());
			model.addAttribute("groupDto",groupService.find(groupService.prepareDto(new GroupDto())));
			return ViewConstant.GROUP_LIST;
		}
	}
}
