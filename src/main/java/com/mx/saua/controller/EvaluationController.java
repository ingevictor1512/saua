package com.mx.saua.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.mx.saua.constant.ErrorConstant;
import com.mx.saua.constant.ViewConstant;
import com.mx.saua.dto.QuestionDto;
import com.mx.saua.dto.StudentChargeDto;
import com.mx.saua.dto.UserDto;
import com.mx.saua.entity.Configuration;
import com.mx.saua.entity.Group;
import com.mx.saua.entity.LoadAuthorization;
import com.mx.saua.entity.Tutorship;
import com.mx.saua.enums.ConfigurationEnum;
import com.mx.saua.enums.InquestTypeEnum;
import com.mx.saua.exception.ServiceException;
import com.mx.saua.repository.ConfigurationRepository;
import com.mx.saua.repository.GroupRepository;
import com.mx.saua.repository.LoadAuthorizationRepository;
import com.mx.saua.repository.TutorshipRepository;
import com.mx.saua.service.KardexService;
import com.mx.saua.service.ProgramService;
import com.mx.saua.service.QuestionService;
import com.mx.saua.service.StudentService;
import com.mx.saua.service.UserAnswerEvalService;
import com.mx.saua.service.UserService;
import com.mx.saua.service.impl.UserSecurityService;


/**
 * Controller que responde a petciones de la evaluacion
 * @author Victor Garcia
 * @version 1.0
 *
 */
@Controller
@RequestMapping("student/evaluation")
public class EvaluationController {
	
	/**
	 * Logger de la clase
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(EvaluationController.class);
	/**
	 * Inyeccion de studentservice
	 */
	@Autowired
	private StudentService studentService;
	/**
	 * Inyección de UserSecurityService
	 */
	@Autowired
	private UserSecurityService userSecurityService;
	/**
	 * Inyeccion de userService
	 */
	@Autowired
	private UserService userService;

	/**
	 * Inyeccion de programService
	 */
	@Autowired
	private ProgramService programService;
	/**
	 * Inyeccion de loadAuthorizationRepository
	 */
	@Autowired
	private LoadAuthorizationRepository loadAuthorizationRepository;

	/**
	 * Inyeccion de tutor
	 */
	@Autowired
	private TutorshipRepository tutorshipRepository;
	
	@Autowired
	private GroupRepository groupRepository;
	
	/**
	 * Inyección de QuestionService
	 */
	@Autowired
	private QuestionService questionService;
	
	@Autowired
	private UserAnswerEvalService userAnswerEvalService;

	@Autowired
	private ConfigurationRepository configurationRepository;
	
	@Autowired
	private KardexService kardexService;

	@GetMapping(value = { "", "/", "/list" })
	public String list(Model model, RedirectAttributes redirAttrs) {
		String autoEval = "";
		try{
			autoEval = kardexService.isAutoEvalActive();
		}catch (ServiceException e) {
			redirAttrs.addFlashAttribute(ErrorConstant.ERROR_MESSAGE,e.getMessage());
			return ViewConstant.REDIRECT_HOME;
		}
		if(!autoEval.isEmpty()) {
			redirAttrs.addFlashAttribute(ViewConstant.MESSAGE, autoEval);
			return ViewConstant.REDIRECT_HOME;
		}
		
		try {
			UserDetails userDetails = userSecurityService.getUserCurrentDetails();
			UserDto user = userService.findByUsername(userDetails.getUsername());
			
			/* Se obtiene el periodo anterior*/
			Configuration conf = configurationRepository.findByKey(ConfigurationEnum.PREVIOUS_PERIOD.toString());		
			
			// Revisar si la carga academica ya se encuentra autorizada
			LoadAuthorization auth = null;
			auth = loadAuthorizationRepository.findByStudentAndActivePeriod(user.getStudentDto().getRegister(), Long.valueOf(conf.getValue()));

			LOGGER.info("{} - {} - Cargando carga acad\u00E9mica de {} para evaluar al docente.", userSecurityService.getIdSession(),userDetails.getUsername(), user.getStudentDto().getRegister());
			StudentChargeDto studentChargeDto = studentService.findChargeByRegisterByPeriodoId(user.getStudentDto().getRegister());
			studentChargeDto.setPrograms(programService.find());
			studentChargeDto.setStudent(studentService.getStudentByRegister(user.getStudentDto().getRegister()));
			studentChargeDto.setAuthorization((auth != null) ? true : false);
			List<Tutorship> tutor = tutorshipRepository.findByStudentRegister(user.getStudentDto().getRegister());
			studentChargeDto.setStudentRegister(user.getStudentDto().getRegister());
			if (tutor != null && tutor.size() > 0) {
				studentChargeDto.setTutorName(
						tutor.get(0).getTeacher().getName() + " " + tutor.get(0).getTeacher().getLastname() + " "
								+ tutor.get(0).getTeacher().getSecondLastname());
			} else {
				studentChargeDto.setTutorName("");
			}
			/**
			 * Se obtiene el status de las evaluaciones
			 */
			studentChargeDto = userAnswerEvalService.getStatusEvaluations(studentChargeDto);
			
			model.addAttribute("studentChargeDto", studentChargeDto);
			Group group = groupRepository.getOne(studentChargeDto.getStudent().getGroupDto().getId());
			studentChargeDto.getStudent().getGroupDto()
					.setTurn(group.getEducationalProgram().getAbr() + " - " + group.getTurn());
			Configuration configuration =configurationRepository.findByKey(ConfigurationEnum.TEXT_HEADER_EVAL.toString());
			studentChargeDto.setTextHeader(configuration.getValue());
			LOGGER.info("{} - {} - StudentController - Termina carga acad\u00E9mica para evaluar al docente.", userSecurityService.getIdSession(),userDetails.getUsername());
			return ViewConstant.EVAL_DOCENT_STUDENT;
		} catch (ServiceException se) {
			model.addAttribute(ErrorConstant.ERROR_MESSAGE, se.getMessage());
			return ViewConstant.EVAL_DOCENT_STUDENT;
		}
	}
	
	@PostMapping(value = "/teacher")
	public String teacherEval(Model model, @RequestParam(name = "id", required = true) Long id) {
		LOGGER.info("{} - StudentController - Inicia evaluación del docente.", userSecurityService.getIdSession());
		/* Extraer preguntas para evaluación de profesor*/
		model.addAttribute("questionDto",questionService.find(new QuestionDto(), InquestTypeEnum.EVAL_DOCENTE.toString()));
		model.addAttribute("studentChargeDto",studentService.getChargeById(id));
		LOGGER.info("{} - StudentController - Termina evaluación del docente.", userSecurityService.getIdSession());
		return ViewConstant.TEACHER_EVALUATION;
	}
	
	@PostMapping(value = "/auto")
	public String autoEval(Model model, @RequestParam(name = "id_auto", required = true) Long id) {
		LOGGER.info("{} - StudentController - Inicia autoevaluación.", userSecurityService.getIdSession());
		/* Extraer preguntas para evaluación de profesor*/
		QuestionDto dto = questionService.find(new QuestionDto(), InquestTypeEnum.AUTOEVALUACION.toString());
		
		model.addAttribute("questionDto",dto);
		model.addAttribute("studentChargeDto",studentService.getChargeById(id));
		LOGGER.info("{} - StudentController - Termina autoevaluación.", userSecurityService.getIdSession());
		return ViewConstant.AUTO_EVALUATION;
	}
}
