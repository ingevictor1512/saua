package com.mx.saua.controller;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.mx.saua.constant.ErrorConstant;
import com.mx.saua.constant.MessagesConstant;
import com.mx.saua.constant.ViewConstant;
import com.mx.saua.dto.CategoryDto;
import com.mx.saua.dto.TeacherDto;
import com.mx.saua.entity.Category;
import com.mx.saua.exception.ServiceException;
import com.mx.saua.repository.CategoryRepository;
import com.mx.saua.service.TeacherService;

/**
 * Controller que responde a crud de profesores
 * @author Benito Morales
 * @version 1.0
 *
 */
@Controller
@RequestMapping("admin/teachers")
public class TeacherController {

	/**
	 * Logger de la clase
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(TeacherController.class);
	
	/**
	 * Inyección del servicio
	 */
	@Autowired
	private TeacherService teacherService;
	
	/**
	 * Inyección de CategoryRepository
	 */
	@Autowired
	private CategoryRepository categoryRepository;
	
	@GetMapping(value = { "", "/", "/list" })
	public String list(Model model) {
		model.addAttribute("teacherDto",teacherService.find(new TeacherDto()));
		return ViewConstant.TEACHER_LIST;
	}
	
	@GetMapping(value = "/add")
	public String add(Model model) {
		TeacherDto teacherDto = new TeacherDto();
		teacherDto.setCategories(getCategoriesDto());
		model.addAttribute("teacherDto", teacherDto);
		return ViewConstant.ADD_TEACHER;
	}

	@PostMapping(value = "/save")
	public String save(Model model, TeacherDto form, RedirectAttributes redirAttrs) {
		try {
			if(form.getId() == 0) {
				teacherService.save(form);
				redirAttrs.addFlashAttribute(ViewConstant.MESSAGE, MessagesConstant.ADD_SUCCESS_MESSAGE);
			}else {
				teacherService.save(form);
				redirAttrs.addFlashAttribute(ViewConstant.MESSAGE, MessagesConstant.EDIT_SUCCESS_MESSAGE);
			}
			form.setCategories(getCategoriesDto());
			model.addAttribute("teacherDto", form);
			return ViewConstant.REDIRECT_TEACHER_LIST;
		} catch (ServiceException se) {
			form.setCategories(getCategoriesDto());
			model.addAttribute(ErrorConstant.ERROR_MESSAGE, se.getMessage());
			model.addAttribute("teacherDto", form);
			return ViewConstant.ADD_TEACHER;
		}
	}
	
	@PostMapping(value = "/edit")
	public String edit(Model model, @RequestParam(name = "register", required = true) String register) {
		TeacherDto teacherDto;
		try {
			teacherDto = teacherService.getTeacherByRegister(register);
			teacherDto.setCategories(getCategoriesDto());
			model.addAttribute("teacherDto", teacherDto);
			return ViewConstant.ADD_TEACHER;
		} catch (ServiceException se) {
			model.addAttribute(ErrorConstant.ERROR_MESSAGE, se.getMessage());
			return ViewConstant.ADD_TEACHER;
		}
	}
	
	@PostMapping(value = "/delete")
	public String delete(Model model, TeacherDto form, RedirectAttributes redirAttrs) {
		try {
			teacherService.delete(form);
			redirAttrs.addFlashAttribute(ViewConstant.MESSAGE, MessagesConstant.DELETE_SUCCESS_MESSAGE);
			model.addAttribute("teacherDto",teacherService.find(new TeacherDto()));
			return ViewConstant.REDIRECT_TEACHER_LIST;
		} catch (ServiceException se) {
			model.addAttribute(ErrorConstant.ERROR_MESSAGE, se.getMessage());
			return ViewConstant.TEACHER_LIST;
		}
	}
	
	private List<CategoryDto> getCategoriesDto() {
		List<Category> categories = categoryRepository.findAll();
		List<CategoryDto> categoriesDto = new ArrayList<>();
		categories.forEach(p -> {
			categoriesDto.add(new CategoryDto(p.getId(), p.getDescription()));
		});
		return categoriesDto;
	}
	@GetMapping("/exportInquestTotal")
	public ResponseEntity<InputStreamResource> exportInquestTotal() throws Exception{
		ByteArrayInputStream  stream = teacherService.exportInquestTotal();
		
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Disposition", "attachment; filename=Encuestas_contestadas.xls");
		
		return ResponseEntity.ok().headers(headers).body(new InputStreamResource(stream));
	}
	
	@GetMapping("/exportEvalsTotal")
	public ResponseEntity<InputStreamResource> exportEvalsTotal() throws Exception{
		ByteArrayInputStream  stream = teacherService.exportEvalsTotal();
		
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Disposition", "attachment; filename=Tutorias.xls");
		
		return ResponseEntity.ok().headers(headers).body(new InputStreamResource(stream));
	}
	@GetMapping("/exportEvalsTeachersAll")
	public ResponseEntity<InputStreamResource> exportEvalsTeachersAll() throws Exception{
		//ByteArrayInputStream  stream = teacherService.exportEvalsTeachersAll(3L);
		
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Disposition", "attachment; filename=Evaluaciones_docentes.xls");
		
		//return ResponseEntity.ok().headers(headers).body(new InputStreamResource(stream));
		return null;
	}

}
