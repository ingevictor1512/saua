package com.mx.saua.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.mx.saua.constant.ErrorConstant;
import com.mx.saua.constant.MessagesConstant;
import com.mx.saua.constant.ViewConstant;
import com.mx.saua.dto.ApplicantDto;
import com.mx.saua.dto.GuestDto;
import com.mx.saua.entity.Applicant;
import com.mx.saua.exception.ServiceException;
import com.mx.saua.service.ApplicantService;
import com.mx.saua.service.Arqsrv;
import com.mx.saua.service.GuestService;

@Controller
@RequestMapping("admin/applicant")
public class ApplicantController extends Arqsrv {

    @Autowired
    private ApplicantService applicantService;
    
    @Autowired
    private GuestService guestService;

    @GetMapping(value = { "", "/", "/list" })
    public String list(Model model) throws ServiceException{
        LOGGER.info("{} - Inicia carga de candidatos a beca.", userSecurityService.getIdSession());
        model.addAttribute("applicationDto", applicantService.find(new ApplicantDto()));
        LOGGER.info("{} - Termina carga de candidatos a beca.", userSecurityService.getIdSession());
        return ViewConstant.APPLICANT_LIST;
    }
    
    @GetMapping(value = "/addstudent")
    public String addStudent(Model model) {
        ApplicantDto applicantDto = applicantService.prepareDto(new ApplicantDto());
        model.addAttribute("applicationDto", applicantDto);
        return ViewConstant.ADD_APPLICANT_STUDENT;
    }

    @GetMapping(value = "/addguest")
    public String addGuest(Model model) {
        GuestDto guestDto = guestService.prepareDto(new GuestDto());
        guestDto.setNewRegister(true);
        model.addAttribute("guestDto", guestDto);
        return ViewConstant.ADD_APPLICANT_GUEST;
    }
    
    @PostMapping(value = "/edit")
    public String edit(Model model, @RequestParam(name = "id", required = true) Long id) throws ServiceException{
        LOGGER.info("{} - Ingresando a editar postulado:{}", userSecurityService.getIdSession(), id);
        ApplicantDto applicantDto = applicantService.getById(id);
        model.addAttribute("applicationDto", applicantDto);
        return ViewConstant.EDIT_APPLICANT;
    }

    @PostMapping(value = "/save")
    public String save(Model model, ApplicantDto form, RedirectAttributes redirAttrs) throws ServiceException {
        ApplicantDto applicant = applicantService.getById(form.getId());
        if(applicant.isGuest()) {
        	LOGGER.info("{} - Inicia guardar Invitado:{}", userSecurityService.getIdSession(), form);
        	applicant.getGuestDto().setStatusGrant(form.isStatusGrant());
        	applicant.getGuestDto().setGrantId(form.getGrantId());
        	applicant.getGuestDto().setApplicantId(applicant.getId());
        	guestService.save(applicant.getGuestDto());
        	LOGGER.info("{} - Termina guardar Invitado.", userSecurityService.getIdSession());
        }else {
        	LOGGER.info("{} - Inicia guardar Postulado:{}", userSecurityService.getIdSession(), form);
        	applicantService.save(form);
        	LOGGER.info("{} - Termina guardar Postulado.", userSecurityService.getIdSession());
        }
        redirAttrs.addFlashAttribute(ViewConstant.MESSAGE, MessagesConstant.EDIT_SUCCESS_MESSAGE);
        LOGGER.info("{} - Termina guardar categoria.", userSecurityService.getIdSession());
        return ViewConstant.REDIRECT_APPLICANT_LIST;
    }
    
    @PostMapping(value = "/assign")
    public String assign(Model model, @RequestParam(name = "id", required = true) Long id) throws ServiceException{
        LOGGER.info("{} - Ingresando a asignar cafeterias:{}", userSecurityService.getIdSession(), id);
        ApplicantDto applicantDto = applicantService.getById(id);
        model.addAttribute("applicationDto", applicantDto);
        return ViewConstant.ASSIGN_APPLICANT;
    }
    
    @PostMapping(value = "/saveguest")
    public String save(Model model, GuestDto form, RedirectAttributes redirAttrs) throws ServiceException {
    	LOGGER.info("Inicia guardar Invitado:{}", form);
        
	    	if(!form.isNewRegister()) {
	            LOGGER.info("{} - Ingresando a modificar la GuestDto: {}", userSecurityService.getIdSession(), form.toString());
	            //guestService.save(form);
	            LOGGER.info("{} - GuestDto modificada correctamente", userSecurityService.getIdSession());
	            redirAttrs.addFlashAttribute(ViewConstant.MESSAGE, MessagesConstant.EDIT_SUCCESS_MESSAGE);
	        } else {
	        	try {
		            LOGGER.info("{} - Ingresando a guardar Invitado: {}", userSecurityService.getIdSession(), form.toString());
		            guestService.save(form);
		            LOGGER.info("{} - Invitado guardado correctamente", userSecurityService.getIdSession());
		            redirAttrs.addFlashAttribute(ViewConstant.MESSAGE, MessagesConstant.ADD_SUCCESS_MESSAGE);
	        	}catch(ServiceException se) {
	            	redirAttrs.addFlashAttribute(ViewConstant.ERROR_MESSAGE, se.getMessage());
	            	model.addAttribute("applicationDto", form);
	            	return ViewConstant.REDIRECT_APPLICANT_GUEST;
	            }
	        }
        LOGGER.info("{} - Termina guardar Invitado.", userSecurityService.getIdSession());
        return ViewConstant.REDIRECT_APPLICANT_LIST;
    }
}
