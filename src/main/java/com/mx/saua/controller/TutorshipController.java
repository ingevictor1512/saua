package com.mx.saua.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.mx.saua.constant.ErrorConstant;
import com.mx.saua.constant.MessagesConstant;
import com.mx.saua.constant.ViewConstant;
import com.mx.saua.dto.TutorshipDto;
import com.mx.saua.exception.ServiceException;
import com.mx.saua.service.TutorshipService;
import com.mx.saua.service.impl.UserSecurityService;

/**
 * Controlador de tutorias
 * @author Vic
 * @version 1.0
 *
 */
@Controller
@RequestMapping("admin/tutorships")
public class TutorshipController {

	/**
	 * Inyección de Services
	 */
	@Autowired
	private TutorshipService tutorshipService;
	
	/**
	 * Inyección de UserSecurityService
	 */
	@Autowired
	private UserSecurityService userSecurityService;
	
	@GetMapping(value = { "", "/", "/list" })
	public String list(Model model) {
		UserDetails userDetails = userSecurityService.getUserCurrentDetails();
		TutorshipDto tutorshipDto = tutorshipService.find(new TutorshipDto());
		tutorshipDto.setAuthority(userDetails.getAuthorities().iterator().next().getAuthority());
		model.addAttribute("tutorshipDto",tutorshipDto);
		return ViewConstant.TUTORSHIP_LIST;
	}
	
	@GetMapping(value = "/add")
	public String add(Model model) {
		TutorshipDto tutorshipDto = tutorshipService.prepareDto(new TutorshipDto());
		UserDetails userDetails = userSecurityService.getUserCurrentDetails();
		tutorshipDto.setAuthority(userDetails.getAuthorities().iterator().next().getAuthority());
		model.addAttribute("tutorshipDto", tutorshipDto);
		return ViewConstant.ADD_TUTORSHIP;
	}
	
	@PostMapping(value = "/save")
	public String save(Model model, TutorshipDto form, RedirectAttributes redirAttrs) {		
		try {			if(form.getId() > 0) {
					tutorshipService.save(form);
					redirAttrs.addFlashAttribute(ViewConstant.MESSAGE, MessagesConstant.EDIT_SUCCESS_MESSAGE);
							
				}else {
					tutorshipService.save(form);
					redirAttrs.addFlashAttribute(ViewConstant.MESSAGE, MessagesConstant.ADD_SUCCESS_MESSAGE);
				}
			model.addAttribute("tutorshipDto", tutorshipService.prepareDto(form));
			return ViewConstant.REDIRECT_TUTORSHIP_LIST;
		} catch (ServiceException se) {
			model.addAttribute(ErrorConstant.ERROR_MESSAGE, se.getMessage());
			model.addAttribute("tutorshipDto", tutorshipService.prepareDto(form));
			return ViewConstant.ADD_TUTORSHIP;
		}
	}
	
	@PostMapping(value = "/edit")
	public String edit(Model model, @RequestParam(name = "id", required = true) Long id) {

		TutorshipDto tutorshipDto;
		try {
			UserDetails userDetails = userSecurityService.getUserCurrentDetails();
			tutorshipDto = tutorshipService.getTutorshipByKey(id);
			tutorshipDto.setAuthority(userDetails.getAuthorities().iterator().next().getAuthority());
			model.addAttribute("tutorshipDto",tutorshipService.prepareDto(tutorshipDto));
			return ViewConstant.ADD_TUTORSHIP;
		} catch (ServiceException se) {
			model.addAttribute(ErrorConstant.ERROR_MESSAGE, se.getMessage());
			return ViewConstant.ADD_TUTORSHIP;
		}
		
	}
	
	@PostMapping(value = "/delete")
	public String delete(Model model, TutorshipDto form, RedirectAttributes redirAttrs) {
		try {
			tutorshipService.delete(form);
			redirAttrs.addFlashAttribute(ViewConstant.MESSAGE, MessagesConstant.DELETE_SUCCESS_MESSAGE);
			model.addAttribute("tutorshipDto",tutorshipService.find(new TutorshipDto()));
			return ViewConstant.REDIRECT_TUTORSHIP_LIST;
		} catch (ServiceException se) {
			model.addAttribute(ErrorConstant.ERROR_MESSAGE, se.getMessage());
			return ViewConstant.REDIRECT_TUTORSHIP_LIST;
		}
	}

}
