package com.mx.saua.controller;

import java.text.SimpleDateFormat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;

import com.mx.saua.dto.UserDto;
import com.mx.saua.service.PeriodService;
import com.mx.saua.service.UserService;
import com.mx.saua.service.impl.UserSecurityService;

/**
 * Controller que responde a las peticiones de información de session
 * @author Benito Morales
 * @version 1.0
 *
 */
@Controller("sessionController")
public class SessionController {

	/**
	 * Inyeccion de userSecurityService
	 */
	@Autowired
	private UserSecurityService userSecurityService;
	
	/**
	 * Inyeccion de userService
	 */
	@Autowired
	private UserService userService;
	
	@Autowired
	private PeriodService periodService;
	
	/**
	 * Obtiene el nombre de usuario, listo para presentarlo en el front
	 * @return nombre de usuario
	 */
	public String getUsername() {
		UserDetails usr = userSecurityService.getUserCurrentDetails();
		UserDto userDto = userService.findByUsername(usr.getUsername());
		switch(usr.getAuthorities().iterator().next().getAuthority()) {
		case "STUDENT":
			return userDto.getStudentDto().getName();
		case "ADMIN":
			return usr.getUsername(); 
		case "TUTORING_COORDINATOR":
			return userDto.getTeacherDto().getName();
		case "TUTOR":
			return userDto.getTeacherDto().getName();
		case "TEACHER":
			return userDto.getTeacherDto().getName();
		}
		return usr.getUsername();
	}
	
	/**
	 * Obtiene la información del ultimo login 
	 * @return ultimo login
	 */
	public String getLastLogin() {
		UserDetails usr = userSecurityService.getUserCurrentDetails();
		UserDto userDto = userService.findByUsername(usr.getUsername());
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy kk:mm:ss");
		return userDto.getLastLogin()!=null?"\u00DAltimo acceso: "+sdf.format(userDto.getLastLogin()):"";
	}
	
	public String getActuallyPeriod() {
		
		return "Periodo: "+periodService.getActivePeriod().getSchoolCycle();
	}
}
