package com.mx.saua.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.mx.saua.constant.MessagesConstant;
import com.mx.saua.constant.ViewConstant;
import com.mx.saua.dto.PhaseDto;
import com.mx.saua.exception.ServiceException;
import com.mx.saua.service.PhaseService;

/**
 * Controller que responde a las peticiones de fases UAP
 * @author Victor Francisco
 * @version 1.0
 *
 */
@Controller
@RequestMapping("admin/phase")
public class PhaseController {
	
	/**
	 * Inyección de PhaseService
	 */
	@Autowired
	private PhaseService phaseService;
	
	@GetMapping(value = { "", "/", "/list"})
	public String list(Model model) {
		model.addAttribute("phase", phaseService.find());
		return ViewConstant.PHASE_LIST;
	}
	
	@GetMapping(value = "/add")
	public String add(Model model) {
		model.addAttribute("phase", new PhaseDto());
		return ViewConstant.ADD_PHASE;
	}
	
	@PostMapping(value = "/edit")
	public String edit(Model model, @RequestParam(name = "id", required = true) Long id) {
		PhaseDto phaseDto = phaseService.getById(id);
		model.addAttribute("phase", phaseDto);
		return ViewConstant.ADD_PHASE;
	}
	
	@PostMapping(value = "/delete")
	public String delete(Model model, PhaseDto form, RedirectAttributes redirAttrs) {
		phaseService.delete(form.getId());
		redirAttrs.addFlashAttribute(ViewConstant.MESSAGE, MessagesConstant.DELETE_SUCCESS_MESSAGE);
		return ViewConstant.REDIRECT_PHASE_LIST;
	
	}
	
	@PostMapping(value = "/save")
	public String save(Model model, PhaseDto form, RedirectAttributes redirAttrs) throws ServiceException {
		if(form.getId() > 0) {
			phaseService.save(form);
			redirAttrs.addFlashAttribute(ViewConstant.MESSAGE, MessagesConstant.EDIT_SUCCESS_MESSAGE);
		} else {
			phaseService.save(form);
			redirAttrs.addFlashAttribute(ViewConstant.MESSAGE, MessagesConstant.ADD_SUCCESS_MESSAGE);
		}
		return ViewConstant.REDIRECT_PHASE_LIST;
	}

}
