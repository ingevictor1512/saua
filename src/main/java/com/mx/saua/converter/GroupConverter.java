package com.mx.saua.converter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.mx.saua.dto.GroupDto;
import com.mx.saua.dto.StudentDto;
import com.mx.saua.entity.Group;

/**
 * Convierte Dto a Entity y viceversa
 * @author Benito Morales
 * @version 1.0
 *
 */
@Component("groupConverter")
public class GroupConverter {

	@Autowired
	private ProgramConverter programConverter;
	
	@Autowired
	private PeriodConverter periodConverter;
	
	@Autowired
	private StudentConverter studentConverter;
	
	private Logger log = LoggerFactory.getLogger(GroupConverter.class);
	
	/**
	 * Convierte de GroupDto a Group
	 * @param groupDto a convertir
	 * @return Group
	 */
	public Group convertGroupDto2Group(GroupDto groupDto) {
		Group group = new Group();
		group.setId(groupDto.getId());
		group.setAbbreviation(groupDto.getAbbreviation());
		group.setEducationalProgram(programConverter.convertProgramDto2Program(groupDto.getEducationalProgramDto()));
		if(groupDto.getGroupLeader()!=null) {
			group.setGroupLeader(studentConverter.convertStudentDto2Student(groupDto.getGroupLeader()));
		}
		group.setGroupType(groupDto.getGroupType());
		group.setLocation(groupDto.getLocation());
		group.setName(groupDto.getName());
		group.setPeriod(periodConverter.convertPeriodDto2Period(groupDto.getPeriodDto()));
		group.setTurn(groupDto.getTurn());
		group.setNextGroup(groupDto.getNextGroup());
		group.setPromoted(groupDto.isPromoted());
		return group;
	}
	
	/**
	 * Convierte a GroupDto
	 * @param period Entidad a convertir
	 * @return GroupDto
	 */
	public GroupDto convertGroup2GroupDto(Group group) {
		GroupDto groupDto = new GroupDto();
		groupDto.setId(group.getId());
		groupDto.setAbbreviation(group.getAbbreviation());
		groupDto.setEducationalProgramDto(programConverter.convertProgram2ProgramDto(group.getEducationalProgram()));
		if(group.getGroupLeader() != null){
			groupDto.setGroupLeader(studentConverter.convertStudent2StudentDto(group.getGroupLeader()));
			groupDto.setLeaderCompleteName(group.getGroupLeader().getName()+" "+group.getGroupLeader().getLastname()+" "+group.getGroupLeader().getSecondLastname());
		}else {
			groupDto.setGroupLeader(new StudentDto());
			groupDto.setLeaderCompleteName("");
		}
		groupDto.setGroupType(group.getGroupType());
		groupDto.setLocation(group.getLocation());
		groupDto.setName(group.getName());
		groupDto.setPeriodDto(periodConverter.convertPeriod2PeriodDto(group.getPeriod()));
		groupDto.setTurn(group.getTurn());
		groupDto.setAbbreviation(group.getAbbreviation());
		groupDto.setNextGroup(group.getNextGroup());
		groupDto.setPromoted(group.isPromoted());
		return groupDto;
	}
}
