package com.mx.saua.converter;

import org.springframework.stereotype.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.mx.saua.dto.ProgramDto;
import com.mx.saua.entity.Program;

/**
 * Convierte Dto a Entity y viceversa
 * @author Benito Morales
 * @version 1.0
 *
 */
@Component("programConverter")
public class ProgramConverter {

	private Logger log = LoggerFactory.getLogger(GroupConverter.class);
	/**
	 * Convierte de ProgramDto a Program
	 * @param programDto a convertir
	 * @return Program
	 */
	public Program convertProgramDto2Program(ProgramDto programDto) {
		Program program = new Program();
		program.setAbr(programDto.getAbr());
		program.setEditdate(programDto.getEditdate());
		program.setObservations(programDto.getObservations());
		program.setRegisterDate(programDto.getRegisterDate());
		program.setSchool(programDto.getSchool());
		program.setStudentPlanKey(programDto.getStudentPlanKey());
		program.setStudentPlanName(programDto.getStudentPlanName());
		program.setStudentPlanVrs(programDto.getStudentPlanVrs());
		return program;
	}
	
	/**
	 * Convierte a ProgramDto
	 * @param program Entidad a convertir
	 * @return ProgramDto
	 */
	public ProgramDto convertProgram2ProgramDto(Program program) {
		ProgramDto programDto = new ProgramDto();
		programDto.setAbr(program.getAbr());
		programDto.setEditdate(program.getEditdate());
		programDto.setObservations(program.getObservations());
		programDto.setRegisterDate(program.getRegisterDate());
		programDto.setSchool(program.getSchool());
		programDto.setStudentPlanKey(program.getStudentPlanKey());
		programDto.setStudentPlanName(program.getStudentPlanName());
		programDto.setStudentPlanVrs(program.getStudentPlanVrs());
		return programDto;
	}
}
