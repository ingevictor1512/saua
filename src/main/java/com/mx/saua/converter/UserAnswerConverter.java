package com.mx.saua.converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.mx.saua.dto.UserAnswerDto;
import com.mx.saua.entity.AnswersUser;

@Component("useAnswerConverter")
public class UserAnswerConverter {

	@Autowired
	private AnswerConverter answerConverter;
	
	@Autowired
	private QuestionConverter questionConverter;
	
	@Autowired
	private UserConverter userConverter;
	
	public UserAnswerDto convertUserAnswer2UserAnswerDto(AnswersUser userAnswer) {
		UserAnswerDto userDto = new UserAnswerDto();
		userDto.setId(userAnswer.getId());
		userDto.setAnswer((userAnswer.getAnswer()!=null)?answerConverter.convertAnswerToAnswerDto(userAnswer.getAnswer()):null);
		userDto.setEspecification(userAnswer.getSpecification());
		userDto.setPersonalizeAnswer(userAnswer.getCustomizedAnswer());
		userDto.setQuestion(questionConverter.convertQuestion2QuestionDto(userAnswer.getQuestion()));
		userDto.setUser(userConverter.convertUser2UserDto(userAnswer.getUser()));
		return userDto;
	}
}
