package com.mx.saua.converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.mx.saua.dto.AnswerDto;
import com.mx.saua.entity.Answer;

@Component("answerConverter")
public class AnswerConverter {

	@Autowired
	private QuestionConverter questionConverter;
	
	public AnswerDto convertAnswerToAnswerDto(Answer answer) {
		AnswerDto dto = new AnswerDto();
		dto.setChecked(answer.isChecked());
		dto.setId(answer.getId());
		dto.setQuestion(null);
		dto.setText(answer.getText());
		dto.setQuestion(questionConverter.convertQuestion2QuestionDto(answer.getQuestion()));
		dto.setSpecify(answer.isSpecify());
		return dto;
	}
	
}
