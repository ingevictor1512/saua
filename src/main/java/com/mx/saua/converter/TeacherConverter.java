package com.mx.saua.converter;

import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.mx.saua.dto.CategoryDto;
import com.mx.saua.dto.TeacherDto;
import com.mx.saua.entity.Authority;
import com.mx.saua.entity.Category;
import com.mx.saua.entity.Teacher;
import com.mx.saua.entity.User;
import com.mx.saua.enums.AuthorityEnum;
import com.mx.saua.repository.AuthorityRepository;
import com.mx.saua.repository.UserRepository;

/**
 * Convertidor de Teacher a TeacherDto y viceversa
 * @author Benito Morales
 * @version 1.0
 *
 */
@Component("teacherConverter")
public class TeacherConverter {
	
	private Logger log = LoggerFactory.getLogger(TeacherConverter.class);
	
	public Teacher convertTeacherDto2Teacher(TeacherDto teacherDto) {
		Teacher teacher = new Teacher();
		teacher.setId(teacherDto.getId());
		teacher.setLastname(teacherDto.getLastname());
		teacher.setSecondLastname(teacherDto.getSecondLastname());
		teacher.setName(teacherDto.getName());
		teacher.setCategory(new Category(teacherDto.getCategoryDto().getId(), teacherDto.getCategoryDto().getDescription()));
		teacher.setCubicle(teacherDto.getCubicle());
		teacher.setRegisterDate(teacherDto.getRegisterDate());
		teacher.setEditDate(teacherDto.getEditDate());
		teacher.setGender(teacherDto.getGender());
		teacher.setRegister(teacherDto.getRegister());
		return teacher;
	}

	public TeacherDto convertTeacher2TeacherDto(Teacher teacher) {
		TeacherDto teacherDto = new TeacherDto();
		teacherDto.setId(teacher.getId());
		teacherDto.setLastname(teacher.getLastname());
		teacherDto.setSecondLastname(teacher.getSecondLastname());
		teacherDto.setName(teacher.getName());
		teacherDto.setCategoryDto(new CategoryDto(teacher.getCategory().getId(), teacher.getCategory().getDescription()));
		teacherDto.setCubicle(teacher.getCubicle());
		teacherDto.setRegisterDate(teacher.getRegisterDate());
		teacherDto.setEditDate(teacher.getEditDate());
		teacherDto.setGender(teacher.getGender());
		teacherDto.setRegister(teacher.getRegister());
		if(teacher.getUsers().size()>0) {
			if(teacher.getUsers().iterator().next().getAuthorities().size() > 0){
				teacherDto.setAuthority(teacher.getUsers().iterator().next().getAuthorities().iterator().next().getAuthority());
			}else {
				teacherDto.setAuthority("undefined");
			}
		}
		if(teacher.getUsers().iterator().hasNext()) {
			teacherDto.setEmail(teacher.getUsers().iterator().next().getEmail());
			teacherDto.setPhone(teacher.getUsers().iterator().next().getPhone());
		}
		return teacherDto;
	}
}
