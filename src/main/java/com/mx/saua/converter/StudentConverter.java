package com.mx.saua.converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.mx.saua.dto.GroupDto;
import com.mx.saua.dto.IncomeDto;
import com.mx.saua.dto.ProgramDto;
import com.mx.saua.dto.StudentDto;
import com.mx.saua.entity.Group;
import com.mx.saua.entity.Student;
import com.mx.saua.repository.GroupRepository;
import com.mx.saua.repository.ProgramRepository;
import com.mx.saua.repository.StudentStatusRepository;
import com.mx.saua.service.IncomeService;

@Component("studentConverter")
public class StudentConverter {
	
	@Autowired
	private StudentStatusConverter studentStatusConverter;
	
	@Autowired
	private ProgramConverter ProgramConverter;
		
	@Autowired
	private IncomeService incomeService;
	
	@Autowired
	private GroupRepository groupRepository;
	
	@Autowired
	private ProgramRepository programRepository;
	
	@Autowired
	private StudentStatusRepository studentStatusRepository;
	
	public Student convertStudentDto2Student(StudentDto studentDto) {
		Student student = new Student();
		student.setRegister(studentDto.getRegister());
		student.setLastname(studentDto.getLastname());
		student.setSecondLastname(studentDto.getSecondLastname());
		student.setName(studentDto.getName());
		student.setCurp(studentDto.getCurp());
		student.setSemester(studentDto.getSemester());
		student.setGeneration(studentDto.getGeneration());
		student.setGender(studentDto.getGender());
		student.setRegisterDate(studentDto.getRegisterDate());
		student.setEditDate(studentDto.getEditDate());
		student.setStudentStatus(studentStatusRepository.getOne(studentDto.getStudentStatusDto().getId()));
		student.setIncome(incomeService.getIncomeById(studentDto.getIncomeDto().getId()));
		student.setTotalAutoEval(studentDto.getTotalAutoEval());
		student.setTotalTeacherEval(studentDto.getTotalTeacherEval());
		Group group = groupRepository.getOne(studentDto.getGroupDto().getId());
		student.setGroup(group);
		student.setProgram(programRepository.getOne(group.getEducationalProgram().getStudentPlanKey()));
		return student;
	}

	public StudentDto convertStudent2StudentDto(Student student) {
		StudentDto studentDto = new StudentDto();
		studentDto.setRegister(student.getRegister());
		studentDto.setLastname(student.getLastname());
		studentDto.setSecondLastname(student.getSecondLastname());
		studentDto.setName(student.getName());
		studentDto.setCurp(student.getCurp());
		studentDto.setSemester(student.getSemester());
		studentDto.setGeneration(student.getGeneration());
		studentDto.setGender(student.getGender());
		studentDto.setRegisterDate(student.getRegisterDate());
		studentDto.setEditDate(student.getEditDate());
		studentDto.setTotalAutoEval(student.getTotalAutoEval());
		studentDto.setTotalTeacherEval(student.getTotalTeacherEval());
		if(student.getUsers()!= null &&  student.getUsers().iterator().hasNext()) {
			studentDto.setEmail(student.getUsers().iterator().next().getEmail());
			studentDto.setPhone(student.getUsers().iterator().next().getPhone());
			studentDto.setFirstLogin(student.getUsers().iterator().next().isFirstLogin());
		}
		if(student.getIncome() != null) {
			studentDto.setIncomeDto(new IncomeDto(student.getIncome().getId(), student.getIncome().getDescription()));
		}
		if(student.getGroup() != null) {
			GroupDto dto = new GroupDto();
			dto.setId(student.getGroup().getId());
			dto.setName(student.getGroup().getName());
			dto.setAbbreviation(student.getGroup().getAbbreviation());
			dto.setEducationalProgramDto(ProgramConverter.convertProgram2ProgramDto(student.getProgram()));
			dto.setTurn(student.getGroup().getTurn());
			studentDto.setGroupDto(dto);
		}
		ProgramDto programDto = new ProgramDto();
		programDto.setStudentPlanKey(student.getProgram().getStudentPlanKey());
		programDto.setStudentPlanName(student.getProgram().getStudentPlanName());
		programDto.setAbr(student.getProgram().getAbr());
		studentDto.setProgramDto(programDto);
		studentDto.setStudentStatusDto(studentStatusConverter.convertStudentStatus2StudentStatusDto(student.getStudentStatus()));
		studentDto.setStudentStatus(student.getStudentStatus().getDescription());
		return studentDto;
	}
}
