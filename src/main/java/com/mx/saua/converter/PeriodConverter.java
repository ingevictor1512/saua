package com.mx.saua.converter;

import org.springframework.stereotype.Component;

import com.mx.saua.dto.PeriodDto;
import com.mx.saua.entity.Period;

/**
 * Convierte Dto a Entity y viceversa
 * @author Benito Morales
 * @version 1.0
 *
 */
@Component("periodConverter")
public class PeriodConverter {

	/**
	 * Convierte de PeriodDto a Period
	 * @param periodDto a convertir
	 * @return Period
	 */
	public Period convertPeriodDto2Period(PeriodDto periodDto) {
		Period period = new Period();
		period.setId(periodDto.getId());
		period.setActivePeriod(periodDto.isActivePeriod());
		period.setActivePeriodAdmin(periodDto.isActivePeriodAdmin());
		period.setEditdate(periodDto.getEditdate());
		period.setFinalDate(periodDto.getFinalDate());
		period.setInitialDate(periodDto.getInitialDate());
		period.setRegisterDate(periodDto.getRegisterDate());
		period.setSchoolCycle(periodDto.getSchoolCycle());
		period.setSchoolPeriod(periodDto.getSchoolPeriod());
		return period;
	}
	
	/**
	 * Convierte a PriodDto
	 * @param period Entidad a convertir
	 * @return PeriodDto
	 */
	public PeriodDto convertPeriod2PeriodDto(Period period) {
		PeriodDto periodDto = new PeriodDto();
		periodDto.setId(period.getId());
		periodDto.setActivePeriod(period.isActivePeriod());
		periodDto.setActivePeriodAdmin(period.isActivePeriodAdmin());
		periodDto.setEditdate(period.getEditdate());
		periodDto.setFinalDate(period.getFinalDate());
		periodDto.setInitialDate(period.getInitialDate());
		periodDto.setRegisterDate(period.getRegisterDate());
		periodDto.setSchoolCycle(period.getSchoolCycle());
		periodDto.setSchoolPeriod(period.getSchoolPeriod());
		return periodDto;
	}
}
