package com.mx.saua.converter;

import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.mx.saua.dto.StudentDto;
import com.mx.saua.dto.UserDto;
import com.mx.saua.entity.Authority;
import com.mx.saua.entity.User;

@Component("userConverter")
public class UserConverter {
	
	private Logger log = LoggerFactory.getLogger(UserConverter.class);
	
	@Autowired
	private StudentConverter studentConverter;
	
	@Autowired
	private TeacherConverter teacherConverter;
	
	public User convertUserDto2User(UserDto userDto) {
		User user = new User();
		user.setIdUser(userDto.getIdUser());
		user.setActive(userDto.isActive());
		user.setRegisterDate(userDto.getCreationDate());
		user.setEditdate(userDto.getEditDate());
		user.setFirstLogin(userDto.isFirstLogin());
		user.setLastLogin(userDto.getLastLogin());
		user.setPassword(userDto.getPassword());
		Set<Authority> authorities = new HashSet<>();
		Authority auth = new Authority();
		auth.setAuthority(userDto.getAuthority());
		auth.setUser(user);
		authorities.add(auth);
		user.setAuthorities(authorities);
		user.setUsername(userDto.getUsername());
		user.setEmail(userDto.getEmail());
		user.setInquest(userDto.isInquest());
		user.setPhone(userDto.getPhone());
		return user;
	}

	public UserDto convertUser2UserDto(User user) {
		UserDto userDto = new UserDto();
		userDto.setIdUser(user.getIdUser());
		userDto.setActive(user.isActive());
		userDto.setCreationDate(user.getRegisterDate());
		userDto.setEditDate(user.getEditdate());
		userDto.setFirstLogin(user.isFirstLogin());
		userDto.setLastLogin(user.getLastLogin());
		userDto.setInquest(user.isInquest());
		userDto.setPhone(user.getPhone());
		//userDto.setPassword(user.getPassword());
		if(user.getAuthorities().iterator().hasNext()) {
			userDto.setAuthority(user.getAuthorities().iterator().next().getAuthority());
		}
		userDto.setUsername(user.getUsername());
		userDto.setEmail(user.getEmail());
		if(user.getStudent() != null) {
			userDto.setStudentDto(studentConverter.convertStudent2StudentDto(user.getStudent()));
		}else {
			StudentDto studentDto = new StudentDto();
			studentDto = new StudentDto();
			studentDto.setName("");
			studentDto.setLastname("");
			studentDto.setSecondLastname("");
			userDto.setStudentDto(studentDto);
		}
		if(user.getTeacher() != null) {
			userDto.setTeacherDto(teacherConverter.convertTeacher2TeacherDto(user.getTeacher()));
		}
		return userDto;
	}
}
