package com.mx.saua.converter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.mx.saua.dto.GroupDto;
import com.mx.saua.dto.IncomeDto;
import com.mx.saua.dto.StudentDto;
import com.mx.saua.dto.UapDto;
import com.mx.saua.entity.Group;
import com.mx.saua.entity.Student;
import com.mx.saua.entity.Uap;
import com.mx.saua.repository.GroupRepository;
import com.mx.saua.repository.ProgramRepository;
import com.mx.saua.service.IncomeService;

@Component("uapConverter")
public class UapConverter {

	Logger log = LoggerFactory.getLogger(UapConverter.class);
	
	public Uap convertUapDto2Uap(UapDto uapDto) {
		Uap uap = new Uap();
		uap.setCommon_code(uapDto.getCommon_code());
		uap.setCredit(uapDto.getCredit());
		uap.setEditdate(uapDto.getEditdate());
		uap.setEducationalProgram(uapDto.getEducationalProgram());
		uap.setKey(uapDto.getKey());
		uap.setName(uapDto.getName());
		uap.setPhaseUaps(uapDto.getPhaseUaps());
		uap.setRegisterDate(uapDto.getRegisterDate());
		uap.setUapType(uapDto.getUapType());
		return uap;
	}
	
	public UapDto convertUap2UapDto(Uap uap) {
		UapDto uapDto = new UapDto();
		uapDto.setCommon_code(uap.getCommon_code());
		uapDto.setCredit(uap.getCredit());
		uapDto.setEditdate(uap.getEditdate());
		uapDto.setEducationalProgram(uap.getEducationalProgram());
		uapDto.setKey(uap.getKey());
		uapDto.setName(uap.getName());
		uapDto.setPhaseUaps(uap.getPhaseUaps());
		uapDto.setRegisterDate(uap.getRegisterDate());
		uapDto.setUapType(uap.getUapType());
		return uapDto;
	}
}
