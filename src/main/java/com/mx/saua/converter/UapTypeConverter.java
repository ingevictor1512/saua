package com.mx.saua.converter;

import org.springframework.stereotype.Component;

import com.mx.saua.dto.UapTypeDto;
import com.mx.saua.entity.UapType;

/**
 * Convierte Dto a Entity y viceversa
 * @author Vic
 * @version 1.0
 *
 */
@Component("uapTypeConverter")
public class UapTypeConverter {
	/**
	 * Convierte de UapTypeDto a UapType
	 * @param UapTypeDto a convertir
	 * @return UapType
	 */
	public UapType convertUapTypeDto2UapType(UapTypeDto uapTyDto) {
		UapType uapType = new UapType();
		uapType.setId(uapTyDto.getId());
		uapType.setCategory(uapTyDto.getCategory());
		uapType.setSemester(uapTyDto.getSemester());
		return uapType;
	}
	
	/**
	 * Convierte a UapTypeDto
	 * @param UapType Entidad a convertir
	 * @return UapTypeDto
	 */
	public UapTypeDto convertUapType2UapTypeDto(UapType uapType) {
		UapTypeDto uapTypeDto = new UapTypeDto();
		uapTypeDto.setId(uapType.getId());
		uapTypeDto.setCategory(uapType.getCategory());
		uapTypeDto.setSemester(uapType.getSemester());
		return uapTypeDto;
	}
}
