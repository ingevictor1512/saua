package com.mx.saua.converter;

import org.springframework.stereotype.Component;

import com.mx.saua.dto.QuestionDto;
import com.mx.saua.entity.Question;

@Component("questionConverter")
public class QuestionConverter {

	public QuestionDto convertQuestion2QuestionDto(Question question) {
		QuestionDto dto = new QuestionDto();
		dto.setAnswers(null);
		dto.setBody(question.getBody());
		dto.setCreationDate(question.getCreationDate());
		dto.setEditDate(question.getEditDate());
		dto.setId(question.getId());
		dto.setPlaceHolder(question.getPlaceHolder());
		dto.setQuestionBodyType(question.getQuestionBodyType());
		dto.setStatus(question.isStatus());
		dto.setText(question.getText());
		return dto;
	}
}
