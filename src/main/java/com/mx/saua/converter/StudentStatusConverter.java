package com.mx.saua.converter;

import org.springframework.stereotype.Component;

import com.mx.saua.dto.StudentStatusDto;
import com.mx.saua.entity.StudentStatus;

/**
 * Convierte de Entidad a Dto y viceversa
 * @author Benito Morales
 *
 */
@Component("studentStatusConverter")
public class StudentStatusConverter {
	
	/**
	 * Convierte un StudentStatusDto a StudentStatus
	 * @param studentStatusDto Dto a convertir
	 * @return StudentStatus
	 */
	public StudentStatus convertStudentStatusDto2StudentStatus(StudentStatusDto studentStatusDto) {
		StudentStatus studentStatus = new StudentStatus();
		studentStatus.setId(studentStatusDto.getId());
		studentStatus.setDescription(studentStatusDto.getDescription());
		studentStatus.setComments(studentStatusDto.getComments());
		return studentStatus;
	}

	/**
	 * Convierte un StudentStatus a StudentStatusDto
	 * @param studentStatus Entidad a convertir
	 * @return
	 */
	public StudentStatusDto convertStudentStatus2StudentStatusDto(StudentStatus studentStatus) {
		StudentStatusDto studentStatusDto = new StudentStatusDto();
		studentStatusDto.setId(studentStatus.getId());
		studentStatusDto.setDescription(studentStatus.getDescription());
		studentStatusDto.setComments(studentStatus.getComments());
		return studentStatusDto;
	}
}
