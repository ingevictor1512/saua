package com.mx.saua.service;

import com.mx.saua.dto.ApplicantDto;
import com.mx.saua.dto.UserDto;
import com.mx.saua.exception.ServiceException;

public interface ApplicantService {
    /**
     * Obtiene la lista de encuestas
     * @param applicantDto
     * @return
     */
    ApplicantDto find(ApplicantDto applicantDto) throws ServiceException;

    ApplicantDto prepareDto(ApplicantDto applicantDto);

    ApplicantDto getById(Long id) throws ServiceException;

    void save(ApplicantDto applicantDto) throws ServiceException;

    ApplicantDto findByUser(UserDto userDto);
    
    void saveApplicantCoffeeShop(ApplicantDto applicantDto) throws ServiceException;
}
