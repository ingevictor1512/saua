package com.mx.saua.service;

import java.util.List;

import com.mx.saua.dto.SchoolDto;

public interface SchoolService {
	
	List<SchoolDto> find();
	
	SchoolDto getById(Long id);
	
	void save(SchoolDto SchoolDto);
	
	void delete(Long id);

}
