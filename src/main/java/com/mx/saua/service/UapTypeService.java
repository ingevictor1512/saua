package com.mx.saua.service;

import java.util.List;

import com.mx.saua.dto.UapTypeDto;



public interface UapTypeService {
	
	List<UapTypeDto> find();
	
	UapTypeDto getById(Long id);
	
	void save(UapTypeDto UapTypeDto);
	
	void delete(Long id);
	

}
