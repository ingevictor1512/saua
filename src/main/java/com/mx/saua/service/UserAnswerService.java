package com.mx.saua.service;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.List;

import com.mx.saua.dto.InquestQuestionDto;
import com.mx.saua.dto.UserAnswerDto;
import com.mx.saua.dto.UserDto;
import com.mx.saua.entity.AnswersUser;
import com.mx.saua.exception.ServiceException;
import org.springframework.core.io.Resource;

public interface UserAnswerService {

	/**
	 * Obtiene la lista de respuestas de la encuesta de los usuarios
	 * @param userAnswerDto Dto donde se regresaran las respuestas
	 * @return dto con las respuestas
	 */
	UserAnswerDto find(UserAnswerDto userAnswerDto);
	
	/**
	 * Genera el excel del concentrado de la encuesta
	 * @param sin parametro
	 * @throws ServiceException
	 */
	ByteArrayInputStream exportAnswersAll() throws IOException;
	
	int totalSurveyAnswered();
	
	/**
	 * Retorna la lista de respuestas de una encuesta
	 * @param user Usuario
	 * @return  Lista de rspuestas
	 */
	List<AnswersUser> findByUser(UserDto user);
	
	/**
	 * Genera el excel del concentrado de la encuesta para el regreso a clases
	 * @param sin parametro
	 * @throws ServiceException
	 */
	ByteArrayInputStream exportAnswersBackToSchoolP() throws IOException;
	
	/**
	 * Guarda la respuesta de la pregunta en cuestion
	 * @param inqDto Dto con los insumos
	 * @return Page de las preguntas que siguen por contestar 
	 * @throws ServiceException Excepcion
	 */
	InquestQuestionDto saveAnswer(InquestQuestionDto inqDto) throws ServiceException;

	/**
	 * Verifica que el usuario no tenga respuestas guardadas para la pregunta en cuestión
	 * @param dto Datos de entrada a validar
	 * @return True en caso de que si halla respuestas
	 */
	boolean verifySaveAnswers(InquestQuestionDto dto);
	
	/**
	 * Exporta las respuestas de una encuesta en particular 
	 * @return
	 * @throws ServiceException
	 */
	Resource export(long idInquest) throws ServiceException;
}
