package com.mx.saua.service.impl;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

import com.mx.saua.entity.*;
import com.mx.saua.repository.*;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import com.mx.saua.converter.UserAnswerConverter;
import com.mx.saua.dto.InquestQuestionDto;
import com.mx.saua.dto.UserAnswerDto;
import com.mx.saua.dto.UserDto;
import com.mx.saua.enums.InquestStatusEnum;
import com.mx.saua.enums.InquestTypeEnum;
import com.mx.saua.exception.ServiceException;
import com.mx.saua.service.InquestQuestionService;
import com.mx.saua.service.UserAnswerService;
import com.mx.saua.service.UserService;

import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;

/**
 * Servicio para la administración de las respuestas de las encuestas de usuario
 * 
 * @author Benito Morales
 *
 */
@Service("userAnswerServiceImpl")
public class UserAnswerServiceImpl implements UserAnswerService {

	/* Variable de Logger */
	private static final Logger LOGGER = LoggerFactory.getLogger(UserAnswerServiceImpl.class);

	@Autowired
	private AnswerUserRepository userAnswerRepository;

	@Autowired
	private QuestionRepository questionRepository;

	@Autowired
	private UserAnswerConverter userAnswerConverter;

	@Autowired
	private UserSecurityService userSecurityService;

	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private InquestRepository inquestRepository;
	
	@Autowired
	private InquestQuestionService inquestQuestionService;
	
	@Autowired
	private UserInquestRepository userInquestRepository;
	
	@Autowired
	private AnswerRepository answerRepository;
	
	@Autowired
    private UserService userService;
	
	@Autowired
	private InquestQuestionRepository inquestQuestionRepository;
	
	@Autowired
	private PeriodRepository periodRepository;

	@Autowired
	private ActivateRepository activateRepository;
	
	@Override
	public UserAnswerDto find(UserAnswerDto userAnswerDto) {
		UserAnswerDto dto = new UserAnswerDto();
		List<AnswersUser> list = userAnswerRepository.findAll();
		List<UserAnswerDto> dtos = new ArrayList<UserAnswerDto>();

		for (AnswersUser uDto : list) {
			dtos.add(userAnswerConverter.convertUserAnswer2UserAnswerDto(uDto));
		}

		dto.setUserAnswers(dtos);
		return dto;
	}

	@Override
	public ByteArrayInputStream exportAnswersAll() throws IOException {
		LOGGER.info("{} - Inicia exportAllTutorships.", userSecurityService.getIdSession());
		List<AnswersUser> list = userAnswerRepository.findAll();
		/* Obteniendo las preguntas para ponerlas de encabezado */
		List<Question> preguntas = questionRepository.findAll();
		/* Encabezado del archivo de excel */
		String[] columns = { "MATRICULA", "ALUMNO", "P.E.", "GRUPO", preguntas.get(0).getText(),
				preguntas.get(1).getText(), preguntas.get(5).getText(), preguntas.get(6).getText(),
				preguntas.get(2).getText(), preguntas.get(3).getText(), preguntas.get(4).getText(),
				preguntas.get(7).getText(), preguntas.get(8).getText(), preguntas.get(9).getText() };
		Workbook workbook = new HSSFWorkbook();
		ByteArrayOutputStream stream = new ByteArrayOutputStream();

		Sheet sheet = workbook.createSheet("Concentrado_respuestas");
		Row row = sheet.createRow(0);

		for (int i = 0; i < columns.length; i++) {
			Cell cell = row.createCell(i);
			cell.setCellValue(columns[i]);
		}

		List<User> users = userRepository.findAll();

		int initRow = 1;
		for (User user : users) {
			if (user.getAnswers().size() > 0) {
				row = sheet.createRow(initRow);
				row.createCell(0).setCellValue(user.getStudent().getRegister());
				row.createCell(1).setCellValue(user.getStudent().getName() + " " + user.getStudent().getLastname() + " "
						+ user.getStudent().getSecondLastname());
				row.createCell(2).setCellValue(user.getStudent().getProgram().getStudentPlanName());
				row.createCell(3).setCellValue(
						user.getStudent().getGroup().getName() + " " + user.getStudent().getGroup().getTurn());
				for (AnswersUser answ : user.getAnswers()) {
					if (answ.getQuestion().getId() == 1) {// Lugar de origen:
						row.createCell(4).setCellValue(answ.getCustomizedAnswer());
					}
					if (answ.getQuestion().getId() == 2) {// Domicilio:
						row.createCell(5).setCellValue(answ.getCustomizedAnswer());
					}
					if (answ.getQuestion().getId() == 6) {// Correo Institucional oficial ( matricula@uagro.mx).
						row.createCell(6).setCellValue(
								(answ.getAnswer() != null) ? answ.getAnswer().getText() : answ.getCustomizedAnswer());
					}
					if (answ.getQuestion().getId() == 7) {// Numero de celular actual (Nota: en caso de cambiar número
															// de celular, notifique a la dirección).
						row.createCell(7).setCellValue(answ.getCustomizedAnswer());
					}
					if (answ.getQuestion().getId() == 3) {// Lugar donde tomará los cursos virtuales del próximo
															// semestre?
						row.createCell(8).setCellValue(
								(answ.getAnswer() != null)
								? (answ.getAnswer().getText().contains("especifique"))
										? "Si, " + answ.getCustomizedAnswer()
										: answ.getAnswer().getText()
								: answ.getCustomizedAnswer());
					}
					if (answ.getQuestion().getId() == 4) {// Para su conexión a sus cursos virtuales cuenta con?
						if(answ.getAnswer()!=null) {
							row.createCell(9).setCellValue(answ.getAnswer().getText());
						}else {
							row.createCell(9).setCellValue("");
						}
					}
					if (answ.getQuestion().getId() == 5) {// La conexión a internet para sus cursos virtuales es?
						if(answ.getAnswer()!=null) {
							row.createCell(10).setCellValue(answ.getAnswer().getText());
						}else {
							row.createCell(9).setCellValue("");
						}
					}
					if (answ.getQuestion().getId() == 8) {// Tiene Alguna Discapacidad?
						row.createCell(11).setCellValue(
								(answ.getAnswer() != null)
										? (answ.getAnswer().getText().contains("especifique"))
												? "Si, " + answ.getCustomizedAnswer()
												: answ.getAnswer().getText()
										: answ.getCustomizedAnswer());
					}
					if (answ.getQuestion().getId() == 9) {// Habla alguna lengua materna originaria?
						row.createCell(12).setCellValue(
								(answ.getAnswer() != null)
										? (answ.getAnswer().getText().contains("especifique"))
												? "Si, " + answ.getCustomizedAnswer()
												: answ.getAnswer().getText()
										: answ.getCustomizedAnswer());
					}
					if (answ.getQuestion().getId() == 10) {// Eres afrodescendiente directo?
						row.createCell(13).setCellValue(answ.getCustomizedAnswer());
					}
				}
				initRow++;
			}
		}

		workbook.write(stream);
		workbook.close();
		LOGGER.info("{} - Termina exportAnswersAll.", userSecurityService.getIdSession());
		return new ByteArrayInputStream(stream.toByteArray());
	}
	
	@Override
	public ByteArrayInputStream exportAnswersBackToSchoolP() throws IOException {
		LOGGER.info("{} - Inicia exportAllTutorships.", userSecurityService.getIdSession());
		List<AnswersUser> list = userAnswerRepository.findAll();
		/* Obteniendo las preguntas para ponerlas de encabezado */
		List<Question> preguntas = questionRepository.findByInquestType(InquestTypeEnum.NUEVO_INGRESO);
		/* Encabezado del archivo de excel */
		String[] columns = { "MATRICULA", "ALUMNO", "P.E.", "GRUPO", 
				preguntas.get(0).getText(),/*Lugar de origen*/
				preguntas.get(1).getText(),/*Domicilio*/
				preguntas.get(2).getText(),/*Comunidad/Ciudad:*/
				preguntas.get(3).getText(),/*Municipio*/
				preguntas.get(4).getText(),/*Estado*/
				preguntas.get(5).getText(),/*¿Lugar donde tomas los cursos virtuales del semestre?*/
				preguntas.get(9).getText(), /*Numero de celular*/
				preguntas.get(14).getText(),//Agregar:Puedes regresar a clases presenciales?
				preguntas.get(15).getText(),//Agregar:Justifica tu respuesta anterior
				preguntas.get(16).getText(), //Agregar:¿Has tenido en tu entorno familiar infectados de coronavirus?
				preguntas.get(17).getText(), //Agregar: Ya te vacuenaste?
				preguntas.get(18).getText(), //Agregar: Te has infectado de coronavirus?
				preguntas.get(19).getText(), //Agregar: ¿Qué te preocupa para el regreso a clases presenciales (Selecciona al menos una opción)?
				preguntas.get(20).getText(), //Agregar: ¿Como te ha afectado la pandemia?
				preguntas.get(21).getText(), //Agregar: Aparte de estudiar, ¿A qué te dedicas?
				preguntas.get(22).getText() //Agregar: ¿Padeces alguna comorbilidad (Coexistencia de dos o más enfermedades en un mismo individuo, generalmente relacionadas)?
				};
		Workbook workbook = new HSSFWorkbook();
		ByteArrayOutputStream stream = new ByteArrayOutputStream();

		Sheet sheet = workbook.createSheet("Concentrado_respuestas");
		Row row = sheet.createRow(0);

		for (int i = 0; i < columns.length; i++) {
			Cell cell = row.createCell(i);
			cell.setCellValue(columns[i]);
		}

		List<User> users = userRepository.findAll();

		int initRow = 1;
		for (User user : users) {
			if (user.getAnswers().size() > 0) {
				row = sheet.createRow(initRow);
				row.createCell(0).setCellValue(user.getStudent().getRegister());
				row.createCell(1).setCellValue(user.getStudent().getName() + " " + user.getStudent().getLastname() + " "
						+ user.getStudent().getSecondLastname());
				row.createCell(2).setCellValue(user.getStudent().getProgram().getStudentPlanName());
				row.createCell(3).setCellValue(
						user.getStudent().getGroup().getName() + " " + user.getStudent().getGroup().getTurn());
				for (AnswersUser answ : user.getAnswers()) {
					if (answ.getQuestion().getId() == 1) {// Lugar de origen:
						row.createCell(4).setCellValue(answ.getCustomizedAnswer());
					}
					if (answ.getQuestion().getId() == 2) {// Domicilio:
						row.createCell(5).setCellValue(answ.getCustomizedAnswer());
					}
					if (answ.getQuestion().getId() == 3) {// Comunidad/Ciudad:
						row.createCell(6).setCellValue(answ.getCustomizedAnswer());
					}
					if (answ.getQuestion().getId() == 4) {// Municipio:
						row.createCell(7).setCellValue(answ.getCustomizedAnswer());
					}
					if (answ.getQuestion().getId() == 5) {// Estado:
						row.createCell(8).setCellValue(answ.getCustomizedAnswer());
					}
					if (answ.getQuestion().getId() == 6) {// Lugar donde tomará los cursos virtuales del próximo semestre?
						row.createCell(9).setCellValue(
						(answ.getAnswer() != null)
						? (answ.getAnswer().getText().contains("indique"))
							? "En, " + answ.getCustomizedAnswer()
							: answ.getAnswer().getText()
						: answ.getCustomizedAnswer());
					}
					if (answ.getQuestion().getId() == 10) {// Numero de celular actual (Nota: en caso de cambiar número de celular, notifique a la dirección).
						row.createCell(10).setCellValue(answ.getCustomizedAnswer());
					}
					if (answ.getQuestion().getId() == 15) {//¿Puedes regresar a clases presenciales?
						row.createCell(11).setCellValue(
								(answ.getAnswer() != null)
								? (answ.getAnswer().getText().contains("especifique"))
										? "Si, " + answ.getCustomizedAnswer()
										: answ.getAnswer().getText()
								: answ.getCustomizedAnswer());
					}
					if (answ.getQuestion().getId() == 16) {//Justifica tu respuesta anterior
						row.createCell(12).setCellValue(answ.getCustomizedAnswer());
					}
					if (answ.getQuestion().getId() == 17) {//¿Has tenido en tu entorno familiar infectados de coronavirus?
						row.createCell(13).setCellValue(
								(answ.getAnswer() != null) ? answ.getAnswer().getText() : answ.getCustomizedAnswer());
					}
					if (answ.getQuestion().getId() == 18) {//¿Ya te vacunaste?
						row.createCell(14).setCellValue(
								(answ.getAnswer() != null)
								? (answ.getAnswer().getText().contains("especifique"))
										? "Si, " + answ.getCustomizedAnswer()
										: answ.getAnswer().getText()
								: answ.getCustomizedAnswer());
					}
					if (answ.getQuestion().getId() == 19) {//¿Te has infectado de coronavirus?
						row.createCell(15).setCellValue(
								(answ.getAnswer() != null)
								? (answ.getAnswer().getText().contains("especifique"))
										? "Si, " + answ.getCustomizedAnswer()
										: answ.getAnswer().getText()
								: answ.getCustomizedAnswer());
					}
					if (answ.getQuestion().getId() == 20) {//¿Qué te preocupa para el regreso a clases presenciales (Selecciona al menos una opción)?
						List<AnswersUser> answers = userAnswerRepository.finByUserAndQuestion(user.getIdUser(), answ.getQuestion().getId());
						if(answers.size()>5) {
							LOGGER.info("ID|USUARIO|RESPUESTAS:{}|{}|{}", user.getIdUser(), user.getUsername(), answers.size());
						}
						if(answers != null) {
						row.createCell(16).setCellValue(answers.stream()
						        .map( n -> n.getAnswer().getText())
						        .collect( Collectors.joining( "|" ) ));
						}
					}
					if (answ.getQuestion().getId() == 21) {//¿Como te ha afectado la pandemia?
						row.createCell(17).setCellValue(answ.getCustomizedAnswer());
					}
					if (answ.getQuestion().getId() == 22) {//Aparte de estudiar, ¿A qué te dedicas?
						row.createCell(18).setCellValue(
								(answ.getAnswer() != null)
										? (answ.getAnswer().getText().contains("especifique"))
												? "Si, " + answ.getCustomizedAnswer()
												: answ.getAnswer().getText()
										: answ.getCustomizedAnswer());
					}
					if (answ.getQuestion().getId() == 23) {//¿Padeces alguna comorbilidad?
						row.createCell(19).setCellValue(
								(answ.getAnswer() != null)
								? (answ.getAnswer().getText().contains("especifique"))
										? "Si, " + answ.getCustomizedAnswer()
										: answ.getAnswer().getText()
								: answ.getCustomizedAnswer());
					}
				}
				initRow++;
			}
		}

		workbook.write(stream);
		workbook.close();
		LOGGER.info("{} - Termina exportAnswersAll.", userSecurityService.getIdSession());
		return new ByteArrayInputStream(stream.toByteArray());
	}

	@Override
	public int totalSurveyAnswered() {
		return userAnswerRepository.findByUserIdUnique().size();
	}

	/**
	 * Retorna la lista de respuestas de una encuesta
	 * @param user Usuario
	 * @return  Lista de respuestas
	 */
	public List<AnswersUser> findByUser(UserDto user){
		return userAnswerRepository.finByUserId(user.getIdUser());
	}
	
	@Override
	public InquestQuestionDto saveAnswer(InquestQuestionDto inqDto) throws ServiceException{
		Inquest inquest = inquestRepository.getOne(inqDto.getIdInquest());
		Question question = questionRepository.getOne(inqDto.getIdq());
		Period period = periodRepository.getOneByActivePeriod(true);
		InquestQuestionDto inqDtoReturn = null;
		
		UserDetails userDetail = userSecurityService.getUserCurrentDetails();
		User user = userService.findUserByUsername(userDetail.getUsername());
		//Verifica que la encuesta no se halla iniciado antes
		UserInquest usrinq = userInquestRepository.findByUserAndInquestAndIdPeriod(user, inquest, period.getId());
		if(usrinq!=null) {
			//LOGGER.info("{} - La encuesta ya se ha iniciado antes:[{}][{}]",userSecurityService.getIdSession(),usrinq.getCurrentQuestionId(), usrinq.getAnsweredDate());
			//Guardando respuesta de la pregunta
			if(!usrinq.getCurrentQuestionId().contains(String.valueOf(inqDto.getIdq()))) {//Evita guardar respuestas duplicadas
				saveAnswers(inqDto, inquest, question, user);
			//Aqui guardare los datos de la nueva tabla activacion

			usrinq.setAnsweredDate(new Date());
			if(inqDto.getCurrentPage() < inqDto.getTotalPages()-1) {
				usrinq.setStatus(InquestStatusEnum.PENDING);
			}else {
				usrinq.setStatus(InquestStatusEnum.ANSWERED);
			}
			usrinq.setUser(user);
			usrinq.setCurrentQuestionId(usrinq.getCurrentQuestionId()+","+String.valueOf(question.getId()));
			usrinq.setIdPeriod(period.getId());
			userInquestRepository.save(usrinq);
			
			//Obteniendo pregunta siguiente
			inqDto.setPage(inqDto.getCurrentPage()+1);
			inqDtoReturn = inquestQuestionService.findPageable(inqDto);
			
			//Revisando si la pregunta siguiente ya ha sido contestada
			if(inqDto.getCurrentPage() < inqDto.getTotalPages()-1) {
				while(questionIsInCurrentQuestionsId(usrinq.getCurrentQuestionId(), inqDtoReturn)) {
					//LOGGER.info("Pregunta ya contestada:{}", inqDtoReturn.getInquestQuestionList().iterator().next().getQuestion().getText());
					inqDto.setPage(inqDto.getCurrentPage()+1);
					inqDtoReturn = inquestQuestionService.findPageable(inqDto);
					inqDtoReturn.setCurrentPage(inqDto.getPage());
					//LOGGER.info("Pregunta siguiente:{}", inqDtoReturn.getInquestQuestionList().iterator().next().getQuestion().getText());
				}
				return inqDtoReturn;
			}else{
				return null;
			}
		}
		}else {
			LOGGER.info("{} - La encuesta no se ha iniciado antes; page:{}",userSecurityService.getIdSession(), inqDto.getCurrentPage());
			if(inqDto.getCurrentPage() < inqDto.getTotalPages()-1 || inqDto.getCurrentPage() == 0) {
				//Guardando respuesta de la pregunta
				saveAnswers(inqDto, inquest, question, user);
				//LOGGER.info("Nombre de la encuesta:{}", inqDto.getMembershipRadios());
					if(inquest.getName().equals("SEGUIMIENTO")){
						//Vamos a poblar la tabla activacion
					Activate activate = new Activate();
					activate.setPeriod(period);
					activate.setUser(user);
					Answer answ = answerRepository.getOne(Long.valueOf(inqDto.getOptionSelected()));
					//LOGGER.info("Valor seleccionado:{}", answ.getText());
					activate.setResponse(answ.getText());
					activate.setRegisterDate(new Date());
					activateRepository.save(activate);
				}

				UserInquest userInquest = new UserInquest();
				userInquest.setAnsweredDate(new Date());
				userInquest.setComments("");
				userInquest.setInquest(inquest);
				if((inqDto.getCurrentPage()+1) < inqDto.getTotalPages()) {
					userInquest.setStatus(InquestStatusEnum.PENDING);
				}else {
					userInquest.setStatus(InquestStatusEnum.ANSWERED);
				}
				userInquest.setUser(user);
				userInquest.setCurrentQuestionId(String.valueOf(question.getId()));
				userInquest.setIdPeriod(period.getId());
				userInquestRepository.save(userInquest);
				
				//Se extrae la página siguiente, dado que ya se mostró la primera
				if((inqDto.getCurrentPage()+1) < inqDto.getTotalPages()) {
					inqDto.setPage(inqDto.getCurrentPage()+1);
				}else {
					//Es solo una pregunta en la encuesta
					return null;
				}
			}else {
				//Llegó al final de la encuesta
				return null;
			}
		}
		inqDtoReturn = inquestQuestionService.findPageable(inqDto);
		return inqDtoReturn;
	}

	private void saveAnswers(InquestQuestionDto inqDto, Inquest inquest, Question question, User user) {
		//LOGGER.info("{} - Guardando respuestas:{}",userSecurityService.getIdSession(), inqDto.getQtype());
		Period period = periodRepository.getOneByActivePeriod(true);
		InquestQuestion inqQ = inquestQuestionRepository.findByInquestAndQuestion(inquest, question);
		AnswersUser userAnsw = new AnswersUser();
		userAnsw.setAnswer(null);
		//LOGGER.info("{} - Order:{}",userSecurityService.getIdSession(), inqQ.getOrderquestion());
		switch(inqDto.getQtype()) {
		case "RATING_0_10":
			//LOGGER.info("Guardando rating:{}",inqDto.getRating());
			userAnsw.setCustomizedAnswer(inqDto.getRating());
			break;
		case "ABIERTA_TEXT":
			//LOGGER.info("Guardando abierta:{}",inqDto.getAnstext());
			userAnsw.setCustomizedAnswer(inqDto.getAnstext());
			break;
		case "ABIERTA_NUM":
			//LOGGER.info("Guardando abierta:{}",inqDto.getAnstext());
			userAnsw.setCustomizedAnswer(inqDto.getAnsnum());
			break;
		case "OPCIONES":
			//LOGGER.info("Guardando respuesta de opción:{}",inqDto.getAnstext());
			//LOGGER.info("OptionSelected:{}",inqDto.getOptionSelected());
			if(inqDto.getAnstext()==null || inqDto.getAnstext().trim().isEmpty()) {
				//Guardar id de respuesta
				if(inqDto.getOptionSelected()!=null || !inqDto.getOptionSelected().trim().isEmpty() || !inqDto.getOptionSelected().trim().equals("#")) {
					Answer answ = answerRepository.getOne(Long.valueOf(inqDto.getOptionSelected()));
					userAnsw.setAnswer(answ);
				}else {
					//El usuario no selecciono nada en el front, se guarda respuesta default|
					for (Answer an : question.getAnswers()) {
						if(an.isChecked()) {
							userAnsw.setAnswer(an);
						}
					}
				}
				
			}else {
				userAnsw.setSpecification(inqDto.getAnstext());
			}
			break;
		case "OPCION":
			//LOGGER.info("Guardando opción:{}",inqDto.getMembershipRadios());
			userAnsw.setCustomizedAnswer(inqDto.getMembershipRadios());
			break;
		case "MULTIPLE":
			//LOGGER.info("Guardando opción multiple:{}",inqDto.getMultselect());
			List<AnswersUser> list = new ArrayList<AnswersUser>();
			String[] arr = inqDto.getMultselect().split("\\|");
			for (String aswId : arr) {
				//LOGGER.info("ARR:{}", aswId);
				if(!aswId.trim().isEmpty()) {
					AnswersUser usrAnsw = new AnswersUser();
					usrAnsw.setAnswer(answerRepository.getOne(Long.valueOf(aswId)));
					usrAnsw.setRegisterDate(new Date());
					usrAnsw.setInquest(inquest);
					usrAnsw.setQuestion(question);
					usrAnsw.setUser(user);
					usrAnsw.setOrderansw(inqQ.getOrderquestion());
					usrAnsw.setIdPeriod(period.getId());
					usrAnsw.setIdPeriod(period.getId());
					list.add(usrAnsw);
				}
			}
			userAnswerRepository.saveAll(list);
			break;
		}
		
		if(!inqDto.getQtype().equals("MULTIPLE")) {
			userAnsw.setRegisterDate(new Date());
			userAnsw.setInquest(inquest);
			userAnsw.setQuestion(question);
			userAnsw.setUser(user);
			userAnsw.setOrderansw(inqQ.getOrderquestion());
			userAnsw.setIdPeriod(period.getId());
			userAnswerRepository.save(userAnsw);
		}
	}

	private boolean questionIsInCurrentQuestionsId(String currentQuestionId, InquestQuestionDto dto) {
		//LOGGER.info("Buscando pregunta {} en las ya contestadas [{}].", dto.getInquestQuestionList().iterator().next().getQuestion().getId(), currentQuestionId);
		String[] idsQ = currentQuestionId.split("\\,");
		for (String idq : idsQ) {
			if(String.valueOf(dto.getInquestQuestionList().iterator().next().getQuestion().getId()).equals(idq)) {
				//LOGGER.info("Pregunta ya contestada, return true.");
				return true;
			}
		}
		//LOGGER.info("Pregunta no contestada, return false.");
		return false;
	}

	@Override
	public boolean verifySaveAnswers(InquestQuestionDto dto) {
		Inquest inquest = inquestRepository.getOne(dto.getIdInquest());
		Question question = questionRepository.getOne(dto.getIdq());
		UserDetails userDetail = userSecurityService.getUserCurrentDetails();
		User user = userService.findUserByUsername(userDetail.getUsername());
		List<AnswersUser> arr = userAnswerRepository.findByQuestionAndUserAndInquest(question, user, inquest);
		//LOGGER.info("ARR.size:{}", arr.size());
		return arr.size() > 0;
	}

	@Override
	public Resource export(long idInquest) throws ServiceException{
		Period period = periodRepository.getOneByActivePeriod(true);
		Inquest inquest = inquestRepository.getOne(idInquest);
		LOGGER.info("{} - Inicia exportAnswers, encuesta:{}", userSecurityService.getIdSession(), inquest.getName());
		List<InquestQuestion> listQuestions = inquestQuestionRepository.findByInquestOrderByOrderquestion(inquest);
		String[] headersName = new String[listQuestions.size()+4];
		Workbook workbook = new HSSFWorkbook();
		Sheet sheet = workbook.createSheet(inquest.getName());
		Row row = sheet.createRow(0);
		Cell cell = row.createCell(0);
		cell.setCellValue("Matrícula");

		cell = row.createCell(1);
		cell.setCellValue("Alumno");

		cell = row.createCell(2);
		cell.setCellValue("Grupo");

		cell = row.createCell(3);
		cell.setCellValue("P.E.");

		int j =4;
		for(InquestQuestion iq : listQuestions) {
			//LOGGER.info("Question:{}", iq.getQuestion().getText());
			cell = row.createCell(j);
			cell.setCellValue(iq.getQuestion().getText());
			j++;
		}

		List<AnswersUser> list = userAnswerRepository.findByInquestOrderByUserAscOrderansw(inquest);
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		Set<User> users = getUsersFromUsersAnswersList(list);
		int initRow = 1;
		for (User user : users) {
			row = sheet.createRow(initRow);
			row.createCell(0).setCellValue(user.getUsername());
			row.createCell(1).setCellValue((user.getStudent() != null) ? user.getStudent().getName() + " " + user.getStudent().getLastname() + " " + user.getStudent().getSecondLastname() : "");
			row.createCell(2).setCellValue(user.getStudent().getGroup().getName()+" "+user.getStudent().getGroup().getTurn());
			row.createCell(3).setCellValue(user.getStudent().getProgram().getStudentPlanName());
			int i = 4;
			for(InquestQuestion q : inquest.getInquestQuestions()){
				row.createCell(i).setCellValue(getAnswers(user, q.getQuestion(), list));
				i++;
			}
			initRow++;
		}
		
		byte[] bytes = null;
		try {
			workbook.write(stream);
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			try {
			    workbook.write(bos);
			} finally {
			    bos.close();
				workbook.close();
				stream.close();
			}
			bytes = bos.toByteArray();
		} catch (IOException e) {
			LOGGER.error("Error al escribir reporte de encuesta:{}", e.getMessage());
		}
		LOGGER.info("{} - Termina exportAnswers, encuesta:{}", userSecurityService.getIdSession(), inquest.getName());
		return new ByteArrayResource(bytes, "Reporte de encuesta "+inquest.getName());
	}
	
	private Set<User> getUsersFromUsersAnswersList(List<AnswersUser> list) {
		Set<User> users = new HashSet<>();
		List<User> usersDB = userRepository.findAll();
		
		list.stream().forEach(p ->{
			usersDB.forEach(u -> {
				if(p.getUser().getUsername().equals(u.getUsername())) {
					users.add(u);
				}
			});
		});
		return users;
	}
	
	/**
	 * Obtiene la respuesta de una pregunta, de un usuario, de la lista de AnswersUser
	 * @param user Usuario que contestó la pregunta
	 * @param question Pregunta
	 * @param list Lista de AnswersUser donde se buscará la respuesta
	 * @return Respuesta del usuario a la pregunta buscada
	 */
	private String getAnswers(User user, Question question, List<AnswersUser> list) {
		StringBuilder concat = new StringBuilder();
		StringBuilder resp = new StringBuilder();
		list.forEach(q -> {
			if(q.getUser().getUsername().equals(user.getUsername()) && q.getQuestion().getId() == question.getId()) {
				if (q.getAnswer() != null) {
					if(q.getQuestion().getQuestionType().toString().equals("MULTIPLE")) {
						concat.append(q.getAnswer().getText()+",");
					}else {
						resp.append(q.getAnswer().getText());
					}
				} else {
					if(q.getCustomizedAnswer()== null || q.getCustomizedAnswer().isEmpty()){
						resp.append(q.getSpecification());
					}else {
						resp.append(q.getCustomizedAnswer());
					}
				}
			}
		});
		return (concat.length()>0)?concat.toString():(resp.length()>0)?resp.toString():"";
	}
}
