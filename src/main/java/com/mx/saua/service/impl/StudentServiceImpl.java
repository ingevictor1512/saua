package com.mx.saua.service.impl;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.lowagie.text.DocumentException;
import com.lowagie.text.pdf.PdfCopyFields;
import com.lowagie.text.pdf.PdfReader;
import com.mx.saua.constant.FormatsConstant;
import com.mx.saua.converter.GroupConverter;
import com.mx.saua.converter.PeriodConverter;
import com.mx.saua.converter.ProgramConverter;
import com.mx.saua.converter.StudentConverter;
import com.mx.saua.converter.StudentStatusConverter;
import com.mx.saua.converter.TeacherConverter;
import com.mx.saua.converter.UapConverter;
import com.mx.saua.converter.UserConverter;
import com.mx.saua.dto.GroupDto;
import com.mx.saua.dto.IncomeDto;
import com.mx.saua.dto.OfferedClassDto;
import com.mx.saua.dto.ProgramDto;
import com.mx.saua.dto.StudentChargeDto;
import com.mx.saua.dto.StudentDto;
import com.mx.saua.dto.StudentResponseDto;
import com.mx.saua.dto.StudentStatusDto;
import com.mx.saua.dto.UserDto;
import com.mx.saua.entity.AnswersUserTeacherEval;
import com.mx.saua.entity.Applicant;
import com.mx.saua.entity.ApplicantCoffeeShop;
import com.mx.saua.entity.Authority;
import com.mx.saua.entity.Configuration;
import com.mx.saua.entity.EntryRegister;
import com.mx.saua.entity.Group;
import com.mx.saua.entity.Guest;
import com.mx.saua.entity.Income;
import com.mx.saua.entity.InfoEvaluation;
import com.mx.saua.entity.LoadAuthorization;
import com.mx.saua.entity.OfferedClass;
import com.mx.saua.entity.Period;
import com.mx.saua.entity.Program;
import com.mx.saua.entity.Schedule;
import com.mx.saua.entity.Student;
import com.mx.saua.entity.StudentCharge;
import com.mx.saua.entity.StudentStatus;
import com.mx.saua.entity.Tutorship;
import com.mx.saua.entity.User;
import com.mx.saua.entity.UserNotification;
import com.mx.saua.enums.AuthorityEnum;
import com.mx.saua.enums.ConfigurationEnum;
import com.mx.saua.enums.DayEnum;
import com.mx.saua.enums.NotificationStatusEnum;
import com.mx.saua.enums.NotificationTypeEnum;
import com.mx.saua.enums.StudyTypeEnum;
import com.mx.saua.exception.ServiceException;
import com.mx.saua.repository.AnswerUserTeacherEvalRepository;
import com.mx.saua.repository.ApplicantCoffeeShopRepository;
import com.mx.saua.repository.ApplicantRepository;
import com.mx.saua.repository.ConfigurationRepository;
import com.mx.saua.repository.EntryRegisterRepository;
import com.mx.saua.repository.GroupRepository;
import com.mx.saua.repository.GuestRepository;
import com.mx.saua.repository.IncomeRepository;
import com.mx.saua.repository.InfoEvaluationRepository;
import com.mx.saua.repository.LoadAuthorizationRepository;
import com.mx.saua.repository.OfferedClassRepository;
import com.mx.saua.repository.PeriodRepository;
import com.mx.saua.repository.ProgramRepository;
import com.mx.saua.repository.ScheduleRepository;
import com.mx.saua.repository.StudentChargeRepository;
import com.mx.saua.repository.StudentRepository;
import com.mx.saua.repository.StudentStatusRepository;
import com.mx.saua.repository.TutorshipRepository;
import com.mx.saua.repository.UserNotificationRepository;
import com.mx.saua.repository.UserRepository;
import com.mx.saua.service.EntryRegisterService;
import com.mx.saua.service.StudentService;
import com.mx.saua.service.UserService;
import com.mx.saua.util.FilesUtil;
import com.mx.saua.util.PageableUtils;
import com.mx.saua.util.SecurityUtils;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;

/**
 * Service que maneja la lógica de negocio para los alumnos
 * 
 * @author Benito Morales
 * @version 1.0
 *
 */
@Service("studentService")
public class StudentServiceImpl implements StudentService {

	/**
	 * Logger de la clase
	 */
	private Logger LOGGER = LoggerFactory.getLogger(StudentServiceImpl.class);
	
	/**
	 * Inyeccion de studentRepository
	 */
	@Autowired
	private StudentRepository studentRepository;

	/**
	 * Inyeccion de studentChargeRepository
	 */
	@Autowired
	private StudentChargeRepository studentChargeRepository;

	/**
	 * Inyeccion de userRepository
	 */
	@Autowired
	private UserRepository userRepository;

	/**
	 * Inyeccion de tutorshipRepository
	 */
	@Autowired
	private TutorshipRepository tutorshipRepository;

	/**
	 * Inyeccion de studentConverter
	 */
	@Autowired
	private StudentConverter studentConverter;

	/**
	 * Inyeccion de userConverter
	 */
	@Autowired
	private UserConverter userConverter;

	/**
	 * Inyeccion de groupConverter
	 */
	@Autowired
	private GroupConverter groupConverter;

	/**
	 * Inyeccion de studentStatusConverter
	 */
	@Autowired
	private StudentStatusConverter studentStatusConverter;

	/**
	 * Inyeccion de programConverter
	 */
	@Autowired
	private ProgramConverter programConverter;

	/**
	 * Inyeccion de periodConverter
	 */
	@Autowired
	private PeriodConverter periodConverter;

	/**
	 * Inyeccion de teacherConverter
	 */
	@Autowired
	private TeacherConverter teacherConverter;

	/**
	 * Inyeccion de uapConverter
	 */
	@Autowired
	private UapConverter uapConverter;

	/**
	 * Inyeccion de passwordEncoder
	 */
	@Autowired
	private PasswordEncoder passwordEncoder;

	/**
	 * Inyeccion de userNotificationRepository
	 */
	@Autowired
	private UserNotificationRepository userNotificationRepository;

	/**
	 * Inyeccion de studentStatusRepository
	 */
	@Autowired
	private StudentStatusRepository studentStatusRepository;

	/**
	 * Inyeccion de incomeRepository
	 */
	@Autowired
	private IncomeRepository incomeRepository;

	/**
	 * Inyeccion de groupRepository
	 */
	@Autowired
	private GroupRepository groupRepository;

	/**
	 * Inyeccion de programRepository
	 */
	@Autowired
	private ProgramRepository programRepository;

	/**
	 * Inyeccion de offeredClassRepository
	 */
	@Autowired
	private OfferedClassRepository offeredClassRepository;
	
	/**
	 * Inyeccion de loadAuthorizationRepository
	 */
	@Autowired
	private LoadAuthorizationRepository loadAuthorizationRepository;

	/**
	 * Inyeccion de userSecurityService
	 */
	@Autowired
	private UserSecurityService userSecurityService;
	
	@Autowired
	private UserService userService;
	
	/**
	 * Inyeccion de configurationRepository
	 */
	@Autowired
	private ConfigurationRepository configurationRepository;
	
	@Autowired
	private PeriodRepository periodRepository;
	
	@Autowired
	private InfoEvaluationRepository infoEvaluationRepository;

	@Autowired
	private ScheduleRepository scheduleRepository;
	
	@Autowired
	private AnswerUserTeacherEvalRepository answerUserTeacherEvalRepository;
	
	@Autowired
	private ApplicantRepository applicantRepository;
	
	@Autowired
	private EntryRegisterRepository entryRegisterRepository;
	
	@Autowired
	private ApplicantCoffeeShopRepository applicantCoffeeShopRepository;
	
	@Autowired
	private GuestRepository guestRepository;
	
	@Override
	public StudentDto find(StudentDto studentDto) {
		List<Student> students = studentRepository.findStudentsByStatus(1L);
		List<StudentDto> studentsDto = new ArrayList<>();
		students.stream().forEach(p -> {
			List<StudentCharge> chargeAux = studentChargeRepository.findStudentChargeByStudentRegisterAndPeriodId(p.getRegister(), 1);
			StudentDto studentDtoTemp = new StudentDto();
			//studentDtoTemp = studentConverter.convertStudent2StudentDto(p);
			studentDtoTemp.setRegister(p.getRegister());
			studentDtoTemp.setName(p.getName());
			studentDtoTemp.setLastname(p.getLastname());
			studentDtoTemp.setSecondLastname(p.getSecondLastname());
			
			if(p.getGroup() != null) {
				GroupDto dto = new GroupDto();
				dto.setId(p.getGroup().getId());
				dto.setName(p.getGroup().getName());
				dto.setAbbreviation(p.getGroup().getAbbreviation());
				dto.setEducationalProgramDto(programConverter.convertProgram2ProgramDto(p.getProgram()));
				dto.setTurn(p.getGroup().getTurn());
				studentDtoTemp.setGroupDto(dto);
			}else {
				studentDtoTemp.setGroupDto(new GroupDto());
			}
			
			if(p.getUsers()!= null &&  p.getUsers().iterator().hasNext()) {
				studentDtoTemp.setEmail(p.getUsers().iterator().next().getEmail());
			}
			studentDtoTemp.setTotalAutoEval(p.getTotalAutoEval());
			studentDtoTemp.setTotalTeacherEval(p.getTotalTeacherEval());
			studentDtoTemp.setTotalChargePreviousPeriod(chargeAux.size());
			
			ProgramDto programDto = new ProgramDto();
			programDto.setStudentPlanKey(p.getProgram().getStudentPlanKey());
			programDto.setStudentPlanName(p.getProgram().getStudentPlanName());
			programDto.setAbr(p.getProgram().getAbr());
			studentDtoTemp.setProgramDto(programDto);
			
			studentsDto.add(studentDtoTemp);
		});
		studentDto.setStudents(studentsDto);
		List<GroupDto> groupsDto = new ArrayList<>();
		List<Group> groups = groupRepository.findByPeriodActivePeriodAdmin(true);
		groups.stream().forEach(p -> {
			studentDto.setGroupDto(groupConverter.convertGroup2GroupDto(p));
		});
		studentDto.setGroupsDto(groupsDto);
		return studentDto;
	}
	
	@Override
	public StudentDto findPageable(StudentDto studentDto) {
		/*LOGGER.info("SearchByValue:{}", studentDto.getSearchByValue());
		LOGGER.info("SearchBy:{}", studentDto.getSearchBy());
		LOGGER.info("Page:{}", studentDto.getPage());*/
		
		String dirStr = PageableUtils.getSortDirection(studentDto);
		int size = 10;
		
		/*if(!studentDto.getSearchByValue().trim().isEmpty()) {
			studentDto.setPage(0);
		}
		*/
		
		studentDto.setPage(studentDto.getPage()-1);
		
		Pageable pageable = PageableUtils.buildPageable(studentDto, size, "register");
		Page<Student> page = null;
		if (studentDto.getSearchBy().isEmpty()) {
//		Student qExample = new Student();
//		qExample.setStatus(true);
//		qExample.setText(form.getText());
//		Example<Student> example = Example.of(qExample);
		page = studentRepository.findAll(pageable);
		} else {
			switch(studentDto.getSearchBy()) {
			case "Matricula":
				page = studentRepository.findStundentByRegister(studentDto.getSearchByValue(), pageable);
				break;
			case "Nombre":
				page = studentRepository.findStundentByName(studentDto.getSearchByValue(), pageable);
				break;
			case "Grupo":
				page = studentRepository.findStundentByGroup(studentDto.getSearchByValue(), pageable);
				break;
			case "E-mail":
				page = studentRepository.findStundentByEmail(studentDto.getSearchByValue(), pageable);
				break;
			case "Tipo de ingreso":
				page = studentRepository.findByIncomeDescription(studentDto.getSearchByValue(), pageable);
				break;
			}
		}
		
		StudentDto result = new StudentDto();
		result.setStudentList(page.getContent());
		result.setDirection(dirStr);
		result.setPage(page.getNumber());
		result.setSize(page.getSize());
		result.setTotalPages(page.getTotalPages());
		result.setTotalItems(page.getTotalElements());
		result.setSearchBy(studentDto.getSearchBy());
		result.setSearchByValue(studentDto.getSearchByValue());
		return result;
	}

	@Override
	public void save(StudentDto studentDto) throws ServiceException {
		Date current = new Date();
		if (studentDto.isNewRegister()) {
			LOGGER.info("{} - Inicia guardado de nuevo alumno.", userSecurityService.getIdSession());
			try {
				// Nuevo estudiante
				studentDto.setEditDate(current);
				studentDto.setRegisterDate(current);

				studentDto.setStudentStatusDto(studentStatusConverter.convertStudentStatus2StudentStatusDto(
						studentStatusRepository.getOne(studentDto.getStudentStatusDto().getId())));
				LOGGER.info("{} - valor recuperado con el id del frontal.", studentDto.getStudentStatusDto().toString());
				User user = new User();
				String password = SecurityUtils.generateRandomPassword();
				user.setUsername(studentDto.getRegister());
				user.setPassword(passwordEncoder.encode(password));

				Student student = studentConverter.convertStudentDto2Student(studentDto);
				student.setGroup(groupRepository.getOne(studentDto.getGroupDto().getId()));

				Program program = new Program();
				program = programRepository.getOne(student.getGroup().getEducationalProgram().getStudentPlanKey());
				student.setProgram(program);
				user.setStudent(student);

				user.setActive(true);
				Set<Authority> authorities = new HashSet<>();
				Authority auth = new Authority();
				auth.setAuthority(AuthorityEnum.STUDENT.toString());
				auth.setRegisterDate(current);
				auth.setEditDate(current);
				auth.setUser(user);
				authorities.add(auth);
				user.setAuthorities(authorities);
				user.setEmail(studentDto.getEmail());
				user.setFirstLogin(true);
				user.setInquest(true);
				user.setLocked(false);
				user.setRegisterDate(current);
				user.setEditdate(current);
				user.setStatus(true);
				user.setPhone(studentDto.getPhone());
				userRepository.save(user);

				User userForNotification = userRepository.findOneByEmailAndUsername(user.getEmail(),
						user.getUsername());
				// Guardar notificacion
				UserNotification userNotification = new UserNotification();
				userNotification.setAddresseeMail(user.getEmail());
				userNotification.setAddresseeName(user.getStudent().getName());
				userNotification.setUser(userForNotification);
				userNotification.setPlainPassword(password);
				userNotification.setReviewable(false);
				userNotification.setStatus(NotificationStatusEnum.REGISTRADA);
				userNotification.setType(NotificationTypeEnum.REGISTER);
				userNotificationRepository.save(userNotification);
				LOGGER.info("{} - Termina guardado de nuevo alumno.", userSecurityService.getIdSession());
			} catch (Exception ex) {
				LOGGER.info("{} - Error:{}.", userSecurityService.getIdSession(), ex.getMessage());
				if (ex.getMessage() != null && ex.getMessage().toString().contains("PRIMARY")) {
					throw new ServiceException("Esta matr\u00EDcula ya se encuentra en uso.");
				} else {
					throw new ServiceException("Ocurri\u00F3 un error.");
				}
			}
		} else {
			LOGGER.info("{} - Inicia edicion de alumno.", userSecurityService.getIdSession());
			Student student = studentRepository.getOne(studentDto.getRegister());
			studentDto.setEditDate(current);
			studentDto.setRegisterDate(student.getRegisterDate());
			User us = student.getUsers().iterator().next();
			us.setEmail(studentDto.getEmail());
			us.setPhone(studentDto.getPhone());
			studentDto.setUserDto(userConverter.convertUser2UserDto(us));
			/*studentDto.setStudentStatusDto(studentStatusConverter.convertStudentStatus2StudentStatusDto(
					studentStatusRepository.getOne(student.getStudentStatus().getId())));*/
			studentRepository.save(studentConverter.convertStudentDto2Student(studentDto));
			LOGGER.info("{} - Termina edicion de alumno.", userSecurityService.getIdSession());
		}
	}

	@Override
	public StudentDto getStudentByRegister(String register) throws ServiceException {
		//LOGGER.info("{} - Inicia obtener alumno[{}].", userSecurityService.getIdSession(), register);
		Student student = studentRepository.getOne(register);
		//LOGGER.info("{} - Termina obtener alumno[{}].", userSecurityService.getIdSession(), register);
		return studentConverter.convertStudent2StudentDto(student);
		
		
	}

	@Override
	public void delete(StudentDto studentDto) throws ServiceException {
		LOGGER.info("{} - Inicia eliminar alumno.", userSecurityService.getIdSession());
		try {
			Student student = studentRepository.getOne(studentDto.getRegister());
			userRepository.delete(userRepository.getOne(student.getUsers().iterator().next().getIdUser()));
			LOGGER.info("{} - Termina eliminar alumno.", userSecurityService.getIdSession());
		}catch(Exception ex){
			LOGGER.info("{} - Termina eliminar alumno.", userSecurityService.getIdSession());
			LOGGER.error(ex.getMessage());
			if(ex.getMessage().contains("constraint")) {
				throw new ServiceException("El alumno que intenta eliminar, tiene dependencias con otras tablas..");
			}
			throw new ServiceException("Ocurri\u00F3 un error al eliminar el alumno.");
		}
	}

	@Override
	public StudentDto prepareDto(StudentDto studentDto, String option) {
		LOGGER.info("{} - Inicia prepareDto.", userSecurityService.getIdSession());
		List<Income> incomes = incomeRepository.findAll();
		List<Group> groups = groupRepository.findByPeriodId(periodRepository.getOneByActivePeriod(true).getId());
		List<Student> students = studentRepository.findAll();
		List<Program> programs = programRepository.findAll();
		List<StudentStatus> studentStatuses = studentStatusRepository.findAll();

		List<IncomeDto> incomesDto = new ArrayList<>();
		List<GroupDto> groupsDto = new ArrayList<>();
		List<StudentDto> studentsDto = new ArrayList<>();
		List<ProgramDto> programsDto = new ArrayList<>();
		List<StudentStatusDto> studentStatusDtos = new ArrayList<>();

		for (Income income : incomes) {
			incomesDto.add(new IncomeDto(income.getId(), income.getDescription()));
		}
		for (StudentStatus status : studentStatuses) {
			studentStatusDtos.add(new StudentStatusDto(status.getId(), status.getDescription(),status.getComments()));
		}
		if ("CHARGE".equals(option)) {
			groupsDto.add(new GroupDto(-1L, "No disponibles", null, null, null, null, null, null, null, null, null,
					null, null, ""));
			studentDto.setGroupsDto(groupsDto);
		} else {
			for (Group group : groups) {
				groupsDto.add(groupConverter.convertGroup2GroupDto(group));
			}
			studentDto.setGroupsDto(groupsDto);
		}

		if(!"EDIT".equals(option)) {
			for (Student student : students) {
				studentsDto.add(studentConverter.convertStudent2StudentDto(student));
			}
		}

		programsDto.add(new ProgramDto("SN", "Seleccione", null, null, null, null, null, null, null, false, false));
		for (Program program : programs) {
			programsDto.add(programConverter.convertProgram2ProgramDto(program));
		}

		studentDto.setProgramsDto(programsDto);
		studentDto.setIncomes(incomesDto);
		studentDto.setStudents(studentsDto);
		studentDto.setStudentStatusDtos(studentStatusDtos);
		LOGGER.info("{} - Termina prepareDto.", userSecurityService.getIdSession());
		return studentDto;
	}

	/**
	 * Método que guarda la carga acad\u00E9mica de un alumno REGLAS DE NEGOCIO: 
	 * 1.- Maximo 8 materias por alumno 2.- La UAP a agregar debe estar ofertada 3.- La
	 * clase donde se encuentra la UAP no debe estar llena 4.- La UAP no debe
	 * existir en la carga acad\u00E9mica del alumno
	 */
	@Override
	public void saveCharge(StudentChargeDto studentChargeDto) throws ServiceException {
		LOGGER.info("{} - Inicia guardado de carga academica.", userSecurityService.getIdSession());
		UserDto dto = userService.findByUsername(userSecurityService.getUserCurrentDetails().getUsername());
		
		Date current = new Date();
		Student student = studentRepository.getOne(studentChargeDto.getStudentRegister());
		if (student == null) {
			LOGGER.info("{} - El alumno no existe.", userSecurityService.getIdSession());
			throw new ServiceException("El alumno no existe.");
		}
		/* Validando si el alumno ya tiene 8 materias */
		List<StudentCharge> currentCharges = studentChargeRepository.findStudentChargeByRegister(student.getRegister(),
				true);
		//Validación total de materias
		if (!dto.getAuthority().equals(AuthorityEnum.ADMIN.toString()) && currentCharges.size() >= 8) {
			LOGGER.info("{} - El alumno tiene maximo de materias.", userSecurityService.getIdSession());
			throw new ServiceException(
					"Ya tienes el m\u00E1ximo de Uaps por semestre, acercate a la administraci\u00F3n para revisar tu caso.");
		}
		
		OfferedClass ocr = offeredClassRepository.findByUapKeyAndGroupId(studentChargeDto.getIdUap(),
				Long.valueOf(studentChargeDto.getIdGroup()));
		if (ocr == null) {
			LOGGER.info("{} - La UAP no existe o no esta ofertada.", userSecurityService.getIdSession());
			throw new ServiceException("La UAP no existe o no esta ofertada.");
		}

		if (!dto.getAuthority().equals(AuthorityEnum.ADMIN.toString()) && ocr.getUapCharge() >= ocr.getMaxCharge()) {
			LOGGER.info("{} - La clase a agregar se encuentra llena.", userSecurityService.getIdSession());
			throw new ServiceException("La clase a agregar se encuentra llena.");
		}

		List<StudentCharge> coincidences = studentChargeRepository
				.findStudentChargeByOfferedClassUapKeyAndRegister(ocr.getUap().getKey(), student.getRegister(), true);
		
		if (coincidences.size() > 0) {
			LOGGER.info("{} - UAP actualmente en carga del alumno.", userSecurityService.getIdSession());
			throw new ServiceException("La UAP a que intenta agregar ya se encuentra en su carga acad\u00E9mica.");
		}
			
		if (!ocr.getUap().getCommon_code().equals("0")) {
			List<StudentCharge> common = studentChargeRepository.findStudentChargeByOfferedClassUapCommon_code(
					ocr.getUap().getCommon_code(), student.getRegister(), true);
			if (common.size() > 0) {
				LOGGER.info("{} - Ya existe una UAP en comun.", userSecurityService.getIdSession());
				throw new ServiceException("Ya existe una UAP en com\u00FAn.");
			}
		} else {
			/*
			 * Si La uap pertenece al plan del alumno cargarsela, sino decirle que no es
			 * flexible
			 */
			if (student.getProgram().getStudentPlanKey() != ocr.getUap().getEducationalProgram().getStudentPlanKey()) {
				LOGGER.info("{} - La Uap seleccionada no es flexible con tu plan de estudios.", userSecurityService.getIdSession());
				throw new ServiceException("La Uap seleccionada no es flexible con tu plan de estudios.");
			}

		}

		/* Falta agregar si la uap es flexible y si ya tiene mas de 8 materias */
		/*
		 * Si tiene codigo 0, validar si la uap es del plan del alumno, si es asi,
		 * cargarsela, caso contrario no es flexible
		 */
		ocr.setUapCharge(ocr.getUapCharge() + 1);

		StudentCharge studentCharge = new StudentCharge();
		studentCharge.setAuthorizationKey("");
		studentCharge.setComun(ocr.getUap().getCommon_code());
		studentCharge.setEditDate(current);
		studentCharge.setOfferedClass(ocr);
		studentCharge.setPeriod(ocr.getGroup().getPeriod());
		studentCharge.setRegisterDate(current);
		studentCharge.setStudent(student);
		studentCharge.setStudyType(studentChargeDto.getStudyType());
		try {
			studentChargeRepository.save(studentCharge);
			LOGGER.info("{} - Termina guardado de carga academica.", userSecurityService.getIdSession());
		} catch (Exception ex) {
			throw new ServiceException("Ocurri\u00F3 un error al guardar la clase seleccionada.");
		}
	}

	@Override
	public void addToCharge(StudentChargeDto studentChargeDto) throws ServiceException {
		LOGGER.info("{} - Inicia agregar a carga academica.", userSecurityService.getIdSession());
		try {
			Long.valueOf(studentChargeDto.getIdGroup());
		}catch(Exception ex) {
			LOGGER.error("{} - Error:{}.", userSecurityService.getIdSession(), ex.getMessage());
			throw new ServiceException("Ocurri\u00F3 un error.");
		}
		
		Date current = new Date();
		Student student = studentRepository.getOne(studentChargeDto.getStudentRegister());

		if (student == null){
			LOGGER.info("{} - El alumno no existe.", userSecurityService.getIdSession());
			throw new ServiceException("El alumno no existe.");
		}

		List<StudentCharge> coincidences = studentChargeRepository
				.findStudentChargeByOfferedClassUapKeyAndRegister(studentChargeDto.getUapRegister(), studentChargeDto.getStudentRegister(), true);
		LOGGER.info("{} - Coincidencias:{}", userSecurityService.getIdSession(),coincidences.toString());
		
		if(coincidences.size()>0) {
			LOGGER.info("{} - Esta uap ya se encuentra en su carga academica.", userSecurityService.getIdSession());
			throw new ServiceException("Esta uap ya se encuentra en su carga acad\u00E9mica.");
		}
		
		List<OfferedClass> listOfferedClass = offeredClassRepository
				.findByGroupRequired(Long.valueOf(studentChargeDto.getIdGroup()));
		List<StudentCharge> listStudenCharge = new ArrayList<>();
		listOfferedClass.stream().forEach(p -> {
			StudentCharge studentCharge = new StudentCharge();
			studentCharge.setAuthorizationKey("");
			studentCharge.setComun(p.getUap().getCommon_code());
			studentCharge.setEditDate(current);
			studentCharge.setOfferedClass(p);
			studentCharge.setPeriod(p.getGroup().getPeriod());
			studentCharge.setRegisterDate(current);
			studentCharge.setStudent(student);
			studentCharge.setStudyType(StudyTypeEnum.STUDY);
			listStudenCharge.add(studentCharge);
			p.setUapCharge(p.getUapCharge() + 1);
		});

		try {
			studentChargeRepository.saveAll(listStudenCharge);
			LOGGER.info("{} - Termina agregar a carga academica.", userSecurityService.getIdSession());
		} catch (Exception ex) {
			throw new ServiceException("Ocurri\u00F3 un error al asignar alumno al grupo seleccionado.");
		}
	}

	@Override
	public StudentChargeDto findCharge(StudentChargeDto studentChargeDto) {
		LOGGER.info("{} - Inicia buscar carga academica.", userSecurityService.getIdSession());
		List<StudentCharge> charge = studentChargeRepository.findAll();
		List<StudentChargeDto> dtos = new ArrayList<>();
		charge.stream().forEach(p -> {
			dtos.add(new StudentChargeDto(p.getId(), studentConverter.convertStudent2StudentDto(p.getStudent()),
					periodConverter.convertPeriod2PeriodDto(p.getPeriod()),
					new OfferedClassDto(p.getOfferedClass().getId(),
							periodConverter.convertPeriod2PeriodDto(p.getOfferedClass().getPeriod()),
							teacherConverter.convertTeacher2TeacherDto(p.getOfferedClass().getTeacher()),
							p.getOfferedClass().getUapCharge(), p.getOfferedClass().getSemester(),
							p.getOfferedClass().getMaxCharge(), p.getOfferedClass().getMinCharge(),
							uapConverter.convertUap2UapDto(p.getOfferedClass().getUap()),
							groupConverter.convertGroup2GroupDto(p.getOfferedClass().getGroup()), null, 0L, 0L),
					p.getRegisterDate(), p.getAuthorizationKey(), p.getComun(), p.getEditDate(), null,
					p.getStudyType(), null));
		});
		studentChargeDto.setStudentCharges(dtos);
		LOGGER.info("{} - Termina agregar a carga academica.", userSecurityService.getIdSession());
		return studentChargeDto;
	}

	@Override
	public void delete(long idCharge) throws ServiceException {
		LOGGER.info("{} - Inicia eliminar carga academica[{}].", userSecurityService.getIdSession(), idCharge);
		StudentCharge sc = studentChargeRepository.getOne(idCharge);
		
		//Revisar si la carga academica ya se encuentra autorizada
		LoadAuthorization auth = loadAuthorizationRepository.findByStudentAndPeriod(sc.getStudent().getRegister(), true);
		/*
		if(auth != null) {
			LOGGER.info("{} - La autorizacion es diferente a null.", userSecurityService.getIdSession());
			if(auth.getId() > 0) {
				throw new ServiceException("No se puede eliminar una UAP, cuando la carga acad\u00F3mica ha sido autorizada.");
			}
		} */
		LOGGER.info("{} - OfferedClass.id:{}", userSecurityService.getIdSession() ,sc.getOfferedClass().getId());
		LOGGER.info("{} - OfferedClass.UapCharge:{}", userSecurityService.getIdSession() ,sc.getOfferedClass().getUapCharge());

		String usr = userSecurityService.getUserCurrentDetails().getUsername();
		User usrActual = userService.findUserByUsername(usr);
		//usrActual.getAuthorities().iterator().next().getAuthority();
		/**
		 * Revisar que la clase ofertada tenga menos de 15 alumnos 
		 */
		if(usrActual.getAuthorities().iterator().next().getAuthority().equals("STUDENT")){
		if(sc.getOfferedClass().getUapCharge() <= 15) {
			LOGGER.info("{} - La materia tiene el minimo requerido:{}", userSecurityService.getIdSession() ,sc.getOfferedClass().getId());
			throw new ServiceException("No puedes eliminar la UAP por que ya tiene el cupo mínimo, acude a la subdirecci\u00F3n para más informaci\u00F3n");
		}
		}
		/**
		 * Revisar que la carga que se esta tratando de eliminar, no tenga encuestas contestadas
		 * 
		 */
		LOGGER.info("CARGA_ALUMNO_ID:{}",sc.getId());
		studentChargeRepository.delete(sc);
		
		/* Actualizamos el contador de cargauap de la clase eliminada */
		LOGGER.info("{} - Actualizando uapCharge. Valor actual:{}", userSecurityService.getIdSession(), sc.getOfferedClass().getUapCharge());
		OfferedClass offeredClass = new OfferedClass();
		BeanUtils.copyProperties(sc.getOfferedClass(), offeredClass);
		if(sc.getOfferedClass().getUapCharge() > 0){
			int carga = sc.getOfferedClass().getUapCharge();
			carga = carga - 1;
			offeredClass.setUapCharge(carga);
			LOGGER.info("{} - Actualizando uapCharge. Nuevo valor:{}", userSecurityService.getIdSession(), carga);
		}
		sc.setOfferedClass(offeredClass);
		/* Eliminar tambien, respuestas asosciadas a las evaluaciones de la carga academica*/
		answerUserTeacherEvalRepository.deleteByStudentChargeId(idCharge);
		LOGGER.info("{} - Respuestas de evaluación docente eliminadas.", userSecurityService.getIdSession());
		offeredClassRepository.save(sc.getOfferedClass());
		LOGGER.info("{} - Termina eliminar carga academica[{}].", userSecurityService.getIdSession(), idCharge);
	}

	@Override
	public StudentChargeDto findChargeByRegister(String register) {
		LOGGER.info("{} - Inicia findChargeByRegister[{}].", userSecurityService.getIdSession(), register);
		StudentChargeDto studentChargeDto = new StudentChargeDto();
		Configuration config = configurationRepository.findByKey(ConfigurationEnum.PREVIOUS_PERIOD.toString());
		Period periodoAnterior = periodRepository.getOne(Long.valueOf(config.getValue()));
		User user = userService.findUserByUsername(register);
		Student student = studentRepository.getOne(register);
		List<StudentCharge> charge = studentChargeRepository.findStudentChargeByRegister(register, true);
		List<StudentCharge> chargeAux = studentChargeRepository.findStudentChargeByStudentRegisterAndPeriodId(register, periodoAnterior.getId());
		List<StudentChargeDto> dtos = new ArrayList<>();
		charge.stream().forEach(p -> {
			dtos.add(new StudentChargeDto(p.getId(), studentConverter.convertStudent2StudentDto(p.getStudent()),
					periodConverter.convertPeriod2PeriodDto(p.getPeriod()),
					new OfferedClassDto(p.getOfferedClass().getId(),
							periodConverter.convertPeriod2PeriodDto(p.getOfferedClass().getPeriod()),
							teacherConverter.convertTeacher2TeacherDto(p.getOfferedClass().getTeacher()),
							p.getOfferedClass().getUapCharge(), p.getOfferedClass().getSemester(),
							p.getOfferedClass().getMaxCharge(), p.getOfferedClass().getMinCharge(),
							uapConverter.convertUap2UapDto(p.getOfferedClass().getUap()),
							groupConverter.convertGroup2GroupDto(p.getOfferedClass().getGroup()), null, 0L, 0L),
					p.getRegisterDate(), p.getAuthorizationKey(), p.getComun(), p.getEditDate(), null,
					p.getStudyType(), null));
		});
		studentChargeDto.setStudentCharges(dtos);
		studentChargeDto.setStudent(studentConverter.convertStudent2StudentDto(student));
		/* Calculo de evaluaciones contestadas*/
		InfoEvaluation info = infoEvaluationRepository.findByIdInfoEvaluationUserAndIdInfoEvaluationPeriod(user,periodoAnterior);
		if(info == null) {
			info = new InfoEvaluation();
		}
		LOGGER.info("{} - Cargando total de encuestas contestadas:[dtos:{}]-[teacherEval:{}]",userSecurityService.getIdSession(),chargeAux.size(),info.getTotalTeacherEval());
		LOGGER.info("{} - Cargando total de encuestas contestadas:[dtos:{}]-[autoEval:{}]",userSecurityService.getIdSession(),chargeAux.size(),info.getTotalAutoEval());
		studentChargeDto.setAutoevalAnswered(info.getTotalAutoEval() == chargeAux.size() && info.getTotalAutoEval() == chargeAux.size());
		studentChargeDto.setTeacherEvalAnswered(info.getTotalTeacherEval() == chargeAux.size() && info.getTotalTeacherEval() == chargeAux.size());
		studentChargeDto.setEvaluationsComplete(studentChargeDto.isTeacherEvalAnswered()&&studentChargeDto.isAutoevalAnswered());
		LOGGER.info("{} - EvaluationsComplete[{}].",userSecurityService.getIdSession(), studentChargeDto.isEvaluationsComplete());
		LOGGER.info("{} - Termina findChargeByRegister[{}].", userSecurityService.getIdSession(), register);
		return studentChargeDto;
	}

	@Override
	public byte[] exportToPdfFile(String register) throws ServiceException {
		LOGGER.info("{} - Iniciar exportToPdfFile[{}].", userSecurityService.getIdSession(), register);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		List<StudentDto> cargaChargeDtos = new ArrayList<>();
		List<StudentCharge> arr = new ArrayList<>();
		StudentDto studentDto = null;
		List<Tutorship> tutorship = new ArrayList<>();
		List<StudentChargeDto> dtos = new ArrayList<>();
		
		Student student = null;
		
		try {
			student = studentRepository.getOne(register);
		}catch(Exception ex) {
			throw new ServiceException("Ocurri\u00F3 un error al consultar los datos del alumno:" + register);
		}
		
		if(student == null) {
			LOGGER.info("{} - El alumno no existe.", userSecurityService.getIdSession());
			throw new ServiceException("El alumno buscado no existe.");
		}
		
		studentDto = studentConverter.convertStudent2StudentDto(student);
		try {
			tutorship = tutorshipRepository.findByStudentRegister(register);
		}catch(Exception ex2) {
			throw new ServiceException("Ocurri\u00F3 un error al consultar los datos del tutor.");
		}
		
		if(tutorship == null || tutorship.size() == 0 ) {
			//LOGGER.info("{} - Inicia seteo de parametros.", userSecurityService.getIdSession());
			throw new ServiceException("El tutor buscado no existe.");
		}
		
		try {
			arr = studentChargeRepository.findStudentChargeByRegister(register, true);
		}catch(Exception ex3){
			throw new ServiceException("Ocurri\u00F3 un error al consultar la carga del alumno.");
		}
		
		if(arr.size() <= 0) {
			LOGGER.info("{} - Alumno no tiene carga.", userSecurityService.getIdSession());
			throw new ServiceException("El alumno "+register+" a\u00FAn no tiene carga.");
		}
		
		arr.stream().forEach(p -> {
			dtos.add(new StudentChargeDto(p.getId(), studentConverter.convertStudent2StudentDto(p.getStudent()),
					periodConverter.convertPeriod2PeriodDto(p.getPeriod()),
					new OfferedClassDto(p.getOfferedClass().getId(),
							periodConverter.convertPeriod2PeriodDto(p.getOfferedClass().getPeriod()),
							teacherConverter.convertTeacher2TeacherDto(p.getOfferedClass().getTeacher()),
							p.getOfferedClass().getUapCharge(), p.getOfferedClass().getSemester(),
							p.getOfferedClass().getMaxCharge(), p.getOfferedClass().getMinCharge(),
							uapConverter.convertUap2UapDto(p.getOfferedClass().getUap()),
							groupConverter.convertGroup2GroupDto(p.getOfferedClass().getGroup()), null, 0L, 0L),
					p.getRegisterDate(), p.getAuthorizationKey(), p.getComun(), p.getEditDate(), null,
					p.getStudyType(), null));
		});
		studentDto.setStudentChargeDtos(dtos);
		//log.info("StudentDto despues de asignarle la carga{}",studentDto.toString());
		cargaChargeDtos.add(studentDto);
		LOGGER.info("{} - Total de registros recuperados:{}", userSecurityService.getIdSession(), arr.size());
		JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(cargaChargeDtos);
		// load file and compile it
		
		// File file = ResourceUtils.getFile("classpath:papeletav1.jrxml");
		InputStream archivo = StudentServiceImpl.class.getResourceAsStream("/papeleta.jasper");
		// JasperReport jasperReport =
		// JasperCompileManager.compileReport(file.getAbsolutePath());
		//LOGGER.info("{} - Inicia seteo de parametros.", userSecurityService.getIdSession());

		Map<String, Object> parameters = new HashMap<>();		
		parameters.put("P_GROUP", student.getGroup().getName()+" ["+student.getProgram().getAbr()+"]");
		parameters.put("P_SCHOOL_PROGRAM", student.getProgram().getStudentPlanName());
		parameters.put("P_SCHOOL_PERIOD", student.getGroup().getPeriod().getSchoolPeriod());
		parameters.put("P_SEMESTER", String.valueOf(student.getSemester()));
		parameters.put("P_GROUP", student.getGroup().getName()+" ["+student.getGroup().getTurn()+"]");
		parameters.put("P_STUDENT_NAME", studentDto.getName()+" "+studentDto.getLastname()+" "+studentDto.getSecondLastname());
		parameters.put("P_STUDENT_REGISTER", studentDto.getRegister());
		parameters.put("P_TUTOR", tutorship.get(0).getTeacher().getName()+" "+tutorship.get(0).getTeacher().getLastname()+" "+tutorship.get(0).getTeacher().getSecondLastname());
		//LOGGER.info("{} - Termina seteo de parametros.", userSecurityService.getIdSession());
		//LOGGER.info("{} - Inicia llenado de reporte.", userSecurityService.getIdSession());
		JasperPrint jasperPrint = null;
		try {
			jasperPrint = JasperFillManager.fillReport(archivo, parameters, dataSource);
		} catch (JRException ex4) {
			throw new ServiceException("Ocurri\u00F3 un error al llenar reporte para papeleta de alumno.");
		}

		JRPdfExporter exporter = new JRPdfExporter();

		exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
		exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, baos);

		try {
			exporter.exportReport();
		} catch (JRException ex5) {
			throw new ServiceException("Ocurri\u00F3 un error al exportar papeleta PDF.");
		}

		byte[] out = baos.toByteArray();
		// JasperExportManager.exportReportToPdfFile(jasperPrint,"Papeleta_" + register
		// + ".pdf");
		LOGGER.info("{} - Termina exportToPdfFile[{}].", userSecurityService.getIdSession(), register);

		return out;
	}	
	
	@Override
	public Resource loadPapeletaAsResource(String register) throws ServiceException {
		LOGGER.info("{} - Inicia loadPapeletaAsResource[{}].", userSecurityService.getIdSession(), register);
		User userpapeleta = userRepository.findByUsername(register)
				.orElseThrow(() -> new ServiceException("Usuario no encontrado."));
		User userSolicitud = userRepository.findByUsername(userSecurityService.getUserCurrentDetails().getUsername())
				.orElseThrow(() -> new ServiceException("Usuario no encontrado."));
		if (userpapeleta.getStudent() == null) {
			LOGGER.error("{} - El usuario que solicita la papeleta no es un alumno.", userSecurityService.getIdSession());
			throw new ServiceException("501");
		}else {
			if(!userSolicitud.getAuthorities().iterator().next().getAuthority().equals("STUDENT")) {
				LOGGER.error("{} - El usuario no esta autorizado para imprimir papeleta.", userSecurityService.getIdSession());
				throw new ServiceException("502");
			}
		}
		
		if(!userpapeleta.getStudent().getRegister().equals(userSolicitud.getStudent().getRegister())) {
			LOGGER.error("{} - El usuario solicita papeleta diferente a la suya.", userSecurityService.getIdSession());
			throw new ServiceException("504");
		}

		Student student = studentRepository.getOne(userpapeleta.getStudent().getRegister());
		LoadAuthorization auth = null;
		auth = loadAuthorizationRepository.findByStudentAndPeriod(userpapeleta.getStudent().getRegister(), true);
		
		if(auth == null) {
			LOGGER.info("{} - Carga academica no autorizada.", userSecurityService.getIdSession());
			throw new ServiceException("503");
		}else {
			if(!auth.isAuthorization()) {
				LOGGER.info("{} - Carga academica no autorizada.", userSecurityService.getIdSession());
				throw new ServiceException("503");
			}
		}
		
		byte[] pdf = null;
		try {
			pdf = exportToPdfFile(student.getRegister());
			
			Resource resource = new ByteArrayResource(pdf, "Papeleta de materias.");
			if (resource.exists()) {
				LOGGER.info("{} - Termina loadPapeletaAsResource[{}].", userSecurityService.getIdSession(), register);
				return resource;
			} else {
				LOGGER.info("{} - Archivo no encontrado.", userSecurityService.getIdSession());
				throw new ServiceException("Archivo no encontrado.");
			}
		}catch(ServiceException se) {
			LOGGER.error("{} - Error al genera papeleta:{}", userSecurityService.getIdSession(), se.getMessage());
			throw new ServiceException("La generación y descarga de la papeleta terminó dcon error.");
		}
	}
	
	@Override
	public Resource loadPapeletaAsResourceAdmin(String register) throws ServiceException {
		LOGGER.info("{} - Inicia loadPapeletaAsResource[{}].", userSecurityService.getIdSession(), register);
		User userpapeleta = userRepository.findByUsername(register)
				.orElseThrow(() -> new ServiceException("Usuario no encontrado."));
		User userSolicitud = userRepository.findByUsername(userSecurityService.getUserCurrentDetails().getUsername())
				.orElseThrow(() -> new ServiceException("Usuario no encontrado."));
		
		if(!userSolicitud.getAuthorities().iterator().next().getAuthority().equals("ADMIN") 
				&& !userSolicitud.getAuthorities().iterator().next().getAuthority().equals("TUTOR")) {
			LOGGER.error("{} - El usuario no esta autorizado para imprimir papeleta.", userSecurityService.getIdSession());
			throw new ServiceException("502");
		}

		Student student = studentRepository.getOne(userpapeleta.getStudent().getRegister());
		LoadAuthorization auth = null;
		auth = loadAuthorizationRepository.findByStudentAndPeriod(userpapeleta.getStudent().getRegister(), true);
		
		if(auth == null) {
			LOGGER.error("{} - Carga academica no autorizada.", userSecurityService.getIdSession());
			throw new ServiceException("503");
		}else {
			if(!auth.isAuthorization()) {
				LOGGER.error("{} - Carga academica no autorizada.", userSecurityService.getIdSession());
				throw new ServiceException("503");
			}
		}
		
		byte[] pdf = null;
		try {
			pdf = exportToPdfFile(student.getRegister());
		}catch(ServiceException se) {
			LOGGER.error("{} - Error al genera papeleta:{}", userSecurityService.getIdSession(), se.getMessage());
		}
		Resource resource = new ByteArrayResource(pdf, "Papeleta de materias.");
		if (resource.exists()) {
			LOGGER.info("{} - Termina loadPapeletaAsResource[{}].", userSecurityService.getIdSession(), register);
			return resource;
		} else {
			LOGGER.info("{} - Archivo no encontrado.", userSecurityService.getIdSession());
			throw new ServiceException("Archivo no encontrado.");
		}
	}
	
	@Override
	public Resource loadPapeletaAsResourceAdminMasivo(String idProgram, String turn) throws ServiceException {
		String PDF_EXTENSION = ".pdf";
		String PDF_TEMP = "/temp.pdf";
		String SLASH = "/";
		List<String> matriculas = new ArrayList<>();
		LOGGER.info("{} - Inicia loadPapeletaAsResourceAdminMasivo[{}-{}].", userSecurityService.getIdSession(), idProgram, turn);
		
		User userSolicitud = userRepository.findByUsername(userSecurityService.getUserCurrentDetails().getUsername())
				.orElseThrow(() -> new ServiceException("Usuario no encontrado."));
		
		if(!userSolicitud.getAuthorities().iterator().next().getAuthority().equals("ADMIN") 
				&& !userSolicitud.getAuthorities().iterator().next().getAuthority().equals("TUTOR")) {
			LOGGER.error("{} - El usuario no esta autorizado para imprimir papeleta.", userSecurityService.getIdSession());
			throw new ServiceException("502");
		}
		
		/* Se extraen los datos para generar papeletas*/
		/* Obtenemos los grupos que pertenecen al turno y Prog educativo recibidos. */
		List<Group> groups = groupRepository.findByTurnAndEducationalprogram(idProgram, turn, true);
		LOGGER.info("{} - grupos recuperados.", groups.size());
		/* obtenemos a los alumnos que pertenecen a cada grupo recuperado */
		for(Group groupTemp: groups) {
			List<Student> studentsGroupList= studentRepository.findStudentsByGroupAndStatus(groupTemp.getId(), 1L);
			LOGGER.info("{} - alumnos recuperados del grupo: {}.", studentsGroupList.size(),groupTemp.getId());
			/* Validando si el alumno tiene la carga autorizada */
			for(Student studentTemp: studentsGroupList) {
				LoadAuthorization loadAuthorizationTemp = loadAuthorizationRepository.findByStudentAndPeriod(studentTemp.getRegister(), true);
				List<StudentCharge> studentChargesTemp = new ArrayList<>();
				studentChargesTemp = studentChargeRepository.findStudentChargeByRegister(studentTemp.getRegister(), true);
				
				if(loadAuthorizationTemp != null && !studentChargesTemp.isEmpty()) {
					matriculas.add(studentTemp.getRegister());
				}
			}
		}
		
		
		byte[] pdf = null;
		try {
			//List<String> matriculas = new ArrayList<>();
			List<String> pdfs = new ArrayList<>();
			String session = userSecurityService.getIdSession();
			/*matriculas.add("14418546");
			matriculas.add("15285373");
			matriculas.add("15401697");
			matriculas.add("16263568");
			matriculas.add("16428846");
			matriculas.add("17269437");
			matriculas.add("17345318"); */
			
			/* Se crea el directorio temporal */
			String temporalDirectory = configurationRepository.findByKey(ConfigurationEnum.TEMPORAL_DIRECTORY.toString()).getValue()+session;
			FilesUtil.createDirectory(temporalDirectory, LOGGER);
			
	        /* Se recorren las matriculas para general la papeleta*/
			for(String mat : matriculas) {
				pdf = exportToPdfFile(mat);
				Path path = Paths.get(temporalDirectory+SLASH+mat+PDF_EXTENSION);
				pdfs.add(temporalDirectory+SLASH+mat+PDF_EXTENSION);
				Files.write(path, pdf);
			}
			
			/* Se unen los pdf's para crear uno solo*/
			PdfCopyFields copy = new PdfCopyFields(new FileOutputStream(temporalDirectory+PDF_TEMP));
			for (String pdf_ : pdfs) {
				 PdfReader reader = new PdfReader(pdf_);
				 copy.addDocument(reader);
			}
		    copy.close();
		    
		    /* Se genera byte[] para devolver al controller*/
		    Path path_ = Paths.get(temporalDirectory+PDF_TEMP);
		   
		    Resource resource = new ByteArrayResource(java.nio.file.Files.readAllBytes(path_), "Papeleta de materias.");
		    /*Se eliminan los temporales*/
		    FilesUtil.deleteTemporals(temporalDirectory, LOGGER);
		    
		    if (resource.exists()) {
		    	LOGGER.info("{} - Termina loadPapeletaAsResourceAdminMasivo[{}-{}].", userSecurityService.getIdSession(), idProgram, turn);
				return resource;
			} else {
				LOGGER.info("{} - Archivo no encontrado.", userSecurityService.getIdSession());
				throw new ServiceException("Archivo no encontrado.");
			}
		}catch(ServiceException se) {
			LOGGER.error("{} - Error al genera papeleta:{}", userSecurityService.getIdSession(), se.getMessage());
		} catch (IOException e) {
			e.printStackTrace();
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@Override
	public int totalStudentsExist() {
		
		return studentRepository.findAll().size();
	}
	
	@Override
	public StudentChargeDto getChargeById(Long id) {
		StudentCharge charge = studentChargeRepository.getOne(id);
		StudentChargeDto dto = new StudentChargeDto(charge.getId(), 
				studentConverter.convertStudent2StudentDto(charge.getStudent()), 
				periodConverter.convertPeriod2PeriodDto(charge.getPeriod()), 
				new OfferedClassDto(charge.getOfferedClass().getId(), periodConverter.convertPeriod2PeriodDto(charge.getPeriod()), teacherConverter.convertTeacher2TeacherDto(charge.getOfferedClass().getTeacher()), charge.getOfferedClass().getUapCharge(), charge.getOfferedClass().getSemester(), charge.getOfferedClass().getMaxCharge(), charge.getOfferedClass().getMinCharge(), uapConverter.convertUap2UapDto(charge.getOfferedClass().getUap()), null, null, 0, 0), 
				null, null, null, null, null, null, null);
		return dto;
	}

	@Override
	public int totalStudentsLoadAutorizedExist() {
		List<LoadAuthorization> loads = loadAuthorizationRepository.findLoadAuthorizationByPeriodId (periodRepository.getOneByActivePeriod(true).getId());
		return loads.size();
	}

	@Override
	public void saveChargeAdmin(StudentChargeDto studentChargeDto) throws ServiceException {
		LOGGER.info("{} - Inicia guardado de carga academica desde session de administrador.", userSecurityService.getIdSession());
		try {
			UserDto userDto = userService.findByUsername(userSecurityService.getUserCurrentDetails().getUsername());
			
			Date current = new Date();
			Student student = studentRepository.getOne(studentChargeDto.getStudentRegister());
			if (!userDto.getAuthority().equals(AuthorityEnum.ADMIN.toString())) {
				throw new ServiceException("No tienes permisos para realizar esta acci\u00F3n.");
			}
			/* Se obtienen todas las uaps que son obligatorias */
			List<OfferedClass> OfferedClassList = offeredClassRepository.findByGroupRequired(Long.valueOf(studentChargeDto.getIdGroup()));
			LOGGER.info("{} - UAPs obligatorias del grupo:{}", userSecurityService.getIdSession(), OfferedClassList.size());
			/* Se obtiene carga actual del alumno */
			List<StudentCharge> studentChargeList = studentChargeRepository.findStudentChargeByRegister(student.getRegister(), true);
			LOGGER.info("{} - UAPs que ya tiene cargadas el alumno:{}", userSecurityService.getIdSession(), studentChargeList.size());
			List<StudentCharge> studentChargeSave = new ArrayList<StudentCharge>();
			if(OfferedClassList.size() > 0) {
				for (OfferedClass offeredClass : OfferedClassList) {
					if(!offeredClassIsInStudentChargeList(studentChargeList, offeredClass)) {
						StudentCharge studentCharge = new StudentCharge();
						studentCharge.setAuthorizationKey("");
						studentCharge.setComun(offeredClass.getUap().getCommon_code());
						studentCharge.setEditDate(current);
						studentCharge.setOfferedClass(offeredClass);
						studentCharge.setPeriod(offeredClass.getGroup().getPeriod());
						studentCharge.setRegisterDate(current);
						studentCharge.setStudent(student);
						studentCharge.setStudyType(StudyTypeEnum.STUDY);
						studentChargeSave.add(studentCharge);
						offeredClass.setUapCharge(offeredClass.getUapCharge() + 1);
					}
				}
				LOGGER.info("{} - UAPs a agregar:{}", userSecurityService.getIdSession(), studentChargeSave.size());
				studentChargeRepository.saveAll(studentChargeSave);
				/* Se actualizan los contadores */
				offeredClassRepository.saveAll(OfferedClassList);
			}
			LOGGER.info("{} - Termina guardado de carga academica desde session de administrador.", userSecurityService.getIdSession());
		}catch(Exception ex) {
			LOGGER.error("Error al guardar carga como admin:", ex);
			throw new ServiceException("Ocurri\u00F3 un problema al procesar su solicitud, favor de intentarlo de nuevo.");
		}
	}

	private boolean offeredClassIsInStudentChargeList(List<StudentCharge> studentChargeList, OfferedClass offeredClass) {
		for (StudentCharge studentCharge : studentChargeList) {
			if(studentCharge.getOfferedClass().getUap().getKey().equals(offeredClass.getUap().getKey())) {
				return true;
			}
		}
		return false;
	}

	@Override
	public StudentChargeDto findChargeByRegisterByPeriodoId(String register) {
		LOGGER.info("{} - Inicia findChargeByRegisterByPeriodoId[{}].", userSecurityService.getIdSession(), register);
		
		Configuration configPreviousPeriod = configurationRepository.findByKey(ConfigurationEnum.PREVIOUS_PERIOD.toString());
		Period previousPeriod = periodRepository.getOne(Long.valueOf(configPreviousPeriod.getValue()));
		
		User user = userService.findUserByUsername(register);
		StudentChargeDto studentChargeDto = new StudentChargeDto();
		Student student = studentRepository.getOne(register);
		List<StudentCharge> chargeAux = studentChargeRepository.findStudentChargeByStudentRegisterAndPeriodId(register, previousPeriod.getId());
		LOGGER.info("{} - Numero de materias recuperadas findStudentChargeByStudentRegisterAndPeriodId[{}].", userSecurityService.getIdSession(), chargeAux.size());
		List<StudentChargeDto> dtos = new ArrayList<>();

		chargeAux.stream().forEach(p -> {
			List<AnswersUserTeacherEval> list = answerUserTeacherEvalRepository
					.findByUserAndStudentChargeAndStudentChargePeriod(user,
							studentChargeRepository.getOne(p.getId()),
							previousPeriod);

			dtos.add(new StudentChargeDto(p.getId(), studentConverter.convertStudent2StudentDto(p.getStudent()),
					periodConverter.convertPeriod2PeriodDto(p.getPeriod()),
					new OfferedClassDto(p.getOfferedClass().getId(),
							periodConverter.convertPeriod2PeriodDto(p.getOfferedClass().getPeriod()),
							teacherConverter.convertTeacher2TeacherDto(p.getOfferedClass().getTeacher()),
							p.getOfferedClass().getUapCharge(), p.getOfferedClass().getSemester(),
							p.getOfferedClass().getMaxCharge(), p.getOfferedClass().getMinCharge(),
							uapConverter.convertUap2UapDto(p.getOfferedClass().getUap()),
							groupConverter.convertGroup2GroupDto(p.getOfferedClass().getGroup()), null, 0L, 0L),
					p.getRegisterDate(), p.getAuthorizationKey(), p.getComun(), p.getEditDate(), null,
					p.getStudyType(), (list.size()>0)?list.iterator().next().getRegisterDate():null));
		});
		studentChargeDto.setStudentCharges(dtos);
		studentChargeDto.setStudent(studentConverter.convertStudent2StudentDto(student));
		/* Calculo de evaluaciones contestadas*/
		InfoEvaluation info = infoEvaluationRepository.findByIdInfoEvaluationUserAndIdInfoEvaluationPeriod(user,previousPeriod);
		if(info == null) {
			info = new InfoEvaluation();
		}
		LOGGER.info("{} - Cargando total de encuestas contestadas:[dtos:{}]-[teacherEval:{}]",userSecurityService.getIdSession(),chargeAux.size(),info.getTotalTeacherEval());
		LOGGER.info("{} - Cargando total de encuestas contestadas:[dtos:{}]-[autoEval:{}]",userSecurityService.getIdSession(),chargeAux.size(),info.getTotalAutoEval());
		studentChargeDto.setAutoevalAnswered(info.getTotalAutoEval() == chargeAux.size() && info.getTotalAutoEval() == chargeAux.size());
		studentChargeDto.setTeacherEvalAnswered(info.getTotalTeacherEval() == chargeAux.size() && info.getTotalTeacherEval() == chargeAux.size());
		studentChargeDto.setEvaluationsComplete(studentChargeDto.isTeacherEvalAnswered()&&studentChargeDto.isAutoevalAnswered());
		LOGGER.info("{} - EvaluationsComplete[{}].",userSecurityService.getIdSession(), studentChargeDto.isEvaluationsComplete());
		LOGGER.info("{} - Termina findChargeByRegister[{}].", userSecurityService.getIdSession(), register);
		return studentChargeDto;
	}

	@Override
	public Resource loadHorarioAsResource(String register) throws ServiceException {
		LOGGER.info("{} - Inicia loadHorarioAsResource[{}].", userSecurityService.getIdSession(), register);
		User userpapeleta = userRepository.findByUsername(register)
				.orElseThrow(() -> new ServiceException("Usuario no encontrado."));
		User userSolicitud = userRepository.findByUsername(userSecurityService.getUserCurrentDetails().getUsername())
				.orElseThrow(() -> new ServiceException("Usuario no encontrado."));
		if (userpapeleta.getStudent() == null) {
			LOGGER.error("{} - El usuario que solicita la papeleta no es un alumno.", userSecurityService.getIdSession());
			throw new ServiceException("501");
		}else {
			if(!userSolicitud.getAuthorities().iterator().next().getAuthority().equals("STUDENT")) {
				LOGGER.error("{} - El usuario no esta autorizado para imprimir papeleta.", userSecurityService.getIdSession());
				throw new ServiceException("502");
			}
		}

		if(!userpapeleta.getStudent().getRegister().equals(userSolicitud.getStudent().getRegister())) {
			LOGGER.error("{} - El usuario solicita papeleta diferente a la suya.", userSecurityService.getIdSession());
			throw new ServiceException("504");
		}

		Student student = studentRepository.getOne(userpapeleta.getStudent().getRegister());

		byte[] pdf = null;
		try {
			pdf = exportHorarioToPdfFile(student.getRegister());

			Resource resource = new ByteArrayResource(pdf, "Horario de materias.");
			if (resource.exists()) {
				LOGGER.info("{} - Termina loadHorarioAsResource[{}].", userSecurityService.getIdSession(), register);
				return resource;
			} else {
				LOGGER.info("{} - Archivo no encontrado.", userSecurityService.getIdSession());
				throw new ServiceException("Archivo no encontrado.");
			}
		}catch(ServiceException se) {
			LOGGER.error("{} - Error al genera el horario:{}", userSecurityService.getIdSession(), se.getMessage());
			throw new ServiceException("La generación y descarga del horario terminó con error.");
		}
	}

	@Override
	public byte[] exportHorarioToPdfFile(String register) throws ServiceException {
		LOGGER.info("{} - Iniciar exportHorarioToPdfFile[{}].", userSecurityService.getIdSession(), register);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		List<StudentDto> cargaChargeDtos = new ArrayList<>();
		List<StudentCharge> arr = new ArrayList<>();
		StudentDto studentDto = null;
		List<Tutorship> tutorship = new ArrayList<>();
		List<StudentChargeDto> dtos = new ArrayList<>();

		Student student = null;

		try {
			student = studentRepository.getOne(register);
		}catch(Exception ex) {
			throw new ServiceException("Ocurri\u00F3 un error al consultar los datos del alumno:" + register);
		}

		if(student == null) {
			LOGGER.info("{} - El alumno no existe.", userSecurityService.getIdSession());
			throw new ServiceException("El alumno buscado no existe.");
		}

		studentDto = studentConverter.convertStudent2StudentDto(student);
		try {
			tutorship = tutorshipRepository.findByStudentRegister(register);
		}catch(Exception ex2) {
			throw new ServiceException("Ocurri\u00F3 un error al consultar los datos del tutor.");
		}

		if(tutorship == null || tutorship.size() == 0 ) {
			LOGGER.info("{} - Inicia seteo de parametros.", userSecurityService.getIdSession());
			throw new ServiceException("El tutor buscado no existe.");
		}

		try {
			arr = studentChargeRepository.findStudentChargeByRegister(register, true);
		}catch(Exception ex3){
			throw new ServiceException("Ocurri\u00F3 un error al consultar la carga del alumno.");
		}

		if(arr.size() <= 0) {
			LOGGER.info("{} - Alumno no tiene carga.", userSecurityService.getIdSession());
			throw new ServiceException("El alumno "+register+" a\u00FAn no tiene carga.");
		}
		/* Recuperamos los horarios de las uaps por su ID */
		arr.stream().forEach(p -> {
			LOGGER.info("{} - ID a buscar: {}", userSecurityService.getIdSession(),p.getOfferedClass().getId());
			List<Schedule> scheduleListUap = scheduleRepository.findByOfferedClass_Id(p.getOfferedClass().getId());
			if(scheduleListUap != null){
				LOGGER.info("{} - Numero de registros recuperados de la tabla horario: {}", userSecurityService.getIdSession(),scheduleListUap.size());
				scheduleListUap.stream().forEach(q -> {
					LOGGER.info("{} - Horario recuperado: {}", userSecurityService.getIdSession(),q.toString());
						});
			}
		});

		arr.stream().forEach(p -> {
			dtos.add(new StudentChargeDto(p.getId(), studentConverter.convertStudent2StudentDto(p.getStudent()),
					periodConverter.convertPeriod2PeriodDto(p.getPeriod()),
					new OfferedClassDto(p.getOfferedClass().getId(),
							periodConverter.convertPeriod2PeriodDto(p.getOfferedClass().getPeriod()),
							teacherConverter.convertTeacher2TeacherDto(p.getOfferedClass().getTeacher()),
							p.getOfferedClass().getUapCharge(), p.getOfferedClass().getSemester(),
							p.getOfferedClass().getMaxCharge(), p.getOfferedClass().getMinCharge(),
							uapConverter.convertUap2UapDto(p.getOfferedClass().getUap()),
							groupConverter.convertGroup2GroupDto(p.getOfferedClass().getGroup()), null, 0L, 0L),
					p.getRegisterDate(), p.getAuthorizationKey(), p.getComun(), p.getEditDate(), null,
					p.getStudyType(), null));
		});
		studentDto.setStudentChargeDtos(dtos);
		//log.info("StudentDto despues de asignarle la carga{}",studentDto.toString());
		cargaChargeDtos.add(studentDto);
		LOGGER.info("{} - Total de registros recuperados:{}", userSecurityService.getIdSession(), arr.size());
		JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(cargaChargeDtos);
		// load file and compile it

		// File file = ResourceUtils.getFile("classpath:papeletav1.jrxml");
		InputStream archivo = StudentServiceImpl.class.getResourceAsStream("/papeleta.jasper");
		// JasperReport jasperReport =
		// JasperCompileManager.compileReport(file.getAbsolutePath());
		LOGGER.info("{} - Inicia seteo de parametros.", userSecurityService.getIdSession());

		Map<String, Object> parameters = new HashMap<>();
		parameters.put("P_GROUP", student.getGroup().getName()+" ["+student.getProgram().getAbr()+"]");
		parameters.put("P_SCHOOL_PROGRAM", student.getProgram().getStudentPlanName());
		parameters.put("P_SCHOOL_PERIOD", student.getGroup().getPeriod().getSchoolPeriod());
		parameters.put("P_SEMESTER", String.valueOf(student.getSemester()));
		parameters.put("P_GROUP", student.getGroup().getName()+" ["+student.getGroup().getTurn()+"]");
		parameters.put("P_STUDENT_NAME", studentDto.getName()+" "+studentDto.getLastname()+" "+studentDto.getSecondLastname());
		parameters.put("P_STUDENT_REGISTER", studentDto.getRegister());
		parameters.put("P_TUTOR", tutorship.get(0).getTeacher().getName()+" "+tutorship.get(0).getTeacher().getLastname()+" "+tutorship.get(0).getTeacher().getSecondLastname());
		LOGGER.info("{} - Termina seteo de parametros.", userSecurityService.getIdSession());
		LOGGER.info("{} - Inicia llenado de reporte.", userSecurityService.getIdSession());
		JasperPrint jasperPrint = null;
		try {
			jasperPrint = JasperFillManager.fillReport(archivo, parameters, dataSource);
		} catch (JRException ex4) {
			throw new ServiceException("Ocurri\u00F3 un error al llenar reporte para papeleta de alumno.");
		}

		JRPdfExporter exporter = new JRPdfExporter();

		exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
		exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, baos);

		try {
			exporter.exportReport();
		} catch (JRException ex5) {
			throw new ServiceException("Ocurri\u00F3 un error al exportar papeleta PDF.");
		}

		byte[] out = baos.toByteArray();
		// JasperExportManager.exportReportToPdfFile(jasperPrint,"Papeleta_" + register
		// + ".pdf");
		LOGGER.info("{} - Termina exportHorarioToPdfFile[{}].", userSecurityService.getIdSession(), register);

		return out;
	}
/*
	@Override
	public QualificationDto findStudentChargeByOfferedClassId(Long id) {
		List<StudentCharge> charge = studentChargeRepository.findStudentChargeByOfferedClassId(id);
		LOGGER.info("{} - Numero de registros recuperados[{}].", userSecurityService.getIdSession(), charge.size());
		QualificationDto qualificationDto = new QualificationDto();
		List<StudentDto> studentsDto = new ArrayList<>();
		// Cargando alumnos en arreglo de dtos
		long i = 1;
		for(StudentCharge ch : charge){
			//teacher = ch.getOfferedClass().getTeacher().getName()+" "+ch.getOfferedClass().getTeacher().getLastname()+" "+ch.getOfferedClass().getTeacher().getSecondLastname();
			//semester = String.valueOf(ch.getOfferedClass().getSemester());
			StudentDto st = studentConverter.convertStudent2StudentDto(ch.getStudent());
			st.setCounter(i);
			studentsDto.add(st);
			i++;
		}
		LOGGER.info("{} - Numero de studentsDto convertidos[{}].", userSecurityService.getIdSession(), studentsDto.size());
		qualificationDto.setStudentsDto(studentsDto);
		return qualificationDto;
	}*/
	
	@Override
	public Resource loadEvaluationVoucherAsResource(String register) throws ServiceException {
		LOGGER.info("{} - Inicia loadEvaluationVoucherAsResource[{}].", userSecurityService.getIdSession(), register);
		User userpapeleta = userRepository.findByUsername(register)
				.orElseThrow(() -> new ServiceException("Usuario no encontrado."));
		User userSolicitud = userRepository.findByUsername(userSecurityService.getUserCurrentDetails().getUsername())
				.orElseThrow(() -> new ServiceException("Usuario no encontrado."));
		if (userpapeleta.getStudent() == null) {
			LOGGER.error("{} - El usuario que solicita el reporte, no es un alumno.", userSecurityService.getIdSession());
			throw new ServiceException("501");
		}else {
			if(!userSolicitud.getAuthorities().iterator().next().getAuthority().equals("STUDENT")) {
				LOGGER.error("{} - El usuario no esta autorizado para imprimir papeleta.", userSecurityService.getIdSession());
				throw new ServiceException("502");
			}
		}
		
		if(!userpapeleta.getStudent().getRegister().equals(userSolicitud.getStudent().getRegister())) {
			LOGGER.error("{} - El usuario solicita reporte diferente al suyo.", userSecurityService.getIdSession());
			throw new ServiceException("504");
		}

		Student student = studentRepository.getOne(userpapeleta.getStudent().getRegister());
		/*LoadAuthorization auth = null;
		auth = loadAuthorizationRepository.findByStudentAndPeriod(userpapeleta.getStudent().getRegister(), true);
		
		if(auth == null) {
			LOGGER.info("{} - Carga academica no autorizada.", userSecurityService.getIdSession());
			throw new ServiceException("503");
		}else {
			if(!auth.isAuthorization()) {
				LOGGER.info("{} - Carga academica no autorizada.", userSecurityService.getIdSession());
				throw new ServiceException("503");
			}
		} */
		
		byte[] pdf = null;
		try {
			pdf = exportEvaluationVoucherToPdfFile(student.getRegister());
			
			Resource resource = new ByteArrayResource(pdf, "Reporte de evaluaciones.");
			if (resource.exists()) {
				LOGGER.info("{} - Termina loadEvaluationVoucherAsResource[{}].", userSecurityService.getIdSession(), register);
				return resource;
			} else {
				LOGGER.info("{} - Archivo no encontrado.", userSecurityService.getIdSession());
				throw new ServiceException("Archivo no encontrado.");
			}
		}catch(ServiceException se) {
			LOGGER.error("{} - Error al genera papeleta:{}", userSecurityService.getIdSession(), se.getMessage());
			throw new ServiceException("La generación y descarga del reporte de evaluación terminó con error.");
		}
	}

	private byte[] exportEvaluationVoucherToPdfFile(String register) throws ServiceException {
		List<StudentChargeDto> dtos = new ArrayList<>();
		List<StudentCharge> arr = null;
		StudentDto studentDto = null;
		List<StudentDto> cargaChargeDtos = new ArrayList<>();
		Student student = null;
		List<Tutorship> tutorship = new ArrayList<>();
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		
		try {
			student = studentRepository.getOne(register);
		}catch(Exception ex) {
			throw new ServiceException("Ocurri\u00F3 un error al consultar los datos del alumno:" + register);
		}
		studentDto = studentConverter.convertStudent2StudentDto(student);
		try {
			tutorship = tutorshipRepository.findByStudentRegister(register);
		}catch(Exception ex2) {
			LOGGER.info("{} - El tutor buscado no existe.", userSecurityService.getIdSession());
		}
		
		/* Se obtiene el periodo anterior*/
		Configuration conf = configurationRepository.findByKey(ConfigurationEnum.PREVIOUS_PERIOD.toString());
		Period period = periodRepository.getOne(Long.valueOf(conf.getValue()));
		try {
			arr = studentChargeRepository.findStudentChargeByStudentRegisterAndPeriodId(register, period.getId());
		}catch(Exception ex){
			LOGGER.error("Error al consultar carga academica:{}", ex.getMessage());
			throw new ServiceException("Ocurri\u00F3 un error al consultar la carga del alumno.");
		}
			
		arr.stream().forEach(p -> {
			User user = userService.findUserByUsername(p.getStudent().getRegister());
			List<AnswersUserTeacherEval> list = answerUserTeacherEvalRepository
					.findByUserAndStudentChargeAndStudentChargePeriod(user,
							studentChargeRepository.getOne(p.getId()),
							period);
			
			dtos.add(new StudentChargeDto(p.getId(), studentConverter.convertStudent2StudentDto(p.getStudent()),
					periodConverter.convertPeriod2PeriodDto(p.getPeriod()),
					new OfferedClassDto(p.getOfferedClass().getId(),
							periodConverter.convertPeriod2PeriodDto(p.getOfferedClass().getPeriod()),
							teacherConverter.convertTeacher2TeacherDto(p.getOfferedClass().getTeacher()),
							p.getOfferedClass().getUapCharge(), p.getOfferedClass().getSemester(),
							p.getOfferedClass().getMaxCharge(), p.getOfferedClass().getMinCharge(),
							uapConverter.convertUap2UapDto(p.getOfferedClass().getUap()),
							groupConverter.convertGroup2GroupDto(p.getOfferedClass().getGroup()), null, 0L, 0L),
					p.getRegisterDate(), p.getAuthorizationKey(), p.getComun(), p.getEditDate(), null,
					p.getStudyType(), (list.size()>0)?list.iterator().next().getRegisterDate():null));
		});
		studentDto.setStudentChargeDtos(dtos);
		
		cargaChargeDtos.add(studentDto);
		LOGGER.info("{} - Total de registros recuperados:{}", userSecurityService.getIdSession(), arr.size());
		JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(cargaChargeDtos);
		
		InputStream archivo = StudentServiceImpl.class.getResourceAsStream("/evaluationvoucher.jasper");
		LOGGER.info("{} - Inicia seteo de parametros.", userSecurityService.getIdSession());

		Map<String, Object> parameters = new HashMap<>();
		parameters.put("P_SCHOOL_PROGRAM", student.getProgram().getStudentPlanName());
		parameters.put("P_SCHOOL_PERIOD", period.getSchoolPeriod());
		parameters.put("P_STUDENT_NAME", studentDto.getName()+" "+studentDto.getLastname()+" "+studentDto.getSecondLastname());
		parameters.put("P_STUDENT_REGISTER", studentDto.getRegister());
		parameters.put("P_TUTOR", (!tutorship.isEmpty())?tutorship.get(0).getTeacher().getName()+" "+tutorship.get(0).getTeacher().getLastname()+" "+tutorship.get(0).getTeacher().getSecondLastname():"SIN TUTOR ASIGNADO");
		LOGGER.info("{} - Termina seteo de parametros.", userSecurityService.getIdSession());
		LOGGER.info("{} - Inicia llenado de reporte.", userSecurityService.getIdSession());
		JasperPrint jasperPrint = null;
		try {
			jasperPrint = JasperFillManager.fillReport(archivo, parameters, dataSource);
		} catch (JRException ex4) {
			throw new ServiceException("Ocurri\u00F3 un error al llenar reporte para papeleta de alumno.");
		}

		JRPdfExporter exporter = new JRPdfExporter();

		exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
		exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, baos);

		try {
			exporter.exportReport();
		} catch (JRException ex5) {
			throw new ServiceException("Ocurri\u00F3 un error al exportar papeleta PDF.");
		}

		byte[] out = baos.toByteArray();
		LOGGER.info("{} - Termina exportToPdfFile[{}].", userSecurityService.getIdSession(), register);

		return out;
	}
	
	@Override
	public StudentResponseDto getStudentResponseByRegister(String register) throws ServiceException {
		StudentResponseDto dto = new StudentResponseDto();
		Student student = null;
		Guest guest = null;
		
		if(register.contains("G")){
			guest = guestRepository.findOneByIdRegister(register);
			
			if(guest==null) {
				throw new ServiceException("No se encontró al invitado.");
			}
			
			dto.setCareer("N/A");
			dto.setErrorCode(HttpStatus.OK);
			dto.setErrorText("");
			dto.setFullname(guest.getName()+" "+guest.getLastname()+" "+guest.getSecondLastname());
			dto.setImagePath("");
			dto.setRegister(register);
			dto.setBeca("N/A");
			return dto;
		}else {
			try {
				student = studentRepository.findStundentByRegister(register);
				if(student==null) {
					LOGGER.info("Alumno no encontrado[{}]", register);
					throw new ServiceException("Alumno no encontrado.");
				}
				
				dto.setCareer(student.getProgram().getStudentPlanName());
				dto.setErrorCode(HttpStatus.OK);
				dto.setErrorText("");
				dto.setFullname(student.getName()+" "+student.getLastname()+" "+student.getSecondLastname());
				dto.setImagePath("");
				dto.setRegister(student.getRegister());
				
				//Validar si el alumno esta en la lista de postulados y ademas está aprobado
				Applicant applicantFind = applicantRepository.findByUserAndStatusGrant(student.getUsers().iterator().next(), true);
				
				if(applicantFind == null){
					throw new ServiceException("El alumno no tiene una beca activa asignada.");
				}
				dto.setBeca(applicantFind.getGrant().getName());
				return dto;
			}catch(Exception ex) {
				LOGGER.error("Error al buscar el alumno[{}]:{}", register, ex.getMessage());
				throw new ServiceException("Ocurrió un error al buscar alumno.");
			}
		}
	}
	
	@Override
	public void registerVisitStudent(StudentDto studentDto) throws ServiceException {
		Student student = studentRepository.findStundentByRegister(studentDto.getRegister());
		Date now = new Date();
		
		if(student==null) {
			LOGGER.info("Alumno no encontrado[{}]", studentDto.getRegister());
			throw new ServiceException("Alumno no encontrado.");
		}
		
		//Validar si el alumno esta en la lista de postulados y ademas está aprobado
		Applicant applicantFind = applicantRepository.findByUserAndStatusGrant(student.getUsers().iterator().next(), true);
		
		if(applicantFind == null){
			throw new ServiceException("El alumno no tiene una beca activa asignada.");
		}
		
		//Validar si el alumno está asignado a la cafeteria
		ApplicantCoffeeShop shop = applicantCoffeeShopRepository.getOneCustomByIdApplicantCoffeeShopId(applicantFind.getId(), studentDto.getCoffeeShopId(), FormatsConstant.SDF_DDMMYYYY.format(now));
		
		if(shop == null) {
			throw new ServiceException("El alumno no tiene asignado un d\u00EDa para esta cafeteria.");
		}
		
		//Validar que el alumno no halla ido antes a la cafeteria.
		List<EntryRegister> registers = entryRegisterRepository.findByDateStringAndUserStudentAndCoffeeShopId(FormatsConstant.SDF_DDMMYYYY.format(now), applicantFind.getUser(), studentDto.getCoffeeShopId());
		
		if(!registers.isEmpty()) {
			throw new ServiceException("El alumno ya ha ingresado a esta cafeteria.");
		}
		
		EntryRegister entry = new EntryRegister();
		entry.setDayEnum(getDayEnum(now));
		entry.setDatee(now);
		entry.setPeriod(periodRepository.getOneByActivePeriod(true));
		entry.setRegisterDate(now);
		
		UserDetails userDt = userSecurityService.getUserCurrentDetails();
		User userCashier = userService.findUserByUsername(userDt.getUsername());
		User userStudent = userService.findUserByUsername(studentDto.getRegister());
		entry.setUserCashier(userCashier);
		entry.setUserStudent(userStudent);
		entry.setDateString(FormatsConstant.SDF_DDMMYYYY.format(now));
		entry.setCoffeeShopId(studentDto.getCoffeeShopId());
		
		entryRegisterRepository.save(entry);
		
		LOGGER.info("Registro de entrada generado correctamente.");
	}
	
	private DayEnum getDayEnum(Date current) {
		switch (current.getDay()) {
		case 0:
			return DayEnum.D;
		case 1:
			return DayEnum.L;
		case 2:
			return DayEnum.M;
		case 3:
			return DayEnum.X;
		case 4:
			return DayEnum.J;
		case 5:
			return DayEnum.V;
		case 6:
			return DayEnum.S;
		}
		return null;
	}

	@Override
	public void registerVisitGuest(StudentDto studentDto) throws ServiceException {
		Guest student = guestRepository.findOneByIdRegister(studentDto.getRegister());
		Date now = new Date();
		
		if(student==null) {
			LOGGER.info("Invitado no encontrado[{}]", studentDto.getRegister());
			throw new ServiceException("Invitado no encontrado.");
		}
		
		//Validar si el invitado esta en la lista de postulados y ademas está aprobado
		Applicant applicantFind = applicantRepository.findByGuestAndStatusGrant(student, true);
		
		if(applicantFind == null){
			throw new ServiceException("El invitado no tiene una beca activa asignada.");
		}
		
		//Validar si el invitado está asignado a la cafeteria
		ApplicantCoffeeShop shop = applicantCoffeeShopRepository.getOneCustomByIdApplicantCoffeeShopId(applicantFind.getId(), studentDto.getCoffeeShopId(), FormatsConstant.SDF_DDMMYYYY.format(now));
		
		if(shop == null) {
			throw new ServiceException("El invitado no tiene asignado un d\u00EDa para esta cafeteria.");
		}
		
		//Validar que el alumno no halla ido antes a la cafeteria.
		List<EntryRegister> registers = entryRegisterRepository.findByDateStringAndUserStudentAndCoffeeShopId(FormatsConstant.SDF_DDMMYYYY.format(now), applicantFind.getUser(), studentDto.getCoffeeShopId());
		
		if(!registers.isEmpty()) {
			throw new ServiceException("El invitado ya ha ingresado a esta cafeteria.");
		}
		
		EntryRegister entry = new EntryRegister();
		entry.setDayEnum(getDayEnum(now));
		entry.setDatee(now);
		entry.setPeriod(periodRepository.getOneByActivePeriod(true));
		entry.setRegisterDate(now);
		
		UserDetails userDt = userSecurityService.getUserCurrentDetails();
		User userCashier = userService.findUserByUsername(userDt.getUsername());
		entry.setUserCashier(userCashier);
		entry.setUserStudent(null);
		entry.setGuest(student);
		entry.setDateString(FormatsConstant.SDF_DDMMYYYY.format(now));
		entry.setCoffeeShopId(studentDto.getCoffeeShopId());
		
		entryRegisterRepository.save(entry);
		
		LOGGER.info("Registro de entrada generado correctamente.");
	}
}
