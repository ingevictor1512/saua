/**
 * 
 */
package com.mx.saua.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import com.mx.saua.entity.*;
import com.mx.saua.enums.*;
import com.mx.saua.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.mx.saua.dto.AnswerDto;
import com.mx.saua.dto.InquestDto;
import com.mx.saua.dto.QuestionDto;
import com.mx.saua.exception.ServiceException;
import com.mx.saua.service.Arqsrv;
import com.mx.saua.service.QuestionService;
import com.mx.saua.util.PageableUtils;

/**
 * @author Benito Morales
 * @version 1.0
 */
@Service("questionService")
public class QuestionServiceImpl extends Arqsrv implements QuestionService {
	/**
	 * Inyección de QuestionRepository
	 */
	@Autowired
	private QuestionRepository questionRepository;

	/**
	 * Inyección de UserRepository
	 */
	@Autowired
	private UserRepository userRepository;

	/**
	 * Inyección de AnswerRepository
	 */
	@Autowired
	private AnswerRepository answerrepository;

	/**
	 * Inyección de AnswerUserRepository
	 */
	@Autowired
	private AnswerUserRepository userAnswerRepository;

	/**
	 * Inyección de AnswerUserTeacherEvalRepository
	 */
	@Autowired
	private AnswerUserTeacherEvalRepository answerUserTeacherEvalRepository;

	/**
	 * Inyección de AnswerUserTeacherEvalRepository
	 */
	@Autowired
	private AnswerUserAutoEvalRepository answerUserAutoEvalRepository;

	/**
	 * Inyección de CategoryQuestionRepository
	 */
	@Autowired
	private CategoryQuestionRepository categoryRepository;

	/**
	 * Inyección de StudentChargeRepository
	 */
	@Autowired
	private StudentChargeRepository studentChargeRepository;

	@Autowired
	private InquestQuestionRepository inquestQuestionRepository;

	@Autowired
	private InquestRepository inquestRepository;
	
	@Autowired
	private InfoEvaluationRepository infoEvaluationRepository;
	
	@Autowired
	private PeriodRepository periodRepository;
	
	@Autowired
	private UserSecurityService userSecurityService;

	@Autowired
	private ConfigurationRepository configurationRepository;

	@Override
	public QuestionDto find(QuestionDto questionDto, String inquestTypeEnum) {
		List<Question> questions = questionRepository.findByInquestType(InquestTypeEnum.valueOf(inquestTypeEnum));
		List<QuestionDto> dtos = new ArrayList<>();
		questions.stream().forEach(p -> {
			Set<AnswerDto> answers = new HashSet<>();
			p.getAnswers().stream().forEach(a -> {
				answers.add(new AnswerDto(a.getId(), null, a.getText(), a.isChecked(), a.isSpecify()));
			});
			/**
			 * Se ordena la lista de respuestas, de acuerdo al id
			 */
			List<AnswerDto> answersDtoSorted = answers.stream().collect(Collectors.toList());
			Collections.sort(answersDtoSorted,
					(o1, o2) -> String.valueOf(o1.getId()).compareTo(String.valueOf(o2.getId())));
			if (p.isStatus()) {
				dtos.add(new QuestionDto(p.getId(), p.getText(), p.getBody(), p.getQuestionType().toString(),
						p.getCreationDate(), p.getEditDate(), p.getQuestionBodyType(), answersDtoSorted,
						p.getPlaceHolder(), p.isStatus()));
			}
		});
		questionDto.setQuestions(dtos);
		return questionDto;
	}

	@Override
	public void saveFromApiRest(QuestionDto questionDto) throws ServiceException {
		try {
			LOGGER.info("Inicia guardado respuestas de encuesta.");
			User user = userRepository.findByUsername(questionDto.getStudentRegister())
					.orElseThrow(() -> new UsernameNotFoundException("No existe usuario."));
			Date current = new Date();
			questionDto.getAnswersl().stream().forEach(p -> {
				String arr[] = p.split("\\-");
				Question question = questionRepository.getOne(Long.valueOf(arr[2]));
				if (arr[1].equals("text")) {// Respuesta abierta
					userAnswerRepository.save(new AnswersUser(0, null, user, arr[3], question, "", current));
				} else if (arr[1].equals("radio")) {// radio, se selecciona solo una opción
					userAnswerRepository.save(new AnswersUser(0, null, user, arr[3], question, "", current));
				} else if (arr[1].equals("chk")) {// Opcion, se selecciona solo una opción
					Answer ans = answerrepository.getOne(Long.valueOf(arr[3]));
					userAnswerRepository.save(new AnswersUser(0, ans, user, "", question, "", current));
				} else if (arr[1].equals("chkm")) {// Opciones multiples
					Answer ans = answerrepository.getOne(Long.valueOf(arr[3]));
					userAnswerRepository.save(new AnswersUser(0, ans, user, "", question, "", current));
				}
			});
			// Se hace un segundo recorrido para encontrar las checks especifique
			LOGGER.info("Respuestas a recorrer:{}", questionDto.getAnswersl());
			questionDto.getAnswersl().stream().forEach(p -> {
				if (p.contains("specify")) {
					String[] arrt = p.split("\\-");
					if (arrt[1].equals("specify") && !arrt[4].equals("empty")
							&& isNotEspecify(arrt[2], questionDto.getAnswersl())) {
						List<AnswersUser> ansUser = userAnswerRepository.finByQuestionAndAnswerAndUser(
								Long.valueOf(arrt[2]), user.getIdUser(), Long.valueOf(arrt[3]));
						ansUser.get(0).setCustomizedAnswer(arrt[4]);
						userAnswerRepository.save(ansUser.get(0));
					}
				}
			});
			user.setInquest(false);
			userRepository.save(user);
			LOGGER.info("Termina guardado respuestas de encuesta.");
		} catch (Exception e) {
			LOGGER.error("Error al guardar respuestas de encuesta:{}", e);
			throw new ServiceException("Ocurri\u00F3 un error al guardar las respuestas.");
		}
	}

	private boolean isNotEspecify(String idQuestion, List<String> answersl) {
		for (String answ : answersl) {
			if (answ.contains("ans-chk-" + idQuestion)) {
				String[] arr = answ.split("\\-");
				Answer ans = answerrepository.getOne(Long.valueOf(arr[3]));
				if (ans.getText().contains("indique") || ans.getText().contains("especifique")) {
					return true;
				}
			}
		}
		return false;
	}

	@Override
	public void saveTeacherEvalFromApiRest(QuestionDto questionDto) throws ServiceException {
		LOGGER.info("Inicia guardado respuestas de evaluacion docente.");
		User user = userRepository.findByUsername(questionDto.getStudentRegister())
				.orElseThrow(() -> new UsernameNotFoundException("No existe usuario."));
		questionDto.getAnswersl().stream().forEach(p -> {
			String arr[] = p.split("\\-");
			Question question = questionRepository.getOne(Long.valueOf(arr[0]));
			StudentCharge stdCh = studentChargeRepository.getOne(Long.valueOf(questionDto.getStudentChargeId()));
			if (questionDto.getEval().equals(AuthorityEnum.TEACHER.toString())) {
				answerUserTeacherEvalRepository
						.save(new AnswersUserTeacherEval(0, null, user, arr[1], question, "", stdCh, new Date()));
			} else {
				answerUserAutoEvalRepository.save(new AnswersUserAutoEval(0, null, user, arr[1], question, "", stdCh));
			}
		});
		Configuration previosPeriod = configurationRepository.findByKey(ConfigurationEnum.PREVIOUS_PERIOD.toString());
		InfoEvaluation infoEval = infoEvaluationRepository.findByIdInfoEvaluationUserAndIdInfoEvaluationPeriod(user, periodRepository.getOne(Long.parseLong(previosPeriod.getValue())));
		if(infoEval == null) {
			infoEval = new InfoEvaluation();
			infoEval.setEditDate(new Date());
			infoEval.setRegisterDate(new Date());
			infoEval.setTotalAutoEval(0);
			infoEval.setTotalTeacherEval(0);
			IdInfoEvaluation idInfoEvaluation = new IdInfoEvaluation();
			idInfoEvaluation.setPeriod(periodRepository.getOneByActivePeriod(true));
			idInfoEvaluation.setUser(user);
			infoEval.setIdInfoEvaluation(idInfoEvaluation);
		}
		if (questionDto.getEval().equals(AuthorityEnum.TEACHER.toString())) {
			/* Actualizar contador */
			infoEval.setTotalTeacherEval(infoEval.getTotalTeacherEval() + 1);
		} else {
			/* Actualizar contador */
			infoEval.setTotalAutoEval(infoEval.getTotalAutoEval() + 1);
		}
		userRepository.save(user);
		infoEvaluationRepository.save(infoEval);
	}

	@Override
	public Object[][] findByCategory(int idCategory) {
		CategoryQuestion category = categoryRepository.getOne(idCategory);
		List<Question> list = questionRepository.findByCategoryQuestionOrderByTextAsc(category);
		Object[][] datos = new Object[list.size()][3];
		AtomicInteger count = new AtomicInteger(0);
		list.stream().forEach(p -> {
			int i = count.getAndIncrement();
			datos[i][0] = p.getId();
			datos[i][1] = p.getText();
			datos[i][2] = p.getQuestionType();
		});
		return datos;
	}

	@Override
	public QuestionDto findPageable(QuestionDto questionDto) {
		/*
		 * LOGGER.info("SearchByValue:{}", studentDto.getSearchByValue());
		 * LOGGER.info("SearchBy:{}", studentDto.getSearchBy()); LOGGER.info("Page:{}",
		 * studentDto.getPage());
		 */

		String dirStr = PageableUtils.getSortDirection(questionDto);
		int size = 5;

		questionDto.setPage(questionDto.getPage() - 1);

		Pageable pageable = PageableUtils.buildPageable(questionDto, size, "id");
		Page<Question> page = null;

		page = questionRepository.findQuestionByCategory(Integer.parseInt(questionDto.getCategoryQuestion()), pageable);

		QuestionDto result = new QuestionDto();
		result.setQuestionList(page.getContent());
		result.setDirection(dirStr);
		result.setPage(page.getNumber());
		result.setSize(page.getSize());
		result.setTotalPages(page.getTotalPages());
		result.setTotalItems(page.getTotalElements());
		return result;
	}

	@Override
	public Object[][] findByIds(QuestionDto questionDto) {
		LOGGER.info("{} - Buscando ids:[{}]", userSecurityService.getIdSession(), questionDto.getIdsQuestionsSelectedString());
		List<Long> listIds = Arrays.asList(questionDto.getIdsQuestionsSelectedString().split(",")).stream()
				.map(s -> Long.parseLong(s)).collect(Collectors.toList());
		List<Question> list = questionRepository.findByQuestionIds(listIds);

		Object[][] datos = new Object[list.size()][3];
		AtomicInteger count = new AtomicInteger(0);
		list.stream().forEach(p -> {
			int i = count.getAndIncrement();
			datos[i][0] = p.getId();
			datos[i][1] = p.getText();
			datos[i][2] = p.getQuestionType();
		});
		return datos;
	}

	@Override
	public QuestionDto findQuestionsByInquestId(long idQuestion) {
		List<QuestionDto> dtos = new ArrayList<QuestionDto>();
		QuestionDto dto = new QuestionDto();
		Inquest inquest = inquestRepository.getOne(idQuestion);
		List<InquestQuestion> list = inquestQuestionRepository.findByInquest(inquest);
		list.stream().forEach(p -> {
			List<AnswerDto> answersDto = new ArrayList<AnswerDto>();
			p.getQuestion().getAnswers().stream().forEach(q -> {
				answersDto.add(new AnswerDto(q.getId(), null, q.getText(), q.isChecked(), q.isSpecify()));
			});
			
			dtos.add(new QuestionDto(p.getId(), 
					p.getQuestion().getText(), 
					p.getQuestion().getBody(),
					p.getQuestion().getQuestionType().toString(), 
					p.getQuestion().getCreationDate(), 
					p.getQuestion().getEditDate(),
					p.getQuestion().getQuestionBodyType(), 
					answersDto, 
					p.getQuestion().getPlaceHolder(), 
					true));
		});
		dto.setQuestions(dtos);
		dto.setInquest(new InquestDto(inquest.getId(), inquest.getName(), inquest.getDescription(), inquest.getNumberQuestions(), inquest.getStatus(), inquest.getCreationDate(), inquest.getEditDate(), null, null,""));
		return dto;
	}

	@Override
	public void save(QuestionDto questionDto) throws ServiceException{
		LOGGER.info("{} - Inicia guardado de pregunta.", userSecurityService.getIdSession());
		Question question = new Question();
		Set<Answer> answers = new HashSet<Answer>();
		question.setText(questionDto.getText());
		question.setStatus(true);
		question.setCategoryQuestion(categoryRepository.getOne(1));
		question.setQuestionType(QuestionType.valueOf(questionDto.getQuestionType()));
		question.setPlaceHolder(questionDto.getPlaceHolder());
		question.setCreationDate(new Date());
		question.setEditDate(new Date());
		question.setQuestionBodyType(QuestionBodyType.TEXTO);
		question.setInquestType(InquestTypeEnum.DEFAULT);
		
		
		if(questionDto.getQuestionType().equals(QuestionType.OPCIONES.toString()) || questionDto.getQuestionType().equals(QuestionType.MULTIPLE.toString())) {
			List<String> listAsnw = Arrays.asList(questionDto.getIdsQuestionsSelectedString().split(",")).stream()
					.map(s -> String.valueOf(s)).collect(Collectors.toList());
			int i = 1;
			for (String string : listAsnw) {
				answers.add(new Answer(0, question, string, false, false, i));
				i++;
			}
		}
		question.setAnswers(answers);
		questionRepository.save(question);
		LOGGER.info("{} - Termina guardado de pregunta.", userSecurityService.getIdSession());
	}
	
    @Override
    public QuestionDto findAll(QuestionDto questionDto) {
        List<QuestionDto> questionsDto = new ArrayList<>();
        List<Question> askEntities = questionRepository.findAll();
        askEntities.stream().forEach(p->{
			questionsDto.add(new QuestionDto(p.getId(), 
					p.getText(), 
					p.getBody(), 
					p.getQuestionType().toString(),
					p.getCreationDate(), 
					p.getEditDate(), 
					p.getQuestionBodyType(), 
					null, 
					p.getPlaceHolder(),
					p.isStatus()));
		});
        questionDto.setQuestions(questionsDto);
        return questionDto;
    }
    
	@Override
	public void delete(QuestionDto questionDto) throws ServiceException {
		try {
			Question question = questionRepository.getOne(questionDto.getId());
			questionRepository.delete(question);
		}catch(Exception se) {
			LOGGER.error("Error al eliminar pregunta:{}", se.getMessage());
			throw new ServiceException("Este registro no se puede eliminar ya que cuenta con dependencias.");
		}
	}
}
