package com.mx.saua.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.mx.saua.converter.UserConverter;
import com.mx.saua.dto.ResetPasswordDto;
import com.mx.saua.dto.StudentDto;
import com.mx.saua.dto.UserDto;
import com.mx.saua.entity.ResetPassword;
import com.mx.saua.entity.Student;
import com.mx.saua.entity.User;
import com.mx.saua.entity.UserNotification;
import com.mx.saua.enums.NotificationStatusEnum;
import com.mx.saua.exception.ServiceException;
import com.mx.saua.repository.ResetPasswordRepository;
import com.mx.saua.repository.UserNotificationRepository;
import com.mx.saua.repository.UserRepository;
import com.mx.saua.service.ResetPasswordService;
import com.mx.saua.service.UserService;
import com.mx.saua.util.PageableUtils;

@Service("resetPasswordService")
public class ResetPasswordServiceImpl implements ResetPasswordService{

	@Autowired
	private ResetPasswordRepository resetPasswordRepository;
	
	@Autowired
	private UserNotificationRepository userNotificationRepository;
	
	@Autowired
	private UserConverter userConverter;
	
	@Autowired
	private UserService userService;
	
	@Override
	public ResetPasswordDto find(ResetPasswordDto dto) {
		List<ResetPassword> list = resetPasswordRepository.findAll();
		List<ResetPasswordDto> dtos = new ArrayList<ResetPasswordDto>();
		for(ResetPassword reset : list) {
			UserNotification notification = userNotificationRepository.findByPlainPassword(String.valueOf(reset.getIdReset()));
			dtos.add(new ResetPasswordDto(reset.getIdReset(), 
					reset.getCreated(), 
					"*************"+reset.getHashValue().substring(reset.getHashValue().length()-10, reset.getHashValue().length()), 
					reset.isUsed(),
					userConverter.convertUser2UserDto(reset.getUser()),
					notification));
		}
		dto.setResetPasswordDtos(dtos);
		return dto;
	}

	@Override
	public ResetPasswordDto getOne(long id) {
		ResetPassword reset = resetPasswordRepository.getOne(id);
		ResetPasswordDto dto = new ResetPasswordDto(reset.getIdReset(), 
				reset.getCreated(), 
				"*************"+reset.getHashValue().substring(reset.getHashValue().length()-10, reset.getHashValue().length()), 
				reset.isUsed(),
				userConverter.convertUser2UserDto(reset.getUser()),
				userNotificationRepository.findByPlainPassword(String.valueOf(id)));
		
		return dto;
	}

	@Override
	public void save(ResetPasswordDto resetPasswordDto) throws ServiceException {
		ResetPassword resetPassword = resetPasswordRepository.getOne(resetPasswordDto.getIdReset());
		UserNotification userNotification = userNotificationRepository.findByPlainPassword(String.valueOf(resetPassword.getIdReset()));
		userNotification.setAddresseeMail(resetPasswordDto.getEmail());
		userNotification.setStatus(NotificationStatusEnum.REGISTRADA);
		resetPassword.setUsed(false);
		resetPassword.setCreated(new Date());
		resetPasswordRepository.save(resetPassword);
		userNotificationRepository.save(userNotification);
	}

	@Override
	public ResetPasswordDto findPageable(ResetPasswordDto resetPasswordDto) {
		String dirStr = PageableUtils.getSortDirection(resetPasswordDto);
		int size = 10;

		resetPasswordDto.setPage(resetPasswordDto.getPage() - 1);

		Pageable pageable = PageableUtils.buildPageable(resetPasswordDto, size, "idReset");
		Page<ResetPassword> page = null;
		if (resetPasswordDto.getSearchBy() == null || resetPasswordDto.getSearchBy().isEmpty()) {
			page = resetPasswordRepository.findAll(pageable);
		} else {
			switch (resetPasswordDto.getSearchBy()) {
			case "Usuario":
				UserDto user = userService.findByUsername(resetPasswordDto.getSearchByValue());
				page = resetPasswordRepository.findResetPasswordByIdUser(user.getIdUser(), pageable);
				break;
			/*case "Nombre":
				page = studentRepository.findStundentByName(resetPasswordDto.getSearchByValue(), pageable);
				break;
			case "E-mail":
				page = studentRepository.findStundentByEmail(resetPasswordDto.getSearchByValue(), pageable);
				break;*/
			}
		}

		ResetPasswordDto result = new ResetPasswordDto();
		result.setResetPasswordList(page.getContent());
		result.setDirection(dirStr);
		result.setPage(page.getNumber());
		result.setSize(page.getSize());
		result.setTotalPages(page.getTotalPages());
		result.setTotalItems(page.getTotalElements());
		result.setSearchBy(resetPasswordDto.getSearchBy());
		result.setSearchByValue(resetPasswordDto.getSearchByValue());
		return result;
	}

	
}
