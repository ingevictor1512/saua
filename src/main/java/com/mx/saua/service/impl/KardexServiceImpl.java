package com.mx.saua.service.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import com.mx.saua.entity.*;
import com.mx.saua.repository.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mx.saua.constant.MessagesConstant;
import com.mx.saua.dto.KardexDto;
import com.mx.saua.dto.PeriodDto;
import com.mx.saua.dto.UapDto;
import com.mx.saua.enums.ConfigurationEnum;
import com.mx.saua.exception.ServiceException;
import com.mx.saua.service.KardexService;

/**
 * Servicio que maneja la logica de negocio para el kardex
 * 
 * @author Victor Francisco
 * @version 1.0
 *
 */
@Service("kardexService")
public class KardexServiceImpl implements KardexService {
	private Logger log = LoggerFactory.getLogger(KardexServiceImpl.class);

	@Autowired
	private KardexRepository kardexRepository;

	@Autowired
	private UapRepository uapRepository;

	@Autowired
	private StudentRepository studentRepository;

	@Autowired
	private PeriodRepository periodRepository;

	@Autowired
	private ConfigurationRepository configurationRepository;

	@Autowired
	private InfoEvaluationRepository infoEvaluationRepository;

	@Autowired
	private UserRepository userRepository;

	/**
	 * Inyección de UserSecurityService
	 */
	@Autowired
	private UserSecurityService userSecurityService;
	
	@Override
	public KardexDto find(String register) {
		log.info("{} - Cargando kardex:{}", userSecurityService.getIdSession(), register);
		KardexDto kardexDto = new KardexDto();
		/* Listas de etapas de uaps */
		List<KardexDto> kardexDtosEFI = new ArrayList<>();
		List<KardexDto> kardexDtosNFBAD = new ArrayList<>();
		List<KardexDto> kardexDtosNFPE = new ArrayList<>();
		List<KardexDto> kardexDtosEIV = new ArrayList<>();
		List<KardexDto> kardexDtosOtras = new ArrayList<>();
		Student studentTemp = studentRepository.getOne(register);
		List<Kardex> kardex = kardexRepository.findByMatricula(register);
		
		/*for(Kardex ka: kardex) {
			log.info("Valor a buscar:{}", ka.getCveUap());
			Uap uapTempUap = uapRepository.findByUapID(ka.getCveUap());
			
			//log.info("uapTempUap recuperada:{}", uapTempUap.toString());
			if (uapTempUap != null ) {
				UapDto uapDto = new UapDto();

				BeanUtils.copyProperties(uapTempUap, uapDto);
				if(uapDto.getPhaseUaps() != null) {
				if (uapDto.getPhaseUaps().getAbbreviation().equals("EFI")) {
					KardexDto kardexTempDto = new KardexDto();
					BeanUtils.copyProperties(ka, kardexTempDto);
					kardexDtosEFI.add(kardexTempDto);
				}
				if (uapDto.getPhaseUaps().getAbbreviation().equals("NFBAD")) {
					KardexDto kardexTempDto = new KardexDto();
					BeanUtils.copyProperties(ka, kardexTempDto);
					kardexDtosNFBAD.add(kardexTempDto);
				}
				if (uapDto.getPhaseUaps().getAbbreviation().equals("NFPE")) {
					KardexDto kardexTempDto = new KardexDto();
					BeanUtils.copyProperties(ka, kardexTempDto);
					kardexDtosNFPE.add(kardexTempDto);
				}
				if (uapDto.getPhaseUaps().getAbbreviation().equals("EIV")) {
					KardexDto kardexTempDto = new KardexDto();
					BeanUtils.copyProperties(ka, kardexTempDto);
					kardexDtosEIV.add(kardexTempDto);
				}
			}else {
				System.out.println("En algun momento uapDto es null");
			}
			
			}else {
				System.out.println("Else del primer if, no se encontro al menos una uap");
			}
		} */

		kardex.stream().forEach(p -> {
			//Uap uapTempUap = null;
			Uap uapTempUap = uapRepository.findByUapID(p.getCveUap());
			//uapTempUap = uapRepository.getOne(p.getCveUap());
			
			if (uapTempUap != null) {
				//log.info("uapTempUap recuperada:{}", uapTempUap.toString());
				UapDto uapDto = new UapDto();

				BeanUtils.copyProperties(uapTempUap, uapDto);
				if (uapDto.getPhaseUaps().getAbbreviation().equals("EFI")) {
					KardexDto kardexTempDto = new KardexDto();
					BeanUtils.copyProperties(p, kardexTempDto);
					kardexDtosEFI.add(kardexTempDto);
				}
				if (uapDto.getPhaseUaps().getAbbreviation().equals("NFBAD")) {
					KardexDto kardexTempDto = new KardexDto();
					BeanUtils.copyProperties(p, kardexTempDto);
					kardexDtosNFBAD.add(kardexTempDto);
				}
				if (uapDto.getPhaseUaps().getAbbreviation().equals("NFPE")) {
					KardexDto kardexTempDto = new KardexDto();
					BeanUtils.copyProperties(p, kardexTempDto);
					kardexDtosNFPE.add(kardexTempDto);
				}
				if (uapDto.getPhaseUaps().getAbbreviation().equals("EIV")) {
					KardexDto kardexTempDto = new KardexDto();
					BeanUtils.copyProperties(p, kardexTempDto);
					kardexDtosEIV.add(kardexTempDto);
				}
			}else {
				KardexDto kardexTempDto = new KardexDto();
				BeanUtils.copyProperties(p, kardexTempDto);
				kardexDtosOtras.add(kardexTempDto);
			}
		}); 

		kardexDto.setKardexDtosEfi(kardexDtosEFI);
		kardexDto.setKardexDtosNfbad(kardexDtosNFBAD);
		kardexDto.setKardexDtosNfpe(kardexDtosNFPE);
		kardexDto.setKardexDtosEiv(kardexDtosEIV);
		kardexDto.setKardexDtosOtras(kardexDtosOtras);
		kardexDto.setNameStudent(studentTemp.getName()+" "+studentTemp.getLastname()+" "+studentTemp.getSecondLastname() +
				"     GRUPO: "+ studentTemp.getGroup().getName()+" "+studentTemp.getGroup().getTurn()+"    E-MAIL: "+studentTemp.getUsers().iterator().next().getEmail());
		kardexDto.setRegister(studentTemp.getRegister());
		
		Period p = periodRepository.getOneByActivePeriod(true);
		PeriodDto periodDto = new PeriodDto(p.getId(), p.getInitialDate(), p.getFinalDate(), p.getSchoolPeriod(),
				p.isActivePeriod(), p.isActivePeriodAdmin(), p.getSchoolCycle(), p.getRegisterDate(), p.getEditdate());

		kardexDto.setPeriod(periodDto);

		/**
		 * Obtiene el número de días que se le sumaran a la fecha de registro
		 */
		Configuration registryDay = configurationRepository.findByKey(ConfigurationEnum.REGISTRY_DAY.toString());
		Configuration registrySubjectsInit = configurationRepository.findByKey(ConfigurationEnum.REGISTRY_SUBJECTS_INIT.toString());
		Configuration registrySubjectsEnd = configurationRepository.findByKey(ConfigurationEnum.REGISTRY_SUBJECTS_END.toString());
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy kk:mm");
		SimpleDateFormat sdff = new SimpleDateFormat("dd ' de ' MMMMMM ' a las ' kk:mm:ss");

		/**
		 * Se obtienen las fechas de evaluciones
		 */
		Configuration evalTeacherIni = configurationRepository.findByKey(ConfigurationEnum.EVAL_TEACHER_INI.toString());
		Configuration evalTeacherEnd = configurationRepository.findByKey(ConfigurationEnum.EVAL_TEACHER_END.toString());
		Configuration evalAutoIni = configurationRepository.findByKey(ConfigurationEnum.EVAL_AUTO_INI.toString());
		Configuration evalAutoEnd = configurationRepository.findByKey(ConfigurationEnum.EVAL_AUTO_END.toString());
		
		try {
			Calendar initialCalendar = Calendar.getInstance();
			Date initialDate = sdf.parse(registrySubjectsInit.getValue());
			//log.info("REGISTRY_SUBJECTS_INIT:{}", initialDate);
			initialCalendar.setTime(initialDate);
			initialCalendar.add(Calendar.DATE, Integer.valueOf(registryDay.getValue()));
			
			Calendar endCalendar = Calendar.getInstance();
			Date endDate = sdf.parse(registrySubjectsEnd.getValue());
			endCalendar.setTime(endDate);
			endCalendar.add(Calendar.DATE, Integer.valueOf(registryDay.getValue()));

			//log.info("REGISTRY_SUBJECTS_END:{}", endDate);
			
			Calendar evalTeacherIniCalendar = Calendar.getInstance();
			Date evalTeacherIniDate = sdf.parse(evalTeacherIni.getValue());
			evalTeacherIniCalendar.setTime(evalTeacherIniDate);
			
			Calendar evalAutoIniCalendar = Calendar.getInstance();
			Date evalAutoIniDate = sdf.parse(evalAutoIni.getValue());
			evalAutoIniCalendar.setTime(evalAutoIniDate);
			
			Calendar evalTeacherEndCalendar = Calendar.getInstance();
			Date evalTeacherEndDate = sdf.parse(evalTeacherEnd.getValue());
			evalTeacherEndCalendar.setTime(evalTeacherEndDate);
			
			Calendar evalAutoEndCalendar = Calendar.getInstance();
			Date evalAutoEndDate = sdf.parse(evalAutoEnd.getValue());
			evalAutoEndCalendar.setTime(evalAutoEndDate);
			
			kardexDto.setInitialPeriod(sdf.format(initialCalendar.getTime()));
			Date now = new Date();
			if (now.before(initialCalendar.getTime())) {
				kardexDto.setBeforePeriod(true);
			}else {
				kardexDto.setBeforePeriod(false);
			}
			
			if (now.after(endCalendar.getTime())) {
				kardexDto.setAfterPeriod(true);
			} else{
				kardexDto.setAfterPeriod(false);
			}

			
			if((now.after(evalTeacherIniCalendar.getTime()) || now.after(evalAutoIniCalendar.getTime())) 
					&& (now.before(evalTeacherEndCalendar.getTime()) || now.before(evalAutoEndCalendar.getTime()))) {
				kardexDto.setEvaluationsActive(true);
			}
			/**
			 * Se va a revisar si el alumno tiene activada la evaluación docente y autoevaluacion para que la conteste
			 */
			User usr = userRepository.findByUsername(register).orElse(null);
			Configuration previosPeriod = configurationRepository.findByKey(ConfigurationEnum.PREVIOUS_PERIOD.toString());
			Period periodoAnterior = periodRepository.getOne(Long.parseLong(previosPeriod.getValue()));
			InfoEvaluation infoEvaluations = infoEvaluationRepository.findByIdInfoEvaluationUserAndIdInfoEvaluationPeriod(usr,periodoAnterior);
			if(infoEvaluations != null){
				if(infoEvaluations.isActiveEval()){
					log.info("{} - Cargando isActiveEval:{}", userSecurityService.getIdSession(), infoEvaluations.isActiveEval());
					kardexDto.setEvaluationsActive(true);
				}
			}
			log.info("{} - Cargando setEvaluationsActive:{}", userSecurityService.getIdSession(), kardexDto.isEvaluationsActive());
			kardexDto.setInitialPeriod(sdf.format(initialCalendar.getTime()));
			kardexDto.setEndPeriod(sdf.format(endCalendar.getTime()));
		} catch (ParseException e) {
			log.error(e.getMessage());
			kardexDto.setBeforePeriod(true);
		}

		return kardexDto;
	}
	
	public String isAutoEvalActive() throws ServiceException {
		String stringRreturn="";
		Configuration registryDay = configurationRepository.findByKey(ConfigurationEnum.REGISTRY_DAY.toString());
		Configuration registrySubjectsInit = configurationRepository
				.findByKey(ConfigurationEnum.EVAL_AUTO_INI.toString());
		Configuration registrySubjectsEnd = configurationRepository
				.findByKey(ConfigurationEnum.EVAL_AUTO_END.toString());
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy kk:mm");
		Calendar initialCalendar = null;
		Calendar endCalendar = null;
		try {
			initialCalendar = Calendar.getInstance();
			Date initialDate = sdf.parse(registrySubjectsInit.getValue());
			initialCalendar.setTime(initialDate);
			initialCalendar.add(Calendar.DATE, Integer.valueOf(registryDay.getValue()));

			endCalendar = Calendar.getInstance();
			Date endDate = sdf.parse(registrySubjectsEnd.getValue());
			endCalendar.setTime(endDate);
			endCalendar.add(Calendar.DATE, Integer.valueOf(registryDay.getValue()));
		} catch (ParseException e) {
			log.error("{} - ERROR:{}", userSecurityService.getIdSession(), e.getMessage());
			 throw new ServiceException("Ocurri\u00F3 un error al calcular la validez de la autoevaluaci\u00F3n.");
		}
		Date now = new Date();
		if (now.before(initialCalendar.getTime())) {
			stringRreturn = MessagesConstant.NOT_INIT_EVAL_PROCESS_MESSAGE;
		}
		if (now.after(endCalendar.getTime())) {
			stringRreturn = MessagesConstant.FINISH_EVAL_PROCESS_MESSAGE;
		}
		return stringRreturn;
	}

}
