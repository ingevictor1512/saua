package com.mx.saua.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import com.mx.saua.dto.TutoringLowDto;
import com.mx.saua.entity.TutoringLow;
import com.mx.saua.repository.TutoringLowRepository;
import com.mx.saua.service.TutoringLowService;

@Service("tutoringLowService")
public class TutoringLowServiceImpl implements TutoringLowService{
	/**
	 * @author Vic
	 */
	@Autowired
	private TutoringLowRepository tutoringLowRepository;
	
	@Override
	public List<TutoringLowDto> find() {		
		List<TutoringLowDto> tutoringLowDtos = new ArrayList<>();
		List<TutoringLow> tutoringLow = tutoringLowRepository.findAll();
		tutoringLow.stream().forEach(p->{
			tutoringLowDtos.add(new TutoringLowDto(p.getId(), p.getDescription(), p.getEditdate(), p.getRegisterDate()));
		});
		return tutoringLowDtos;
	}

	@Override
	public TutoringLowDto getById(Long id) {
		TutoringLow tutoringLow = tutoringLowRepository.getOne(id);
		TutoringLowDto dto = new TutoringLowDto();
		BeanUtils.copyProperties(tutoringLow, dto);
		return dto;
	}

	@Override
	public void save(TutoringLowDto tutoringLowDto) {
		Date current = new Date();
		if(tutoringLowDto.getId() > -1L) {
			/* Update */
			TutoringLow tutoringLowUpdate = new TutoringLow();
			BeanUtils.copyProperties(tutoringLowDto, tutoringLowUpdate);
			tutoringLowUpdate.setEditdate(current);
			tutoringLowRepository.save(tutoringLowUpdate);
			
		} else {
			/* New register */
			TutoringLow tutoringLowInsert = new TutoringLow();
			BeanUtils.copyProperties(tutoringLowDto, tutoringLowInsert);
			tutoringLowInsert.setEditdate(current);
			tutoringLowInsert.setRegisterDate(current);
			tutoringLowRepository.save(tutoringLowInsert);
		}
		
	}

	@Override
	public void delete(Long id) {
		try {
			tutoringLowRepository.deleteById(id);
		} catch (DataIntegrityViolationException ve) {
			ve.printStackTrace();
			
		}
		
	}

}
