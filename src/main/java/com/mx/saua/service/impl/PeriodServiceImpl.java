package com.mx.saua.service.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import com.mx.saua.dto.KardexDto;
import com.mx.saua.dto.PeriodDto;
import com.mx.saua.entity.Configuration;
import com.mx.saua.entity.Period;
import com.mx.saua.enums.ConfigurationEnum;
import com.mx.saua.exception.ServiceException;
import com.mx.saua.repository.ConfigurationRepository;
import com.mx.saua.repository.PeriodRepository;
import com.mx.saua.service.PeriodService;

@Service("periodService")
public class PeriodServiceImpl implements PeriodService{
	/*
	 * @Vic
	 */
	@Autowired
	private PeriodRepository periodRepository;
	
	
	@Autowired
	private ConfigurationRepository configurationRepository;
	/**
	 * Logger de la clase
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(PeriodServiceImpl.class);
	
	@Override
	public List<PeriodDto> find() {
		List<PeriodDto> periodDtos = new ArrayList<>();
		List<Period> periods = periodRepository.findAll();
		periods.stream().forEach(p->{
			periodDtos.add(new PeriodDto(p.getId(), 
					p.getInitialDate(), 
					p.getFinalDate(), 
					p.getSchoolPeriod(), 
					p.isActivePeriod(), 
					p.isActivePeriodAdmin(), 
					p.getSchoolCycle(), 
					p.getRegisterDate(), 
					p.getEditdate()));
		});
		LOGGER.info("Busqueda de periodos realizada correctamente");
		return periodDtos;
	}

	@Override
	public PeriodDto getById(Long id) {
		Period period = periodRepository.getOne(id);
		PeriodDto periodDto = new PeriodDto();
		BeanUtils.copyProperties(period, periodDto);
		LOGGER.info("Busqueda por periodo realizada correctamente");
		return periodDto;
	}

	@Override
	public void save(PeriodDto periodDto) {
		if(periodDto.getId() > -1L) {
			//Update
			//Cargando el periodo completo
			Period period = periodRepository.getOne(periodDto.getId());
			
			period.setEditdate(new Date());
			period.setInitialDate(periodDto.getInitialDate());
			period.setFinalDate(periodDto.getFinalDate());
			period.setSchoolCycle(periodDto.getSchoolCycle());
			period.setSchoolPeriod(periodDto.getSchoolPeriod());
			periodRepository.save(period);
		} else {
			/* New register */
			Period periodInsert = new Period();
			BeanUtils.copyProperties(periodDto, periodInsert);
			periodInsert.setEditdate(new Date());
			periodInsert.setRegisterDate(new Date());
			periodInsert.setActivePeriod(false);
			periodInsert.setActivePeriodAdmin(false);
			periodRepository.save(periodInsert);
			LOGGER.info("Insercion del periodo realizada correctamente");
		}
		
	}

	@Override
	public void delete(Long id) throws ServiceException{
		Period period = periodRepository.getOne(id);
		if(period.isActivePeriod()) {
			throw new ServiceException("No puedes eliminar este periodo; ya que se encuentra activo. Activa otro y vuelve a intentarlo.");
		}
		try {
			periodRepository.deleteById(id);
			LOGGER.info("Se elimi\u00F3o el periodo correctamente.");
		} catch (DataIntegrityViolationException dve) {
			dve.printStackTrace();
		}
		
	}
	
	@Override
	public void activePeriod(Long id) throws ServiceException{
		Period period = periodRepository.getOne(id);
		if(period.isActivePeriod()) {
			throw new ServiceException("Este periodo ya se encuentra activo.");
		}
		List<Period> periods = periodRepository.findAll();
		Date now = new Date();
		periods.stream().forEach(p -> {
			if(p.getId().equals(id)) {
				p.setActivePeriod(true);
				p.setActivePeriodAdmin(true);
			}else {
				p.setActivePeriod(false);
				p.setActivePeriodAdmin(false);
			}
			p.setEditdate(now);
		});
		periodRepository.saveAll(periods);
	}
	
	@Override
	public Period getActivePeriod() {
		return periodRepository.getOneByActivePeriod(true);
	}
	
	public KardexDto isAfterRegistrySubjects(KardexDto kardexDto) {
		Configuration registryDay = configurationRepository.findByKey(ConfigurationEnum.REGISTRY_DAY.toString());
		Configuration registrySubjectsInit = configurationRepository.findByKey(ConfigurationEnum.REGISTRY_SUBJECTS_INIT.toString());
		Configuration registrySubjectsEnd = configurationRepository.findByKey(ConfigurationEnum.REGISTRY_SUBJECTS_END.toString());
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy kk:mm");
		
		try {
			Calendar initialCalendar = Calendar.getInstance();
			Date initialDate = sdf.parse(registrySubjectsInit.getValue());
			//LOGGER.info("REGISTRY_SUBJECTS_INIT:{}", initialDate);
			initialCalendar.setTime(initialDate);
			initialCalendar.add(Calendar.DATE, Integer.valueOf(registryDay.getValue()));
			
			Calendar endCalendar = Calendar.getInstance();
			Date endDate = sdf.parse(registrySubjectsEnd.getValue());
			endCalendar.setTime(endDate);
			endCalendar.add(Calendar.DATE, Integer.valueOf(registryDay.getValue()));
			
			kardexDto.setInitialPeriod(sdf.format(initialCalendar.getTime()));
			Date now = new Date();
			if (now.before(initialCalendar.getTime())) {
				kardexDto.setBeforePeriod(true);
			}else {
				kardexDto.setBeforePeriod(false);
			}
			
			
		} catch (ParseException e) {
			LOGGER.error(e.getMessage());
			kardexDto.setBeforePeriod(true);
		}
		
		return null;
	}
	
	public boolean isBeforeRegistrySubjects() {
		return false;
	}

}
