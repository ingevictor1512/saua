package com.mx.saua.service.impl;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mx.saua.constant.FormatsConstant;
import com.mx.saua.dto.ApplicantCoffeeShopDto;
import com.mx.saua.dto.ApplicantCoffeeShopIdDto;
import com.mx.saua.dto.ApplicantDto;
import com.mx.saua.dto.CoffeeShopDto;
import com.mx.saua.dto.UserDto;
import com.mx.saua.entity.Applicant;
import com.mx.saua.entity.ApplicantCoffeeShop;
import com.mx.saua.entity.ApplicantCoffeeShopId;
import com.mx.saua.entity.CoffeeShop;
import com.mx.saua.entity.Guest;
import com.mx.saua.exception.ServiceException;
import com.mx.saua.repository.ApplicantCoffeeShopRepository;
import com.mx.saua.repository.ApplicantRepository;
import com.mx.saua.repository.CoffeeShopRepository;
import com.mx.saua.repository.GuestRepository;
import com.mx.saua.service.ApplicantCoffeeShopService;

/**
 * Servicios para el manejo de los postulados a las cafeterías
 * @author Bento Morales
 * @version 1.0
 *
 */
@Service("applicantCoffeeShopService")
public class ApplicantCoffeeShopServiceImpl implements ApplicantCoffeeShopService{

	/**
	 * Logger de la clase
	 */
	private Logger LOGGER = LoggerFactory.getLogger(ApplicantCoffeeShopServiceImpl.class);
	
	/**
	 * Repositorio de las cafeterías
	 */
	@Autowired
	private CoffeeShopRepository coffeeShopRepository;
	
	/**
	 * Repositorio de los postulados
	 */
	@Autowired
	private ApplicantRepository applicantRepository;
	
	/**
	 * Repositorio de lospostulados-cafeterías
	 */
	@Autowired
	private ApplicantCoffeeShopRepository applicantCoffeeShopRepository;
	
	@Autowired
	private GuestRepository guestRepository;
	
	@Override
	public void save(ApplicantCoffeeShopDto applicantCoffeeShopDto) throws ServiceException {
		LOGGER.info("applicantCoffeeShopDto:{}", applicantCoffeeShopDto);
		Applicant applicant = null;
		Guest guest = null;
		long applicantId = 0L;
		List<ApplicantCoffeeShop> applicantCoffeeShopsDB  = null;
		if(applicantCoffeeShopDto.getApplicantRegister().contains("G")) {
			guest = guestRepository.getOne(applicantCoffeeShopDto.getApplicantRegister());
			LOGGER.info("Guest:{}", guest);
			//Revisar si ya existen registros
			applicantCoffeeShopsDB = applicantCoffeeShopRepository.findByApplicantCoffeeShopIdApplicantId(applicantCoffeeShopDto.getApplicantId());
			applicantId = applicantCoffeeShopDto.getApplicantId();
		}else {
			applicant = applicantRepository.findByUserUsername(applicantCoffeeShopDto.getApplicantRegister());
			LOGGER.info("Applicant:{}", applicant);
			applicantCoffeeShopsDB = applicantCoffeeShopRepository.findByApplicantCoffeeShopIdApplicantId(applicant.getId());
			applicantId = applicant.getId();
		}
		LOGGER.info("applicantCoffeeShopsDB:{}", applicantCoffeeShopsDB);
		
		List<String> listDate = Arrays.asList(applicantCoffeeShopDto.getData().split(",")).stream()
				.map(s -> s).collect(Collectors.toList());
		List<ApplicantCoffeeShop> applicantCoffeeShops = new ArrayList<>();
		
		Date current = new Date();
		for(String p : listDate) {
			if(!isInDate(p, applicantCoffeeShopsDB)) {
				Date useDate = null;
				String list[] = p.split("\\|");
				String listCoffeeId[] = list[0].split("\\-");
				try {
					useDate = FormatsConstant.SDF_DDMMYYYY.parse(list[1]);
				} catch (ParseException e) {
					LOGGER.error("Error al parsear fecha:{}",list[1]);
				}
				ApplicantCoffeeShop applicantCoffeeShop = new ApplicantCoffeeShop();
				
				ApplicantCoffeeShopId id = new ApplicantCoffeeShopId();
				id.setApplicantId(applicantId);
				id.setCoffeeShopId(coffeeShopRepository.getOne(Integer.valueOf(listCoffeeId[0])).getId());
				id.setUsedate(FormatsConstant.SDF_DDMMYYYY.format(useDate));
				
				applicantCoffeeShop.setApplicantCoffeeShopId(id);
				applicantCoffeeShop.setEditDate(current);
				applicantCoffeeShop.setRegisterDate(current);
				applicantCoffeeShop.setUseNumber(1);
				
				applicantCoffeeShops.add(applicantCoffeeShop);
			}
		}
		try {
			applicantCoffeeShopRepository.saveAll(applicantCoffeeShops);
		}catch(Exception ex) {
			throw new ServiceException("Una de las fechas seleccionadas, ya se enuentra registrada.");
		}
	}

	private boolean isInDate(String p, List<ApplicantCoffeeShop> applicantCoffeeShopsDB) {
		for (ApplicantCoffeeShop app : applicantCoffeeShopsDB) {
			if(p.contains(app.getApplicantCoffeeShopId().getUsedate())) {
				return true;
			}
		}
		return false;
	}

	@Override
	public List<ApplicantCoffeeShopDto> getByUser(UserDto userDto) {
		LOGGER.info("getByUser recibido:{}", userDto.getIdUser());
		List<ApplicantCoffeeShop> applicantCoffeeShops = new ArrayList<>();
		List<ApplicantCoffeeShopDto> applicantCoffeeShopsDto = new ArrayList<>();
		applicantCoffeeShops = applicantCoffeeShopRepository.findByApplicantCoffeeShopIdApplicantId(applicantRepository.findByUserUsername(userDto.getUsername()).getId());
		LOGGER.info("Numero de registros recuperados:{}", applicantCoffeeShops.size());
		if(applicantCoffeeShops.size() > 0){
			for(ApplicantCoffeeShop x : applicantCoffeeShops){
				ApplicantCoffeeShopDto applicantCoffeeShopDtoTemp = new ApplicantCoffeeShopDto();
				
				Applicant applicant = applicantRepository.getOne(x.getApplicantCoffeeShopId().getApplicantId());
				ApplicantCoffeeShopIdDto id = new ApplicantCoffeeShopIdDto();
				id.setApplicant(new ApplicantDto(applicant.getId(), applicant.getUser().getUsername(), applicant.getGrant(), applicant.getPeriod(), false, null, null));
				
				//applicantCoffeeShopDtoTemp.setApplicant();
				CoffeeShop coffeeShop = new CoffeeShop();
				coffeeShop = coffeeShopRepository.getOne(x.getApplicantCoffeeShopId().getCoffeeShopId());
				CoffeeShopDto coffeeShopDto = new CoffeeShopDto();
				coffeeShopDto.setId(coffeeShop.getId());
				coffeeShopDto.setEditDate(coffeeShop.getEditDate());
				coffeeShopDto.setRegisterDate(coffeeShop.getRegisterDate());
				coffeeShopDto.setGrantNumber(coffeeShop.getGrantNumber());
				coffeeShopDto.setLocation(coffeeShop.getLocation());
				coffeeShopDto.setName(coffeeShop.getName());
				coffeeShopDto.setManager(coffeeShop.getManager());
				
				id.setCoffeeShop(coffeeShopDto);
				id.setUsedate(x.getApplicantCoffeeShopId().getUsedate());
				applicantCoffeeShopDtoTemp.setApplicantCoffeeShopIdDto(id);
				applicantCoffeeShopDtoTemp.setUseDateFormated(FormatsConstant.SDF_DDMMYYYY.format(x.getApplicantCoffeeShopId().getUsedate()));
				
				applicantCoffeeShopsDto.add(applicantCoffeeShopDtoTemp);

			}
		}
		return applicantCoffeeShopsDto;
	}

	public List<ApplicantCoffeeShopDto> getApplicantCoffeeShop(String register) {
		List<ApplicantCoffeeShop> list = null;
		if(register.contains("G")) {
			list = applicantCoffeeShopRepository.findByApplicantCoffeeShopIdApplicantId(applicantRepository.findByGuestIdRegister(register).getId());
		}else {
			list = applicantCoffeeShopRepository.findByApplicantCoffeeShopIdApplicantId(applicantRepository.findByUserUsername(register).getId());
		}
		List<ApplicantCoffeeShopDto> dtos = new ArrayList<>();
		list.forEach(p -> {
			CoffeeShop coffeeShop = coffeeShopRepository.getOne(p.getApplicantCoffeeShopId().getCoffeeShopId());
			try {
				dtos.add(new ApplicantCoffeeShopDto(
						new ApplicantCoffeeShopIdDto(
							new ApplicantDto(p.getApplicantCoffeeShopId().getApplicantId(), "", null, null, false, "", null), 
							new CoffeeShopDto(p.getApplicantCoffeeShopId().getCoffeeShopId(), coffeeShop.getName(), coffeeShop.getGrantNumber(), coffeeShop.getManager(), coffeeShop.getLocation(), coffeeShop.getRegisterDate(), coffeeShop.getEditDate()),
							p.getApplicantCoffeeShopId().getUsedate()
						), 
						p.getUseNumber(), 
						p.getRegisterDate(), 
						p.getEditDate(), 
						"", 
						register,
						p.getApplicantCoffeeShopId().getUsedate(),
						FormatsConstant.DD.format(FormatsConstant.SDF_DDMMYYYY.parse(p.getApplicantCoffeeShopId().getUsedate())),
						0,
						"",
						0L));
			} catch (ParseException e) {
				LOGGER.error("Error al obtener ApplicantCoffeeShops:{}",e.getMessage());
			}
		});
		return dtos;
	}

	@Override
	public void delete(ApplicantCoffeeShopDto applicantCoffeeShopDto) throws ServiceException {
		try {
			applicantCoffeeShopRepository.delete(applicantCoffeeShopRepository.getOneCustomByIdApplicantCoffeeShopId(applicantCoffeeShopDto.getApplicantId(), 
					applicantCoffeeShopDto.getIdCoffeeShop(), 
					applicantCoffeeShopDto.getCoffeeShopDay()));
		}catch(Exception ex) {
			LOGGER.error("Error al eliminar dia:{}", ex.getMessage());
			throw new ServiceException("Ocurri\u00F3 un error al eliminar d\u00EDa asignado.");
		}
	}
}
