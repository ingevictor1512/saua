package com.mx.saua.service.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mx.saua.dto.ConfigurationDto;
import com.mx.saua.entity.Configuration;
import com.mx.saua.repository.ConfigurationRepository;
import com.mx.saua.service.ConfigService;

@Service("configService")
public class ConfigServiceImpl implements ConfigService {

	@Autowired
	ConfigurationRepository configurationRepository;

	Logger LOGGER = LoggerFactory.getLogger(ConfigServiceImpl.class);

	@Override
	public List<ConfigurationDto> find() {
		List<ConfigurationDto> configDtos = new ArrayList<>();
		List<Configuration> configs = configurationRepository.findAll();
		configs.stream().forEach(p -> {
			configDtos.add(new ConfigurationDto(p.getIdConfiguration(), 
					p.getKey(), 
					p.getValue(), 
					p.getDescription(),
					p.getType()));
		});
		LOGGER.info("Busqueda de configuraciones realizada correctamente");
		return configDtos;
	}

	@Override
	public ConfigurationDto getById(Long id) {
		Configuration config = configurationRepository.getOne(id);
		ConfigurationDto configDto = new ConfigurationDto();
		BeanUtils.copyProperties(config, configDto);
		if(config.getType()==4){
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy kk:mm");
			SimpleDateFormat sdp = new SimpleDateFormat("yyyy-MM-dd kk:mm:ss");
			try {
				Date date = sdp.parse(configDto.getValue());
				configDto.setValue(sdf.format(date));
			} catch (ParseException e) {
				LOGGER.error("Error al parsear fecha");
			}
		}
		LOGGER.info("Busqueda configuracion por id; realizada correctamente");
		return configDto;
	}

	@Override
	public void save(ConfigurationDto configurationDto) {
		Configuration config = configurationRepository.getOne(configurationDto.getIdConfiguration());
		config.setValue(configurationDto.getValue());
		config.setDescription(configurationDto.getDescription());
		configurationRepository.save(config);
	}

}
