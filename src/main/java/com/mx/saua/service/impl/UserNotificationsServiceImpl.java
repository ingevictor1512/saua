package com.mx.saua.service.impl;

import java.util.List;

import javax.mail.internet.MimeMessage;
import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import com.mx.saua.entity.ResetPassword;
import com.mx.saua.entity.UserNotification;
import com.mx.saua.enums.ConfigurationEnum;
import com.mx.saua.repository.ConfigurationRepository;
import com.mx.saua.repository.ResetPasswordRepository;
import com.mx.saua.repository.UserNotificationRepository;
import com.mx.saua.service.UserNotificationsService;

/**
 * Servicio de envío de notificaciones para los usuarios
 * 
 * @author Benito Morales
 *
 */
@Service("userNotificationsService")
public class UserNotificationsServiceImpl implements UserNotificationsService {

	/**
	 * Logger de la clase
	 */
	private Logger LOGGER = LoggerFactory.getLogger(UserNotificationsServiceImpl.class);

	@Autowired
	private UserNotificationRepository userNotificationRepository;

	@Autowired
	private ResetPasswordRepository resetPasswordRepository;

	@Autowired
	private ConfigurationRepository configurationRepository;

	@Autowired
	private JavaMailSender mailSender;

	@Autowired
	private TemplateEngine templateEngine;

	//@Value("${spring.mail.sender}")
	private String SENDER = "noreply@saua.com.mx";

	@Scheduled(fixedDelay = 60000)
	@Override
	@Transactional
	public void sendNotifications() {
		List<UserNotification> userNotifications = userNotificationRepository
				.findAllByStatus(com.mx.saua.enums.NotificationStatusEnum.REGISTRADA);
		if (!userNotifications.isEmpty()) {
			userNotifications.stream().forEach(p -> {
				try {

					MimeMessagePreparator preparator = null;
					switch (p.getType()) {
					case RESET:
						preparator = prepareResetPasswordByMail(p);
						break;
					case REGISTER:
						preparator = prepareRegister(p);
						break;
					case CHANGED_P:
						preparator = changedPassword(p);
						break;
					case AUTHORIZED_L:
						preparator = loadAuthorized(p);
						break;
					}
					mailSender.send(preparator);
					p.setStatus(com.mx.saua.enums.NotificationStatusEnum.ENVIADA);
					userNotificationRepository.save(p);
					LOGGER.info("E-mail enviado.");
				} catch (MailException ex) {
					p.setStatus(com.mx.saua.enums.NotificationStatusEnum.ENVIADA_FAIL);
					userNotificationRepository.save(p);
					LOGGER.info("E-mail no enviado.");
					LOGGER.error(ex.getMessage(), ex.getMessage());
				}
			});
		}
	}

	/**
	 * Prepara plantilla de registro, para envío por correo
	 * 
	 * @param notification datos a enviar
	 * @return mimeMessagePreparator
	 */
	private MimeMessagePreparator prepareRegister(UserNotification notification) {
		Context context = new Context();
		context.setVariable("name", notification.getAddresseeName());
		context.setVariable("mail", notification.getAddresseeMail());
		context.setVariable("username", notification.getUser().getUsername());
		context.setVariable("password", notification.getPlainPassword());
		context.setVariable("urlimagen", configurationRepository.findByKey(ConfigurationEnum.URL_SITE.toString()).getValue());
		context.setVariable("administration", configurationRepository.findByKey(ConfigurationEnum.ADMINISTRATION_NAME.toString()).getValue());

		return new MimeMessagePreparator() {

			@Override
			public void prepare(MimeMessage mimeMessage) throws Exception {
				MimeMessageHelper helper = new MimeMessageHelper(mimeMessage);
				helper.addTo(notification.getAddresseeMail());
				helper.setSubject("Bienvenido al Saua.");
				helper.setFrom(SENDER, SENDER);
				helper.setReplyTo(SENDER);
				helper.setText(templateEngine.process("mail/register", context), true);
				LOGGER.info("Prepare:{}|Registro", notification.getAddresseeMail());
			}
		};
	}

	/**
	 * Prepara plantilla de reseteo de contraseña, para envío por correo
	 * 
	 * @param notification datos a enviar
	 * @return mimeMessagePreparator
	 */
	private MimeMessagePreparator prepareResetPasswordByMail(UserNotification notification) {
		ResetPassword res = resetPasswordRepository.getOne(Long.valueOf(notification.getPlainPassword()));

		Context context = new Context();
		context.setVariable("name", notification.getAddresseeName());
		context.setVariable("mail", notification.getAddresseeMail());
		context.setVariable("url", configurationRepository.findByKey(ConfigurationEnum.URL_SITE.toString()).getValue()
				+ "/chkhash?h=" + res.getHashValue());
		context.setVariable("urlimagen", configurationRepository.findByKey(ConfigurationEnum.URL_SITE.toString()).getValue());
		context.setVariable("administration", configurationRepository.findByKey(ConfigurationEnum.ADMINISTRATION_NAME.toString()).getValue());

		return new MimeMessagePreparator() {

			@Override
			public void prepare(MimeMessage mimeMessage) throws Exception {
				MimeMessageHelper helper = new MimeMessageHelper(mimeMessage);
				helper.addTo(notification.getAddresseeMail());
				helper.setSubject("Reestablecer contrase\u00F1a.");
				helper.setFrom(SENDER, SENDER);
				helper.setReplyTo(SENDER);
				helper.setText(templateEngine.process("mail/reset", context), true);
				LOGGER.info("Prepare:{}|Reset password", notification.getAddresseeMail());
			}
		};
	}

	/**
	 * Prepara plantilla de cambio de contraseña, para envío por correo
	 * 
	 * @param notification Datos a enviar
	 * @return mimeMessagePreparator
	 */
	private MimeMessagePreparator changedPassword(UserNotification notification) {
		Context context = new Context();
		context.setVariable("name", notification.getAddresseeName());
		context.setVariable("mail", notification.getAddresseeMail());
		context.setVariable("urlimagen", configurationRepository.findByKey(ConfigurationEnum.URL_SITE.toString()).getValue());
		context.setVariable("administration", configurationRepository.findByKey(ConfigurationEnum.ADMINISTRATION_NAME.toString()).getValue());

		return new MimeMessagePreparator() {

			@Override
			public void prepare(MimeMessage mimeMessage) throws Exception {
				MimeMessageHelper helper = new MimeMessageHelper(mimeMessage);
				helper.addTo(notification.getAddresseeMail());
				helper.setSubject("Contrase\u00F1a actualizada.");
				helper.setFrom(SENDER, SENDER);
				helper.setReplyTo(SENDER);
				helper.setText(templateEngine.process("mail/updatepassword", context), true);
				LOGGER.info("Prepare:{}|Update password", notification.getAddresseeMail());
			}
		};
	}

	/**
	 * Prepara plantilla de autorizacion de carca academica, para envío por correo
	 * 
	 * @param notification Datos a enviar
	 * @return mimeMessagePreparator
	 */
	private MimeMessagePreparator loadAuthorized(UserNotification notification) {
		Context context = new Context();
		context.setVariable("name", notification.getAddresseeName());
		context.setVariable("mail", notification.getAddresseeMail());
		context.setVariable("urlimagen", configurationRepository.findByKey(ConfigurationEnum.URL_SITE.toString()).getValue());
		context.setVariable("administration", configurationRepository.findByKey(ConfigurationEnum.ADMINISTRATION_NAME.toString()).getValue());

		return new MimeMessagePreparator() {

			@Override
			public void prepare(MimeMessage mimeMessage) throws Exception {
				MimeMessageHelper helper = new MimeMessageHelper(mimeMessage);
				helper.addTo(notification.getAddresseeMail());
				helper.setSubject("Carga acad\u00E9mica autorizada");
				helper.setFrom(SENDER, SENDER);
				helper.setReplyTo(SENDER);
				helper.setText(templateEngine.process("mail/loadauthorized", context), true);
				LOGGER.info("Prepare:{}|Autorizacion de carga academica", notification.getAddresseeMail());
			}
		};
	}

}
