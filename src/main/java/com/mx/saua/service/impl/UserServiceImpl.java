package com.mx.saua.service.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.codec.digest.DigestUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.mx.saua.converter.UserConverter;
import com.mx.saua.constant.MessagesConstant;
import com.mx.saua.converter.TeacherConverter;
import com.mx.saua.dto.InquestDto;
import com.mx.saua.dto.InquestRoleDto;
import com.mx.saua.dto.TeacherDto;
import com.mx.saua.dto.UserDto;
import com.mx.saua.entity.Authority;
import com.mx.saua.entity.CoffeeShop;
import com.mx.saua.entity.Configuration;
import com.mx.saua.entity.InquestRole;
import com.mx.saua.entity.Period;
import com.mx.saua.entity.ResetPassword;
import com.mx.saua.entity.User;
import com.mx.saua.entity.UserInquest;
import com.mx.saua.entity.UserNotification;
import com.mx.saua.enums.AuthorityEnum;
import com.mx.saua.enums.ConfigurationEnum;
import com.mx.saua.enums.InquestStatusEnum;
import com.mx.saua.enums.NotificationStatusEnum;
import com.mx.saua.enums.NotificationTypeEnum;
import com.mx.saua.exception.ServiceException;
import com.mx.saua.repository.AuthorityRepository;
import com.mx.saua.repository.CoffeeShopRepository;
import com.mx.saua.repository.ConfigurationRepository;
import com.mx.saua.repository.InquestRoleRepository;
import com.mx.saua.repository.PeriodRepository;
import com.mx.saua.repository.ResetPasswordRepository;
import com.mx.saua.repository.UserInquestRepository;
import com.mx.saua.repository.UserNotificationRepository;
import com.mx.saua.repository.UserRepository;
import com.mx.saua.service.CoffeeShopService;
import com.mx.saua.service.UserService;
import com.mx.saua.util.SecurityUtils;

/**
 * 
 * @author Benito Morales Ramirez
 * @version 1.0
 *
 */
@Service("userService")
public class UserServiceImpl implements UserService {

	/**
	 * Logger de la aplicacion
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(UserServiceImpl.class);

	/**
	 * Inyección de UserRepository
	 */
	@Autowired
	private UserRepository userRepository;

	/**
	 * Inyección de UserConverter
	 */
	@Autowired
	private UserConverter userConverter;

	/**
	 * Inyección de PasswordEncoder
	 */
	@Autowired
	private PasswordEncoder passwordEncoder;

	/**
	 * Inyección de UserNotificationRepository
	 */
	@Autowired
	private UserNotificationRepository userNotificationRepository;

	/**
	 * Inyección de ConfigurationRepository
	 */
	@Autowired
	private ResetPasswordRepository resetPasswordRepository;

	/**
	 * Inyección de ConfigurationRepository
	 */
	@Autowired
	private ConfigurationRepository configurationRepository;

	/**
	 * Inyección de AuthorityRepository
	 */
	@Autowired
	private AuthorityRepository authorityRepository;

	/**
	 * Inyección de TeacherConverter
	 */
	@Autowired
	private TeacherConverter teacherConverter;

	/**
	 * Inyección de UserSecurityService
	 */
	@Autowired
	private UserSecurityService userSecurityService;

	/**
	 * Inyección de InquestRepository
	 */
	@Autowired
	private InquestRoleRepository inquestRoleRepository;

	/**
	 * Inyección de UserInquestRepository
	 */
	@Autowired
	private UserInquestRepository userInquestRepository;
	
	@Autowired
	private PeriodRepository periodRepository;
	
	@Autowired
	private CoffeeShopRepository coffeeShopRepository;
	
	@Autowired
	private CoffeeShopService coffeeShopService;
	
	@Override
	public UserDto findByUsername(String username) {
		User user = userRepository.findByUsername(username)
				.orElseThrow(() -> new UsernameNotFoundException("No existe usuario"));
		Period period = periodRepository.getOneByActivePeriod(true);
		/**
		 * Extrae todas las encuestas
		 */
		List<InquestRole> inquestList = inquestRoleRepository
				.findByRole(user.getAuthorities().iterator().next().getAuthority());
		List<InquestRoleDto> inquestListPendingDto = new ArrayList<InquestRoleDto>();
		UserDto userDto = userConverter.convertUser2UserDto(user);
		
		Date current = new Date();
		List<InquestRole> inquestListInrange = new ArrayList<InquestRole>();
		/**
		 * Recorre todas las encuestas del rol al que pertenece el usuario, y si la fecha
		 * esta dentro del rango de la encuesta. Se agrega a la lista de encuestas disponibles
		 * a contestar.
		 */
		inquestList.stream().forEach(p -> {
			if(current.after(p.getInquestInitDate()) && current.before(p.getInquestEndDate())) {
				inquestListInrange.add(p);
			}
		});
		
		for (InquestRole iqe : inquestListInrange) {
			/**
			 * Busca información de la encuesta iqe. Aquí agregar, que solo se busque respuestas del usuario 
			 * en el periodo actual.
			 */
			UserInquest ui = userInquestRepository.findByUserAndInquestAndIdPeriod(user, iqe.getInquest(), period.getId());
			
			if(ui == null) {
				/**
				 * Si no se encuentra información, significa que la encuesta por lo menos no se iniciaro, por lo tanto hay que agregarla
				 * a la lisya de encuestas pendientes 
				 */
				inquestListPendingDto.add(new InquestRoleDto(iqe.getId(),
						new InquestDto(iqe.getInquest().getId(), iqe.getInquest().getName(),
								iqe.getInquest().getDescription(), iqe.getInquest().getNumberQuestions(),
								iqe.getInquest().getStatus(), null, null, null, "",""),
						iqe.getRole(), iqe.getInquestInitDate(), iqe.getInquestEndDate(), iqe.getInquestStatusEnum()));
			}else {
				/**
				 * Si se encontró información. Revisar si ya se ha contestado, y agregarla a la lista de encuestas
				 */
				if(!ui.getStatus().equals(InquestStatusEnum.ANSWERED)){
					inquestListPendingDto.add(new InquestRoleDto(iqe.getId(),
						new InquestDto(iqe.getInquest().getId(), iqe.getInquest().getName(),
								iqe.getInquest().getDescription(), iqe.getInquest().getNumberQuestions(),
								ui.getStatus(), null, null, null, "",""),
						iqe.getRole(), iqe.getInquestInitDate(), iqe.getInquestEndDate(), iqe.getInquestStatusEnum()));
				}
			}
		}
		userDto.setInquestRoles(inquestListPendingDto);
		userDto.setInquest(inquestListPendingDto.size() > 0);
		
		/*Revisar si el usuario es un cajero*/
		if(user.getAuthorities().iterator().next().getAuthority().equals(AuthorityEnum.CASHIER.toString())) {
			List<CoffeeShop> coffees = coffeeShopRepository.findByManager(String.valueOf(user.getIdUser()));
			if(!coffees.isEmpty()) {
				userDto.setCoffeeShop(coffeeShopService.getById(coffees.iterator().next().getId()));
			}
		}
		return userDto;
	}

	@Override
	public UserDto find(UserDto userDto) {
		List<User> users = userRepository.findAll();
		List<UserDto> usersDto = new ArrayList<>();

		for (User user : users) {
			UserDto dto = userConverter.convertUser2UserDto(user);
			dto.translateAutorities();
			usersDto.add(dto);
		}
		userDto.setUsers(usersDto);
		return userDto;
	}

	@Override
	public void save(UserDto userDto) throws ServiceException {
		LOGGER.info("Guardando usuario.......");
		if (userDto.getIdUser() > -1L) {
			// Update user
			User userUpdate = userConverter.convertUserDto2User(userDto);
			User userPass = userRepository.getOne(userDto.getIdUser());
			Set<Authority> auths = new HashSet<>();
			userPass.getAuthorities().stream().forEach(p -> {
				p.setAuthority(userDto.getAuthority());
				p.setEditDate(new Date());
				auths.add(p);
			});
			userUpdate.setAuthorities(auths);
			userUpdate.setPassword(userPass.getPassword());
			userUpdate.setEditdate(new Date());
			userUpdate.setRegisterDate(userPass.getRegisterDate());
			if (userPass.getTeacher() != null) {
				userUpdate.setTeacher(userPass.getTeacher());
			}
			if (userPass.getStudent() != null) {
				userUpdate.setStudent(userPass.getStudent());
			}
			userRepository.save(userUpdate);
		} else {
			boolean finded = false;
			try {
				User userValidate = userRepository.customFindByUsername(userDto.getUsername());
				LOGGER.info("username-duplicado encontrado:{}", userValidate.getUsername());
				finded = true;
			}catch(Exception ex) {
				LOGGER.error("Error al buscar usuario duplicado:{}", ex.getMessage());
			}
			
			if(finded) {
				throw new ServiceException("El usuario ya existe, favor de intentar con otro.");
			}else {
				User user = userConverter.convertUserDto2User(userDto);
				// user.setAuthorities(authorities);
				user.setActive(true);
				user.setFirstLogin(true);
				user.setRegisterDate(new Date());
				user.setEditdate(new Date());
	
				if (userDto.getPassword().compareTo(userDto.getVerifyPassword()) != 0) {
					throw new ServiceException(MessagesConstant.NOT_MATCH);
				}
				if (!SecurityUtils.isValidPassword(userDto.getPassword())) {
					throw new ServiceException(MessagesConstant.INVALID_MSG);
				}
	
				// Encrypt
				String encodePass = passwordEncoder.encode(userDto.getPassword());
				user.setPassword(encodePass);
				// Save user
				userRepository.save(user);
			}
		}
	}

	@Override
	public UserDto getById(Long idUser) {
		User user = userRepository.getOne(idUser);
		UserDto userDto = new UserDto();
		userDto.setPassword(null);
		userDto.setVerifyPassword(null);
		return userConverter.convertUser2UserDto(user);
	}

	@Override
	public void delete(Long idUser) throws ServiceException {
		LOGGER.info("Eliminar usuario:{}", idUser);
		userRepository.delete(userRepository.getOne(idUser));
	}

	@Override
	public void updatePassword(UserDto userDto) throws ServiceException {
		LOGGER.info("{} - Inicia cambio de contraseña.", userSecurityService.getIdSession());
		if (!userDto.getPassword().equals(userDto.getVerifyPassword())) {
			throw new ServiceException("Las contrase\u00F1as no coinciden.");
		}

		if (!SecurityUtils.isValidPassword(userDto.getPassword())) {
			throw new ServiceException(MessagesConstant.INVALID_MSG);
		}

		User user = userRepository.getOne(userDto.getIdUser());
		if (passwordEncoder.matches(userDto.getVerifyPassword(), user.getPassword())) {
			throw new ServiceException("No es posible usar la misma contrase\u00F1a, use una distinta.");
		}

		if (userDto.getHash().equals("EDIT")) {
			if (user != null && user.isActive()) {
				user.setPassword(passwordEncoder.encode(userDto.getPassword()));
				userRepository.save(user);
			} else {
				throw new ServiceException("Error al buscar el usuario.");
			}
			LOGGER.info("{} - Termina edicion de contraseña.", userSecurityService.getIdSession());
		} else {
			if (user != null && user.isActive()) {
				user.setFirstLogin(false);
				user.setPassword(passwordEncoder.encode(userDto.getPassword()));
				userRepository.save(user);

				User userForNotification = userRepository.findOneByEmailAndUsername(user.getEmail(),
						user.getUsername());
				// Guardar notificacion
				UserNotification userNotification = new UserNotification();
				userNotification.setAddresseeMail(user.getEmail());
				if (user.getStudent() != null) {
					userNotification.setAddresseeName(user.getStudent().getName());
				} else if (user.getTeacher() != null) {
					userNotification.setAddresseeName(user.getTeacher().getName());
				} else {
					userNotification.setAddresseeName("");
				}
				userNotification.setUser(userForNotification);
				userNotification.setPlainPassword("");
				userNotification.setReviewable(false);
				userNotification.setStatus(NotificationStatusEnum.REGISTRADA);
				userNotification.setType(NotificationTypeEnum.CHANGED_P);
				userNotificationRepository.save(userNotification);
				LOGGER.info("{} - Termina cambio de contraseña.", userSecurityService.getIdSession());
			} else {
				throw new ServiceException("Error al buscar el usuario.");
			}
		}
	}

	@Override
	public void resetPasswordForMail(UserDto userDto) throws ServiceException {
		User user = userRepository.findOneByEmailAndUsername(userDto.getEmail(), userDto.getUsername());
		if (user == null) {
			throw new ServiceException("Datos incorrectos!.");
		} else {
			ResetPassword reset = new ResetPassword();
			reset.setUser(user);
			reset.setCreated(new java.util.Date());

			String message = String.format("%s%s", user.getEmail(), new java.util.Date());
			reset.setHashValue(DigestUtils.sha1Hex(message));

			reset.setUsed(false);
			resetPasswordRepository.save(reset);

			UserNotification notification = new UserNotification();
			notification
					.setAddresseeName((user.getStudent() != null) ? user.getStudent().getName() : user.getUsername());
			notification.setAddresseeMail(user.getEmail());
			notification.setPlainPassword(String.valueOf(reset.getIdReset()));

			User manager = new User();
			manager.setIdUser(user.getIdUser());
			notification.setUser(manager);
			notification.setStatus(NotificationStatusEnum.REGISTRADA);
			notification.setExpirationDate(new java.util.Date());
			notification.setType(NotificationTypeEnum.RESET);
			userNotificationRepository.save(notification);
		}
	}

	@Override
	public void compareHash(String hash) throws ServiceException {
		LOGGER.info("Hash recibido:{}", hash);
		ResetPassword reset = resetPasswordRepository.findOneByHashValue(hash);
		if (reset == null) {
			throw new ServiceException("Token inv\u00E1lido.");
		}
		// Validar si el token no ha caducado
		Configuration expirationDay = configurationRepository.findByKey(ConfigurationEnum.EXPIRATION_DAY.toString());

		Date current = new Date();
		Calendar initialCalendar = Calendar.getInstance();
		initialCalendar.setTime(reset.getCreated());
		initialCalendar.add(Calendar.DATE, Integer.valueOf(expirationDay.getValue()));

		if (current.after(initialCalendar.getTime())) {
			throw new ServiceException("Este token ya no es v\u00E1lido.");
		}

		if (reset.isUsed()) {
			throw new ServiceException("Este token ya ha sido utilizado.");
		}
	}

	@Override
	public void updatePasswordForReset(UserDto form) throws ServiceException {

		if (form.getPassword().compareTo(form.getVerifyPassword()) != 0) {
			throw new ServiceException(MessagesConstant.NOT_MATCH);
		}
		if (!SecurityUtils.isValidPassword(form.getPassword())) {
			throw new ServiceException(MessagesConstant.INVALID_MSG);
		}

		ResetPassword reset = null;
		User user = null;
		try {
			reset = resetPasswordRepository.findOneByHashValue(form.getHash());
			LOGGER.info("ResetObject.id:{}", reset.getIdReset());
			// Ocasionalmente manda nullPointer en producción
			user = userRepository.getOne(Long.valueOf(String.valueOf(reset.getUser().getIdUser())));
		} catch (Exception ex) {
			LOGGER.error("Error al resetear contraseña por hash:[{}][{}]", form.getHash(), ex.getMessage());
			throw new ServiceException(
					"Ocurri\u00F3 un problema al procesar tu solicitud, favor de intentarlo de nuevo.");
		}
		reset.setUsed(true);
		user.setPassword(passwordEncoder.encode(form.getPassword()));
		resetPasswordRepository.save(reset);
		userRepository.save(user);
	}

	@Override
	public List<TeacherDto> findAllByRole(String role) {
		List<Authority> authorities = authorityRepository.findByAuthority(role);
		List<TeacherDto> dtos = new ArrayList<>();
		for (Authority auth : authorities) {
			dtos.add(teacherConverter.convertTeacher2TeacherDto(auth.getUser().getTeacher()));
		}
		return dtos;
	}

	@Override
	public User findUserByUsername(String username) {
		return userRepository.findByUsername(username)
				.orElseThrow(() -> new UsernameNotFoundException("No existe usuario"));
	}
	
	@Override
	public List<String> doAutoCompleteUsername(String input) {
		List<User> list = userRepository.findByUsernameCustom(input);
		List<String> listStrings = new ArrayList<>();
		
		list.forEach(p -> {
			listStrings.add(p.getUsername());
		});
		
		return listStrings;
	}
}
