package com.mx.saua.service.impl;

import com.mx.saua.dto.InfoEvaluationDto;
import com.mx.saua.dto.PeriodDto;
import com.mx.saua.dto.UserDto;
import com.mx.saua.entity.*;
import com.mx.saua.enums.ConfigurationEnum;
import com.mx.saua.exception.ServiceException;
import com.mx.saua.repository.*;
import com.mx.saua.service.GroupService;
import com.mx.saua.service.InfoEvaluationService;
import com.mx.saua.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * Servicio que maneja la logica de negocio para la informacion de las evaluaciones
 *
 * @author Victor Francisco
 * @version 1.0
 *
 */
@Service("infoEvaluationService")
public class InfoEvaluationServiceImpl implements InfoEvaluationService {
    /**
     * Logger de la clase
     */
    private Logger LOGGER = LoggerFactory.getLogger(InfoEvaluationServiceImpl.class);
    /**
     * Inyección de UserSecurityService
     */
    @Autowired
    private UserSecurityService userSecurityService;
    @Autowired
    private InfoEvaluationRepository infoEvaluationRepository;
    @Autowired
    private PeriodRepository periodRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private StudentChargeRepository studentChargeRepository;
    @Autowired
    private ConfigurationRepository configurationRepository;

    @Override
    public InfoEvaluationDto find(InfoEvaluationDto infoEvaluationDto) {
        /* Obtenemos el periodo anterior de la tabla configuracion */
        Configuration previosPeriod= configurationRepository.findByKey(ConfigurationEnum.PREVIOUS_PERIOD.toString());
        List<InfoEvaluation> infoEvaluations = infoEvaluationRepository.findInfoEvaluationByIdInfoEvaluation_Period_Id(Long.parseLong(previosPeriod.getValue()));
        LOGGER.info("{} -Id del Periodo a consultar: {}", userSecurityService.getIdSession(),previosPeriod.getValue());
        List<InfoEvaluationDto> infoEvaluationDtos = new ArrayList<>();
        infoEvaluations.stream().forEach(p-> {
            /* Buscamos el numero de uaps que cargo el semestre anterior */
            List<StudentCharge> cargaAnterior = studentChargeRepository.findStudentChargeByStudentRegisterAndPeriodId(p.getIdInfoEvaluation().getUser().getStudent().getRegister(),Long.parseLong(previosPeriod.getValue()));
            infoEvaluationDtos.add(new InfoEvaluationDto(p.getIdInfoEvaluation().getUser().getIdUser(),
                    p.getIdInfoEvaluation().getUser().getStudent().getRegister(),
                    p.getIdInfoEvaluation().getPeriod().getSchoolPeriod(),
                    p.getTotalAutoEval(),
                    p.getTotalTeacherEval(),
                    p.getRegisterDate(),
                    p.getEditDate(), p.isActiveEval(),
                    p.getIdInfoEvaluation().getUser().getStudent().getName()+" "+p.getIdInfoEvaluation().getUser().getStudent().getLastname()+" "+p.getIdInfoEvaluation().getUser().getStudent().getSecondLastname(), cargaAnterior.size()
                    ));
           // infoEvaluationDtos.add((new InfoEvaluationDto(p.getIdInfoEvaluation(), p.getTotalAutoEval(), p.getTotalTeacherEval())))
        });
        infoEvaluationDto.setInfoEvaluationDtos(infoEvaluationDtos);
        return infoEvaluationDto;
    }

    @Override
    public InfoEvaluationDto findInfoEvaluationInActivePeriod(InfoEvaluationDto infoEvaluationDto) {
        return null;
    }

    @Override
    public InfoEvaluationDto findByIdInfoEvaluationUserAndIdInfoEvaluationPeriod(long user, long period) {
        Period periodoAnterior = periodRepository.getOne(period);
        User usuario = userRepository.getOne(user);
        InfoEvaluation infoEvaluation = infoEvaluationRepository.findByIdInfoEvaluationUserAndIdInfoEvaluationPeriod(usuario,periodoAnterior);
        InfoEvaluationDto infoEvaluationDto = new InfoEvaluationDto();
        infoEvaluationDto.setIdUsuario(infoEvaluation.getIdInfoEvaluation().getUser().getIdUser());
        infoEvaluationDto.setRegister(infoEvaluation.getIdInfoEvaluation().getUser().getStudent().getRegister());
        infoEvaluationDto.setName(infoEvaluation.getIdInfoEvaluation().getUser().getStudent().getName()+" "+infoEvaluation.getIdInfoEvaluation().getUser().getStudent().getLastname()+" "+infoEvaluation.getIdInfoEvaluation().getUser().getStudent().getSecondLastname());
        infoEvaluationDto.setPeriodo(infoEvaluation.getIdInfoEvaluation().getPeriod().getSchoolPeriod());
        infoEvaluationDto.setTotalAutoEval(infoEvaluation.getTotalAutoEval());
        infoEvaluationDto.setTotalTeacherEval(infoEvaluation.getTotalTeacherEval());
        return infoEvaluationDto;
    }

    @Override
    public void save(InfoEvaluationDto infoEvaluationDto) throws ServiceException {
        Configuration previosPeriod= configurationRepository.findByKey(ConfigurationEnum.PREVIOUS_PERIOD.toString());
        if(infoEvaluationDto.getIdUsuario() > -1L){
            //Vamos a actualizar los datos
            User usuario = userRepository.getOne(infoEvaluationDto.getIdUsuario());
            Period periodoAnterior = periodRepository.getOne(Long.parseLong(previosPeriod.getValue()));
            IdInfoEvaluation id = new IdInfoEvaluation();
            id.setPeriod(periodoAnterior);
            id.setUser(usuario);
            InfoEvaluation infoEvaluation = infoEvaluationRepository.getOne(id);
            infoEvaluation.setTotalAutoEval(infoEvaluationDto.getTotalAutoEval());
            infoEvaluation.setTotalTeacherEval(infoEvaluationDto.getTotalTeacherEval());
            infoEvaluationRepository.save(infoEvaluation);
            LOGGER.info("Actualizacion de la informacion de evaluaciones realizada correctamente");
        } else {
            /* New register */
            Optional<User> usuario = userRepository.findByUsername(infoEvaluationDto.getRegister());
            Period periodoAnterior = periodRepository.getOne(Long.parseLong(previosPeriod.getValue()));
            IdInfoEvaluation id = new IdInfoEvaluation();
            id.setPeriod(periodoAnterior);
            if(usuario.isPresent()){
                id.setUser(usuario.get());
                LOGGER.error("El usuario existe");
            } else {
                LOGGER.error("El usuario no existe");
                throw new ServiceException("La matrícula no existe.");
            }

            InfoEvaluation infoEvaluation = new InfoEvaluation();
            infoEvaluation.setIdInfoEvaluation(id);
            infoEvaluation.setTotalAutoEval(infoEvaluationDto.getTotalAutoEval());
            infoEvaluation.setTotalTeacherEval(infoEvaluationDto.getTotalTeacherEval());
            infoEvaluation.setEditDate(new Date());
            infoEvaluation.setRegisterDate(new Date());
            infoEvaluation.setActiveEval(false);
            infoEvaluationRepository.save(infoEvaluation);
            LOGGER.info("Insercion de la informacion de evaluaciones realizada correctamente");
        }
    }

    @Override
    public void activateEvaluations(Long id) throws ServiceException {
        LOGGER.info("Valor recibido {}",id);
        Configuration previosPeriod= configurationRepository.findByKey(ConfigurationEnum.PREVIOUS_PERIOD.toString());
        User usuario = userRepository.getOne(id);
        Period periodoAnterior = periodRepository.getOne(Long.parseLong(previosPeriod.getValue()));
        InfoEvaluation infoEvaluation = infoEvaluationRepository.findByIdInfoEvaluationUserAndIdInfoEvaluationPeriod(usuario, periodoAnterior);
        infoEvaluation.setActiveEval(true);
        infoEvaluationRepository.save(infoEvaluation);

    }

    @Override
    public void desactivateEvaluations(Long id) throws ServiceException {
        Configuration previosPeriod= configurationRepository.findByKey(ConfigurationEnum.PREVIOUS_PERIOD.toString());
        User usuario = userRepository.getOne(id);
        Period periodoAnterior = periodRepository.getOne(Long.parseLong(previosPeriod.getValue()));
        InfoEvaluation infoEvaluation = infoEvaluationRepository.findByIdInfoEvaluationUserAndIdInfoEvaluationPeriod(usuario, periodoAnterior);
        infoEvaluation.setActiveEval(false);
        infoEvaluationRepository.save(infoEvaluation);
    }

}
