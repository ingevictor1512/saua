package com.mx.saua.service.impl;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import com.mx.saua.converter.StudentConverter;
import com.mx.saua.dto.DocumentDto;
import com.mx.saua.dto.StudentDto;
import com.mx.saua.dto.UapDto;
import com.mx.saua.dto.UserDto;
import com.mx.saua.entity.Document;
import com.mx.saua.entity.OfferedClass;
import com.mx.saua.entity.Student;
import com.mx.saua.entity.StudentCharge;
import com.mx.saua.entity.Teacher;
import com.mx.saua.entity.Tutorship;
import com.mx.saua.enums.AuthorityEnum;
import com.mx.saua.enums.ConfigurationEnum;
import com.mx.saua.exception.ServiceException;
import com.mx.saua.repository.ConfigurationRepository;
import com.mx.saua.repository.DocumentRepository;
import com.mx.saua.repository.OfferedClassRepository;
import com.mx.saua.repository.StudentChargeRepository;
import com.mx.saua.repository.StudentRepository;
import com.mx.saua.repository.TeacherRepository;
import com.mx.saua.repository.TutorshipRepository;
import com.mx.saua.service.DocumentService;
import com.mx.saua.service.UserService;

/**
 * Servicio que implementa la logica de negocio para la administracion
 * de documentos(formatos).
 * @author Benito Morales
 *
 */
@Service("documentService")
public class DocumentServiceImpl implements DocumentService {

	/**
	 * Inyeccion de documentRepository
	 */
	@Autowired
	private DocumentRepository documentRepository;

	/**
	 * Inyeccion de configurationRepository
	 */
	@Autowired
	private ConfigurationRepository configurationRepository;

	/**
	 * Inyeccion de userService
	 */
	@Autowired
	private UserService userService;
	
	@Autowired
	private TeacherRepository teacherRepository;
	
	@Autowired
	private TutorshipRepository tutorshipRepository;
	
	@Autowired
	private OfferedClassRepository offeredClassRepository;
	
	@Autowired
	private StudentChargeRepository studentChargeRepository;
	
	@Autowired
	private StudentConverter studentConverter;
	@Autowired
	private StudentRepository studentRepository;

	/**
	 * Logger de la clase
	 */
	private Logger LOGGER = LoggerFactory.getLogger(DocumentServiceImpl.class);

	/**
	 * Inyeccion de userSecurityService
	 */
	@Autowired
	private UserSecurityService userSecurityService;
	
	
	@Override
	public DocumentDto find(DocumentDto documentDto) {
		LOGGER.info("{} - Inicia carga de documentos.", userSecurityService.getIdSession());
		List<Document> documents = documentRepository.findAll();
		List<DocumentDto> documentsDto = new ArrayList<>();
		List<DocumentDto> documentsDtoGral = new ArrayList<>();
		List<DocumentDto> documentsDtoMap = new ArrayList<>();
		UserDetails userDetails = userSecurityService.getUserCurrentDetails();
		
		UserDto userDto = null;
		try {
			userDto = userService.findByUsername(userDetails.getUsername());
		} catch (Exception e) {
			LOGGER.error("No se encontró el usuario.");
		}
		
		for(Document document : documents){
			if(document.getAuthorizationsRole().contains(userDto.getAuthority()) && document.getTypeDocument() == 1 ) {
				documentsDto.add(new DocumentDto(document.getId(), document.getRealName(), document.getViewName(), document.getDescription(),
						document.getAuthorizationsRole(), document.getTypeDocument()));
			}
			if(document.getAuthorizationsRole().contains(userDto.getAuthority()) && document.getTypeDocument() == 2 ) {
				documentsDtoMap.add(new DocumentDto(document.getId(), document.getRealName(), document.getViewName(), document.getDescription(),
						document.getAuthorizationsRole(), document.getTypeDocument()));
			}
			if(document.getAuthorizationsRole().contains(userDto.getAuthority()) && document.getTypeDocument() == 3 ) {
				documentsDtoGral.add(new DocumentDto(document.getId(), document.getRealName(), document.getViewName(), document.getDescription(),
						document.getAuthorizationsRole(), document.getTypeDocument()));
			}
		}
		documentDto.setDocumentsDtoSchedule(documentsDto);
		documentDto.setDocumentsDtoMap(documentsDtoMap);
		documentDto.setDocumentsDto(documentsDtoGral);
		LOGGER.info("{} - Termina carga de documentos.", userSecurityService.getIdSession());
		return documentDto;
	}

	@Override
	public DocumentDto findByRole(DocumentDto documentDto, AuthorityEnum role) {
		LOGGER.info("{} - Inicia carga de documentos por rol[{}].", userSecurityService.getIdSession(), role.toString());
		List<Document> documents = documentRepository.findByAuthorizationsRoleIgnoreCaseContaining(role.toString());
		List<DocumentDto> documentsDto = new ArrayList<>();
		List<DocumentDto> documentsDtoGral = new ArrayList<>();
		List<DocumentDto> documentsDtoMap = new ArrayList<>();
		for(Document document : documents){
			if(document.getTypeDocument() == 1 ) {
				documentsDto.add(new DocumentDto(document.getId(), document.getRealName(), document.getViewName(), document.getDescription(),
						document.getAuthorizationsRole(), document.getTypeDocument()));
			}
			if(document.getTypeDocument() == 2 ) {
				documentsDtoMap.add(new DocumentDto(document.getId(), document.getRealName(), document.getViewName(), document.getDescription(),
						document.getAuthorizationsRole(), document.getTypeDocument()));
			}
			if(document.getTypeDocument() == 3 ) {
				documentsDtoGral.add(new DocumentDto(document.getId(), document.getRealName(), document.getViewName(), document.getDescription(),
						document.getAuthorizationsRole(), document.getTypeDocument()));
			}
		}
		documentDto.setDocumentsDtoSchedule(documentsDto);
		documentDto.setDocumentsDtoMap(documentsDtoMap);
		documentDto.setDocumentsDto(documentsDtoGral);
		LOGGER.info("{} - Termina carga de documentos por rol[{}].", userSecurityService.getIdSession(), role.toString());
		return documentDto;
	}

	@Override
	public Resource loadFileAsResource(String id, UserDetails userDetails) throws ServiceException {
		LOGGER.info("{} - Inicia descarga de archivo[id - {}].", userSecurityService.getIdSession(),id);
		Document document = documentRepository.getOne(Long.valueOf(id));
		LOGGER.info("Document:{}", document.toString());
		UserDto userDto = null;
		try {
			userDto = userService.findByUsername(userDetails.getUsername());
		} catch (Exception e) {
			throw new ServiceException(e.getMessage());
		}
		
		if(!document.getAuthorizationsRole().contains(userDto.getAuthority())) {
			throw new ServiceException("El documento no est\u00E1 autorizado para el usuario.");
		}
		
		try {
			Path filePath = Paths
					.get(configurationRepository.findByKey(ConfigurationEnum.DOCUMENT_PATH.toString()).getValue()
							+ document.getRealName());
			Resource resource = new UrlResource(filePath.toUri());
			if (resource.exists()) {
				LOGGER.info("{} - Termina descarga de archivo[id - {}].", userSecurityService.getIdSession(),id);
				return resource;
			} else {
				throw new ServiceException("Archivo '"+document.getRealName()+"' no encontrado." );
			}
		} catch (MalformedURLException e) {
			throw new ServiceException("URL mal formada.");
		}
	}

	@Override
	public void delete(DocumentDto documenDto) throws ServiceException {
		LOGGER.info("{} - Inicia eliminar archivo[id - {}].", userSecurityService.getIdSession(),documenDto.getId());
		Document document = documentRepository.getOne(documenDto.getId());
		File file = null;
		try {
			file = new File(configurationRepository.findByKey(ConfigurationEnum.DOCUMENT_PATH.toString()).getValue()
					+ document.getRealName());
			
		} catch (Exception ex) {
			LOGGER.error("Error generar archivo:{}",
					configurationRepository.findByKey(ConfigurationEnum.DOCUMENT_PATH.toString()).getValue()
							+ document.getRealName());
			throw new ServiceException("No se pudo eliminar el registro.");
		}

		if (!file.exists()) {
			LOGGER.error("El archivo {} no existe.", document.getRealName());
			throw new ServiceException("El documento '" + document.getViewName() + "' no existe.");
		} else {
			file.delete();
			LOGGER.info("{} - Termina eliminar archivo[id - {}].", userSecurityService.getIdSession(),documenDto.getId());
			try {
				documentRepository.delete(document);
			} catch (Exception ex) {
				throw new ServiceException("No se pudo eliminar el registro.");
			}
		}
	}

	@Override
	public ByteArrayInputStream exportListExcelByGroupId(Long ID) throws IOException {
		LOGGER.info("{} - DocumentServiceImpl - Recibiendo como parametro ID de grupo: {}", userSecurityService.getIdSession(),ID);
		/* Obtenemos el listado de las clases ofertadas del grupo */
		List<OfferedClass> idClasses= offeredClassRepository.findByGroup(ID);
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		Workbook workbook = new HSSFWorkbook();
		/* Colocamos en negrita */
		CellStyle headerStyle = workbook.createCellStyle();
        Font font = workbook.createFont();
        font.setBold(true);
        headerStyle.setFont(font);
		/* Recorremos el arreglo de las clases ofertadas */
		for(OfferedClass xClass : idClasses) {
		String teacher = "";
		String semester = "";
        // Se obtiene la lista
		LOGGER.info("Obteniendo lista de alumnos de la clase ofertada."+xClass.getId());
		List<StudentCharge> charge = studentChargeRepository.findStudentChargeByOfferedClassId(xClass.getId());
		// Para guardar la lista de alumnos
		List<StudentDto> studentsDto = new ArrayList<>();
		// Cargando alumnos en arreglo de dtos
		long i = 1;
		for(StudentCharge ch : charge){
			teacher = ch.getOfferedClass().getTeacher().getName()+" "+ch.getOfferedClass().getTeacher().getLastname()+" "+ch.getOfferedClass().getTeacher().getSecondLastname();
			semester = String.valueOf(ch.getOfferedClass().getSemester());
			StudentDto st = studentConverter.convertStudent2StudentDto(ch.getStudent());
			st.setCounter(i);
			studentsDto.add(st);
			LOGGER.info("student:{}",ch.getStudent().getRegister());
			i++;
		}
		/*Definiendo los encabezados de la lista */
		String [] columns = {"No.","MATRÍCULA", "NOMBRE ALUMNO","CORREO", "TELÉFONO"};
		Sheet sheet;
		if(xClass.getUap().getName().length() < 10) {
			sheet = workbook.createSheet(xClass.getUap().getKey()+"-"+ xClass.getUap().getName());
		} else {
			sheet = workbook.createSheet(xClass.getUap().getKey()+"-"+ xClass.getUap().getName().substring(0, 10));
		}
		
		
		Row row = sheet.createRow(0);
		/* Colocamos los datos de la materia */
		//row = sheet.createRow(0);
		sheet.addMergedRegion(new CellRangeAddress(0,0,0,1));
		Cell cellPlan = row.createCell(0);
		cellPlan.setCellValue("PLAN DE ESTUDIOS: ");
		cellPlan.setCellStyle(headerStyle);
		sheet.addMergedRegion(new CellRangeAddress(0,0,2,3)); 
		row.createCell(2).setCellValue(xClass.getUap().getEducationalProgram().getStudentPlanName());
		sheet.addMergedRegion(new CellRangeAddress(0,0,4,5)); 
		Cell cellLectivo = row.createCell(4);
		cellLectivo.setCellValue("P. LECTIVO: ");
		cellLectivo.setCellStyle(headerStyle);
		sheet.addMergedRegion(new CellRangeAddress(0,0,6,7)); 
		row.createCell(6).setCellValue("FEBRERO - JULIO 2022");
        // Ahora creamos una fila en la posicion 1
		row = sheet.createRow(1);
		sheet.addMergedRegion(new CellRangeAddress(1,1,0,1));
		Cell cellUap = row.createCell(0);
		cellUap.setCellValue("UNIDAD DE APRENDIZAJE: ");
		cellUap.setCellStyle(headerStyle);
		sheet.addMergedRegion(new CellRangeAddress(1,1,2,6)); 
		row.createCell(2).setCellValue(xClass.getUap().getName());
		
        // Ahora creamos una fila en la posicion 2
		row = sheet.createRow(2);
		sheet.addMergedRegion(new CellRangeAddress(2,2,0,1));
		Cell cellGrupo = row.createCell(0);
		cellGrupo.setCellValue("GRUPO: ");
		cellGrupo.setCellStyle(headerStyle);
		sheet.addMergedRegion(new CellRangeAddress(2,2,2,3)); 
		row.createCell(2).setCellValue(xClass.getGroup().getName());
		sheet.addMergedRegion(new CellRangeAddress(2,2,4,5)); 
		Cell cellTurno = row.createCell(4);
		cellTurno.setCellValue("TURNO: ");
		cellTurno.setCellStyle(headerStyle);
		sheet.addMergedRegion(new CellRangeAddress(2,2,6,7)); 
		row.createCell(6).setCellValue(xClass.getGroup().getTurn());
		// Ahora creamos una fila en la posicion 2
		row = sheet.createRow(3);
		sheet.addMergedRegion(new CellRangeAddress(3,3,0,1));
		Cell cellDocente = row.createCell(0);
		cellDocente.setCellValue("DOCENTE: ");
		cellDocente.setCellStyle(headerStyle);
		
		sheet.addMergedRegion(new CellRangeAddress(3,3,2,6)); 
		row.createCell(2).setCellValue(teacher);
		//row2 = sheet.createRow(1);
		row = sheet.createRow(5);
		for(int k = 0; k < columns.length; k++) {
			Cell cell = row.createCell(k);
			cell.setCellValue(columns[k]);
			cell.setCellStyle(headerStyle);
		}
		/* Colocamos los datos de cada alumno	 */
		int initRow = 6;
		int no = 1;
		for(StudentDto studentDtoTemp : studentsDto) {
			row = sheet.createRow(initRow);
			row.createCell(0).setCellValue(no);
			row.createCell(1).setCellValue(studentDtoTemp.getRegister());
			row.createCell(2).setCellValue(studentDtoTemp.getName()+" "+studentDtoTemp.getLastname()+" "+studentDtoTemp.getSecondLastname());
			row.createCell(3).setCellValue(studentDtoTemp.getEmail());
			row.createCell(4).setCellValue(studentDtoTemp.getPhone());		
			initRow++;
			no++;
		}
		
		
	} //Termina iteracion de clases ofertadas
		workbook.write(stream);
		workbook.close();		
		LOGGER.info("{} - TutorshipServiceImpl - Generacion del listado en excel exitosa", userSecurityService.getIdSession());
		return new ByteArrayInputStream(stream.toByteArray());
	}

	@Override
	public ByteArrayInputStream exportListExcelByTeacherId(String ID) throws IOException {
		LOGGER.info("{} - DocumentServiceImpl - Recibiendo como parametro ID de docente: {}", userSecurityService.getIdSession(),ID);
		/* obtenemos las clases asociadas al docente recibido */
		int randomId = 1;
		List<OfferedClass> idClasses = offeredClassRepository.findByTeacherRegisterActivePeriod(ID);
		/* Obtenemos el listado de las clases ofertadas del grupo */
		//List<OfferedClass> idClasses= offeredClassRepository.findByGroup(ID);
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		Workbook workbook = new HSSFWorkbook();
		/* Colocamos en negrita */
		CellStyle headerStyle = workbook.createCellStyle();
        Font font = workbook.createFont();
        font.setBold(true);
        headerStyle.setFont(font);
		/* Recorremos el arreglo de las clases ofertadas */
		for(OfferedClass xClass : idClasses) {
		String teacher = "";
		String semester = "";
        // Se obtiene la lista
		LOGGER.info("Obteniendo lista de alumnos de la clase ofertada."+xClass.getId());
		List<StudentCharge> charge = studentChargeRepository.findStudentChargeByOfferedClassId(xClass.getId());
		// Para guardar la lista de alumnos
		List<StudentDto> studentsDto = new ArrayList<>();
		// Cargando alumnos en arreglo de dtos
		long i = 1;
		for(StudentCharge ch : charge){
			teacher = ch.getOfferedClass().getTeacher().getName()+" "+ch.getOfferedClass().getTeacher().getLastname()+" "+ch.getOfferedClass().getTeacher().getSecondLastname();
			semester = String.valueOf(ch.getOfferedClass().getSemester());
			StudentDto st = studentConverter.convertStudent2StudentDto(ch.getStudent());
			st.setCounter(i);
			studentsDto.add(st);
			i++;
		}
		/*Definiendo los encabezados de la lista */
		String [] columns = {"No.","MATRÍCULA", "NOMBRE ALUMNO","CORREO", "TELÉFONO"};
		Sheet sheet;
		if(xClass.getUap().getName().length() < 10) {
			sheet = workbook.createSheet(randomId+"-"+xClass.getUap().getKey()+"-"+ xClass.getUap().getName());
		} else {
			sheet = workbook.createSheet(randomId+"-"+xClass.getUap().getKey()+"-"+ xClass.getUap().getName().substring(0, 10));
		}

		Row row = sheet.createRow(0);
		/* Colocamos los datos de la materia */
		//row = sheet.createRow(0);
		sheet.addMergedRegion(new CellRangeAddress(0,0,0,1));
		Cell cellPlan = row.createCell(0);
		cellPlan.setCellValue("PLAN DE ESTUDIOS: ");
		cellPlan.setCellStyle(headerStyle);
		sheet.addMergedRegion(new CellRangeAddress(0,0,2,3)); 
		row.createCell(2).setCellValue(xClass.getUap().getEducationalProgram().getStudentPlanName());
		sheet.addMergedRegion(new CellRangeAddress(0,0,4,5)); 
		Cell cellLectivo = row.createCell(4);
		cellLectivo.setCellValue("P. LECTIVO: ");
		cellLectivo.setCellStyle(headerStyle);
		sheet.addMergedRegion(new CellRangeAddress(0,0,6,7)); 
		row.createCell(6).setCellValue("FEBRERO - JULIO 2022");
        // Ahora creamos una fila en la posicion 1
		row = sheet.createRow(1);
		sheet.addMergedRegion(new CellRangeAddress(1,1,0,1));
		Cell cellUap = row.createCell(0);
		cellUap.setCellValue("UNIDAD DE APRENDIZAJE: ");
		cellUap.setCellStyle(headerStyle);
		sheet.addMergedRegion(new CellRangeAddress(1,1,2,6)); 
		row.createCell(2).setCellValue(xClass.getUap().getName());
		
        // Ahora creamos una fila en la posicion 2
		row = sheet.createRow(2);
		sheet.addMergedRegion(new CellRangeAddress(2,2,0,1));
		Cell cellGrupo = row.createCell(0);
		cellGrupo.setCellValue("GRUPO: ");
		cellGrupo.setCellStyle(headerStyle);
		sheet.addMergedRegion(new CellRangeAddress(2,2,2,3)); 
		row.createCell(2).setCellValue(xClass.getGroup().getName());
		sheet.addMergedRegion(new CellRangeAddress(2,2,4,5)); 
		Cell cellTurno = row.createCell(4);
		cellTurno.setCellValue("TURNO: ");
		cellTurno.setCellStyle(headerStyle);
		sheet.addMergedRegion(new CellRangeAddress(2,2,6,7)); 
		row.createCell(6).setCellValue(xClass.getGroup().getTurn());
		// Ahora creamos una fila en la posicion 2
		row = sheet.createRow(3);
		sheet.addMergedRegion(new CellRangeAddress(3,3,0,1));
		Cell cellDocente = row.createCell(0);
		cellDocente.setCellValue("DOCENTE: ");
		cellDocente.setCellStyle(headerStyle);
		
		sheet.addMergedRegion(new CellRangeAddress(3,3,2,6)); 
		row.createCell(2).setCellValue(teacher);
		//row2 = sheet.createRow(1);
		row = sheet.createRow(5);
		for(int k = 0; k < columns.length; k++) {
			Cell cell = row.createCell(k);
			cell.setCellValue(columns[k]);
			cell.setCellStyle(headerStyle);
		}
		/* Colocamos los datos de cada alumno	 */
		int initRow = 6;
		int no = 1;
		for(StudentDto studentDtoTemp : studentsDto) {
			row = sheet.createRow(initRow);
			row.createCell(0).setCellValue(no);
			row.createCell(1).setCellValue(studentDtoTemp.getRegister());
			row.createCell(2).setCellValue(studentDtoTemp.getName()+" "+studentDtoTemp.getLastname()+" "+studentDtoTemp.getSecondLastname());
			row.createCell(3).setCellValue(studentDtoTemp.getEmail());
			row.createCell(4).setCellValue(studentDtoTemp.getPhone());		
			initRow++;
			no++;
		}
		randomId++;
		
	} //Termina iteracion de clases ofertadas
		workbook.write(stream);
		workbook.close();		
		LOGGER.info("{} - TutorshipServiceImpl - Generacion del listado en excel exitosa", userSecurityService.getIdSession());
		return new ByteArrayInputStream(stream.toByteArray());
	}

	@Override
	public ByteArrayInputStream exportStudentsListExcelByGroupId(Long id) throws IOException {
		LOGGER.info("{} - DocumentServiceImpl - Recibiendo como parametro ID de grupo: {}", userSecurityService.getIdSession(),id);
		/* Obtenemos el listado de los alumnos que tienen asignado ese grupo */
		List<Student> listStudents = studentRepository.findByGroupId(id);
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		Workbook workbook = new HSSFWorkbook();
		 /*Definiendo los encabezados de la lista */
		String [] columns = {"No.","MATRÍCULA", "NOMBRE ALUMNO","SEMESTRE", "GRUPO"};
		/* Colocamos en negrita */
		CellStyle headerStyle = workbook.createCellStyle();
        Font font = workbook.createFont();
        font.setBold(true);
        headerStyle.setFont(font);
    	Sheet sheet = workbook.createSheet("Alumnos del grupo");
		Row row = sheet.createRow(0);
        row = sheet.createRow(5);
		for(int k = 0; k < columns.length; k++) {
			Cell cell = row.createCell(k);
			cell.setCellValue(columns[k]);
			cell.setCellStyle(headerStyle);
		}
       
		/* Colocamos los datos de cada alumno	 */
		int initRow = 6;
		int no = 1;
		for(Student studentDtoTemp : listStudents) {
			row = sheet.createRow(initRow);
			row.createCell(0).setCellValue(no);
			row.createCell(1).setCellValue(studentDtoTemp.getRegister());
			row.createCell(2).setCellValue(studentDtoTemp.getName()+" "+studentDtoTemp.getLastname()+" "+studentDtoTemp.getSecondLastname());
			row.createCell(3).setCellValue(studentDtoTemp.getSemester());	
			row.createCell(4).setCellValue(studentDtoTemp.getGroup().getName()+" "+studentDtoTemp.getGroup().getAbbreviation()+" "+studentDtoTemp.getGroup().getEducationalProgram().getAbr());
			initRow++;
			no++;
		}

		workbook.write(stream);
		workbook.close();		
		LOGGER.info("{} - TutorshipServiceImpl - Generacion del listado en excel exitosa", userSecurityService.getIdSession());
		return new ByteArrayInputStream(stream.toByteArray());
	}

}
