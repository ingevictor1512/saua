package com.mx.saua.service.impl;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Service;

import com.mx.saua.converter.StudentConverter;
import com.mx.saua.dto.GroupDto;
import com.mx.saua.dto.StudentDto;
import com.mx.saua.dto.UapDto;
import com.mx.saua.entity.Group;
import com.mx.saua.entity.OfferedClass;
import com.mx.saua.entity.StudentCharge;
import com.mx.saua.entity.Uap;
import com.mx.saua.exception.ServiceException;
import com.mx.saua.repository.GroupRepository;
import com.mx.saua.repository.OfferedClassRepository;
import com.mx.saua.repository.StudentChargeRepository;
import com.mx.saua.repository.UapRepository;
import com.mx.saua.service.ListService;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;

@Service("listService")
public class ListServiceImpl implements ListService {

	private Logger log = LoggerFactory.getLogger(ListServiceImpl.class);

	@Autowired
	private StudentChargeRepository studentChargeRepository;

	@Autowired
	private GroupRepository groupRepository;

	@Autowired
	private UapRepository uapRepository;
	
	@Autowired
	private OfferedClassRepository offeredClassRepository;

	@Autowired
	private StudentConverter studentConverter;

	@Override
	public byte[] studentListByGroupUap(GroupDto groupDto) throws ServiceException {
		String teacher = "";
		String semester = "";
		try {
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			// Se obtiene la información del grupo
			log.info("Generando lista de:{}", groupDto.toString());
			Group group = groupRepository.getOne(Long.valueOf(groupDto.getIdGroup()));
			log.info("Grupo obtenido:[{} - {}][{}]", group.getName(), group.getTurn(), group.getEducationalProgram().getAbr());
			// Se obtiene la información de la materia a generar la lista
			//Uap uap = uapRepository.getOne(groupDto.getIdUap());//Aqui obtener la uap a partir del id de la clase ofertada
			OfferedClass offeredClass = offeredClassRepository.getOne(Long.valueOf(groupDto.getIdUap()));
			log.info("Clase ofertada obtenida:[{}] - {}", offeredClass.getUap().getKey(),offeredClass.getUap().getName());

			// Se obtiene la lista
			log.info("Obteniendo lista de alumnos.");
			List<StudentCharge> charge = studentChargeRepository.findStudentChargeByOfferedClassId(offeredClass.getId());
			// Para guardar la lista de alumnos
			List<UapDto> uapDtos = new ArrayList<>();
			UapDto uapDto = new UapDto();
			List<StudentDto> studentsDto = new ArrayList<>();
			// Cargando alumnos en arreglo de dtos
			long i = 1;
			for(StudentCharge ch : charge){
				teacher = ch.getOfferedClass().getTeacher().getName()+" "+ch.getOfferedClass().getTeacher().getLastname()+" "+ch.getOfferedClass().getTeacher().getSecondLastname();
				semester = String.valueOf(ch.getOfferedClass().getSemester());
				StudentDto st = studentConverter.convertStudent2StudentDto(ch.getStudent());
				st.setCounter(i);
				studentsDto.add(st);
				log.info("student:{}",ch.getStudent().getRegister());
				i++;
			}
			uapDto.setStudentsDto(studentsDto);
			uapDtos.add(uapDto);
			// Se crea el datasource con los alumnos obtenidos
			JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(uapDtos);

			log.info("Leyendo jasper.");
			InputStream archivo = StudentServiceImpl.class.getResourceAsStream("/studentList.jasper");
			log.info("Seteando parametros.");
			Map<String, Object> parameters = new HashMap<>();
			parameters.put("P_GROUP", group.getName()+" ["+group.getEducationalProgram().getAbr()+"]");
			parameters.put("P_UAP", offeredClass.getUap().getName());
			parameters.put("P_STUDY_PLAIN", group.getEducationalProgram().getStudentPlanName());
			parameters.put("P_SCHOOL_PERIOD", group.getPeriod().getSchoolPeriod());
			parameters.put("P_TEACHER", teacher);
			parameters.put("P_SEMESTER", semester);
			parameters.put("P_TURN", group.getTurn());
			
			JasperPrint jasperPrint = JasperFillManager.fillReport(archivo, parameters, dataSource);

			// Para exportar a pdf
			JRPdfExporter exporter = new JRPdfExporter();

			exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
			exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, baos);
			//Exporta a pdf
			exporter.exportReport();

			byte[] out = baos.toByteArray();
			log.info("Lista generada correctamente");

			return out;
		} catch (JRException e) {
			throw new ServiceException("Ocurri\u00F3 un error al generar la lista.["+e.getMessage()+"]");
		}
	}

}
