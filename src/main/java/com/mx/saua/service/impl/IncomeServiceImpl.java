package com.mx.saua.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import com.mx.saua.dto.IncomeDto;
import com.mx.saua.entity.Income;
import com.mx.saua.repository.IncomeRepository;
import com.mx.saua.service.IncomeService;

@Service("incomeService")
public class IncomeServiceImpl implements IncomeService{
	/*
	 * @Vic
	 */
	@Autowired
	private IncomeRepository incomeRepository;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(IncomeServiceImpl.class);

	@Override
	public List<IncomeDto> find() {
		List<IncomeDto> incomeDtos = new ArrayList<>();
		List<Income> incomes = incomeRepository.findAll();
		incomes.stream().forEach(p->{
			incomeDtos.add(new IncomeDto(p.getId(), p.getDescription()));
		});
		LOGGER.info("Busqueda de tipo de ingresos realizada correctamente:{}", incomeDtos.toString());
		return incomeDtos;
	}
	
	@Override
	public IncomeDto getById(Long id) {
		Income income = incomeRepository.getOne(id);
		IncomeDto incomeDto = new IncomeDto();
		BeanUtils.copyProperties(income, incomeDto);
		LOGGER.info("Busqueda por tipos de ingreso realizada correctamente");
		return incomeDto;
	}
	
	@Override
	public Income getIncomeById(Long id) {
		Income income = incomeRepository.getOne(id);
		LOGGER.info("Busqueda por tipos de ingreso realizada correctamente");
		return income;
	}
	
	@Override
	public void save(IncomeDto incomeDto) {
		if(incomeDto.getId() > -1L) {
			/* Update */
			Income incomeUpdate = new Income();
			BeanUtils.copyProperties(incomeDto, incomeUpdate);
			incomeRepository.save(incomeUpdate);
			LOGGER.info("Actualizacion del tipo de ingreso realizada correctamente");
		} else {
			/* New register */
			Income incomeInsert = new Income();
			BeanUtils.copyProperties(incomeDto, incomeInsert);
			incomeRepository.save(incomeInsert);
			LOGGER.info("Insercion del tipo de ingreso realizada correctamente");
		}
		
	}

	@Override
	public void delete(Long id) {
		try {
			incomeRepository.deleteById(id);
			LOGGER.info("Eliminacion del tipo de ingreso realizada correctamente");
		} catch (DataIntegrityViolationException dve) {
			dve.printStackTrace();
		}
		
	} 


	/* GETTERS Y SETTERS */
	public IncomeRepository getIncomeRepository() {
		return incomeRepository;
	}

	public void setIncomeRepository(IncomeRepository incomeRepository) {
		this.incomeRepository = incomeRepository;
	}
	

	

}
