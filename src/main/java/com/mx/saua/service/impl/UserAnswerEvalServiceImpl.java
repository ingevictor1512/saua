package com.mx.saua.service.impl;

import java.util.List;

import com.mx.saua.entity.*;
import com.mx.saua.enums.ConfigurationEnum;
import com.mx.saua.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mx.saua.dto.StudentChargeDto;
import com.mx.saua.repository.AnswerUserAutoEvalRepository;
import com.mx.saua.repository.AnswerUserTeacherEvalRepository;
import com.mx.saua.repository.PeriodRepository;
import com.mx.saua.repository.StudentChargeRepository;
import com.mx.saua.service.UserAnswerEvalService;
import com.mx.saua.service.UserService;

/**
 * Servicio para el manejo de respuestas de evaluacion docente
 * 
 * @author Benito Morales
 * @version 1.0
 *
 */
@Service("userAnswerEvalService")
public class UserAnswerEvalServiceImpl implements UserAnswerEvalService {

	/**
	 * Inyección de AnswerUserTeacherEvalRepository
	 */
	@Autowired
	private AnswerUserTeacherEvalRepository answerUserTeacherEvalRepository;

	/**
	 * Inyección de StudentChargeRepository
	 */
	@Autowired
	private StudentChargeRepository studentChargeRepository;

	/**
	 * Inyección de AnswerUserTeacherEvalRepository
	 */
	@Autowired
	private AnswerUserAutoEvalRepository answerAutoEvalRepository;

	@Autowired
	private PeriodRepository periodRepository;
	
	@Autowired
	private UserService userService;

	@Autowired
	private InfoEvaluationRepository infoEvaluationRepository;
	
	@Autowired
	private ConfigurationRepository configurationRepository;

	@Override
	public StudentChargeDto getStatusEvaluations(StudentChargeDto dto) {
		/* Se obtiene el periodo anterior*/
		Configuration conf = configurationRepository.findByKey(ConfigurationEnum.PREVIOUS_PERIOD.toString());
		Period period = periodRepository.getOne(Long.valueOf(conf.getValue()));
		
		for (StudentChargeDto stCharge : dto.getStudentCharges()) {
			User user = userService.findUserByUsername(stCharge.getStudent().getRegister());
			List<AnswersUserTeacherEval> list = answerUserTeacherEvalRepository
					.findByUserAndStudentChargeAndStudentChargePeriod(user,
							studentChargeRepository.getOne(stCharge.getId()),period);
			stCharge.setTeacherEvalAnswered(list.size() > 0);
		}

		for (StudentChargeDto stCharge : dto.getStudentCharges()) {
			User user = userService.findUserByUsername(stCharge.getStudent().getRegister());
			List<AnswersUserAutoEval> listAuto = answerAutoEvalRepository
					.findByUserAndStudentChargeAndStudentChargePeriod(user,
							studentChargeRepository.getOne(stCharge.getId()), period);
			stCharge.setAutoevalAnswered(listAuto.size() > 0);
		}
		return dto;
	}

	@Override
	public int totalAutoSurveyAnswered() {
		/* Se obtiene el periodo anterior*/
		Configuration conf = configurationRepository.findByKey(ConfigurationEnum.PREVIOUS_PERIOD.toString());
		Period period = periodRepository.getOne(Long.valueOf(conf.getValue()));
		List<InfoEvaluation> evaluations = infoEvaluationRepository.findInfoEvaluationByIdInfoEvaluation_Period_IdAndTotalAutoEvalGreaterThan(period.getId(), 0);
		return evaluations.size();
	}

	@Override
	public int totalSurveyDocentAnswered() {
		/* Se obtiene el periodo anterior*/
		Configuration conf = configurationRepository.findByKey(ConfigurationEnum.PREVIOUS_PERIOD.toString());
		Period period = periodRepository.getOne(Long.valueOf(conf.getValue()));
		List<InfoEvaluation> evaluations = infoEvaluationRepository.findInfoEvaluationByIdInfoEvaluation_Period_IdAndTotalTeacherEvalGreaterThan(period.getId(), 0);
		return evaluations.size();
	}

}
