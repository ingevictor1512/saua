package com.mx.saua.service.impl;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.mx.saua.entity.*;
import com.mx.saua.repository.*;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.mx.saua.converter.GroupConverter;
import com.mx.saua.converter.PeriodConverter;
import com.mx.saua.converter.ProgramConverter;
import com.mx.saua.converter.TeacherConverter;
import com.mx.saua.converter.UapConverter;
import com.mx.saua.dto.CategoryDto;
import com.mx.saua.dto.OfferedClassDto;
import com.mx.saua.dto.PeriodDto;
import com.mx.saua.dto.ResultDto;
import com.mx.saua.dto.StudentChargeDto;
import com.mx.saua.dto.StudentDto;
import com.mx.saua.dto.TeacherDto;
import com.mx.saua.dto.TeacherEvalReportDto;
import com.mx.saua.enums.ConfigurationEnum;
import com.mx.saua.enums.NotificationStatusEnum;
import com.mx.saua.enums.NotificationTypeEnum;
import com.mx.saua.exception.ServiceException;
import com.mx.saua.service.TeacherService;
import com.mx.saua.util.SecurityUtils;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;

/**
 * Service que maneja la lógica de negocio de los profesores
 * 
 * @author Benito Morales
 * @version 1.0
 *
 */
@Service("teacherService")
public class TeacherServiceImpl implements TeacherService {

	private Logger log = LoggerFactory.getLogger(TeacherServiceImpl.class);

	@Autowired
	private TeacherRepository teacherRepository;

	@Autowired
	private TeacherConverter teacherConverter;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private UserNotificationRepository userNotificationRepository;

	@Autowired
	private CategoryRepository categoryRepository;
	/**
	 * Inyeccion de userSecurityService
	 */
	@Autowired
	private UserSecurityService userSecurityService;
	@Autowired
	private OfferedClassRepository offeredClassRepository;
	@Autowired
	private GroupConverter groupConverter;
	@Autowired
	private UapConverter uapConverter;
	@Autowired
	private AnswerUserTeacherEvalRepository answerUserTeacherEvalRepository;
	@Autowired
	private AnswerUserAutoEvalRepository answerUserAutoEvalRepository;
	@Autowired
	private InfoEvaluationRepository infoEvaluationRepository;
	@Autowired
	private ConfigurationRepository configurationRepository;
	@Autowired
	private PeriodRepository periodRepository;
	@Autowired
	private PeriodConverter periodConverter;
	
	@Override
	public TeacherDto find(TeacherDto teacherDto) {
		List<Teacher> teachers = teacherRepository.findAllOrderByNameLastnameSecondLastnameAsc();
		List<TeacherDto> teachersDto = new ArrayList<>();
		teachers.stream().forEach(p->{
			p.setCubicle((p.getCubicle()!=null)?p.getCubicle():"");
			teachersDto.add(teacherConverter.convertTeacher2TeacherDto(p));
		});
		teacherDto.setTeachers(teachersDto);
		
		Configuration configPreviousPeriod = configurationRepository.findByKey(ConfigurationEnum.PREVIOUS_PERIOD.toString());
		Period actualPeriod = periodRepository.getOneByActivePeriod(true);
		Period previousPeriod = periodRepository.getOne(Long.valueOf(configPreviousPeriod.getValue()));
		
		List<PeriodDto> periods = new ArrayList<>();
		periods.add(periodConverter.convertPeriod2PeriodDto(actualPeriod));
		periods.add(periodConverter.convertPeriod2PeriodDto(previousPeriod));
		teacherDto.setPeriods(periods);
		return teacherDto;
	}

	@Override
	public void save(TeacherDto teacherDto) throws ServiceException {
		Date current = new Date();
		if (teacherDto.getId() > 0) {
			// Editar profesor
			Teacher teacher = teacherRepository.getOneByRegister(teacherDto.getRegister());
			User user = userRepository.getOne(teacher.getUsers().iterator().next().getIdUser());
			teacherDto.setRegisterDate(teacher.getRegisterDate());
			teacherDto.setEditDate(current);
			Category category = categoryRepository.getOne(teacherDto.getCategoryDto().getId());
			teacherDto.setCategoryDto(new CategoryDto(category.getId(), category.getDescription()));
			teacherRepository.save(teacherConverter.convertTeacherDto2Teacher(teacherDto));
			
			user.setEmail(teacherDto.getEmail());
			user.setEditdate(current);
			user.getAuthorities().iterator().next().setAuthority(teacherDto.getAuthority());
			userRepository.save(user);
		} else {
			// Nuevo registro
			if (teacherRepository.getOneByRegister(teacherDto.getRegister()) != null) {
				throw new ServiceException("Ya se ecuentra registrado algui\u00E9n con este n\u00FAmero de empleado.");
			}
			try {
				teacherDto.setId(Long.valueOf(teacherDto.getRegister()));
				Category category = categoryRepository.getOne(teacherDto.getCategoryDto().getId());
				teacherDto.setCategoryDto(new CategoryDto(category.getId(), category.getDescription()));
				teacherDto.setRegisterDate(current);
				teacherDto.setEditDate(current);

				User user = new User();
				user.setActive(false);

				// Rol
				Set<Authority> authorities = new HashSet<>();
				Authority auth = new Authority();
				auth.setAuthority(teacherDto.getAuthority());
				auth.setRegisterDate(current);
				auth.setEditDate(current);
				auth.setUser(user);
				authorities.add(auth);
				user.setAuthorities(authorities);
				user.setEditdate(current);
				user.setEmail(teacherDto.getEmail());
				user.setExpirationDate(null);
				user.setInquest(false);
				user.setFirstLogin(true);
				user.setLocked(false);
				user.setActive(true);

				String password = SecurityUtils.generateRandomPassword();
				user.setPassword(passwordEncoder.encode(password));

				user.setRegisterDate(current);
				user.setStatus(true);
				user.setTeacher(teacherConverter.convertTeacherDto2Teacher(teacherDto));
				user.setUsername(teacherDto.getRegister());
				userRepository.save(user);
				
				User userForNotification = userRepository.findOneByEmailAndUsername(user.getEmail(),
						user.getUsername());
				// Generar notificación
				UserNotification userNotification = new UserNotification();
				userNotification.setAddresseeMail(user.getEmail());
				userNotification.setAddresseeName(user.getTeacher().getName());
				userNotification.setUser(userForNotification);
				userNotification.setPlainPassword(password);
				userNotification.setReviewable(false);
				userNotification.setStatus(NotificationStatusEnum.REGISTRADA);
				userNotification.setType(NotificationTypeEnum.REGISTER);
				userNotificationRepository.save(userNotification);
			} catch (Exception ex) {
				ex.printStackTrace();
				throw new ServiceException("Ocurri\u00F3 un problema al guardar el profesor");
			}
		}
	}

	@Override
	public TeacherDto getTeacherByRegister(String register) throws ServiceException {
		Teacher teacher = teacherRepository.getOneByRegister(register);
		return teacherConverter.convertTeacher2TeacherDto(teacher);
	}

	@Override
	public void delete(TeacherDto teacherDto) throws ServiceException {
		Teacher teacher = teacherRepository.getOne(Long.valueOf(teacherDto.getRegister()));
		teacherRepository.delete(teacher);
	}

	@Override
	public List<TeacherDto> findAll() {
		List<Teacher> teachers = teacherRepository.findAllOrderByNameLastnameSecondLastnameAsc();
		List<TeacherDto> teachersDto = new ArrayList<>();
		teachers.stream().forEach(p->{
			teachersDto.add(teacherConverter.convertTeacher2TeacherDto(p));
		});
		return teachersDto;
	}

	@Override
	public Object[][] findObject() {
		List<Teacher> list = teacherRepository.findAllOrderByNameLastnameSecondLastnameAsc();
		Object[][] arrObject = new Object[list.size()][2];
		int i = 0;
		for(Teacher teacher: list) {
			arrObject[i][0] = teacher.getRegister();
			arrObject[i][1] = teacher.getName()+" "+teacher.getLastname()+" "+teacher.getSecondLastname();
			i++;
		}
		return arrObject;
	}

	@Override
	public Resource loadEvaluacionTeacherAsResource(String register) throws ServiceException {
		log.info("{} - Inicia loadEvaluacionTeacherAsResource[{}].", userSecurityService.getIdSession(), register);
		User userpapeleta = userRepository.findByUsername(register)
				.orElseThrow(() -> new ServiceException("Usuario no encontrado."));
		User userSolicitud = userRepository.findByUsername(userSecurityService.getUserCurrentDetails().getUsername())
				.orElseThrow(() -> new ServiceException("Usuario no encontrado."));


		Teacher teacher = teacherRepository.getOne(userpapeleta.getTeacher().getId());
				
		
		byte[] pdf = null;
		try {
			pdf = exportToPdfFile(Long.parseLong(teacher.getRegister()));
			
			Resource resource = new ByteArrayResource(pdf, "Evaluacion docente.");
			if (resource.exists()) {
				log.info("{} - Termina loadEvaluacionTeacherAsResource[{}].", userSecurityService.getIdSession(), register);
				return resource;
			} else {
				log.info("{} - Archivo no encontrado.", userSecurityService.getIdSession());
				throw new ServiceException("Archivo no encontrado.");
			}
		}catch(ServiceException se) {
			log.error("{} - Error al genera papeleta:{}", userSecurityService.getIdSession(), se.getMessage());
			throw new ServiceException("La generación y descarga de la papeleta terminó con error.");
		}
	}

	@Override
	public byte[] exportToPdfFile(Long register) throws ServiceException {
		log.info("{} - Iniciar exportToPdfFile[{}].", userSecurityService.getIdSession(), register);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		List<TeacherEvalReportDto> teacherEval = new ArrayList<>();
		List<TeacherEvalReportDto> prueba = new ArrayList<>();

		List<ResultDto> dtos = new ArrayList<>();
		
		Teacher teacher = null;
		
		try {
			teacher = teacherRepository.getOneByRegister(String.valueOf(register));
		}catch(Exception ex) {
			throw new ServiceException("Ocurri\u00F3 un error al consultar los datos del docente:" + register);
		}
		
		if(teacher == null) {
			log.info("{} - El docente no existe.", userSecurityService.getIdSession());
			throw new ServiceException("El docente buscado no existe.");
		}
		TeacherEvalReportDto zDto = new TeacherEvalReportDto();
		zDto.setId(1);
		teacherEval.add(zDto);
		//-----------------------------------------------------------------
		ResultDto wDto = new ResultDto();
		wDto.setName("APRENDIZAJE SIGNIFICATIVO");
		/* Obtenemos la calificacion de la categoria */
		List<AnswersUserTeacherEval> questionsEvalswDto = answerUserTeacherEvalRepository.findByTeacherAndQuestionId(teacher.getRegister(), 31L);
	
		double promedio = 0.00;
		double sum=0;
		for(AnswersUserTeacherEval xAnswersUserTeacherEval : questionsEvalswDto) {
			
			sum = sum + Double.parseDouble(xAnswersUserTeacherEval.getCustomizedAnswer());
		}
		promedio = sum /questionsEvalswDto.size();
		
		wDto.setScore(Math.round(promedio * Math.pow(10, 2)) / Math.pow(10, 2));
		dtos.add(wDto);
		//-----------------------------------------------------------------
		ResultDto aDto = new ResultDto();
		aDto.setName("PLANIFICACIÓN DEL APRENDIZAJE");
		/* Obtenemos la calificacion de la categoria */
		List<AnswersUserTeacherEval> questionsEvalsaDto = answerUserTeacherEvalRepository.findByTeacherAndQuestionId(teacher.getRegister(), 32L);
		
		double promedioaDto = 0.00;
		double sumaDto=0;
		for(AnswersUserTeacherEval xAnswersUserTeacherEval : questionsEvalsaDto) {
			
			sumaDto = sumaDto + Double.parseDouble(xAnswersUserTeacherEval.getCustomizedAnswer());
		}
		List<AnswersUserTeacherEval> questionsEvalsaDto2= answerUserTeacherEvalRepository.findByTeacherAndQuestionId(teacher.getRegister(), 33L);
		
		//double promedioaDto = 0.00;
		//double sumaDto=0;
		for(AnswersUserTeacherEval xAnswersUserTeacherEval : questionsEvalsaDto2) {
			
			sumaDto = sumaDto + Double.parseDouble(xAnswersUserTeacherEval.getCustomizedAnswer());
		}
		promedioaDto = sumaDto /(questionsEvalsaDto.size() + questionsEvalsaDto2.size());
		
		aDto.setScore(Math.round(promedioaDto * Math.pow(10, 2)) / Math.pow(10, 2));
		dtos.add(aDto);
		//-----------------------------------------------------------------
		ResultDto bDto = new ResultDto();
		bDto.setName("ESTRATEGIAS DE ENSEÑANZA-APRENDIZAJE");
		/* Obtenemos la calificacion de la categoria */
		List<AnswersUserTeacherEval> questionsEvalsbDto = answerUserTeacherEvalRepository.findByTeacherAndQuestionId(teacher.getRegister(), 34L);
		
		double promediobDto = 0.00;
		double sumbDto=0;
		for(AnswersUserTeacherEval xAnswersUserTeacherEval : questionsEvalsbDto) {
			
			sumbDto = sumbDto + Double.parseDouble(xAnswersUserTeacherEval.getCustomizedAnswer());
		}
		promediobDto = sumbDto /questionsEvalsbDto.size();
		
		bDto.setScore(Math.round(promediobDto * Math.pow(10, 2)) / Math.pow(10, 2));
		dtos.add(bDto);
		//-----------------------------------------------------------------
		ResultDto cDto = new ResultDto();
		cDto.setName("EVALUACIÓN DEL APRENDIZAJE");
		/* Obtenemos la calificacion de la categoria */
		List<AnswersUserTeacherEval> questionsEvalscDto = answerUserTeacherEvalRepository.findByTeacherAndQuestionId(teacher.getRegister(), 35L);
		
		double promediocDto = 0.00;
		double sumcDto=0;
		for(AnswersUserTeacherEval xAnswersUserTeacherEval : questionsEvalscDto) {
			
			sumcDto = sumcDto + Double.parseDouble(xAnswersUserTeacherEval.getCustomizedAnswer());
		}
		List<AnswersUserTeacherEval> questionsEvalscDto2= answerUserTeacherEvalRepository.findByTeacherAndQuestionId(teacher.getRegister(), 36L);
		
		for(AnswersUserTeacherEval xAnswersUserTeacherEval : questionsEvalscDto2) {
			
			sumcDto = sumcDto + Double.parseDouble(xAnswersUserTeacherEval.getCustomizedAnswer());
		}
		List<AnswersUserTeacherEval> questionsEvalscDto3= answerUserTeacherEvalRepository.findByTeacherAndQuestionId(teacher.getRegister(), 37L);
		
		for(AnswersUserTeacherEval xAnswersUserTeacherEval : questionsEvalscDto3) {
			
			sumcDto = sumcDto + Double.parseDouble(xAnswersUserTeacherEval.getCustomizedAnswer());
		}
		List<AnswersUserTeacherEval> questionsEvalscDto4= answerUserTeacherEvalRepository.findByTeacherAndQuestionId(teacher.getRegister(), 38L);
		
		for(AnswersUserTeacherEval xAnswersUserTeacherEval : questionsEvalscDto4) {
			
			sumcDto = sumcDto + Double.parseDouble(xAnswersUserTeacherEval.getCustomizedAnswer());
		}
		
		promediocDto = sumcDto /(questionsEvalscDto.size() + questionsEvalscDto2.size() + questionsEvalscDto3.size() + questionsEvalscDto4.size());
		
		cDto.setScore(Math.round(promediocDto * Math.pow(10, 2)) / Math.pow(10, 2));
		dtos.add(cDto);
		//-----------------------------------------------------------------
		ResultDto dDto = new ResultDto();
		dDto.setName("AMBIENTE DE APRENDIZAJE AUTÓNOMO Y COLABORACIÓN");
		/* Obtenemos la calificacion de la categoria */
		List<AnswersUserTeacherEval> questionsEvalsdDto = answerUserTeacherEvalRepository.findByTeacherAndQuestionId(teacher.getRegister(), 39L);
		
		double promediodDto = 0.00;
		double sumdDto=0;
		for(AnswersUserTeacherEval xAnswersUserTeacherEval : questionsEvalsdDto) {
			
			sumdDto = sumdDto + Double.parseDouble(xAnswersUserTeacherEval.getCustomizedAnswer());
		}
		List<AnswersUserTeacherEval> questionsEvalsdDto2= answerUserTeacherEvalRepository.findByTeacherAndQuestionId(teacher.getRegister(), 40L);
		
		for(AnswersUserTeacherEval xAnswersUserTeacherEval : questionsEvalsdDto2) {
		
			sumdDto = sumdDto + Double.parseDouble(xAnswersUserTeacherEval.getCustomizedAnswer());
		}
		promediodDto = sumdDto /(questionsEvalsdDto.size() + questionsEvalsdDto2.size());
		
		dDto.setScore(Math.round(promediodDto * Math.pow(10, 2)) / Math.pow(10, 2));
		dtos.add(dDto);
		//-------------------------------------------------------------------------------------------------------------------------------------------
		ResultDto eDto = new ResultDto();
		eDto.setName("AMBIENTE SANO E INTEGRAL DEL ESTUDIANTE");
		/* Obtenemos la calificacion de la categoria */
		List<AnswersUserTeacherEval> questionsEvalseDto = answerUserTeacherEvalRepository.findByTeacherAndQuestionId(teacher.getRegister(), 41L);
		
		double promedioeDto = 0.00;
		double sumeDto=0;
		for(AnswersUserTeacherEval xAnswersUserTeacherEval : questionsEvalseDto) {
			
			sumeDto = sumeDto + Double.parseDouble(xAnswersUserTeacherEval.getCustomizedAnswer());
		}
		promedioeDto = sumeDto /questionsEvalseDto.size();
		
		eDto.setScore(Math.round(promedioeDto * Math.pow(10, 2)) / Math.pow(10, 2));
		dtos.add(eDto);
		//-------------------------------------------------------------------------------------------------------------------------------------------
		ResultDto fDto = new ResultDto();
		fDto.setName("COMUNICACIÓN EFICAZ DE LAS IDEAS");
		/* Obtenemos la calificacion de la categoria */
		List<AnswersUserTeacherEval> questionsEvalsfDto = answerUserTeacherEvalRepository.findByTeacherAndQuestionId(teacher.getRegister(), 42L);
		
		double promediofDto = 0.00;
		double sumfDto=0;
		for(AnswersUserTeacherEval xAnswersUserTeacherEval : questionsEvalsfDto) {
			
			sumfDto = sumfDto + Double.parseDouble(xAnswersUserTeacherEval.getCustomizedAnswer());
		}
		promediofDto = sumfDto /questionsEvalsfDto.size();
		
		fDto.setScore(Math.round(promediofDto * Math.pow(10, 2)) / Math.pow(10, 2));
		dtos.add(fDto);
		//-------------------------------------------------------------------------------------------------------------------------------------------
		ResultDto gDto = new ResultDto();
		gDto.setName("USO Y PERTINENCIA DE LAS TIC");
		/* Obtenemos la calificacion de la categoria */
		List<AnswersUserTeacherEval> questionsEvalsgDto = answerUserTeacherEvalRepository.findByTeacherAndQuestionId(teacher.getRegister(), 43L);
		
		double promediogDto = 0.00;
		double sumgDto=0;
		for(AnswersUserTeacherEval xAnswersUserTeacherEval : questionsEvalsgDto) {
			
			sumgDto = sumgDto + Double.parseDouble(xAnswersUserTeacherEval.getCustomizedAnswer());
		}
		List<AnswersUserTeacherEval> questionsEvalsgDto2= answerUserTeacherEvalRepository.findByTeacherAndQuestionId(teacher.getRegister(), 44L);
		
		for(AnswersUserTeacherEval xAnswersUserTeacherEval : questionsEvalsgDto2) {
			
			sumgDto = sumgDto + Double.parseDouble(xAnswersUserTeacherEval.getCustomizedAnswer());
		}
		promediogDto = sumgDto /(questionsEvalsgDto.size() + questionsEvalsgDto2.size());
		
		gDto.setScore(Math.round(promediogDto * Math.pow(10, 2)) / Math.pow(10, 2));
		dtos.add(gDto);
		//-------------------------------------------------------------------------------------------------------------------------------------------
		ResultDto hDto = new ResultDto();
		hDto.setName("                                                                                          PROMEDIO GENERAL:");
		double promedioGeneral = 0.00; 
		promedioGeneral = (wDto.getScore() + aDto.getScore() + bDto.getScore() + cDto.getScore() + dDto.getScore() + eDto .getScore()+ fDto.getScore()+ gDto.getScore()) / 8; 
		hDto.setScore(Math.round(promedioGeneral * Math.pow(10, 2)) / Math.pow(10, 2));
		
		dtos.add(hDto);
		/*Recuperando las uaps que imparte un profesor */
		
		List<OfferedClass> classTeacher= offeredClassRepository.findByTeacherRegisterPeriodBefore(teacher.getRegister());
		
		for(OfferedClass xDto: classTeacher) {
			TeacherEvalReportDto yDto = new TeacherEvalReportDto(); 
			OfferedClassDto tempDto = new OfferedClassDto();
			log.info("{} - uap recuperada: {}", userSecurityService.getIdSession(),xDto.getUap().getName()+" Id periodo:"+xDto.getPeriod().getId());
			tempDto.setMaxCharge(xDto.getMaxCharge());
			tempDto.setMinCharge(xDto.getMinCharge()); 
			tempDto.setGroup(groupConverter.convertGroup2GroupDto(xDto.getGroup()));
			tempDto.setUap(uapConverter.convertUap2UapDto(xDto.getUap()));
			tempDto.setId(xDto.getId());
			yDto.setOfferedClass(tempDto);
			/* Obtenemos el total de encuestas que se contestaron por uap */
			List<AnswersUserTeacherEval> responsEvals = answerUserTeacherEvalRepository.findByTeacherAndStudentCharge(teacher.getRegister(), xDto.getId());
			yDto.setTotalEvaluation(responsEvals.size()/14);
			prueba.add(yDto);
		
		}
		teacherEval.get(0).setTeacherEvalReportDtos(prueba);
		teacherEval.get(0).setResultDtos(dtos);
				
		JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(teacherEval);
		// load file and compile it
		
		// File file = ResourceUtils.getFile("classpath:papeletav1.jrxml");
		InputStream archivo = StudentServiceImpl.class.getResourceAsStream("/evalDocente.jasper");
		// JasperReport jasperReport =
		// JasperCompileManager.compileReport(file.getAbsolutePath());
		log.info("{} - Inicia seteo de parametros.", userSecurityService.getIdSession());

		Map<String, Object> parameters = new HashMap<>();		
		parameters.put("P_SCHOOL_PERIOD", "Agosto 2021 - Enero 2022.");
		parameters.put("P_TEACHER", teacher.getName()+" "+teacher.getLastname()+" "+teacher.getSecondLastname());
		log.info("{} - Termina seteo de parametros.", userSecurityService.getIdSession());
		log.info("{} - Inicia llenado de reporte.", userSecurityService.getIdSession());
		JasperPrint jasperPrint = null;
		try {
			jasperPrint = JasperFillManager.fillReport(archivo, parameters, dataSource);
		} catch (JRException ex4) {
			ex4.printStackTrace();
			throw new ServiceException("Ocurri\u00F3 un error al llenar reporte del docente.");
		}

		JRPdfExporter exporter = new JRPdfExporter();

		exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
		exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, baos);

		try {
			exporter.exportReport();
		} catch (JRException ex5) {
			throw new ServiceException("Ocurri\u00F3 un error al exportar papeleta PDF.");
		}

		byte[] out = baos.toByteArray();
		// JasperExportManager.exportReportToPdfFile(jasperPrint,"Papeleta_" + register
		// + ".pdf");
		log.info("{} - Termina exportToPdfFile[{}].", userSecurityService.getIdSession(), register);

		return out;
	}

	@Override
	public ByteArrayInputStream exportInquestTotal() throws IOException {
		log.info("{} - TeacherServiceImpl - Iniciando reporte de encuestas contestadas de docente", userSecurityService.getIdSession());
		List<Teacher> teachers = new ArrayList<>();
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		Workbook workbook = new HSSFWorkbook();
		/* Colocamos en negrita */
		CellStyle headerStyle = workbook.createCellStyle();
        Font font = workbook.createFont();
        font.setBold(true);
        headerStyle.setFont(font);
        /* Obtenemos el listado de todos los docentes */
        teachers = teacherRepository.findAllOrderByNameLastnameSecondLastnameAsc();
        /*Definiendo los encabezados de la lista */
		String [] columns = {"No.P","UNIDAD DE APRENDIZAJE","PROGRAMA EDUCATIVO", "GRUPO","TOTAL DE ENCUESTAS"};
		String [] columnsTable = {"ASPECTOS EVALUADOS","CALIFICACIÓN"};
		/* Recorremos todos los docentes */
		for(Teacher teacherTemp : teachers) {
			Sheet sheet = workbook.createSheet(teacherTemp.getName()+"-"+ teacherTemp.getRegister());
			Row row = sheet.createRow(0);
			/* Colocamos los datos de la materia */
			sheet.addMergedRegion(new CellRangeAddress(0,0,0,5));
			Cell cellPlan = row.createCell(0);
			cellPlan.setCellValue("ESTADÍSTICAS DE:  "+teacherTemp.getName()+" "+teacherTemp.getLastname()+" "+teacherTemp.getSecondLastname());
			cellPlan.setCellStyle(headerStyle);
			row = sheet.createRow(5);
			for(int k = 0; k < columns.length; k++) {
				Cell cell = row.createCell(k);
				cell.setCellValue(columns[k]);
				cell.setCellStyle(headerStyle);
			}
			row = sheet.createRow(5);
			row.createCell(0).setCellValue("Encuestas contestadas:");
			int initRow = 6;
			int no = 1;
			/*Recuperando las uaps que imparte un profesor */			
			List<OfferedClass> classTeacher= offeredClassRepository.findByTeacherRegister(teacherTemp.getRegister());
			for(OfferedClass xDto: classTeacher) {
				TeacherEvalReportDto yDto = new TeacherEvalReportDto(); 
				OfferedClassDto tempDto = new OfferedClassDto();
				tempDto.setMaxCharge(xDto.getMaxCharge());
				tempDto.setMinCharge(xDto.getMinCharge()); 
				tempDto.setGroup(groupConverter.convertGroup2GroupDto(xDto.getGroup()));
				tempDto.setUap(uapConverter.convertUap2UapDto(xDto.getUap()));
				tempDto.setId(xDto.getId());
				yDto.setOfferedClass(tempDto);
				
				/* Obtenemos el total de encuestas que se contestaron por uap */
				List<AnswersUserTeacherEval> responsEvals = answerUserTeacherEvalRepository.findByTeacherAndStudentCharge(teacherTemp.getRegister(), xDto.getId());
				
				yDto.setTotalEvaluation(responsEvals.size()/14);
				/* Imprimiendo los datos*/
				row = sheet.createRow(initRow);
				row.createCell(0).setCellValue(no);
				row.createCell(1).setCellValue(yDto.getOfferedClass().getUap().getName());
				row.createCell(2).setCellValue(yDto.getOfferedClass().getUap().getEducationalProgram().getStudentPlanName());
				row.createCell(3).setCellValue(yDto.getOfferedClass().getGroup().getName()+" "+yDto.getOfferedClass().getGroup().getTurn());
				row.createCell(4).setCellValue(yDto.getTotalEvaluation());		
				initRow++;
				no++;		
			}
			row = sheet.createRow(18);
			row.createCell(0).setCellValue("Resultados:");
			/* Dibujando la tabla */
			row = sheet.createRow(19);
			for(int k = 0; k < columnsTable.length; k++) {
				Cell cell = row.createCell(k);
				cell.setCellValue(columnsTable[k]);
				cell.setCellStyle(headerStyle);
			}
			/* Colocamos las estadisticas */
			//-----------------------------------------------------------------
			ResultDto wDto = new ResultDto();
			wDto.setName("APRENDIZAJE SIGNIFICATIVO");
			/* Obtenemos la calificacion de la categoria */
			List<AnswersUserTeacherEval> questionsEvalswDto = answerUserTeacherEvalRepository.findByTeacherAndQuestionId(teacherTemp.getRegister(), 31L);
			
			double promedio = 0.00;
			double sum=0;
			for(AnswersUserTeacherEval xAnswersUserTeacherEval : questionsEvalswDto) {
				
				sum = sum + Double.parseDouble(xAnswersUserTeacherEval.getCustomizedAnswer());
			}
			promedio = sum /questionsEvalswDto.size();
			
			wDto.setScore(Math.round(promedio * Math.pow(10, 2)) / Math.pow(10, 2));
			/* Imprimiendo los datos*/
			row = sheet.createRow(20);
			row.createCell(0).setCellValue(wDto.getName());
			row.createCell(1).setCellValue(wDto.getScore());
			//-----------------------------------------------------------------
			ResultDto aDto = new ResultDto();
			aDto.setName("PLANIFICACIÓN DEL APRENDIZAJE");
			/* Obtenemos la calificacion de la categoria */
			List<AnswersUserTeacherEval> questionsEvalsaDto = answerUserTeacherEvalRepository.findByTeacherAndQuestionId(teacherTemp.getRegister(), 32L);
			
			double promedioaDto = 0.00;
			double sumaDto=0;
			for(AnswersUserTeacherEval xAnswersUserTeacherEval : questionsEvalsaDto) {
				
				sumaDto = sumaDto + Double.parseDouble(xAnswersUserTeacherEval.getCustomizedAnswer());
			}
			List<AnswersUserTeacherEval> questionsEvalsaDto2= answerUserTeacherEvalRepository.findByTeacherAndQuestionId(teacherTemp.getRegister(), 33L);
			
			for(AnswersUserTeacherEval xAnswersUserTeacherEval : questionsEvalsaDto2) {
				
				sumaDto = sumaDto + Double.parseDouble(xAnswersUserTeacherEval.getCustomizedAnswer());
			}
			promedioaDto = sumaDto /(questionsEvalsaDto.size() + questionsEvalsaDto2.size());
			
			aDto.setScore(Math.round(promedioaDto * Math.pow(10, 2)) / Math.pow(10, 2));
			/* Imprimiendo los datos*/
			row = sheet.createRow(21);
			row.createCell(0).setCellValue(aDto.getName());
			row.createCell(1).setCellValue(aDto.getScore());
			//-----------------------------------------------------------------
			ResultDto bDto = new ResultDto();
			bDto.setName("ESTRATEGIAS DE ENSEÑANZA-APRENDIZAJE");
			/* Obtenemos la calificacion de la categoria */
			List<AnswersUserTeacherEval> questionsEvalsbDto = answerUserTeacherEvalRepository.findByTeacherAndQuestionId(teacherTemp.getRegister(), 34L);
			
			double promediobDto = 0.00;
			double sumbDto=0;
			for(AnswersUserTeacherEval xAnswersUserTeacherEval : questionsEvalsbDto) {
				
				sumbDto = sumbDto + Double.parseDouble(xAnswersUserTeacherEval.getCustomizedAnswer());
			}
			promediobDto = sumbDto /questionsEvalsbDto.size();
			
			bDto.setScore(Math.round(promediobDto * Math.pow(10, 2)) / Math.pow(10, 2));
			/* Imprimiendo los datos*/
			row = sheet.createRow(22);
			row.createCell(0).setCellValue(bDto.getName());
			row.createCell(1).setCellValue(bDto.getScore());
			//-----------------------------------------------------------------
			ResultDto cDto = new ResultDto();
			cDto.setName("EVALUACIÓN DEL APRENDIZAJE");
			/* Obtenemos la calificacion de la categoria */
			List<AnswersUserTeacherEval> questionsEvalscDto = answerUserTeacherEvalRepository.findByTeacherAndQuestionId(teacherTemp.getRegister(), 35L);
			
			double promediocDto = 0.00;
			double sumcDto=0;
			for(AnswersUserTeacherEval xAnswersUserTeacherEval : questionsEvalscDto) {
				
				sumcDto = sumcDto + Double.parseDouble(xAnswersUserTeacherEval.getCustomizedAnswer());
			}
			List<AnswersUserTeacherEval> questionsEvalscDto2= answerUserTeacherEvalRepository.findByTeacherAndQuestionId(teacherTemp.getRegister(), 36L);
			
			for(AnswersUserTeacherEval xAnswersUserTeacherEval : questionsEvalscDto2) {
				
				sumcDto = sumcDto + Double.parseDouble(xAnswersUserTeacherEval.getCustomizedAnswer());
			}
			List<AnswersUserTeacherEval> questionsEvalscDto3= answerUserTeacherEvalRepository.findByTeacherAndQuestionId(teacherTemp.getRegister(), 37L);
			
			for(AnswersUserTeacherEval xAnswersUserTeacherEval : questionsEvalscDto3) {
				
				sumcDto = sumcDto + Double.parseDouble(xAnswersUserTeacherEval.getCustomizedAnswer());
			}
			List<AnswersUserTeacherEval> questionsEvalscDto4= answerUserTeacherEvalRepository.findByTeacherAndQuestionId(teacherTemp.getRegister(), 38L);
			
			for(AnswersUserTeacherEval xAnswersUserTeacherEval : questionsEvalscDto4) {
				
				sumcDto = sumcDto + Double.parseDouble(xAnswersUserTeacherEval.getCustomizedAnswer());
			}
			
			promediocDto = sumcDto /(questionsEvalscDto.size() + questionsEvalscDto2.size() + questionsEvalscDto3.size() + questionsEvalscDto4.size());
			
			cDto.setScore(Math.round(promediocDto * Math.pow(10, 2)) / Math.pow(10, 2));
			/* Imprimiendo los datos*/
			row = sheet.createRow(23);
			row.createCell(0).setCellValue(cDto.getName());
			row.createCell(1).setCellValue(cDto.getScore());
			//-----------------------------------------------------------------
			ResultDto dDto = new ResultDto();
			dDto.setName("AMBIENTE DE APRENDIZAJE AUTÓNOMO Y COLABORACIÓN");
			/* Obtenemos la calificacion de la categoria */
			List<AnswersUserTeacherEval> questionsEvalsdDto = answerUserTeacherEvalRepository.findByTeacherAndQuestionId(teacherTemp.getRegister(), 39L);
			
			double promediodDto = 0.00;
			double sumdDto=0;
			for(AnswersUserTeacherEval xAnswersUserTeacherEval : questionsEvalsdDto) {
				
				sumdDto = sumdDto + Double.parseDouble(xAnswersUserTeacherEval.getCustomizedAnswer());
			}
			List<AnswersUserTeacherEval> questionsEvalsdDto2= answerUserTeacherEvalRepository.findByTeacherAndQuestionId(teacherTemp.getRegister(), 40L);
			
			for(AnswersUserTeacherEval xAnswersUserTeacherEval : questionsEvalsdDto2) {
				
				sumdDto = sumdDto + Double.parseDouble(xAnswersUserTeacherEval.getCustomizedAnswer());
			}
			promediodDto = sumdDto /(questionsEvalsdDto.size() + questionsEvalsdDto2.size());
			
			dDto.setScore(Math.round(promediodDto * Math.pow(10, 2)) / Math.pow(10, 2));
			/* Imprimiendo los datos*/
			row = sheet.createRow(24);
			row.createCell(0).setCellValue(dDto.getName());
			row.createCell(1).setCellValue(dDto.getScore());
			//-------------------------------------------------------------------------------------------------------------------------------------------
			ResultDto eDto = new ResultDto();
			eDto.setName("AMBIENTE SANO E INTEGRAL DEL ESTUDIANTE");
			/* Obtenemos la calificacion de la categoria */
			List<AnswersUserTeacherEval> questionsEvalseDto = answerUserTeacherEvalRepository.findByTeacherAndQuestionId(teacherTemp.getRegister(), 41L);
			
			double promedioeDto = 0.00;
			double sumeDto=0;
			for(AnswersUserTeacherEval xAnswersUserTeacherEval : questionsEvalseDto) {
				
				sumeDto = sumeDto + Double.parseDouble(xAnswersUserTeacherEval.getCustomizedAnswer());
			}
			promedioeDto = sumeDto /questionsEvalseDto.size();
			
			eDto.setScore(Math.round(promedioeDto * Math.pow(10, 2)) / Math.pow(10, 2));
			/* Imprimiendo los datos*/
			row = sheet.createRow(25);
			row.createCell(0).setCellValue(eDto.getName());
			row.createCell(1).setCellValue(eDto.getScore());
			//-------------------------------------------------------------------------------------------------------------------------------------------
			ResultDto fDto = new ResultDto();
			fDto.setName("COMUNICACIÓN EFICAZ DE LAS IDEAS");
			/* Obtenemos la calificacion de la categoria */
			List<AnswersUserTeacherEval> questionsEvalsfDto = answerUserTeacherEvalRepository.findByTeacherAndQuestionId(teacherTemp.getRegister(), 42L);
			
			double promediofDto = 0.00;
			double sumfDto=0;
			for(AnswersUserTeacherEval xAnswersUserTeacherEval : questionsEvalsfDto) {
			
				sumfDto = sumfDto + Double.parseDouble(xAnswersUserTeacherEval.getCustomizedAnswer());
			}
			promediofDto = sumfDto /questionsEvalsfDto.size();
			
			fDto.setScore(Math.round(promediofDto * Math.pow(10, 2)) / Math.pow(10, 2));
			/* Imprimiendo los datos*/
			row = sheet.createRow(26);
			row.createCell(0).setCellValue(fDto.getName());
			row.createCell(1).setCellValue(fDto.getScore());
			//-------------------------------------------------------------------------------------------------------------------------------------------
			ResultDto gDto = new ResultDto();
			gDto.setName("USO Y PERTINENCIA DE LAS TIC");
			/* Obtenemos la calificacion de la categoria */
			List<AnswersUserTeacherEval> questionsEvalsgDto = answerUserTeacherEvalRepository.findByTeacherAndQuestionId(teacherTemp.getRegister(), 43L);
			
			double promediogDto = 0.00;
			double sumgDto=0;
			for(AnswersUserTeacherEval xAnswersUserTeacherEval : questionsEvalsgDto) {
			
				sumgDto = sumgDto + Double.parseDouble(xAnswersUserTeacherEval.getCustomizedAnswer());
			}
			List<AnswersUserTeacherEval> questionsEvalsgDto2= answerUserTeacherEvalRepository.findByTeacherAndQuestionId(teacherTemp.getRegister(), 44L);
			
			for(AnswersUserTeacherEval xAnswersUserTeacherEval : questionsEvalsgDto2) {
			
				sumgDto = sumgDto + Double.parseDouble(xAnswersUserTeacherEval.getCustomizedAnswer());
			}
			promediogDto = sumgDto /(questionsEvalsgDto.size() + questionsEvalsgDto2.size());
			
			gDto.setScore(Math.round(promediogDto * Math.pow(10, 2)) / Math.pow(10, 2));
			/* Imprimiendo los datos*/
			row = sheet.createRow(27);
			row.createCell(0).setCellValue(gDto.getName());
			row.createCell(1).setCellValue(gDto.getScore());
			//-------------------------------------------------------------------------------------------------------------------------------------------
			ResultDto hDto = new ResultDto();
			hDto.setName("                                                                                          PROMEDIO GENERAL:");
			double promedioGeneral = 0.00; 
			promedioGeneral = (wDto.getScore() + aDto.getScore() + bDto.getScore() + cDto.getScore() + dDto.getScore() + eDto .getScore()+ fDto.getScore()+ gDto.getScore()) / 8; 
			hDto.setScore(Math.round(promedioGeneral * Math.pow(10, 2)) / Math.pow(10, 2));
			
			/* Imprimiendo los datos*/
			row = sheet.createRow(28);
			row.createCell(0).setCellValue(hDto.getName());
			row.createCell(1).setCellValue(hDto.getScore());

		}

		workbook.write(stream);
		workbook.close();		
		log.info("{} - TeacherServiceImpl - Generacion del listado en excel exitosa", userSecurityService.getIdSession());
		return new ByteArrayInputStream(stream.toByteArray());
	}

	@Override
	public ByteArrayInputStream exportEvalsTotal() throws IOException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Resource exportEvalsTeachersAll(long idPeriod) throws ServiceException {
		log.info("{} - TeacherServiceImpl - Iniciando reporte de todas las evaluaciones contestadas", userSecurityService.getIdSession());
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		Workbook workbook = new HSSFWorkbook();
		/* Colocamos en negrita */
		CellStyle headerStyle = workbook.createCellStyle();
        Font font = workbook.createFont();
        font.setBold(true);
        headerStyle.setFont(font);        
        /*Definiendo los encabezados de la lista */
		String [] columns = {"ID USUARIO","MATRICULA","UNIDAD DE APRENDIZAJE","PROGRAMA EDUCATIVO", "GRUPO","NO. EMPLEADO", "DOCENTE","¿Al inicio del semestre, el profesor presentó un programa o temario de la UAp y sus alcances (secuencia didáctica y contenido de la Uap)?" ,
				"¿El profesor, presenta y expone las clases de manera organizada y estructurada?","¿El profesor, dió seguimiento y cumplió con todo el contenido planteado al inicio del curso?","¿Asistió puntualmente el docente durante el curso?",
				"¿Durante el curso, el profesor aplicó estrategias adecuadas INNOVADORAS, para lograr el aprendizaje esperado? (hace uso de videos, diapositivas, ejercicios prácticos, entre otros.)",
				"¿El profesor, utilizó la retroalimentación al realizar alguna evaluación, para mejorar la entrega y comprensión de las actividades o proyectos?", "¿El profesor evaluó el aprendizaje de cada una de las actividades establecidas en la secuencia didáctica?",
				"¿El profesor fomentó la evaluación por ti mismo (autoevaluación) y que tú evaluaras a tus compañeros (coevaluación)?",
				"¿El profesor, aplicó algunos de los diferentes instrumentos para evaluar el aprendizaje de los estudiantes (Rúbrica, lista de cotejo, escala estimativa, guía de entrevista, seguimiento al proyecto, evaluación del conocimiento (examenes, cuestionarios, ejercicios), entre otros)?",
				"¿El profesor complementa la clase presencial o a distancia con actividades fuera del aula, sea de manera individual o en grupos?","¿El profesor realiza actividades en equipo como proyectos, investigaciones y su presentación en el aula?",
				"¿El profesor, propicia el desarrollo de Valores, así como un ambiente de respeto y confianza?","¿Cuándo le hacían alguna pregunta al profesor, mostraba claridad de sus respuestas?", "¿El profesor explica los temas, logrando que el estudiante capte dichos contenidos?",
				"¿El profesor, emplea las tecnologías de la información y de la comunicación como un medio que facilite el aprendizaje?","¿El profesor, es accesible y está dispuesto a brindarte ayuda académica?",
				"¿El profesor, entregó calificación final?","¿Contaste con herramientas tecnológicas (celular, computadora, entre otros) durante el desarrollo de tu UAP?","¿Consideras que en este curso obtuviste más del 60% de asistencia?",
				"Si te evaluaras, ¿cuál consideras que debería ser tu calificación de este curso?","¿Qué % de conocimientos adquiriste en este curso?","¿Que propones para el mejoramiento Didáctico del Docente?"};
		
		/*Definimos los encabezados */
        Sheet sheet = workbook.createSheet("Evaluaciones_docentes");
		Row row = sheet.createRow(0);
		int initRow = 1;
		for(int k = 0; k < columns.length; k++) {
			Cell cell = row.createCell(k);
			cell.setCellValue(columns[k]);
			cell.setCellStyle(headerStyle);
		}
		/*Obtenemos el listado de usuarios unicos que han respondido la evaluacion docente */
		//log.info("{} - TeacherServiceImpl - Voy a ir a consultar los usuarios unicos", userSecurityService.getIdSession());
		List<InfoEvaluation> usrTemp= infoEvaluationRepository.findInfoEvaluationByIdInfoEvaluation_Period_Id(idPeriod);
		//log.info("{} - TeacherServiceImpl - Total de usuarios unicos recuperados desde info_evaluacion: {}", userSecurityService.getIdSession(),usrTemp.size());
		List<Long>  usersListUnique= answerUserTeacherEvalRepository.findByUserIdUnique();
		//log.info("{} - TeacherServiceImpl - Total de usuarios unicos recuperados: {}", userSecurityService.getIdSession(),usersListUnique.size());
		/* Recorremos todos los usuarios obtenidos */
		for(InfoEvaluation userTemp : usrTemp) {

			/* Obtenemos todas las clases ofertadas del usuario */
			List<Long> studentChargeList = answerUserTeacherEvalRepository.findByUserIdStudentCharge(userTemp.getIdInfoEvaluation().getUser().getIdUser(),idPeriod);
			//log.info("{} - TeacherServiceImpl - El usuario: {} - Tiene: {} - materias cargadas", userSecurityService.getIdSession(),userTemp.getIdInfoEvaluation().getUser().getIdUser(), studentChargeList.size());
			/* Obtenemos el listado de todas las preguntas por carga_alumno y las pintamos en el excel */
			for(Long studentChargeLong : studentChargeList) {
				List<AnswersUserTeacherEval> evalDocent = null;
				List<AnswersUserAutoEval> evalAuto = null;
				try {
				evalDocent = answerUserTeacherEvalRepository.findBystudentChargeId(studentChargeLong);
				evalAuto = answerUserAutoEvalRepository.findBystudentChargeId(studentChargeLong);
				row = sheet.createRow(initRow);
				row.createCell(0).setCellValue(evalDocent.get(0).getUser().getIdUser());
				row.createCell(1).setCellValue(evalDocent.get(0).getUser().getUsername());
				row.createCell(2).setCellValue(evalDocent.get(0).getStudentCharge().getOfferedClass().getUap().getName());
				row.createCell(3).setCellValue(evalDocent.get(0).getStudentCharge().getOfferedClass().getUap().getEducationalProgram().getAbr());
				row.createCell(4).setCellValue(evalDocent.get(0).getStudentCharge().getOfferedClass().getGroup().getName()+" "+evalDocent.get(0).getStudentCharge().getOfferedClass().getGroup().getTurn());
				row.createCell(5).setCellValue(evalDocent.get(0).getStudentCharge().getOfferedClass().getTeacher().getRegister());		
				row.createCell(6).setCellValue(evalDocent.get(0).getStudentCharge().getOfferedClass().getTeacher().getName()+" "+evalDocent.get(0).getStudentCharge().getOfferedClass().getTeacher().getLastname()+" "+evalDocent.get(0).getStudentCharge().getOfferedClass().getTeacher().getSecondLastname());	
				}catch(Exception se) {
					log.error("Ocurrió un error en el llenado del reporte de evaluaciones:{}", se.getMessage());
					throw new ServiceException("Ocurrió un error al generar el reporte de evaluaciones.");
				}
				//log.info("{} - TeacherServiceImpl - El usuario: {} - Tiene: {} - respuestas de la carga de alumno: {}", userSecurityService.getIdSession(),evalDocent.get(0).getUser().getIdUser(), evalDocent.size(), studentChargeLong);
				for(AnswersUserTeacherEval answersUserTeacherEvalTemp: evalDocent) {				
					if (answersUserTeacherEvalTemp.getQuestion().getId() == 51) {// ¿Al inicio del semestre, el profesor presentó un programa o temario de la UAp y sus alcances (secuencia didáctica y contenido de la Uap)?
						if(answersUserTeacherEvalTemp.getCustomizedAnswer() != null) {
							row.createCell(7).setCellValue(answersUserTeacherEvalTemp.getCustomizedAnswer());
						}else {
							row.createCell(7).setCellValue("");
						}
					}
					if (answersUserTeacherEvalTemp.getQuestion().getId() == 52) {// ¿El profesor, presenta y expone las clases de manera organizada y estructurada?
						if(answersUserTeacherEvalTemp.getCustomizedAnswer() != null) {
							row.createCell(8).setCellValue(answersUserTeacherEvalTemp.getCustomizedAnswer());
						}else {
							row.createCell(8).setCellValue("");
						}
					}
					if (answersUserTeacherEvalTemp.getQuestion().getId() == 53) {// ¿El profesor, dió seguimiento y cumplió con todo el contenido planteado al inicio del curso?
						if(answersUserTeacherEvalTemp.getCustomizedAnswer() != null) {
							row.createCell(9).setCellValue(answersUserTeacherEvalTemp.getCustomizedAnswer());
						}else {
							row.createCell(9).setCellValue("");
						}
					}
					if (answersUserTeacherEvalTemp.getQuestion().getId() == 54) {// ¿Asistió puntualmente el docente durante el curso?
						if(answersUserTeacherEvalTemp.getCustomizedAnswer() != null) {
							row.createCell(10).setCellValue(answersUserTeacherEvalTemp.getCustomizedAnswer());
						}else {
							row.createCell(10).setCellValue("");
						}
					}
					if (answersUserTeacherEvalTemp.getQuestion().getId() == 55) {// ¿Durante el curso, el profesor aplicó estrategias adecuadas INNOVADORAS, para lograr el aprendizaje esperado? (hace uso de videos, diapositivas, ejercicios prácticos, entre otros.)
						if(answersUserTeacherEvalTemp.getCustomizedAnswer() != null) {
							row.createCell(11).setCellValue(answersUserTeacherEvalTemp.getCustomizedAnswer());
						}else {
							row.createCell(11).setCellValue("");
						}
					}
					if (answersUserTeacherEvalTemp.getQuestion().getId() == 56) {// ¿El profesor, utilizó la retroalimentación al realizar alguna evaluación, para mejorar la entrega y comprensión de las actividades o proyectos?
						if(answersUserTeacherEvalTemp.getCustomizedAnswer() != null) {
							row.createCell(12).setCellValue(answersUserTeacherEvalTemp.getCustomizedAnswer());
						}else {
							row.createCell(12).setCellValue("");
						}
					}
					if (answersUserTeacherEvalTemp.getQuestion().getId() == 57) {// ¿El profesor evaluó el aprendizaje de cada una de las actividades establecidas en la secuencia didáctica?
						if(answersUserTeacherEvalTemp.getCustomizedAnswer() != null) {
							row.createCell(13).setCellValue(answersUserTeacherEvalTemp.getCustomizedAnswer());
						}else {
							row.createCell(13).setCellValue("");
						}
					}
					if (answersUserTeacherEvalTemp.getQuestion().getId() == 58) {// ¿El profesor fomentó la evaluación por ti mismo (autoevaluación) y que tú evaluaras a tus compañeros (coevaluación)?
						if(answersUserTeacherEvalTemp.getCustomizedAnswer() != null) {
							row.createCell(14).setCellValue(answersUserTeacherEvalTemp.getCustomizedAnswer());
						}else {
							row.createCell(14).setCellValue("");
						}
					}
					if (answersUserTeacherEvalTemp.getQuestion().getId() == 59) {// ¿El profesor, aplicó algunos de los diferentes instrumentos para evaluar el aprendizaje de los estudiantes (Rúbrica, lista de cotejo, escala estimativa, guía de entrevista, seguimiento al proyecto, evaluación del conocimiento (examenes, cuestionarios, ejercicios), entre otros)?
						if(answersUserTeacherEvalTemp.getCustomizedAnswer() != null) {
							row.createCell(15).setCellValue(answersUserTeacherEvalTemp.getCustomizedAnswer());
						}else {
							row.createCell(15).setCellValue("");
						}
					}
					if (answersUserTeacherEvalTemp.getQuestion().getId() == 60) {// ¿El profesor complementa la clase presencial o a distancia con actividades fuera del aula, sea de manera individual o en grupos?
						if(answersUserTeacherEvalTemp.getCustomizedAnswer() != null) {
							row.createCell(16).setCellValue(answersUserTeacherEvalTemp.getCustomizedAnswer());
						}else {
							row.createCell(16).setCellValue("");
						}
					}
					if (answersUserTeacherEvalTemp.getQuestion().getId() == 61) {// ¿El profesor realiza actividades en equipo como proyectos, investigaciones y su presentación en el aula?
						if(answersUserTeacherEvalTemp.getCustomizedAnswer() != null) {
							row.createCell(17).setCellValue(answersUserTeacherEvalTemp.getCustomizedAnswer());
						}else {
							row.createCell(17).setCellValue("");
						}
					}
					if (answersUserTeacherEvalTemp.getQuestion().getId() == 62) {// ¿El profesor, propicia el desarrollo de Valores, así como un ambiente de respeto y confianza?
						if(answersUserTeacherEvalTemp.getCustomizedAnswer() != null) {
							row.createCell(18).setCellValue(answersUserTeacherEvalTemp.getCustomizedAnswer());
						}else {
							row.createCell(18).setCellValue("");
						}
					}
					if (answersUserTeacherEvalTemp.getQuestion().getId() == 63) {// ¿Cuándo le hacían alguna pregunta al profesor, mostraba claridad de sus respuestas?
						if(answersUserTeacherEvalTemp.getCustomizedAnswer() != null) {
							row.createCell(19).setCellValue(answersUserTeacherEvalTemp.getCustomizedAnswer());
						}else {
							row.createCell(19).setCellValue("");
						}
					}
					if (answersUserTeacherEvalTemp.getQuestion().getId() == 64) {// ¿El profesor explica los temas, logrando que el estudiante capte dichos contenidos?
						if(answersUserTeacherEvalTemp.getCustomizedAnswer() != null) {
							row.createCell(20).setCellValue(answersUserTeacherEvalTemp.getCustomizedAnswer());
						}else {
							row.createCell(20).setCellValue("");
						}
					}
					if (answersUserTeacherEvalTemp.getQuestion().getId() == 65) {// ¿El profesor, emplea las tecnologías de la información y de la comunicación como un medio que facilite el aprendizaje?
						if(answersUserTeacherEvalTemp.getCustomizedAnswer() != null) {
							row.createCell(21).setCellValue(answersUserTeacherEvalTemp.getCustomizedAnswer());
						}else {
							row.createCell(21).setCellValue("");
						}
					}
					if (answersUserTeacherEvalTemp.getQuestion().getId() == 66) {// ¿El profesor, es accesible y está dispuesto a brindarte ayuda académica?
						if(answersUserTeacherEvalTemp.getCustomizedAnswer() != null) {
							row.createCell(22).setCellValue(answersUserTeacherEvalTemp.getCustomizedAnswer());
						}else {
							row.createCell(22).setCellValue("");
						}
					}
						
					
				}
				for(AnswersUserAutoEval answersUserAutoEvalTemp: evalAuto) {
					if (answersUserAutoEvalTemp.getQuestion().getId() == 67) {// ¿El profesor, entregó calificación final?
						if(answersUserAutoEvalTemp.getCustomizedAnswer() != null) {
							row.createCell(23).setCellValue(answersUserAutoEvalTemp.getCustomizedAnswer());
						}else {
							row.createCell(23).setCellValue("");
						}
					}
					if (answersUserAutoEvalTemp.getQuestion().getId() == 68) {// ¿Contaste con herramientas tecnológicas (celular, computadora, entre otros) durante el desarrollo de tu UAP?
						if(answersUserAutoEvalTemp.getCustomizedAnswer() != null) {
							row.createCell(24).setCellValue(answersUserAutoEvalTemp.getCustomizedAnswer());
						}else {
							row.createCell(24).setCellValue("");
						}
					}
					if (answersUserAutoEvalTemp.getQuestion().getId() == 69) {// ¿Consideras que en este curso obtuviste más del 60% de asistencia?
						if(answersUserAutoEvalTemp.getCustomizedAnswer() != null) {
							row.createCell(25).setCellValue(answersUserAutoEvalTemp.getCustomizedAnswer());
						}else {
							row.createCell(25).setCellValue("");
						}
					}
					if (answersUserAutoEvalTemp.getQuestion().getId() == 70) {// Si te evaluaras, ¿cuál consideras que debería ser tu calificación de este curso?
						if(answersUserAutoEvalTemp.getCustomizedAnswer() != null) {
							row.createCell(26).setCellValue(answersUserAutoEvalTemp.getCustomizedAnswer());
						}else {
							row.createCell(26).setCellValue("");
						}
					}
					if (answersUserAutoEvalTemp.getQuestion().getId() == 71) {// ¿Qué % de conocimientos adquiriste en este curso?
						if(answersUserAutoEvalTemp.getCustomizedAnswer() != null) {
							row.createCell(27).setCellValue(answersUserAutoEvalTemp.getCustomizedAnswer());
						}else {
							row.createCell(27).setCellValue("");
						}
					}
					if (answersUserAutoEvalTemp.getQuestion().getId() == 72) {// ¿Que propones para el mejoramiento Didáctico del Docente?
						if(answersUserAutoEvalTemp.getCustomizedAnswer() != null) {
							row.createCell(28).setCellValue(answersUserAutoEvalTemp.getCustomizedAnswer());
						}else {
							row.createCell(28).setCellValue("");
						}
					}
				}
				initRow++;
			}
			
		}
		
		byte[] bytes = null;
		try {
			workbook.write(stream);
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			try {
			    workbook.write(bos);
			} finally {
			    bos.close();
				workbook.close();
				stream.close();
			}
			bytes = bos.toByteArray();
		} catch (IOException e) {
			log.error("{} - TeacherServiceImpl - Ocurrió un error en la escritura del reporte de evaluaciones:{}", userSecurityService.getIdSession(), e.getMessage());
			throw new ServiceException("Ocurrió un error al generar reporte de evaluaciones de profesores.");
		}
		
		log.info("{} - TeacherServiceImpl - Generacion de reporte de evaluaciones", userSecurityService.getIdSession());
		return new ByteArrayResource(bytes, "Reporte de evaluaciones de profesores.");
	}

}
