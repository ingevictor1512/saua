package com.mx.saua.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import com.mx.saua.dto.UapTypeDto;
import com.mx.saua.entity.UapType;
import com.mx.saua.repository.UapTypeRepository;
import com.mx.saua.service.UapTypeService;

@Service("uapTypeService")
public class UapTypeServiceImpl implements UapTypeService {
	/*
	 * @Vic
	 */
	@Autowired
	private UapTypeRepository uapTypeRepository;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(UapTypeServiceImpl.class);


	@Override
	public List<UapTypeDto> find() {
		List<UapTypeDto> uapTypeDtos = new ArrayList<>();
		List<UapType> uapTypes = uapTypeRepository.findAll();
		uapTypes.stream().forEach(p->{
			uapTypeDtos.add(new UapTypeDto(p.getId(), p.getCategory(), p.getSemester()));
		});
		LOGGER.info("Busqueda de tipos de uaps realizada correctamente");
		return uapTypeDtos;
	}

	@Override
	public UapTypeDto getById(Long id) {
		UapType uapType = uapTypeRepository.getOne(id);
		UapTypeDto uapTypeDto = new UapTypeDto();
		BeanUtils.copyProperties(uapType, uapTypeDto);
		LOGGER.info("Busqueda de tipo de uap realizada correctamente");
		return uapTypeDto;
	}

	@Override
	public void save(UapTypeDto UapTypeDto) {
		if(UapTypeDto.getId() > -1L) {
			/* Update */
			UapType uapTypeUpdate = new UapType();
			BeanUtils.copyProperties(UapTypeDto, uapTypeUpdate);
			uapTypeRepository.save(uapTypeUpdate);
			LOGGER.info("Actualizacion de tipo de uap realizada correctamente");
		} else {
			/* New register */
			UapType uapTypeInsert = new UapType();
			BeanUtils.copyProperties(UapTypeDto, uapTypeInsert);
			uapTypeRepository.save(uapTypeInsert);
			LOGGER.info("Insercion de tipo de uap realizada correctamente");
		}
		
	}

	@Override
	public void delete(Long id) {
		try {
			uapTypeRepository.deleteById(id);
			LOGGER.info("Elimninacion de tipo de uap realizada correctamente");
		} catch (DataIntegrityViolationException dve) {
			dve.printStackTrace();
		}
		
	}
	/* GETTERS Y SETTERS */
	public UapTypeRepository getUapTypeRepository() {
		return uapTypeRepository;
	}

	public void setUapTypeRepository(UapTypeRepository uapTypeRepository) {
		this.uapTypeRepository = uapTypeRepository;
	}
}
