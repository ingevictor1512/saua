package com.mx.saua.service.impl;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mx.saua.constant.FormatsConstant;
import com.mx.saua.dto.ApplicantCoffeeShopDto;
import com.mx.saua.dto.ApplicantCoffeeShopIdDto;
import com.mx.saua.dto.ApplicantDto;
import com.mx.saua.dto.GrantDto;
import com.mx.saua.dto.GuestDto;
import com.mx.saua.dto.UserDto;
import com.mx.saua.entity.Applicant;
import com.mx.saua.entity.ApplicantCoffeeShop;
import com.mx.saua.entity.Grant;
import com.mx.saua.entity.Guest;
import com.mx.saua.entity.Period;
import com.mx.saua.entity.Student;
import com.mx.saua.entity.User;
import com.mx.saua.exception.ServiceException;
import com.mx.saua.repository.ApplicantCoffeeShopRepository;
import com.mx.saua.repository.ApplicantRepository;
import com.mx.saua.repository.GrantRepository;
import com.mx.saua.repository.PeriodRepository;
import com.mx.saua.repository.StudentRepository;
import com.mx.saua.service.ApplicantService;
import com.mx.saua.service.CoffeeShopService;
import com.mx.saua.service.UserService;

@Service("applicantService")
public class ApplicantServiceImpl implements ApplicantService {

	private Logger LOGGER = LoggerFactory.getLogger(ApplicantServiceImpl.class);

	@Autowired
	private ApplicantRepository applicantRepository;

	@Autowired
	private GrantRepository grantRepository;

	@Autowired
	private PeriodRepository periodRepository;

	@Autowired
	private StudentRepository studentRepository;

	@Autowired
	private UserService userService;

	@Autowired
	private CoffeeShopService coffeeShopService;

	@Autowired
	private ApplicantCoffeeShopRepository applicantCoffeeShopRepository;

	@Override
	public ApplicantDto find(ApplicantDto applicantDto) throws ServiceException {
		List<Applicant> list = applicantRepository.findAll();
		List<ApplicantDto> dtos = new ArrayList<ApplicantDto>();
		// SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		for (Applicant applicant : list) {
			// applicant.getApplicantCoffeeShops().forEach(n ->
			// System.out.println(sdf.format(n.getUsedate())));
			Grant grant = grantRepository.getOne(applicant.getGrant().getId());
			Period period = periodRepository.getOne(applicant.getPeriod().getId());

			if (applicant.getUser() != null) {
				try {
					dtos.add(new ApplicantDto(applicant.getId(),
							applicant.getUser().getStudent().getName() + ' '
									+ applicant.getUser().getStudent().getLastname() + ' '
									+ applicant.getUser().getStudent().getSecondLastname(),
							grant, period, applicant.isStatusGrant(), applicant.getUser().getUsername(),
							getApplicantCoffeeShopDtos(applicant)));
				} catch (ParseException e) {
					LOGGER.error("Error al obtener dtos:{}", e.getMessage());
					throw new ServiceException("Ocurri\u00F3 un error al obtener potulado.");
				}
			} else {
				try {
					dtos.add(new ApplicantDto(applicant.getId(),
							applicant.getGuest().getName() + ' ' + applicant.getGuest().getLastname() + ' '
									+ applicant.getGuest().getSecondLastname(),
							grant, period, applicant.isStatusGrant(), applicant.getGuest().getIdRegister(),
							getApplicantCoffeeShopDtos(applicant)));
				} catch (ParseException e) {
					LOGGER.error("Error al obtener dtos:{}", e.getMessage());
					throw new ServiceException("Ocurri\u00F3 un error al obtener potulado.");
				}
			}
		}
		applicantDto.setApplicantDtoList(dtos);
		return applicantDto;
	}

	@Override
	public ApplicantDto prepareDto(ApplicantDto applicantDto) {
		List<Grant> grants = grantRepository.findAll();
		List<GrantDto> grantDtos = new ArrayList<>();
		grants.stream().forEach(p -> {
			GrantDto temp = new GrantDto();
			temp.setId(p.getId());
			temp.setGrant(p.getName());
			temp.setDescription(p.getDescription());
			grantDtos.add(temp);
		});
		applicantDto.setGrantsDto(grantDtos);
		return applicantDto;
	}

	@Override
	public ApplicantDto getById(Long id) throws ServiceException {
		Applicant applicant = applicantRepository.getOne(id);
		// Obtener la beca seleccionada
		Grant beca = grantRepository.getOne(applicant.getGrant().getId());

		Period periodo = periodRepository.getOne(applicant.getPeriod().getId());
		ApplicantDto applicantDto = new ApplicantDto();
		applicantDto.setId(applicant.getId());
		// applicantDto.setApplicant(applicant.getUser().getUsername());
		if (applicant.getGuest() != null) {
			applicantDto.setApplicant(applicant.getGuest().getName() + " " + applicant.getGuest().getLastname() + " "
					+ applicant.getGuest().getSecondLastname());
			applicantDto.setGrant(beca);
			applicantDto.setRegister(applicant.getGuest().getIdRegister());
			applicantDto.setGuest(true);
			applicantDto.setGuestDto(GuestDto.builder().idRegister(applicant.getGuest().getIdRegister())
					.lastname(applicant.getGuest().getLastname()).name(applicant.getGuest().getName())
					.origin(applicant.getGuest().getOrigin()).secondLastname(applicant.getGuest().getSecondLastname()).applicantId(id)
					.build());
		} else {
			applicantDto.setApplicant(
					applicant.getUser().getStudent().getName() + " " + applicant.getUser().getStudent().getLastname()
							+ " " + applicant.getUser().getStudent().getSecondLastname());
			applicantDto.setGrant(beca);
			applicantDto.setRegister(applicant.getUser().getUsername());
			applicantDto.setGuest(false);
		}

		// applicantDto.setGrant(grantRepository.getOne(applicant.getGrant().getId()));
		// applicantDto.setPeriod(periodRepository.getOne(applicant.getPeriod().getId()));
		applicantDto.setPeriod(periodo);
		applicantDto.setGrantId(beca.getId());
		applicantDto.setStatusGrant(applicant.isStatusGrant());
		applicantDto.setIdsStudentSelectedString(applicantDto.getApplicant());
		// Obteniendo el listado de becas
		List<Grant> grants = grantRepository.findAll();
		List<GrantDto> grantDtos = new ArrayList<>();
		grants.stream().forEach(p -> {
			GrantDto temp = new GrantDto();
			temp.setId(p.getId());
			temp.setGrant(p.getName());
			temp.setDescription(p.getDescription());
			grantDtos.add(temp);
		});
		applicantDto.setGrantsDto(grantDtos);
		applicantDto.setCoffeeShops(coffeeShopService.find());

		try {
			/* Cargar los días que ya tiene asignados */
			applicantDto.setApplicantCoffeeShopDtos(getApplicantCoffeeShopDtos(applicant));
		} catch (Exception ex) {
			LOGGER.error("Error al obtener dtos:{}", ex.getMessage());
			throw new ServiceException("Ocurri\u00F3 un error al obtener potulado.");
		}

		List<ApplicantCoffeeShop> arr = applicantCoffeeShopRepository
				.findByApplicantCoffeeShopIdApplicantId(applicant.getId());
		applicantDto.setSlctdate(
				arr.stream().map(p -> p.getApplicantCoffeeShopId().getUsedate()).collect(Collectors.joining(",")));

		return applicantDto;
	}

	@Override
	public void save(ApplicantDto applicantDto) throws ServiceException {
		if (applicantDto.getId() < 0) {
			List<String> listMats = Arrays.asList(applicantDto.getIdsStudentSelectedString().split(",")).stream()
					.map(s -> String.valueOf(s)).collect(Collectors.toList());
			Period period = periodRepository.getOneByActivePeriod(true);
			Grant grant = grantRepository.getOne(applicantDto.getGrantId());
			for (String matricula : listMats) {
				Student student = studentRepository.findStudentByRegisterCustom(matricula);
				if (student != null) {
					applicantRepository.save(Applicant.builder().period(period).grant(grant).statusGrant(false)
							.user(student.getUsers().iterator().next()).guest(null).build());
				}

			}
		} else {
			Applicant applicant = applicantRepository.getOne(applicantDto.getId());
			applicant.setStatusGrant(applicantDto.isStatusGrant());
			applicant.setGrant(grantRepository.getOne(applicantDto.getGrantId()));
			applicantRepository.save(applicant);
		}
	}

	@Override
	public ApplicantDto findByUser(UserDto userDto) {
		User usr = userService.findUserByUsername(userDto.getUsername());
		// Validar si el alumno esta en la lista de postulados y ademas está aprobado
		Applicant applicantFind = applicantRepository.findByUserAndStatusGrant(usr, true);
		ApplicantDto applicantDto = null;
		if (applicantFind != null) {
			applicantDto = new ApplicantDto();
			applicantDto.setId(applicantFind.getId());
			applicantDto.setStatusGrant(applicantFind.isStatusGrant());
		}

		return applicantDto;
	}

	@Override
	public void saveApplicantCoffeeShop(ApplicantDto applicantDto) throws ServiceException {

	}

	private List<ApplicantCoffeeShopDto> getApplicantCoffeeShopDtos(Applicant applicant) throws ParseException {
		List<ApplicantCoffeeShopDto> dtos = new ArrayList<>();
		List<ApplicantCoffeeShop> arr = applicantCoffeeShopRepository
				.findByApplicantCoffeeShopIdApplicantId(applicant.getId());
		for (ApplicantCoffeeShop appl : arr) {
			dtos.add(new ApplicantCoffeeShopDto(
					new ApplicantCoffeeShopIdDto(new ApplicantDto(),
							coffeeShopService.getById(appl.getApplicantCoffeeShopId().getCoffeeShopId()), ""),
					appl.getUseNumber(), appl.getRegisterDate(), appl.getEditDate(), null, null,
					appl.getApplicantCoffeeShopId().getUsedate(),
					FormatsConstant.DD
							.format(FormatsConstant.SDF_DDMMYYYY.parse(appl.getApplicantCoffeeShopId().getUsedate())),
					0, "", 0L));
		}
		return dtos;
	}
}
