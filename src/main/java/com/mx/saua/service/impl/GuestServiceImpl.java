package com.mx.saua.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mx.saua.dto.GrantDto;
import com.mx.saua.dto.GuestDto;
import com.mx.saua.entity.Applicant;
import com.mx.saua.entity.Grant;
import com.mx.saua.entity.Guest;
import com.mx.saua.exception.ServiceException;
import com.mx.saua.repository.ApplicantRepository;
import com.mx.saua.repository.GrantRepository;
import com.mx.saua.repository.GuestRepository;
import com.mx.saua.service.GuestService;
import com.mx.saua.service.PeriodService;

@Service("guestService")
public class GuestServiceImpl implements GuestService {

	private Logger LOGGER = LoggerFactory.getLogger(GuestServiceImpl.class);

	@Autowired
	private GuestRepository guestRepository;

	@Autowired
	private GrantRepository grantRepository;

	@Autowired
	private PeriodService periodService;

	@Autowired
	private ApplicantRepository applicantRepository;

	@Override
	public GuestDto find(GuestDto applicantDto) throws ServiceException {
		List<Guest> list = guestRepository.findAll();
		List<GuestDto> dtos = new ArrayList<GuestDto>();
		for (Guest guest : list) {

		}
		return applicantDto;
	}

	@Override
	public GuestDto prepareDto(GuestDto guestDto) {
		List<Grant> grants = grantRepository.findAll();
		List<GrantDto> grantDtos = new ArrayList<>();
		grants.stream().forEach(p -> {
			GrantDto temp = new GrantDto();
			temp.setId(p.getId());
			temp.setGrant(p.getName());
			temp.setDescription(p.getDescription());
			grantDtos.add(temp);
		});
		guestDto.setGrants(grantDtos);
		return guestDto;
	}

	@Override
	public void save(GuestDto guestDto) throws ServiceException {
		LOGGER.info("Guardando datos de invitado:{}", guestDto);
		String idRegister = "";
		List<Long> listLong = new ArrayList<>();
		
		if(guestDto.getGrantId()==-1L) {
			throw new ServiceException("Debe seleccionar una beca.");
		}
		
		if (guestDto.isNewRegister()) {
			List<Guest> guests = guestRepository.findAll();
			if (guests.isEmpty()) {
				idRegister = "G0000001";
			} else {
				guests.stream().forEach(p -> {
					String[] arr = p.getIdRegister().split("G");
					if (!arr[1].isEmpty()) {
						listLong.add(Long.valueOf(arr[1]));
					}
				});
				long number = Collections.max(listLong);
				idRegister = "G" + String.format("%07d", number + 1);
			}
			guestRepository.save(Guest.builder().idRegister(idRegister).lastname(guestDto.getLastname())
					.secondLastname(guestDto.getSecondLastname()).name(guestDto.getName()).origin(guestDto.getOrigin())
					.build());

			applicantRepository.save(Applicant.builder().grant(grantRepository.getOne(guestDto.getGrantId()))
					.period(periodService.getActivePeriod()).user(null).statusGrant(false)
					.guest(guestRepository.getOne(idRegister)).build());
		} else {
			/*guestRepository.save(Guest.builder().idRegister(guestDto.getIdRegister()).lastname(guestDto.getLastname())
					.name(guestDto.getName()).origin(guestDto.getOrigin()).secondLastname(guestDto.getSecondLastname())
					.build());*/
			Applicant applicant = applicantRepository.getOne(guestDto.getApplicantId());
			applicant.setStatusGrant(guestDto.isStatusGrant());
			applicant.setGrant(grantRepository.getOne(guestDto.getGrantId()));
			applicantRepository.save(applicant);
		}
	}
}
