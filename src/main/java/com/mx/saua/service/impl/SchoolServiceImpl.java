package com.mx.saua.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import com.mx.saua.dto.SchoolDto;
import com.mx.saua.entity.School;
import com.mx.saua.repository.SchoolRepository;
import com.mx.saua.service.SchoolService;


@Service("schoolService")
public class SchoolServiceImpl implements SchoolService{
	/*
	 * @Vic
	 */
	@Autowired
	private SchoolRepository schoolRepository;
	@Autowired
	private UserSecurityService userSecurityService;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(SchoolServiceImpl.class);

	@Override
	public List<SchoolDto> find() {
		LOGGER.info("{} - SchoolServiceImpl - Consultando el schoolRepository.", userSecurityService.getIdSession());
		List<SchoolDto> schoolDtos = new ArrayList<>();
		List<School> schools = schoolRepository.findAll();
		schools.stream().forEach(p->{
			schoolDtos.add(new SchoolDto(p.getId(), p.getName(), p.getAddress(), p.getPhone(), p.getMail(), p.getRegisterDate(), p.getEditdate()));
		});
		return schoolDtos;
	}

	@Override
	public SchoolDto getById(Long id) {
		School school = schoolRepository.getOne(id);
		SchoolDto schoolDto = new SchoolDto();
		BeanUtils.copyProperties(school, schoolDto);
		LOGGER.info("Busqueda por escuela realizada correctamente");
		return schoolDto;
	}

	@Override
	public void save(SchoolDto SchoolDto) {
		if(SchoolDto.getId() > -1L) {
			/* Update */
			School schoolUpdate = new School();
			BeanUtils.copyProperties(SchoolDto, schoolUpdate);
			schoolUpdate.setEditdate(new java.util.Date());
			schoolRepository.save(schoolUpdate);
			LOGGER.info("Actualizacion de escuela realizada correctamente");
		} else {
			/* New register */
			School schoolInsert = new School();
			BeanUtils.copyProperties(SchoolDto, schoolInsert);
			schoolInsert.setRegisterDate(new java.util.Date());
			schoolInsert.setEditdate(new java.util.Date());
			schoolRepository.save(schoolInsert);
			LOGGER.info("Insercion de escuela realizada correctamente");
		}
		
	}

	@Override
	public void delete(Long id) {
		try {
			schoolRepository.deleteById(id);
			LOGGER.info("Eliminacion de escuela realizada correctamente");
		} catch (DataIntegrityViolationException dve) {
			dve.printStackTrace();
		}
		
	}
	/* GETTERS Y SETTERS */
	public SchoolRepository getSchoolRepository() {
		return schoolRepository;
	}

	public void setSchoolRepository(SchoolRepository schoolRepository) {
		this.schoolRepository = schoolRepository;
	}


	
}
