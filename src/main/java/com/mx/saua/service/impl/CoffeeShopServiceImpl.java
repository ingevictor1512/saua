package com.mx.saua.service.impl;

import com.mx.saua.dto.CategoryDto;
import com.mx.saua.dto.CoffeeShopDto;
import com.mx.saua.dto.UserDto;
import com.mx.saua.entity.Category;
import com.mx.saua.entity.CoffeeShop;
import com.mx.saua.entity.User;
import com.mx.saua.repository.CoffeeShopRepository;
import com.mx.saua.service.CoffeeShopService;
import com.mx.saua.service.UserService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
@Service("coffeeShopService")
public class CoffeeShopServiceImpl implements CoffeeShopService {

    @Autowired
    private CoffeeShopRepository coffeeShopRepository;
    
    @Autowired
    private UserService userService;
    
    private Logger LOGGER = LoggerFactory.getLogger(CoffeeShopServiceImpl.class);

    @Override
    public List<CoffeeShopDto> find() {
        List<CoffeeShopDto> coffeeShopDtos = new ArrayList<>();
        List<CoffeeShop> coffeeShops = coffeeShopRepository.findAll();

        coffeeShops.stream().forEach(p->{
            coffeeShopDtos.add(new CoffeeShopDto(p.getId(), 
            		p.getName(), p.getGrantNumber(), userService.getById(Long.valueOf(p.getManager())).getUsername(), 
            		p.getLocation(), p.getRegisterDate(), p.getEditDate()));
        });
        return coffeeShopDtos;
    }

    @Override
    public CoffeeShopDto getById(int id) {
        CoffeeShop coffeeShop = coffeeShopRepository.getOne(id);
        CoffeeShopDto coffeeShopDto = new CoffeeShopDto();
        BeanUtils.copyProperties(coffeeShop, coffeeShopDto);
        UserDto userDto = userService.getById(Long.valueOf(coffeeShop.getManager()));
        coffeeShopDto.setManager(userDto.getUsername());
        return coffeeShopDto;
    }

    @Override
    public void save(CoffeeShopDto coffeeShopDto) {
        if(coffeeShopDto.getId() > -1L) {
            /* Update */
            CoffeeShop coffeeShopUpdate = new CoffeeShop();
            BeanUtils.copyProperties(coffeeShopDto, coffeeShopUpdate);
            User user = userService.findUserByUsername(coffeeShopUpdate.getManager());
            coffeeShopUpdate.setManager(String.valueOf(user.getIdUser()));
            coffeeShopRepository.save(coffeeShopUpdate);
        } else {
            /* New register */
            CoffeeShop coffeeShopInsert = new CoffeeShop();
            BeanUtils.copyProperties(coffeeShopDto, coffeeShopInsert);
            
            User user = userService.findUserByUsername(coffeeShopInsert.getManager());
            coffeeShopInsert.setManager(String.valueOf(user.getIdUser()));
            coffeeShopRepository.save(coffeeShopInsert);
        }

    }

    @Override
    public void delete(int id) {
        try {
            coffeeShopRepository.deleteById(id);
        } catch (DataIntegrityViolationException dve) {
            dve.printStackTrace();
        }
    }
}
