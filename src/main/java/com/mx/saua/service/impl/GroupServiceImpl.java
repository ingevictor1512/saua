package com.mx.saua.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mx.saua.converter.GroupConverter;
import com.mx.saua.converter.PeriodConverter;
import com.mx.saua.converter.ProgramConverter;
import com.mx.saua.dto.GroupDto;
import com.mx.saua.dto.PeriodDto;
import com.mx.saua.dto.ProgramDto;
import com.mx.saua.dto.StudentDto;
import com.mx.saua.dto.UserDto;
import com.mx.saua.entity.Activate;
import com.mx.saua.entity.AnswersUser;
import com.mx.saua.entity.Configuration;
import com.mx.saua.entity.Group;
import com.mx.saua.entity.OfferedClass;
import com.mx.saua.entity.Period;
import com.mx.saua.entity.Program;
import com.mx.saua.entity.Student;
import com.mx.saua.entity.StudentCharge;
import com.mx.saua.entity.User;
import com.mx.saua.enums.ConfigurationEnum;
import com.mx.saua.enums.GroupTypeEnum;
import com.mx.saua.enums.StudentStatusEnum;
import com.mx.saua.enums.StudyTypeEnum;
import com.mx.saua.enums.TurnEnum;
import com.mx.saua.exception.ServiceException;
import com.mx.saua.repository.ActivateRepository;
import com.mx.saua.repository.AnswerUserRepository;
import com.mx.saua.repository.ConfigurationRepository;
import com.mx.saua.repository.GroupRepository;
import com.mx.saua.repository.OfferedClassRepository;
import com.mx.saua.repository.PeriodRepository;
import com.mx.saua.repository.ProgramRepository;
import com.mx.saua.repository.StudentChargeRepository;
import com.mx.saua.repository.StudentRepository;
import com.mx.saua.repository.UserRepository;
import com.mx.saua.service.GroupService;
import com.mx.saua.service.StudentService;
import com.mx.saua.service.UserService;

/**
 * Servicio que maneja la logica de negocio para los grupos
 *
 * @author Benito Morales
 * @version 1.0
 *
 */
@Service("groupService")
public class GroupServiceImpl implements GroupService {

	/**
	 * Logger de la clase
	 */
	private Logger LOGGER = LoggerFactory.getLogger(GroupServiceImpl.class);
	/**
	 * Inyección de groupRepository
	 */
	@Autowired
	private GroupRepository groupRepository;

	@Autowired
	private GroupConverter groupConverter;

	@Autowired
	private ProgramRepository programRepository;

	@Autowired
	private PeriodRepository periodReporsitory;

	@Autowired
	private PeriodConverter periodConverter;

	@Autowired
	private StudentService studentService;

	@Autowired
	private StudentChargeRepository studentChargeRepository;

	@Autowired
	private StudentRepository studentRepository;

	@Autowired
	private OfferedClassRepository offeredClassRepository;

	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private AnswerUserRepository answerUserRepository;
	/**
	 * Inyección de UserSecurityService
	 */
	@Autowired
	private UserSecurityService userSecurityService;

	/**
	 * Inyeccion de periodConverter
	 */
	@Autowired
	private ProgramConverter programConverter;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private ConfigurationRepository configurationRepository;

	@Autowired
	private ActivateRepository activateRepository;
	
	@Autowired
	private GroupService groupService;

	@Override
	public GroupDto find(GroupDto groupDto) {
		List<Group> groups = groupRepository.findByPeriodActivePeriodAdmin(true);
		List<GroupDto> groupsDto = new ArrayList<>();
		groups.stream().forEach(p -> {
			groupsDto.add(groupConverter.convertGroup2GroupDto(p));
		});
		groupDto.setGroupsDto(groupsDto);
		return groupDto;
	}

	@Override
	public GroupDto findGroupsInActivePeriod(GroupDto groupDto) {
		List<Group> groups = groupRepository.findByPeriodActivePeriodAdmin(true);
		List<GroupDto> groupsDto = new ArrayList<>();
		groups.stream().forEach(p -> {
			groupsDto.add(groupConverter.convertGroup2GroupDto(p));
		});
		groupDto.setGroupsDto(groupsDto);
		return groupDto;
	}
	
	@Override
	public GroupDto findGroupsInPreviuosPeriod(GroupDto groupDto) {
		Configuration config = configurationRepository.findByKey(ConfigurationEnum.PREVIOUS_PERIOD.toString());
		List<Group> groups = groupRepository.findByPeriodId(Long.valueOf(config.getValue()));
		List<GroupDto> groupsDto = new ArrayList<>();
		groups.stream().forEach(p -> {
			groupsDto.add(groupConverter.convertGroup2GroupDto(p));
		});
		groupDto.setGroupsDto(groupsDto);
		return groupDto;
	}

	@Override
	public void save(GroupDto groupDto) throws ServiceException {
		// Obtener información del jefe de grupo
		if (groupDto.getRegister() != null && !groupDto.getRegister().isEmpty()) {
			try {
				StudentDto studentDto = studentService.getStudentByRegister(groupDto.getRegister());
				groupDto.setGroupLeader(studentDto);
			} catch (Exception ex) {
				throw new ServiceException("El jefe de grupo no existe. Matr\u00EDcula incorrecta.");
			}
		} else {
			groupDto.setGroupLeader(null);
		}

		Group group = groupConverter.convertGroupDto2Group(groupDto);
		switch (group.getTurn()) {
		case "MORNING":
			group.setTurn(TurnEnum.MORNING.getValue());
			break;
		case "AFTERNOON":
			group.setTurn(TurnEnum.AFTERNOON.getValue());
			break;
		}

		switch (group.getGroupType()) {
		case "NORMAL":
			group.setGroupType(GroupTypeEnum.NORMAL.getValue());
			break;
		case "MIXED":
			group.setGroupType(GroupTypeEnum.MIXED.getValue());
			break;
		case "REPEAT":
			group.setGroupType(GroupTypeEnum.REPEAT.getValue());
			break;
		}

		Program program = programRepository.getOne(groupDto.getEducationalProgramDto().getStudentPlanKey());
		// Obtener el programa seleccionado
		group.setEducationalProgram(programRepository.getOne(groupDto.getEducationalProgramDto().getStudentPlanKey()));

		// Obtener periodo seleccionado
		group.setPeriod(periodReporsitory.getOne(groupDto.getPeriodDto().getId()));
		try {
			groupRepository.save(group);
		} catch (Exception ex) {
			if (ex.getMessage().toString().contains("PRIMARY")) {
				throw new ServiceException("Esta grupo ya se encuentra registrado.");
			} else {
				throw new ServiceException("Ocurri\u00F3 un error.");
			}
		}
	}

	@Override
	public GroupDto getGroupById(long id) throws ServiceException {
		return groupConverter.convertGroup2GroupDto(groupRepository.getOne(id));
	}

	@Override
	public void delete(GroupDto groupDto) throws ServiceException {
		try{
			Group group = groupRepository.getOne(groupDto.getId());
			groupRepository.delete(group);
		}catch(Exception ex) {
			LOGGER.error("Error:{}", ex.getMessage());
			throw new ServiceException("Ocurrió un problema al eliminar el grupo.");
		}
	}

	@Override
	public GroupDto prepareDto(GroupDto groupDto) {
		List<Program> programs = programRepository.findAll();
		List<Period> periods = periodReporsitory.findAll();
		List<ProgramDto> programsDto = new ArrayList<>();
		List<PeriodDto> periodsDto = new ArrayList<>();
		programs.stream().forEach(p -> {
			programsDto.add(programConverter.convertProgram2ProgramDto(p));
		});
		periods.stream().forEach(p -> {
			periodsDto.add(periodConverter.convertPeriod2PeriodDto(p));
		});
		groupDto.setProgramsDto(programsDto);
		groupDto.setPeriodsDto(periodsDto);
		if (groupDto.getRegister() == null) {
			groupDto.setRegister("");
		}
		if (groupDto.getGroupLeader() == null) {
			StudentDto studentDto = new StudentDto();
			studentDto.setRegister("");
			groupDto.setGroupLeader(studentDto);
		}
		return groupDto;
	}

	@Override
	public List<GroupDto> find() {
		List<Group> groups = groupRepository.findAll();
		List<GroupDto> groupsDto = new ArrayList<>();
		groups.stream().forEach(p -> {
			groupsDto.add(groupConverter.convertGroup2GroupDto(p));
		});
		return groupsDto;
	}

	@Override
	public List<GroupDto> findByProgram(String programName) {
		Program program = programRepository.findByStudentPlanName(programName);
		List<Group> groups = groupRepository.findByEducationalProgramAndPeriodActivePeriodOrderByNameAsc(program, true);
		List<GroupDto> groupsDto = new ArrayList<>();
		for (Group group : groups) {
			groupsDto.add(groupConverter.convertGroup2GroupDto(group));
		}
		return groupsDto;
	}

	@Override
	public Object[][] findObjectByProgram(String idprogram) {
		Program program = programRepository.getOne(idprogram);
		List<Group> groups = groupRepository.findByEducationalProgramAndPeriodActivePeriodOrderByNameAsc(program, true);
		Object[][] arr = new Object[groups.size()][2];

		AtomicInteger count = new AtomicInteger(0);
		groups.stream().forEach(p -> {
			int j = count.getAndIncrement();
			arr[j][0] = p.getId();
			arr[j][1] = p.getName() + " " + p.getTurn() + " [" + p.getEducationalProgram().getAbr() + "]";
		});
		return arr;
	}

	@Override
	public Object[][] findObjectByGrouPromote(long idGroup) throws ServiceException{
		Period period = periodReporsitory.getOneByActivePeriod(true);
		Program program = null;
		try {
			program = programRepository.getOne(groupService.getGroupById(idGroup).getEducationalProgramDto().getStudentPlanKey());
		} catch (ServiceException e) {
			LOGGER.error("Error:{}", e.getMessage());
			throw new ServiceException("Ocurrió un error al recuperar los grupos.");
		}
		/**
		 * 19/082023: Se obtienen los grupos del periodo actual
		 */
		List<Group> list = groupRepository.findByPeriodIdAndEducationalProgram(period.getId(),program);
		Object[][] datos = new Object[list.size()][2];
		AtomicInteger count = new AtomicInteger(0);
		list.stream().forEach(p -> {
			int i = count.getAndIncrement();
			datos[i][0] = p.getId();
			datos[i][1] = p.getName() + " - " + p.getEducationalProgram().getAbr() + "[" + p.getTurn() + "]["
					+ p.getPeriod().getSchoolPeriod() + "]";
		});
		return datos;
	}

	@Override
	public void promote(GroupDto groupDto) throws ServiceException {
		Group group = groupRepository.getOne(Long.valueOf(groupDto.getIdGroup()));
		Period period = periodReporsitory.getOneByActivePeriod(true);
		LOGGER.info("{} - Inicia promocion de alumnos[grupo:{}].", userSecurityService.getIdSession(),
				group.getName() + " - " + group.getEducationalProgram().getAbr());

		if (group.isPromoted()) {
			LOGGER.info("{} - Este grupo ya ha sido promovido.", userSecurityService.getIdSession());
			throw new ServiceException("Este grupo ya ha sido promovido.");
		}

		Group groupPromote = groupRepository.getOne(Long.valueOf(groupDto.getIdGroupPromote()));
		LOGGER.info("promoteService -> GROUPPROMOTE:{}", groupPromote.getName());
		List<Student> students = new ArrayList<>();
		List<Student> studentsCharge = studentRepository.findByGroupId(group.getId());

		for (Student studentCharge : studentsCharge) {
			if (!studentCharge.getStudentStatus().getDescription().equals(StudentStatusEnum.GRADUATED.toString())
					|| !studentCharge.getStudentStatus().getDescription()
							.equals(StudentStatusEnum.TEMPORARY_LOW.toString())
					|| !studentCharge.getStudentStatus().getDescription()
							.equals(StudentStatusEnum.DEFINITIVE_LOW.toString())) {
				//studentCharge.setGroup(groupPromote);
				//students.add(studentCharge);
				//revisamos si contestó la encuesta del proximo semestre para agregarlo
				List<Activate> resultQuiz = new ArrayList<>();
				User user = userService.findUserByUsername(studentCharge.getRegister());
				resultQuiz = activateRepository.findActivateByUserAndPeriod(user, period);
				if(!resultQuiz.isEmpty()) {
					//Faltaría validar si la respuesta es diferente a ninguno
					studentCharge.setGroup(groupPromote);
					students.add(studentCharge);
				}
			}
		}
		int semesterNew = 0;
		String firstCharacter = "";
		try {
			firstCharacter = groupPromote.getName().substring(0, 1);
			semesterNew = Integer.valueOf(firstCharacter);
		} catch (Exception e) {
			semesterNew = 0;
		}
		LOGGER.info("{} - Asignando el nuevo semestre {}.", userSecurityService.getIdSession(), semesterNew);
		for (Student s : students) {

			if (semesterNew != 0) {
				s.setSemester(semesterNew);
			}
		}
		LOGGER.info("{} - Termina asignación del nuevo semestre {}.", userSecurityService.getIdSession(), semesterNew);
		if (students.size() <= 0) {
			LOGGER.info("{} - Este grupo no tiene alumnos asignados.", userSecurityService.getIdSession());
			throw new ServiceException("Este grupo a\u00FAn no tiene alumnos asignados.");
		}

		studentRepository.saveAll(students);
		try {
			group.setPromoted(true);
			groupRepository.save(group);
			LOGGER.info("{} - Termina promocion de alumnos.", userSecurityService.getIdSession());
		} catch (Exception ex) {
			LOGGER.error("Error al guardar cambios en grupo promovido:{}", ex.getMessage());
			throw new ServiceException("Ocurri\u00F3 un error al pomover grupo.");
		}
	}

	@Override
	public int setAcademicChargeByGroup(long idGroup) throws ServiceException {
		LOGGER.info("{} - Inicia GroupServiceImpl {}.", userSecurityService.getIdSession(), idGroup);
		/* Obtener el grupo educativo que se procesara */
		Group groupToAssign = groupRepository.getOne(idGroup);
		// Obtenemos las materias obligatorias ofertadas de cada grupo
		List<OfferedClass> offeredClassObligatory = offeredClassRepository.findByGroupRequired(groupToAssign.getId());

		// Obtenemos el listado de alumnos que pertenecen al grupo
		List<Student> listStudents = studentRepository.findStudentsByGroupAndStatus(groupToAssign.getId(), 1L);
		LOGGER.info("El numero de alumnos recuperados del grupo es: " + listStudents.size());
		for (Student student : listStudents) {
			/*
			 * Verificamos si el alumno contesto la encuesta, si contesto se le asignan las
			 * uaps
			 */
			Optional<User> user = userRepository.findByUsername(student.getRegister());
			List<AnswersUser> userAnswer = answerUserRepository.finByUserId(user.get().getIdUser());
			//if (userAnswer.size() > 0) {
				LOGGER.info("El alumno " + student.getRegister() + " contesto encuesta, por lo tanto se le cargara");
				// if(!"RMAX".contains(student.getGroup().getName())) {
				for (OfferedClass xClass : offeredClassObligatory) {
					LOGGER.info("        -Clase obligatoria recuperada: " + xClass.getUap().getName() + " del grupo: "
							+ xClass.getGroup().getName() + " " + xClass.getGroup().getTurn() + " del PE: "
							+ xClass.getUap().getEducationalProgram().getAbr() + " Tipo uap: "
							+ xClass.getUap().getUapType().getCategory());

					// Buscamos si el alumno ya tiene esa materia cargada en carga alumno mandamos
					// matricula, id clase ofertada y periodo activo
					List<StudentCharge> existStudentCharges = studentChargeRepository
							.findStudentChargeByOfferedClassUapKeyAndRegister(xClass.getUap().getKey(),
									student.getRegister(), true);
					if (existStudentCharges.size() > 0) {
						LOGGER.info("El alumno " + student.getRegister()
								+ " tiene la uap asignada, por lo tanto no se le cargara");
					} else {
						Date current = new Date();
						LOGGER.info("El alumno " + student.getRegister()
								+ " no tiene la uap asignada, por lo tanto se le cargara la uap");
						// Asignando la uap al alumno
						xClass.setUapCharge(xClass.getUapCharge() + 1);

						StudentCharge studentCharge = new StudentCharge();
						studentCharge.setAuthorizationKey("");
						studentCharge.setComun(xClass.getUap().getCommon_code());
						studentCharge.setEditDate(current);
						studentCharge.setOfferedClass(xClass);
						studentCharge.setPeriod(xClass.getGroup().getPeriod());
						studentCharge.setRegisterDate(current);
						studentCharge.setStudent(student);
						studentCharge.setStudyType(StudyTypeEnum.STUDY.STUDY);
						try {
							studentChargeRepository.save(studentCharge);
							LOGGER.info("{} - Termina guardado de carga academica.",
									userSecurityService.getIdSession());

						} catch (Exception ex) {
							throw new ServiceException("Ocurri\u00F3 un error al guardar la clase seleccionada.");
						}
					}

				}
				// }
			//} else {
			//	LOGGER.info(
			//			"El alumno " + student.getRegister() + " NO CONTESTO encuesta, por lo tanto NO se le cargara");
			//}
		}

		return 1;

	}

}
