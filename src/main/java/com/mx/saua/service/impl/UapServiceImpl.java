package com.mx.saua.service.impl;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import com.mx.saua.converter.GroupConverter;
import com.mx.saua.converter.PeriodConverter;
import com.mx.saua.converter.ProgramConverter;
import com.mx.saua.converter.TeacherConverter;
import com.mx.saua.converter.UapConverter;
import com.mx.saua.converter.UapTypeConverter;
import com.mx.saua.dto.OfferedClassDto;
import com.mx.saua.dto.PhaseDto;
import com.mx.saua.dto.ProgramDto;
import com.mx.saua.dto.StudentChargeDto;
import com.mx.saua.dto.UapDto;
import com.mx.saua.dto.UapTypeDto;
import com.mx.saua.entity.OfferedClass;
import com.mx.saua.entity.Period;
import com.mx.saua.entity.Phase;
import com.mx.saua.entity.Program;
import com.mx.saua.entity.StudentCharge;
import com.mx.saua.entity.Uap;
import com.mx.saua.entity.UapType;
import com.mx.saua.entity.User;
import com.mx.saua.exception.ServiceException;
import com.mx.saua.repository.OfferedClassRepository;
import com.mx.saua.repository.PeriodRepository;
import com.mx.saua.repository.PhaseRepository;
import com.mx.saua.repository.ProgramRepository;
import com.mx.saua.repository.StudentChargeRepository;
import com.mx.saua.repository.UapRepository;
import com.mx.saua.repository.UapTypeRepository;
import com.mx.saua.repository.UserRepository;
import com.mx.saua.service.StudentService;
import com.mx.saua.service.UapService;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;

/**
 * Servicio que maneja la logica de negocio para las uaps
 * @author Vic
 * @version 1.0
 *
 */
@Service("uapService")
public class UapServiceImpl implements UapService{
	/**
	 * Logger de la clase
	 */
	private Logger LOGGER = LoggerFactory.getLogger(UapServiceImpl.class);
	/**
	 * Inyección de Repository
	 */
	@Autowired
	private UapRepository uapRepository;
	
	@Autowired
	private UapTypeRepository uapTypeRepository;
	
	@Autowired
	private PhaseRepository phaseRepository;
	
	@Autowired
	private ProgramRepository programRepository;
	
	/**
	 * Inyeccion de Converter
	 */
	//Crear el converter para la uap
	@Autowired
	private ProgramConverter programConverter;
	
	@Autowired
	private UapTypeConverter uapTypeConverter;
	
	@Autowired
	private UapConverter uapConverter;

	/**
	 * Inyeccion de userSecurityService
	 */
	@Autowired
	private UserSecurityService userSecurityService;
	
	/**
	 * Inyeccion de userRepository
	 */
	@Autowired
	private UserRepository userRepository;

	/**
	 * Inyeccion de periodConverter
	 */
	@Autowired
	private PeriodConverter periodConverter;
	
	/**
	 * Inyeccion de teacherConverter
	 */
	@Autowired
	private TeacherConverter teacherConverter;
	
	/**
	 * Inyeccion de groupConverter
	 */
	@Autowired
	private GroupConverter groupConverter;

	@Autowired
	private OfferedClassRepository offeredClassRepository;
	
	@Autowired
	private PeriodRepository periodRepository;
	
	@Autowired
	private StudentChargeRepository studentChargeRepository;
	
	@Autowired
	private StudentService studentService;
	
	@Override
	public UapDto find(UapDto uapDto) {
		List<Uap> uaps = uapRepository.findAll();
		List<UapDto> uapDtos = new ArrayList<>();
		uaps.stream().forEach(p->{
			UapDto uapDt = new UapDto();
			BeanUtils.copyProperties(p, uapDt);
			uapDtos.add(uapDt);
		});
		uapDto.setUapDtos(uapDtos);
		return uapDto;
	}

	@Override
	public void save(UapDto uapDto) throws ServiceException {
		Date current = new Date();
		if(uapDto.isNewRegister()) {
			/* New uap */
			LOGGER.info("Entro a New register");
			Uap uapInsert = new Uap();
			uapDto.setEditdate(current);
			uapDto.setRegisterDate(current);
			BeanUtils.copyProperties(uapDto, uapInsert);
			Program program = new Program();
			program = programRepository.getOne(uapDto.getEducationalProgram().getStudentPlanKey());
			uapInsert.setEducationalProgram(program);
			
			UapType uapType = new UapType();
			uapType = uapTypeRepository.getOne(uapDto.getUapType().getId());
			uapInsert.setUapType(uapType);
			
			Phase phase = new Phase();
			phase = phaseRepository.getOne(uapDto.getPhaseUaps().getId());
			uapInsert.setPhaseUaps(phase);
			LOGGER.info("Valor de uapInsert" + uapInsert.toString());
			uapRepository.save(uapInsert);
			
	
		} else {
			/* Update uap */
			LOGGER.info("Entro a Update");
			uapDto.setEditdate(current);
			Uap uapUpdate = new Uap();
			BeanUtils.copyProperties(uapDto, uapUpdate);
			Program program = new Program();
			program = programRepository.getOne(uapDto.getEducationalProgram().getStudentPlanKey());
			uapUpdate.setEducationalProgram(program);
			
			UapType uapType = new UapType();
			uapType = uapTypeRepository.getOne(uapDto.getUapType().getId());
			uapUpdate.setUapType(uapType);
			
			Phase phase = new Phase();
			phase = phaseRepository.getOne(uapDto.getPhaseUaps().getId());
			uapUpdate.setPhaseUaps(phase);
			LOGGER.info("Valor de uapUpdate" + uapUpdate.toString());
			uapRepository.save(uapUpdate);
			
		}
		
	}

	@Override
	public void delete(UapDto uapDto) throws ServiceException {
		Uap uap = uapRepository.getOne(uapDto.getKey());
		try {
			uapRepository.delete(uap);
		}catch(Exception ex) {
			throw new ServiceException("No puede eliminar esta UAP, ya que se encuentra ligada a otros registros.");
		}
	}
	
	@Override
	public void deleteFromList(String list) throws ServiceException {
		String str = "";
		if(list.startsWith(",")) {
			str = list.substring(1, list.length());
		}else {
			str = list;
		}
		LOGGER.info("STR:{}", str);
		String[] ids = str.split("\\,");
		List<StudentCharge> array = new ArrayList<>();
		for(int i = 0; i < ids.length; i++) {
			if(!ids[i].contains(",")) {
				array.add(studentChargeRepository.getOne(Long.valueOf(ids[i])));
			}
		}
		
		UserDetails usr = userSecurityService.getUserCurrentDetails();
		if(usr.getAuthorities().iterator().next().getAuthority().equals("STUDENT")) {
			//Validar que las UAPs a eliminar correspondan al alumno
			StudentChargeDto studenChargetDto = studentService.findChargeByRegister(usr.getUsername());
			for(StudentCharge dto : array) {
				LOGGER.info("Charge:{}",dto.getId());
					if(!containsInArray(dto, studenChargetDto.getStudentCharges())) {
						LOGGER.error("Las UAPs a eliminar no te corresponden.");
						throw new ServiceException("Las UAPs a eliminar no corresponden al alumno.");
					}
				}
		}
		
		try {
			LOGGER.info("Eliminar[{}]:{}", array.size(), array.toString());
			studentChargeRepository.deleteAll(array);
		}catch(Exception ex) {
			throw new ServiceException("Ocurrió un error al eliminar las UAPs seleccionadas.");
		}
	}

	private boolean containsInArray(StudentCharge p, List<StudentChargeDto> array) {
		boolean flag = false;
		for (StudentChargeDto studentCharge : array) {
			LOGGER.info("compare:[{}] - [{}]", studentCharge.getId(), p.getId());
			if(studentCharge.getId()==p.getId()) {
				LOGGER.info("true");
				flag = true;
				break;
			}
		}
		return flag;
	}

	@Override
	public UapDto prepareDto(UapDto uapDto) {
		List<Program> programs = programRepository.findAll();
		List<ProgramDto> programsDto = new ArrayList<>();
		/*Tipos de uaps */
		List<UapType> uapTypes = uapTypeRepository.findAll();
		List<UapTypeDto> uapTypeDtos = new ArrayList<>();
		/*Fases de uaps */
		List<Phase> phases = phaseRepository.findAll();
		List<PhaseDto> phaseDtos = new ArrayList<>();
		
		programs.stream().forEach(p->{
			programsDto.add(programConverter.convertProgram2ProgramDto(p));
		});
		
		uapTypes.stream().forEach(p->{
			uapTypeDtos.add(uapTypeConverter.convertUapType2UapTypeDto(p));
		});
		
		phases.stream().forEach(p->{
			PhaseDto phaseDto = new PhaseDto();
			BeanUtils.copyProperties(p, phaseDto);
			phaseDtos.add(phaseDto);
		});
		uapDto.setProgramsDto(programsDto);
		uapDto.setUapTypeDtos(uapTypeDtos);
		uapDto.setPhaseDtos(phaseDtos);
		
		return uapDto;
	}

	@Override
	public UapDto getUapByKey(String key) throws ServiceException {
		UapDto uapDto = new UapDto();
		Uap uap = uapRepository.getOne(key);
		BeanUtils.copyProperties(uap, uapDto);
		return uapDto;
	}

	@Override
	public List<Uap> findByName(String nameLike) {
		String split[] = nameLike.split("\\-");
		Program program = programRepository.findByStudentPlanName(split[0]);
		List<Uap> uaps = uapRepository.findByNameContainingIgnoreCaseAndEducationalProgram(split[1], program);
		return uaps;
	}

	@Override
	public List<UapDto> findByProgram(String programName) {
		List<UapDto> uapsDto = new ArrayList<>();
		Program program = programRepository.findByStudentPlanName(programName);
		List<Uap> uaps = uapRepository.findByEducationalProgramOrderByNameAsc(program);
		uaps.stream().forEach(p->{
			uapsDto.add(uapConverter.convertUap2UapDto(p));
		});
		return uapsDto;
	}

	@Override
	public Object[][] findByProgramForCatalog(String idprogram){
		Program program = programRepository.getOne(idprogram);
		List<Uap> uaps = uapRepository.findByEducationalProgramOrderByNameAsc(program);
		Object[][] arr = new Object[uaps.size()][2];
		
		AtomicInteger count = new AtomicInteger(0);
		uaps.stream().forEach(p -> {
			int j = count.getAndIncrement();
			arr[j][0] = p.getKey();
			arr[j][1] = p.getName();
		});
		return arr;
	}
	
	@Override
	public byte[] exportToPdfFile() throws ServiceException {
		LOGGER.info("{} - Inicia exportToPdfFile.", userSecurityService.getIdSession());
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		List<OfferedClassDto> chargeDtos = new ArrayList<>();
		List<OfferedClass> arr = new ArrayList<>();
		OfferedClassDto offeredClassDto = new OfferedClassDto();
		List<OfferedClassDto> dtos = new ArrayList<>();
		List<OfferedClass> offeredClasses = offeredClassRepository.findByPeriodActivePeriod(true);
		//List<OfferedClass> offeredClasses = offeredClassRepository.findAll();findByPeriodActivePeriod
		//LOGGER.info("{} - Total de clases ofertadas recuperados:{}", userSecurityService.getIdSession(), offeredClasses.size());
		//arr = offeredClassRepository.findAll();
		
		offeredClasses.stream().forEach(p -> {
			long ofertadas = studentChargeRepository.countOfferedClass(p.getId());
			long autorizadas = studentChargeRepository.countOfferedClassAuthorized(p.getId());
			
			dtos.add(new OfferedClassDto(p.getId(), periodConverter.convertPeriod2PeriodDto(p.getPeriod()),
					teacherConverter.convertTeacher2TeacherDto(p.getTeacher()), 
					p.getUapCharge(), p.getSemester(), p.getMaxCharge(), p.getMinCharge(), 
					uapConverter.convertUap2UapDto(p.getUap()), 
					groupConverter.convertGroup2GroupDto(p.getGroup()), null, ofertadas - autorizadas, autorizadas));
		});
		
		offeredClassDto.setOfferedClasses(dtos);
		chargeDtos.add(offeredClassDto);
		LOGGER.info("{} - Total de registros recuperados:{}", userSecurityService.getIdSession(), offeredClasses.size());
		JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(chargeDtos);
		InputStream archivo = StudentServiceImpl.class.getResourceAsStream("/uaps_ofertadas.jasper");
		
		LOGGER.info("{} - Inicia seteo de parametros.", userSecurityService.getIdSession());

		Period period = periodRepository.getOneByActivePeriod(true);
		
		Map<String, Object> parameters = new HashMap<>();		
		parameters.put("P_SCHOOL_PERIOD", period.getSchoolPeriod());
		LOGGER.info("{} - Termina seteo de parametros.", userSecurityService.getIdSession());
		LOGGER.info("{} - Inicia llenado de reporte.", userSecurityService.getIdSession());
		JasperPrint jasperPrint = null;
		try {
			jasperPrint = JasperFillManager.fillReport(archivo, parameters, dataSource);
		} catch (JRException ex4) {
			throw new ServiceException("Ocurri\u00F3 un error al llenar reporte.");
		}

		JRPdfExporter exporter = new JRPdfExporter();

		exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
		exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, baos);

		try {
			exporter.exportReport();
		} catch (JRException ex5) {
			throw new ServiceException("Ocurri\u00F3 un error al exportar papeleta PDF.");
		}

		byte[] out = baos.toByteArray();
		LOGGER.info("{} - Termina exportToPdfFile.", userSecurityService.getIdSession());

		return out;
	}	
	
	@Override
	public Resource loadStatisticsUAPsAsResource() throws ServiceException {
		LOGGER.info("{} - Inicia loadStatisticsUAPsAsResource.", userSecurityService.getIdSession());
		User userSolicitud = userRepository.findByUsername(userSecurityService.getUserCurrentDetails().getUsername())
				.orElseThrow(() -> new ServiceException("Usuario no encontrado."));
		
		if(!userSolicitud.getAuthorities().iterator().next().getAuthority().equals("ADMIN")) {
			LOGGER.info("{} - El usuario no esta autorizado para imprimir este reporte.", userSecurityService.getIdSession());
			throw new ServiceException("502");
		}
		
		byte[] pdf = null;
		try {
			pdf = exportToPdfFile();
		}catch(ServiceException se) {
			LOGGER.error("{} - Error al generar reporte:{}", userSecurityService.getIdSession(), se.getMessage());
		}
		Resource resource = new ByteArrayResource(pdf, "Reporte de UAP's ofertadas.");
		if (resource.exists()) {
			LOGGER.info("{} - Termina loadStatisticsUAPsAsResource.", userSecurityService.getIdSession());
			return resource;
		} else {
			LOGGER.info("{} - Archivo no encontrado.", userSecurityService.getIdSession());
			throw new ServiceException("Archivo no encontrado.");
		}
	}
}
