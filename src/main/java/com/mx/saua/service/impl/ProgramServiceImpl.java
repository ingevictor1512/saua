package com.mx.saua.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mx.saua.dto.ProgramDto;
import com.mx.saua.dto.SchoolDto;
import com.mx.saua.entity.AnswersUser;
import com.mx.saua.entity.Group;
import com.mx.saua.entity.OfferedClass;
import com.mx.saua.entity.Program;
import com.mx.saua.entity.School;
import com.mx.saua.entity.Student;
import com.mx.saua.entity.StudentCharge;
import com.mx.saua.entity.User;
import com.mx.saua.enums.StudyTypeEnum;
import com.mx.saua.exception.ServiceException;
import com.mx.saua.repository.AnswerUserRepository;
import com.mx.saua.repository.GroupRepository;
import com.mx.saua.repository.OfferedClassRepository;
import com.mx.saua.repository.PeriodRepository;
import com.mx.saua.repository.ProgramRepository;
import com.mx.saua.repository.SchoolRepository;
import com.mx.saua.repository.StudentChargeRepository;
import com.mx.saua.repository.StudentRepository;
import com.mx.saua.repository.UserRepository;
import com.mx.saua.service.GroupService;
import com.mx.saua.service.ProgramService;

@Service("programService")
public class ProgramServiceImpl implements ProgramService{
	/*
	 * @Vic
	 */
	@Autowired
	private ProgramRepository programRepository;
	
	@Autowired
	private SchoolRepository schoolRepository;
	
	@Autowired
	private UserSecurityService userSecurityService;
	
	@Autowired
	private GroupService groupService;
	
	@Autowired
	private	GroupRepository groupRepository;
	
	@Autowired
	private OfferedClassRepository offeredClassRepository;
	
	@Autowired
	private StudentRepository studentRepository;
	
	@Autowired
	private PeriodRepository periodRepository;  
	
	@Autowired
	private StudentChargeRepository studentChargeRepository;
	
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private AnswerUserRepository answerUserRepository;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ProgramServiceImpl.class);
	
	@Override
	public List<ProgramDto> find() {
		List<Program> programs = programRepository.findAllOrderByStudentPlanNameAsc();
		List<ProgramDto> programsDto = new ArrayList<>();
		programs.forEach(p ->{
			programsDto.add(new ProgramDto(p.getStudentPlanKey(), 
					p.getStudentPlanName(), 
					p.getStudentPlanVrs(), 
					p.getAbr(), 
					p.getObservations(), null, null, null, null,false, true));
		});
		return programsDto;
	}

	@Override
	public ProgramDto find(ProgramDto programDto) {
		List<Program> programs = programRepository.findAll();
		List<ProgramDto> programDtos = new ArrayList<>();
		programs.stream().forEach(p->{
			ProgramDto dto = new ProgramDto();
			BeanUtils.copyProperties(p, dto);
			programDtos.add(dto);
		});
		programDto.setProgramDtos(programDtos);
		return programDto;
	}

	@Override
	public void save(ProgramDto programDto) throws ServiceException {
		Date current = new Date();
		if(programDto.isNewRegister()) {
			/* New uap */
			
			Program programInsert = new Program();
			programDto.setEditdate(current);
			programDto.setRegisterDate(current);
			BeanUtils.copyProperties(programDto, programInsert);
			
			School school = new School();
			school = schoolRepository.getOne(programDto.getSchool().getId());
			programInsert.setSchool(school);

			programRepository.save(programInsert);
			
		} else {
			/* Update uap */
			programDto.setEditdate(current);
			Program programUpdate = new Program();
			BeanUtils.copyProperties(programDto, programUpdate);
			
			School school = new School();
			school = schoolRepository.getOne(programDto.getSchool().getId());
			programUpdate.setSchool(school);

			programRepository.save(programUpdate);			
		}
		
	}

	@Override
	public void delete(ProgramDto programDto) throws ServiceException {
		Program program = programRepository.getOne(programDto.getStudentPlanKey());
		programRepository.delete(program);
	}

	@Override
	public ProgramDto prepareDto(ProgramDto programDto) {
		List<School> schools = schoolRepository.findAll();
		List<SchoolDto> schoolDtos = new ArrayList<>();
		schools.stream().forEach(p->{
			SchoolDto dto = new SchoolDto();
			BeanUtils.copyProperties(p, dto);
			schoolDtos.add(dto);
		});
		programDto.setSchoolDtos(schoolDtos);
		return programDto;
	}

	@Override
	public ProgramDto getProgramByKey(String key) throws ServiceException {
		ProgramDto programDto = new ProgramDto();
		Program program = programRepository.getOne(key);
		BeanUtils.copyProperties(program, programDto);
		return programDto;
	}

	@Override
	public Object[][] findObject() {
		List<Program> list = programRepository.findAll();
		Object[][] arrObject = new Object[list.size()][2];
		AtomicInteger count = new AtomicInteger(0);
		list.stream().forEach(p -> {
			int i = count.getAndIncrement();
			arrObject[i][0] = p.getStudentPlanKey();
			arrObject[i][1] = p.getStudentPlanName();
		});
		return arrObject;
	}

	@Override
	public int setAcademicChargeByProgram(String studentPlanKey) throws ServiceException {
		LOGGER.info("{} - Llego al ProgramServiceImpl con el ID de PE {}.", userSecurityService.getIdSession(), studentPlanKey);
		/* Contadores para ver si se asignaron todas las uaps */
		int assignCount =0;
		int studentsSum = 0;
		/* Obtener el programa educativo que se procesara */
		Program program = programRepository.getOne(studentPlanKey);
				
		/* Obtenemos el listado de grupos relacionados al programa educativo */
		List<Group> listGroups = groupRepository.findByEducationalProgramAndPeriodActivePeriodOrderByNameAsc(program, true);

		for(Group xGroup : listGroups) {
			//LOGGER.info("Grupo: "+xGroup.getName()+" "+xGroup.getTurn()+" Pe al que pertenece el grupo "+xGroup.getEducationalProgram().getStudentPlanKey()+" "+xGroup.getEducationalProgram().getStudentPlanName());
			
			if(!xGroup.getName().contains("RMAX")){
				//Obtenemos las materias obligatorias ofertadas de cada grupo
				List<OfferedClass> offeredClassObligatory = offeredClassRepository.findByGroupRequired(xGroup.getId());
				
				//Obtenemos el listado de alumnos que pertenecen al grupo
				List<Student> listStudents = studentRepository.findStudentsByGroupAndStatus(xGroup.getId(), 1L);
				//LOGGER.info("El numero de alumnos recuperados del grupo es: "+listStudents.size());
				studentsSum = studentsSum + listStudents.size();
				for(Student student : listStudents) {
					/* Verificamos si el alumno contesto la encuesta, si contesto se le asignan las uaps */
					Optional<User> user = userRepository.findByUsername(student.getRegister());
					List<AnswersUser> userAnswer = answerUserRepository.finByUserId(user.get().getIdUser());
					//if(userAnswer.size() > 0 || student.getSemester()==1) {
						//LOGGER.info("El alumno "+student.getRegister()+" contesto encuesta o es de primer semestre "+student.getSemester()+", por lo tanto se le cargara");
					if(!"RMAX".contains(student.getGroup().getName())) {
						for(OfferedClass xClass : offeredClassObligatory){
							//LOGGER.info("        -Clase obligatoria recuperada: "+xClass.getUap().getName()+" del grupo: "+xClass.getGroup().getName()+" "+xGroup.getTurn()+" del PE: "+xClass.getUap().getEducationalProgram().getAbr()+" Tipo uap: "+xClass.getUap().getUapType().getCategory());
							
							//Buscamos si el alumno ya tiene esa materia cargada en carga alumno mandamos matricula, id clase ofertada y periodo activo
							List<StudentCharge> existStudentCharges = studentChargeRepository.findStudentChargeByOfferedClassUapKeyAndRegister(xClass.getUap().getKey(), student.getRegister(), true);
							if(existStudentCharges.size() > 0) {
								//LOGGER.info("El alumno "+student.getRegister()+" tiene la uap asignada, por lo tanto no se le cargara");
							} else {
								Date current = new Date();
								//LOGGER.info("El alumno "+student.getRegister()+" no tiene la uap asignada, por lo tanto se le cargara la uap");
								// Asignando la uap al alumno 
								xClass.setUapCharge(xClass.getUapCharge() + 1);
		
								StudentCharge studentCharge = new StudentCharge();
								studentCharge.setAuthorizationKey("");
								studentCharge.setComun(xClass.getUap().getCommon_code());
								studentCharge.setEditDate(current);
								studentCharge.setOfferedClass(xClass);
								studentCharge.setPeriod(xClass.getGroup().getPeriod());
								studentCharge.setRegisterDate(current);
								studentCharge.setStudent(student);
								studentCharge.setStudyType(StudyTypeEnum.STUDY.STUDY);
								try {
									studentChargeRepository.save(studentCharge);
									LOGGER.info("{} - Termina guardado de carga academica.", userSecurityService.getIdSession());
									assignCount = assignCount + 1;
								} catch (Exception ex) {
									throw new ServiceException("Ocurri\u00F3 un error al guardar la clase seleccionada.");
								}
							}
							
						}
					}
				  /*} else {
					  LOGGER.info("El alumno "+student.getRegister()+" NO CONTESTO encuesta, por lo tanto NO se le cargara");
				  } */
				}
			}
	
		}
		program.setEstatusCarga(true);
		programRepository.save(program);

		return 1;

	}

}
