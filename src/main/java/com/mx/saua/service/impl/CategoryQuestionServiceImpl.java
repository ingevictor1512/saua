package com.mx.saua.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mx.saua.dto.CategoryQuestionDto;
import com.mx.saua.dto.QuestionDto;
import com.mx.saua.entity.CategoryQuestion;
import com.mx.saua.repository.CategoryQuestionRepository;
import com.mx.saua.service.CategoryQuestionService;
import com.mx.saua.service.QuestionService;

@Service("categoryQuestionService")
public class CategoryQuestionServiceImpl implements CategoryQuestionService{

	@Autowired
	private CategoryQuestionRepository categoryQuestionRepository;
	
	@Autowired
	private QuestionService questionService;
	
	@Override
	public List<CategoryQuestionDto> find() {
		List<CategoryQuestionDto> dtos = new ArrayList<>();
		List<CategoryQuestion> categories = categoryQuestionRepository.findAll();
		QuestionDto questionDto = new QuestionDto();
		dtos.add(new CategoryQuestionDto(-1, "Seleccione", ""));
		categories.forEach(p -> {
			questionDto.setCategoryQuestion(String.valueOf(p.getId()));
			QuestionDto dto = questionService.findPageable(questionDto);
			dtos.add(new CategoryQuestionDto(p.getId(), p.getName(), (dto.getTotalPages() == 0) ? "1" : String.valueOf(dto.getTotalPages())));
		});
		return dtos;
	}

}
