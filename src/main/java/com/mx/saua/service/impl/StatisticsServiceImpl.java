/**
 * 
 */
package com.mx.saua.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import com.mx.saua.entity.LoadAuthorization;
import com.mx.saua.entity.Tutorship;
import com.mx.saua.enums.AuthorityEnum;
import com.mx.saua.repository.AuthorityRepository;
import com.mx.saua.repository.LoadAuthorizationRepository;
import com.mx.saua.repository.TutorshipRepository;
import com.mx.saua.service.StatisticsService;

/**
 * @author Benito Morales
 * @version 1.0
 *
 */
@Service("statisticsService")
public class StatisticsServiceImpl implements StatisticsService{

	Logger LOGGER = LoggerFactory.getLogger(StatisticsServiceImpl.class);
	
	@Autowired
	private AuthorityRepository authorityRepository;
	@Autowired
	private TutorshipRepository tutorshipRepository;
	@Autowired
	private LoadAuthorizationRepository loadAuthorizationRepository;
	@Autowired
	private UserSecurityService userSecurityService;
	
	@Override
	public Object[][] findStatisticsForAdmin() {
		long countStudents = authorityRepository.countByAuthority(AuthorityEnum.STUDENT.toString());
		long countTeachers = authorityRepository.countByAuthority(AuthorityEnum.TEACHER.toString());
		long countTutors = authorityRepository.countByAuthority(AuthorityEnum.TUTOR.toString());
		Object[][] arrObject = new Object[3][2];
		
		arrObject[0][0] = "students";
		arrObject[0][1] = countStudents;
		arrObject[1][0] = "teachers";
		arrObject[1][1] = countTeachers;
		arrObject[2][0] = "tutors";
		arrObject[2][1] = countTutors;
		
		return arrObject;
	}

	@Override
	public Object[][] findStatisticsForTutor() {
		UserDetails userDetails = userSecurityService.getUserCurrentDetails();
		List<Tutorship> tutorships = tutorshipRepository.findByTeacherAndStatus(userDetails.getUsername(), 7L);
		long countAuthorizationLoads=0L;
		long countWithAuthorizationLoads=0L;
		/* buscamos si el alumno recuperado ya tiene su carga autorizada en Autorizacion_carga */
		for(Tutorship tutorshipTemp : tutorships) {
			LoadAuthorization loadTemp= loadAuthorizationRepository.findByStudentAndPeriod(tutorshipTemp.getStudent().getRegister(), true);
			if(loadTemp != null) {
				countAuthorizationLoads = countAuthorizationLoads + 1;
			} else {
				countWithAuthorizationLoads = countWithAuthorizationLoads + 1;
			}
		}
		
		Object[][] arrObject = new Object[2][2];
		
		arrObject[0][0] = "students";
		arrObject[0][1] = countWithAuthorizationLoads;
		arrObject[1][0] = "teachers";
		arrObject[1][1] = countAuthorizationLoads;
		LOGGER.info("Total de tutorados del tutor "+tutorships.size());
		LOGGER.info("Total de tutorados con carga autorizada "+arrObject[1][1]);
		return arrObject;
	}

}
