package com.mx.saua.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.mx.saua.dto.InquestDto;
import com.mx.saua.dto.InquestQuestionDto;
import com.mx.saua.entity.Inquest;
import com.mx.saua.entity.InquestQuestion;
import com.mx.saua.repository.InquestQuestionRepository;
import com.mx.saua.repository.InquestRepository;
import com.mx.saua.service.InquestQuestionService;
import com.mx.saua.util.PageableUtils;

@Service("inquestQuestionService")
public class InquestQuestionServiceImpl implements InquestQuestionService{

	@Autowired
	private InquestQuestionRepository inquestQuestionRepository;
	
	@Autowired
	private InquestRepository inquestRepository;

	@Autowired
	private UserSecurityService userSecurityService;
	
	private Logger LOGGER = LoggerFactory.getLogger(InquestQuestionServiceImpl.class);
	
	@Override
	public InquestQuestionDto findPageable(InquestQuestionDto inquestQuestionDto) {
		String dirStr = PageableUtils.getSortDirection(inquestQuestionDto);
		int size = 1;
		//LOGGER.info("{} - Obteniendo pagina:{}",userSecurityService.getIdSession(), inquestQuestionDto.getPage());
		inquestQuestionDto.setPage(inquestQuestionDto.getPage());
		
		Pageable pageable = PageableUtils.buildPageable(inquestQuestionDto, size, "id");
		Page<InquestQuestion> page = null;

		Inquest inquest = inquestRepository.getOne(inquestQuestionDto.getIdInquest());
		page = inquestQuestionRepository.findInquestQuestionByInquestOrderByOrderquestion(inquest, pageable);
		
		InquestQuestionDto result = new InquestQuestionDto();
		result.setInquestQuestionList(page.getContent());
		result.setDirection(dirStr);
		result.setPage(page.getNumber());
		result.setSize(page.getSize());
		result.setTotalPages(page.getTotalPages());
		result.setTotalItems(page.getTotalElements());
		
		result.setInquest(new InquestDto(
				inquest.getId(), 
				inquest.getName(), 
				inquest.getDescription(), 
				inquest.getNumberQuestions(), 
				inquest.getStatus(), null, null, null, "",""));
		result.setIdq((page.getContent().iterator().hasNext())?page.getContent().iterator().next().getQuestion().getId():-1);
		result.setQtype((page.getContent().iterator().hasNext())?page.getContent().iterator().next().getQuestion().getQuestionType().toString():"");
		result.setCurrentPageView(page.getNumber()+1);
		return result;
	}

}
