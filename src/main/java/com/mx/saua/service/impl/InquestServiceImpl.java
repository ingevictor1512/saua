package com.mx.saua.service.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mx.saua.dto.CategoryQuestionDto;
import com.mx.saua.dto.InquestDto;
import com.mx.saua.dto.QuestionDto;
import com.mx.saua.entity.CategoryQuestion;
import com.mx.saua.entity.Inquest;
import com.mx.saua.entity.InquestQuestion;
import com.mx.saua.entity.InquestRole;
import com.mx.saua.entity.Question;
import com.mx.saua.enums.AuthorityEnum;
import com.mx.saua.enums.InquestStatusEnum;
import com.mx.saua.exception.ServiceException;
import com.mx.saua.repository.CategoryQuestionRepository;
import com.mx.saua.repository.InquestQuestionRepository;
import com.mx.saua.repository.InquestRepository;
import com.mx.saua.repository.InquestRoleRepository;
import com.mx.saua.repository.QuestionRepository;
import com.mx.saua.service.Arqsrv;
import com.mx.saua.service.InquestService;
import com.mx.saua.service.QuestionService;

/**
 * Servicio para el manejo de encuestas
 * 
 * @author Benito Morales
 * @version 2.0
 *
 */
@Service("inquestService")
public class InquestServiceImpl extends Arqsrv implements InquestService {

	/**
	 * Inyección de Inquestrepository
	 */
	@Autowired
	private InquestRepository inquestRepository;

	/**
	 * Inyección de QuestionRepository
	 */
	@Autowired
	private QuestionRepository questionRepository;

	/**
	 * Inyección de CategoryQuestionRepository
	 */
	@Autowired
	private CategoryQuestionRepository categoryQuestionRepository;

	/**
	 * Inyección de QuestionService
	 */
	@Autowired
	private QuestionService questionService;

	/**
	 * Inyección de InquestQuestionRepository
	 */
	@Autowired
	private InquestQuestionRepository inquestQuestionRepository;

	/**
	 * Inyección de InquestRoleRepository
	 */
	@Autowired
	private InquestRoleRepository inquestRoleRepository;

	/**
	 * Formato de fecha hora
	 */
	private SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy kk:mm");

	@Override
	public InquestDto find(InquestDto questionDto) {
		List<Inquest> list = inquestRepository.findAll();
		List<InquestDto> dtos = new ArrayList<InquestDto>();
		for (Inquest inquest : list) {
			InquestRole inqR = inquestRoleRepository.findByInquestId(inquest.getId());
			dtos.add(new InquestDto(inquest.getId(), inquest.getName(), inquest.getDescription(),
					inquest.getNumberQuestions(), inquest.getStatus(), inquest.getCreationDate(), inquest.getEditDate(),
					null, inquest.getInquestRoles().stream().map(p -> p.getRole()).collect(Collectors.joining("|")),(new Date().after(inqR.getInquestEndDate()) || new Date().before(inqR.getInquestInitDate()))?"Inactiva":"Activa"));
		}
		questionDto.setInquests(dtos);
		return questionDto;
	}

	
	@Override
	public InquestDto prepare(InquestDto inquestDto) {
		List<CategoryQuestion> categoryQuestionList = categoryQuestionRepository.findAll();
		List<CategoryQuestionDto> categoryQuestionDtos = new ArrayList<CategoryQuestionDto>();
		categoryQuestionDtos.add(new CategoryQuestionDto(-1, "Seleccione", ""));
		QuestionDto questionDto = new QuestionDto();
		for (CategoryQuestion categoryQuestion : categoryQuestionList) {
			/**
			 * Se obtiene el numero de paginas que contine cada categoria
			 */
			questionDto.setCategoryQuestion(String.valueOf(categoryQuestion.getId()));
			QuestionDto dto = questionService.findPageable(questionDto);
			categoryQuestionDtos.add(new CategoryQuestionDto(categoryQuestion.getId(), categoryQuestion.getName(),
					(dto.getTotalPages() == 0) ? "1" : String.valueOf(dto.getTotalPages())));
		}
		inquestDto.setCategoryQuestionList(categoryQuestionDtos);
		inquestDto.setNewRegistry(true);
		return inquestDto;
	}

	
	@Override
	public void save(InquestDto inquestDto) throws ServiceException {
		Inquest inquest = null;
		List<Long> listIds = Arrays.asList(inquestDto.getQuestionsSelected().split(",")).stream()
				.map(s -> Long.parseLong(s)).collect(Collectors.toList());
		List<Question> list = questionRepository.findByQuestionIds(listIds);
		List<InquestQuestion> inquestQuestions = new ArrayList<InquestQuestion>();
		List<InquestRole> inquestRoles = new ArrayList<InquestRole>();
		try {
			if (inquestDto.getId() <= 0) {
				inquest = new Inquest(inquestDto.getId(), inquestDto.getName(), inquestDto.getDescription(),
						list.size(), inquestDto.getStatus(), new Date(), new Date(), null, null);
				inquest.setStatus(InquestStatusEnum.ACTIVA);
			} else {
				inquest = inquestRepository.getOne(inquestDto.getId());
				inquest.setName(inquestDto.getName());
				inquest.setDescription(inquestDto.getDescription());
				inquest.setNumberQuestions(list.size());
				inquest.setEditDate(new Date());
			}
			// Se integran las preguntas con la encuesta a guardar
			LOGGER.info("{} - Orden original de preguntas:{}",this.userSecurityService.getIdSession(), inquestDto.getQuestionsSelected());
			for (Question question : list) {
				try {
					inquestQuestions.add(new InquestQuestion(0L, inquest, question, findOrderByList(inquestDto.getQuestionsSelected(), question)));
				}catch(ServiceException se) {
					LOGGER.error(se.getMessage());
					throw new ServiceException("Ocurri\u00F3 un error al guardar la encuesta.");
				}
			}
			inquest.setInquestQuestions(inquestQuestions);

			String[] arrRole = inquestDto.getRoles().split("\\,");
			for (String role : arrRole) {
				Date initDate = null;
				Date endDate = null;
				if (role.equals(AuthorityEnum.STUDENT.toString())) {
					initDate = sdf.parse(inquestDto.getStudentInitDate());
					endDate = sdf.parse(inquestDto.getStudentEndDate());
				}
				if (role.equals(AuthorityEnum.TEACHER.toString())) {
					initDate = sdf.parse(inquestDto.getTeacherInitDate());
					endDate = sdf.parse(inquestDto.getTeacherEndDate());
				}
				if (role.equals(AuthorityEnum.TUTOR.toString())) {
					initDate = sdf.parse(inquestDto.getTutInitDate());
					endDate = sdf.parse(inquestDto.getTutEndDate());
				}
				inquestRoles.add(new InquestRole(0, inquest, role, initDate, endDate, InquestStatusEnum.ACTIVA));
			}
			inquest.setInquestRoles(inquestRoles);
			inquest.setNumberQuestions(listIds.size());
			inquestRepository.save(inquest);
		} catch (Exception ex) {
			LOGGER.error(ex.getMessage());
			throw new ServiceException("Ocurri\u00F3 un error al guardar la encuesta.");
		}
	}

	/**
	 * Encuentra el orden original que se seleccionó desde el front
	 * @param originalOrder Orden original
	 * @param question Pregunta a buscar
	 * @return un entero, que indica el orden de la pregunta
	 */
	private int findOrderByList(String originalOrder, Question question) throws ServiceException{
		List<String> listIds = Arrays.asList(originalOrder.split(","));
		int i = 1;
		LOGGER.info("{} - QuestionId a buscar:{}", this.userSecurityService.getIdSession(), question.getId());
		for (String id : listIds) {
			if(id.equals(String.valueOf(question.getId()))) {
				LOGGER.info("{} - Pregunta encontrada en el orden:{}", this.userSecurityService.getIdSession(), i);
				return i;
			}
			i++;
		}
		throw new ServiceException("Pregunta buscada no se encontro en el orden original.");
	}

	@Override
	public InquestDto findInquestById(long id) {
		Inquest inquest = inquestRepository.getOne(id);
		InquestDto dto = new InquestDto();
		dto.setId(inquest.getId());
		dto.setName(inquest.getName());
		dto.setDescription(inquest.getDescription());
		dto.setNumberQuestions(inquest.getNumberQuestions());
		dto.setStatus(inquest.getStatus());
		dto.setCreationDate(inquest.getCreationDate());
		dto.setEditDate(inquest.getEditDate());
		boolean isStudent = false;
		boolean isTeacher = false;
		boolean isTutor = false;
		String questionsSelected = "";

		List<QuestionDto> questions = new ArrayList<>();
		List<InquestQuestion> questionList = inquestQuestionRepository.findByInquestOrderByOrderquestion(inquest);
		for (InquestQuestion inqQuestion : questionList) {
			QuestionDto qDto = new QuestionDto();
			qDto.setId(inqQuestion.getQuestion().getId());
			qDto.setText(inqQuestion.getQuestion().getText());
			qDto.setQuestionType(inqQuestion.getQuestion().getQuestionType().toString());
			qDto.setOrder(inqQuestion.getOrderquestion());
			questions.add(qDto);
			questionsSelected += inqQuestion.getQuestion().getId() + ",";
		}
		dto.setQuestionsSelected(questionsSelected.replaceFirst(".$",""));
		dto.setQuestionsListSelected(questions);

		for (InquestRole inqRole : inquest.getInquestRoles()) {
			if (inqRole.getRole().equals(AuthorityEnum.STUDENT.toString())) {
				isStudent = true;
				dto.setStudentInitDate(sdf.format(inqRole.getInquestInitDate()));
				dto.setStudentEndDate(sdf.format(inqRole.getInquestEndDate()));
			}
			if (inqRole.getRole().equals(AuthorityEnum.TEACHER.toString())) {
				isTeacher = true;
				dto.setTeacherInitDate(sdf.format(inqRole.getInquestInitDate()));
				dto.setTeacherEndDate(sdf.format(inqRole.getInquestEndDate()));
			}
			if (inqRole.getRole().equals(AuthorityEnum.TUTOR.toString())) {
				isTutor = true;
				dto.setTutInitDate(sdf.format(inqRole.getInquestInitDate()));
				dto.setTutEndDate(sdf.format(inqRole.getInquestEndDate()));
			}
		}
		dto.setRoleStudentSelected(isStudent);
		dto.setRoleTeacherSelected(isTeacher);
		dto.setRoleTutorSelected(isTutor);
		return dto;
	}

	@Override
	public void delete(InquestDto inquestDto) throws ServiceException {
		LOGGER.info("{} - Inicia eliminar encuesta.", this.userSecurityService.getIdSession());
		try {
			Inquest inquest = inquestRepository.getOne(inquestDto.getId());
			inquestRepository.delete(inquest);
			LOGGER.info("{} - Termina eliminar encuesta.", this.userSecurityService.getIdSession());
		} catch (Exception ex) {
			LOGGER.info("{} - Termina eliminar encuesta.", this.userSecurityService.getIdSession());
			LOGGER.error(ex.getMessage());
			if (ex.getMessage().contains("constraint")) {
				throw new ServiceException("La encuesta que intenta eliminar, tiene dependencias con otras tablas..");
			}
			throw new ServiceException("Ocurri\u00F3 un error al eliminar la encuesta.");
		}
	}

	@Override
	public void update(InquestDto inquestDto) throws ServiceException {
		LOGGER.info("{} - Inicia actualización de encuesta.", this.userSecurityService.getIdSession());
		Inquest inquest = inquestRepository.getOne(inquestDto.getId());
		// Borrar las preguntas actuales de la encuesta
		 inquestQuestionRepository.deleteInquestQuestionByInquest(inquest);
		// Guardar las nuevas preguntas
		List<Long> listIds = Arrays.asList(inquestDto.getQuestionsSelected().split(",")).stream()
				.map(s -> Long.parseLong(s)).collect(Collectors.toList());
		List<Question> list = questionRepository.findByQuestionIds(listIds);

		List<InquestQuestion> inquestQuestions = new ArrayList<InquestQuestion>();
		List<InquestRole> inquestRoles = new ArrayList<InquestRole>();

		LOGGER.info("{} - Orden original de preguntas:{}",this.userSecurityService.getIdSession(), inquestDto.getQuestionsSelected());
		for (Question question : list) {
			try {
				inquestQuestions.add(new InquestQuestion(0L, inquest, question, findOrderByList(inquestDto.getQuestionsSelected(), question)));
			}catch(ServiceException se) {
				LOGGER.error(se.getMessage());
				throw new ServiceException("Ocurri\u00F3 un error al guardar la encuesta.");
			}
		}
		inquest.setInquestQuestions(inquestQuestions);
		// Actualizar las fechas por rol
		try {
			inquestRoleRepository.deleteInquestRoleByInquest(inquest);
			String[] arrRole = inquestDto.getRoles().split("\\,");
			for (String role : arrRole) {
				Date initDate = null;
				Date endDate = null;
				if (role.equals(AuthorityEnum.STUDENT.toString())) {
					initDate = sdf.parse(inquestDto.getStudentInitDate());
					endDate = sdf.parse(inquestDto.getStudentEndDate());
				}
				if (role.equals(AuthorityEnum.TEACHER.toString())) {
					initDate = sdf.parse(inquestDto.getTeacherInitDate());
					endDate = sdf.parse(inquestDto.getTeacherEndDate());
				}
				if (role.equals(AuthorityEnum.TUTOR.toString())) {
					initDate = sdf.parse(inquestDto.getTutInitDate());
					endDate = sdf.parse(inquestDto.getTutEndDate());
				}
				inquestRoles.add(new InquestRole(0, inquest, role, initDate, endDate, InquestStatusEnum.ACTIVA));
			}
			inquest.setInquestRoles(inquestRoles);
		} catch (Exception ex) {
			LOGGER.error(ex.getMessage());
			throw new ServiceException("Ocurri\u00F3 un error al guardar la encuesta.");
		}
		// Actualizar los datos de la encuesta
		inquest.setName(inquestDto.getName());
		inquest.setDescription(inquestDto.getDescription());
		inquest.setNumberQuestions(listIds.size());
		inquest.setEditDate(new Date());
		inquestRepository.save(inquest);
	}

}
