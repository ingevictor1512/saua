package com.mx.saua.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.mx.saua.converter.GroupConverter;
import com.mx.saua.converter.PeriodConverter;
import com.mx.saua.converter.ProgramConverter;
import com.mx.saua.converter.TeacherConverter;
import com.mx.saua.converter.UapConverter;
import com.mx.saua.dto.CountDto;
import com.mx.saua.dto.GroupDto;
import com.mx.saua.dto.OfferedClassDto;
import com.mx.saua.dto.ProgramDto;
import com.mx.saua.dto.StudentDto;
import com.mx.saua.dto.TeacherDto;
import com.mx.saua.entity.Group;
import com.mx.saua.entity.OfferedClass;
import com.mx.saua.entity.Student;
import com.mx.saua.entity.Uap;
import com.mx.saua.exception.ServiceException;
import com.mx.saua.repository.GroupRepository;
import com.mx.saua.repository.OfferedClassRepository;
import com.mx.saua.repository.PeriodRepository;
import com.mx.saua.repository.TeacherRepository;
import com.mx.saua.repository.UapRepository;
import com.mx.saua.service.GroupService;
import com.mx.saua.service.OfferedClassService;
import com.mx.saua.service.ProgramService;
import com.mx.saua.service.TeacherService;
import com.mx.saua.service.UapService;
import com.mx.saua.util.PageableUtils;

/**
 * Servicio que proporciona la logica de negocio para el manejo de las clases ofertadas
 * 
 * @author Benito Morales
 * @version 1.0
 *
 */
@Service("offeredClassService")
public class OfferedClassServiceImpl implements OfferedClassService{

	Logger log = LoggerFactory.getLogger(OfferedClass.class);
	
	@Autowired
	private OfferedClassRepository offeredClassRepository;
	
	@Autowired
	private PeriodConverter periodConverter;
	
	@Autowired
	private TeacherConverter teacherConverter;
	
	@Autowired
	private GroupConverter groupConverter;
	
	@Autowired
	private UapConverter uapConverter;
	
	@Autowired
	private ProgramConverter programConverter;
	
	@Autowired
	private GroupRepository groupRepository;
	
	@Autowired
	private TeacherRepository teacherRepository;
	
	@Autowired
	private UapRepository uapRepository;
	
	@Autowired
	private PeriodRepository periodRepository;
	
	@Autowired
	private ProgramService programService;
	
	@Autowired
	private TeacherService teacherService;
	
	@Autowired
	private UapService uapService;
	
	@Autowired
	private GroupService groupService;
	
	/**
	 * Inyección de UserSecurityService
	 */
	@Autowired
	private UserSecurityService userSecurityService;
	
	@Override
	public OfferedClassDto find(OfferedClassDto offeredClassDto) {
		List<OfferedClass> offeredClasses = offeredClassRepository.findByPeriodActivePeriod(true);
		List<OfferedClassDto> offeredClassesDto = new ArrayList<>();
		offeredClasses.forEach(p -> {
			offeredClassesDto.add(new OfferedClassDto(p.getId(), 
					periodConverter.convertPeriod2PeriodDto(p.getPeriod()), 
					teacherConverter.convertTeacher2TeacherDto(p.getTeacher()), 
					p.getUapCharge(), p.getSemester(), p.getMaxCharge(), p.getMinCharge(), 
					uapConverter.convertUap2UapDto(p.getUap()), groupConverter.convertGroup2GroupDto(p.getGroup()), null, 0L, 0L));
		});
		offeredClassDto.setOfferedClasses(offeredClassesDto);
		return offeredClassDto;
	}
	
	@Override
	public OfferedClassDto findPageable(OfferedClassDto offeredClassDto) {
		/*LOGGER.info("SearchByValue:{}", studentDto.getSearchByValue());
		LOGGER.info("SearchBy:{}", studentDto.getSearchBy());
		LOGGER.info("Page:{}", studentDto.getPage());*/
		
		String dirStr = PageableUtils.getSortDirection(offeredClassDto);
		int size = 10;
		
		offeredClassDto.setPage(offeredClassDto.getPage()-1);
		
		Pageable pageable = PageableUtils.buildPageable(offeredClassDto, size, "id");
		Page<OfferedClass> page = null;
		if (offeredClassDto.getSearchBy().isEmpty()) {
			page = offeredClassRepository.findByPeriodActivePeriodOrderById(pageable, true);
		} else {
			switch(offeredClassDto.getSearchBy()) {
			case "Clave":
				page = offeredClassRepository.findOfferedClassByUapKey(offeredClassDto.getSearchByValue(), pageable);
				break;
			case "UAP":
				page = offeredClassRepository.findOfferedClassByUapName(offeredClassDto.getSearchByValue(), pageable);
				break;
			case "Docente":
				page = offeredClassRepository.findOfferedClassByTeacherName(offeredClassDto.getSearchByValue(), pageable);
				break;
			case "Grupo":
				page = offeredClassRepository.findOfferedClassByGroupName(offeredClassDto.getSearchByValue(), pageable);
				break;
			case "P. Educativo":
				page = offeredClassRepository.findOfferedClassByProgram(offeredClassDto.getSearchByValue(), pageable);
				break;
			case "Semestre":
				page = offeredClassRepository.findOfferedClassBySemester(offeredClassDto.getSearchByValue(), pageable);
				break;
			}
		}
		
		OfferedClassDto result = new OfferedClassDto();
		result.setOfferedClassList(page.getContent());
		result.setDirection(dirStr);
		result.setPage(page.getNumber());
		result.setSize(page.getSize());
		result.setTotalPages(page.getTotalPages());
		result.setTotalItems(page.getTotalElements());
		result.setSearchBy(offeredClassDto.getSearchBy());
		result.setSearchByValue(offeredClassDto.getSearchByValue());
		return result;
	}

	@Override
	public void save(OfferedClassDto offeredClassDto) throws ServiceException {
		OfferedClass offeredClass = null;
		if(offeredClassDto.getId()>0) {
			//Editar registro
			offeredClass = offeredClassRepository.getOne(offeredClassDto.getId());
			offeredClassRepository.save(offeredClass);
		}else {
			//Nuevo registro
			offeredClass = new OfferedClass();
		}
		offeredClass.setGroup(groupRepository.getOne(offeredClassDto.getGroup().getId()));
		offeredClass.setMaxCharge(offeredClassDto.getMaxCharge());
		offeredClass.setMinCharge(offeredClassDto.getMinCharge());
		offeredClass.setPeriod(periodRepository.getOneByActivePeriod(true));
		offeredClass.setSemester(offeredClassDto.getSemester());
		offeredClass.setTeacher(teacherRepository.getOne(offeredClassDto.getTeacher().getId()));
		offeredClass.setUap(uapRepository.getOne(offeredClassDto.getUap().getKey()));
		log.info("{} - Guardando clase ofertada [uap:{} - uapCharge:{}].", 
				userSecurityService.getIdSession(), offeredClassDto.getUap().getKey(), 
				offeredClassDto.getUapCharge());
		offeredClass.setUapCharge(offeredClassDto.getUapCharge());
		offeredClassRepository.save(offeredClass);
	}

	@Override
	public OfferedClassDto getOfferedClassById(long id) throws ServiceException {
		OfferedClass offeredClass = offeredClassRepository.getOne(id);
		OfferedClassDto offeredClassDto = new OfferedClassDto(offeredClass.getId(), periodConverter.convertPeriod2PeriodDto(offeredClass.getPeriod()), 
				teacherConverter.convertTeacher2TeacherDto(offeredClass.getTeacher()), offeredClass.getUapCharge(), 
				offeredClass.getSemester(), offeredClass.getMaxCharge(), 
				offeredClass.getMinCharge(), uapConverter.convertUap2UapDto(offeredClass.getUap()), groupConverter.convertGroup2GroupDto(offeredClass.getGroup()), null, 0L, 0L);
		offeredClassDto.setProgram(programConverter.convertProgram2ProgramDto(offeredClass.getUap().getEducationalProgram()));
		
		//Cargando uaps que corresponden al programa educativo
		offeredClassDto.setUaps(uapService.findByProgram(offeredClass.getUap().getEducationalProgram().getStudentPlanName()));
		
		//Cargando grupos que corresponden al programa educativo
		offeredClassDto.setGroups(groupService.findByProgram(offeredClass.getUap().getEducationalProgram().getStudentPlanName()));
		offeredClassDto.setTeachers(teacherService.findAll());
		offeredClassDto.setPrograms(programService.find());
		return offeredClassDto;
	}

	@Override
	public void delete(OfferedClassDto offeredClassDto) throws ServiceException {
		OfferedClass offeredClass = offeredClassRepository.getOne(offeredClassDto.getId());
		offeredClassRepository.delete(offeredClass);
	}

	@Override
	public OfferedClassDto prepareDto(OfferedClassDto offeredClassDto) {		
		List<GroupDto> groups = new ArrayList<>();
		groups.add(new GroupDto(-1L, "No disponibles", null, null, null, null, null, null, null, null, null, null, null, ""));
		offeredClassDto.setGroups(groups);
		
		List<ProgramDto> programs = new ArrayList<>();
		List<ProgramDto> programDtos = programService.find();
		programs.add(new ProgramDto("SN", "Seleccione", null, null, null, null, null, null, null, false, false));
		programDtos.stream().forEach(p->{
			programs.add(p);
		});
		offeredClassDto.setPrograms(programs);
		
		List<TeacherDto> teachers = new ArrayList<>();
		List<TeacherDto> teachersDto = teacherService.findAll();
		teachers.add(new TeacherDto(-1, "Seleccione", "", "", "", null, null, null, null, null, null,""));
		teachersDto.stream().forEach(p->{
			teachers.add(p);
		});
		offeredClassDto.setTeachers(teachers);
		return offeredClassDto;
	}

	@Override
	public Object[][] getUapsByGroup(long idGroup) {
        List<OfferedClass> list = offeredClassRepository.findByGroup(idGroup);
        Object[][] datos = new Object[list.size()][3];
        AtomicInteger count = new AtomicInteger(0);
		list.stream().forEach(p -> {
			int i = count.getAndIncrement();
			datos[i][0] = p.getUap().getKey();
        	datos[i][1] = p.getUap().getName(); p.getTeacher().getName();
        	datos[i][2] = p.getTeacher().getName()+" "+p.getTeacher().getLastname() + " "+p.getTeacher().getSecondLastname();
		});
		return datos;
	}


	@Override
	public Object[][] getUapsRequiredByGroup(long idGroup) {
        List<OfferedClass> list = offeredClassRepository.findByGroupRequired(idGroup);
        Object[][] datos = new Object[list.size()][2];
        AtomicInteger count = new AtomicInteger(0);
		list.stream().forEach(p -> {
			int i = count.getAndIncrement();
			datos[i][0] = p.getUap().getName();
        	datos[i][1] = p.getTeacher().getName()+" "+p.getTeacher().getLastname()+" "+p.getTeacher().getSecondLastname();
		});
		return datos;
	}
	
	@Override
	public String countStudentsByGroup(CountDto countDto) {
        Group group = groupRepository.getOne(countDto.getIdGroup());
        Uap uap = uapRepository.findByUapID(countDto.getUap());
		OfferedClass offeredClass = offeredClassRepository.findOfferedClassByGroupAndUap(group, uap);
        log.info("Total de alumnos en [{}-{}][{}]:{}",group.getId(),group.getName(), uap.getKey(), offeredClass.getUapCharge());
		return String.valueOf(offeredClass.getUapCharge());
	}
	
	@Override
	public Object[][] findByUapsForGroup(long idgroup){
		Group group = groupRepository.getOne(idgroup);
		List<OfferedClass> uaps = offeredClassRepository.findByGroup(group);
		Object[][] arr = new Object[uaps.size()][2];
		
		AtomicInteger count = new AtomicInteger(0);
		uaps.stream().forEach(p -> {
			int j = count.getAndIncrement();
			arr[j][0] = p.getId();
			arr[j][1] = p.getUap().getName();
		});
		return arr;
	}
}
