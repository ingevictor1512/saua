package com.mx.saua.service.impl;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import com.mx.saua.dto.GrantDto;
import com.mx.saua.entity.Grant;
import com.mx.saua.exception.ServiceException;
import com.mx.saua.repository.GrantRepository;
import com.mx.saua.repository.UserRepository;
import com.mx.saua.service.GrantService;

@Service("grantService")
public class GrantServiceImpl implements GrantService{

    	/**
	 * Logger de la clase
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(GrantServiceImpl.class);

    @Autowired
	private GrantRepository grantRepository;
	@Autowired
	private UserSecurityService userSecurityService;
	@Autowired
	private UserRepository userRepository;

    @Override
    public List<GrantDto> find() {
        List<GrantDto> grantDtos = new ArrayList<>();
		List<Grant> grants = grantRepository.findAll();
		grants.stream().forEach(p->{
			grantDtos.add(new GrantDto(p.getId(), 
                    p.getName(),
                    p.getDescription()));
		});
		LOGGER.info("Busqueda de becas realizada correctamente");
		return grantDtos;
    }

    @Override
    public void save(GrantDto grantDto) throws ServiceException {
        if(grantDto.getId() > -1L) {
			//Update
			//Cargando la beca completa
			Grant grant = grantRepository.getOne(grantDto.getId());
			grant.setDescription(grantDto.getDescription());
            grant.setName(grantDto.getGrant());			
			grantRepository.save(grant);
		} else {
			/* New register */
			Grant grantInsert = new Grant();
			BeanUtils.copyProperties(grantDto, grantInsert);
			grantRepository.save(grantInsert);
			LOGGER.info("Insercion de la beca realizada correctamente");
		}
        
    }

    @Override
    public void delete(Long id) throws ServiceException {      
        try {
			grantRepository.deleteById(id);
			LOGGER.info("Se elimi\u00F3o la beca correctamente.");
		} catch (DataIntegrityViolationException dve) {
			dve.printStackTrace();
		}
        
    }

    @Override
    public GrantDto getById(Long id) {
		Grant grant = grantRepository.getOne(id);
		GrantDto grantDto = new GrantDto();
		BeanUtils.copyProperties(grant, grantDto);
		LOGGER.info("Busqueda por beca realizada correctamente");
		return grantDto;
    }

	@Override
	public ByteArrayInputStream exportAllGrants() throws IOException {
			LOGGER.info("{} - Inicia exportAllGrants.", userSecurityService.getIdSession());
			String [] columns = {"ID", "BECA", "DESCUENTO"};

			Workbook workbook = new HSSFWorkbook();
			ByteArrayOutputStream stream = new ByteArrayOutputStream();
			/* Colocamos en negrita */
			CellStyle headerStyle = workbook.createCellStyle();
			Font font = workbook.createFont();
			font.setBold(true);
			headerStyle.setFont(font);

			Sheet sheet = workbook.createSheet("Becas");
			Row row = sheet.createRow(0);

			for(int i = 0; i < columns.length; i++) {
				Cell cell = row.createCell(i);
				cell.setCellValue(columns[i]);
				cell.setCellStyle(headerStyle);
			}
			List<Grant> list = grantRepository.findAll();
			int initRow = 1;
			for (Grant grant : list) {
				row = sheet.createRow(initRow);
				row.createCell(0).setCellValue(grant.getId());//ID
				row.createCell(1).setCellValue(grant.getName());//BECA
				row.createCell(2).setCellValue("$ "+grant.getDescription());//DESCRIPCION
				initRow++;
			}

			workbook.write(stream);
			workbook.close();
			LOGGER.info("{} - Termina exportAllGrants.", userSecurityService.getIdSession());
			return new ByteArrayInputStream(stream.toByteArray());
		}

	/*@Override
	public Resource exportBecarios(long idInquest) throws ServiceException {
		Grant grant = grantRepository.getOne(idInquest);
		//Inquest inquest = inquestRepository.getOne(idInquest);
		LOGGER.info("{} - Inicia exportBecarios, beca:{}", userSecurityService.getIdSession(), grant.getGrant());
		List<User> users = userRepository.findByGrantId(grant.getId());
		LOGGER.info("{} - Total de registros recuperados:{}", userSecurityService.getIdSession(), users.size());
		//List<InquestQuestion> listQuestions = inquestQuestionRepository.findByInquestOrderByOrderquestion(inquest);
		String[] headersName = new String[users.size()+4];
		Workbook workbook = new HSSFWorkbook();
		Sheet sheet = workbook.createSheet("Becarios");
		Row row = sheet.createRow(0);
		Cell cell = row.createCell(0);
		cell.setCellValue("Matrícula");

		cell = row.createCell(1);
		cell.setCellValue("Alumno");

		cell = row.createCell(2);
		cell.setCellValue("P.E.");

		cell = row.createCell(3);
		cell.setCellValue("Beca");

		int j =4;

		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		int initRow = 1;
		if(grant.getId() != 7) {
			for (User user : users) {
				row = sheet.createRow(initRow);
				row.createCell(0).setCellValue(user.getUsername());
				row.createCell(1).setCellValue((user.getStudent() != null) ? user.getStudent().getName() + " " + user.getStudent().getLastname() + " " + user.getStudent().getSecondLastname() : "");
				row.createCell(2).setCellValue(user.getStudent().getProgram().getStudentPlanName());
				row.createCell(3).setCellValue(user.getGrant().getGrant());
				initRow++;
			}
		} else {
			LOGGER.info("{} - La beca es de adulto mayor", userSecurityService.getIdSession());
			for (User user : users) {
				row = sheet.createRow(initRow);
				row.createCell(0).setCellValue(user.getNotStudent().getIdNotStudent());
				row.createCell(1).setCellValue((user.getNotStudent() != null) ? user.getNotStudent().getName() + " " + user.getNotStudent().getLastname() + " " + user.getNotStudent().getSecondLastname() : "");
				row.createCell(2).setCellValue("");
				row.createCell(3).setCellValue(user.getGrant().getGrant());
				initRow++;
			}
		}
		byte[] bytes = null;
		try {
			workbook.write(stream);
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			try {
				workbook.write(bos);
			} finally {
				bos.close();
				workbook.close();
				stream.close();
			}
			bytes = bos.toByteArray();
		} catch (IOException e) {
			LOGGER.error("Error al escribir reporte de encuesta:{}", e.getMessage());
		}
		LOGGER.info("{} - Termina exportBecarios, encuesta:{}", userSecurityService.getIdSession(), grant.getGrant());
		return new ByteArrayResource(bytes, "Reporte de encuesta "+grant.getGrant());
	}*/
}