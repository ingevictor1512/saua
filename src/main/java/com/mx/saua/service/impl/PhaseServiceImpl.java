package com.mx.saua.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import com.mx.saua.dto.PhaseDto;
import com.mx.saua.entity.Phase;
import com.mx.saua.repository.PhaseRepository;
import com.mx.saua.service.PhaseService;

@Service("phaseService")
public class PhaseServiceImpl implements PhaseService{
	/*
	 * @Vic
	 */
	@Autowired
	private PhaseRepository phaseRepository;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(PhaseServiceImpl.class);

	@Override
	public List<PhaseDto> find() {
		List<PhaseDto> phaseDtos = new ArrayList<>();
		List<Phase> phases = phaseRepository.findAll();
		phases.stream().forEach(p->{
			phaseDtos.add(new PhaseDto(p.getId(), p.getDescription(), p.getAbbreviation()));
		});
		LOGGER.info("Busqueda de fases realizada correctamente");
		return phaseDtos;
	}

	@Override
	public PhaseDto getById(Long id) {
		Phase phase = phaseRepository.getOne(id);
		PhaseDto phaseDto = new PhaseDto();
		BeanUtils.copyProperties(phase, phaseDto);
		LOGGER.info("Busqueda por fases realizada correctamente");
		return phaseDto;
	}

	@Override
	public void save(PhaseDto PhaseDto) {
		if(PhaseDto.getId() > -1L) {
			/* Update */
			Phase phaseUpdate = new Phase();
			BeanUtils.copyProperties(PhaseDto, phaseUpdate);
			phaseRepository.save(phaseUpdate);
			LOGGER.info("Actualizacion de la fase realizada correctamente");
		} else {
			/* New register */
			Phase phaseInsert = new Phase();
			BeanUtils.copyProperties(PhaseDto, phaseInsert);
			phaseRepository.save(phaseInsert);
			LOGGER.info("Insercion de fase realizada correctamente");
		}
		
	}

	@Override
	public void delete(Long id) {
		try {
			phaseRepository.deleteById(id);
			LOGGER.info("Eliminacion de la fase realizada correctamente");
		} catch (DataIntegrityViolationException dve) {
			dve.printStackTrace();
		}
		
	}
	/* GETTERS Y SETTERS */
	public PhaseRepository getPhaseRepository() {
		return phaseRepository;
	}

	public void setPhaseRepository(PhaseRepository phaseRepository) {
		this.phaseRepository = phaseRepository;
	}
	
	

}
