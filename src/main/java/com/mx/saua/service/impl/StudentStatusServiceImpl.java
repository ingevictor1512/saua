package com.mx.saua.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import com.mx.saua.dto.StudentStatusDto;
import com.mx.saua.entity.StudentStatus;
import com.mx.saua.repository.StudentStatusRepository;
import com.mx.saua.service.StudentStatusService;

@Service("studentStatusService")
public class StudentStatusServiceImpl implements StudentStatusService{	
	/*
	 * @Vic
	 */
	@Autowired
	private StudentStatusRepository studentStatusRepository;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(StudentStatusServiceImpl.class);

	@Override
	public List<StudentStatusDto> find() {
		List<StudentStatusDto> studentStatusDtos = new ArrayList<>();
		List<StudentStatus> studentStatus = studentStatusRepository.findAll();
		studentStatus.stream().forEach(p->{
			studentStatusDtos.add(new StudentStatusDto(p.getId(), p.getDescription(), p.getComments()));
		});
		LOGGER.info("Busqueda del estatus del alumno realizada correctamente:{}", studentStatusDtos.toString());
		return studentStatusDtos;
	}

	@Override
	public StudentStatusDto getById(Long id) {
		StudentStatus studenStatus = studentStatusRepository.getOne(id);
		StudentStatusDto statusDto = new StudentStatusDto();
		BeanUtils.copyProperties(studenStatus, statusDto);
		LOGGER.info("Busqueda del estatus del alumno realizada correctamente");
		return statusDto;
	}

	@Override
	public void save(StudentStatusDto studentStatusDto) {
		if(studentStatusDto.getId() > -1L) {
			/* Update */
			StudentStatus studenStatusUpdate = new StudentStatus();
			BeanUtils.copyProperties(studentStatusDto, studenStatusUpdate);
			studentStatusRepository.save(studenStatusUpdate);
			LOGGER.info("Actualizacion del estatus del alumno realizada correctamente");
		} else {
			/* New register */
			StudentStatus studenStatusInsert = new StudentStatus();
			BeanUtils.copyProperties(studentStatusDto, studenStatusInsert);
			studentStatusRepository.save(studenStatusInsert);
			LOGGER.info("Insercion del estatus del alumno realizada correctamente");
		}
		
	}

	@Override
	public void delete(Long id) {
		try {
			studentStatusRepository.deleteById(id);
			LOGGER.info("Eliminacion del estatus del alumno realizada correctamente");
		} catch (DataIntegrityViolationException dve) {
			dve.printStackTrace();
		}
		
	}
	/* GETTERS Y SETTERS */
	public StudentStatusRepository getStudentStatusRepository() {
		return studentStatusRepository;
	}

	public void setStudentStatusRepository(StudentStatusRepository studentStatusRepository) {
		this.studentStatusRepository = studentStatusRepository;
	}


	
}
