package com.mx.saua.service.impl;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import com.mx.saua.converter.StudentConverter;
import com.mx.saua.converter.TeacherConverter;
import com.mx.saua.dto.LoadAuthorizationDto;
import com.mx.saua.dto.StudentDto;
import com.mx.saua.dto.TeacherDto;
import com.mx.saua.dto.TutoringLowDto;
import com.mx.saua.dto.TutorshipDto;
import com.mx.saua.dto.UserDto;
import com.mx.saua.entity.Configuration;
import com.mx.saua.entity.LoadAuthorization;
import com.mx.saua.entity.Period;
import com.mx.saua.entity.Student;
import com.mx.saua.entity.StudentCharge;
import com.mx.saua.entity.StudentStatus;
import com.mx.saua.entity.Teacher;
import com.mx.saua.entity.TutoringLow;
import com.mx.saua.entity.Tutorship;
import com.mx.saua.entity.User;
import com.mx.saua.entity.UserNotification;
import com.mx.saua.enums.AuthorityEnum;
import com.mx.saua.enums.ConfigurationEnum;
import com.mx.saua.enums.NotificationStatusEnum;
import com.mx.saua.enums.NotificationTypeEnum;
import com.mx.saua.exception.ServiceException;
import com.mx.saua.repository.ConfigurationRepository;
import com.mx.saua.repository.LoadAuthorizationRepository;
import com.mx.saua.repository.PeriodRepository;
import com.mx.saua.repository.StudentChargeRepository;
import com.mx.saua.repository.StudentRepository;
import com.mx.saua.repository.StudentStatusRepository;
import com.mx.saua.repository.TeacherRepository;
import com.mx.saua.repository.TutoringLowRepository;
import com.mx.saua.repository.TutorshipRepository;
import com.mx.saua.repository.UserNotificationRepository;
import com.mx.saua.repository.UserRepository;
import com.mx.saua.service.TutorshipService;
import com.mx.saua.service.UserService;

/**
 * Servicio que maneja la logica de negocio para las tutorias
 * 
 * @author Vic
 * @version 1.0
 *
 */
@Service("tutorshipService")
public class TutorshipServiceImpl implements TutorshipService {

	private static final Logger LOGGER = LoggerFactory.getLogger(TutorshipServiceImpl.class);

	/* Inyeccion de repositorios */
	@Autowired
	private TutorshipRepository tutorshipRepository;
	@Autowired
	private TutoringLowRepository tutoringLowRepository;
	@Autowired
	private StudentRepository studentRepository;
	@Autowired
	private TeacherRepository teacherRepository;
	@Autowired
	private LoadAuthorizationRepository loadAuthorizationRepository;
	@Autowired
	private PeriodRepository periodRepository;
	@Autowired
	private UserRepository userRepository; 
	@Autowired
	private UserNotificationRepository userNotificationRepository;
	@Autowired
	private StudentStatusRepository studentStatusRepository;
	@Autowired
	private StudentChargeRepository studentChargeRepository;

	/* Inyeccion de servicios */
	@Autowired
	private UserSecurityService userSecurityService;
	@Autowired
	private StudentConverter studentConverter;
	@Autowired
	private TeacherConverter teacherConverter;
	@Autowired
	private UserService userService;
	@Autowired
	private ConfigurationRepository configurationRepository;
	
	@Override
	public TutorshipDto find(TutorshipDto tutorshipDto) {
		LOGGER.info("{} - Inicia carga de tutores.", userSecurityService.getIdSession());
		List<Tutorship> tutorships = tutorshipRepository.findAll();
		List<TutorshipDto> tutorshipDtos = new ArrayList<>();
		tutorships.stream().forEach(p -> {
			TutorshipDto tutorshipDtoTemp = new TutorshipDto();
			BeanUtils.copyProperties(p, tutorshipDtoTemp);
			//LOGGER.info("{} - p.getStudent().getStudentStatus().getId()", p.getStudent().getStudentStatus().getId());
			tutorshipDtoTemp.setStudent(studentConverter.convertStudent2StudentDto(p.getStudent()));
			tutorshipDtoTemp.setTeacher(teacherConverter.convertTeacher2TeacherDto(p.getTeacher()));
			TutoringLowDto tutoringLowTemp = new TutoringLowDto();
			BeanUtils.copyProperties(p.getTutoringLow(), tutoringLowTemp);
			tutorshipDtoTemp.setTutoringLow(tutoringLowTemp);
			LoadAuthorization authorization= loadAuthorizationRepository.findByStudentAndPeriod(tutorshipDtoTemp.getStudent().getRegister(), true);
			LoadAuthorizationDto authorizationDto = new LoadAuthorizationDto();
			if(authorization != null){
				BeanUtils.copyProperties(authorization,authorizationDto);
			} else {
				authorizationDto = null;
			}

			tutorshipDtoTemp.setLoadAuthorizationDto(authorizationDto);
			// tutorshipDtoTemp.setRegister(tutorshipDtoTemp.getStudent().getRegister());
			tutorshipDtos.add(tutorshipDtoTemp);
		});
		tutorshipDto.setTutorshipsDto(tutorshipDtos);
		LOGGER.info("{} - Termina carga de tutores.", userSecurityService.getIdSession());
		return tutorshipDto;
	}

	@Override
	public void save(TutorshipDto tutorshipDto) throws ServiceException {
		Date current = new Date();
		if (tutorshipDto.getId() > -1L) {
			LOGGER.info("{} - Inicia actualizacion de tutor.", userSecurityService.getIdSession());
			/* Update */
			tutorshipDto.setEditdate(current);
			Tutorship tutorshipUpdate = new Tutorship();
			BeanUtils.copyProperties(tutorshipDto, tutorshipUpdate);
			/* asignando el alumno */
			Student student = new Student();
			student = studentRepository.getOne(tutorshipDto.getRegister());
			tutorshipUpdate.setStudent(student);
			/* obteniendo los datos del tutor */
			Teacher teacher = new Teacher();
			teacher = teacherRepository.getOneByRegister(tutorshipDto.getTeacher().getRegister());
			tutorshipUpdate.setTeacher(teacher);
			/* obteniendo el estatus del alumno */
			TutoringLow tutoringLow = new TutoringLow();
			tutoringLow = tutoringLowRepository.getOne(tutorshipDto.getTutoringLow().getId());
			tutorshipUpdate.setTutoringLow(tutoringLow);
			UserDetails userDetails = userSecurityService.getUserCurrentDetails();
			tutorshipUpdate.setAssignedTutor(userDetails.getUsername());
			tutorshipRepository.save(tutorshipUpdate);
			LOGGER.info("{} - Termina actualizacion de tutor.", userSecurityService.getIdSession());
		} else {
			/* New register */
			/* Validamos si ya existe un registro */
			LOGGER.info("{} - Inicia guardado de nuevo tutor.", userSecurityService.getIdSession());
			List<Tutorship> xTutorship = tutorshipRepository.findByStudentRegister(tutorshipDto.getRegister());
			if (xTutorship == null || xTutorship.size() == 0 ) {
				LOGGER.info("Se insertara un registro");
				tutorshipDto.setEditdate(current);
				tutorshipDto.setRegisterDate(current);
				Tutorship tutorshipInsert = new Tutorship();
				BeanUtils.copyProperties(tutorshipDto, tutorshipInsert);
				/* asignando el alumno */
				Student student = new Student();
				student = studentRepository.getOne(tutorshipDto.getRegister());
				// Recuperado:{}",student.toString());
				tutorshipInsert.setStudent(student);
				/* obteniendo los datos del tutor */
				Teacher teacher = new Teacher();
				teacher = teacherRepository.getOneByRegister(tutorshipDto.getTeacher().getRegister());
				tutorshipInsert.setTeacher(teacher);
				// Recuperado:{}",teacher.toString());
				/* obteniendo el estatus del alumno */
				TutoringLow tutoringLow = new TutoringLow();
				tutoringLow = tutoringLowRepository.getOne(tutorshipDto.getTutoringLow().getId());
				tutorshipInsert.setTutoringLow(tutoringLow);
				// Recuperado:{}",tutoringLow.toString());
				UserDetails userDetails = userSecurityService.getUserCurrentDetails();
				tutorshipInsert.setAssignedTutor(userDetails.getUsername());
				tutorshipRepository.save(tutorshipInsert);
				LOGGER.info("{} - Termina guardado de nuevo tutor.", userSecurityService.getIdSession());
			}

		}

	}

	@Override
	public void delete(TutorshipDto tutorshipDto) throws ServiceException {
		LOGGER.info("{} - Inicia guardado eliminar tutor.", userSecurityService.getIdSession());
		Tutorship turTutorship = tutorshipRepository.getOne(tutorshipDto.getId());
		try {
			tutorshipRepository.delete(turTutorship);
			LOGGER.info("{} - termina guardado eliminar tutor.", userSecurityService.getIdSession());
		} catch (Exception ex) {
			throw new ServiceException("No puede eliminar esta tutoria, ya que se encuentra ligada a otros registros.");
		}

	}

	@Override
	public TutorshipDto prepareDto(TutorshipDto tutorshipDto) {
		LOGGER.info("{} - Inicia prepareDto de tutor.", userSecurityService.getIdSession());

		List<TutoringLow> tutoringLows = tutoringLowRepository.findAll();
		List<TutoringLowDto> tutoringLowDtos = new ArrayList<>();

		List<Student> students = studentRepository.findAll();
		List<StudentDto> studentDtos = new ArrayList<>();

		List<Teacher> teachers = teacherRepository.findAllOrderByNameLastnameSecondLastnameAsc();
		List<TeacherDto> teachersDtos = new ArrayList<>();

		tutoringLows.stream().forEach(p -> {
			TutoringLowDto tutoringLowDto = new TutoringLowDto();
			BeanUtils.copyProperties(p, tutoringLowDto);
			tutoringLowDtos.add(tutoringLowDto);
		});

		students.stream().forEach(p -> {
			StudentDto studentDto = new StudentDto();
			BeanUtils.copyProperties(p, studentDto);
			tutorshipDto.setRegister(studentDto.getRegister());
			studentDtos.add(studentDto);
		});

		teachers.stream().forEach(p -> {
			TeacherDto teacherDto = new TeacherDto();
			BeanUtils.copyProperties(p, teacherDto);
			teachersDtos.add(teacherDto);
		});
		
		/* Matricula para alumno */
		if (tutorshipDto.getStudent() == null) {
			LOGGER.info("{} El alumno es igual a null.", userSecurityService.getIdSession());
			tutorshipDto.setRegister("");
		} else {
			LOGGER.info("{} El alumno es diferente a null.", userSecurityService.getIdSession());
			tutorshipDto.setRegister(tutorshipDto.getStudent().getRegister());
		}
		// tutorshipDto.setStudent(studentRegister);
		tutorshipDto.setTeachersDto(teachersDtos);
		tutorshipDto.setStudentsDto(studentDtos);
		tutorshipDto.setTutoringLowsDto(tutoringLowDtos);
		LOGGER.info("{} - Termina prepareDto de tutor.", userSecurityService.getIdSession());
		return tutorshipDto;

	}

	@Override
	public TutorshipDto getTutorshipByKey(Long key) throws ServiceException {
		LOGGER.info("{} - Inicia getTutorshipByKey de tutor[key - {}].", userSecurityService.getIdSession(), key);
		TutorshipDto tutorshipDto = new TutorshipDto();
		Tutorship tutorship = tutorshipRepository.getOne(key);
		/* Obteniendo el student */
		StudentDto studentDto = new StudentDto();
		BeanUtils.copyProperties(tutorship.getStudent(), studentDto);
		/* Obteniendo el tipo de baja */
		TutoringLowDto tutoringLowDto = new TutoringLowDto();
		BeanUtils.copyProperties(tutorship.getTutoringLow(), tutoringLowDto);
		/* Obteniendo al Tutor */
		TeacherDto teacherDto = new TeacherDto();
		BeanUtils.copyProperties(tutorship.getTeacher(), teacherDto);
		BeanUtils.copyProperties(tutorship, tutorshipDto);
		tutorshipDto.setStudent(studentDto);
		tutorshipDto.setTutoringLow(tutoringLowDto);
		tutorshipDto.setTeacher(teacherDto);

		tutorshipDto.setRegister(tutorship.getStudent().getRegister());
		LOGGER.info("{} - Termina getTutorshipByKey de tutor[key - {}].", userSecurityService.getIdSession(), key);
		return tutorshipDto;
	}

	@Override
	public TutorshipDto getTutoredsByTeacher(TutorshipDto tutorshipDto) {
		LOGGER.info("{} - Inicia getTutoredsByTeacher de tutor.", userSecurityService.getIdSession());
		UserDetails userDetails = userSecurityService.getUserCurrentDetails();
		List<Tutorship> tutoredsList = tutorshipRepository.findByTeacherAndStatus(userDetails.getUsername(), 7L);
		List<TutorshipDto> tutorshipDtos = new ArrayList<>();
		tutoredsList.stream().forEach(p->{
			TutorshipDto tutorshipDtoTemp = new TutorshipDto();
			BeanUtils.copyProperties(p, tutorshipDtoTemp);
			tutorshipDtoTemp.setStudent(studentConverter.convertStudent2StudentDto(p.getStudent()));
			tutorshipDtoTemp.setTeacher(teacherConverter.convertTeacher2TeacherDto(p.getTeacher()));
			TutoringLowDto tutoringLowTemp = new TutoringLowDto();
			BeanUtils.copyProperties(p.getTutoringLow(), tutoringLowTemp);
			tutorshipDtoTemp.setTutoringLow(tutoringLowTemp);
			/* Obteniendo el estatus de la carga academica del alumno */
			LoadAuthorization loadAuthorization = loadAuthorizationRepository
					.findByStudentAndPeriod(p.getStudent().getRegister(), true);
			LoadAuthorizationDto loadAuthorizationDto = new LoadAuthorizationDto();
			if (loadAuthorization != null) {
				BeanUtils.copyProperties(loadAuthorization, loadAuthorizationDto);
				tutorshipDtoTemp.setLoadAuthorizationDto(loadAuthorizationDto);
			} else {
				tutorshipDtoTemp.setLoadAuthorizationDto(null);
			}
			// tutorshipDtoTemp.setRegister(tutorshipDtoTemp.getStudent().getRegister());
			tutorshipDtos.add(tutorshipDtoTemp);
		});
		tutorshipDto.setTutorshipsDto(tutorshipDtos);
		/* Establecemos si aun esta en periodo de autorizaciones */
		Configuration autorizationInit = configurationRepository.findByKey(ConfigurationEnum.AUTHORIZATION_SUBJECTS_INIT.toString());
		Configuration autorizationEnd = configurationRepository.findByKey(ConfigurationEnum.AUTHORIZATION_SUBJECTS_END.toString());
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy kk:mm");
		try {
			//Calendar initialCalendar = Calendar.getInstance();
			Date initialDate = sdf.parse(autorizationInit.getValue());
			//initialCalendar.setTime(initialDate);
			//initialCalendar.add(Calendar.DATE, Integer.valueOf(registryDay.getValue()));
			
			//Calendar endCalendar = Calendar.getInstance();
			Date endDate = sdf.parse(autorizationEnd.getValue());
			//endCalendar.setTime(endDate);
			//endCalendar.add(Calendar.DATE, Integer.valueOf(registryDay.getValue()));		
			Date now = new Date();
			
			if (now.before(endDate) && now.after(initialDate)) {
				tutorshipDto.setPeriod(true);
			}
			
		} catch (ParseException e) {
			LOGGER.error(e.getMessage());
			tutorshipDto.setPeriod(true);
		}
		LOGGER.info("{} - Termina getTutoredsByTeacher de tutor.", userSecurityService.getIdSession());
		return tutorshipDto;
	}

	@Override
	public void saveTutored(TutorshipDto tutorshipDto) throws ServiceException {
		LOGGER.info("{} - Inicia guardado de tutorados.", userSecurityService.getIdSession());
		Date current = new Date();
		/* Update */
		LOGGER.info("Se actualizara el registro");
		Tutorship tutorshipUpdate = new Tutorship();
		tutorshipUpdate = tutorshipRepository.getOne(tutorshipDto.getId());

		/* obteniendo el estatus del alumno */
		TutoringLow tutoringLow = new TutoringLow();
		tutoringLow = tutoringLowRepository.getOne(tutorshipDto.getTutoringLow().getId());
		tutorshipUpdate.setTutoringLow(tutoringLow);
		tutorshipUpdate.setObservations(tutorshipDto.getObservations());
		tutorshipUpdate.setEditdate(current);
		LOGGER.info("{} - Termina guardado de tutorados.", userSecurityService.getIdSession());
		tutorshipRepository.save(tutorshipUpdate);

	}

	@Override
	public void loadAuthorization(String idRegister) throws ServiceException {
		LOGGER.info("{} - Inicia carga de autorizacion[{}].", userSecurityService.getIdSession(), idRegister);
		Date current = new Date();
		LoadAuthorization searchStudent = loadAuthorizationRepository.findByStudentAndPeriod(idRegister, true);
		if (searchStudent == null) {
			LoadAuthorization loadAuthorizationInsert = new LoadAuthorization();
			loadAuthorizationInsert.setAuthorization(true);
			loadAuthorizationInsert.setAuthorizationKey("Autorizado");
			loadAuthorizationInsert.setEditdate(current);
			loadAuthorizationInsert.setObservations("");
			loadAuthorizationInsert.setRegisterDate(current);
			Period period = periodRepository.getOneByActivePeriod(true);
			Student student = studentRepository.getOne(idRegister);
			loadAuthorizationInsert.setStudent(student); // Obtener el estudiante
			loadAuthorizationInsert.setPeriod(period);
			; // Obtener el periodo
			loadAuthorizationRepository.save(loadAuthorizationInsert);
			/* Mandamos correo al alumno de que su carga fue autorizada */

			UserDto studentDtoMail = userService.findByUsername(idRegister);
			User userForNotification = userRepository.findOneByEmailAndUsername(studentDtoMail.getEmail(),
					studentDtoMail.getUsername());
			// Guardar notificacion

			UserNotification userNotification = new UserNotification();
			userNotification.setAddresseeMail(studentDtoMail.getEmail());
			userNotification.setAddresseeName(studentDtoMail.getStudentDto().getName());
			userNotification.setUser(userForNotification);
			userNotification.setPlainPassword("");
			userNotification.setReviewable(false);
			userNotification.setStatus(NotificationStatusEnum.REGISTRADA);
			userNotification.setType(NotificationTypeEnum.AUTHORIZED_L);
			userNotificationRepository.save(userNotification);
			LOGGER.info("{} - Termina carga de autorizacion[{}].", userSecurityService.getIdSession(), idRegister);
		}

	}

	@Override
	public void saveTutortoGroup(TutorshipDto tutorshipDto) throws ServiceException {
		LOGGER.info("{} - Inicia saveTutortoGroup.", userSecurityService.getIdSession());
		Date current = new Date();
		TutoringLow tutoringLow =tutoringLowRepository.findByDescription("Activo");
		UserDetails userDetails = userSecurityService.getUserCurrentDetails();
		/* obtenemos el tutor a insertar */
		Teacher teacher = new Teacher();
		teacher = teacherRepository.getOneByRegister(String.valueOf(tutorshipDto.getIdTeacher()));
		/* Obtener el estatus de registrado o no registrado */
		StudentStatus status= studentStatusRepository.getOneByDescription("REGISTRADO");
		/* Crear el metodo para obtener los alumnos por grupo y estatus alumno */
		List<Student> students = studentRepository.findStudentsByGroupAndStatus(tutorshipDto.getIdGroup(), status.getId());
		if(students.size() > 0) {
			/* Verificando si el alumno ya tiene tutor, si ya tiene tutor dejarle el que tiene o actualizarlo */
			for(Student studentTemp : students) {
				List<Tutorship> tutorship = tutorshipRepository.findByStudentRegister(studentTemp.getRegister());
				LOGGER.info("{} - Tamanio de tutorship {}", userSecurityService.getIdSession(),tutorship.size());
				if(tutorship.size() == 0) {
					/* Insertamos el alumno ya que no existe en la tabla tutorias */
					Tutorship tutorshipInsert = new Tutorship();
					tutorshipInsert.setAssignedTutor(userDetails.getUsername());
					tutorshipInsert.setTutoringLow(tutoringLow);
					tutorshipInsert.setTeacher(teacher);
					tutorshipInsert.setStudent(studentTemp);
					tutorshipInsert.setObservations("");
					tutorshipInsert.setRegisterDate(current);
					tutorshipInsert.setEditdate(current);
					tutorshipRepository.save(tutorshipInsert);
				} else {
					/* Actualizamos el tutor del alumno que ya tiene */
					tutorship.get(0).setEditdate(current);
					tutorship.get(0).setAssignedTutor(userDetails.getUsername());
					tutorship.get(0).setTeacher(teacher);
					tutorship.get(0).setTutoringLow(tutoringLow);
					tutorshipRepository.save(tutorship.get(0));				
				}
				LOGGER.info("{} - Termina saveTutortoGroup.", userSecurityService.getIdSession());
			}
			
		} else {
			throw new ServiceException("El grupo tiene "+students.size()+" alumnos registrados");
		}
		
		
	}

	@Override
	public ByteArrayInputStream exportAllTutorships() throws IOException {
		LOGGER.info("{} - Inicia exportAllTutorships.", userSecurityService.getIdSession());
		String [] columns = {"ID", "MATRICULA", "NOMBRE ALUMNO","ESTATUS ALUMNO", "ESTATUS CARGA ACADEMICA","ENCUESTA CONTESTADA","TUTOR", "P.E.", "GRUPO"};
		
		Workbook workbook = new HSSFWorkbook();
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		/* Colocamos en negrita */
		CellStyle headerStyle = workbook.createCellStyle();
        Font font = workbook.createFont();
        font.setBold(true);
        headerStyle.setFont(font);
        
		Sheet sheet = workbook.createSheet("Tutorias");
		Row row = sheet.createRow(0);
		
		for(int i = 0; i < columns.length; i++) {
			Cell cell = row.createCell(i);
			cell.setCellValue(columns[i]);
			cell.setCellStyle(headerStyle);
		}
		
		//List<Tutorship> tutorias = tutorshipRepository.findAll();
		List<Student> list = studentRepository.findAll();
		int initRow = 1;
		for (Student student : list) {
			List<Tutorship> tutorias = tutorshipRepository.findAllByStudent(student);
			LoadAuthorization loadAuthorization = loadAuthorizationRepository.findByStudentAndPeriod(student.getRegister(), true);
			User userTest = userRepository.findByUsername(student.getRegister()).orElse(null);
			row = sheet.createRow(initRow);
			row.createCell(0).setCellValue(initRow);
			row.createCell(1).setCellValue(student.getRegister());
			row.createCell(2).setCellValue(student.getName()+" "+student.getLastname()+" "+student.getSecondLastname());
			row.createCell(3).setCellValue((tutorias.size()>0)?tutorias.get(0).getTutoringLow().getDescription():"");
			row.createCell(4).setCellValue((loadAuthorization != null) ? loadAuthorization.getAuthorizationKey():""); //Se coloca si tiene carga autorizada o no
			row.createCell(5).setCellValue((userTest.isInquest() == false) ? "SI":"NO"); //Se coloca si el alumno contesto encuesta o no
			row.createCell(6).setCellValue((tutorias.size()>0)?tutorias.get(0).getTeacher().getName()+" "+tutorias.get(0).getTeacher().getLastname()+" "+tutorias.get(0).getTeacher().getSecondLastname():"");
			row.createCell(7).setCellValue(student.getProgram().getStudentPlanName());
			row.createCell(8).setCellValue(student.getGroup().getName()+" - "+student.getGroup().getTurn());
			initRow++;
		}
		
		
		/*int initRow = 1;
		for(Tutorship tutoria : tutorias) {
			row = sheet.createRow(initRow);
			row.createCell(0).setCellValue(tutoria.getId());
			row.createCell(1).setCellValue(tutoria.getStudent().getRegister());
			row.createCell(2).setCellValue(tutoria.getStudent().getName()+" "+tutoria.getStudent().getLastname()+" "+tutoria.getStudent().getSecondLastname());
			row.createCell(3).setCellValue(tutoria.getTutoringLow().getDescription());
			row.createCell(4).setCellValue(tutoria.getTeacher().getName()+" "+tutoria.getTeacher().getLastname()+" "+tutoria.getTeacher().getSecondLastname());
			initRow++;
		}*/
		
		workbook.write(stream);
		workbook.close();
		LOGGER.info("{} - Termina exportAllTutorships.", userSecurityService.getIdSession());
		return new ByteArrayInputStream(stream.toByteArray());
	}

	@Override
	public void saveStudentsToTutor(TutorshipDto tutorshipDto) throws ServiceException {
		LOGGER.info("{} - Inicia asignar varios alumnos a un tutor.",  userSecurityService.getIdSession());
		Date current = new Date();
		TutoringLow tutoringLow =tutoringLowRepository.findByDescription("Activo");
		UserDetails userDetails = userSecurityService.getUserCurrentDetails();
		/* obtenemos el tutor a insertar */
		Teacher teacher = new Teacher();
		teacher = teacherRepository.getOneByRegister(String.valueOf(tutorshipDto.getIdTeacher()));
		LOGGER.info("{} - El tutor es {}",  userSecurityService.getIdSession(),teacher.getName()+" "+teacher.getLastname()+" "+teacher.getSecondLastname());
		/*Separando las matriculas recibidas*/
		String[] parts = tutorshipDto.getRegister().split(",");		
		if(parts[0].equals("")) {
			throw new ServiceException("Favor de capturar las Matriculas de los alumnos");
		} else {
			/*Revisamos si el alumno ya existe en la tabla de tutorias */
			for(String x: parts) {
				List<Tutorship> t = tutorshipRepository.findByStudentRegister(x);
				LOGGER.info("{} - El tamaño recuperado de t es: {}",  userSecurityService.getIdSession(),t.size());
				
				
				if(t.size() == 0) {
					LOGGER.info("{} - El alumno no existe, se insertara",  userSecurityService.getIdSession());
					Student students = studentRepository.getOne(x);
					Tutorship insertTutorship = new Tutorship();
					/* Se insertara el alumno con el tutor recibido desde el rest */
					insertTutorship.setAssignedTutor(userDetails.getUsername());
					insertTutorship.setEditdate(current);
					insertTutorship.setRegisterDate(current);
					insertTutorship.setTeacher(teacher);
					insertTutorship.setStudent(students);
					insertTutorship.setObservations("");
					insertTutorship.setTutoringLow(tutoringLow);
					tutorshipRepository.save(insertTutorship);
					LOGGER.info("{} - Termina asignar varios alumnos a un tutor.",  userSecurityService.getIdSession());
				} else {
					LOGGER.info("{} - El alumno ya existe, se actualizara su tutor",  userSecurityService.getIdSession());
					t.get(0).setTeacher(teacher);
					tutorshipRepository.save(t.get(0));
				}
			}
		}
	}

	@Override
	public ByteArrayInputStream exportTutoredsByTutors(String ID) throws IOException {
		LOGGER.info("{} - TutorshipServiceImpl - Recibiendo como parametro: {}", userSecurityService.getIdSession(),ID);
		String [] columns = {"ID", "MATRICULA", "NOMBRE ALUMNO","ESTATUS ALUMNO", "P.E. ALUMNO","TUTOR"};
		
		Workbook workbook = new HSSFWorkbook();
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		
		Sheet sheet = workbook.createSheet("Tutorias");
		Row row = sheet.createRow(0);
		
		for(int i = 0; i < columns.length; i++) {
			Cell cell = row.createCell(i);
			cell.setCellValue(columns[i]);
		}
		
		List<Tutorship> tutorias = tutorshipRepository.findByTeacher(ID);
		int initRow = 1;
		for(Tutorship tutoria : tutorias) {
			row = sheet.createRow(initRow);
			row.createCell(0).setCellValue(tutoria.getId());
			row.createCell(1).setCellValue(tutoria.getStudent().getRegister());
			row.createCell(2).setCellValue(tutoria.getStudent().getName()+" "+tutoria.getStudent().getLastname()+" "+tutoria.getStudent().getSecondLastname());
			row.createCell(3).setCellValue(tutoria.getTutoringLow().getDescription());
			row.createCell(4).setCellValue(tutoria.getStudent().getProgram().getStudentPlanName());
			row.createCell(5).setCellValue(tutoria.getTeacher().getName()+" "+tutoria.getTeacher().getLastname()+" "+tutoria.getTeacher().getSecondLastname());
			initRow++;
		}
		
		workbook.write(stream);
		workbook.close();
		LOGGER.info("{} - TutorshipServiceImpl - Generacion del excel exitosa", userSecurityService.getIdSession());
		return new ByteArrayInputStream(stream.toByteArray());

	}

	@Override
	public void deleteloadAuthorization(long id) throws ServiceException {
		LOGGER.info("{} - TutorshipServiceImpl - Ingresando a eliminar la autorizacion de la carga academica",  userSecurityService.getIdSession());
		loadAuthorizationRepository.deleteById(id);
		
	}

	@Override
	public List<TutorshipDto> findAll() throws ServiceException {
		TutoringLow idTutoringLow= tutoringLowRepository.findByDescription("Activo");
		List<Tutorship> tutorships = tutorshipRepository.findByTeacherDistinct();
		List<TutorshipDto> tutorshipDtos = new ArrayList<>();
		for(Tutorship TutorshipTemp : tutorships) {
			List<Tutorship> tutorship = tutorshipRepository.findByTeacherAndStatus(TutorshipTemp.getTeacher().getRegister(), idTutoringLow.getId());
			TutorshipDto tutorshipDtoTemp = new TutorshipDto();
			BeanUtils.copyProperties(TutorshipTemp, tutorshipDtoTemp);
			tutorshipDtoTemp.setTeacher(teacherConverter.convertTeacher2TeacherDto(TutorshipTemp.getTeacher()));
			tutorshipDtoTemp.setTutoredActives(tutorship.size());
			tutorshipDtos.add(tutorshipDtoTemp);
		}
		/*List<Tutorship> tutorships = tutorshipRepository.findAllGroupbyTutoriateacher();
		List<TutorshipDto> tutorshipDtos = new ArrayList<>();
		
		for (Tutorship ts : tutorships) {
			TutorshipDto tutorshipDtoTemp = new TutorshipDto();
			BeanUtils.copyProperties(ts, tutorshipDtoTemp);
			tutorshipDtoTemp.setTeacher(teacherConverter.convertTeacher2TeacherDto(ts.getTeacher()));
			tutorshipDtos.add(tutorshipDtoTemp);
		}*/
		
		return tutorshipDtos;
	}
	@Override	
	public TutorshipDto findByStudent(StudentDto studentDto) {
		Student student = studentRepository.getOne(studentDto.getRegister());
		Tutorship tut = tutorshipRepository.findByStudent(student);
		if(tut!=null) {
			return new TutorshipDto(tut.getId(),teacherConverter.convertTeacher2TeacherDto(tut.getTeacher()));
		}else {
			TeacherDto dto = new TeacherDto();
			dto.setName("");
			dto.setLastname("");
			dto.setSecondLastname("");
			return new TutorshipDto(1L,dto);
		}
	}
	
	@Override
	public List<TeacherDto> findAllTutors() throws ServiceException {
		List<TeacherDto> array= userService.findAllByRole(AuthorityEnum.TUTOR.getValue());

		return array;
	}

	@Override
	public ByteArrayInputStream exportTutoredsByIdTutor(Long ID) throws IOException {
		LOGGER.info("{} - TutorshipServiceImpl - Recibiendo como parametro ID de profesor: {}", userSecurityService.getIdSession(),ID);
		String [] columns = {"MATRICULA", "NOMBRE ALUMNO","ESTATUS ALUMNO", "P.E. ALUMNO","TUTOR"};
		
		Workbook workbook = new HSSFWorkbook();
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		
		Sheet sheet = workbook.createSheet("Tutorados");
		Row row = sheet.createRow(0);
		
		for(int i = 0; i < columns.length; i++) {
			Cell cell = row.createCell(i);
			cell.setCellValue(columns[i]);
		}
		/* Obtener el profesor con el ID recibido desde la tabla profesor	 */
		Teacher teacher = teacherRepository.getOne(ID);
		List<Tutorship> tutorias = tutorshipRepository.findByTeacher(teacher.getRegister());
		int initRow = 1;
		for(Tutorship tutoria : tutorias) {
			row = sheet.createRow(initRow);
			row.createCell(0).setCellValue(tutoria.getStudent().getRegister());
			row.createCell(1).setCellValue(tutoria.getStudent().getName()+" "+tutoria.getStudent().getLastname()+" "+tutoria.getStudent().getSecondLastname());
			row.createCell(2).setCellValue(tutoria.getTutoringLow().getDescription());
			row.createCell(3).setCellValue(tutoria.getStudent().getProgram().getStudentPlanName());
			row.createCell(4).setCellValue(tutoria.getTeacher().getName()+" "+tutoria.getTeacher().getLastname()+" "+tutoria.getTeacher().getSecondLastname());
			initRow++;
		}
		
		workbook.write(stream);
		workbook.close();
		LOGGER.info("{} - TutorshipServiceImpl - Generacion del listado en excel exitosa", userSecurityService.getIdSession());
		return new ByteArrayInputStream(stream.toByteArray());
	}

	@Override
	public ByteArrayInputStream exportStudentsWithUaps() throws IOException {
		LOGGER.info("{} - TutorshipServiceImpl - Iniciando reporte de exportStudentsWithUaps", userSecurityService.getIdSession());
		String [] columns = {"No. P","MATRICULA", "NOMBRE ALUMNO","PROG EDUCATIVO", "GRUPO", "NO UAPS"};
		
		Workbook workbook = new HSSFWorkbook();
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		
		Sheet sheet = workbook.createSheet("Alumnos con uaps");
		Row row = sheet.createRow(0);
		/* Colocamos en negrita */
		CellStyle headerStyle = workbook.createCellStyle();
        Font font = workbook.createFont();
        font.setBold(true);
        headerStyle.setFont(font);
		
		for(int i = 0; i < columns.length; i++) {
			Cell cell = row.createCell(i);
			cell.setCellValue(columns[i]);
			cell.setCellStyle(headerStyle);
		}
		/* Obtener el profesor con el ID recibido desde la tabla profesor	 */
		List<Student> list = studentRepository.findAll();
		int initRow = 1;
		for (Student student : list) {
			/* Obtenemos los registros que existen de ese alumno en la tabla carga alumno */
			List<StudentCharge> studentCharges = studentChargeRepository.findStudentChargeByStudentRegisterAndPeriod(student.getRegister(), 4);
			if(studentCharges.size() > 0) {
			row = sheet.createRow(initRow);
			row.createCell(0).setCellValue(initRow);
			row.createCell(1).setCellValue(student.getRegister());
			row.createCell(2).setCellValue(student.getName()+" "+student.getLastname()+" "+student.getSecondLastname());
			row.createCell(3).setCellValue(student.getProgram().getStudentPlanName());
			row.createCell(4).setCellValue(student.getGroup().getName()+" "+student.getGroup().getAbbreviation()); //
			row.createCell(5).setCellValue(studentCharges.size()); // Total de materias por alumno para valentin
			initRow++;
			} else {
				row = sheet.createRow(initRow);
				row.createCell(0).setCellValue(initRow);
				row.createCell(1).setCellValue(student.getRegister());
				row.createCell(2).setCellValue(student.getName()+" "+student.getLastname()+" "+student.getSecondLastname());
				row.createCell(3).setCellValue(student.getProgram().getStudentPlanName());
				row.createCell(4).setCellValue(student.getGroup().getName()+" "+student.getGroup().getAbbreviation()); //
				row.createCell(5).setCellValue(0); // Total de materiaspor alumno para valentin
				initRow++;
			}
		}
		
		workbook.write(stream);
		workbook.close();
		LOGGER.info("{} - TutorshipServiceImpl - Generacion del listado en excel exitosa", userSecurityService.getIdSession());
		return new ByteArrayInputStream(stream.toByteArray());
	}
}
