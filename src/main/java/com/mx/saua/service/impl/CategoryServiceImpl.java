package com.mx.saua.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import com.mx.saua.dto.CategoryDto;
import com.mx.saua.entity.Category;
import com.mx.saua.repository.CategoryRepository;
import com.mx.saua.service.CategoryService;

@Service("categoryService")
public class CategoryServiceImpl implements CategoryService{
	/*
	 * @Vic
	 */
	@Autowired
	private CategoryRepository categoryRepository;

	@Override
	public List<CategoryDto> find() {
		List<CategoryDto> categoryDtos = new ArrayList<>();
		List<Category> categories = categoryRepository.findAll();
		categories.stream().forEach(p->{
			categoryDtos.add(new CategoryDto(p.getId(), p.getDescription()));
		});
		return categoryDtos;
	}
	
	@Override
	public CategoryDto getById(Long id) {
		Category category = categoryRepository.getOne(id);
		CategoryDto categoryDto = new CategoryDto();
		BeanUtils.copyProperties(category, categoryDto);
		return categoryDto;
	}
	
	@Override
	public void save(CategoryDto categoryDto) {
		if(categoryDto.getId() > -1L) {
			/* Update */
			Category categoryUpdate = new Category();
			BeanUtils.copyProperties(categoryDto, categoryUpdate);
			categoryRepository.save(categoryUpdate);
		} else {
			/* New register */
			Category categoryInsert = new Category();
			BeanUtils.copyProperties(categoryDto, categoryInsert);
			categoryRepository.save(categoryInsert);
		}
		
	}

	@Override
	public void delete(Long id) {
		try {
			categoryRepository.deleteById(id);
		} catch (DataIntegrityViolationException dve) {
			dve.printStackTrace();
		}
		
	} 
	/* GETTERS Y SETTERS */

	public CategoryRepository getCategoryRepository() {
		return categoryRepository;
	}

	public void setCategoryRepository(CategoryRepository categoryRepository) {
		this.categoryRepository = categoryRepository;
	}
}
