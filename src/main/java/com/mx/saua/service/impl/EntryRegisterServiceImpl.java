package com.mx.saua.service.impl;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mx.saua.dto.EntryRegisterDto;
import com.mx.saua.entity.EntryRegister;
import com.mx.saua.exception.ServiceException;
import com.mx.saua.repository.EntryRegisterRepository;
import com.mx.saua.service.EntryRegisterService;

@Service("entryRegisterService")
public class EntryRegisterServiceImpl implements EntryRegisterService{

	@Autowired
	private EntryRegisterRepository entryRegisterRepository;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(EntryRegisterServiceImpl.class);
	
    @Override
    public void save(EntryRegisterDto entryRegisterDto) throws ServiceException {
        if(entryRegisterDto.getRegisterId() > -1L) {
			//Update
			//Cargando la beca completa

		} else {
			/* New register */
			Date current = new Date();
        	EntryRegister entry = new EntryRegister();
        	entry.setDatee(current);
        	entry.setDayEnum(null);
        	entry.setPeriod(null);
        	entry.setRegisterDate(null);
        	entry.setUserCashier(null);
        	entry.setUserStudent(null);
			LOGGER.info("Insercion de la beca realizada correctamente");
		}
        
    }

}
