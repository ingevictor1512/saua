package com.mx.saua.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.mx.saua.entity.Authority;
import com.mx.saua.entity.User;
import com.mx.saua.exception.ServiceException;
import com.mx.saua.repository.UserRepository;

/**
 * 
 * @author Benito Morales Ramirez
 * @implNote Implementa la seguridad de la aplicacion
 * 
 */
@Service("userSecurityService")
public class UserSecurityService implements UserDetailsService {

	/**
	 * Repositorio de usuario
	 */
	@Autowired
	@Qualifier("userRepository")
	private UserRepository userRepository;

	/**
	 * Obtiene userDetails a traves del username
	 */
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		com.mx.saua.entity.User user = userRepository.findByUsername(username)
				.orElseThrow(() -> new UsernameNotFoundException("No existe usuario"));
		List<GrantedAuthority> authorities = buildAuthorities(user.getAuthorities().iterator().next());
		return buildUser(user, authorities);
	}

	/**
	 * Construye usuario 
	 * @param user Usuario
	 * @param authorities Roles
	 * @return userSecurity
	 */
	private org.springframework.security.core.userdetails.User buildUser(com.mx.saua.entity.User user,
			List<GrantedAuthority> authorities) {
		return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(),
				user.isActive(), true, true, true, authorities);
	}

	/**
	 * Construye lista de roles
	 * @param role Authority
	 * @return Lista de roles
	 */
	private List<GrantedAuthority> buildAuthorities(Authority role) {
		Set<GrantedAuthority> auths = new HashSet<GrantedAuthority>();
		auths.add(new SimpleGrantedAuthority(role.getAuthority()));
		return new ArrayList<GrantedAuthority>(auths);
	}

	/**
	 * Obtiene el usuario actual loggeado
	 * @return UserDetails
	 */
	public UserDetails getUserCurrentDetails() {
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		UserDetails userDetails = null;
		if (principal instanceof UserDetails) {
			userDetails = (UserDetails) principal;
		}
		return userDetails;
	}
	
	/**
	 * Actualiza la fecha/hora en que sucede el logout
	 * @throws ServiceException
	 */
	public void updateLogout() throws ServiceException {
		UserDetails usr = getUserCurrentDetails();
		User user = userRepository.findByUsername(usr.getUsername())
				.orElseThrow(() -> new UsernameNotFoundException("No existe usuario"));
		user.setLastLogin(new Date());
		userRepository.save(user);
	}
	
	/**
	 * Obtiene el id de la sesión, que se usa para rastrear las operaciones del usuario
	 * @return idSession
	 */
	public String getIdSession() {
		String[] split = SecurityContextHolder.getContext().getAuthentication().getDetails().toString().split("\\,");
		String[] sp2 = split[1].replace("]", "").split("\\=");
		return sp2[1];
	}
}
