package com.mx.saua.service;

import java.util.List;

import com.mx.saua.dto.IncomeDto;
import com.mx.saua.entity.Income;

public interface IncomeService {
	
	List<IncomeDto> find();
	
	IncomeDto getById(Long id);
	
	Income getIncomeById(Long id);
	
	void save(IncomeDto incomeDto);
	
	void delete(Long id);

}
