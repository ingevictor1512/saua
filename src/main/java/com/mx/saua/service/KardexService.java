package com.mx.saua.service;

import com.mx.saua.dto.KardexDto;
import com.mx.saua.exception.ServiceException;

public interface KardexService {
	
	/**
	 * Obtiene la lista de las uaps actuales
	 * @param groupDto Dto donde se regresaran los grupos
	 * @return dto con los uaps cursadas
	 */
	KardexDto find(String  register);
	
	/**
	 * Obtiene el estado de la autoevaluación
	 * @return
	 * @throws ServiceException
	 */
	String isAutoEvalActive() throws ServiceException;

}
