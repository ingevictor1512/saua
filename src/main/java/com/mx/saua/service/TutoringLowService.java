package com.mx.saua.service;

import java.util.List;

import com.mx.saua.dto.TutoringLowDto;


public interface TutoringLowService {
	
	List<TutoringLowDto> find();
	
	TutoringLowDto getById(Long id);
	
	void save(TutoringLowDto tutoringLowDto);
	
	void delete(Long id);

}
