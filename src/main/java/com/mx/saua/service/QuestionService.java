/**
 * 
 */
package com.mx.saua.service;

import com.mx.saua.dto.QuestionDto;
import com.mx.saua.exception.ServiceException;

/**
 * @author Benito Morales
 * @version 1.0
 */
public interface QuestionService {

	/**
	 * Extrae todas preguntas para llenar el cuestionario a realizar
	 * @param studentDto
	 * @return
	 */
	QuestionDto find(QuestionDto questionDto, String inquestTypeEnum);
	
	/**
	 * Guarda las respuestas del usuario
	 * @param questionDto 		- Objeto con la informacion a guardar
	 * @throws ServiceException - Excepcion de servicio
	 */
	void saveFromApiRest(QuestionDto questionDto) throws ServiceException;
	
	/**
	 * Guarda las respuestas de la evaluación del profesor, realizada por el alumno
	 * @param questionDto Objeto con las respuestas
	 * @throws ServiceException Exception de servicio
	 */
	void saveTeacherEvalFromApiRest(QuestionDto questionDto) throws ServiceException;
	
	/**
	 * Obtiene la lista de preguntas, de acuerdo a una categoria
	 * @param idCategory Id de la categoria a buscar
	 * @return Lista de preguntas
	 */
	Object[][] findByCategory(int idCategory);
	
	/**
	 * Obtiene lista de preguntas paginada
	 * @param studentDto
	 * @return
	 */
	QuestionDto findPageable(QuestionDto studentDto);
	
	/**
	 * Obtiene una lista de preguntas por medio de su id
	 * @param questionDto Objeto donde viene la lista solicitada
	 * @return
	 */
	Object[][] findByIds(QuestionDto questionDto);
	
	/**
	 * Obtiene las preguntas de una encuesta
	 * @param idQuestion id de la encuesta
	 * @return Dto con la información
	 */
	QuestionDto findQuestionsByInquestId(long idQuestion);
	
	/**
	 * Guarda una pregunta con sus respuestas
	 * @param questionDto Objeto donde viajan los datos a guardar
	 * @throws ServiceException Excepcion de servicio
	 */
	void save(QuestionDto questionDto) throws ServiceException;
	
	/**
	 * Extrae todas preguntas para llenar el cuestionario a realizar
	 * 
	 * @param questionDto
	 * @return
	 */
	QuestionDto findAll(QuestionDto questionDto);
	
	/**
	 * 
	 * @param questionDto Pregunta a eliminar
	 * @throws ServiceException
	 */
	void delete(QuestionDto questionDto) throws ServiceException;
}
