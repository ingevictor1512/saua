package com.mx.saua.service;

/**
 * 
 * @author Benito Morales Ramirez
 * @version 1.0
 *
 */
public interface UserNotificationsService {
	
	/**
	 * Envía notificaciones por e-mail
	 */
	void sendNotifications();

}
