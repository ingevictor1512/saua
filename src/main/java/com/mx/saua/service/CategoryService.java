package com.mx.saua.service;

import java.util.List;

import com.mx.saua.dto.CategoryDto;


public interface CategoryService {
	
	List<CategoryDto> find();
	
	CategoryDto getById(Long id);
	
	void save(CategoryDto categoryDto);
	
	void delete(Long id);

}
