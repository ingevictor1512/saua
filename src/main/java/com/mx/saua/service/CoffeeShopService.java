package com.mx.saua.service;



import com.mx.saua.dto.CoffeeShopDto;

import java.util.List;

public interface CoffeeShopService {

    List<CoffeeShopDto> find();

    CoffeeShopDto getById(int id);

    void save(CoffeeShopDto coffeeShopDto);

    void delete(int id);
}
