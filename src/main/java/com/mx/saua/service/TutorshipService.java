package com.mx.saua.service;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.List;

import com.mx.saua.dto.StudentDto;
import com.mx.saua.dto.TeacherDto;
import com.mx.saua.dto.TutorshipDto;
import com.mx.saua.exception.ServiceException;

public interface TutorshipService {
	/**
	 * Obtiene la lista de uaps actuales
	 * @param uapDto Dto donde se regresaran las uaps
	 * @return dto con las uaps
	 */
	TutorshipDto find(TutorshipDto tutorshipDto);
	
	/**
	 * Guarda/actualiza uaps
	 * @param uapDto Dto con la uap a guardar/actualizar
	 * @throws ServiceException
	 */
	void save(TutorshipDto tutorshipDto) throws ServiceException;
	
	/**
	 * 
	 * @param uapDto uap a eliminar
	 * @throws ServiceException
	 */
	void delete(TutorshipDto tutorshipDto) throws ServiceException;
	
	/**
	 * Prepara Dto para agregar/modificar una uap 
	 * @param uapDto  Donde se cargaran los catalogos
	 * @return Dto con los catalogos necesarios
	 */
	TutorshipDto prepareDto(TutorshipDto tutorshipDto);
	/**
	 * Obtiene uap a través de la clave
	 * @param key Clave de la materia
	 * @return UapDto
	 * @throws ServiceException
	 */
	TutorshipDto getTutorshipByKey(Long key) throws ServiceException;
	/**
	 * Obtiene uap a través de la clave
	 * @param key Clave de la materia
	 * @return UapDto
	 * @throws ServiceException
	 */
	TutorshipDto getTutoredsByTeacher(TutorshipDto tutorshipDto);
	/**
	 * Guarda/actualiza tutorados
	 * @param uapDto Dto con la uap a guardar/actualizar
	 * @throws ServiceException
	 */
	void saveTutored(TutorshipDto tutorshipDto) throws ServiceException;
	
	/**
	 * Autoriza la carga academica del alumno
	 * @param String Matricula del alumno
	 * @throws ServiceException
	 */
	void loadAuthorization(String idRegister) throws ServiceException;
	
	/**
	 * Asigna un tutor a un grupo
	 * @param String Matricula del alumno
	 * @throws ServiceException
	 */
	void saveTutortoGroup(TutorshipDto tutorshipDto) throws ServiceException;
	
	/**
	 * Genera el excel de todas las tutorias
	 * @param Ninguno
	 * @throws ServiceException
	 */
	ByteArrayInputStream exportAllTutorships() throws IOException;
	
	/**
	 * Asigna una lista de alumnos a un tutor
	 * @param TutorshipDto TutorshipDto
	 * @throws ServiceException
	 */
	void saveStudentsToTutor(TutorshipDto tutorshipDto) throws ServiceException;
	
	/**
	 * Genera el excel de todas las tutorias
	 * @param Ninguno
	 * @throws ServiceException
	 */
	ByteArrayInputStream exportTutoredsByTutors(String ID) throws IOException;
	/**
	 * Elimina la autorización de carga academica del alumno
	 * @param long Id del registro de autorizacion
	 * @throws ServiceException
	 */
	void deleteloadAuthorization(long id) throws ServiceException;
	/**
	 * Obtiene todas las tutorias y sus estadisticas de tutorados activos e inactivos
	 * @throws ServiceException
	 */
	List<TutorshipDto> findAll() throws ServiceException;
	
	List<TeacherDto> findAllTutors() throws ServiceException; 
	
	TutorshipDto findByStudent(StudentDto studentDto);
	/**
	 * Genera el excel del listado de tutorados por ID tutor
	 * @param ID Id de la tabla profesor
	 * @throws ServiceException
	 */
	ByteArrayInputStream exportTutoredsByIdTutor(Long ID) throws IOException;
	/**
	 * Genera el excel de todos los alumnos que tienen al menos una uap cargada en el periodo activo
	 * @param Ninguno
	 * @throws ServiceException
	 */
	ByteArrayInputStream exportStudentsWithUaps() throws IOException;
}
