package com.mx.saua.service;

import com.mx.saua.dto.ResetPasswordDto;
import com.mx.saua.exception.ServiceException;

public interface ResetPasswordService {

	ResetPasswordDto find(ResetPasswordDto dto);
	
	ResetPasswordDto getOne(long id);
	
	void save(ResetPasswordDto resetPasswordDto) throws ServiceException;
	
	/**
	 * Obtiene lista pageable de solicitudes de reseteo de contraseñas
	 * @param resetPasswordDto objeto ResetPasswordDto
	 * @return Objeto ResetPasswordDto con los datos solicitados
	 */
	ResetPasswordDto findPageable(ResetPasswordDto resetPasswordDto);
}
