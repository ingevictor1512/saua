package com.mx.saua.service;

import java.util.List;

import com.mx.saua.dto.UserDto;
import com.mx.saua.entity.User;
import com.mx.saua.exception.ServiceException;
import com.mx.saua.dto.TeacherDto;

/**
 * 
 * @author Benito Morales Ramírez
 * @version 1.0
 *
 */

public interface UserService {

	/**
	 * Obtiene la lista de usuarios actuales
	 * @param userDto Dto donde se regresaran los usuarios
	 * @return dto con los usuarios
	 */
	UserDto find(UserDto userDto);
	
	/**
	 * Guarda/actualiza usuario
	 * @param userDto Dto con el usuario a guardar/actualizar
	 * @throws ServiceException
	 */
	void save(UserDto userDto) throws ServiceException;
	
	/**
	 * Obtiene usuario por id
	 * @param idUser 
	 * @return dto usuario
	 */
	UserDto getById(Long idUser);
	
	/**
	 * Elimina un usuario
	 * @param idUser Id de usuario a eliminar
	 * @throws ServiceException
	 */
	void delete(Long idUser) throws ServiceException;
	
	/**
	 * Obtiene usuario a traves de su  username
	 * @param username Nombre de usuario a buscar
	 * @return Usuario
	 */
	UserDto findByUsername(String username);
	
	/**
	 * Actualiza la contraseña actual del usuario
	 * @param userDto usuario a actualizar
	 * @throws ServiceException
	 */
	void updatePassword(UserDto userDto) throws ServiceException;
	
	/**
	 * 
	 * Guarda notificación de reseteo de contraseña, para que se envíe por correo
	 * @param user usuario que contiene los datos para resetear la contraseña
	 * @throws ServiceException 
	 */
	void resetPasswordForMail(UserDto user) throws ServiceException;
	
	/**
	 * Compara cadena hash enviada por correo electronico para resetear password
	 * @param hash
	 * @throws ServiceException
	 */
	void compareHash(String hash)throws ServiceException;
	
	/**
	 * Actualiza contraseña mediante hash enviado por correo
	 * @param form user 
	 * @throws ServiceException
	 */
	void updatePasswordForReset(UserDto form) throws ServiceException;
	/**
	 * Obtiene el listado de los maestros por rol
	 * @param form user 
	 * @throws ServiceException
	 */
	List<TeacherDto> findAllByRole(String role);
	
	/**
	 * Obtiene usuario atraves de su username
	 * @param username - Username
	 * @return - User
	 */
	User findUserByUsername(String username);
	
	/**
	 * Servicio para obtener una lista de matriculas para autocompletar un input en el front
	 * @param input
	 * @return
	 */
	List<String> doAutoCompleteUsername(String input);
}
