package com.mx.saua.service;

import java.util.List;

import com.mx.saua.dto.PhaseDto;



public interface PhaseService {
	
	List<PhaseDto> find();
	
	PhaseDto getById(Long id);
	
	void save(PhaseDto PhaseDto);
	
	void delete(Long id);

}
