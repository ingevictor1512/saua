package com.mx.saua.service;

import org.springframework.core.io.Resource;

import com.mx.saua.dto.StudentChargeDto;
import com.mx.saua.dto.StudentDto;
import com.mx.saua.dto.StudentResponseDto;
import com.mx.saua.exception.ServiceException;

/**
 * 
 * @author Benito Morales
 * @version 1.0
 *
 */
public interface StudentService {

	/**
	 * Obtiene la lista de alumnos actuales
	 * @param studentDto Dto donde se regresaran los alumnos
	 * @return dto con los alumnos
	 */
	StudentDto find(StudentDto studentDto);
	
	/**
	 * Obtiene la lista paginada de alumnos actuales
	 * @param studentDto DTO donde se regresaran los datos encontrados
	 * @return DTO con alumnos
	 */
	StudentDto findPageable(StudentDto studentDto);
	
	/**
	 * Guarda/actualiza alumno
	 * @param studentDto con el alumno a guardar/actualizar
	 * @throws ServiceException
	 */
	void save(StudentDto studentDto) throws ServiceException;
	
	/**
	 * Obtiene alumno a través de la matricula
	 * @param register Matricula
	 * @return StudentDto
	 * @throws ServiceException
	 */
	StudentDto getStudentByRegister(String register) throws ServiceException;
	
	/**
	 * Elimina un alumno
	 * @param studentDto Alumno a eliminar
	 * @throws ServiceException
	 */
	void delete(StudentDto studentDto) throws ServiceException;
	
	/**
	 * Prepara el dto con los catalogos que necesita
	 * @param groupDto a llenar
	 * @param option
	 * @return groupDto
	 */
	StudentDto prepareDto(StudentDto groupDto, String option);
	
	/**
	 * Guarda la carga academica del alumno
	 * @param studenChargeDto dto a guardar
	 * @throws ServiceException
	 */
	void saveCharge(StudentChargeDto studenChargeDto) throws ServiceException;
	
	/**
	 * Guarda la carga academica desde la session de administrador
	 * @param studenChargeDto Objeto a guardar
	 * @throws ServiceException Excepcion de servicio
	 */
	void saveChargeAdmin (StudentChargeDto studenChargeDto) throws ServiceException;
	
	/**
	 * Obtiene la lista carga actual del alunmo
	 * @param studentDto Dto donde se regresaran los alumnos
	 * @return dto con los alumnos
	 */
	StudentChargeDto findCharge(StudentChargeDto studentChargeDto);
	
	/**
	 * Elimina una clase de la carga academica del alumno
	 * @param studentChargeDto
	 * @throws ServiceException
	 */
	void delete(long idCharge) throws ServiceException;
	
	/**
	 * Obtiene la lista carga actual del alunmo
	 * @param StudentChargeDto Dto donde se regresaran los alumnos
	 * @return dto con la carga academica
	 */
	StudentChargeDto findChargeByRegister(String register);
	
	/**
	 * Realiza la logica de negocio para la generación de papeleta de materias de alumno
	 * @param register Matrícula del alumno
	 * @return Papeleta PDF convertida en un rreglo de bytes
	 * @throws ServiceException Excepción de servicio
	 */
	byte[] exportToPdfFile(String register) throws ServiceException;
	
	/**
	 * Agrega una materia a la carga de un alumno
	 * @param studentChargeDto Datos del alumno y materia a agregar
	 * @throws ServiceException Excepción de servicio
	 */
	void addToCharge(StudentChargeDto studentChargeDto) throws ServiceException;
	
	/**
	 * Recibe la solicitud de generación de papeleta en PDF, para su posterior descarga
	 * @param register Datos del usuario que solicitó la descarga
	 * @param isStudent Indicador para saber quien solicita la descarga de la papeleta
	 * @return Papeleta en PDF para su posterior descarga desde el front
	 * @throws ServiceException Excepcion de servicio
	 */
	Resource loadPapeletaAsResource(String register) throws ServiceException;
	
	Resource loadPapeletaAsResourceAdmin(String register) throws ServiceException;
	
    Resource loadPapeletaAsResourceAdminMasivo(String idProgram, String turn) throws ServiceException;
	
	int totalStudentsExist();
	
	/**
	 * Extrae carga por id
	 * @param id
	 * @return
	 */
	StudentChargeDto getChargeById(Long id);
	/**
	 * Devuelve el total de alumnos que exiten en la tabla de cargas autorizadas
	 */
	int totalStudentsLoadAutorizedExist();
	/**
	 * Obtiene la lista carga del periodo anterior del alunmo
	 * @param StudentChargeDto Dto donde se regresaran los alumnos
	 * @return dto con la carga academica
	 */
	StudentChargeDto findChargeByRegisterByPeriodoId(String register);

	Resource loadHorarioAsResource(String register) throws ServiceException;
	/**
	 * Realiza la logica de negocio para la generación de horario de materias del alumno
	 * @param register Matrícula del alumno
	 * @return Horario PDF convertido en un arreglo de bytes
	 * @throws ServiceException Excepción de servicio
	 */
	byte[] exportHorarioToPdfFile(String register) throws ServiceException;
	
	Resource loadEvaluationVoucherAsResource(String register) throws ServiceException;
	
	StudentResponseDto getStudentResponseByRegister(String register) throws ServiceException;
	
	void registerVisitStudent(StudentDto studentDto) throws ServiceException;
	
	void registerVisitGuest(StudentDto studentDto) throws ServiceException;
}
