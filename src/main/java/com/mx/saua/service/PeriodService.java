package com.mx.saua.service;

import java.util.List;

import com.mx.saua.dto.PeriodDto;
import com.mx.saua.entity.Period;
import com.mx.saua.exception.ServiceException;


public interface PeriodService {
	
	List<PeriodDto> find();
	
	PeriodDto getById(Long id);
	
	void save(PeriodDto periodDto);
	
	void delete(Long id) throws ServiceException;
	
	void activePeriod(Long id) throws ServiceException;

	Period getActivePeriod();
}
