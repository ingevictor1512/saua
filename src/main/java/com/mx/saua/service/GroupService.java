package com.mx.saua.service;

import java.util.List;

import com.mx.saua.dto.GroupDto;
import com.mx.saua.exception.ServiceException;

public interface GroupService {

	/**
	 * Obtiene la lista de grupos actuales
	 * @param groupDto Dto donde se regresaran los grupos
	 * @return dto con los grupos
	 */
	GroupDto find(GroupDto groupDto);
	
	/**
	 * Obtiene solo los grupos ligados al periodo activo
	 * @param groupDto
	 * @return
	 */
	public GroupDto findGroupsInActivePeriod(GroupDto groupDto);
	
	/**
	 * Obtiene solo los grupos ligados al anterior
	 * @param groupDto
	 * @return
	 */
	GroupDto findGroupsInPreviuosPeriod(GroupDto groupDto);
	
	/**
	 * Guarda/actualiza grupo
	 * @param groupDto Dto con el grupo a guardar/actualizar
	 * @throws ServiceException
	 */
	void save(GroupDto groupDto) throws ServiceException;
	
	/**
	 * Obtiene alumno a través del id
	 * @param register Matricula
	 * @return StudentDto
	 * @throws ServiceException
	 */
	GroupDto getGroupById(long id) throws ServiceException;
	
	/**
	 * 
	 * @param groupDto grupo a eliminar
	 * @throws ServiceException
	 */
	void delete(GroupDto groupDto) throws ServiceException;
	
	/**
	 * Prepara Dto para agregar/modificar un grupo 
	 * @param groupDto  Donde se cargaran los catalogos
	 * @return Dto con los catalogos necesarios
	 */
	GroupDto prepareDto(GroupDto groupDto);
	
	/**
	 * Retorna la lista de grupos
	 * @return
	 */
	List<GroupDto> find();
	

	/**
	 * Obtiene lista de grupos que pertenece a un Programa Educativo
	 * @param programName Programa educativo
	 * @return Lista de grupos
	 */
	List<GroupDto> findByProgram(String idprogram);
	
	/**
	 * Obtiene lista de programas para enviarlos al front
	 * @param idprogram
	 * @return
	 */
	public Object[][] findObjectByProgram(String idprogram);
	
	/**
	 * Obtiene los grupos válidos a promover de acuerdo a un grupo en espeficico
	 * @param idGroup
	 * @return
	 */
	public Object[][] findObjectByGrouPromote(long idGroup) throws ServiceException;
	
	public void promote(GroupDto groupDto) throws ServiceException;
	/**
	 * Asigna la carga academica de un grupo a todos sus alumnos
	 * @param idGroup
	 * @return Resultado de la transacción
	 */
	public int setAcademicChargeByGroup(long idGroup) throws ServiceException;
	
}
