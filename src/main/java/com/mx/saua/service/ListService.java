package com.mx.saua.service;

import java.io.FileNotFoundException;

import com.mx.saua.dto.GroupDto;
import com.mx.saua.exception.ServiceException;

import net.sf.jasperreports.engine.JRException;

public interface ListService {

	public byte[] studentListByGroupUap(GroupDto groupDto) throws ServiceException;
}
