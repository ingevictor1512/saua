package com.mx.saua.service;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.List;

import com.mx.saua.dto.GrantDto;
import com.mx.saua.exception.ServiceException;
import org.springframework.core.io.Resource;

public interface GrantService {
     /**
	 * Obtiene la lista de becas actuales
	 * @return dto con las becas
	 */
    List<GrantDto> find();
    /**
	 * Obtiene la lista de uaps actuales
	 * @param uapDto Dto donde se regresaran las uaps
	 * @return dto con las uaps
	 
	ProgramDto find(ProgramDto programDto);
	*/
	/**
	 * Guarda/actualiza uaps
	 * @param GrantDto Dto con la beca a guardar/actualizar
	 * @throws ServiceException
	 */
	void save(GrantDto grantDto) throws ServiceException;
	
	/**
	 * 
	 * @param GrantDto beca a eliminar
	 * @throws ServiceException
	 */
	void delete(Long id) throws ServiceException;

    GrantDto getById(Long id);
	/**
	 * Genera el excel de todas las becas registradas
	 * @param
	 * @throws IOException
	 */
	ByteArrayInputStream exportAllGrants() throws IOException;

	/**
	 * Exporta las respuestas de una encuesta en particular
	 * @return
	 * @throws ServiceException
	 */
	//Resource exportBecarios(long idInquest) throws ServiceException;
    
}
