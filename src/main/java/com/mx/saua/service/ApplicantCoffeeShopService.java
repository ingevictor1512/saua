package com.mx.saua.service;

import java.util.List;

import com.mx.saua.dto.ApplicantCoffeeShopDto;
import com.mx.saua.dto.UserDto;
import com.mx.saua.exception.ServiceException;


/**
 * Interfaz para el manejo de los postulados con las cafeterias
 * @author Benito Morales
 * @version 1.0
 *
 */
public interface ApplicantCoffeeShopService {
	
	/**
	 * Guarda registros de postulados, con las con las cafeterias a las cuales tiene acceso
	 * @param applicantCoffeeShopDto
	 * @throws ServiceException
	 */
	void save(ApplicantCoffeeShopDto applicantCoffeeShopDto) throws ServiceException;

	/**
	 * Obtiene una lista de postulados-cafeterias por usuario
	 * @param userDto usuario
	 * @return Lista de postulados-cafeterias
	 */
	List<ApplicantCoffeeShopDto> getByUser(UserDto userDto);

	/**
	 * Obtiene una lista de postulados-cafeterias pasando como parametro la matricula del alumno
	 * @param register Matricula a buscar
	 * @return Lista de postulados-cafeterias
	 */
	List<ApplicantCoffeeShopDto> getApplicantCoffeeShop(String register);
	
	/**
	 * Elimina un registro de postulados-cafeterias
	 * @param applicantCoffeeShopDto Postulado a eliminar
	 * @throws ServiceException Excepción de servicio
	 */
	void delete(ApplicantCoffeeShopDto applicantCoffeeShopDto) throws ServiceException;

}
