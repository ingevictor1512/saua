package com.mx.saua.service;

import com.mx.saua.dto.InquestQuestionDto;

public interface InquestQuestionService {

	/**
	 * Obtiene la lista paginada de preguntas
	 * @param studentDto DTO donde se regresaran los datos encontrados
	 * @return DTO con alumnos
	 */
	InquestQuestionDto findPageable(InquestQuestionDto inquestQuestionDto);
}
