package com.mx.saua.service;

import com.mx.saua.dto.CountDto;
import com.mx.saua.dto.OfferedClassDto;
import com.mx.saua.exception.ServiceException;

public interface OfferedClassService {
	
	/**
	 * Obtiene la lista de clases ofertadas actuales
	 * @param offeredClassDto donde se regresaran las clases ofertadas
	 * @return dto con las clase ofertadas actuales
	 */
	OfferedClassDto find(OfferedClassDto offeredClassDto);
	
	/**
	 * Obtiene la lista paginada de clases ofertadas actuales
	 * @param offeredClassDto donde se regresaran las clases ofertadas
	 * @return dto con las clase ofertadas actuales
	 */
	OfferedClassDto findPageable(OfferedClassDto offeredClassDto);
	
	/**
	 * Guarda/actualiza profesor
	 * @param offeredClassDto Dto con la clase ofertada a guardar/actualizar
	 * @throws ServiceException
	 */
	void save(OfferedClassDto offeredClassDto) throws ServiceException;
	
	/**
	 * Obtiene una clase ofertada a través de su id
	 * @param id de la clase
	 * @return offeredClassDto
	 * @throws ServiceException
	 */
	OfferedClassDto getOfferedClassById(long id) throws ServiceException;
	
	/**
	 * Elimina una clase ofertada
	 * @param offeredClassDto a eliminar
	 * @throws ServiceException
	 */
	void delete(OfferedClassDto offeredClassDto) throws ServiceException;
	
	/**
	 * Prepara el dto que se enciara al front.
	 * @param offeredClassDto Dto a preparar
	 * @return Dto
	 */
	OfferedClassDto prepareDto(OfferedClassDto offeredClassDto);
	
	/**
	 * Obtiene una lista de materias ofertadas, del grupo solicitado
	 * @param idGroup Id del grupo
	 * @return Lista de Clases ofertadas
	 */
	Object[][] getUapsByGroup(long idGroup);
	
	Object[][] getUapsRequiredByGroup(long idGroup);
	
	Object[][] findByUapsForGroup(long idgroup);
	
	String countStudentsByGroup(CountDto countDto);
}
