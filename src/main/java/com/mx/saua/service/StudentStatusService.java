package com.mx.saua.service;

import java.util.List;

import com.mx.saua.dto.StudentStatusDto;


public interface StudentStatusService {
	
	List<StudentStatusDto> find();
	
	StudentStatusDto getById(Long id);
		
	void save(StudentStatusDto studentStatusDto);
	
	void delete(Long id);

}
