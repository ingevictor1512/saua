package com.mx.saua.service;

import com.mx.saua.dto.GuestDto;
import com.mx.saua.exception.ServiceException;

public interface GuestService {
    /**
     * Obtiene la lista de invitados para las cafeterias
     * @param gestDto
     * @return
     */
	GuestDto find(GuestDto gestDto) throws ServiceException;

	GuestDto prepareDto(GuestDto gestDto);

    void save(GuestDto gestDto) throws ServiceException;
}
