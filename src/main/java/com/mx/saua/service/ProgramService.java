package com.mx.saua.service;

import java.util.List;

import com.mx.saua.dto.ProgramDto;
import com.mx.saua.dto.UapDto;
import com.mx.saua.exception.ServiceException;



public interface ProgramService {
	
	List<ProgramDto> find();
	
	/**
	 * Obtiene la lista de uaps actuales
	 * @param uapDto Dto donde se regresaran las uaps
	 * @return dto con las uaps
	 */
	ProgramDto find(ProgramDto programDto);
	
	/**
	 * Guarda/actualiza uaps
	 * @param uapDto Dto con la uap a guardar/actualizar
	 * @throws ServiceException
	 */
	void save(ProgramDto programDto) throws ServiceException;
	
	/**
	 * 
	 * @param uapDto uap a eliminar
	 * @throws ServiceException
	 */
	void delete(ProgramDto programDto) throws ServiceException;
	
	/**
	 * Prepara Dto para agregar/modificar una uap 
	 * @param uapDto  Donde se cargaran los catalogos
	 * @return Dto con los catalogos necesarios
	 */
	ProgramDto prepareDto(ProgramDto programDto);
	/**
	 * Obtiene uap a través de la clave
	 * @param key Clave de la materia
	 * @return UapDto
	 * @throws ServiceException
	 */
	ProgramDto getProgramByKey(String key) throws ServiceException;

	/**
	 * Regresa una lista de programas convertidos a Object[][]
	 * @return Object[][]
	 */
	Object[][] findObject();
	
	/**
	 * Asignar la carga academica inicial (obligatorias) a los alumnos por programa educativo
	 * @param key Clave de la materia
	 * @return int indicador para verificar que se realizo la carga correctamente
	 * @throws ServiceException
	 */
	int setAcademicChargeByProgram(String studentPlanKey) throws ServiceException;
	
}
