package com.mx.saua.service;

import java.util.List;

import org.springframework.core.io.Resource;

import com.mx.saua.dto.UapDto;
import com.mx.saua.entity.Uap;
import com.mx.saua.exception.ServiceException;

public interface UapService {
	
	/**
	 * Obtiene la lista de uaps actuales
	 * @param uapDto Dto donde se regresaran las uaps
	 * @return dto con las uaps
	 */
	UapDto find(UapDto uapDto);
	
	/**
	 * Guarda/actualiza uaps
	 * @param uapDto Dto con la uap a guardar/actualizar
	 * @throws ServiceException
	 */
	void save(UapDto uapDto) throws ServiceException;
	
	/**
	 * 
	 * @param uapDto uap a eliminar
	 * @throws ServiceException
	 */
	void delete(UapDto uapDto) throws ServiceException;
	
	/**
	 * Elimina lista de materias
	 * @param list
	 * @throws ServiceException
	 */
	void deleteFromList(String list) throws ServiceException;
	
	/**
	 * Prepara Dto para agregar/modificar una uap 
	 * @param uapDto  Donde se cargaran los catalogos
	 * @return Dto con los catalogos necesarios
	 */
	UapDto prepareDto(UapDto uapDto);
	/**
	 * Obtiene uap a través de la clave
	 * @param key Clave de la materia
	 * @return UapDto
	 * @throws ServiceException
	 */
	UapDto getUapByKey(String key) throws ServiceException;
	
	/**
	 * Busca Uaps por nombre
	 * @param nameLike
	 * @return Lista de coincidencias
	 */
	List<Uap> findByName(String nameLike);
	
	/**
	 * Obtiene lista de UAPs que pertenecen a un Programa Educativo
	 * @param programName Programa educativo
	 * @return Lista de UAPs
	 */
	List<UapDto> findByProgram(String programName);
	
	public Object[][] findByProgramForCatalog(String idprogram);
	
	/**
	 * Metodo para generar el reporte estadistico de unidades de aprendizaje ofertadas
	 * 
	 * @return recurso, a convertir a PDF
	 * @throws ServiceException Excepción de servicio
	 */
	Resource loadStatisticsUAPsAsResource() throws ServiceException;
	
	/**
	 * Metodo para exportar a pdf recurso generado en loadStatisticsUAPsAsResource
	 * 
	 * @return Arreglo de Bytes
	 * @throws ServiceException Excepción de servicio
	 */
	byte[] exportToPdfFile() throws ServiceException;
}
