package com.mx.saua.service;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.List;

import org.springframework.core.io.Resource;

import com.mx.saua.dto.TeacherDto;
import com.mx.saua.exception.ServiceException;

/**
 * 
 * @author Benito Morales
 * @version 1.0
 *
 */
public interface TeacherService {

	/**
	 * Obtiene la lista de profesores actuales
	 * @param teacherDto donde se regresaran los profesores
	 * @return dto con los profesores
	 */
	TeacherDto find(TeacherDto teacherDto);
	
	/**
	 * Guarda/actualiza profesor
	 * @param teacherDto Dto con el profesor a guardar/actualizar
	 * @throws ServiceException
	 */
	void save(TeacherDto teacherDto) throws ServiceException;
	
	/**
	 * Obtiene profesor a través de su Numero de empleado
	 * @param register Numero de empleado del profesor
	 * @return teacherDto
	 * @throws ServiceException
	 */
	TeacherDto getTeacherByRegister(String register) throws ServiceException;
	
	/**
	 * Elimina un profesor
	 * @param teacherDto a eliminar
	 * @throws ServiceException
	 */
	void delete(TeacherDto teacherDto) throws ServiceException;
	
	/**
	 * Obtiene la lista completa de los profesores
	 * @return Lista de profesores
	 */
	List<TeacherDto> findAll();
	
	/**
	 * Regresa una lista de teachers convertidos a Object[][]
	 * @return Object[][]
	 */
	Object[][] findObject();
	/**
	 * Recibe la solicitud de generación de evaluacion docente en PDF, para su posterior descarga
	 * @param register Datos del usuario del que se generará la evaluación
	 * @return Papeleta en PDF para su posterior descarga desde el front
	 * @throws ServiceException Excepcion de servicio
	 */
	Resource loadEvaluacionTeacherAsResource(String register) throws ServiceException;
	/**
	 * Realiza la logica de negocio para la generación de la evaluacion docente
	 * @param register No de empleado del docente
	 * @return Papeleta PDF convertida en un arreglo de bytes
	 * @throws ServiceException Excepción de servicio
	 */
	byte[] exportToPdfFile(Long register) throws ServiceException;
	/**
	 * Realiza la logica de negocio para la generación del total de encuestas contestadas para todos los docentes
	 * @return un arreglo de bytes
	 * @throws ServiceException Excepción de servicio
	 */
	ByteArrayInputStream exportInquestTotal() throws IOException; 
	/**
	 * Realiza la logica de negocio para la generación de las evaluaciones para todos los docentes
	 * @return un arreglo de bytes
	 * @throws ServiceException Excepción de servicio
	 */
	ByteArrayInputStream exportEvalsTotal() throws IOException; 
	
	/**
	 * Realiza la logica de negocio para la exportacion de las evaluaciones para todos los docentes de todos los alumnos
	 * @return un arreglo de bytes
	 * @throws ServiceException Excepción de servicio
	 */
	Resource exportEvalsTeachersAll(long idPeriod) throws ServiceException; 
}
