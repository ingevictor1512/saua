package com.mx.saua.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.mx.saua.service.impl.UserSecurityService;

public class Arqsrv {
	
	/**
	 * Servicio para obtener datos de la sesion
	 */
	@Autowired
	public UserSecurityService userSecurityService;
	
	public Logger LOGGER = LoggerFactory.getLogger(Arqsrv.class);
}
