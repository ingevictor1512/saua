package com.mx.saua.service;

import com.mx.saua.dto.StudentChargeDto;

/**
 * Service UserAnswerEvalService
 * @author Benito Morales
 * @version 1.0
 *
 */
public interface UserAnswerEvalService {

	/**
	 * Obtiene el status de la evaluación docente
	 * @param dto Objeto con las preguntas de la evaluación
	 * @return Objeto con preguntas de la evaluacion + el status de la misma
	 */
	StudentChargeDto getStatusEvaluations(StudentChargeDto dto);
	/**
	 * Obtiene el total de alumnos que contestaron la autoevaluacion
	 */
	int totalAutoSurveyAnswered();
	
	/**
	 * Obtiene el total de alumnos que contestaron la evaluación docente
	 */
	int totalSurveyDocentAnswered();
	
}
