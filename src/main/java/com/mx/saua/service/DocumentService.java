package com.mx.saua.service;
import java.io.ByteArrayInputStream;
import java.io.IOException;

import org.springframework.core.io.Resource;
import org.springframework.security.core.userdetails.UserDetails;

import com.mx.saua.dto.DocumentDto;
import com.mx.saua.enums.AuthorityEnum;
import com.mx.saua.exception.ServiceException;

public interface DocumentService {

	/**
	 * Obtiene la lista de documentos actuales
	 * @param documentDto Dto donde se regresaran los documentos
	 * @return dto con los documentos
	 */
	DocumentDto find(DocumentDto documentDto);
	
	DocumentDto findByRole(DocumentDto documentDto, AuthorityEnum role);
	
	/**
	 * Obtiene documento del path donde se encuentre guardado
	 * @param id del documento a extraer
	 * @return documento, para descargar
	 * @throws ServiceException
	 */
	Resource loadFileAsResource(String id, UserDetails user) throws ServiceException;
	
	/**
	 * 
	 * @param documentDto a eliminar
	 * @throws ServiceException
	 */
	void delete(DocumentDto documenDto) throws ServiceException;
	
	/**
	 * Genera el excel del listado alumnos de las uaps por grupo
	 * @param ID Id del grupo
	 * @throws ServiceException
	 */
	ByteArrayInputStream exportListExcelByGroupId(Long ID) throws IOException;
	/**
	 * Genera el excel del listado de alumnos de las uaps por docente
	 * @param ID Id del grupo
	 * @throws ServiceException
	 */
	ByteArrayInputStream exportListExcelByTeacherId(String ID) throws IOException;

	ByteArrayInputStream exportStudentsListExcelByGroupId(Long id) throws IOException;
	
}
