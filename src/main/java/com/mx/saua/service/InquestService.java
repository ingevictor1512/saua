package com.mx.saua.service;

import com.mx.saua.dto.InquestDto;
import com.mx.saua.exception.ServiceException;

/**
 * Interfaz para el manejo de encuestas
 * @author Benito Morales
 * @version 1.0
 *
 */
public interface InquestService {

	/**
	 * Obtiene la lista de encuestas
	 * @param InquestDto
	 * @return
	 */
	InquestDto find(InquestDto  questionDto);
	
	/**
	 * Guarda una encuesta, con la lista seleccionada de preguntas
	 * @param inquestDto
	 * @throws ServiceException
	 */
	void save(InquestDto inquestDto) throws ServiceException;
	
	/**
	 * Actualiza una encuesta
	 * @param inquestDto Objeto con los datos a actualizar
	 * @throws ServiceException Excepción de servicio
	 */
	void update(InquestDto inquestDto) throws ServiceException;
	
	/**
	 * Busca una encuesta por id
	 * @param id de la encuesta
	 * @return DTO con los datos
	 */
	InquestDto findInquestById(long id);
	
	/**
	 * Elimina una encuesta
	 * @param inquestDto Objeto con los datos de la encuesta a eliminar
	 * @throws ServiceException Excepción de servicio
	 */
	void delete(InquestDto inquestDto) throws ServiceException;
	
	/**
	 * Prepara los datos que serán mostrados en el modal, para seleccionar las preguntas a agregar a una encuesta
	 * @param questionDto Objeto donde se regresaran los datos al front
	 * @return
	 */
	InquestDto prepare(InquestDto questionDto);
}
