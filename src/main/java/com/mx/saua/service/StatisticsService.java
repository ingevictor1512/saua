/**
 * 
 */
package com.mx.saua.service;

/**
 * @author Benito Morales
 *
 */
public interface StatisticsService {

	/**
	 * Regresa una lista de objetos con las estadisticas 
	 * para dashboard de administrador
	 * @return Object[][]
	 */
	Object[][] findStatisticsForAdmin();
	
	/**
	 * Regresa una lista de objetos con las estadisticas 
	 * para dashboard de tutor
	 * @return Object[][]
	 */
	Object[][] findStatisticsForTutor();
}
