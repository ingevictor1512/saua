package com.mx.saua.service;

import java.util.List;

import com.mx.saua.dto.CategoryQuestionDto;

public interface CategoryQuestionService {

	List<CategoryQuestionDto> find();
}
