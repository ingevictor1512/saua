package com.mx.saua.service;

import com.mx.saua.dto.EntryRegisterDto;
import com.mx.saua.exception.ServiceException;

public interface EntryRegisterService {

	void save(EntryRegisterDto entryRegisterDto) throws ServiceException;
}
