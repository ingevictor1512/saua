package com.mx.saua.service;

import java.util.List;

import com.mx.saua.dto.ConfigurationDto;
import com.mx.saua.dto.PeriodDto;

public interface ConfigService {

	List<ConfigurationDto> find();

	ConfigurationDto getById(Long id);

	void save(ConfigurationDto configurationDto);
}
