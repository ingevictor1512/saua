package com.mx.saua.service;

import com.mx.saua.dto.GroupDto;
import com.mx.saua.dto.InfoEvaluationDto;
import com.mx.saua.dto.PeriodDto;
import com.mx.saua.dto.UserDto;
import com.mx.saua.entity.Period;
import com.mx.saua.entity.User;
import com.mx.saua.exception.ServiceException;

public interface InfoEvaluationService {
    /**
     * Obtiene la lista de la informacion de las evaluaciones
     * @param infoEvaluationDto Dto donde se regresara la info de las evaluaciones
     * @return dto con la info de las evaluaciones
     */
    InfoEvaluationDto find(InfoEvaluationDto infoEvaluationDto);
    /**
     * Obtiene solo la informacion de las evaluaciones del periodo activo
     * @param infoEvaluationDto
     * @return
     */
    InfoEvaluationDto findInfoEvaluationInActivePeriod(InfoEvaluationDto infoEvaluationDto);
    /**
     * Obtiene solo la informacion de las evaluaciones del periodo activo y de un usuario especifico
     * @param user
     * @param period
     * @return dto con la informacion
     */
    InfoEvaluationDto findByIdInfoEvaluationUserAndIdInfoEvaluationPeriod(long user, long period);

    /**
     * Guarda/actualiza informacion de evaluaciones
     * @param infoEvaluationDto Dto con el infoEvaluationDto a guardar/actualizar
     * @throws ServiceException
     */
    void save(InfoEvaluationDto infoEvaluationDto) throws ServiceException;

    void activateEvaluations(Long id) throws ServiceException;

    void desactivateEvaluations(Long id) throws ServiceException;
}
