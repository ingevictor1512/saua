package com.mx.saua.enums;

/**
 * 
 * @author Benito Morales
 * @version 1.0
 *
 */
public enum AuthorityEnum {
	STUDENT("ALUMNO"), ADMIN("ADMINISTRADOR"), TEACHER("PROFESOR"), TUTORING_COORDINATOR("COORDINADOR DE TUTORIAS"),
	TUTOR("TUTOR"), CASHIER("CAJERO"), UNKNOWN("UNKNOWN");

	/**
	 * Valor del enum
	 */
	private String value;

	AuthorityEnum(final String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}
}
