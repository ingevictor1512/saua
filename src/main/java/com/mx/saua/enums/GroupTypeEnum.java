package com.mx.saua.enums;

/**
 * Enum para los tipos de grupos
 * @author Benito Morales
 * @version 1.0
 *
 */
public enum GroupTypeEnum {
	NORMAL("NORMAL"), MIXED("MIXTO"), REPEAT("REPITE");

	/**
	 * Valor del enum
	 */
	private String value;
	
	GroupTypeEnum(final String value){
		this.value = value;
	}
	
	public String getValue() {
		return value;
	}
}