package com.mx.saua.enums;

/**
 * Enum para notificaciones
 * @author Benito Morales Ramirez
 * @version 1.0
 *
 */
public enum NotificationTypeEnum {
	RESET, RESET_DIRECT, REGISTER, CHANGED_P, AUTHORIZED_L;
}
