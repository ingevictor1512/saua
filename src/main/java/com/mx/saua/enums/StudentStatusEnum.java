package com.mx.saua.enums;

/**
 * Enum para los estatus del alumno
 * @author Benito Morales
 * @version 1.0
 *
 */
public enum StudentStatusEnum {
	REGISTER("REGISTRADO"), NOT_REGISTER("NO REGISTRADO"), 
	GRADUATED("EGRESADO"), TEMPORARY_LOW("BAJA TEMPORAL"), DEFINITIVE_LOW("BAJA DEFINITIVA");
	
	/**
	 * Valor del enum
	 */
	private String value;
	
	StudentStatusEnum(final String value){
		this.value = value;
	}
	
	public String getValue() {
		return value;
	}
}
