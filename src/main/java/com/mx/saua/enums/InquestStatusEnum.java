package com.mx.saua.enums;

/**
 * Enum para status de 	encuestas
 * @author Benito Morales ramirez
 * @version 1.0
 *
 */
public enum InquestStatusEnum {
	ACTIVA, INACTIVA, VALIDACION, ANSWERED, PENDING;
}
