package com.mx.saua.enums;

public enum QuestionType {
	OPCIONES("OPCIONES"),  ABIERTA("ABIERTA"), ABIERTA_TEXT("ABIERTA_TEXT"), ABIERTA_NUM("ABIERTA_NUM"), FALSO_VERDADERO("FALSO_VERDADERO"), FALSO("FALSO"),
	VERDADERO("VERDADERO"), OPCION("OPCION"), MULTIPLE("MULTIPLE"), RATING_0_10("RATING_0_10");

	/**
	 * Valor del enum
	 */
	private String value;

	QuestionType(final String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}
}
