package com.mx.saua.enums;

/**
 * Enum para status de 	encuestas
 * @author Benito Morales ramirez
 * @version 1.0
 *
 */
public enum InquestTypeEnum {
	NUEVO_INGRESO("NUEVO_INGRESO"), AUTOEVALUACION("AUTOEVALUACION"), EVAL_DOCENTE("EVAL_DOCENTE"), ENCUESTA("ENCUESTA"), DEFAULT("DEFAULT");
	
	/**
	 * Valor del enum
	 */
	private String value;
	
	InquestTypeEnum(final String value){
		this.value = value;
	}
	
	public String getValue() {
		return value;
	}
}
