package com.mx.saua.enums;

/**
 * Enum para status de notificacioens
 * @author Benito Morales ramirez
 * @version 1.0
 *
 */
public enum NotificationStatusEnum {
    REGISTRADA, ENVIADA, ENVIADA_FAIL, RESET_FAIL, REGISTER_FAIL, CHANGED_P_FAIL, AUTHORIZED_L_FAIL; 
}
