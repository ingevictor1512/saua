/**
 * 
 */
package com.mx.saua.enums;

/**
 * @author Benito Morales
 *
 */
public enum StudyTypeEnum {

	STUDY("CURSAR"), RE_STUDY("RECURSAR");

	/**
	 * Valor del enum
	 */
	private String value;
	
	StudyTypeEnum(final String value){
		this.value = value;
	}
	
	public String getValue() {
		return value;
	}
}
