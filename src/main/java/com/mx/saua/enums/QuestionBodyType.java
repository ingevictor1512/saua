package com.mx.saua.enums;

public enum QuestionBodyType {
	TEXTO,
	IMAGEN,
	NINGUNO,
	CODIGO
}
