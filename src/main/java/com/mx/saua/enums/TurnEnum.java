package com.mx.saua.enums;

/**
 * Enum para los turnos
 * @author Benito Morales
 * @version 1.0
 *
 */
public enum TurnEnum {
	MORNING("MATUTINO"), AFTERNOON("VESPERTINO");

	/**
	 * Valor del enum
	 */
	private String value;
	
	TurnEnum(final String value){
		this.value = value;
	}
	
	public String getValue() {
		return value;
	}
}
