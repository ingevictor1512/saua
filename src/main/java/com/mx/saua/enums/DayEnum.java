package com.mx.saua.enums;

public enum DayEnum {
	L("LUNES"), M("MARTES"), X("MIERCOLES"), J("JUEVES"), V("VIERNES"), S("SABADO"), D("DOMINGO");
	
	/**
	 * Valor del enum
	 */
	private String value;
	
	DayEnum(final String value){
		this.value = value;
	}
	
	public String getValue() {
		return value;
	}
}
