package com.mx.saua.exception;

/**
 * ServiceException
 * @author Benito Morales Ramirez
 * @version 1.0
 *
 */
public class ServiceException extends Exception {

	/**
	 * Serial de la clase
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Manejador de excepcion
	 * @param message
	 */
	public ServiceException(String message) {
		super(message);
	}

}
