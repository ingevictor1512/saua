package com.mx.saua.repository;

import java.util.List;

import org.hibernate.query.NativeQuery;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.mx.saua.entity.Student;

/**
 * Repositorio student
 * @author Benito Morales
 *
 */

@Repository("studentRepository")
public interface StudentRepository extends JpaRepository<Student, String>{
	
	@Query("select o from Student o where o.group.id = :idGroup and o.studentStatus.id =:idStatus")
	List<Student> findStudentsByGroupAndStatus(long idGroup, long idStatus);
	
	/**
	 * Obtiene la lista de resultados paginados de la matricula buscada
	 * @param register Matricula
	 * @param pageable Objeto pageable
	 * @return Page
	 */
    @Query("select s from Student s where s.register like CONCAT('%',:register,'%')")
    Page<Student> findStundentByRegister(@Param("register") String register, Pageable pageable);
    
    /**
     * Obtiene la lista de resultados paginados del nombre buscado
     * @param name Nombre a buscar
     * @param pageable Objeto pageable
     * @return Page
     */
    @Query("select s from Student s where CONCAT(name,' ',lastname,' ', secondLastname) like CONCAT('%',:name,'%')")
    Page<Student> findStundentByName(@Param("name") String name, Pageable pageable);
    
    /**
     * Obtiene la lista de alumnos que coinciden con el nombre de grupo, turno o programa educativo
     * @param groupname Nombre del grupo
     * @param pageable Objeto pageable
     * @return Page
     */
    @Query("select s from Student s where s.group.name like CONCAT('%',:groupname,'%') "
    		+ "or s.group.turn like CONCAT('%',:groupname,'%') "
    		+ "or s.program.abr like CONCAT('%',:groupname,'%')")
    Page<Student> findStundentByGroup(@Param("groupname") String groupname, Pageable pageable);
    
    /**
     * Obtiene lista de alumnos que coincidan con el correo buscado
     * @param email email buscado
     * @param pageable Objeto pageable
     * @return Page
     */
    @Query(value = "select s from Student s Inner join User usr on usr.student = s where usr.email LIKE CONCAT('%',:email,'%')")
    Page<Student> findStundentByEmail(@Param("email") String email, Pageable pageable);
    
    List<Student> findByGroupId(long id);
    /**
     * Obtiene la lista de alumnos que tengan el estatus recibido por el parametro
     * @param idStatus email buscado
     * @return Page
     */
    @Query("select o from Student o where o.studentStatus.id =:idStatus")
    List<Student> findStudentsByStatus(long idStatus);
    
	/**
	 * Obtiene la lista de resultados paginados de la matricula buscada
	 * @param register Matricula
	 * @return Student
	 */
    Student findStundentByRegister(@Param("register") String register);
    
    /**
     * Obtiene la lista de resultados de la matricula buscada
     * @param register Matricula
     * @return Student
     */
    @Query("select s from Student s where s.register like CONCAT('%',:register,'%')")
    Student findStudentByRegisterCustom(@Param("register") String register);
    
    
    Page<Student> findByIncomeDescription(String description, Pageable pageable);
}
