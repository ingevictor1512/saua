package com.mx.saua.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mx.saua.entity.Inquest;
/**
 * Repositorio para encuestas
 * @author Benito Morales
 *
 */

@Repository("inquestRepository")
public interface InquestRepository extends JpaRepository<Inquest, Long>{

}
