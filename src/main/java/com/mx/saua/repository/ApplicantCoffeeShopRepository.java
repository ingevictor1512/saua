package com.mx.saua.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.mx.saua.entity.ApplicantCoffeeShop;
import com.mx.saua.entity.ApplicantCoffeeShopId;

@Repository("applicantCoffeeShopRepository")
public interface ApplicantCoffeeShopRepository  extends JpaRepository<ApplicantCoffeeShop, ApplicantCoffeeShopId> {
   
	List<ApplicantCoffeeShop> findByApplicantCoffeeShopIdApplicantId(Long idApplicantId);
	
	List<ApplicantCoffeeShop> findByApplicantCoffeeShopId(Long idApplicantId);
	
	@Query("select app from ApplicantCoffeeShop app where app.applicantCoffeeShopId.applicantId=:applicantId "
			+ "and app.applicantCoffeeShopId.coffeeShopId=:coffeeShopId "
			+ "and app.applicantCoffeeShopId.usedate=:usedate")
	ApplicantCoffeeShop getOneCustomByIdApplicantCoffeeShopId(Long applicantId, int coffeeShopId, String usedate);
	
}
