package com.mx.saua.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mx.saua.entity.Space;

/**
 * @author Victor FG
 * @version 1.0
 */
@Repository("spacesRepository")
public interface SpacesRepository extends JpaRepository<Space, Long>{

}
