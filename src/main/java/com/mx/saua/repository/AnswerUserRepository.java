/**
 * 
 */
package com.mx.saua.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.mx.saua.entity.AnswersUser;
import com.mx.saua.entity.Inquest;
import com.mx.saua.entity.Question;
import com.mx.saua.entity.User;

/**
 * @author Benito Morales
 *
 */
@Repository("userAnswerRepository")
public interface AnswerUserRepository extends JpaRepository<AnswersUser, Long>{

	@Query("Select ans from AnswersUser ans where ans.question.id = :idQuestion and ans.user.idUser = :idUser and ans.answer.id = :idAnswer")
	List<AnswersUser> finByQuestionAndAnswerAndUser(long idQuestion, long idUser, long idAnswer);
	
	@Query("Select ans from AnswersUser ans where ans.user.idUser = :idUser")
	List<AnswersUser> finByUserId(long idUser);
	
	@Query("Select DISTINCT (ans.user.idUser) from AnswersUser ans")
	List<Long> findByUserIdUnique();
	
	@Query("Select ans from AnswersUser ans where ans.question.id = :idQuestion and ans.user.idUser = :idUser")
	List<AnswersUser> finByUserAndQuestion(long idUser, long idQuestion);
	
	List<AnswersUser> findByQuestionAndUserAndInquest(Question question, User user, Inquest inquest);
	
	List<AnswersUser> findByInquestOrderByUserAscOrderansw(Inquest inquest);
	
	@Query("Select ans from AnswersUser ans where ans.question.id = :idQuestion and ans.user.username = :username and ans.idPeriod = :idPeriod")
	List<AnswersUser> finByQuestionAndAnswerAndPeriod(long idQuestion, String username, long idPeriod);
}
