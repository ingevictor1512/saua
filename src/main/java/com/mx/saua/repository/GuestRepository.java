package com.mx.saua.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mx.saua.entity.Guest;

@Repository("guestRepository")
public interface GuestRepository extends JpaRepository<Guest, String> {
	
	Guest findOneByIdRegister(String idRegister);
	
}
 