package com.mx.saua.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.mx.saua.entity.Authority;
import com.mx.saua.entity.User;

/**
 * AuthorityRepository
 * @author Benito Morales Ramirez
 * @version 1.0
 *
 */
@Repository("authorityRepository")
public interface AuthorityRepository extends JpaRepository<Authority, Long>{

	//@Query("select o from StudentCharge o where o.student.register = :idStudent and o.period.activePeriod = :idActivo")

	//@Query("select auth from Authority auth where auth.user.idUser = :idUser")
	List<Authority> findByUser(User user);
	
	@Query("select count(a) from Authority a where a.authority = :auth")
	long countByAuthority(@Param("auth") String auth);
	
	List<Authority> findByAuthority(String name);
}
