package com.mx.saua.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mx.saua.entity.UapType;

@Repository("uapTypeRepository")
public interface UapTypeRepository extends JpaRepository<UapType, Long> {

}
