package com.mx.saua.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.mx.saua.entity.Kardex;

/**
 * Repositorio para el kardex
 * @author Victor Francisco
 * @version 1.0
 *
 */
@Repository("kardexRepository")
public interface KardexRepository extends JpaRepository<Kardex, Long>{
	
	@Query("select o from Kardex o where o.register = :register order by o.cveUap ASC")
	List<Kardex> findByMatricula(String register);
}
