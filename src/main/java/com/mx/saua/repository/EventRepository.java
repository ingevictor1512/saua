package com.mx.saua.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mx.saua.entity.Events;

/**
 * @author Victor FG
 * @version 1.0
 */
@Repository("eventRepository")
public interface EventRepository extends JpaRepository<Events, Long>{

}
