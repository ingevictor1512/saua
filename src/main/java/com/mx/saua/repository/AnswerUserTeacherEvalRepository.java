/**
 * 
 */
package com.mx.saua.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.mx.saua.entity.AnswersUserTeacherEval;
import com.mx.saua.entity.Period;
import com.mx.saua.entity.StudentCharge;
import com.mx.saua.entity.User;

/**
 * Repositorio AnswerUserTeacherEvalRepository
 * @author Benito Morales
 * @version 1.0
 *
 */
@Repository("answerUserTeacherEvalRepository")
public interface AnswerUserTeacherEvalRepository extends JpaRepository<AnswersUserTeacherEval, Long>{

	/**
	 * Obtiene la lista de respuestas de la evaluación docente de un usuario
	 * @param user Usuario
	 * @param StudentCharge Carga del alumno
	 * @return Lista de respuestas
	 */
	List<AnswersUserTeacherEval> findByUserAndStudentChargeAndStudentChargePeriod(User user, StudentCharge StudentCharge, Period period);
	
	/**
	 * Obtiene la lista de respuestas de la evaluación docente de una clase ofertada
	 * @param user Docente a buscar
	 * @param StudentCharge Carga del alumno
	 * @return Lista de respuestas
	 */
	@Query("select o from AnswersUserTeacherEval o where o.studentCharge.offeredClass.id = :studentCharge and o.studentCharge.offeredClass.teacher.register = :registerTeacher")
	List<AnswersUserTeacherEval> findByTeacherAndStudentCharge(String registerTeacher, long studentCharge);
	/**
	 * Obtiene la lista de respuestas de la evaluación docente de una pregunta determinada para obtener la calificacion
	 * @param user Docente a buscar
	 * @return Lista de respuestas
	 */
	@Query("select o from AnswersUserTeacherEval o where o.question.id = :questionId and o.studentCharge.offeredClass.teacher.register = :registerTeacher")
	List<AnswersUserTeacherEval> findByTeacherAndQuestionId(String registerTeacher, long questionId);
	/**
	 * Obtiene el total de alumnos que han evaluado a los docentes
	 */
	@Query("Select DISTINCT (ans.user.idUser) from AnswersUserTeacherEval ans")
	List<Long> findByUserIdUnique();
	/**
	 * Obtiene la lista de clases ofertadas de un usuario
	 * @param id Usuario a buscar
	 * @return Lista de clases ofertadas.
	 */
	@Query("select DISTINCT (ans.studentCharge.id) from AnswersUserTeacherEval ans where ans.user.idUser = :idUser and ans.studentCharge.period.id=:periodId")
	List<Long> findByUserIdStudentCharge(long idUser, long periodId);
	/**
	 * Obtiene la lista de respuestas de la evaluación docente de una clase ofertada
	 * @param cargaalumnoid Carga de un alumno a buscar
	 * @return Lista de respuestas
	 */
	@Query("select o from AnswersUserTeacherEval o where o.studentCharge.id = :cargaAlumnoId")
	List<AnswersUserTeacherEval> findBystudentChargeId(long cargaAlumnoId);
	
	@Transactional
	@Modifying
	@Query("delete from AnswersUserTeacherEval o where o.studentCharge.id = :cargaAlumnoId")
	void deleteByStudentChargeId(long cargaAlumnoId);
}
