package com.mx.saua.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mx.saua.entity.Period;

@Repository("periodRepository")
public interface PeriodRepository  extends JpaRepository<Period, Long>{
	
	/**
	 * Obtiene el periodo alctualmente activo
	 * @param idActivePeriod
	 * @return
	 */
	Period getOneByActivePeriod(boolean idActivePeriod);
}
