package com.mx.saua.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mx.saua.entity.UserNotification;
import com.mx.saua.enums.NotificationStatusEnum;

@Repository("userNotificationRepository")
public interface UserNotificationRepository extends JpaRepository<UserNotification, Long> {

    List<UserNotification> findAllByStatus(NotificationStatusEnum notificationStatus);
    
    UserNotification findByPlainPassword(String plainPassword);
}
