package com.mx.saua.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mx.saua.entity.Category;


@Repository("categoryRepository")
public interface CategoryRepository extends JpaRepository<Category, Long> {
	
}
