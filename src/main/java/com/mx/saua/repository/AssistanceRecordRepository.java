package com.mx.saua.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mx.saua.entity.AssistanceRecord;
/**
 * @author Victor FG
 * @version 1.0
 */
@Repository("assistanceRecordRepository")
public interface AssistanceRecordRepository extends JpaRepository<AssistanceRecord, Long> {

}
