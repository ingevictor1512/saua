/**
 * 
 */
package com.mx.saua.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.mx.saua.entity.Group;
import com.mx.saua.entity.StudentCharge;
import com.mx.saua.entity.Uap;

/**
 * @author Benito Morales
 * @version 1.0
 *
 */

@Repository("studentChargeRepository")
public interface StudentChargeRepository extends JpaRepository<StudentCharge, Long>{
	
	/**
	 * Obtiene la carga academica del alumno
	 * @param idStudent Id del alumno a buscar
	 * @param idActivo Id del programa educativp
	 * @return Lista de uaps de la carga academica del alumno en cuestion
	 */
	@Query("select o from StudentCharge o where o.student.register = :idStudent and o.period.activePeriod = :idActivo")
	List<StudentCharge> findStudentChargeByRegister(String idStudent, boolean idActivo);
	
	List<StudentCharge> findStudentChargeByStudentRegisterAndPeriodId(String idStudent, long idPeriod);
	@Query("select o from StudentCharge o where o.student.register = :idStudent and o.period.id = :idPeriod")
	List<StudentCharge> findStudentChargeByStudentRegisterAndPeriod(String idStudent, long idPeriod);

	/**
	 * Busca coincidencias de UAP que ya se encuentren agregadas a la carga del alumno
	 * @param key Id de la UAP a buscar
	 * @return Lista de UAP's encontradas
	 */
	@Query("select s from StudentCharge s where s.student.register = :register and s.period.activePeriod = :idActivo and s.offeredClass.uap.key = :key")
	List<StudentCharge> findStudentChargeByOfferedClassUapKeyAndRegister(String key, String register, boolean idActivo);
	
	/**
	 * Busca coincidencias de UAP con codigo comun
	 * @param common_code Codigo en comun a buscar
	 * @return Lista de coincidencias
	 */
	@Query("select s from StudentCharge s where s.period.activePeriod = :idActivo and s.student.register = :register and s.offeredClass.uap.common_code = :common_code")
	List<StudentCharge> findStudentChargeByOfferedClassUapCommon_code(String common_code,  String register, boolean idActivo);
	
	/**
	 * Obtiene la lista de clases ofertadas de una UAP en especifico
	 * @param uap
	 * @return Lista de clases ofertadas
	 */
	List<StudentCharge> findStudentChargeByOfferedClassUap(@Param("uap")Uap uap);
	
	/**
	 * Obtiene la carga de una clase ofertada en especifico
	 * @param uap
	 * @return Lista de clases ofertadas
	 */
	@Query("select sc from StudentCharge sc where sc.offeredClass.id = :id order by sc.student.lastname, sc.student.secondLastname, sc.student.name ASC")
	List<StudentCharge> findStudentChargeByOfferedClassId(Long id);
	
	/**
	 * Obtiene la lista de clases ofertadas de un grupo en especifico
	 * @param group
	 * @return Lista de clases ofertadas
	 */
	List<StudentCharge> findStudentChargeByOfferedClassGroup(@Param("group")Group group);
	
	@Query("select count(s.id) from StudentCharge s where s.offeredClass.id = :id")
	long countOfferedClass(long id);
	
	@Query("select count(s.id) from StudentCharge s "
			+ "inner join LoadAuthorization auth on s.student.register = auth.student.register where s.offeredClass.id = :id")
	long countOfferedClassAuthorized(long id);
}
