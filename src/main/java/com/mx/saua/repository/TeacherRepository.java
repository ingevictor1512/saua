package com.mx.saua.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.mx.saua.entity.Teacher;

@Repository("teacherRepository")
public interface TeacherRepository extends JpaRepository<Teacher, Long>{

	Teacher getOneByRegister(String register);
	
	@Query("select t from Teacher t order by t.name,t.lastname,t.secondLastname asc")
	List<Teacher> findAllOrderByNameLastnameSecondLastnameAsc();
}
