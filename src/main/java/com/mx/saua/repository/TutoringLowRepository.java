package com.mx.saua.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mx.saua.entity.TutoringLow;


@Repository("tutoringLowRepository")
public interface TutoringLowRepository extends JpaRepository<TutoringLow, Long> {
	
	TutoringLow findByDescription(String description);
	
}
