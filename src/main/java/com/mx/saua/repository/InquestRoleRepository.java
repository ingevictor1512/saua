package com.mx.saua.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Repository;

import com.mx.saua.entity.Inquest;
import com.mx.saua.entity.InquestRole;

@Repository("inquestRoleRepository")
public interface InquestRoleRepository extends JpaRepository<InquestRole, Long>{

	List<InquestRole> findByRole(String role);
	
    @Transactional
    @Modifying
	void deleteInquestRoleByInquest(Inquest inquest);
    
    InquestRole findByInquestId(long inquestId);
}
