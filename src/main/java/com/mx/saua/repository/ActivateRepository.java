package com.mx.saua.repository;

import com.mx.saua.entity.Activate;
import com.mx.saua.entity.Period;
import com.mx.saua.entity.User;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository("activateRepository")
public interface ActivateRepository extends JpaRepository<Activate, Long> {

	List<Activate> findActivateByUserAndPeriod(User user, Period period);
}
