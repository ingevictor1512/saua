package com.mx.saua.repository;

import com.mx.saua.entity.CoffeeShop;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository("coffeeShopRepository")
public interface CoffeeShopRepository extends JpaRepository<CoffeeShop, Integer> {
	
	List<CoffeeShop> findByManager(String managerId);
}
