																																																																												package com.mx.saua.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.mx.saua.entity.Group;
import com.mx.saua.entity.OfferedClass;
import com.mx.saua.entity.Uap;

/**
 * 
 * @author Benito Morales
 * @version 1.0
 *
 */
@Repository("offeredClassRepository")
public interface OfferedClassRepository extends JpaRepository<OfferedClass, Long>{
	
	@Query("select o from OfferedClass o where o.group.id = :idGroup order by o.uap.name")
	List<OfferedClass> findByGroup(long idGroup);
	
	@Query("select o from OfferedClass o where o.group.id = :idGroup and o.uap.uapType.category = 'OBLIGATORIA' order by o.uap.name")
	List<OfferedClass> findByGroupRequired(long idGroup);
	
	OfferedClass findByUapKeyAndGroupId(String key, long id);
	
	//@Query("select o from OfferedClass o where o.group.id = :idGroup and o.uap.uapType.category = 'OBLIGATORIA' order by o.uap.name")
	List<OfferedClass> findByGroup(Group group);
	
	List<OfferedClass> findByPeriodActivePeriod(boolean isActivePeriod);
	
	Page<OfferedClass> findByPeriodActivePeriodOrderById(Pageable pageable, boolean isActivePeriod);
	
	@Query("select o from OfferedClass o where o.teacher.register = :register order by o.uap.name")
	List<OfferedClass> findByTeacherRegister(String register);
	
	@Query("select o from OfferedClass o where o.teacher.register = :register and o.period.activePeriodAdmin = 1 order by o.uap.name")
	List<OfferedClass> findByTeacherRegisterActivePeriod(String register);
	
	@Query("select o from OfferedClass o where o.teacher.register = :register and o.period.id = 1 order by o.uap.name")
	List<OfferedClass> findByTeacherRegisterPeriodBefore(String register);
	
	/**
	 * Busqueda paginada por clave de AUP
	 * @param key Clave a buscar
	 * @param pageable Objeto pageable
	 * @return Lista de coincidencias
	 */
    @Query("select o from OfferedClass o where o.uap.key like CONCAT('%',:key,'%') and o.period.activePeriodAdmin = 1")
    Page<OfferedClass> findOfferedClassByUapKey(@Param("key") String key, Pageable pageable);
    
    /**
     * Busqueda paginada por nombre de UAP
     * @param uapname Nombre de UAP
     * @param pageable Objeto pageable
     * @return Lista de coincidencias
     */
    @Query("select o from OfferedClass o where o.uap.name like CONCAT('%',:uapname,'%') and o.period.activePeriodAdmin = 1")
    Page<OfferedClass> findOfferedClassByUapName(@Param("uapname") String uapname, Pageable pageable);
    
    /**
     * Busqueda paginada por nombre profesor
     * @param teacherName Nombre del profesor a buscar
     * @param pageable Objeto pageable
     * @return Lista de coincidencias
     */
    @Query("select o from OfferedClass o "
    		+ "inner join Teacher tch on tch.id = o.teacher.id "
    		+ "where (o.teacher.name like CONCAT('%',:teacherName,'%') "
    		+ "or o.teacher.lastname like CONCAT('%',:teacherName,'%') "
    		+ "or o.teacher.secondLastname like CONCAT('%',:teacherName,'%')) and o.period.activePeriodAdmin = 1")
    Page<OfferedClass> findOfferedClassByTeacherName(@Param("teacherName") String teacherName, Pageable pageable);
    
    /**
     * Busqueda paginada por grupo
     * @param groupname Nombre de gurpo
     * @param pageable Objeto pageable
     * @return Lista de coincidencias
     */
    @Query("select o from OfferedClass o where (o.group.name like CONCAT('%',:groupname,'%') "
    		+ "or o.group.turn like CONCAT('%',:groupname,'%')) and o.period.activePeriodAdmin = 1")
    Page<OfferedClass> findOfferedClassByGroupName(@Param("groupname") String groupname, Pageable pageable);
    
    /**
     * Busqueda paginada por programa educativo
     * @param program Programa educativo
     * @param pageable Objeto pageable
     * @return Lista de coincidencias
     */
    @Query("select o from OfferedClass o where o.group.educationalProgram.abr like CONCAT('%',:program,'%')")
    Page<OfferedClass> findOfferedClassByProgram(@Param("program") String program, Pageable pageable);
    
    /**
     * Busqueda paginada por semestre
     * @param semester semestre a buscar
     * @param pageable Objeto pageable
     * @return Lista de coincidencias
     */
    @Query("select o from OfferedClass o where o.semester like CONCAT('%',:semester,'%')")
    Page<OfferedClass> findOfferedClassBySemester(@Param("semester") String semester, Pageable pageable);
    
    OfferedClass findOfferedClassByGroupAndUap(Group group, Uap uap);
}
