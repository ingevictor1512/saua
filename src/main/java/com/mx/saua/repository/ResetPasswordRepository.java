package com.mx.saua.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.mx.saua.entity.ResetPassword;

@Repository("resetPasswordRepository")
public interface ResetPasswordRepository  extends JpaRepository<ResetPassword, Long>{
	
	@Query("select r from ResetPassword r where r.hashValue =:hash")
	ResetPassword findOneByHashValue(@Param("hash") String hash);
	
	/**
	 * Obtiene la lista de resultados paginados de la matricula buscada
	 * @param register Matricula
	 * @param pageable Objeto pageable
	 * @return Page
	 */
    @Query("select r from ResetPassword r where r.idReset=:idUser")
    Page<ResetPassword> findResetPasswordByIdUser(@Param("idUser") long idUser, Pageable pageable);
}
