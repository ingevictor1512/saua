package com.mx.saua.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.mx.saua.entity.Program;

/**
 * @author Vic
 * @version 1.0
 *
 */

@Repository("programRepository")
public interface ProgramRepository extends JpaRepository<Program, String> {

	Program findByStudentPlanName(String studentPlanName);
	
	@Query("select p from Program p order by studentPlanName asc")
	List<Program> findAllOrderByStudentPlanNameAsc();
}
