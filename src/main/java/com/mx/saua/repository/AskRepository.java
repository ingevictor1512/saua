package com.mx.saua.repository;

import com.mx.saua.entity.Question;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository("askRepository")
public interface AskRepository extends JpaRepository<Question, Long> {
    
}
