/**
 * 
 */
package com.mx.saua.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mx.saua.entity.Answer;
import com.mx.saua.entity.Question;

/**
 * @author Benito Morales
 * @version 1.0
 */
@Repository("answerrepository")
public interface AnswerRepository extends JpaRepository<Answer, Long>{

	List<Answer> findByQuestionOrderByIdAsc(Question question);
	
	Answer findByText(String text);
}
