package com.mx.saua.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.mx.saua.entity.Student;
import com.mx.saua.entity.Tutorship;

@Repository("tutorshipRepository")
public interface TutorshipRepository extends JpaRepository<Tutorship, Long> {
	
	@Query("select o from Tutorship o where o.teacher.register = :idTeacher and o.tutoringLow.id = :idEstatus  order by o.student.name")
	List<Tutorship> findByTeacherAndStatus(String idTeacher, Long idEstatus);
	
	List<Tutorship> findByStudentRegister(String idRegister);
	
	@Query("select o from Tutorship o where o.teacher.register = :idTeacher order by o.student.name")
	List<Tutorship> findByTeacher(String idTeacher);
	
	@Query("select DISTINCT(o) from Tutorship o")
	List<Tutorship> findByTeacherDistinct();

	Tutorship findByStudent(Student student);

	List<Tutorship> findAllByStudent(Student student);
	
	@Query("select t from Tutorship t group by t.teacher.register")
	List<Tutorship> findAllGroupbyTutoriateacher();
}
