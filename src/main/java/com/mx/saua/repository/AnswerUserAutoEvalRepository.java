/**
 * 
 */
package com.mx.saua.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.mx.saua.entity.AnswersUserAutoEval;
import com.mx.saua.entity.Period;
import com.mx.saua.entity.StudentCharge;
import com.mx.saua.entity.User;

/**
 * Repositorio AnswerUserTeacherEvalRepository
 * @author Benito Morales
 * @version 1.0
 *
 */
@Repository("answerUserAutoEvalRepository")
public interface AnswerUserAutoEvalRepository extends JpaRepository<AnswersUserAutoEval, Long>{

	/**
	 * Obtiene la lista de respuestas de la evaluación docente de un usuario
	 * @param user Usuario
	 * @param StudentCharge Carga del alumno
	 * @return Lista de respuestas
	 */
	List<AnswersUserAutoEval> findByUserAndStudentChargeAndStudentChargePeriod(User user, StudentCharge StudentCharge, Period period);
	
	@Query("Select DISTINCT (ans.user.idUser) from AnswersUserAutoEval ans")
	List<Long> findByUserIdUnique();
	/**
	 * Obtiene la lista de respuestas de la evaluación docente de una clase ofertada
	 * @param cargaalumnoid Carga de un alumno a buscar
	 * @return Lista de respuestas
	 */
	@Query("select o from AnswersUserAutoEval o where o.studentCharge.id = :cargaAlumnoId")
	List<AnswersUserAutoEval> findBystudentChargeId(long cargaAlumnoId);
	
	@Transactional
	@Modifying
	@Query("delete from AnswersUserAutoEval o where o.studentCharge.id = :cargaAlumnoId")
	void deleteByStudentChargeId(long cargaAlumnoId);
}
