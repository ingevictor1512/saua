package com.mx.saua.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mx.saua.entity.EntryRegister;
import com.mx.saua.entity.User;

@Repository("entryRegisterRepository")
public interface EntryRegisterRepository extends JpaRepository<EntryRegister, Long>{
	
	List<EntryRegister> findByDateStringAndUserStudentAndCoffeeShopId(String dateString, User user, int coffeeShopId);
}
