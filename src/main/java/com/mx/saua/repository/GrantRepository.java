package com.mx.saua.repository;

import com.mx.saua.entity.Grant;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository("grantRepository")
public interface GrantRepository extends JpaRepository<Grant, Long>{
    
}
