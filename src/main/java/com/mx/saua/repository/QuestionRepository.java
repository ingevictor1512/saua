/**
 * 
 */
package com.mx.saua.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.mx.saua.entity.CategoryQuestion;
import com.mx.saua.entity.Question;
import com.mx.saua.enums.InquestTypeEnum;

/**
 * @author Benito Morales
 * @version 1.0
 */
@Repository("questionRepository")
public interface QuestionRepository extends JpaRepository<Question, Long>{
	
	List<Question> findByStatus(boolean status);
	
	List<Question> findByInquestType(InquestTypeEnum inquestTypeEnum);

	List<Question> findByCategoryQuestionOrderByTextAsc(CategoryQuestion category);
	
    @Query("select q from Question q where q.categoryQuestion.id = :idCategory")
    Page<Question> findQuestionByCategory(@Param("idCategory") int idCategory, Pageable pageable);
    
    @Query( "select q from Question q where q.id in :ids" )
    List<Question> findByQuestionIds(@Param("ids") List<Long> questionIdList);
}
