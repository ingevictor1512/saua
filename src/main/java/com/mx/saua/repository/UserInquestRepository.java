package com.mx.saua.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mx.saua.entity.Inquest;
import com.mx.saua.entity.User;
import com.mx.saua.entity.UserInquest;

@Repository("userInquestRepository")
public interface UserInquestRepository extends JpaRepository<UserInquest, Long>{
	
	UserInquest findByUserAndInquestAndIdPeriod(User user, Inquest inquest, long idPeriod);
}
