package com.mx.saua.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mx.saua.entity.Document;

@Repository("documentRepository")
public interface DocumentRepository extends JpaRepository<Document, Long>{

	List<Document> findByAuthorizationsRoleIgnoreCaseContaining(String likeRole);
}
