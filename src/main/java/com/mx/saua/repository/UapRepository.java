package com.mx.saua.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.mx.saua.entity.Kardex;
import com.mx.saua.entity.Program;
import com.mx.saua.entity.Uap;

/**
 * Repositorio para las uaps
 * @author Vic
 * @version 1.0
 *
 */
@Repository("uapRepository")
public interface UapRepository extends JpaRepository<Uap, String>{

	/**
	 * Encuentra todas las coincidencias del texto a buscar
	 * @author Benito Morales
	 * @param nameLike nombre de la UAP a buscar
	 * @param program programa educativo al que pertenece la uap buscada
	 * @return Lista de uaps encontradas
	 */
	List<Uap> findByNameContainingIgnoreCaseAndEducationalProgram(@Param("nameLike") String nameLike, @Param("program") Program program);
	
	/**
	 * Obtiene la lista de materias que pertenecen a un programa educativo
	 * @param program programa educativo
	 * @return lista de UAPs
	 */
	List<Uap> findByEducationalProgramOrderByNameAsc(@Param("program") Program program);
	
	@Query("select o from Uap o where o.key = :register")
	Uap findByUapID(String register);
}
