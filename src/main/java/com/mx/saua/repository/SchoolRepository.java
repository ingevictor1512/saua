package com.mx.saua.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mx.saua.entity.School;

@Repository("schoolRepository")
public interface SchoolRepository extends JpaRepository<School, Long> {

}
