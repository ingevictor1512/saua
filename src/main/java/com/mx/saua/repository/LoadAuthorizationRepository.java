package com.mx.saua.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.mx.saua.entity.LoadAuthorization;
import com.mx.saua.entity.Period;

import java.util.List;

/**
 * Repositorio para las cargas academicas
 * @author Vic
 * @version 1.0
 *
 */
@Repository("loadAuthorizationRepository")
public interface LoadAuthorizationRepository extends JpaRepository<LoadAuthorization, Long>{
	@Query("select o from LoadAuthorization o where o.student.register = :idStudent and o.period.activePeriod = :idActivo")
	LoadAuthorization findByStudentAndPeriod(String idStudent, boolean idActivo);
	
	@Query("select o from LoadAuthorization o where o.student.register = :idStudent and o.period.activePeriod.id = :idPeriod")
	LoadAuthorization findByStudentAndActivePeriod(String idStudent, Long idPeriod);

	List<LoadAuthorization> findLoadAuthorizationByPeriodId(Long idPeriod);



}
