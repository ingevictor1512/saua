package com.mx.saua.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mx.saua.entity.Applicant;
import com.mx.saua.entity.Guest;
import com.mx.saua.entity.User;

@Repository("applicantRepository")
public interface ApplicantRepository extends JpaRepository<Applicant, Long> {

	Applicant findByUserAndStatusGrant(User user, boolean statusGrant);

	Applicant findByUser(User user);
	
	Applicant findByUserUsername(String username);
	
	Applicant findByGuestIdRegister(String idRegister);
	
	Applicant findByGuestAndStatusGrant(Guest guest, boolean statusGrant);
}
