package com.mx.saua.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mx.saua.entity.Phase;

@Repository("phaseRepository")
public interface PhaseRepository extends JpaRepository<Phase, Long> {

}
