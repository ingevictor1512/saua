package com.mx.saua.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mx.saua.entity.Income;

@Repository("incomeRepository")
public interface IncomeRepository extends JpaRepository<Income, Long> {

}
