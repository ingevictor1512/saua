package com.mx.saua.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mx.saua.entity.IdInfoEvaluation;
import com.mx.saua.entity.InfoEvaluation;
import com.mx.saua.entity.Period;
import com.mx.saua.entity.User;

import java.util.List;

@Repository("infoEvaluationRepository")
public interface InfoEvaluationRepository extends JpaRepository<InfoEvaluation, IdInfoEvaluation>{

	InfoEvaluation findByIdInfoEvaluationUserAndIdInfoEvaluationPeriod(User user, Period period);

	List<InfoEvaluation> findInfoEvaluationByIdInfoEvaluation_Period_IdAndTotalAutoEvalGreaterThan(Long idPeriod, int total);

	List<InfoEvaluation> findInfoEvaluationByIdInfoEvaluation_Period_IdAndTotalTeacherEvalGreaterThan(Long idPeriod, int total);

	List<InfoEvaluation> findInfoEvaluationByIdInfoEvaluation_Period_Id(Long idPeriod);

}
