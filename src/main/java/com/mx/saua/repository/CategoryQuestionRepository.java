package com.mx.saua.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mx.saua.entity.CategoryQuestion;

@Repository("categoryQuestionRepository")
public interface CategoryQuestionRepository extends JpaRepository<CategoryQuestion, Integer> {

	CategoryQuestion findByName(String name);
}
