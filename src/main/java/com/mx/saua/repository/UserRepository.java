package com.mx.saua.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.mx.saua.entity.User;

/**
 * UserRepository
 * @author Benito Morales Ramirez
 * @version 1.0
 *
 */
@Repository("userRepository")
public interface UserRepository extends JpaRepository<User, Long>{

	/**
	 * Obtiene usuario por su username
	 * @param username a buscar
	 * @return Optional user
	 */
	public Optional<User> findByUsername(String username);
	
	@Query("select u from User u where u.username=:username")
	public User customFindByUsername(String username);
	
	/**
	 * Obtiene un usuario por su email
	 * @param email
	 * @return
	 */
	public User findOneByEmailAndUsername(String email, String username);
	
	@Query("select u from User u where u.username like CONCAT('%',:username,'%')")
	List<User> findByUsernameCustom(String username);
}
