package com.mx.saua.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mx.saua.entity.Configuration;


@Repository("configurationRepository")
public interface ConfigurationRepository extends JpaRepository<Configuration, Long> {

    Configuration findByKey(String key);
}
