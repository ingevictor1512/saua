package com.mx.saua.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.mx.saua.entity.Group;
import com.mx.saua.entity.Program;

/**
 * Repositorio para los grupos
 * @author Benito Morales
 * @version 1.0
 *
 */
@Repository("groupRepository")
public interface GroupRepository extends JpaRepository<Group, Long>{

	List<Group> findByEducationalProgramAndPeriodActivePeriodOrderByNameAsc(Program program,boolean isActivePeriod);
	
	List<Group> findByNameAndEducationalProgramOrderByNameAsc(String name , Program program);
	
	List<Group> findByPeriodActivePeriodAdmin(boolean isActivePeriodAdmin);
	
	List<Group> findByPeriodId(Long id);
	
	@Query("select o from Group o where o.abbreviation = :turn  and o.educationalProgram.studentPlanKey = :register and o.period.activePeriod = :isActive")
	List<Group> findByTurnAndEducationalprogram(String register, String turn, boolean isActive);
	
	List<Group> findByPeriodIdAndEducationalProgram(Long id, Program program);
}
