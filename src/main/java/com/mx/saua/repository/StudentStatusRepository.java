package com.mx.saua.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mx.saua.entity.StudentStatus;

/**
 * Repositorio de StudentStatus
 * @author Benito Morales
 * @version 1.0
 *
 */
@Repository("studentStatusRepository")
public interface StudentStatusRepository extends JpaRepository<StudentStatus, Long>{
	
	StudentStatus getOneByDescription(String decription);
}
