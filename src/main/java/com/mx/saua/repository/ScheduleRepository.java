package com.mx.saua.repository;

import com.mx.saua.entity.Schedule;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("scheduleRepository")
public interface ScheduleRepository extends JpaRepository<Schedule, Long> {
    List<Schedule> findByOfferedClass_Id(Long id);
}
