package com.mx.saua.repository;

import java.util.List;

import javax.transaction.Transactional;

import com.mx.saua.entity.Question;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Repository;

import com.mx.saua.entity.Inquest;
import com.mx.saua.entity.InquestQuestion;

@Repository("inquestQuestionRepository")
public interface InquestQuestionRepository extends JpaRepository<InquestQuestion, Long>{

	List<InquestQuestion> findByInquest(Inquest inquest);
	
    Page<InquestQuestion> findInquestQuestionByInquestOrderByOrderquestion(Inquest inquest, Pageable pageable);
    
    @Transactional
    @Modifying
    void deleteInquestQuestionByInquest(Inquest inquest);
    
    List<InquestQuestion> findByInquestOrderByOrderquestion(Inquest inquest);

    InquestQuestion findByInquestAndQuestion(Inquest inquest, Question question);
}
