package com.mx.saua.entity;

import java.io.Serializable;

import javax.persistence.Embeddable;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Embeddable
public class IdInfoEvaluation implements Serializable{
	
	private static final long serialVersionUID = -4331074028640915719L;

	@ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "usuario_id", nullable=true)
	private User user;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "periodo_id", nullable=true)
	private Period period;
	
	
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public Period getPeriod() {
		return period;
	}
	public void setPeriod(Period period) {
		this.period = period;
	}
}
