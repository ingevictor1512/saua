package com.mx.saua.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 
 * @author Victor Francisco
 * @version 2.0
 */
@Entity
@Table(name = "kardex")
@Setter
@AllArgsConstructor
@Getter
@NoArgsConstructor
public class Kardex  implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "id", unique = true, nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@Column(name = "MATRICULA")
	private String register;
	
	@Column(name = "NOMBRE_ALUMNO")
	private String nameStudent;
	
	@Column(name = "CVE_ESCU_ALUMNO")
	private String cveSchoolStudent;
	
	@Column(name = "NOMBRE_ESCU_ALUMNO")
	private String nameSchoolStudent;
	
	@Column(name = "CVE_PLAN_ALUMNO")
	private String cvePlanStudent;
	
	@Column(name = "VRS_PLAN_ALUMNO")
	private String vrsPlanStudent;
	
	@Column(name = "NOMBRE_PLAN_ALUMNO")
	private String namePlanStudent;
	
	@Column(name = "CVE_MATERIA")
	private String cveUap;
	
	@Column(name = "NOMBRE_MATERIA")
	private String nameUap;
	
	@Column(name = "TIPO_EXAMEN")
	private String testType;
	
	@Column(name = "CALIFICACION")
	private String score;
	
	@Column(name = "CVE_APROB_REPROB")
	private String cveAproRepro;
	
	@Column(name = "CVE_PERIODO")
	private String cvePeriod;
	
	@Column(name = "FECHA_EXAMEN")
	private String examDate;
	
	@Column(name = "CVE_ESCU_MATERIA")
	private String cveSchoolUap;
	
	@Column(name = "NOMBRE_ESCU_MATERIA")
	private String nameSchoolUap;
	
	@Column(name = "NOMBRE_PLAN_MATERIA")
	private String namePlanUap;

	public Kardex(String register, String nameStudent, String cveSchoolStudent, String nameSchoolStudent,
			String cvePlanStudent, String vrsPlanStudent, String namePlanStudent, String cveUap, String nameUap,
			String testType, String score, String cveAproRepro, String cvePeriod, String examDate, String cveSchoolUap,
			String nameSchoolUap, String namePlanUap) {
		super();
		this.register = register;
		this.nameStudent = nameStudent;
		this.cveSchoolStudent = cveSchoolStudent;
		this.nameSchoolStudent = nameSchoolStudent;
		this.cvePlanStudent = cvePlanStudent;
		this.vrsPlanStudent = vrsPlanStudent;
		this.namePlanStudent = namePlanStudent;
		this.cveUap = cveUap;
		this.nameUap = nameUap;
		this.testType = testType;
		this.score = score;
		this.cveAproRepro = cveAproRepro;
		this.cvePeriod = cvePeriod;
		this.examDate = examDate;
		this.cveSchoolUap = cveSchoolUap;
		this.nameSchoolUap = nameSchoolUap;
		this.namePlanUap = namePlanUap;
	}
}
