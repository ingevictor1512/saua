package com.mx.saua.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 
 * @author Benito Morales
 * @version 2.0
 *
 */
@Entity
@Table(name = "info_evaluacion")
@Setter
@AllArgsConstructor
@Getter
@NoArgsConstructor
public class InfoEvaluation implements Serializable{

	private static final long serialVersionUID = -7475208392442662824L;
	
	@EmbeddedId
	private IdInfoEvaluation idInfoEvaluation;
	
	@Column(name = "total_auto_eval")
	private int totalAutoEval;
	
	@Column(name = "total_eval_pfr")
	private int totalTeacherEval;
	
	@Column(name = "f_registro")
	private Date registerDate;

	@Column(name = "f_modif")
	private Date editDate;

	@Column(name = "eval_activa")
	private boolean activeEval;
	
}	