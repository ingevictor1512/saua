package com.mx.saua.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.mx.saua.enums.InquestStatusEnum;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "encuesta_usuario")
@Setter
@AllArgsConstructor
@Getter
@NoArgsConstructor
public class UserInquest implements Serializable{

	/**
	 * Serial de la clase
	 */
	private static final long serialVersionUID = -7308277937893442101L;

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "encuesta_id", nullable=true)
	private Inquest inquest;
	
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "usuario_id", nullable=true)
	private User user;
	
	@Column(name = "status")
	@Enumerated(EnumType.STRING)
	private InquestStatusEnum status;
	
	@Column(name = "f_contesto")
	@Temporal(TemporalType.DATE)
	private Date answeredDate;
	
	@Column(name = "comentarios")
	private String comments;
	
	@Column(name = "current_question_id")
	private String currentQuestionId;
	
	@Column(name = "periodo_id")
	private long idPeriod;

}
