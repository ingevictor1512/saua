package com.mx.saua.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * @author Benito Morales
 * @version 2.0
 */
@Entity
@Table(name = "postulado_cafeteria")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ApplicantCoffeeShop implements Serializable{

    private static final long serialVersionUID = 1L;

	@EmbeddedId
	private ApplicantCoffeeShopId applicantCoffeeShopId;
    
    @Column(name = "num_uso")
    private int useNumber;
    
    @Column(name = "f_registro")
    @Temporal(TemporalType.DATE)
    private Date registerDate;
    
    @Column(name = "f_modif")
    @Temporal(TemporalType.DATE)
    private Date editDate;
    
}
