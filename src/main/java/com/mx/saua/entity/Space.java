package com.mx.saua.entity;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 
 * @author Victor FG
 * @version 1.0
 */
@Entity
@Table(name = "laboratorios")
@Setter
@AllArgsConstructor
@Getter
@NoArgsConstructor
public class Space implements Serializable{

	/**
	 * Serializador de la clase
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(nullable = false, name = "id_laboratorio")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@Column(name = "nombre_Laboratorio")
	private String labName;
	
	@Column(name = "Descripcion")
	private String description;
	
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "usuario_id", nullable=true)
	private User user;
}
