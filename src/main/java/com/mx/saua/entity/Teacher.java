package com.mx.saua.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.mx.saua.enums.GenderEnum;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


/**
 * 
 * @author Benito Morales
 * @version 2.0
 */
@Entity
@Table(name = "profesor")
@Setter
@AllArgsConstructor
@Getter
@NoArgsConstructor
public class Teacher implements Serializable{

	/**
	 * Serial de la clase
	 */
	private static final long serialVersionUID = -4581224053853652243L;

	@Id
	@Column(unique = true, nullable = false)
	private long id;
	
	@Column(name = "nombre")
	private String name;
	
	@Column(name = "paterno")
	private String lastname;
	
	@Column(name = "materno")
	private String secondLastname;
	
	@Column(name = "cubiculo")
	private String cubicle;
	
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "categoria_profesor_id", nullable=false)
	private Category category;
	
	@Column(name = "f_registro")
	@Temporal(TemporalType.DATE)
	private Date registerDate;
	
	@Column(name = "f_modif")
	@Temporal(TemporalType.DATE)
	private Date editDate;

	@Column(name = "num_emp")
	private String register;
	
	@Column(name = "genero")
	@Enumerated(EnumType.STRING)
	private GenderEnum gender;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "teacher", cascade = CascadeType.ALL)
	private Set<User> users;
	
}
