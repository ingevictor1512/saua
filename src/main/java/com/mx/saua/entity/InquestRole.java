package com.mx.saua.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.mx.saua.enums.InquestStatusEnum;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Benito Morales
 * @version 2.0
 */
@Entity
@Table(name = "encuesta_rol")
@Setter
@AllArgsConstructor
@Getter
@NoArgsConstructor
public class InquestRole  implements Serializable{

	/**
	 * Serial de la clase
	 */
	private static final long serialVersionUID = 2905914816793856755L;

	@Id
	@Column
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "encuesta_id", nullable=true)
	private Inquest inquest;
	
	@Column(name = "rol")
	private String role;
	
	@Column(name = "f_inicio")
	private Date inquestInitDate;
	
	@Column(name = "f_fin")
	private Date inquestEndDate;
	
	@Column(name = "status")
	@Enumerated(EnumType.STRING)
	private InquestStatusEnum inquestStatusEnum;
}
