package com.mx.saua.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 
 * @author Victor FG
 * @version 2.0
 */
@Entity
@Table(name = "eventos")
@Setter
@AllArgsConstructor
@Getter
@NoArgsConstructor
public class Events implements Serializable{

	/**
	 * Serializador de la clase
	 */
	private static final long serialVersionUID = 566083574117661140L;
	
	@Id
	@Column(nullable = false, name = "id_Eventos")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@Column(name = "nombre_Evento")
	private String eventName;
	
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "id_laboratorio", nullable=true)
	private Space spaces;
	
	@Column(name = "fecha")
	@Temporal(TemporalType.DATE)
	private Date date;
	
	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "cve_prog_educ", nullable = false)
	private Program educationalProgram;
}
