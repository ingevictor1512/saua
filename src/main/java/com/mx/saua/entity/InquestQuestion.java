package com.mx.saua.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.mx.saua.dto.PatternDto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Benito Morales
 * @version 2.0
 */
@Entity
@Table(name = "encuesta_preguntas")
@Setter
@AllArgsConstructor
@Getter
@NoArgsConstructor
public class InquestQuestion extends PatternDto implements Serializable{

	/**
	 * Serializador de la clase
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "encuesta_id", nullable=true)
	private Inquest inquest;
	
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "preguntas_id", nullable=true)
	private Question question;
	
	@Column(name = "orden")
	private int orderquestion;
}
