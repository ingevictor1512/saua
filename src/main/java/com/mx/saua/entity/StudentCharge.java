/**
 * 
 */
package com.mx.saua.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.mx.saua.enums.StudyTypeEnum;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Benito Morales
 * @version 2.0
 *
 */
@Entity
@Table(name = "carga_alumno")
@Setter
@AllArgsConstructor
@Getter
@NoArgsConstructor
public class StudentCharge implements Serializable {

	/**
	 * Serial de la clase
	 */
	private static final long serialVersionUID = 8045891372471265841L;

	@Id
	@Column
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "alumno_matricula", nullable=false)
	private Student student;
	
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "periodo_id", nullable=false)
	private Period period;
	
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "clase_ofertada_id", nullable=false)
	private OfferedClass offeredClass;
	
	@Column(name = "f_carga")
	@Temporal(TemporalType.TIMESTAMP)
	private Date registerDate;
	
	@Column(name = "cve_autorizacion")
	private String authorizationKey;
	
	@Column
	private String comun;
	
	@Column(name = "f_modif")
	@Temporal(TemporalType.TIMESTAMP)
	private Date editDate;

	@Column(name = "tipo_cursamiento")
	@Enumerated(EnumType.STRING)
	private StudyTypeEnum studyType;

}
