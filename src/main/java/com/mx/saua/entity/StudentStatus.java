package com.mx.saua.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 
 * @author Benito Morales
 * @version 2.0
 */

@Entity
@Table(name = "status_alumno")
@Setter
@AllArgsConstructor
@Getter
@NoArgsConstructor
public class StudentStatus implements Serializable{

	/**
	 * Serial de la clase
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id", unique = true, nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@Column(name = "descripcion")
	private String description;
	
	@Column(name = "observaciones")
	private String comments;
	
}
