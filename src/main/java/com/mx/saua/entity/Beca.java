package com.mx.saua.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Entity
@Table(name = "beca")
public class Beca implements Serializable{

	/**
	 * Serial de la clase
	 */
	private static final long serialVersionUID = 1L;

	/**Entidad que permite guardar la informacion de las escuelas
	 * 
	 * @Vic
	 */
	@Id
	@Column(name = "id_Beca", unique = true, nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@Column(name = "beca")
	private String name;
	
	@Column(name = "descripcion")
	private String description;
}
