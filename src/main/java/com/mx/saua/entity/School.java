package com.mx.saua.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name="escuela_proc")
@Setter
@AllArgsConstructor
@Getter
@NoArgsConstructor
public class School implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**Entidad que permite guardar la informacion de las escuelas
	 * 
	 * @Vic
	 */
	@Id
	@Column(name = "id", unique = true, nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@Column(name = "nombre")
	private String name;
	
	@Column(name = "direccion")
	private String address;
	
	@Column(name = "telefono")
	private String phone;
	
	@Column(name = "mail")
	private String mail;
	
	@Column(name = "f_registro")
	private Date registerDate;

	@Column(name = "f_ult_modif")
	private Date editdate;
}
