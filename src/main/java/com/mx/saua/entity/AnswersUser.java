/**
 * 
 */
package com.mx.saua.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Benito Morales
 * @version 2.0
 */
@Entity
@Table(name = "usuario_respuestas")
@Setter
@AllArgsConstructor
@Getter
@NoArgsConstructor
public class AnswersUser implements Serializable{

	/**
	 * Serializador de la clase
	 */
	private static final long serialVersionUID = 5845997584031773338L;
	
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "id_respuesta", nullable=true)
	private Answer answer;
	
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "id_usuario", nullable=false)
	private User user;
	
	@Column(name = "resp_personalizada", nullable = true)
	private String customizedAnswer;
	
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "id_pregunta", nullable=false)
	private Question question;
	
	@Column(name = "especificacion")
	private String specification;
	
	@Column(name = "f_registro")
	private Date registerDate;
	
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "id_encuesta", nullable=true)
	private Inquest inquest;

	@Column(name = "orden")
	private int orderansw;

	@Column(name = "id_periodo")
	private long idPeriod;
	
	public AnswersUser(long id, Answer answer, User user, String customizedAnswer, Question question, String specification, Date registerDate) {
		super();
		this.id = id;
		this.answer = answer;
		this.user = user;
		this.customizedAnswer = customizedAnswer;
		this.question = question;
		this.specification = specification;
		this.registerDate = registerDate;
	}


}
