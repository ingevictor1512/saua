/**
 * 
 */
package com.mx.saua.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mx.saua.enums.InquestTypeEnum;
import com.mx.saua.enums.QuestionBodyType;
import com.mx.saua.enums.QuestionType;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Benito Morales
 * @version 2.0
 */
@Entity
@Table(name = "preguntas")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Question implements Serializable {

	/**
	 * Serializadore de la clase
	 */
	private static final long serialVersionUID = 8307614799090497447L;

	@Id
	@Column
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Column(name = "texto")
	private String text;

	@Column(name = "cuerpo")
	private String body;

	@Column(name = "tipo")
	@Enumerated(EnumType.STRING)
	private QuestionType questionType;

	@Column(name = "f_creacion")
	@Temporal(TemporalType.DATE)
	private Date creationDate;
	
	@Column(name = "place_holder")
	private String placeHolder;
	
	@Column
	private boolean status;

	@Column(name = "f_modif")
	@Temporal(TemporalType.DATE)
	private Date editDate;

	@Column(name = "tipo_cuerpo")
	@Enumerated(EnumType.STRING)
	private QuestionBodyType questionBodyType;

	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "question")
	@JsonIgnore
	@OrderBy("order ASC")
	private Set<Answer> answers;
	
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_categoria", nullable=true)
	@JsonIgnore
	private CategoryQuestion categoryQuestion;
	
	@Column(name = "encuesta")
	@Enumerated(EnumType.STRING)
	private InquestTypeEnum inquestType;

	public Question(long id, String text, String body, QuestionType questionType, Date creationDate, Date editDate,
			QuestionBodyType questionBodyType, String placeHolder, boolean status, CategoryQuestion categoryQuestion) {
		super();
		this.id = id;
		this.text = text;
		this.body = body;
		this.questionType = questionType;
		this.creationDate = creationDate;
		this.editDate = editDate;
		this.questionBodyType = questionBodyType;
		this.placeHolder = placeHolder;
		this.status = status;
		this.categoryQuestion = categoryQuestion;
	}
}
