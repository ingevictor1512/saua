package com.mx.saua.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 
 * @author Benito Morales
 * @version 2.0
 */
@Entity
@Table(name = "documentos")
@Setter
@AllArgsConstructor
@Getter
@NoArgsConstructor
public class Document implements Serializable{

	/**
	 * Serializador de la clase
	 */
	private static final long serialVersionUID = -9213539822551597400L;

	@Id
	@Column(nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@Column(name = "nombre_real")
	private String realName;
	
	@Column(name = "nombre_mostrar")
	private String viewName;
	
	@Column(name = "descripcion")
	private String description;
	
	@Column(name = "roles_aut")
	private String authorizationsRole;
	
	@Column(name = "type_documents")
	private int typeDocument;
	
}
