package com.mx.saua.entity;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.sun.istack.NotNull;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "invitado")
@Setter
@AllArgsConstructor
@Getter
@NoArgsConstructor
@ToString
@Builder
@Data
public class Guest implements Serializable{

	/**
	 * Serial de la clase
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id")
	@NotNull
	private String idRegister;

	@Column(name = "paterno")
	private String lastname;

	@Column(name = "materno")
	private String secondLastname;

	@Column(name = "nombre")
	private String name;
	
	@Column(name = "procedencia")
	private String origin;
	
}
