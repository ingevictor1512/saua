package com.mx.saua.entity;

import java.io.Serializable;
import java.sql.Time;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 *
 * @author Victor
 * @version 2.0
 *
 */
@Entity
@Table(name="registro_asistencias")
@Setter
@AllArgsConstructor
@Getter
@NoArgsConstructor
public class AssistanceRecord implements Serializable{

	/**
	 * Serializador de la clase
	 */
	private static final long serialVersionUID = 1748086842473672374L;
	
	@Id
	@Column(name = "id", unique = true, nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@Column(name = "hora_inicio")
	private Time initialTime;
	
	@Column(name = "hora_fin")
	private Time endTime;
	
	@Column(name = "fecha")
	private Date date;
	
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "id_Eventos", nullable=true)
	private Events events;
	
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "usuario_id", nullable=true)
	private User user;
}
