package com.mx.saua.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name="baja_tutoria")
@Setter
@AllArgsConstructor
@Getter
@NoArgsConstructor
public class TutoringLow implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * @author Vic
	 */
	@Id
	@Column(name = "id", unique = true, nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@Column(name = "descripcion")
	private String description;
	
	@Column(name = "f_registro")
	private Date registerDate;

	@Column(name = "f_modif")
	private Date editdate;
	
}
