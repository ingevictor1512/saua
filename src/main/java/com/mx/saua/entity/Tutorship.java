package com.mx.saua.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name="tutoria")
@Setter
@AllArgsConstructor
@Getter
@NoArgsConstructor
public class Tutorship implements Serializable{

	private static final long serialVersionUID = 1L;

	/**
	 * @author Vic
	 * Entidad que permite guardar las tutorias
	 */
	@Id
	@Column(name = "id", unique = true, nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@Column(name = "observaciones")
	private String observations;
	
	@Column(name = "asigno_tutor")
	private String assignedTutor;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "alumno_matricula", nullable = false)
	private Student student;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "profesor_id", nullable = false)
	private Teacher teacher;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "baja_tutoria_id", nullable = false)
	private TutoringLow tutoringLow;
	
	@Column(name = "f_registro")
	private Date registerDate;

	@Column(name = "f_modif")
	private Date editdate;
}
