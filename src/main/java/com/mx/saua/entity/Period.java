package com.mx.saua.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 
 * @author Benito Morales
 * @version 2.0
 *
 */
@Entity
@Table(name = "periodo")
@Setter
@AllArgsConstructor
@Getter
@NoArgsConstructor
public class Period implements Serializable{

	/**
	 * Serializador de la clase
	 */
	private static final long serialVersionUID = 8742184399632929947L;

	@Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
	
    @Column(name = "f_inicial", nullable = false)
    @Temporal(TemporalType.DATE)
    private Date initialDate;
    
    @Column(name = "f_final", nullable = false)
    @Temporal(TemporalType.DATE)
    private Date finalDate;
	
    @Column(name = "periodo_lectivo", nullable = false)
    private String schoolPeriod;
    
    @Column(name = "periodo_activo", nullable = false)
    private boolean activePeriod;
    
    @Column(name = "periodo_activo_admin", nullable = false)
    private boolean activePeriodAdmin;
    
    @Column(name = "ciclo_escolar", nullable = false)
    private String schoolCycle;
    
	@Column(name = "f_registro")
	private Date registerDate;

	@Column(name = "f_modif")
	private Date editdate;
}
