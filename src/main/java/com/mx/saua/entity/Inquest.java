package com.mx.saua.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.mx.saua.enums.InquestStatusEnum;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Benito Morales
 * @version 2.0
 */
@Entity
@Table(name = "encuesta")
@Setter
@AllArgsConstructor
@Getter
@NoArgsConstructor
public class Inquest implements Serializable{

	/**
	 * Serializador de la clase
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@Column(name = "nombre")
	private String name;
	
	@Column(name = "descripcion")
	private String description;
	
	@Column(name = "num_preguntas")
	private int numberQuestions;
	
	@Column(name = "status")
	@Enumerated(EnumType.STRING)
	private InquestStatusEnum status;
	
	@Column(name = "f_creacion")
	private Date creationDate;
	
	@Column(name = "f_modif")
	private Date editDate;

	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "inquest")
//	@JsonIgnore
	private List<InquestQuestion> inquestQuestions;
	
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "inquest")
	private List<InquestRole> inquestRoles;

}
