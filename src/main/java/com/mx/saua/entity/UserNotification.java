package com.mx.saua.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.mx.saua.enums.NotificationStatusEnum;
import com.mx.saua.enums.NotificationTypeEnum;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "usuario_notificaciones")
@Setter
@AllArgsConstructor
@Getter
@NoArgsConstructor
public class UserNotification implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idNotificacion;
    
    @Column(name = "nombre_destinatario", nullable = false)
    private String addresseeName;
    
    @Column(name = "email_destinatario", nullable = false)
    private String addresseeMail;
    
    @Column(name = "plain_password", nullable = false)
    private String plainPassword;
    

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "usuario_id", nullable = true)
    private User user;
    
    @Column(name = "status", nullable = false)
    @Enumerated(EnumType.STRING)
    private NotificationStatusEnum status;
    
    @Column(name = "f_expiracion", nullable = true)
    @Temporal(TemporalType.DATE)
    private Date expirationDate;
    
    @Column(name = "tipo_notificacion", nullable = false)
    @Enumerated(EnumType.STRING)
    private NotificationTypeEnum type;
    
    @Column(name = "visto", nullable = false)
    private boolean reviewable;
  
}
