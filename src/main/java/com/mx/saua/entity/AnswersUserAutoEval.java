package com.mx.saua.entity;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Benito Morales
 * @version 2.0
 */
@Entity
@Table(name = "usuario_respuestas_autoeval")
@Setter
@AllArgsConstructor
@Getter
@NoArgsConstructor
public class AnswersUserAutoEval implements Serializable{

	/**
	 * Serializador de la clase
	 */
	private static final long serialVersionUID = 5845997584031773338L;
	
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "id_respuesta", nullable=true)
	private Answer answer;
	
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "id_usuario", nullable=false)
	private User user;
	
	@Column(name = "resp_personalizada", nullable = true)
	private String customizedAnswer;
	
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "id_pregunta", nullable=false)
	private Question question;
	
	@Column(name = "especificacion")
	private String specification;
	
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "carga_alumno_id", nullable=false)
	private StudentCharge studentCharge;
}
