package com.mx.saua.entity;

import javax.persistence.*;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;
/**
 * Entidad que permite guardar/consultar los horarios de cada clase ofertada
 * @author Victor Francisco Garcia
 * @version 2.0
 */
@Entity
@Table(name="horario")
@Setter
@AllArgsConstructor
@Getter
@NoArgsConstructor
public class Schedule implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = "idhorario", unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "aula")
    private String classroom;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "clase_ofertada_id", nullable=true)
    private OfferedClass offeredClass;

    @Column(name = "hora_inicio")
    private String startTime;

    @Column(name = "hora_fin")
    private String endTime;

    @Column(name = "dia")
    private String day;

    @Column(name = "f_modif")
    private Date editDate;

}
