package com.mx.saua.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name="tipo_uap")
@Setter
@AllArgsConstructor
@Getter
@NoArgsConstructor
public class UapType implements Serializable {
	/**
	 * Entidad que permite guardar los tipos de uaps
	 * @author Vic
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "id", unique = true, nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@Column(name = "categoria_optativa")
	private String category;
	
	@Column(name = "semestre")
	private String semester;
}
