package com.mx.saua.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 
 * @author Benito Morales Ramirez
 * @version 2.0
 */
@Entity
@Table(name = "config")
@Setter
@AllArgsConstructor
@Getter
@NoArgsConstructor
public class Configuration implements Serializable {

	/**
	 * Serial de la clase
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "config_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long idConfiguration;

	@Column(name = "config_key", nullable = false)
	private String key;

	@Column(name = "config_value", nullable = false)
	private String value;

	@Column(name = "config_desc", nullable = false)
	private String description;
	
	/**
	 * 1:String
	 * 2:Number
	 * 3:Date
	 */
	@Column(name = "config_type", nullable = false)
	private int type;
}
