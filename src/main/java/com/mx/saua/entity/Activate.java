package com.mx.saua.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * @author Victor Francisco
 * @version 1.0
 */
@Entity
@Table(name = "activacion")
@Setter
@AllArgsConstructor
@Getter
@NoArgsConstructor
public class Activate implements Serializable {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "usuario_id", nullable=false)
    private User user;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "periodo_id", nullable=false)
    private Period period;

    @Column(name = "respuesta")
    private String response;

    @Column(name = "f_registro")
    private Date registerDate;
}
