package com.mx.saua.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 
 * @author Victor FG
 * @version 2.0
 *
 */
@Entity
@Table(name = "prog_educativo")
@Setter
@AllArgsConstructor
@Getter
@NoArgsConstructor
public class Program implements Serializable{
	
	/**
	 * @author Vic
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
    @Column(name = "CVE_PLAN_ALUMNO")
	private String studentPlanKey;
	
    @Column(name = "NOMBRE_PLAN_ALUMNO")
    private String studentPlanName;
    
    @Column(name = "VRS_PLAN_ALUMNO")
    private String studentPlanVrs;
    
    @Column(name = "abr")
    private String abr;
    
    @Column(name = "observaciones")
    private String observations;
    
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "escuela_proc_id", nullable = false)
    private School school;
    
	@Column(name = "estatus_carga")
	private boolean estatusCarga;
    
	@Column(name = "f_registro")
	private Date registerDate;

	@Column(name = "f_modif")
	private Date editdate;
}
