package com.mx.saua.entity;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 
 * @author Benito Morales
 * @version 2.0
 */
@Entity
@Table(name = "grupo")
@Setter
@AllArgsConstructor
@Getter
@NoArgsConstructor
public class Group implements Serializable{

	/**
	 * Serial de la clase
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@Column(name = "nombre")
	private String name;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "jefe_grupo", nullable = true)
	private Student groupLeader;
	
	@Column(name = "ubicacion")
	private String location;
	
	@Column(name = "turno")
	private String turn;
	
	@Column(name = "tipo_grupo")
	private String groupType;
	
	@Column(name = "abr")
	private String abbreviation;
	
	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "prog_educativo_CVE_PLAN_ALUMNO", nullable = false)
	private Program educationalProgram;
	
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_periodo", nullable=false)
	private Period period;
	
	@Column(name = "prox_grupo")
	private String nextGroup;
	
	@Column(name = "promovido")
	private boolean promoted;
	
}
