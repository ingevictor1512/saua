package com.mx.saua.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.mx.saua.enums.DayEnum;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@ToString
@Entity
@Table(name = "registro_entradas")
public class EntryRegister implements Serializable{
	
	/**
	 * Serial de la clase
	 */
	private static final long serialVersionUID = -3707873575124815127L;
	
	@Id
	@Column(name = "id", unique = true, nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long registerId;
	
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    @JoinColumn(name = "usuario_id", nullable=true)
	private User userStudent;
	
	@Column(name = "fecha")
	private Date datee;
	
	@Column(name = "dia")
	@Enumerated(EnumType.STRING)
	private DayEnum dayEnum;
	
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    @JoinColumn(name = "usuario_id_cajero", nullable=true)
	private User userCashier;
	
	@Column(name = "f_registro")
	@Temporal(TemporalType.TIMESTAMP)
	private Date registerDate;
	
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    @JoinColumn(name = "periodo_id", nullable=true)
	private Period period;
	
	@Column(name = "fecha_string")
	private String dateString;
	
	@Column(name = "cafeteria_id")
	private int coffeeShopId;
	
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    @JoinColumn(name = "invitado_id", nullable=true)
	private Guest guest;
}
