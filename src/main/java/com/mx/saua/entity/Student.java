package com.mx.saua.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.mx.saua.dto.PatternDto;
import com.mx.saua.enums.GenderEnum;
import com.sun.istack.NotNull;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 
 * @author Benito Morales
 * @version 1.0
 *
 */
@Entity
@Table(name = "alumno")
@Setter
@AllArgsConstructor
@Getter
@NoArgsConstructor
public class Student extends PatternDto implements Serializable {

	/**
	 * Serial de la clase
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "matricula")
	@NotNull
	private String register;

	@Column(name = "paterno")
	private String lastname;

	@Column(name = "materno")
	private String secondLastname;

	@Column(name = "nombre")
	private String name;

	@Column
	private String curp;

	@Column(name = "semestre")
	private int semester;

	@Column(name = "generacion")
	private String generation;
	
	@Column(name = "genero")
	@Enumerated(EnumType.STRING)
	private GenderEnum gender;

	@Column(name = "f_registro")
	private Date registerDate;

	@Column(name = "f_modif")
	private Date editDate;
	
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "student")
	private Set<User> users;
	
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "status_alumno_id", nullable=true)
	private StudentStatus studentStatus;
	
	@ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "tipo_ingreso_id", nullable=true)
	private Income income;

	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "grupo_id", nullable=true)
	private Group group;
	
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "pe_CVE_PLAN_ALUMNO", nullable=true)
	private Program program;
	
	@Column(name = "total_auto_eval")
	private int totalAutoEval;
	
	@Column(name = "total_eval_pfr")
	private int totalTeacherEval;
	
}
