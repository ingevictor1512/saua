package com.mx.saua.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 
 * @author Vic
 * @version 1.0
 */
@Entity
@Table(name = "uap")
@Setter
@AllArgsConstructor
@Getter
@NoArgsConstructor
public class Uap implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1361937184191427718L;

	@Id
	@Column(name = "clave", nullable = false)
	private String key;
	
	@Column(name = "nombre")
	private String name;
	
	@Column(name = "creditos")
	private long credit;
	
	@Column(name = "codigo_comun")
	private String common_code;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "pe_CVE_PLAN_ALUMNO", nullable = false)
	private Program educationalProgram;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "tipo_uap_id", nullable = false)
	private UapType uapType;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "etapas_uap_id", nullable = false)
	private Phase phaseUaps;
	
	@Column(name = "f_registro")
	private Date registerDate;

	@Column(name = "f_modif")
	private Date editdate;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "uap")
	private List<OfferedClass> offeredClasses;
	
}
