package com.mx.saua.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 
 * @author Benito Morales
 * @version 2.0
 */
@Entity
@Table(name = "usuario")
@Setter
@AllArgsConstructor
@Getter
@ToString
public class User implements Serializable {

	/**
	 * Serial de la clase
	 */
	private static final long serialVersionUID = 1562402792652842098L;

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long idUser;

	@Column
	private String username;

	@Column
	private String password;

	@Column
	private String email;

	@Column(name = "activo")
	private boolean active;

	@Column(name = "bloqueo")
	private boolean locked;

	@Column(name = "status")
	private boolean status;

	@Column(name = "f_registro")
	@Temporal(TemporalType.TIMESTAMP)
	private Date registerDate;

	@Column(name = "f_modif")
	@Temporal(TemporalType.TIMESTAMP)
	private Date editdate;

	@Column(name = "f_expiracion")
	@Temporal(TemporalType.DATE)
	private Date expirationDate;
	
	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "user")
	private Set<Authority> authorities;

	@Column(name = "primer_login")
	private boolean firstLogin;
	
	@Column(name = "ultimo_login")
	private Date lastLogin;
	
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "alumno_matricula", nullable=true)
	private Student student;
	
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "profesor_id", nullable=true)
	private Teacher teacher;
	
	@Column(name = "encuesta_activa")
	private boolean inquest;
	
	@Column(name = "telefono")
	private String phone;
	
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "user")
	private Set<AnswersUser> answers;
	
	public User() {
		authorities = new HashSet<>();
	}

}
