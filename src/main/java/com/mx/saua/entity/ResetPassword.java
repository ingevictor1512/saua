package com.mx.saua.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 
 * @author Benito Morales
 * @version 2.0
 *
 */
@Entity
@Table(name = "reset_password")
@Setter
@AllArgsConstructor
@Getter
@NoArgsConstructor
public class ResetPassword {

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long idReset;
	
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "usuario_id", nullable=false)
	private User user;
	
	@Column(name = "f_registro")
	private Date created;
	
	@Column(name = "hash_value")
	private String hashValue;
	
	@Column(name = "uso")
	private boolean used;
	
}
