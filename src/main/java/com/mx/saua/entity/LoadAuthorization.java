package com.mx.saua.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
/**
 * @author Vic
 * @version 2.0
 */
@Entity
@Table(name="autorizacion_carga")
@Setter
@AllArgsConstructor
@Getter
@NoArgsConstructor
public class LoadAuthorization implements Serializable {
	/**
	 * Entidad que permite guardar el estatus de las cargas academicas
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id", unique = true, nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@Column(name = "clave_aut")
	private String authorizationKey;
	
	@Column(name = "autorizacion")
	private boolean authorization;
	
	@Column(name = "observaciones")
	private String observations;
	
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "periodo_id", nullable=false)
	private Period period;
    
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "alumno_matricula", nullable = false)
    private Student student;
	
	@Column(name = "f_registro")
	private Date registerDate;

	@Column(name = "f_modif")
	private Date editdate;

}
