package com.mx.saua.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Entity
@Table(name = "cafeteria")
public class CoffeeShop implements Serializable {
	
    private static final long serialVersionUID = 1L;

	@Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "nombre")
    private String name;

    @Column(name = "numero_becas")
    private Long grantNumber;

    @Column(name = "responsable")
    private String manager;

    @Column(name = "ubicacion")
    private String location;

    @Column(name = "f_registro")
    private Date registerDate;

    @Column(name = "f_modif")
    private Date editDate;
}
