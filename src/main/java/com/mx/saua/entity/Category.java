package com.mx.saua.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Entidad Category
 * @author Victor
 * @version 2.0
 *
 */
@Entity
@Table(name="categoria_profesor")
@Setter
@AllArgsConstructor
@Getter
@NoArgsConstructor
public class Category implements Serializable{
	/**
	 * @author Vic
	 */
	private static final long serialVersionUID = 1L;

	/**Entidad que permite guardar las categorias de los maestros
	 * 
	 */
	@Id
	@Column(name = "id", unique = true, nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@Column(name = "descripcion")
	private String description;

}
