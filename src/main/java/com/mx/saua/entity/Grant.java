package com.mx.saua.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Entity
@Table(name = "beca")
public class Grant implements Serializable{

    /**
	 * 
	 */
	private static final long serialVersionUID = 9066517230202685693L;

	@Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "nombre")
    private String name;

    @Column(name = "Descripcion")
    private String description;
    
	@Column(name = "f_registro")
	@Temporal(TemporalType.TIMESTAMP)
	private Date registerDate;

	@Column(name = "f_edicion")
	@Temporal(TemporalType.TIMESTAMP)
	private Date editdate;

}
