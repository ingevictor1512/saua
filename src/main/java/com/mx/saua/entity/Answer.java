package com.mx.saua.entity;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Benito Morales
 * @version 2.0
 */
@Entity
@Table(name = "respuestas")
@Setter
@AllArgsConstructor
@Getter
@NoArgsConstructor
public class Answer implements Serializable{

	/**
	 * Serializador de la clase
	 */
	private static final long serialVersionUID = 3258577837414464714L;

	@Id
	@Column
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "id_pregunta", nullable=true)
	private Question question;
	
	@Column(name = "texto")
	private String text;
	
	@Column
	private boolean checked;
	
	@Column(name = "especificable")
	private boolean specify;
	
	@Column(name = "orden")
	private int order;
}
