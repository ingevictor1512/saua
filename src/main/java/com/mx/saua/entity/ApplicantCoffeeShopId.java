package com.mx.saua.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Embeddable
@ToString
public class ApplicantCoffeeShopId implements Serializable{

	private static final long serialVersionUID = 8699532595943270128L;
	
    @Column(name = "postulado_id", nullable=false)
    private Long applicantId;
    
    @Column(name = "cafeteria_id", nullable=false)
    private int coffeeShopId;
    
    @Column(name = "fecha_uso", nullable=false)
    private String usedate;
	
}
