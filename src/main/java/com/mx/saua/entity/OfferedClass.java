package com.mx.saua.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.mx.saua.dto.PatternDto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 
 * @author Benito Morales
 * @version 1.0
 */
@Entity
@Table(name = "clase_ofertada")
@Setter
@AllArgsConstructor
@Getter
@NoArgsConstructor
public class OfferedClass extends PatternDto implements Serializable{

	/**
	 * Serial de la clase
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "periodo_id", nullable=false)
	private Period period;
	
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "profesor_id", nullable=false)
	private Teacher teacher;
	
	@Column(name = "carga_uap")
	private int uapCharge;
	
	@Column(name = "semestre")
	private int semester;
	
	@Column(name = "carga_maxima")
	private int maxCharge;
	
	@Column(name = "carga_minima")
	private int minCharge;
	
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "uap_clave", nullable=false)
	private Uap uap;
	
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "grupo_id", nullable=false)
	private Group group;
	
}
