package com.mx.saua.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 
 * @author Victor FG
 * @version 2.0
 *
 */
@Entity
@Table(name="etapas_uap")
@Setter
@AllArgsConstructor
@Getter
@NoArgsConstructor
public class Phase implements Serializable{
	/**
	 * @author Vic
	 */
	private static final long serialVersionUID = 1L;

	/**Entidad que permite guardar las etapas de las uaps
	 * 
	 */
	@Id
	@Column(name = "id", unique = true, nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@Column(name = "descripcion")
	private String description;
	
	@Column(name = "abr")
	private String abbreviation;

}
