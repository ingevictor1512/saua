package com.mx.saua.constant;

import java.text.SimpleDateFormat;

public class FormatsConstant {
	
	public static SimpleDateFormat SDF_DDMMYYYY = new SimpleDateFormat("dd/MM/yyyy");
	
	public static SimpleDateFormat DD = new SimpleDateFormat("dd");
	
	public static SimpleDateFormat SDF_YYYYMMDD = new SimpleDateFormat("yyyy-MM-dd");
	
	public static SimpleDateFormat SDF_DDMMYYYY_KKMMSS = new SimpleDateFormat("dd-MM-yyyy kk:mm:ss");
}
