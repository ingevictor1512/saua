package com.mx.saua.constant;

public class ViewConstant {
	
	public static final String INDEX = "index";
	public static final String REDIRECT_HOME = "redirect:/home";
	public static final String NO_ENCONTRADA = "error/404";
	public static final String LOGIN = "login";
	public static final String REDIRECT_LOGIN = "redirect:login";
	public static final String MESSAGE = "message";
	public static final String ERROR_MESSAGE = "errorMessage";
	public static final String REDIRECT_USERS_LIST = "redirect:/admin/users";
	public static final String ADD_USER = "admin/user/add";
	public static final String EDIT_USER = "admin/usuarios/edit";
	public static final String USER_LIST = "admin/user/list";
	public static final String RESET_PASSWORD = "admin/user/reset_password";
	public static final String GENERAL_INQUEST = "general_inquest";
	/* Profesores*/
	public static final String TEACHER_DASHBOARD = "teacher/dashboard";
	/* Escuelas */
	public static final String ADD_SCHOOL = "admin/school/add";
	public static final String SCHOOL_LIST = "admin/school/list";
	public static final String REDIRECT_SCHOOL_LIST = "redirect:/admin/school";
	/* Etapas de las uaps */
	public static final String ADD_PHASE = "admin/phase/add";
	public static final String PHASE_LIST = "admin/phase/list";
	public static final String REDIRECT_PHASE_LIST = "redirect:/admin/phase";
	/* Tipos de uaps */
	public static final String ADD_TYPES = "admin/types/add";
	public static final String TYPES_LIST = "admin/types/list";
	public static final String REDIRECT_TYPES_LIST = "redirect:/admin/types";
	/* Tipos de ingreso */
	public static final String ADD_INCOME = "admin/income/add";
	public static final String INCOME_LIST = "admin/income/list";
	public static final String REDIRECT_INCOME_LIST = "redirect:/admin/income";
	/* Categorias de maestros */
	public static final String ADD_CATEGORY = "admin/category/add";
	public static final String CATEGORY_LIST = "admin/category/list";
	public static final String REDIRECT_CATEGORY_LIST = "redirect:/admin/category";

	/* Periodos */
	public static final String ADD_PERIOD = "admin/period/add";
	public static final String PERIOD_LIST = "admin/period/list";
	public static final String REDIRECT_PERIOD_LIST = "redirect:/admin/period";
	
	/* Configuraciones */
	public static final String EDIT_CONFIG = "admin/configuration/edit";
	public static final String CONFIG_LIST = "admin/configuration/list";
	public static final String REDIRECT_CONFIG_LIST = "redirect:/admin/config";

	/* Main */
	public static final String REGISTER = "/register";
	public static final String FAQ = "/faq";
	public static final String PROFILE = "profile";
	public static final String ADMIN_DASHBOARD = "admin/dashboard";
	public static final String STUDENT_DASHBOARD = "student/dashboard";
//	public static final String CHANGE_PASSWORD_ = "changePassword";
	public static final String INQUEST = "inquest";
	public static final String INQUEST_LIST = "admin/inquest/list";
	public static final String REDIRECT_INQUEST_LIST = "redirect:/admin/inquest/";
	public static final String ADD_INQUEST = "admin/inquest/add";
	public static final String INQUEST_RESULT = "admin/inquest/result";
	public static final String TUTORING_COORDINATOR_DASHBOARD = "coordinator/dashboard";
	public static final String TUTOR_DASHBOARD = "tutor/dashboard";
	/* Reset password*/
	public static final String CHANGE_PASSWORD_VIEW = "changepassword";
	public static final String RESET_PASSWORD_VIEW = "resetpassword";
	public static final String NEW_PASSWORD_VIEW = "newpassword";
	public static final String REDIRECT_RESET_PASSWORD = "redirect:/chkhash?h=";
	public static final String INQUEST_LIST_USER = "inquest_list";
	/* Alumno */
	public static final String STUDENT_LIST = "admin/student/list";
	public static final String STUDENT_LIST_PAGE = "admin/student/list-page";
	public static final String ADD_STUDENT = "admin/student/add";
	public static final String ADD_CHARGE = "admin/student/add_charge";
	public static final String REDIRECT_ADD_CHARGE = "redirect:/admin/students/list";
	public static final String STUDENT_ADD_CHARGE = "student/add_charge";
	public static final String REDIRECT_STUDENT_CHARGE = "redirect:/student/academiccharge";
	public static final String REDIRECT_STUDEN_LIST = "redirect:/student/academiccharge";
	public static final String STUDENT_DOCUMENTS_LIST = "student/documents/list";
	public static final String STUDENT_LIST_PAPELETAS = "admin/papeletas/list";
	public static final String STUDENT_LIST_PAPELETASMASIVO = "admin/papeletas/listado";
	public static final String EVAL_DOCENT_STUDENT = "student/inquest/list_evaluations";
	public static final String SCHOOL_TRACKING_STUDENT = "student/schooltracking/list";
	public static final String TEACHER_EVALUATION = "student/inquest/teacher_evaluation";
	public static final String AUTO_EVALUATION = "student/inquest/auto_evaluation";
	/* Estatus del Alumno */
	public static final String STUDENTSTATUS_LIST = "admin/statusstudent/list";
	public static final String ADD_STUDENTSTATUS = "admin/statusstudent/add";
	public static final String REDIRECT_STUDENTSTATUS_LIST = "redirect:/admin/statusstudent";
	public static final String REDIRECT_STUDENT_LIST = "redirect:/admin/students";
	/* Grupos */
	public static final String GROUP_LIST = "admin/group/list";
	public static final String ADD_GROUP = "admin/group/add";
	public static final String REDIRECT_GROUP_LIST = "redirect:/admin/groups";
	public static final String REDIRECT_ADD_GROUP = "redirect:/admin/groups/add";
	/* Uaps */
	public static final String UAPS_LIST = "admin/uaps/list";
	public static final String ADD_UAPS = "admin/uaps/add";
	public static final String REDIRECT_UAPS_LIST = "redirect:/admin/uaps";
	public static final String REDIRECT_ADD_UAPS = "redirect:/admin/uaps/add";
	/* Uaps */
	public static final String TEACHER_LIST = "admin/teacher/list";
	public static final String ADD_TEACHER = "admin/teacher/add";
	public static final String REDIRECT_TEACHER_LIST = "redirect:/admin/teachers";
	public static final String REDIRECT_ADD = "redirect:/admin/teachers/add";
	/* Clases ofertadas */
	public static final String OFFEREDCLASS_LIST = "admin/offeredclass/list";
	public static final String OFFEREDCLASS_LIST_PAGE = "admin/offeredclass/list-page";
	public static final String ADD_OFFEREDCLASS = "admin/offeredclass/add";
	public static final String REDIRECT_OFFEREDCLASS_LIST = "redirect:/admin/offeredclass";
	public static final String REDIRECT_ADD_OFFEREDCLASS = "redirect:/admin/offeredclass/add";
	/* Bajas tutorias del alumno */
	public static final String TUTORINGLOW_LIST = "admin/tutoringlows/list";
	public static final String ADD_TUTORINGLOW = "admin/tutoringlows/add";
	public static final String REDIRECT_TUTORINGLOW_LIST = "redirect:/admin/tutoringlows";
	public static final String REDIRECT_ADD_TUTORINGLOW = "redirect:/admin/tutoringlows/add";
	/* Tutorias */
	public static final String TUTORSHIP_LIST = "admin/tutorships/list";
	public static final String ADD_TUTORSHIP = "admin/tutorships/add";
	public static final String REDIRECT_TUTORSHIP_LIST = "redirect:/admin/tutorships";
	public static final String REDIRECT_ADD_TUTORSHIP = "redirect:/admin/tutorships/add";
	/* Programas educativos */
	public static final String PROGRAM_LIST = "admin/programs/list";
	public static final String ADD_PROGRAM = "admin/programs/add";
	public static final String REDIRECT_PROGRAM_LIST = "redirect:/admin/programs";
	public static final String REDIRECT_ADD_PROGRAM = "redirect:/admin/programs/add";
	/* Tutorados */
	public static final String TUTORED_LIST = "tutor/tutored/list";
	public static final String ADD_TUTORED = "tutor/removetutored/add";
	public static final String REDIRECT_TUTORED_LIST = "redirect:/tutor/tutoreds";
	public static final String REDIRECT_ADD_TUTORED = "redirect:/tutor/tutoreds/add";
	public static final String REDIRECT_KARDEX_TUTORED = "tutor/kardex/list";
	public static final String REDIRECT_LOAD_TUTORED = "tutor/load/list";
	public static final String REDIRECT_AUTHORIZATION_LIST = "tutor/load/show";
	public static final String TUTOR_DOCUMENT_LIST = "tutor/documents/list";
	/* Coordinador de Tutorias */
	public static final String TUTORSHIP_COORDINATOR_LIST = "coordinator/list";
	public static final String ADD_TUTORSHIP_COORDINATOR = "coordinator/add";
	public static final String REDIRECT_TUTORSHIP_LIST_COORDINATOR = "redirect:/coordinator/tutorships";
	public static final String REDIRECT_ADD_TUTORSHIP_COORDINATOR = "redirect:/admin/tutorships/add";
	public static final String TUTORSHIP_COORDINATOR_DOCUMENTS_LIST = "coordinator/documents/list";
	public static final String TUTORSHIP_COORDINATOR_ADD_DOCUMENT = "coordinator/documents/add";
	public static final String TUTORSHIP_COORDINATOR_REDIRECT_ADD_DOCUMENT = "redirect:/coordinator/tutorships/adddocument";
	public static final String TUTORSHIP_COORDINATOR_REDIRECT_LIST_DOCUMENTS = "redirect:/coordinator/tutorships/listdocuments";
	public static final String REDIRECT_STUDENTCHARGE_LIST = "coordinator/show";
	/* Documentos */
	public static final String DOCUMENTS_LIST = "admin/documents/list";
	public static final String ADD_DOCUMENT = "admin/documents/add";
	public static final String REDIRECT_DOCUMENTS_LIST = "redirect:/admin/documents/list";
	public static final String REDIRECT_ADD_DOCUMENT = "redirect:/admin/documents/add";
	/* Asignar uaps a un grupo */
	public static final String ASSIGN_LIST = "admin/uapsassign/list";
	//public static final String ADD_DOCUMENT = "admin/documents/add";
	//public static final String REDIRECT_DOCUMENTS_LIST = "redirect:/admin/documents/list";
	//public static final String REDIRECT_ADD_DOCUMENT = "redirect:/admin/documents/add";
	
	/*Promover*/
	public static final String PROMOTE = "admin/promote/promote";
	
	/*Reset password*/
	public static final String RESET_PASSWORD_LIST = "admin/reset/list-page";
	public static final String REDIRECT_RESET_PASSWORD_LIST = "redirect:/admin/resetpassword";
	public static final String RESET_PASSWORD_EDIT = "admin/reset/edit";
	/* Preguntas */
	public static final String QUESTION_LIST = "admin/question/list";
	public static final String ADD_QUESTION = "admin/question/add";
	public static final String REDIRECT_QUESTION_LIST = "redirect:/admin/question/list";
	//public static final String ADD_PROGRAM = "admin/programs/add";
	//public static final String REDIRECT_PROGRAM_LIST = "redirect:/admin/programs";
	//public static final String REDIRECT_ADD_PROGRAM = "redirect:/admin/programs/add";
	/* Informacion de evaluaciones */
	public static final String ADD_INFOEVAL = "admin/infoevaluations/add";
	public static final String EDIT_INFOEVAL = "admin/infoevaluations/edit";
	public static final String INFOEVAL_LIST = "admin/infoevaluations/list";
	public static final String REDIRECT_INFOEVAL_LIST = "redirect:/admin/infoeval";
	public static final String INFOEVAL_LIST_REPORT = "admin/infoevaluations/list_evaluations";
		/* Becas */
	public static final String GRANT_LIST = "admin/grant/list";
	public static final String ADD_GRANT = "admin/grant/add";
	public static final String REDIRECT_GRANT_LIST = "redirect:/admin/grant";
	public static final String REDIRECT_ADD_GRANT = "redirect:/admin/grant/add";
	/* CRUD Cafeterias  */
	public static final String ADD_COFFEESHOP = "admin/coffeeshop/add";
	public static final String COFFEESHOP_LIST = "admin/coffeeshop/list";
	public static final String REDIRECT_COFFEESHOP_LIST = "redirect:/admin/coffeeshop";
	/* CRUD candidatos  */
	public static final String ADD_APPLICANT_STUDENT = "admin/applicant/add";
	public static final String ADD_APPLICANT_GUEST = "admin/guest/add";
	public static final String REDIRECT_APPLICANT_GUEST = "redirect:/admin/applicant/addguest";
	public static final String EDIT_APPLICANT = "admin/applicant/edit";
	public static final String ASSIGN_APPLICANT = "admin/applicant/assign";
	public static final String APPLICANT_LIST = "admin/applicant/list";
	public static final String REDIRECT_APPLICANT_LIST = "redirect:/admin/applicant";
	public static final String STUDENT_DAYSGRANT_LIST = "student/grant/list";
	//Cajero
	public static final String CASHIER_DASHBOARD = "cajero/dashboard";
}
