package com.mx.saua.config;

import java.util.concurrent.atomic.AtomicInteger;

import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.mx.saua.exception.ServiceException;
import com.mx.saua.service.impl.UserSecurityService;

@WebListener
public class CustomSessionListner implements HttpSessionListener {

	private static final Logger LOG= LoggerFactory.getLogger(CustomSessionListner.class);

    private final AtomicInteger counter = new AtomicInteger();
    
    @Override
    public void sessionCreated(HttpSessionEvent se) {
//        LOG.info("New session is created. Adding Session to the counter.");
        counter.incrementAndGet();  //incrementing the counter
        updateSessionCounter(se);
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent se) {
//        LOG.info("{} - Session destruida - USER:{}");
        counter.decrementAndGet();  //decrementing counter
        updateSessionCounter(se);
//        try {
//			userService.updateLogout();
//		} catch (ServiceException e) {
//			LOG.error("Error al actualizar logout:{}",e.getMessage());
//		}
    }

    private void updateSessionCounter(HttpSessionEvent httpSessionEvent){
        httpSessionEvent.getSession().getServletContext()
                .setAttribute("activeSession", counter.get());
//        LOG.info("Total de sesiones activas:{} ",counter.get());
    }
	
}
