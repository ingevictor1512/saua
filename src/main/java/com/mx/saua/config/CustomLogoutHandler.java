package com.mx.saua.config;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import com.mx.saua.exception.ServiceException;
import com.mx.saua.service.impl.UserSecurityService;

@Service
public class CustomLogoutHandler implements org.springframework.security.web.authentication.logout.LogoutHandler{

	Logger log = LoggerFactory.getLogger(CustomLogoutHandler.class);
	
	@Autowired
	private UserSecurityService userService;
	
	@Override
	public void logout(HttpServletRequest request, HttpServletResponse response, Authentication authentication) {
		try {
			userService.updateLogout();
		} catch (ServiceException e) {
			log.info("Ocurrió un error al actualizar fecha de cierre de sessión.");
		}
	}

}
