package com.mx.saua.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.csrf.CsrfFilter;
import org.springframework.web.filter.CharacterEncodingFilter;

import com.mx.saua.service.impl.UserSecurityService;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	private Logger logger = LoggerFactory.getLogger(WebSecurityConfig.class);

	/**
	 * passwordEncoder
	 */
	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	private CustomLogoutHandler logoutHandler;
	
	public enum RollType {
		ADMIN("ADMIN"), STUDENT("STUDENT"), COOORD_TUTOR("TUTORING_COORDINATOR"), TUTOR("TUTOR");

		private String rollName;

		private RollType(String rollName) {
			this.rollName = rollName;
		}

		public String getRollName() {
			return rollName;
		}
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		try {
			http.httpBasic().disable();

			CharacterEncodingFilter filter = new CharacterEncodingFilter();
			filter.setEncoding("UTF-8");
			filter.setForceEncoding(true);
			http.addFilterBefore(filter, CsrfFilter.class);

			http.authorizeRequests().antMatchers("/css/**", "/fonts/**", "/images/**", "/js/**", "/favicon.ico",
					"/webjars/**", "/directLogin/**", "/vendors/**").permitAll()
				.antMatchers("/admin/**").hasAuthority("ADMIN")	
				.antMatchers("/student/**").hasAuthority("STUDENT")
				.antMatchers("/tutoringcoordinator/**").hasAuthority("TUTORING_COORDINATOR")
				.antMatchers("/tutor/**").hasAuthority("TUTOR")
				.antMatchers("/api/student/**").hasAuthority("STUDENT")
				.antMatchers("/api/admin/**").hasAuthority("ADMIN")
				.antMatchers("/api/coordinator/**").hasAuthority("TUTORING_COORDINATOR")
				.antMatchers("/api/any/**").hasAnyAuthority("ADMIN", "STUDENT", "TUTORING_COORDINATOR", "TUTOR")
				.antMatchers("/api/tutor/**").hasAuthority("TUTOR")
//				.antMatchers("/").hasAnyAuthority("ADMIN", "STUDENT", "TUTORING_COORDINATOR", "TUTOR")
					.anyRequest()
					.authenticated()
					.and()
					.formLogin()
					.loginPage("/login").permitAll()
					.defaultSuccessUrl("/home", true)
					.failureUrl("/login?error")
					.and().logout()
					//.logoutUrl("/logout")
					.addLogoutHandler(logoutHandler)
					.deleteCookies("remember-me")
					.logoutSuccessUrl("/login?logout")
					.and().sessionManagement().maximumSessions(1);
		} catch (Exception e) {
			logger.error("WebSecurityConfing.configure", e);
		}
	}

	@Override
	public void configure(WebSecurity web) {
		web.ignoring().antMatchers("/resetp")
				.antMatchers("/register")
				.antMatchers("/faq")
				.antMatchers("/resetPassword")
				.antMatchers("/chkhash")
				.antMatchers("/changepassword");
	}

	/**
	 * 
	 * @param auth
	 * @param userService
	 */
	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth, UserSecurityService userService) {
		try {
			auth.userDetailsService(userService).passwordEncoder(passwordEncoder);
		} catch (Exception e) {
			logger.error("WebSecurityConfig.configureGlobal", e);
		}
	}

	/**
	 * Principal password encoder.
	 * 
	 * @return PasswordEncoder of the application.
	 */
	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}
}
