package com.mx.saua.dto;

import java.io.Serializable;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Setter
@AllArgsConstructor
@Getter
@NoArgsConstructor
@ToString
@Builder
@Data
public class GuestDto implements Serializable{

	/**
	 * Serial de la clase
	 */
	private static final long serialVersionUID = 1L;

	private String idRegister;

	private String lastname;

	private String secondLastname;

	private String name;
	
	private String origin;
	
	private List<GrantDto> grants;
	
	private boolean newRegister;
	
	private long grantId;
	
	private boolean statusGrant;
	
	private long applicantId;
	
}
