package com.mx.saua.dto;

import java.util.List;

/**
 * Dto auxiliar para la paginación
 * @author benito Morales
 * @version 1.0
 */
public abstract class PatternDto {

	protected int page;
	protected int size;
	protected int totalPages;
	protected long totalItems;
	protected String direction;
	protected List<String> categoriesDesc;

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public int getTotalPages() {
		return totalPages;
	}

	public void setTotalPages(int totalPages) {
		this.totalPages = totalPages;
	}

	public long getTotalItems() {
		return totalItems;
	}

	public void setTotalItems(long totalItems) {
		this.totalItems = totalItems;
	}

	public String getDirection() {
		return direction;
	}

	public void setDirection(String direction) {
		this.direction = direction;
	}

	public List<String> getCategoriesDesc() {
		return categoriesDesc;
	}

	public void setCategoriesDesc(List<String> categoriesDesc) {
		this.categoriesDesc = categoriesDesc;
	}
	
	
}
