package com.mx.saua.dto;

import java.io.Serializable;
import java.util.Date;

public class SchoolDto implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**DTO que permite guardar la informacion de las escuelas
	 * 
	 * @Vic
	 */
	private long id;
	private String name;
	private String address;
	private String phone;
	private String mail;
	private Date registerDate;
	private Date editdate;
	
	public SchoolDto() {
		this.id=-1L;
	}

	public SchoolDto(long id, String name, String address, String phone, String mail, Date registerDate,
			Date editdate) {
		super();
		this.id = id;
		this.name = name;
		this.address = address;
		this.phone = phone;
		this.mail = mail;
		this.registerDate = registerDate;
		this.editdate = editdate;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public Date getRegisterDate() {
		return registerDate;
	}

	public void setRegisterDate(Date registerDate) {
		this.registerDate = registerDate;
	}

	public Date getEditdate() {
		return editdate;
	}

	public void setEditdate(Date editdate) {
		this.editdate = editdate;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "SchoolDto [id=" + id + ", name=" + name + ", address=" + address + ", phone=" + phone + ", mail=" + mail
				+ ", registerDate=" + registerDate + ", editdate=" + editdate + "]";
	}
	
	
	
}
