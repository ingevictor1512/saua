package com.mx.saua.dto;

import java.io.Serializable;

public class ResultDto  implements Serializable{
	/**
	 * Serial de la clase
	 */
	private static final long serialVersionUID = 1L;
	private long id;
	private String name;
	private double score;
	
	public ResultDto() {

	}

	public ResultDto(long id, String name, float score) {
		super();
		this.id = id;
		this.name = name;
		this.score = score;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getScore() {
		return score;
	}

	public void setScore(double score) {
		this.score = score;
	}

	
	

}
