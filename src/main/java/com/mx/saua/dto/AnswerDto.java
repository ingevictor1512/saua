package com.mx.saua.dto;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Benito Morales
 * @version 1.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AnswerDto implements Serializable{

	/**
	 * Serializador de la clase
	 */
	private static final long serialVersionUID = 3258577837414464714L;

	private long id;
	private QuestionDto question;
	private String text;
	private boolean checked;
	private boolean specify;
	
}
