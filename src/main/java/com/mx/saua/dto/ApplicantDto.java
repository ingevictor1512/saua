package com.mx.saua.dto;

import com.mx.saua.entity.Grant;
import com.mx.saua.entity.Period;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.List;

@Setter
@Getter
@ToString
public class ApplicantDto implements Serializable {
	
    private static final long serialVersionUID = 1L;
	private long id;
    private String applicant;
    private Grant grant;
    private Period period;
    private boolean statusGrant;
    private String idsStudentSelectedString;
    private boolean newRegister;
    private List<ApplicantDto> applicantDtoList;
    private long grantId;
    private List<GrantDto> grantsDto;
    private List<CoffeeShopDto> coffeeShops;
    private String data;
    private String register;
    private List<ApplicantCoffeeShopDto> applicantCoffeeShopDtos;
    private String slctdate;
    private boolean guest;
    private GuestDto guestDto;

    public ApplicantDto() {
        this.id=-1L;
    }

    public ApplicantDto(long id, String applicant, Grant grant, Period period, boolean statusGrant, String register, List<ApplicantCoffeeShopDto> applicantCoffeeShopDtos) {
        this.id = id;
        this.applicant = applicant;
        this.grant = grant;
        this.period = period;
        this.statusGrant = statusGrant;
        this.register = register;
        this.applicantCoffeeShopDtos = applicantCoffeeShopDtos;
    }
}

