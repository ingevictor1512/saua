package com.mx.saua.dto;

import java.io.Serializable;

public class PhaseDto implements Serializable {
	/**
	 * DTO que permite guardar la informacion de las etapas de las uaps
	 * @author Vic
	 */
	private static final long serialVersionUID = 1L;
	
	private long id;
	private String description;
	private String abbreviation;
	
	public PhaseDto() {
		this.id=-1L;
	}

	public PhaseDto(long id, String description, String abbreviation) {
		this.id = id;
		this.description = description;
		this.abbreviation = abbreviation;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getAbbreviation() {
		return abbreviation;
	}

	public void setAbbreviation(String abbreviation) {
		this.abbreviation = abbreviation;
	}

	@Override
	public String toString() {
		return "PhaseDto [id=" + id + ", description=" + description + ", abbreviation=" + abbreviation + "]";
	}
	
	
}
