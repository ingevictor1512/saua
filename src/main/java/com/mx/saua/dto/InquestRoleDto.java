package com.mx.saua.dto;

import java.io.Serializable;
import java.util.Date;

import com.mx.saua.enums.InquestStatusEnum;

public class InquestRoleDto implements Serializable{

	/**
	 * Serial de la clase
	 */
	private static final long serialVersionUID = -4555803451152149912L;
	
	private long id;
	private InquestDto inquest;
	private String role;
	private Date inquestInitDate;
	private Date inquestEndDate;
	private InquestStatusEnum inquestStatusEnum;
	
	public InquestRoleDto() {
		
	}
	
	public InquestRoleDto(long id, InquestDto inquest, String role, Date inquestInitDate, Date inquestEndDate,
			InquestStatusEnum inquestStatusEnum) {
		super();
		this.id = id;
		this.inquest = inquest;
		this.role = role;
		this.inquestInitDate = inquestInitDate;
		this.inquestEndDate = inquestEndDate;
		this.inquestStatusEnum = inquestStatusEnum;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public InquestDto getInquest() {
		return inquest;
	}
	public void setInquest(InquestDto inquest) {
		this.inquest = inquest;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public Date getInquestInitDate() {
		return inquestInitDate;
	}
	public void setInquestInitDate(Date inquestInitDate) {
		this.inquestInitDate = inquestInitDate;
	}
	public Date getInquestEndDate() {
		return inquestEndDate;
	}
	public void setInquestEndDate(Date inquestEndDate) {
		this.inquestEndDate = inquestEndDate;
	}
	public InquestStatusEnum getInquestStatusEnum() {
		return inquestStatusEnum;
	}
	public void setInquestStatusEnum(InquestStatusEnum inquestStatusEnum) {
		this.inquestStatusEnum = inquestStatusEnum;
	}
	
	
}
