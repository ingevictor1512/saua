package com.mx.saua.dto;

import java.io.Serializable;

public class UapTypeDto implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/*
	 * DTO que permite guardar la informacion de los tipos de uaps
	 * @Vic
	 */
	private long id;
	private String category;
	private String semester;
	
	public UapTypeDto() {
		this.id = -1L;
	}

	public UapTypeDto(long id, String category, String semester) {
		super();
		this.id = id;
		this.category = category;
		this.semester = semester;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getSemester() {
		return semester;
	}

	public void setSemester(String semester) {
		this.semester = semester;
	}

	@Override
	public String toString() {
		return "UapTypeDto [id=" + id + ", category=" + category + ", semester=" + semester + "]";
	}
		
}
