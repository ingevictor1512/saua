package com.mx.saua.dto;

import java.io.Serializable;

public class CategoryQuestionDto implements Serializable{

	/**
	 * Serializador de la clase
	 */
	private static final long serialVersionUID = 1L;
	
	private int id;
	private String name;
	private String description;

	public CategoryQuestionDto(int id, String name, String description) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
	}
	
	public CategoryQuestionDto() {
		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return "CategoryQuestionDto [id=" + id + ", name=" + name + ", description=" + description + "]";
	}
	
}
