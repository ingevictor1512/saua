package com.mx.saua.dto;

import java.io.Serializable;
import java.util.List;

public class KardexDto implements Serializable {
	

	private static final long serialVersionUID = 1L;
	private long id;
	private String register;
	private String nameStudent;
	private String cveSchoolStudent;
	private String nameSchoolStudent;
	private String cvePlanStudent;
	private String vrsPlanStudent;
	private String namePlanStudent;
	private String cveUap;
	private String nameUap;
	private String testType;
	private String score;
	private String cveAproRepro;
	private String cvePeriod;
	private String examDate;
	private String cveSchoolUap;
	private String nameSchoolUap;
	private String namePlanUap;
	private PeriodDto period;
	private boolean beforePeriod;
	private boolean afterPeriod;
	/* No interactuan con la entity */
	private List<KardexDto> kardexDtosEfi;
	private List<KardexDto> kardexDtosNfbad;
	private List<KardexDto> kardexDtosNfpe;
	private List<KardexDto> kardexDtosEiv;
	private List<KardexDto> kardexDtosOtras;
	private String initialPeriod;
	private String endPeriod;
	private boolean evaluationsActive;
	
	public KardexDto() {
		this.beforePeriod = false;
		this.afterPeriod = false;
	}

	public KardexDto(long id, String register, String nameStudent, String cveSchoolStudent, String nameSchoolStudent,
			String cvePlanStudent, String vrsPlanStudent, String namePlanStudent, String cveUap, String nameUap,
			String testType, String score, String cveAproRepro, String cvePeriod, String examDate, String cveSchoolUap,
			String nameSchoolUap, String namePlanUap, List<KardexDto> kardexDtosEfi, List<KardexDto> kardexDtosNfbad,
			List<KardexDto> kardexDtosNfpe, List<KardexDto> kardexDtosEiv, List<KardexDto> kardexDtosOtras) {
		super();
		this.id = id;
		this.register = register;
		this.nameStudent = nameStudent;
		this.cveSchoolStudent = cveSchoolStudent;
		this.nameSchoolStudent = nameSchoolStudent;
		this.cvePlanStudent = cvePlanStudent;
		this.vrsPlanStudent = vrsPlanStudent;
		this.namePlanStudent = namePlanStudent;
		this.cveUap = cveUap;
		this.nameUap = nameUap;
		this.testType = testType;
		this.score = score;
		this.cveAproRepro = cveAproRepro;
		this.cvePeriod = cvePeriod;
		this.examDate = examDate;
		this.cveSchoolUap = cveSchoolUap;
		this.nameSchoolUap = nameSchoolUap;
		this.namePlanUap = namePlanUap;
		this.kardexDtosEfi = kardexDtosEfi;
		this.kardexDtosNfbad = kardexDtosNfbad;
		this.kardexDtosNfpe = kardexDtosNfpe;
		this.kardexDtosEiv = kardexDtosEiv;
		this.kardexDtosOtras = kardexDtosOtras;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getRegister() {
		return register;
	}

	public void setRegister(String register) {
		this.register = register;
	}

	public String getNameStudent() {
		return nameStudent;
	}

	public void setNameStudent(String nameStudent) {
		this.nameStudent = nameStudent;
	}

	public String getCveSchoolStudent() {
		return cveSchoolStudent;
	}

	public void setCveSchoolStudent(String cveSchoolStudent) {
		this.cveSchoolStudent = cveSchoolStudent;
	}

	public String getNameSchoolStudent() {
		return nameSchoolStudent;
	}

	public void setNameSchoolStudent(String nameSchoolStudent) {
		this.nameSchoolStudent = nameSchoolStudent;
	}

	public String getCvePlanStudent() {
		return cvePlanStudent;
	}

	public void setCvePlanStudent(String cvePlanStudent) {
		this.cvePlanStudent = cvePlanStudent;
	}

	public String getVrsPlanStudent() {
		return vrsPlanStudent;
	}

	public void setVrsPlanStudent(String vrsPlanStudent) {
		this.vrsPlanStudent = vrsPlanStudent;
	}

	public String getNamePlanStudent() {
		return namePlanStudent;
	}

	public void setNamePlanStudent(String namePlanStudent) {
		this.namePlanStudent = namePlanStudent;
	}

	public String getCveUap() {
		return cveUap;
	}

	public void setCveUap(String cveUap) {
		this.cveUap = cveUap;
	}

	public String getNameUap() {
		return nameUap;
	}

	public void setNameUap(String nameUap) {
		this.nameUap = nameUap;
	}

	public String getTestType() {
		return testType;
	}

	public void setTestType(String testType) {
		this.testType = testType;
	}

	public String getScore() {
		return score;
	}

	public void setScore(String score) {
		this.score = score;
	}

	public String getCveAproRepro() {
		return cveAproRepro;
	}

	public void setCveAproRepro(String cveAproRepro) {
		this.cveAproRepro = cveAproRepro;
	}

	public String getCvePeriod() {
		return cvePeriod;
	}

	public void setCvePeriod(String cvePeriod) {
		this.cvePeriod = cvePeriod;
	}

	public String getExamDate() {
		return examDate;
	}

	public void setExamDate(String examDate) {
		this.examDate = examDate;
	}

	public String getCveSchoolUap() {
		return cveSchoolUap;
	}

	public void setCveSchoolUap(String cveSchoolUap) {
		this.cveSchoolUap = cveSchoolUap;
	}

	public String getNameSchoolUap() {
		return nameSchoolUap;
	}

	public void setNameSchoolUap(String nameSchoolUap) {
		this.nameSchoolUap = nameSchoolUap;
	}

	public String getNamePlanUap() {
		return namePlanUap;
	}

	public void setNamePlanUap(String namePlanUap) {
		this.namePlanUap = namePlanUap;
	}

	public List<KardexDto> getKardexDtosEfi() {
		return kardexDtosEfi;
	}

	public void setKardexDtosEfi(List<KardexDto> kardexDtosEfi) {
		this.kardexDtosEfi = kardexDtosEfi;
	}

	public List<KardexDto> getKardexDtosNfbad() {
		return kardexDtosNfbad;
	}

	public void setKardexDtosNfbad(List<KardexDto> kardexDtosNfbad) {
		this.kardexDtosNfbad = kardexDtosNfbad;
	}

	public List<KardexDto> getKardexDtosNfpe() {
		return kardexDtosNfpe;
	}

	public void setKardexDtosNfpe(List<KardexDto> kardexDtosNfpe) {
		this.kardexDtosNfpe = kardexDtosNfpe;
	}

	public List<KardexDto> getKardexDtosEiv() {
		return kardexDtosEiv;
	}

	public void setKardexDtosEiv(List<KardexDto> kardexDtosEiv) {
		this.kardexDtosEiv = kardexDtosEiv;
	}

	public PeriodDto getPeriod() {
		return period;
	}

	public void setPeriod(PeriodDto period) {
		this.period = period;
	}

	public boolean isBeforePeriod() {
		return beforePeriod;
	}

	public void setBeforePeriod(boolean beforePeriod) {
		this.beforePeriod = beforePeriod;
	}

	public boolean isAfterPeriod() {
		return afterPeriod;
	}

	public void setAfterPeriod(boolean afterPeriod) {
		this.afterPeriod = afterPeriod;
	}

	public String getInitialPeriod() {
		return initialPeriod;
	}

	public void setInitialPeriod(String initialPeriod) {
		this.initialPeriod = initialPeriod;
	}

	public String getEndPeriod() {
		return endPeriod;
	}

	public void setEndPeriod(String endPeriod) {
		this.endPeriod = endPeriod;
	}

	public List<KardexDto> getKardexDtosOtras() {
		return kardexDtosOtras;
	}

	public void setKardexDtosOtras(List<KardexDto> kardexDtosOtras) {
		this.kardexDtosOtras = kardexDtosOtras;
	}

	public boolean isEvaluationsActive() {
		return evaluationsActive;
	}

	public void setEvaluationsActive(boolean evaluationsActive) {
		this.evaluationsActive = evaluationsActive;
	}

	@Override
	public String toString() {
		return "KardexDto [id=" + id + ", register=" + register + ", nameStudent=" + nameStudent + ", cveSchoolStudent="
				+ cveSchoolStudent + ", nameSchoolStudent=" + nameSchoolStudent + ", cvePlanStudent=" + cvePlanStudent
				+ ", vrsPlanStudent=" + vrsPlanStudent + ", namePlanStudent=" + namePlanStudent + ", cveUap=" + cveUap
				+ ", nameUap=" + nameUap + ", testType=" + testType + ", score=" + score + ", cveAproRepro="
				+ cveAproRepro + ", cvePeriod=" + cvePeriod + ", examDate=" + examDate + ", cveSchoolUap="
				+ cveSchoolUap + ", nameSchoolUap=" + nameSchoolUap + ", namePlanUap=" + namePlanUap
				+ ", kardexDtosEfi=" + kardexDtosEfi + ", kardexDtosNfbad=" + kardexDtosNfbad + ", kardexDtosNfpe="
				+ kardexDtosNfpe + ", kardexDtosEiv=" + kardexDtosEiv + "]";
	}
}
