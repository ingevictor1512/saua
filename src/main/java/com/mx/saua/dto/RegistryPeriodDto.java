/**
 * 
 */
package com.mx.saua.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * @author Benito Morales
 * @version 1.0
 *
 */
public class RegistryPeriodDto implements Serializable {

	/**
	 * Serializador de la clase
	 */
	private static final long serialVersionUID = 8463977054196506470L;

	private long id;

	private Date initialDate;

	private Date finalDate;

	private PeriodDto period;

	public RegistryPeriodDto(long id, Date initialDate, Date finalDate, PeriodDto period) {
		super();
		this.id = id;
		this.initialDate = initialDate;
		this.finalDate = finalDate;
		this.period = period;
	}

	public RegistryPeriodDto() {
		
	}
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Date getInitialDate() {
		return initialDate;
	}

	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}

	public Date getFinalDate() {
		return finalDate;
	}

	public void setFinalDate(Date finalDate) {
		this.finalDate = finalDate;
	}

	public PeriodDto getPeriod() {
		return period;
	}

	public void setPeriod(PeriodDto period) {
		this.period = period;
	}

	
}
