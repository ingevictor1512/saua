package com.mx.saua.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.mx.saua.entity.ResetPassword;
import com.mx.saua.entity.UserNotification;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 
 * @author Benito Morales
 * @version 2.0
 * 
 */
@Getter
@Setter
@ToString
@AllArgsConstructor
public class ResetPasswordDto extends PatternDto implements Serializable{

	/**
	 * Serializador de la clase
	 */
	private static final long serialVersionUID = 2309460466548235019L;
	
	private long idReset;
	private UserDto user;
	private Date created;
	private String hashValue;
	private boolean used;
	private List<ResetPasswordDto> resetPasswordDtos;
	private UserNotification userNotification;
	private String email;
	private List<String> comboSearch;
	private String searchBy;
	private String searchByValue;
	private List<ResetPassword> resetPasswordList;
	
	public ResetPasswordDto() {
		this.comboSearch = new ArrayList<>();
		this.resetPasswordList = new ArrayList<>();
	}
	
	public ResetPasswordDto(long idReset, Date created, String hashValue, boolean used, UserDto user, UserNotification notification) {
		super();
		this.idReset = idReset;
		this.created = created;
		this.hashValue = hashValue;
		this.used = used;
		this.user = user;
		this.userNotification = notification;
	}
	
}
