package com.mx.saua.dto;

import java.io.Serializable;
import java.sql.Time;
import java.util.Date;
import java.util.List;

import com.mx.saua.entity.Events;
import com.mx.saua.entity.User;

/**
 * 
 * @author Victor FG
 * @version 1.0
 */
public class AssistanceRecordDto implements Serializable{

	/**
	 * Serializador de la clase
	 */
	private static final long serialVersionUID = -6645735741428602681L;
	
	private long id;
	private Time initialTime;
	private Time endTime;
	private Date date;
	private Events events;
	private User user;
	/* No interactuan con la Entity */
	private List<AssistanceRecordDto> assistanceRecordDtos;
	
	
	public List<AssistanceRecordDto> getAssistanceRecordDtos() {
		return assistanceRecordDtos;
	}
	public void setAssistanceRecordDtos(List<AssistanceRecordDto> assistanceRecordDtos) {
		this.assistanceRecordDtos = assistanceRecordDtos;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public Time getInitialTime() {
		return initialTime;
	}
	public void setInitialTime(Time initialTime) {
		this.initialTime = initialTime;
	}
	public Time getEndTime() {
		return endTime;
	}
	public void setEndTime(Time endTime) {
		this.endTime = endTime;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public Events getEvents() {
		return events;
	}
	public void setEvents(Events events) {
		this.events = events;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public AssistanceRecordDto(long id, Time initialTime, Time endTime, Date date, Events events, User user) {
		super();
		this.id = id;
		this.initialTime = initialTime;
		this.endTime = endTime;
		this.date = date;
		this.events = events;
		this.user = user;
	}
	public AssistanceRecordDto() {

	}
	
}
