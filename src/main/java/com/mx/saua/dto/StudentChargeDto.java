/**
 * 
 */
package com.mx.saua.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.mx.saua.enums.StudyTypeEnum;

/**
 * @author Benito Morales
 * @version 1.0
 *
 */
public class StudentChargeDto implements Serializable{

	/**
	 * Serial de la clase
	 */
	private static final long serialVersionUID = -957463014500258708L;

	private long id;
	private StudentDto student;
	private PeriodDto period;
	private OfferedClassDto offeredClass;
	private Date registerDate;
	private String authorizationKey;
	private String comun;
	private Date editDate;
	private List<StudentChargeDto> studentCharges;
	private String idProgram;
	private List<ProgramDto> programs;
	private String idUap;
	private StudyTypeEnum studyType;
	private String idGroup;
	private String studentRegister;
	private GroupDto group;
	private String uapRegister;
	private boolean authorization;
	private String tutorName;
	private boolean teacherEvalAnswered;
	private boolean autoevalAnswered;
	private boolean evaluationsComplete;
	private String textHeader;
	private Date evaluationAnsweredDate;
	
	public StudentChargeDto() {
		
	}

	public StudentChargeDto(long id, StudentDto student, PeriodDto period, OfferedClassDto offeredClass, Date registerDate,
			String authorizationKey, String comun, Date editDate, GroupDto group, StudyTypeEnum studyType, Date evaluationAnsweredDate) {
		super();
		this.id = id;
		this.student = student;
		this.period = period;
		this.offeredClass = offeredClass;
		this.registerDate = registerDate;
		this.authorizationKey = authorizationKey;
		this.comun = comun;
		this.editDate = editDate;
		this.group = group;
		this.studyType = studyType;
		this.evaluationAnsweredDate = evaluationAnsweredDate;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public StudentDto getStudent() {
		return student;
	}

	public void setStudent(StudentDto student) {
		this.student = student;
	}

	public PeriodDto getPeriod() {
		return period;
	}

	public void setPeriod(PeriodDto period) {
		this.period = period;
	}

	public OfferedClassDto getOfferedClass() {
		return offeredClass;
	}

	public void setOfferedClass(OfferedClassDto offeredClass) {
		this.offeredClass = offeredClass;
	}

	public Date getRegisterDate() {
		return registerDate;
	}

	public void setRegisterDate(Date registerDate) {
		this.registerDate = registerDate;
	}

	public String getAuthorizationKey() {
		return authorizationKey;
	}

	public void setAuthorizationKey(String authorizationKey) {
		this.authorizationKey = authorizationKey;
	}

	public String getComun() {
		return comun;
	}

	public void setComun(String comun) {
		this.comun = comun;
	}

	public Date getEditDate() {
		return editDate;
	}

	public void setEditDate(Date editDate) {
		this.editDate = editDate;
	}

	public List<StudentChargeDto> getStudentCharges() {
		return studentCharges;
	}

	public void setStudentCharges(List<StudentChargeDto> studentCharges) {
		this.studentCharges = studentCharges;
	}

	public StudyTypeEnum getStudyType() {
		return studyType;
	}

	public void setStudyType(StudyTypeEnum studyType) {
		this.studyType = studyType;
	}
	
	public String getIdProgram() {
		return idProgram;
	}

	public void setIdProgram(String idProgram) {
		this.idProgram = idProgram;
	}

	public String getIdUap() {
		return idUap;
	}

	public void setIdUap(String idUap) {
		this.idUap = idUap;
	}

	public String getIdGroup() {
		return idGroup;
	}

	public void setIdGroup(String idGroup) {
		this.idGroup = idGroup;
	}

	public String getStudentRegister() {
		return studentRegister;
	}

	public void setStudentRegister(String studentRegister) {
		this.studentRegister = studentRegister;
	}

	public GroupDto getGroup() {
		return group;
	}

	public void setGroup(GroupDto group) {
		this.group = group;
	}
	
	public String getUapRegister() {
		return uapRegister;
	}

	public void setUapRegister(String uapRegister) {
		this.uapRegister = uapRegister;
	}

	public boolean isAuthorization() {
		return authorization;
	}

	public void setAuthorization(boolean authorization) {
		this.authorization = authorization;
	}

	public String getStudyTypeValue() {
		if(getStudyType().toString().equals(StudyTypeEnum.STUDY.toString())) {
			return StudyTypeEnum.STUDY.getValue();
		}
		return StudyTypeEnum.RE_STUDY.getValue();
	}

	public void setStudyTypeValue(String studyTypeValue) {
	}

	public String getTutorName() {
		return tutorName;
	}

	public void setTutorName(String tutorName) {
		this.tutorName = tutorName;
	}

	public List<ProgramDto> getPrograms() {
		return programs;
	}

	public void setPrograms(List<ProgramDto> programs) {
		this.programs = programs;
	}

	public boolean isTeacherEvalAnswered() {
		return teacherEvalAnswered;
	}

	public void setTeacherEvalAnswered(boolean teacherEvalAnswered) {
		this.teacherEvalAnswered = teacherEvalAnswered;
	}

	public boolean isAutoevalAnswered() {
		return autoevalAnswered;
	}

	public void setAutoevalAnswered(boolean autoevalAnswered) {
		this.autoevalAnswered = autoevalAnswered;
	}

	public boolean isEvaluationsComplete() {
		return evaluationsComplete;
	}

	public void setEvaluationsComplete(boolean evaluationsComplete) {
		this.evaluationsComplete = evaluationsComplete;
	}


	public String getTextHeader() {
		return textHeader;
	}

	public void setTextHeader(String textHeader) {
		this.textHeader = textHeader;
	}
	public Date getEvaluationAnsweredDate() {
		return evaluationAnsweredDate;
	}

	public void setEvaluationAnsweredDate(Date evaluationAnsweredDate) {
		this.evaluationAnsweredDate = evaluationAnsweredDate;

	}

	@Override
	public String toString() {
		return "StudentChargeDto [id=" + id + ", studentRegister=" + studentRegister 
				+ ", idGroup=" + idGroup + ", idUap=" + idUap + ", idProgram=" + idProgram + ", studyType=" + studyType
				+ ", period=" + period + ", offeredClass=" + offeredClass 
				+ ", registerDate=" + registerDate + ", authorizationKey=" + authorizationKey
				+ ", comun=" + comun + ", editDate=" + editDate + ", uapRegister=" + uapRegister  + ", authorization=" + authorization + ", programs=" + programs + "]";
	}
	
}
