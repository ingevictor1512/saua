package com.mx.saua.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
@AllArgsConstructor
public class CoffeeShopDto implements Serializable {
	
    private static final long serialVersionUID = -5341382154001998095L;
    
	private int id;
    private String name;
    private Long grantNumber;
    private String manager;
    private String location;
    private Date registerDate;
    private Date editDate;

    public CoffeeShopDto() {
        this.id=-1;
    }
}
