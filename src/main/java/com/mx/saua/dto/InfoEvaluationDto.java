package com.mx.saua.dto;

import com.mx.saua.entity.IdInfoEvaluation;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class InfoEvaluationDto implements Serializable {
    /**
     * Serializador de la clase
     */
    private static final long serialVersionUID = 1L;

    private long idUsuario;
    private String register;
    private String name;
    private String periodo;
    private int totalAutoEval;
    private int totalTeacherEval;
    private int cargaAnterior;
    private Date registerDate;
    private Date editDate;
    private List<InfoEvaluationDto> infoEvaluationDtos;
    private boolean activeEval;

    public InfoEvaluationDto() {
        this.idUsuario = -1L;
    }

    public InfoEvaluationDto(long idUsuario, String register, String periodo, int totalAutoEval, int totalTeacherEval, Date registerDate, Date editDate, boolean activeEval, String name, int cargaAnterior) {
        this.idUsuario = idUsuario;
        this.register = register;
        this.periodo = periodo;
        this.totalAutoEval = totalAutoEval;
        this.totalTeacherEval = totalTeacherEval;
        this.registerDate = registerDate;
        this.editDate = editDate;
        this.name = name;
        this.activeEval = activeEval;
        this.cargaAnterior = cargaAnterior;
    }

    public long getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(long idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getRegister() {
        return register;
    }

    public void setRegister(String register) {
        this.register = register;
    }

    public String getPeriodo() {
        return periodo;
    }

    public void setPeriodo(String periodo) {
        this.periodo = periodo;
    }

    public int getTotalAutoEval() {
        return totalAutoEval;
    }

    public void setTotalAutoEval(int totalAutoEval) {
        this.totalAutoEval = totalAutoEval;
    }

    public int getTotalTeacherEval() {
        return totalTeacherEval;
    }

    public void setTotalTeacherEval(int totalTeacherEval) {
        this.totalTeacherEval = totalTeacherEval;
    }

    public Date getRegisterDate() {
        return registerDate;
    }

    public void setRegisterDate(Date registerDate) {
        this.registerDate = registerDate;
    }

    public Date getEditDate() {
        return editDate;
    }

    public void setEditDate(Date editDate) {
        this.editDate = editDate;
    }

    public boolean isActiveEval() {
        return activeEval;
    }

    public void setActiveEval(boolean activeEval) {
        this.activeEval = activeEval;
    }

    public List<InfoEvaluationDto> getInfoEvaluationDtos() {
        return infoEvaluationDtos;
    }

    public void setInfoEvaluationDtos(List<InfoEvaluationDto> infoEvaluationDtos) {
        this.infoEvaluationDtos = infoEvaluationDtos;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCargaAnterior() {
        return cargaAnterior;
    }

    public void setCargaAnterior(int cargaAnterior) {
        this.cargaAnterior = cargaAnterior;
    }

    @Override
    public String toString() {
        return "InfoEvaluationDto{" +
                "idUsuario=" + idUsuario +
                ", register='" + register + '\'' +
                ", name='" + name + '\'' +
                ", periodo='" + periodo + '\'' +
                ", totalAutoEval=" + totalAutoEval +
                ", totalTeacherEval=" + totalTeacherEval +
                ", registerDate=" + registerDate +
                ", editDate=" + editDate +
                ", infoEvaluationDtos=" + infoEvaluationDtos +
                '}';
    }
}
