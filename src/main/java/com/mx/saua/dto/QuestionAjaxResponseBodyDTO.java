package com.mx.saua.dto;

import java.util.List;

import com.mx.saua.entity.Question;

public class QuestionAjaxResponseBodyDTO {

    String msg;
    List<Question> result;
    int totalPages;
    List<String> idsQuestions;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<Question> getResult() {
        return result;
    }

    public void setResult(List<Question> result) {
        this.result = result;
    }

	public int getTotalPages() {
		return totalPages;
	}

	public void setTotalPages(int totalPages) {
		this.totalPages = totalPages;
	}

	public List<String> getIdsQuestions() {
		return idsQuestions;
	}

	public void setIdsQuestions(List<String> idsQuestions) {
		this.idsQuestions = idsQuestions;
	}

	
}