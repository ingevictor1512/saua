package com.mx.saua.dto;

import com.mx.saua.entity.OfferedClass;

import java.io.Serializable;
import java.util.Date;

/**
 * Dto que permite guardar/consultar los horarios de cada clase ofertada
 * @author Victor Francisco Garcia
 */
public class ScheduleDto implements Serializable {
    private static final long serialVersionUID = 1L;
    private long id;
    private String classroom;
    private OfferedClass offeredClass;
    private String startTime;
    private String endTime;
    private String day;
    private Date editDate;

    public ScheduleDto() {
        this.id = -1;
    }

    public ScheduleDto(long id, String classroom, OfferedClass offeredClass, String startTime, String endTime, String day, Date editDate) {
        this.id = id;
        this.classroom = classroom;
        this.offeredClass = offeredClass;
        this.startTime = startTime;
        this.endTime = endTime;
        this.day = day;
        this.editDate = editDate;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getClassroom() {
        return classroom;
    }

    public void setClassroom(String classroom) {
        this.classroom = classroom;
    }

    public OfferedClass getOfferedClass() {
        return offeredClass;
    }

    public void setOfferedClass(OfferedClass offeredClass) {
        this.offeredClass = offeredClass;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public Date getEditDate() {
        return editDate;
    }

    public void setEditDate(Date editDate) {
        this.editDate = editDate;
    }

    @Override
    public String toString() {
        return "ScheduleDto{" +
                "id=" + id +
                ", classroom='" + classroom + '\'' +
                ", offeredClass=" + offeredClass +
                ", startTime='" + startTime + '\'' +
                ", endTime='" + endTime + '\'' +
                ", day='" + day + '\'' +
                ", editDate=" + editDate +
                '}';
    }
}
