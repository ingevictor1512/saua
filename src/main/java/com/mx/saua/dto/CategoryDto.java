package com.mx.saua.dto;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class CategoryDto implements Serializable {
	/**
	 * DTO que permite guardar la informacion de las categorias de los maestros
	 * @author Vic
	 */
	private static final long serialVersionUID = 1L;
	
	private long id;
	private String description;
	
	public CategoryDto() {
		this.id=-1L;
	}
}
