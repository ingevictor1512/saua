package com.mx.saua.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;


public class TutorshipDto implements Serializable{
	
	/**
	 * @author Vic
	 */
	private static final long serialVersionUID = 1L;
	private long id;
	private String observations;
	private String assignedTutor;
	private StudentDto student;
	private TeacherDto teacher;
	private TutoringLowDto tutoringLow;
	private Date registerDate;
	private Date editdate;
	/* No tienen interaccion con la Entity */
	private String register;
	private List<StudentDto> studentsDto;
	private List<TeacherDto> teachersDto;
	private List<TutoringLowDto> tutoringLowsDto;
	private List<TutorshipDto> tutorshipsDto;
	private String authority;
	private LoadAuthorizationDto loadAuthorizationDto;
	private List<ProgramDto> programs;
	/* Variables para agregar un tutor a un grupo */
	private long idProgram;
	private long idGroup;
	private long idTeacher;
	/* Para el conteo de tutores activos */
	private long tutoredActives;
	/* Para saber si aun esta en fechas de autorizacion */
	private boolean isPeriod;
	public TutorshipDto() {
		this.id=-1L;
		this.isPeriod= false;

	}

	public TutorshipDto(long id, String observations, String assignedTutor, StudentDto student, TeacherDto teacher,
			TutoringLowDto tutoringLow, Date registerDate, Date editdate, String register, List<StudentDto> studentsDto,
			List<TeacherDto> teachersDto, List<TutoringLowDto> tutoringLowsDto, List<TutorshipDto> tutorshipsDto,
			String authority, LoadAuthorizationDto loadAuthorizationDto) {
		super();
		this.id = id;
		this.observations = observations;
		this.assignedTutor = assignedTutor;
		this.student = student;
		this.teacher = teacher;
		this.tutoringLow = tutoringLow;
		this.registerDate = registerDate;
		this.editdate = editdate;
		this.register = register;
		this.studentsDto = studentsDto;
		this.teachersDto = teachersDto;
		this.tutoringLowsDto = tutoringLowsDto;
		this.tutorshipsDto = tutorshipsDto;
		this.authority = authority;
		this.loadAuthorizationDto = loadAuthorizationDto;
	}

	public boolean isPeriod() {
		return isPeriod;
	}

	public void setPeriod(boolean isPeriod) {
		this.isPeriod = isPeriod;
	}

	public TutorshipDto(long id, TeacherDto teacher) {
		super();
		this.id = id;
		this.teacher = teacher;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getObservations() {
		return observations;
	}

	public void setObservations(String observations) {
		this.observations = observations;
	}

	public String getAssignedTutor() {
		return assignedTutor;
	}

	public void setAssignedTutor(String assignedTutor) {
		this.assignedTutor = assignedTutor;
	}

	public StudentDto getStudent() {
		return student;
	}

	public void setStudent(StudentDto student) {
		this.student = student;
	}

	public TeacherDto getTeacher() {
		return teacher;
	}

	public void setTeacher(TeacherDto teacher) {
		this.teacher = teacher;
	}

	public TutoringLowDto getTutoringLow() {
		return tutoringLow;
	}

	public void setTutoringLow(TutoringLowDto tutoringLow) {
		this.tutoringLow = tutoringLow;
	}

	public Date getRegisterDate() {
		return registerDate;
	}

	public void setRegisterDate(Date registerDate) {
		this.registerDate = registerDate;
	}

	public Date getEditdate() {
		return editdate;
	}

	public void setEditdate(Date editdate) {
		this.editdate = editdate;
	}

	public List<StudentDto> getStudentsDto() {
		return studentsDto;
	}

	public void setStudentsDto(List<StudentDto> studentsDto) {
		this.studentsDto = studentsDto;
	}

	public List<TeacherDto> getTeachersDto() {
		return teachersDto;
	}

	public void setTeachersDto(List<TeacherDto> teachersDto) {
		this.teachersDto = teachersDto;
	}

	public List<TutoringLowDto> getTutoringLowsDto() {
		return tutoringLowsDto;
	}

	public void setTutoringLowsDto(List<TutoringLowDto> tutoringLowsDto) {
		this.tutoringLowsDto = tutoringLowsDto;
	}

	public List<TutorshipDto> getTutorshipsDto() {
		return tutorshipsDto;
	}

	public void setTutorshipsDto(List<TutorshipDto> tutorshipsDto) {
		this.tutorshipsDto = tutorshipsDto;
	}

	public String getRegister() {
		return register;
	}

	public void setRegister(String register) {
		this.register = register;
	}

	public String getAuthority() {
		return authority;
	}

	public void setAuthority(String authority) {
		this.authority = authority;
	}

	public LoadAuthorizationDto getLoadAuthorizationDto() {
		return loadAuthorizationDto;
	}

	public void setLoadAuthorizationDto(LoadAuthorizationDto loadAuthorizationDto) {
		this.loadAuthorizationDto = loadAuthorizationDto;
	}

	public List<ProgramDto> getPrograms() {
		return programs;
	}

	public void setPrograms(List<ProgramDto> programs) {
		this.programs = programs;
	}

	public long getIdProgram() {
		return idProgram;
	}

	public void setIdProgram(long idProgram) {
		this.idProgram = idProgram;
	}

	public long getIdGroup() {
		return idGroup;
	}

	public void setIdGroup(long idGroup) {
		this.idGroup = idGroup;
	}

	public long getIdTeacher() {
		return idTeacher;
	}

	public void setIdTeacher(long idTeacher) {
		this.idTeacher = idTeacher;
	}
	

	public long getTutoredActives() {
		return tutoredActives;
	}

	public void setTutoredActives(long tutoredActives) {
		this.tutoredActives = tutoredActives;
	}

	@Override
	public String toString() {
		return "TutorshipDto [id=" + id + ", observations=" + observations + ", assignedTutor=" + assignedTutor
				+ ", student=" + student + ", teacher=" + teacher + ", tutoringLow=" + tutoringLow + ", registerDate="
				+ registerDate + ", editdate=" + editdate + ", register=" + register + ", studentsDto=" + studentsDto
				+ ", teachersDto=" + teachersDto + ", tutoringLowsDto=" + tutoringLowsDto + ", tutorshipsDto="
				+ tutorshipsDto + ", authority=" + authority + ", loadAuthorizationDto=" + loadAuthorizationDto
				+ ", programs=" + programs + ", idProgram=" + idProgram + ", idGroup=" + idGroup + ", idTeacher="
				+ idTeacher + "]";
	}


}
