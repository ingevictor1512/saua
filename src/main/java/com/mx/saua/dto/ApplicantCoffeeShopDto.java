package com.mx.saua.dto;

import java.io.Serializable;
import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ApplicantCoffeeShopDto implements Serializable{

    private static final long serialVersionUID = 1L;

	private ApplicantCoffeeShopIdDto applicantCoffeeShopIdDto;
    
    private int useNumber;
    
    private Date registerDate;
    
    private Date editDate;
    
    private String data;
    
    private String applicantRegister;
    
    private String useDateFormated;
    
    private String day;
    
    private int idCoffeeShop;
    
    private String coffeeShopDay;
    
    private long applicantId;
}
