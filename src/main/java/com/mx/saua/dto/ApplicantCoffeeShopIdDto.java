package com.mx.saua.dto;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ApplicantCoffeeShopIdDto implements Serializable{

	private static final long serialVersionUID = -2899894118945739628L;
	
    private ApplicantDto applicant;

    private CoffeeShopDto coffeeShop;
    
    private String usedate;
}
