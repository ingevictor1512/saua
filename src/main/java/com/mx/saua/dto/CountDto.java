package com.mx.saua.dto;

import java.io.Serializable;

public class CountDto implements Serializable{

	private static final long serialVersionUID = 6803332125309435759L;
	
	private Long idGroup;
	private String uap;
	
	public CountDto() {
		
	}
	
	public CountDto(Long idGroup, String uap) {
		super();
		this.idGroup = idGroup;
		this.uap = uap;
	}
	
	public Long getIdGroup() {
		return idGroup;
	}
	public void setIdGroup(Long idGroup) {
		this.idGroup = idGroup;
	}
	public String getUap() {
		return uap;
	}
	public void setUap(String uap) {
		this.uap = uap;
	}

}
