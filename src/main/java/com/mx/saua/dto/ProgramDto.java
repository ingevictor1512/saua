package com.mx.saua.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.mx.saua.entity.School;

public class ProgramDto implements Serializable{	
	/**
	 * @author Vic
	 */
	private static final long serialVersionUID = 1L;
	
	private String studentPlanKey;
	private String studentPlanName;
	private String studentPlanVrs;
	private String abr;
	private String observations;
	private School school;
	private boolean estatusCarga;
	private Date registerDate;
	private Date editdate;
	/* No interactuan con la Entity */
	List<SchoolDto> schoolDtos;
	List<ProgramDto> programDtos;
	private boolean newRegister;
	
	public ProgramDto() {
		this.newRegister = true;

	}
	
	public ProgramDto(String studentPlanKey, String studentPlanName, String studentPlanVrs, String abr,
			String observations, School school, Date registerDate, Date editdate, List<SchoolDto> schoolDtos, boolean estatusCarga, boolean newRegister) {
		super();
		this.studentPlanKey = studentPlanKey;
		this.studentPlanName = studentPlanName;
		this.studentPlanVrs = studentPlanVrs;
		this.abr = abr;
		this.observations = observations;
		this.school = school;
		this.estatusCarga = estatusCarga;
		this.registerDate = registerDate;
		this.editdate = editdate;
		this.schoolDtos = schoolDtos;
		this.newRegister = newRegister;
	}

	public boolean isEstatusCarga() {
		return estatusCarga;
	}

	public void setEstatusCarga(boolean estatusCarga) {
		this.estatusCarga = estatusCarga;
	}

	public String getStudentPlanKey() {
		return studentPlanKey;
	}
	public void setStudentPlanKey(String studentPlanKey) {
		this.studentPlanKey = studentPlanKey;
	}
	public String getStudentPlanName() {
		return studentPlanName;
	}
	public void setStudentPlanName(String studentPlanName) {
		this.studentPlanName = studentPlanName;
	}
	public String getStudentPlanVrs() {
		return studentPlanVrs;
	}
	public void setStudentPlanVrs(String studentPlanVrs) {
		this.studentPlanVrs = studentPlanVrs;
	}
	public String getAbr() {
		return abr;
	}
	public void setAbr(String abr) {
		this.abr = abr;
	}
	public String getObservations() {
		return observations;
	}
	public void setObservations(String observations) {
		this.observations = observations;
	}
	public School getSchool() {
		return school;
	}
	public void setSchool(School school) {
		this.school = school;
	}
	public Date getRegisterDate() {
		return registerDate;
	}
	public void setRegisterDate(Date registerDate) {
		this.registerDate = registerDate;
	}
	public Date getEditdate() {
		return editdate;
	}
	public void setEditdate(Date editdate) {
		this.editdate = editdate;
	}
	
	public List<SchoolDto> getSchoolDtos() {
		return schoolDtos;
	}

	public void setSchoolDtos(List<SchoolDto> schoolDtos) {
		this.schoolDtos = schoolDtos;
	}

	public boolean isNewRegister() {
		return newRegister;
	}

	public void setNewRegister(boolean newRegister) {
		this.newRegister = newRegister;
	}

	public List<ProgramDto> getProgramDtos() {
		return programDtos;
	}

	public void setProgramDtos(List<ProgramDto> programDtos) {
		this.programDtos = programDtos;
	}

	@Override
	public String toString() {
		return "ProgramDto [studentPlanKey=" + studentPlanKey + ", studentPlanName=" + studentPlanName
				+ ", studentPlanVrs=" + studentPlanVrs + ", abr=" + abr + ", observations=" + observations + ", school="
				+ school + ", registerDate=" + registerDate + ", editdate=" + editdate + "]";
	}

}
