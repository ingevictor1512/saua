package com.mx.saua.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.mx.saua.enums.GenderEnum;

/**
 * Dto del profesor
 * 
 * @author Benito Morales
 * @version 1.0
 *
 */
public class TeacherDto implements Serializable {

	/**
	 * Serial de la clase
	 */
	private static final long serialVersionUID = 7104156757716993316L;

	private long id;
	private String name;
	private String lastname;
	private String secondLastname;
	private String cubicle;
	private CategoryDto categoryDto;
	private Date registerDate;
	private Date editDate;
	private List<TeacherDto> teachers;
	private List<CategoryDto> categories;
	private String register;
	private String email;
	private String phone;
	private GenderEnum gender;
	private UserDto userDto;
	private String authority;
	private List<PeriodDto> periods;

	public TeacherDto() {
		this.id = 0;
		teachers = new ArrayList<>();
		categories = new ArrayList<>();
		periods = new ArrayList<>();
	}

	public TeacherDto(long id, String name, String lastname, String secondLastname, String cubicle,
			CategoryDto categoryDto, Date registerDate, Date editDate, String email, GenderEnum gender,
			String authority, String phone) {
		super();
		this.id = id;
		this.name = name;
		this.lastname = lastname;
		this.secondLastname = secondLastname;
		this.cubicle = cubicle;
		this.categoryDto = categoryDto;
		this.registerDate = registerDate;
		this.editDate = editDate;
		this.email = email;
		this.gender = gender;
		this.authority = authority;
		this.phone = phone;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getSecondLastname() {
		return secondLastname;
	}

	public void setSecondLastname(String secondLastname) {
		this.secondLastname = secondLastname;
	}

	public String getCubicle() {
		return cubicle;
	}

	public void setCubicle(String cubicle) {
		this.cubicle = cubicle;
	}

	public CategoryDto getCategoryDto() {
		return categoryDto;
	}

	public void setCategoryDto(CategoryDto categoryDto) {
		this.categoryDto = categoryDto;
	}

	public Date getRegisterDate() {
		return registerDate;
	}

	public void setRegisterDate(Date registerDate) {
		this.registerDate = registerDate;
	}

	public Date getEditDate() {
		return editDate;
	}

	public void setEditDate(Date editDate) {
		this.editDate = editDate;
	}

	public List<TeacherDto> getTeachers() {
		return teachers;
	}

	public void setTeachers(List<TeacherDto> teachers) {
		this.teachers = teachers;
	}

	public List<CategoryDto> getCategories() {
		return categories;
	}

	public void setCategories(List<CategoryDto> categories) {
		this.categories = categories;
	}

	public String getRegister() {
		return register;
	}

	public void setRegister(String register) {
		this.register = register;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public GenderEnum getGender() {
		return gender;
	}

	public void setGender(GenderEnum gender) {
		this.gender = gender;
	}

	public UserDto getUserDto() {
		return userDto;
	}

	public void setUserDto(UserDto userDto) {
		this.userDto = userDto;
	}

	public String getAuthority() {
		return authority;
	}

	public void setAuthority(String authority) {
		this.authority = authority;
	}

	public List<PeriodDto> getPeriods() {
		return periods;
	}

	public void setPeriods(List<PeriodDto> periods) {
		this.periods = periods;
	}

	@Override
	public String toString() {
		return "TeacherDto [id=" + id + ", name=" + name + ", lastname=" + lastname + ", secondLastname="
				+ secondLastname + ", cubicle=" + cubicle + ", categoryDto=" + categoryDto + ", registerDate="
				+ registerDate + ", editDate=" + editDate + ", email=" + email + ", gender=" + gender + ", register="
				+ register + ", authority=" + authority + ", phone=" + phone + ", periods=" + periods.toString() + "]";
	}
}
