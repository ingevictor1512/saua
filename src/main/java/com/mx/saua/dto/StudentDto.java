package com.mx.saua.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.mx.saua.entity.Student;
import com.mx.saua.enums.GenderEnum;

public class StudentDto extends PatternDto implements Serializable {

	/**
	 * Serial de la clase
	 */
	private static final long serialVersionUID = 1L;

	private String register;
	private String lastname;
	private String secondLastname;
	private String name;
	private String curp;
	private int semester;
	private String generation;
	private GenderEnum gender;
	private Date registerDate;
	private Date editDate;
	private List<StudentDto> students;
	private boolean newRegister;
	private String email;
	private UserDto userDto;
	private StudentStatusDto studentStatusDto;
	private List<IncomeDto> incomes;
	private IncomeDto incomeDto;
	private List<GroupDto> groupsDto;
	private GroupDto groupDto;
	private ProgramDto programDto;
	private List<ProgramDto> programsDto;
	private UapDto uap;
	private List<UapDto> uaps;
	private List<StudentChargeDto> studentChargeDtos; 
	private String phone;
	private Long counter;
	private boolean firstLogin;
	private String searchBy;
	private String searchByValue;
	private List<String> comboSearch;
	private List<Student> studentList;
	private int totalAutoEval;
	private int totalTeacherEval;
	private int totalChargePreviousPeriod;
	private String studentStatus;
	private List<StudentStatusDto> studentStatusDtos;
	private int coffeeShopId;


	public StudentDto() {
		this.newRegister = true;
		this.register= "";
		this.searchBy = "";
		this.searchByValue = "";
	}

	public StudentDto(String register, String lastname, String secondLastname, String name, String curp, int semester,
			String generation, GenderEnum gender, Date registerDate, Date editDate, List<StudentDto> students, boolean newRegister, 
			String email, UserDto userDto, StudentStatusDto studentStatusDto, IncomeDto incomeDto, List<GroupDto> groupsDto, 
			GroupDto groupDto, ProgramDto programDto, String phone) {
		super();
		this.register = register;
		this.lastname = lastname;
		this.secondLastname = secondLastname;
		this.name = name;
		this.curp = curp;
		this.semester = semester;
		this.generation = generation;
		this.gender = gender;
		this.registerDate = registerDate;
		this.editDate = editDate;
		this.students = students;
		this.newRegister = newRegister;
		this.email = email;
		this.userDto = userDto;
		this.studentStatusDto = studentStatusDto;
		this.incomeDto = incomeDto;
		this.groupsDto = groupsDto;
		this.groupDto = groupDto;
		this.programDto = programDto;
		this.phone = phone;
	}

	public String getRegister() {
		return register;
	}

	public void setRegister(String register) {
		this.register = register;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getSecondLastname() {
		return secondLastname;
	}

	public void setSecondLastname(String secondLastname) {
		this.secondLastname = secondLastname;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCurp() {
		return curp;
	}

	public void setCurp(String curp) {
		this.curp = curp;
	}

	public int getSemester() {
		return semester;
	}

	public void setSemester(int semester) {
		this.semester = semester;
	}

	public String getGeneration() {
		return generation;
	}

	public void setGeneration(String generation) {
		this.generation = generation;
	}

	public GenderEnum getGender() {
		return gender;
	}

	public void setGender(GenderEnum gender) {
		this.gender = gender;
	}

	public Date getRegisterDate() {
		return registerDate;
	}

	public void setRegisterDate(Date registerDate) {
		this.registerDate = registerDate;
	}

	public Date getEditDate() {
		return editDate;
	}

	public void setEditDate(Date editDate) {
		this.editDate = editDate;
	}
	
	public List<StudentDto> getStudents() {
		return students;
	}

	public void setStudents(List<StudentDto> students) {
		this.students = students;
	}

	public boolean isNewRegister() {
		return newRegister;
	}

	public void setNewRegister(boolean newRegister) {
		this.newRegister = newRegister;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	public UserDto getUserDto() {
		return userDto;
	}

	public void setUserDto(UserDto userDto) {
		this.userDto = userDto;
	}

	public StudentStatusDto getStudentStatusDto() {
		return studentStatusDto;
	}

	public void setStudentStatusDto(StudentStatusDto studentStatusDto) {
		this.studentStatusDto = studentStatusDto;
	}
	
	public List<IncomeDto> getIncomes() {
		return incomes;
	}

	public void setIncomes(List<IncomeDto> incomes) {
		this.incomes = incomes;
	}
	
	public IncomeDto getIncomeDto() {
		return incomeDto;
	}

	public void setIncomeDto(IncomeDto incomeDto) {
		this.incomeDto = incomeDto;
	}
	
	public List<GroupDto> getGroupsDto() {
		return groupsDto;
	}

	public void setGroupsDto(List<GroupDto> groupsDto) {
		this.groupsDto = groupsDto;
	}

	public GroupDto getGroupDto() {
		return groupDto;
	}

	public void setGroupDto(GroupDto groupDto) {
		this.groupDto = groupDto;
	}
	
	public ProgramDto getProgramDto() {
		return programDto;
	}

	public void setProgramDto(ProgramDto programDto) {
		this.programDto = programDto;
	}

	public List<ProgramDto> getProgramsDto() {
		return programsDto;
	}

	public void setProgramsDto(List<ProgramDto> programsDto) {
		this.programsDto = programsDto;
	}

	public UapDto getUap() {
		return uap;
	}

	public void setUap(UapDto uap) {
		this.uap = uap;
	}

	public List<UapDto> getUaps() {
		return uaps;
	}

	public void setUaps(List<UapDto> uaps) {
		this.uaps = uaps;
	}

	public List<StudentChargeDto> getStudentChargeDtos() {
		return studentChargeDtos;
	}

	public void setStudentChargeDtos(List<StudentChargeDto> studentChargeDtos) {
		this.studentChargeDtos = studentChargeDtos;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Long getCounter() {
		return counter;
	}

	public void setCounter(Long counter) {
		this.counter = counter;
	}

	public boolean isFirstLogin() {
		return firstLogin;
	}

	public void setFirstLogin(boolean firstLogin) {
		this.firstLogin = firstLogin;
	}

	public String getSearchBy() {
		return searchBy;
	}

	public void setSearchBy(String searchBy) {
		this.searchBy = searchBy;
	}

	public String getSearchByValue() {
		return searchByValue;
	}

	public void setSearchByValue(String searchByValue) {
		this.searchByValue = searchByValue;
	}

	public List<String> getComboSearch() {
		return comboSearch;
	}

	public void setComboSearch(List<String> comboSearch) {
		this.comboSearch = comboSearch;
	}

	public List<Student> getStudentList() {
		return studentList;
	}

	public void setStudentList(List<Student> studentList) {
		this.studentList = studentList;
	}

	
	public String getStudentStatus() {
		return studentStatus;
	}

	public void setStudentStatus(String studentStatus) {
		this.studentStatus = studentStatus;
	}

	public List<StudentStatusDto> getStudentStatusDtos() {
		return studentStatusDtos;
	}

	public void setStudentStatusDtos(List<StudentStatusDto> studentStatusDtos) {
		this.studentStatusDtos = studentStatusDtos;
	}

	public int getCoffeeShopId() {
		return coffeeShopId;
	}

	public void setCoffeeShopId(int coffeeShopId) {
		this.coffeeShopId = coffeeShopId;
	}

	@Override
	public String toString() {
		StringBuilder sbuilder = new StringBuilder();
		sbuilder.append("Student [register="+register);
		sbuilder.append(", lastname="+lastname);
		sbuilder.append(", secondLastname="+secondLastname);
		sbuilder.append(", name="+name);
		sbuilder.append(", curp="+curp);
		sbuilder.append(", semester="+semester);
		sbuilder.append(", generation="+generation);
		sbuilder.append(", gender="+gender);
		sbuilder.append(", students="+students);
		sbuilder.append(", registerDate="+registerDate);
		sbuilder.append(", editDate="+editDate);
		sbuilder.append(", newRegister = "+newRegister);
		sbuilder.append(", email = "+email);
		sbuilder.append(", phone = "+phone);
		sbuilder.append(", programDto = "+(programDto!=null?programDto.toString():"null"));
		sbuilder.append(", incomeDto = "+(incomeDto!=null?incomeDto.toString():"null"));
		sbuilder.append(", userDto = "+(userDto!=null?userDto.toString():"null"));
		sbuilder.append(", groupDto = "+(groupDto!=null?groupDto.toString():"null"));
		sbuilder.append(", studentStatusDto = "+(studentStatusDto!=null?studentStatusDto.toString():"null"));
		sbuilder.append(", studentChargeDtos = "+(studentChargeDtos!=null?studentChargeDtos.toString():"null"));
		sbuilder.append("]");
		return sbuilder.toString();
	}

	public int getTotalAutoEval() {
		return totalAutoEval;
	}

	public void setTotalAutoEval(int totalAutoEval) {
		this.totalAutoEval = totalAutoEval;
	}

	public int getTotalTeacherEval() {
		return totalTeacherEval;
	}

	public void setTotalTeacherEval(int totalTeacherEval) {
		this.totalTeacherEval = totalTeacherEval;
	}

	public int getTotalChargePreviousPeriod() {
		return totalChargePreviousPeriod;
	}

	public void setTotalChargePreviousPeriod(int totalChargePreviousPeriod) {
		this.totalChargePreviousPeriod = totalChargePreviousPeriod;
	}
	

}
