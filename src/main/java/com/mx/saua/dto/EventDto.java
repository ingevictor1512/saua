package com.mx.saua.dto;

import java.io.Serializable;
import java.util.Date;

import com.mx.saua.entity.Program;
import com.mx.saua.entity.Space;

/**
 * 
 * @author Victor FG
 * @version 1.0
 */
public class EventDto implements Serializable{

	/**
	 * Serializador de la clase
	 */
	private static final long serialVersionUID = -4998966523652776853L;
	
	private long id;
	private String eventName;
	private Space spaces;
	private Date date;
	private Program educationalProgram;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getEventName() {
		return eventName;
	}
	public void setEventName(String eventName) {
		this.eventName = eventName;
	}
	public Space getSpaces() {
		return spaces;
	}
	public void setSpaces(Space spaces) {
		this.spaces = spaces;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public Program getEducationalProgram() {
		return educationalProgram;
	}
	public void setEducationalProgram(Program educationalProgram) {
		this.educationalProgram = educationalProgram;
	}
	
}
