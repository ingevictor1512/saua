package com.mx.saua.dto;

import java.io.Serializable;

public class ConfigurationDto implements Serializable {

	/**
	 * Serializador de la clase
	 */
	private static final long serialVersionUID = 1L;

	private long idConfiguration;
	private String key;
	private String value;
	private String description;
	private int type;

	public long getIdConfiguration() {
		return idConfiguration;
	}

	public void setIdConfiguration(long idConfiguration) {
		this.idConfiguration = idConfiguration;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public ConfigurationDto(long idConfiguration, String key, String value, String description, int type) {
		super();
		this.idConfiguration = idConfiguration;
		this.key = key;
		this.value = value;
		this.description = description;
		this.type = type;
	}

	public ConfigurationDto() {
	}

	@Override
	public String toString() {
		return "ConfigurationDto [idConfiguration=" + idConfiguration + ", key=" + key + ", value=" + value
				+ ", description=" + description + ", type=" + type + "]";
	}
	
	
}
