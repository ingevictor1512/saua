package com.mx.saua.dto;

import java.io.Serializable;
import java.util.List;

/**
 * 
 * @author Benito Morales
 * @version 1.0
 */
public class DocumentDto implements Serializable{

	/**
	 * Serializador de la clase
	 */
	private static final long serialVersionUID = -9213539822551597400L;
	private long id;
	private String realName;
	private String viewName;
	private String description;
	private String authorizationsRole;
	private int typeDocument;
	private List<DocumentDto> documentsDto;
	private List<DocumentDto> documentsDtoSchedule;
	private List<DocumentDto> documentsDtoMap;
	private boolean adminRole;
	private boolean studentRole;
	private boolean tutorRole;
	private boolean coordinatorRole;
	private boolean teacherRole;
	private boolean beforePeriod;
	private boolean afterPeriod;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getRealName() {
		return realName;
	}

	public void setRealName(String realName) {
		this.realName = realName;
	}

	public String getViewName() {
		return viewName;
	}

	public void setViewName(String viewName) {
		this.viewName = viewName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<DocumentDto> getDocumentsDto() {
		return documentsDto;
	}

	public void setDocumentsDto(List<DocumentDto> documentsDto) {
		this.documentsDto = documentsDto;
	}

	public List<DocumentDto> getDocumentsDtoSchedule() {
		return documentsDtoSchedule;
	}

	public void setDocumentsDtoSchedule(List<DocumentDto> documentsDtoSchedule) {
		this.documentsDtoSchedule = documentsDtoSchedule;
	}

	public List<DocumentDto> getDocumentsDtoMap() {
		return documentsDtoMap;
	}

	public void setDocumentsDtoMap(List<DocumentDto> documentsDtoMap) {
		this.documentsDtoMap = documentsDtoMap;
	}

	public String getAuthorizationsRole() {
		return authorizationsRole;
	}

	public void setAuthorizationsRole(String authorizationsRole) {
		this.authorizationsRole = authorizationsRole;
	}
	
	public boolean isAdminRole() {
		return adminRole;
	}

	public void setAdminRole(boolean adminRole) {
		this.adminRole = adminRole;
	}

	public boolean isStudentRole() {
		return studentRole;
	}

	public void setStudentRole(boolean studentRole) {
		this.studentRole = studentRole;
	}

	public boolean isTutorRole() {
		return tutorRole;
	}

	public void setTutorRole(boolean tutorRole) {
		this.tutorRole = tutorRole;
	}

	public boolean isCoordinatorRole() {
		return coordinatorRole;
	}

	public void setCoordinatorRole(boolean coordinatorRole) {
		this.coordinatorRole = coordinatorRole;
	}

	public boolean isTeacherRole() {
		return teacherRole;
	}

	public void setTeacherRole(boolean teacherRole) {
		this.teacherRole = teacherRole;
	}

	public DocumentDto() {
	}
	
	public boolean isBeforePeriod() {
		return beforePeriod;
	}

	public void setBeforePeriod(boolean beforePeriod) {
		this.beforePeriod = beforePeriod;
	}

	public boolean isAfterPeriod() {
		return afterPeriod;
	}

	public void setAfterPeriod(boolean afterPeriod) {
		this.afterPeriod = afterPeriod;
	}

	public int getTypeDocument() {
		return typeDocument;
	}

	public void setTypeDocument(int typeDocument) {
		this.typeDocument = typeDocument;
	}

	public DocumentDto(long id, String realName, String viewName, String description, String authorizationsRole, int typeDocument) {
		super();
		this.id = id;
		this.realName = realName;
		this.viewName = viewName;
		this.description = description;
		this.authorizationsRole = authorizationsRole;
		this.typeDocument = typeDocument;
	}

	@Override
	public String toString() {
		return "DocumentDto [id=" + id + ", realName=" + realName + ", viewName=" + viewName + ", description="
				+ description + ", authorizationsRole=" + authorizationsRole + ", typeDocument=" + typeDocument
				+ ", documentsDto=" + documentsDto + ", adminRole=" + adminRole + ", studentRole=" + studentRole
				+ ", tutorRole=" + tutorRole + ", coordinatorRole=" + coordinatorRole + ", teacherRole=" + teacherRole
				+ ", beforePeriod=" + beforePeriod + ", afterPeriod=" + afterPeriod + "]";
	}

}
