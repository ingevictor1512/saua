package com.mx.saua.dto;

import org.springframework.http.HttpStatus;

import java.io.Serializable;

public class StudentResponseDto implements Serializable {
    private static final long serialVersionUID = 1L;
	private String register;
    private String fullname;
    private String career;
    private String beca;
    private String imagePath;
    private String errorText;
    private HttpStatus errorCode;
    private byte[] photo;
    public StudentResponseDto(){
    }

    public StudentResponseDto(String register, String fullname, String career, String beca, String imagePath, HttpStatus errorCode, String errorText) {
        this.register = register;
        this.fullname = fullname;
        this.career = career;
        this.beca = beca;
        this.imagePath = imagePath;
        this.errorCode = errorCode;
        this.errorText = errorText;
    }

    public String getRegister() {
        return register;
    }

    public void setRegister(String register) {
        this.register = register;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getCareer() {
        return career;
    }

    public void setCareer(String career) {
        this.career = career;
    }

    public String getBeca() {
        return beca;
    }

    public void setBeca(String beca) {
        this.beca = beca;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public String getErrorText() {
        return errorText;
    }

    public void setErrorText(String errorText) {
        this.errorText = errorText;
    }

    public HttpStatus getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(HttpStatus errorCode) {
        this.errorCode = errorCode;
    }

    public byte[] getPhoto() {
        return photo;
    }

    public void setPhoto(byte[] photo) {
        this.photo = photo;
    }
}
