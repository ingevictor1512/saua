package com.mx.saua.dto;

import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
public class GrantDto implements Serializable{

    private static final long serialVersionUID = 1L;

    private Long id;
    private String grant;
    private String description;
    /* No interactuan con la Entity */

    public GrantDto() {
        this.id = -1L;
    }

    @Override
    public String toString() {
        return "GrantDto [description=" + description + ", grant=" + grant + ", id=" + id + "]";
    }
 
}
