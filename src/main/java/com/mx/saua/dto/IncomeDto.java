package com.mx.saua.dto;

import java.io.Serializable;

public class IncomeDto implements Serializable {
	/**
	 * DTO que permite guardar la informacion de las etapas de las uaps
	 * @author Vic
	 */
	private static final long serialVersionUID = 1L;
	
	private long id;
	private String description;
	
	public IncomeDto() {
		this.id=-1L;
	}

	public IncomeDto(long id, String description) {
		this.id = id;
		this.description = description;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return "PhaseDto [id=" + id + ", description=" + description + "]";
	}
	
	
}
