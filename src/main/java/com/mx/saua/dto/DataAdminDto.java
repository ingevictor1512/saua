package com.mx.saua.dto;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * @author Victor FG
 * @version 1.0
 */
@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Data
public class DataAdminDto implements Serializable {
	/**
	 * Serializador de la clase
	 */
	private static final long serialVersionUID = -1687751696632432535L;
	
	private long id;
	private int surveyAnswered;
	private int totalStudents;
	private int totalAutosurveyAnswered;
	private int totalsurveyDocentAnswered;

}
