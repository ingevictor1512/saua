package com.mx.saua.dto;

import java.io.Serializable;
import java.util.List;

import com.mx.saua.entity.InquestQuestion;

public class InquestQuestionDto extends PatternDto implements Serializable{

	/**
	 * Serial de la clase
	 */
	private static final long serialVersionUID = 5823461912783124459L;
	
	private long id;
	private InquestDto inquest;
	private QuestionDto question;
	private int order;
	private List<InquestQuestion> inquestQuestionList;
	private long idInquest;
	private int currentPage;
	private String rating;
	private long idq;
	private String qtype;
	private String anstext;
	private String membershipRadios;//Guarda la respuesta de opcion si/no
	private String optionSelected;
	private String multselect;
	private String optanswselect;
	private int currentPageView;
	private String ansnum;
	
	public InquestQuestionDto() {
		
	}
	
	public InquestQuestionDto(long id, InquestDto inquest, QuestionDto question, int order,
			List<InquestQuestion> inquestQuestionList, long idInquest, int currentPage, String rating, long idq,
			String qtype, String anstext, String ansnum) {
		super();
		this.id = id;
		this.inquest = inquest;
		this.question = question;
		this.order = order;
		this.inquestQuestionList = inquestQuestionList;
		this.idInquest = idInquest;
		this.currentPage = currentPage;
		this.rating = rating;
		this.idq = idq;
		this.qtype = qtype;
		this.anstext = anstext;
		this.ansnum = ansnum;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public InquestDto getInquest() {
		return inquest;
	}

	public void setInquest(InquestDto inquest) {
		this.inquest = inquest;
	}

	public QuestionDto getQuestion() {
		return question;
	}

	public void setQuestion(QuestionDto question) {
		this.question = question;
	}

	public int getOrder() {
		return order;
	}

	public void setOrder(int order) {
		this.order = order;
	}

	public List<InquestQuestion> getInquestQuestionList() {
		return inquestQuestionList;
	}

	public void setInquestQuestionList(List<InquestQuestion> inquestQuestionList) {
		this.inquestQuestionList = inquestQuestionList;
	}

	public long getIdInquest() {
		return idInquest;
	}

	public void setIdInquest(long idInquest) {
		this.idInquest = idInquest;
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	public String getRating() {
		return rating;
	}

	public void setRating(String rating) {
		this.rating = rating;
	}

	public long getIdq() {
		return idq;
	}

	public void setIdq(long idq) {
		this.idq = idq;
	}

	public String getQtype() {
		return qtype;
	}

	public void setQtype(String qtype) {
		this.qtype = qtype;
	}

	public String getAnstext() {
		return anstext;
	}

	public void setAnstext(String anstext) {
		this.anstext = anstext;
	}

	public String getMembershipRadios() {
		return membershipRadios;
	}

	public void setMembershipRadios(String membershipRadios) {
		this.membershipRadios = membershipRadios;
	}

	public String getOptionSelected() {
		return optionSelected;
	}

	public void setOptionSelected(String optionSelected) {
		this.optionSelected = optionSelected;
	}

	public String getMultselect() {
		return multselect;
	}

	public void setMultselect(String multselect) {
		this.multselect = multselect;
	}

	public String getOptanswselect() {
		return optanswselect;
	}

	public void setOptanswselect(String optanswselect) {
		this.optanswselect = optanswselect;
	}

	public int getCurrentPageView() {
		return currentPageView;
	}

	public void setCurrentPageView(int currentPageView) {
		this.currentPageView = currentPageView;
	}

	public String getAnsnum() {
		return ansnum;
	}

	public void setAnsnum(String ansnum) {
		this.ansnum = ansnum;
	}

	@Override
	public String toString() {
		return "InquestQuestionDto [id=" + id + ", inquest=" + inquest + ", question=" + question + ", order=" + order
				+ ", inquestQuestionList=" + inquestQuestionList + "]";
	}

}
