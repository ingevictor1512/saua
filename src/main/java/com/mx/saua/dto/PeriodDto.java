package com.mx.saua.dto;

import java.io.Serializable;
import java.util.Date;

public class PeriodDto implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long id;
	private Date initialDate;
	private Date finalDate;
	private String schoolPeriod;
	private boolean activePeriod;
	private boolean activePeriodAdmin;
	private String schoolCycle;
	private Date registerDate;
	private Date editdate;
	private String initialDateString;
	private String finalDateString;
	
	public PeriodDto() {
		this.id = -1L;
	}

	public PeriodDto(Long id, Date initialDate, Date finalDate, String schoolPeriod, boolean activePeriod,
			boolean activePeriodAdmin, String schoolCycle, Date registerDate, Date editdate) {
		super();
		this.id = id;
		this.initialDate = initialDate;
		this.finalDate = finalDate;
		this.schoolPeriod = schoolPeriod;
		this.activePeriod = activePeriod;
		this.activePeriodAdmin = activePeriodAdmin;
		this.schoolCycle = schoolCycle;
		this.registerDate = registerDate;
		this.editdate = editdate;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getInitialDate() {
		return initialDate;
	}

	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}

	public Date getFinalDate() {
		return finalDate;
	}

	public void setFinalDate(Date finalDate) {
		this.finalDate = finalDate;
	}

	public String getSchoolPeriod() {
		return schoolPeriod;
	}

	public void setSchoolPeriod(String schoolPeriod) {
		this.schoolPeriod = schoolPeriod;
	}

	public boolean isActivePeriod() {
		return activePeriod;
	}

	public void setActivePeriod(boolean activePeriod) {
		this.activePeriod = activePeriod;
	}

	public boolean isActivePeriodAdmin() {
		return activePeriodAdmin;
	}

	public void setActivePeriodAdmin(boolean activePeriodAdmin) {
		this.activePeriodAdmin = activePeriodAdmin;
	}

	public String getSchoolCycle() {
		return schoolCycle;
	}

	public void setSchoolCycle(String schoolCycle) {
		this.schoolCycle = schoolCycle;
	}

	public Date getRegisterDate() {
		return registerDate;
	}

	public void setRegisterDate(Date registerDate) {
		this.registerDate = registerDate;
	}

	public Date getEditdate() {
		return editdate;
	}

	public void setEditdate(Date editdate) {
		this.editdate = editdate;
	}

	public String getInitialDateString() {
		return initialDateString;
	}

	public void setInitialDateString(String initialDateString) {
		this.initialDateString = initialDateString;
	}

	public String getFinalDateString() {
		return finalDateString;
	}

	public void setFinalDateString(String finalDateString) {
		this.finalDateString = finalDateString;
	}

	@Override
	public String toString() {
		return "PeriodDto [id=" + id + ", initialDate=" + initialDate + ", finalDate=" + finalDate + ", schoolPeriod="
				+ schoolPeriod + ", activePeriod=" + activePeriod + ", activePeriodAdmin=" + activePeriodAdmin
				+ ", schoolCycle=" + schoolCycle + ", registerDate=" + registerDate + ", editdate=" + editdate 
				+ "]";
	}
	
	
}
