package com.mx.saua.dto;

import java.io.Serializable;
import java.util.Date;

public class TutoringLowDto implements Serializable{
	
	/**
	 * @author Vic
	 */
	private static final long serialVersionUID = 1L;
	private long id;
	private String description;
	private Date registerDate;
	private Date editdate;
	
	public TutoringLowDto() {
		this.id = -1L;
	}
	public TutoringLowDto(long id, String description, Date registerDate, Date editdate) {
		super();
		this.id = id;
		this.description = description;
		this.registerDate = registerDate;
		this.editdate = editdate;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Date getRegisterDate() {
		return registerDate;
	}
	public void setRegisterDate(Date registerDate) {
		this.registerDate = registerDate;
	}
	public Date getEditdate() {
		return editdate;
	}
	public void setEditdate(Date editdate) {
		this.editdate = editdate;
	}
	@Override
	public String toString() {
		return "TutoringLowDto [id=" + id + ", description=" + description + ", registerDate=" + registerDate
				+ ", editdate=" + editdate + "]";
	}
	

}
