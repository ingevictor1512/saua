package com.mx.saua.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class GroupDto implements Serializable {

	/**
	 * Serial de la clase
	 */
	private static final long serialVersionUID = 1L;

	private long id;
	private String name;
	private StudentDto groupLeader;
	private String register;
	private String location;
	private String turn;
	private String groupType;
	private String abbreviation;
	private String nextGroup;
	private boolean promoted;
	private PeriodDto periodDto;
	private List<GroupDto> groupsDto;
	private List<ProgramDto> programsDto;
	private List<PeriodDto> periodsDto;
	private ProgramDto educationalProgramDto;
	private String leaderCompleteName;
	private String idGroup;
	private String idUap;
	private String idGroupPromote;
	private String filename;

	public GroupDto() {
		this.id = -1;
		this.register = "";
		this.groupLeader = new StudentDto();
		this.programsDto = new ArrayList<ProgramDto>();
		this.periodsDto = new ArrayList<PeriodDto>();
	}

	public GroupDto(long id, String name, StudentDto groupLeader, String location, String turn, String groupType,
			String abbreviation, PeriodDto periodDto, List<GroupDto> groupsDto, 
			List<ProgramDto> programsDto,ProgramDto programDto, List<PeriodDto> periodsDto, String register, String nextGroup) {
		super();
		this.id = id;
		this.name = name;
		this.groupLeader = groupLeader;
		this.location = location;
		this.turn = turn;
		this.groupType = groupType;
		this.abbreviation = abbreviation;
		this.periodDto = periodDto;
		this.groupsDto = groupsDto;
		this.programsDto = programsDto;
		this.educationalProgramDto = programDto;
		this.periodsDto = periodsDto;
		this.register = register;
		this.nextGroup = nextGroup;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public StudentDto getGroupLeader() {
		return groupLeader;
	}

	public void setGroupLeader(StudentDto groupLeader) {
		this.groupLeader = groupLeader;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getTurn() {
		return turn;
	}

	public void setTurn(String turn) {
		this.turn = turn;
	}

	public String getGroupType() {
		return groupType;
	}

	public void setGroupType(String groupType) {
		this.groupType = groupType;
	}

	public String getAbbreviation() {
		return abbreviation;
	}

	public void setAbbreviation(String abbreviation) {
		this.abbreviation = abbreviation;
	}

	public PeriodDto getPeriodDto() {
		return periodDto;
	}

	public void setPeriodDto(PeriodDto periodDto) {
		this.periodDto = periodDto;
	}

	public List<GroupDto> getGroupsDto() {
		return groupsDto;
	}

	public void setGroupsDto(List<GroupDto> groupsDto) {
		this.groupsDto = groupsDto;
	}

	public List<ProgramDto> getProgramsDto() {
		return programsDto;
	}

	public void setProgramsDto(List<ProgramDto> programsDto) {
		this.programsDto = programsDto;
	}

	public ProgramDto getEducationalProgramDto() {
		return educationalProgramDto;
	}

	public void setEducationalProgramDto(ProgramDto educationalProgramDto) {
		this.educationalProgramDto = educationalProgramDto;
	}

	public List<PeriodDto> getPeriodsDto() {
		return periodsDto;
	}

	public void setPeriodsDto(List<PeriodDto> periodsDto) {
		this.periodsDto = periodsDto;
	}

	public String getRegister() {
		return register;
	}

	public void setRegister(String register) {
		this.register = register;
	}
	
	public String getLeaderCompleteName() {
		return leaderCompleteName;
	}

	public void setLeaderCompleteName(String leaderCompleteName) {
		this.leaderCompleteName = leaderCompleteName;
	}
	
	public String getIdGroup() {
		return idGroup;
	}

	public void setIdGroup(String idGroup) {
		this.idGroup = idGroup;
	}

	public String getIdUap() {
		return idUap;
	}

	public void setIdUap(String idUap) {
		this.idUap = idUap;
	}

	public String getNextGroup() {
		return nextGroup;
	}

	public void setNextGroup(String nextGroup) {
		this.nextGroup = nextGroup;
	}

	public boolean isPromoted() {
		return promoted;
	}

	public void setPromoted(boolean promoted) {
		this.promoted = promoted;
	}

	public String getIdGroupPromote() {
		return idGroupPromote;
	}

	public void setIdGroupPromote(String idGroupPromote) {
		this.idGroupPromote = idGroupPromote;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	@Override
	public String toString() {
		StringBuilder sbuilder = new StringBuilder();
		sbuilder.append("GroupDto [id="+id);
		sbuilder.append(", name="+name);
		sbuilder.append(", register="+register);
		sbuilder.append(", location="+location);
		sbuilder.append(", turn="+turn);
		sbuilder.append(", groupType="+groupType);
		sbuilder.append(", abbreviation="+abbreviation);
		sbuilder.append(", periodDto="+(periodDto != null?periodDto.toString():"null"));
		sbuilder.append(", groupsDto="+(groupsDto != null?groupsDto.toString():"null"));
		sbuilder.append(", programsDto="+(programsDto != null?programsDto.toString():"null"));
		sbuilder.append(", idGroup="+idGroup);
		sbuilder.append(", idUap="+idUap);
		sbuilder.append(", nextGroup="+nextGroup);
		sbuilder.append(", promoted="+promoted);
		sbuilder.append(", idGroupPromote="+ idGroupPromote);
		sbuilder.append("]");
		return sbuilder.toString();
	}
	
}
