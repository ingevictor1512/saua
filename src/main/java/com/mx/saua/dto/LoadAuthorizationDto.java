package com.mx.saua.dto;

import java.io.Serializable;
import java.util.Date;

import com.mx.saua.entity.Period;
import com.mx.saua.entity.Student;

/**
 * 
 * @author Victor Francisco
 * @version 1.0
 *
 */
public class LoadAuthorizationDto implements Serializable{
	
	/**
	 * Dto de la entity LoadAuthorization
	 */
	private static final long serialVersionUID = 1L;
	private long id;
	private String authorizationKey;
	private boolean authorization;
	private String observations;
	private Period period;
	private Student student;
	private Date registerDate;
	private Date editdate;
	
	public LoadAuthorizationDto() {
		id = 0;
	}

	public LoadAuthorizationDto(long id, String authorizationKey, boolean authorization, String observations,
			Period period, Student student, Date registerDate, Date editdate) {
		super();
		this.id = id;
		this.authorizationKey = authorizationKey;
		this.authorization = authorization;
		this.observations = observations;
		this.period = period;
		this.student = student;
		this.registerDate = registerDate;
		this.editdate = editdate;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getAuthorizationKey() {
		return authorizationKey;
	}

	public void setAuthorizationKey(String authorizationKey) {
		this.authorizationKey = authorizationKey;
	}

	public boolean isAuthorization() {
		return authorization;
	}

	public void setAuthorization(boolean authorization) {
		this.authorization = authorization;
	}

	public String getObservations() {
		return observations;
	}

	public void setObservations(String observations) {
		this.observations = observations;
	}

	public Period getPeriod() {
		return period;
	}

	public void setPeriod(Period period) {
		this.period = period;
	}

	public Student getStudent() {
		return student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}

	public Date getRegisterDate() {
		return registerDate;
	}

	public void setRegisterDate(Date registerDate) {
		this.registerDate = registerDate;
	}

	public Date getEditdate() {
		return editdate;
	}

	public void setEditdate(Date editdate) {
		this.editdate = editdate;
	}

	@Override
	public String toString() {
		return "LoadAuthorizationDto [id=" + id + ", authorizationKey=" + authorizationKey + ", authorization="
				+ authorization + ", observations=" + observations + ", period=" + period + ", student=" + student
				+ ", registerDate=" + registerDate + ", editdate=" + editdate + "]";
	}
	
}
