package com.mx.saua.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.mx.saua.entity.Phase;
import com.mx.saua.entity.Program;
import com.mx.saua.entity.UapType;

public class UapDto implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -687167052637045106L;
	
	private String key;
	private String name;
	private long credit;
	private String common_code;
	private Program educationalProgram;
	private UapType uapType;
	private Phase phaseUaps;
	private List<ProgramDto> programsDto;
	private List<UapTypeDto> uapTypeDtos;
	private List<PhaseDto> phaseDtos;
	private List<UapDto> uapDtos;
	private Date registerDate;
	private Date editdate;
	/* No tienen interaccion con la entity */
	private boolean newRegister;
	private List<StudentDto> studentsDto;
	
	public UapDto() {
		this.newRegister = true;
	}

	public UapDto(String key, String name, long credit, String common_code, Program educationalProgram, UapType uapType,
			Phase phaseUaps, List<ProgramDto> programsDto, List<UapTypeDto> uapTypeDtos, List<PhaseDto> phaseDtos,
			List<UapDto> uapDtos, Date registerDate, Date editdate, boolean newRegister) {
		super();
		this.key = key;
		this.name = name;
		this.credit = credit;
		this.common_code = common_code;
		this.educationalProgram = educationalProgram;
		this.uapType = uapType;
		this.phaseUaps = phaseUaps;
		this.programsDto = programsDto;
		this.uapTypeDtos = uapTypeDtos;
		this.phaseDtos = phaseDtos;
		this.uapDtos = uapDtos;
		this.registerDate = registerDate;
		this.editdate = editdate;
		this.newRegister = newRegister;
	}



	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getCredit() {
		return credit;
	}

	public void setCredit(long credit) {
		this.credit = credit;
	}

	public String getCommon_code() {
		return common_code;
	}

	public void setCommon_code(String common_code) {
		this.common_code = common_code;
	}

	public Program getEducationalProgram() {
		return educationalProgram;
	}

	public void setEducationalProgram(Program educationalProgram) {
		this.educationalProgram = educationalProgram;
	}

	public UapType getUapType() {
		return uapType;
	}

	public void setUapType(UapType uapType) {
		this.uapType = uapType;
	}

	public Phase getPhaseUaps() {
		return phaseUaps;
	}

	public void setPhaseUaps(Phase phaseUaps) {
		this.phaseUaps = phaseUaps;
	}

	public List<ProgramDto> getProgramsDto() {
		return programsDto;
	}

	public void setProgramsDto(List<ProgramDto> programsDto) {
		this.programsDto = programsDto;
	}

	public List<UapTypeDto> getUapTypeDtos() {
		return uapTypeDtos;
	}

	public void setUapTypeDtos(List<UapTypeDto> uapTypeDtos) {
		this.uapTypeDtos = uapTypeDtos;
	}

	public List<PhaseDto> getPhaseDtos() {
		return phaseDtos;
	}

	public void setPhaseDtos(List<PhaseDto> phaseDtos) {
		this.phaseDtos = phaseDtos;
	}

	public List<UapDto> getUapDtos() {
		return uapDtos;
	}

	public void setUapDtos(List<UapDto> uapDtos) {
		this.uapDtos = uapDtos;
	}

	public Date getRegisterDate() {
		return registerDate;
	}

	public void setRegisterDate(Date registerDate) {
		this.registerDate = registerDate;
	}

	public Date getEditdate() {
		return editdate;
	}

	public void setEditdate(Date editdate) {
		this.editdate = editdate;
	}

	public boolean isNewRegister() {
		return newRegister;
	}

	public void setNewRegister(boolean newRegister) {
		this.newRegister = newRegister;
	}

	public List<StudentDto> getStudentsDto() {
		return studentsDto;
	}

	public void setStudentsDto(List<StudentDto> studentsDto) {
		this.studentsDto = studentsDto;
	}

	@Override
	public String toString() {
		return "UapDto [key=" + key + ", name=" + name + ", credit=" + credit + ", common_code=" + common_code
				+ ", educationalProgram=" + educationalProgram + ", uapType=" + uapType + ", phaseUaps=" + phaseUaps
				+ ", programsDto=" + programsDto + ", uapTypeDtos=" + uapTypeDtos + ", phaseDtos=" + phaseDtos
				+ ", uapDtos=" + uapDtos + ", registerDate=" + registerDate + ", editdate=" + editdate
				+ ", newRegister=" + newRegister + ", studentsDto=" + studentsDto + "]";
	}

	

}
