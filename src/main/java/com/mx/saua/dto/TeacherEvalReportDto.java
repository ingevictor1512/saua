package com.mx.saua.dto;

import java.io.Serializable;
import java.util.List;

public class TeacherEvalReportDto implements Serializable{
	/**
	 * Serial de la clase
	 */
	private static final long serialVersionUID = 8355433559675813004L;
	private long id;
	private OfferedClassDto offeredClass;
	private int totalEvaluation;
	private List<TeacherEvalReportDto> teacherEvalReportDtos;
	private List<ResultDto> resultDtos;
	
	public List<TeacherEvalReportDto> getTeacherEvalReportDtos() {
		return teacherEvalReportDtos;
	}

	public void setTeacherEvalReportDtos(List<TeacherEvalReportDto> teacherEvalReportDtos) {
		this.teacherEvalReportDtos = teacherEvalReportDtos;
	}

	public TeacherEvalReportDto() {

	}

	public TeacherEvalReportDto(long id, OfferedClassDto offeredClass, int totalEvaluation) {
		super();
		this.id = id;
		this.offeredClass = offeredClass;
		this.totalEvaluation = totalEvaluation;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public OfferedClassDto getOfferedClass() {
		return offeredClass;
	}

	public void setOfferedClass(OfferedClassDto offeredClass) {
		this.offeredClass = offeredClass;
	}

	public int getTotalEvaluation() {
		return totalEvaluation;
	}

	public void setTotalEvaluation(int totalEvaluation) {
		this.totalEvaluation = totalEvaluation;
	}


	public List<ResultDto> getResultDtos() {
		return resultDtos;
	}

	public void setResultDtos(List<ResultDto> resultDtos) {
		this.resultDtos = resultDtos;
	}

	@Override
	public String toString() {
		return "TeacherEvalReportDto [id=" + id + ", offeredClass=" + offeredClass + ", totalEvaluation="
				+ totalEvaluation + "]";
	}
		

}
