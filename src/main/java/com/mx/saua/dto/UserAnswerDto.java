package com.mx.saua.dto;

import java.io.Serializable;
import java.util.List;

public class UserAnswerDto implements Serializable{

	/**
	 * Serializador de la clase
	 */
	private static final long serialVersionUID = 1528633156327822409L;
	private long id;
	private AnswerDto answer;
	private UserDto user;
	private String personalizeAnswer;
	private QuestionDto question;
	private String especification;
	private List<UserAnswerDto> userAnswers;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public AnswerDto getAnswer() {
		return answer;
	}

	public void setAnswer(AnswerDto answer) {
		this.answer = answer;
	}

	public UserDto getUser() {
		return user;
	}

	public void setUser(UserDto user) {
		this.user = user;
	}

	public String getPersonalizeAnswer() {
		return personalizeAnswer;
	}

	public void setPersonalizeAnswer(String personalizeAnswer) {
		this.personalizeAnswer = personalizeAnswer;
	}

	public QuestionDto getQuestion() {
		return question;
	}

	public void setQuestion(QuestionDto question) {
		this.question = question;
	}

	public String getEspecification() {
		return especification;
	}

	public void setEspecification(String especification) {
		this.especification = especification;
	}

	public List<UserAnswerDto> getUserAnswers() {
		return userAnswers;
	}

	public void setUserAnswers(List<UserAnswerDto> userAnswers) {
		this.userAnswers = userAnswers;
	}
}
