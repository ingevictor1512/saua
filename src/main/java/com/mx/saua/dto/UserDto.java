package com.mx.saua.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.mx.saua.enums.AuthorityEnum;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@Builder
@AllArgsConstructor
public class UserDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private long idUser;
	private String username;
	private String password;
	private String authority;

	// Aqui van a ir las entidades de Studen y Administrativo

	private Date creationDate;
	private Date editDate;
	private boolean active;
	private boolean firstLogin;
	private Date lastLogin;
	private List<UserDto> users;
	private String verifyPassword;
	private String email;
	private String hash;
	private StudentDto studentDto;
	private TeacherDto teacherDto;
	private boolean inquest;
	private String phone;
	private TeacherDto tutor;
	private boolean reset;
	private List<InquestRoleDto> inquestRoles;
	private CoffeeShopDto coffeeShop;
	

	public UserDto() {
		this.idUser = -1L;
		this.reset = false;
	}

	/*public UserDto(long idUser, String username, String password, String authority, Date creationDate, Date editDate,
			boolean active, boolean firstLogin, Date lastLogin, List<UserDto> users, String email, String hash, StudentDto studentDto, 
			TeacherDto teacherDto, boolean inquest, String phone) {
		super();
		this.idUser = idUser;
		this.username = username;
		this.password = password;
		this.authority = authority;
		this.creationDate = creationDate;
		this.editDate = editDate;
		this.active = active;
		this.firstLogin = firstLogin;
		this.lastLogin = lastLogin;
		this.users = users;
		this.email = email;
		this.hash = hash;
		this.studentDto = studentDto;
		this.teacherDto = teacherDto;
		this.inquest = inquest;
		this.phone = phone;
	}*/

	public void translateAutorities() {
		switch(authority){
		case "STUDENT":
			this.authority = AuthorityEnum.STUDENT.getValue();
			break;
		case "ADMIN":
			this.authority = AuthorityEnum.ADMIN.getValue();
			break;
		case "TUTOR":
			this.authority = AuthorityEnum.TUTOR.getValue();
			break;
		case "TUTORING_COORDINATOR":
			this.authority = AuthorityEnum.TUTORING_COORDINATOR.getValue();
			break;
		case "TEACHER":
			this.authority = AuthorityEnum.TEACHER.getValue();
			break;
		case "CASHIER":
			this.authority = AuthorityEnum.CASHIER.getValue();
			break;
		default:
			this.authority = AuthorityEnum.UNKNOWN.getValue();
		}
	}

	/*@Override
	public String toString() {
		return "UserDto [idUser=" + idUser + ", username=" + username + ", password=" + password + ", authority="
				+ authority + ", creationDate=" + creationDate + ", editDate=" + editDate + ", active=" + active
				+ ", firstLogin=" + firstLogin + ", lastLogin=" + lastLogin + ", users=" + users + ", verifyPassword="
				+ verifyPassword + ", email=" + email + ", hash=" + hash + ", studentDto=" + studentDto
				+ ", teacherDto=" + teacherDto + ", inquest=" + inquest + ", phone=" + phone + "]";
	}*/
	
}
