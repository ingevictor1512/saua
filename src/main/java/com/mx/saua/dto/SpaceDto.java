package com.mx.saua.dto;

import java.io.Serializable;

import com.mx.saua.entity.User;

/**
 * 
 * @author Victor FG
 * @version 1.0
 */
public class SpaceDto implements Serializable{

	/**
	 * Serializador de la clase
	 */
	private static final long serialVersionUID = 3948605628293113177L;
	
	private long id;
	private String labName;
	private String description;
	private User user;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getLabName() {
		return labName;
	}
	public void setLabName(String labName) {
		this.labName = labName;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}

	
}
