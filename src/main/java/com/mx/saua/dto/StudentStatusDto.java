package com.mx.saua.dto;

import java.io.Serializable;


/**
 * Dto para StudentStatus
 * @author Benito Morales
 * @version 1.0
 *
 */
public class StudentStatusDto implements Serializable{

	/**
	 * Serial de la clase
	 */
	private static final long serialVersionUID = 1L;

	private long id;
	private String description;
	private String comments;
	
	public StudentStatusDto() {
		
	}

	public StudentStatusDto(long id, String description, String comments) {
		super();
		this.id = id;
		this.description = description;
		this.comments = comments;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	@Override
	public String toString() {
		return "StudentStatusDto [id=" + id + ", description=" + description + ", comments=" + comments + "]";
	}
}
