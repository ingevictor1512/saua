package com.mx.saua.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.mx.saua.entity.OfferedClass;

/**
 * 
 * @author Benito Morales
 * @version 1.0
 *
 */
public class OfferedClassDto extends PatternDto implements Serializable{

	/**
	 * Serial de la clase
	 */
	private static final long serialVersionUID = 8355433559675813004L;

	private long id;
	private PeriodDto period;
	private TeacherDto teacher;
	private ProgramDto program;
	private int uapCharge;
	private int semester;
	private int maxCharge;
	private int minCharge;
	private UapDto uap;
	private GroupDto group;
	private List<PeriodDto> periods;
	private List<TeacherDto> teachers;
	private List<UapDto> uaps;
	private List<GroupDto> groups;
	private List<ProgramDto> programs;
	private List<OfferedClassDto> offeredClasses;
	//Para reporte estadistico
	private long requested;
	private long total;
	private String searchBy;
	private String searchByValue;
	private List<String> comboSearch;
	private List<OfferedClass> offeredClassList;
	
	
	public OfferedClassDto() {
		id = 0;
		uaps = new ArrayList<UapDto>();
		uaps.add(new UapDto("SN", "No disponibles", 0, null, null, null, null, null, null, null, null, null, null, false));
		this.searchBy = "";
		this.searchByValue = "";
	}

	public OfferedClassDto(long id, PeriodDto period, TeacherDto teacher, int uapCharge, int semester, int maxCharge,
			int minCharge, UapDto uap, GroupDto group, List<OfferedClassDto> offeredClasses, long requested, long total) {
		super();
		this.id = id;
		this.period = period;
		this.teacher = teacher;
		this.uapCharge = uapCharge;
		this.semester = semester;
		this.maxCharge = maxCharge;
		this.minCharge = minCharge;
		this.uap = uap;
		this.group = group;
		this.offeredClasses = offeredClasses;
		this.requested = requested;
		this.total = total;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public PeriodDto getPeriod() {
		return period;
	}

	public void setPeriod(PeriodDto period) {
		this.period = period;
	}

	public TeacherDto getTeacher() {
		return teacher;
	}

	public ProgramDto getProgram() {
		return program;
	}

	public void setProgram(ProgramDto program) {
		this.program = program;
	}

	public void setTeacher(TeacherDto teacher) {
		this.teacher = teacher;
	}

	public int getUapCharge() {
		return uapCharge;
	}

	public void setUapCharge(int uapCharge) {
		this.uapCharge = uapCharge;
	}

	public int getSemester() {
		return semester;
	}

	public void setSemester(int semester) {
		this.semester = semester;
	}

	public int getMaxCharge() {
		return maxCharge;
	}

	public void setMaxCharge(int maxCharge) {
		this.maxCharge = maxCharge;
	}

	public int getMinCharge() {
		return minCharge;
	}

	public void setMinCharge(int minCharge) {
		this.minCharge = minCharge;
	}

	public UapDto getUap() {
		return uap;
	}

	public void setUap(UapDto uap) {
		this.uap = uap;
	}

	public GroupDto getGroup() {
		return group;
	}

	public void setGroup(GroupDto group) {
		this.group = group;
	}

	public List<OfferedClassDto> getOfferedClasses() {
		return offeredClasses;
	}

	public void setOfferedClasses(List<OfferedClassDto> offeredClasses) {
		this.offeredClasses = offeredClasses;
	}

	public List<PeriodDto> getPeriods() {
		return periods;
	}

	public void setPeriods(List<PeriodDto> periods) {
		this.periods = periods;
	}

	public List<TeacherDto> getTeachers() {
		return teachers;
	}

	public void setTeachers(List<TeacherDto> teachers) {
		this.teachers = teachers;
	}

	public List<UapDto> getUaps() {
		return uaps;
	}

	public void setUaps(List<UapDto> uaps) {
		this.uaps = uaps;
	}

	public List<GroupDto> getGroups() {
		return groups;
	}

	public void setGroups(List<GroupDto> groups) {
		this.groups = groups;
	}

	public List<ProgramDto> getPrograms() {
		return programs;
	}

	public void setPrograms(List<ProgramDto> programs) {
		this.programs = programs;
	}

	public long getRequested() {
		return requested;
	}

	public void setRequested(long requested) {
		this.requested = requested;
	}

	public long getTotal() {
		return total;
	}

	public void setTotal(long total) {
		this.total = total;
	}

	public String getSearchBy() {
		return searchBy;
	}

	public void setSearchBy(String searchBy) {
		this.searchBy = searchBy;
	}

	public String getSearchByValue() {
		return searchByValue;
	}

	public void setSearchByValue(String searchByValue) {
		this.searchByValue = searchByValue;
	}

	public List<String> getComboSearch() {
		return comboSearch;
	}

	public void setComboSearch(List<String> comboSearch) {
		this.comboSearch = comboSearch;
	}

	public List<OfferedClass> getOfferedClassList() {
		return offeredClassList;
	}

	public void setOfferedClassList(List<OfferedClass> offeredClassList) {
		this.offeredClassList = offeredClassList;
	}

	@Override
	public String toString() {
		return "OfferedClassDto [id=" + id +  ", program=" + program + ", period=" + period + ", teacher=" + teacher + ", uapCharge=" + uapCharge
				+ ", semester=" + semester + ", maxCharge=" + maxCharge + ", minCharge=" + minCharge + ", uap=" + uap
				+ ", group=" + group + ", offeredClasses=" + offeredClasses + "]";
	}
}
