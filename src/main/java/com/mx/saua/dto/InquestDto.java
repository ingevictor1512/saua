package com.mx.saua.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.mx.saua.enums.InquestStatusEnum;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@AllArgsConstructor
@ToString
public class InquestDto implements Serializable {

	/**
	 * Serializador de la clase
	 */
	private static final long serialVersionUID = 1L;

	private long id;
	private String name;
	private String description;
	private int numberQuestions;
	private InquestStatusEnum status;
	private Date creationDate;
	private Date editDate;
	private List<InquestDto> inquests;
	private List<CategoryQuestionDto> categoryQuestionList;
	private String roles;
	private String questionsSelected;
	private String studentInitDate;
	private String studentEndDate;
	private String teacherInitDate;
	private String teacherEndDate;
	private String tutInitDate;
	private String tutEndDate;
	private boolean roleStudentSelected;
	private boolean roleTeacherSelected;
	private boolean roleTutorSelected;
	private List<QuestionDto> questionsListSelected;
	private boolean newRegistry;
	private String actuallyStatus;
	
	public InquestDto() {
		this.id = -1;
		this.questionsSelected = "##";
	}

	public InquestDto(long id, String name, String description, int numberQuestions, InquestStatusEnum status,
			Date creationDate, Date editDate, List<CategoryQuestionDto> categoryQuestionList, String roles, String actuallyStatus) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
		this.numberQuestions = numberQuestions;
		this.status = status;
		this.creationDate = creationDate;
		this.editDate = editDate;
		this.categoryQuestionList = categoryQuestionList;
		this.roles = roles;
		this.actuallyStatus = actuallyStatus;
	}
}
