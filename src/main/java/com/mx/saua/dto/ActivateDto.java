package com.mx.saua.dto;

import com.mx.saua.entity.Period;
import com.mx.saua.entity.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ActivateDto implements Serializable {
    private static final long serialVersionUID = 7207693457397492225L;
	private long id;
    private User user;
    private Period period;
    private String response;
    private Date registerDate;
}
