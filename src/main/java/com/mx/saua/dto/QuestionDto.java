/**
 * 
 */
package com.mx.saua.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.mx.saua.entity.Question;
import com.mx.saua.enums.QuestionBodyType;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @author Benito Morales
 * @version 2.0
 */
@Setter
@Getter
@ToString
public class QuestionDto extends PatternDto implements Serializable {

	/**
	 * Serializadore de la clase
	 */
	private static final long serialVersionUID = 8307614799090497447L;

	private long id;
	private String text;
	private String body;
	private String questionType;
	private String placeHolder;
	private boolean status;
	private Date creationDate;
	private Date editDate;
	private QuestionBodyType questionBodyType;
	private List<AnswerDto> answers;
	private List<QuestionDto> questions;
	private List<String> answersl;
	private String studentRegister;
	private String studentChargeId;
	private String eval;
	private List<Question> questionList;
	private String categoryQuestion;
	private List<String> idsQuestionsSelected;
	private String idsQuestionsSelectedString;
	private InquestDto inquest;
	private long inquestIdAnswered;
	private long order;
	private boolean newRegister;

	public QuestionDto() {
		answersl = new ArrayList<>();
		this.newRegister = false;
	}

	public QuestionDto(long id, String text, String body, String questionType, Date creationDate, Date editDate,
			QuestionBodyType questionBodyType, List<AnswerDto> answers, String placeHolder, boolean status) {
		super();
		this.id = id;
		this.text = text;
		this.body = body;
		this.questionType = questionType;
		this.creationDate = creationDate;
		this.editDate = editDate;
		this.questionBodyType = questionBodyType;
		this.answers = answers;
		this.placeHolder = placeHolder;
		this.status = status;
	}

}
