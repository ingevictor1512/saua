package com.mx.saua.dto;

import java.io.Serializable;
import java.util.Date;

import com.mx.saua.enums.DayEnum;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
public class EntryRegisterDto implements Serializable{

	private static final long serialVersionUID = -8664922672439629464L;

	private long registerId;
	
	private UserDto userStudent;
	
	private Date date;
	
	private DayEnum dayEnum;
	
	private UserDto userCashier;

	private Date registerDate;
	
	private PeriodDto period;
}
