package com.mx.saua.util;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.mx.saua.dto.PatternDto;

/**
 * Servicio utilidad para paginación
 * @author Benito Morales
 * @Verison 1.0
 */
@Service
public class PageableUtils {

	private PageableUtils() {
	}
	
	public static Pageable buildPageable(PatternDto form, int defaultSize, String directionSortName ) {
		String dirStr = Direction.DESC.toString();
		int numPage = 0;
		int size = defaultSize;

		if (form.getDirection() != null) {
			dirStr = form.getDirection();
		}
		if (form.getPage() > 0) {
			numPage = form.getPage();
		}
		if (form.getSize() > 0) {
			size = form.getSize();
		}
		Direction direction = Direction.valueOf(dirStr);
		Pageable pageable = PageRequest.of(numPage, size, Sort.by(direction,directionSortName));
		return pageable;
	}
	
	
	public static String getSortDirection(PatternDto form) {
		if (form.getDirection() != null) {
			return form.getDirection();
		}
		return Direction.DESC.toString();
	}
}
