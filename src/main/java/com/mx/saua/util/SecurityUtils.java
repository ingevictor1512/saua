package com.mx.saua.util;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Utilidades de seguridad
 * 
 * @author Benito Morales Ramirez
 * @version 1.0
 *
 */
public class SecurityUtils {

	private static Logger LOGGER = LoggerFactory.getLogger(SecurityUtils.class);

	/**
	 * Pattern para validar contraseñas
	 */
	private static final Pattern pwdPattern = Pattern
			.compile("((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%&*-]).{8,20})");

	/**
	 * 
	 */
	private static final String CHAR_ARRAY = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!@#$%&*-0123456789";

	/**
	 * Tamaño de contraseñas
	 */
	private static final int PASSWORD_LENGTH = 6;

	private SecurityUtils() {

	}

	/**
	 * Valida contraseña ingresada por usuario
	 * 
	 * @param password contraseña a validar
	 * @return true si es una contraseña valida
	 */
	public static boolean isValidPassword(String password) {
		return pwdPattern.matcher(password).matches();
	}

	/**
	 * Genera contraseña aleatoria
	 * 
	 * @return contraseña generada
	 */
	public static String generateRandomPassword() {
		StringBuilder sb = new StringBuilder();
		List<Integer> idxPassword = new ArrayList<>(PASSWORD_LENGTH);
		ThreadLocalRandom.current().ints(0, CHAR_ARRAY.length()).limit(PASSWORD_LENGTH).forEach(idxPassword::add);
		idxPassword.stream().map(idx -> CHAR_ARRAY.charAt(idx)).forEach(sb::append);
		return sb.toString();
	}
}
