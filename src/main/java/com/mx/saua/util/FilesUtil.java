package com.mx.saua.util;

import java.io.File;

import org.slf4j.Logger;

import com.mx.saua.exception.ServiceException;

/*
 * Clase utileria para el manejo de arvhivos
 * 
 */
public class FilesUtil {

	/**
	 * Elimina los archivos temporales de una carpeta, eliminando la carpeta al finalizar el proceso
	 * @param directoty Directorio que se eliminará
	 * @param LOGGER Logger
	 */
	public static void deleteTemporals(String directory, Logger LOGGER) {
		File path = new File(directory);
        File[] ficheros = path.listFiles();
        File f = null;
        if (path.exists()) {
            for (File fichero : ficheros) {
                f = new File(fichero.toString());
                f.delete();
            }
            path.delete();
        } else {
            LOGGER.info("No existe el directorio.");
        }
        LOGGER.info("Archivos temporales borrados.");
	}
	
	/**
	 * Crea un directorio en caso de no existir
	 * @param temporalDirectory Directorio a crear
	 * @param LOGGER Logger
	 * @return true si el directorio se creo.
	 */
	public static boolean createDirectory(String temporalDirectory, Logger LOGGER) throws ServiceException{
		File directorio = new File(temporalDirectory);
        if (!directorio.exists()) {
            if (directorio.mkdirs()) {
            	LOGGER.info("Directorio creado");
                return true;
            } else {
            	LOGGER.error("Ocurrió un error al crear directorio temporal.");
            	throw new ServiceException("No se pudo crear directorio temporal");
            }
        }
		return false;
	}
}
