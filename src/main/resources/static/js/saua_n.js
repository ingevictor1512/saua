/**
 * 
 */

var SAUA = {};

SAUA.TIMEOUT = 3000;

SAUA.tutor = function(idInput, idValue) {
	$("#" + idInput, "#tutor-form").val(idValue);
	$("#tutor-form").submit();
};
SAUA.list = function(idInput, idValue) {
	$("#" + idInput, "#list-form").val(idValue);
	$("#list-form").submit();
};
SAUA.edit = function(idInput, idValue) {
	$("#" + idInput, "#edit-form").val(idValue);
	$("#edit-form").submit();
};

SAUA.addCharge = function(idInput, idValue) {
	$("#" + idInput, "#charge-form").val(idValue);
	$("#charge-form").submit();
};

SAUA.resetPassword = function(idInput, idValue) {
	$("#" + idInput, "#reset-form").val(idValue);
	$("#reset-form").submit();
};

SAUA.show = function(idInput, idValue) {
	$("#" + idInput, "#show-form").val(idValue);
	$("#show-form").submit();
};

SAUA.authorized = function(idInput, idValue) {
	$("#" + idInput, "#authorized-form").val(idValue);
	$("#authorized-form").submit();
};

SAUA.assign = function(idInput, idValue) {
	$("#" + idInput, "#assign-form").val(idValue);
	$("#assign-form").submit();
};

SAUA.kardex = function(idInput, idValue) {
	$("#" + idInput, "#kardex-form").val(idValue);
	$("#kardex-form").submit();
};

SAUA.validatePassword = function() {
	var passwTyped = $("#password_").val();
	if(passwTyped.match("((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%&*-]).{8,20})"))
		alert("La matrícula es correcta"); 
	else 
		alert("La matrícula NO es correcta");
	
};

SAUA.toDelete = function(idInput, idValue) {
	$("#" + idInput, "#delete-form").val(idValue);
	swal({
        title: 'Eliminar registro',
        text: "Está seguro de eliminar el registro?",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3f51b5',
        cancelButtonColor: '#ff4081',
        confirmButtonText: 'Great ',
        buttons: {
          cancel: {
            text: "Cancelar",
            value: null,
            visible: true,
            className: "btn btn-danger",
            closeModal: true,
          },
          confirm: {
            text: "Aceptar",
            value: true,
            visible: true,
            className: "btn btn-primary",
            closeModal: true
          }
        }
      }).then(function (result) {
    	  if(result){
    		  $("#delete-form").submit();
    	  }
      });
};

SAUA.toAuthorization = function(idInput, idValue) {
	$("#" + idInput, "#authorization-form").val(idValue);
	swal({
        title: 'Autorizar carga acad\u00E9mica',
        text: "Al autorizar la carga del alumno ya no se podrán hacer modificaciones",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3f51b5',
        cancelButtonColor: '#ff4081',
        confirmButtonText: 'Great ',
        buttons: {
          cancel: {
            text: "Cancelar",
            value: null,
            visible: true,
            className: "btn btn-danger",
            closeModal: true,
          },
          confirm: {
            text: "Continuar",
            value: true,
            visible: true,
            className: "btn btn-primary",
            closeModal: true
          }
        }
      }).then(function (result) {
    	  if(result){
    		  $("#authorization-form").submit();
    	  }
      });
};
SAUA.toDesAuthorization = function(idInput, idValue) {
	$("#" + idInput, "#desauthorized-form").val(idValue);
	swal({
        title: 'Remover autorización de carga acad\u00E9mica',
        text: "Al quitar la autorización, el alumno podrá hacer modificaciones (eliminar o agregar uaps)",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3f51b5',
        cancelButtonColor: '#ff4081',
        confirmButtonText: 'Great ',
        buttons: {
          cancel: {
            text: "Cancelar",
            value: null,
            visible: true,
            className: "btn btn-danger",
            closeModal: true,
          },
          confirm: {
            text: "Continuar",
            value: true,
            visible: true,
            className: "btn btn-primary",
            closeModal: true
          }
        }
      }).then(function (result) {
    	  if(result){
    		  $("#desauthorized-form").submit();
    	  }
      });
};

SAUA.selectProgramOnOfferedClass = function(){
	$.ajax({
		url : '/api/admin/uaps/findbyprogram/'+$("#program option:selected").val(),
		method : 'GET',
		cache : false,
		contentType : 'application/json',
		dataType : 'json',
		success :function(data){
			  $("#uap").empty();
			  $("#uap").append('<option value="SN">Seleccione</option>');
		      $.each(data,function(key,registro) {
			        $("#uap").append('<option value='+registro[0]+'>'+registro[1]+'</option>');
			      });        
			    },
			    error: function(data) {
			    	console.log("Error al cargar datos.");
			    }
	});
	$.ajax({
		url : '/api/any/groups/find/'+$("#program option:selected").val(),
		method : 'GET',
		cache : false,
		contentType : 'application/json',
		dataType : 'json',
		success : function(data){
			  $("#group").empty();
			  $("#group").append('<option value="-1">Seleccione</option>');
		      $.each(data,function(key,registro) {
			        $("#group").append('<option value='+registro[0]+'>'+registro[1]+'</option>');
			      });        
			    },
			    error: function(data) {
			    	console.log("Error al cargar datos.");
			    }
	});
}

SAUA.chargePrograms =  function(){
	$.ajax({
		url : '/api/any/programs/find/',
		method : 'GET',
		cache : false,
		contentType : 'application/json',
		dataType : 'json',
		success : function(data){
			  $("#program").empty();
			  $("#program").append('<option value="SN">Seleccione</option>');
		      $.each(data,function(key,registro) {
			        $("#program").append('<option value='+registro[0]+'>'+registro[1]+'</option>');
			      });        
			    },
			    error: function(data) {
			    	console.log("Error al cargar datos.");
			    }
	});
}

SAUA.admChargeTeachers =  function(){
	$.ajax({
		url : '/api/admin/teachers/find/',
		method : 'GET',
		cache : false,
		contentType : 'application/json',
		dataType : 'json',
		success : function(data){
			  $("#teacher").empty();
			  $("#teacher").append('<option value="SN">Seleccione</option>');
		      $.each(data,function(key,registro) {
			        $("#teacher").append('<option value='+registro[0]+'>'+registro[1]+'</option>');
			      });        
			    },
			    error: function(data) {
			    	console.log("Error al cargar datos.");
			    }
	});
}
SAUA.coorChargeTeachers =  function(){
	$.ajax({
		url : '/api/coordinator/teachers/find/',
		method : 'GET',
		cache : false,
		contentType : 'application/json',
		dataType : 'json',
		success : function(data){
			  $("#teacher").empty();
			  $("#teacher").append('<option value="SN">Seleccione</option>');
		      $.each(data,function(key,registro) {
			        $("#teacher").append('<option value='+registro[0]+'>'+registro[1]+'</option>');
			      });        
			    },
			    error: function(data) {
			    	console.log("Error al cargar datos.");
			    }
	});
}

SAUA.admChargeTeachersModal2 =  function(){
	$.ajax({
		url : '/api/admin/teachers/find/',
		method : 'GET',
		cache : false,
		contentType : 'application/json',
		dataType : 'json',
		success : function(data){
			  $("#teacheruno").empty();
			  $("#teacheruno").append('<option value="SN">Seleccione</option>');
		      $.each(data,function(key,registro) {
			        $("#teacheruno").append('<option value='+registro[0]+'>'+registro[1]+'</option>');
			      });        
			    },
			    error: function(data) {
			    	console.log("Error al cargar datos.");
			    }
	});
}

SAUA.coorChargeTeachersModal2 =  function(){
	$.ajax({
		url : '/api/coordinator/teachers/find/',
		method : 'GET',
		cache : false,
		contentType : 'application/json',
		dataType : 'json',
		success : function(data){
			  $("#teacheruno").empty();
			  $("#teacheruno").append('<option value="SN">Seleccione</option>');
		      $.each(data,function(key,registro) {
			        $("#teacheruno").append('<option value='+registro[0]+'>'+registro[1]+'</option>');
			      });        
			    },
			    error: function(data) {
			    	console.log("Error al cargar datos.");
			    }
	});
}

SAUA.selectProgramOnChargeStudent =  function(){
	$.ajax({
		url : '/api/any/groups/find/'+$("#program option:selected").val(),
		method : 'GET',
		cache : false,
		contentType : 'application/json',
		dataType : 'json',
		success : function(data){
			  $("#countStudents").text("");
	          $("#countStudents").hide();
			  $("#group").empty();
			  $("#group").append('<option value="">Seleccione</option>');
			  $("#uaps").empty();
			  $("#uaps").append('<option value="SN">Seleccione</option>');
		      $.each(data,function(key,registro) {
			        $("#group").append('<option value='+registro[0]+'>'+registro[1]+'</option>');
			      });        
			    },
			    error: function(data) {
			    	console.log("Error al cargar datos.");
			    }
	});
}

SAUA.admUapsForGroup =  function(){
	$.ajax({
		url : '/api/admin/groups/offeredclass/find/'+$("#group option:selected").val(),
		method : 'GET',
		cache : false,
		contentType : 'application/json',
		dataType : 'json',
		success : function(data){
			  $("#countStudents").text("");
			  $("#countStudents").hide();
			  $("#uaps").empty();
			  $("#uaps").append('<option value="SN">Seleccione</option>');
		      $.each(data,function(key,registro) {
			        $("#uaps").append('<option value='+registro[0]+'>'+registro[1]+'</option>');
			      });        
			    },
			    error: function(data) {
			    	console.log("Error al cargar datos.");
			    }
	});
}

SAUA.stdUapsForGroup =  function(){
	$.ajax({
		url : '/api/student/groups/offeredclass/find/'+$("#group option:selected").val(),
		method : 'GET',
		cache : false,
		contentType : 'application/json',
		dataType : 'json',
		success : function(data){
			  $("#countStudents").text("");
			  $("#countStudents").hide();
			  $("#uaps").empty();
			  $("#uaps").append('<option value="SN">Seleccione</option>');
		      $.each(data,function(key,registro) {
			        $("#uaps").append('<option value='+registro[0]+'>'+registro[1]+'</option>');
			      });        
			    },
			    error: function(data) {
			    	console.log("Error al cargar datos.");
			    }
	});
}

SAUA.countForUAP =  function(){
	if($("#uaps option:selected").val()=="SN"){
		$("#countStudents").text("");
		$("#countStudents").hide();
	}else{
		$.ajax({
			headers:
			{
				'X-CSRF-TOKEN': $('#csrf').val(),
				'Accept': 'application/json',
				'Content-Type': 'application/json'
			},
			url: '/api/admin/groups/offeredclass/count',
			method: 'post',
			cache: false,
			dataType: 'json',
			data: JSON.stringify({
				"uap" : $("#uaps option:selected").val(),
				"idGroup" : $("#group option:selected").val()
			}),
			success: function (data) {
				console.log("Success!!!");
				$("#countStudents").show();
			    $("#countStudents").text("Carga actual: "+data+" alumnos");    
			},
			error: function (data) {
				console.log("Error!!!");
			}
		});
	}
}

SAUA.stdSaveCharge = function(){
	 var incomplete = false;
	  if ($("#program option:selected").val() == 'SN' || $("#group option:selected").val() == '-1' 
			  || $("#uaps option:selected").val() == 'SN' || $("#studyTypes option:selected").val() == '') {
	    incomplete = true;
	  }
	   
	  if (incomplete) {
		  swal({
		        title: 'Informaci\u00F3n incompleta',
		        text: "Faltan campos importantes por seleccionar.",
		        icon: 'warning',
		        showCancelButton: true,
		        confirmButtonColor: '#3f51b5',
		        cancelButtonColor: '#ff4081',
		        confirmButtonText: 'Great ',
		        buttons: {
		          cancel: {
		            text: "Aceptar",
		            value: null,
		            visible: true,
		            className: "btn btn-danger",
		            closeModal: true,
		          }
		      }
		  })
	      return;
	  }else {	 
			$.ajax({
				  headers:
			        { 'X-CSRF-TOKEN': $('#csrf').val(),
			          'Accept': 'application/json',
			          'Content-Type': 'application/json'                    
			        },
					url : '/api/student/students/savecharge',
					method : 'post',
					cache : false,
					contentType : 'application/json',
					dataType : 'json',
					data : JSON.stringify({
						"idProgram" : $("#program option:selected").val(),
						"idGroup" : $("#group option:selected").val(),
						"idUap" : $("#uaps option:selected").val(),
						"studyType" : $("#studyTypes option:selected").val(),
						"studentRegister" : $("#studentRegister").val()
					}),
					success : function(data){
			        	console.log("Success!!!!");
			        	$("#exampleModal-4").modal('hide');
			        	swal({
			                title: '',
			                text: 'Clase agregada correctamente!!',
			                icon: 'success',
			                button: {
			                  text: "Ok",
			                  value: true,
			                  visible: true,
			                  className: "btn btn-primary"
			                }
			              }).then(function() {
			            	    window.location.reload();
			              });
					},
					error: function(data) {
						console.log("Error!!!!");
						swal({
					        title: '',
					        text: data.responseText,
					        icon: 'error',
					        button: {
					          text: "Ok",
					          value: true,
					          visible: true,
					          className: "btn btn-warning"
					        }
					      });
						$("#exampleModal-4").modal('hide');
					}
				});
	  }
}

SAUA.admSaveCharge = function(){
	 var incomplete = false;
	  if ($("#program option:selected").val() == 'SN' || $("#group option:selected").val() == '-1' 
			  || $("#uaps option:selected").val() == 'SN' || $("#studyTypes option:selected").val() == '') {
	    incomplete = true;
	  }
	   
	  if (incomplete) {
		  swal({
		        title: 'Informaci\u00F3n incompleta',
		        text: "Faltan campos importantes por seleccionar.",
		        icon: 'warning',
		        showCancelButton: true,
		        confirmButtonColor: '#3f51b5',
		        cancelButtonColor: '#ff4081',
		        confirmButtonText: 'Great ',
		        buttons: {
		          cancel: {
		            text: "Aceptar",
		            value: null,
		            visible: true,
		            className: "btn btn-danger",
		            closeModal: true,
		          }
		      }
		  })
	      return;
	  }else {	 
			$.ajax({
				  headers:
			        { 'X-CSRF-TOKEN': $('#csrf').val(),
			          'Accept': 'application/json',
			          'Content-Type': 'application/json'                    
			        },
					url : '/api/admin/student/savecharge',
					method : 'post',
					cache : false,
					contentType : 'application/json',
					dataType : 'json',
					data : JSON.stringify({
						"idProgram" : $("#program option:selected").val(),
						"idGroup" : $("#group option:selected").val(),
						"idUap" : $("#uaps option:selected").val(),
						"studyType" : $("#studyTypes option:selected").val(),
						"studentRegister" : $("#studentRegister").val()
					}),
					success : function(data){
			        	console.log("Success!!!!");
			        	$("#exampleModal-4").modal('hide');
			        	swal({
			                title: '',
			                text: 'Clase agregada correctamente!!',
			                icon: 'success',
			                button: {
			                  text: "Ok",
			                  value: true,
			                  visible: true,
			                  className: "btn btn-primary"
			                }
			              }).then(function() {
			            	    window.location.reload();
			              });
					},
					error: function(data) {
						console.log("Error!!!!");
						swal({
					        title: '',
					        text: data.responseText,
					        icon: 'error',
					        button: {
					          text: "Ok",
					          value: true,
					          visible: true,
					          className: "btn btn-warning"
					        }
					      });
						$("#exampleModal-4").modal('hide');
					}
				});
	  }
}

SAUA.coorSaveTutortoGroup = function(){
	 var incomplete = false;
	  if ($("#program option:selected").val() == 'SN' || $("#group option:selected").val() == '-1' 
			  || $("#teacher option:selected").val() == 'SN') {
	    incomplete = true;
	  }
	   
	  if (incomplete) {
		  swal({
		        title: 'Informaci\u00F3n incompleta',
		        text: "Faltan campos importantes por seleccionar.",
		        icon: 'warning',
		        showCancelButton: true,
		        confirmButtonColor: '#3f51b5',
		        cancelButtonColor: '#ff4081',
		        confirmButtonText: 'Great ',
		        buttons: {
		          cancel: {
		            text: "Aceptar",
		            value: null,
		            visible: true,
		            className: "btn btn-danger",
		            closeModal: true,
		          }
		      }
		  })
	      return;
	  }else {	 
			$.ajax({
				  headers:
			        { 'X-CSRF-TOKEN': $('#csrf').val(),
			          'Accept': 'application/json',
			          'Content-Type': 'application/json'                    
			        },
					url : '/api/coordinator/tutorship/saveTutortoGroup',
					method : 'post',
					cache : false,
					contentType : 'application/json',
					dataType : 'json',
					data : JSON.stringify({
						"idProgram" : $("#program option:selected").val(),
						"idGroup" : $("#group option:selected").val(),
						"idTeacher" : $("#teacher option:selected").val()
					}),
					success : function(data){
			        	console.log("Success!!!!");
			        	$("#exampleModal-4").modal('hide');
			        	swal({
			                title: '',
			                text: 'Tutor asignado correctamente!!',
			                icon: 'success',
			                button: {
			                  text: "Ok",
			                  value: true,
			                  visible: true,
			                  className: "btn btn-primary"
			                }
			              }).then(function() {
			            	    window.location.reload();
			              });
					},
					error: function(data) {
						console.log("Error!!!!");
						swal({
					        title: 'No se puede asignar el tutor al grupo',
					        text: data.responseText,
					        icon: 'error',
					        button: {
					          text: "Ok",
					          value: true,
					          visible: true,
					          className: "btn btn-warning"
					        }
					      });
						$("#exampleModal-4").modal('hide');
					}
				});
	  }
}

SAUA.admSaveTutortoGroup = function(){
	 var incomplete = false;
	  if ($("#program option:selected").val() == 'SN' || $("#group option:selected").val() == '-1' 
			  || $("#teacher option:selected").val() == 'SN') {
	    incomplete = true;
	  }
	   
	  if (incomplete) {
		  swal({
		        title: 'Informaci\u00F3n incompleta',
		        text: "Faltan campos importantes por seleccionar.",
		        icon: 'warning',
		        showCancelButton: true,
		        confirmButtonColor: '#3f51b5',
		        cancelButtonColor: '#ff4081',
		        confirmButtonText: 'Great ',
		        buttons: {
		          cancel: {
		            text: "Aceptar",
		            value: null,
		            visible: true,
		            className: "btn btn-danger",
		            closeModal: true,
		          }
		      }
		  })
	      return;
	  }else {	 
			$.ajax({
				  headers:
			        { 'X-CSRF-TOKEN': $('#csrf').val(),
			          'Accept': 'application/json',
			          'Content-Type': 'application/json'                    
			        },
					url : '/api/admin/tutorship/saveTutortoGroup',
					method : 'post',
					cache : false,
					contentType : 'application/json',
					dataType : 'json',
					data : JSON.stringify({
						"idProgram" : $("#program option:selected").val(),
						"idGroup" : $("#group option:selected").val(),
						"idTeacher" : $("#teacher option:selected").val()
					}),
					success : function(data){
			        	console.log("Success!!!!");
			        	$("#exampleModal-4").modal('hide');
			        	swal({
			                title: '',
			                text: 'Tutor asignado correctamente!!',
			                icon: 'success',
			                button: {
			                  text: "Ok",
			                  value: true,
			                  visible: true,
			                  className: "btn btn-primary"
			                }
			              }).then(function() {
			            	    window.location.reload();
			              });
					},
					error: function(data) {
						console.log("Error!!!!");
						swal({
					        title: 'No se puede asignar el tutor al grupo',
					        text: data.responseText,
					        icon: 'error',
					        button: {
					          text: "Ok",
					          value: true,
					          visible: true,
					          className: "btn btn-warning"
					        }
					      });
						$("#exampleModal-4").modal('hide');
					}
				});
	  }
}
SAUA.saveStudentsToTutor = function(){
	 var incomplete = false;
	  if ($("#teacheruno option:selected").val() == '-1' ) {
	    incomplete = true;
	  }
	   
	  if (incomplete) {
		  swal({
		        title: 'Informaci\u00F3n incompleta',
		        text: "Faltan campos importantes por seleccionar.",
		        icon: 'warning',
		        showCancelButton: true,
		        confirmButtonColor: '#3f51b5',
		        cancelButtonColor: '#ff4081',
		        confirmButtonText: 'Great ',
		        buttons: {
		          cancel: {
		            text: "Aceptar",
		            value: null,
		            visible: true,
		            className: "btn btn-danger",
		            closeModal: true,
		          }
		      }
		  })
	      return;
	  }else {	 
			$.ajax({
				  headers:
			        { 'X-CSRF-TOKEN': $('#csrf').val(),
			          'Accept': 'application/json',
			          'Content-Type': 'application/json'                    
			        },
					url : '/api/coordinator/tutorship/saveStudentsToTutor',
					method : 'post',
					cache : false,
					contentType : 'application/json',
					dataType : 'json',
					data : JSON.stringify({
						"register" : $("#matriculas").val(),
						"idTeacher" : $("#teacheruno option:selected").val()
					}),
					success : function(data){
			        	console.log("Success!!!!");
			        	$("#exampleModal-4").modal('hide');
			        	swal({
			                title: '',
			                text: 'Tutor asignado correctamente!!',
			                icon: 'success',
			                button: {
			                  text: "Ok",
			                  value: true,
			                  visible: true,
			                  className: "btn btn-primary"
			                }
			              }).then(function() {
			            	    window.location.reload();
			              });
					},
					error: function(data) {
						console.log("Error!!!!");
						swal({
					        title: 'No se pueden asignar los alumnos al tutor',
					        text: data.responseText,
					        icon: 'error',
					        button: {
					          text: "Ok",
					          value: true,
					          visible: true,
					          className: "btn btn-warning"
					        }
					      });
						$("#exampleModal-4").modal('hide');
					}
				});
	  }
}

SAUA.saveQuestions = function(){
if($("#questions-form").valid()) {
	var list = new Array();
	$("input[type=text]").each(function(){
		if(this.value == "") {
			list.push(this.id+"-empty");
		}else {
			list.push(this.id+"-"+this.value);
		}
	});
	$("input[type=radio]:checked").each(function(){
		list.push(this.id+"-"+this.value);
	});

	var check = 0;
	$("input[type=checkbox]:checked").each(function(){
		list.push(this.id+"-"+this.value);
		check++
	});
	
	if(check > 0){
	$.ajax({
		  headers:
	        { 'X-CSRF-TOKEN': $('#csrf').val(),
	          'Accept': 'application/json',
	          'Content-Type': 'application/json'                    
	        },
			url : '/api/any/inquest/save',
			method : 'post',
			cache : false,
			contentType : 'application/json',
			dataType : 'json',
			data : JSON.stringify({
				"studentRegister" : $("#register").val(),
				"answersl" : list
			}),
			success : function(data){
	        	console.log("Success!!!!");
	        	swal({
	                title: 'Informaci\u00F3n guardada correctamente.',
	                text: 'Gracias por tu participaci\u00F3n',
	                icon: 'success',
	                button: {
	                  text: "Ok",
	                  value: true,
	                  visible: true,
	                  className: "btn btn-primary"
	                }
	              }).then(function() {
	            	    window.location.reload();
	              });
			},
			error: function(data) {
				console.log("Error!!!!");
				swal({
			        title: '',
			        text: data.responseText,
			        icon: 'error',
			        button: {
			          text: "Ok",
			          value: true,
			          visible: true,
			          className: "btn btn-warning"
			        }
			      });
			}
		});
		}else{
			swal({
		        title: 'Faltan opciones por seleccionar',
		        icon: 'error',
		        button: {
		          text: "Ok",
		          value: true,
		          visible: true,
		          className: "btn btn-warning"
		        }
		      });
		}
	}
}

SAUA.changePeriod = function(idInput, idValue){
	$("#" + idInput, "#active-form").val(idValue);
	swal({
        title: 'Activar periodo',
        text: "Al activar este periodo, el que est\u00E9 activo, se desactivar\u00E1. Esta seguro de activarlo?",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3f51b5',
        cancelButtonColor: '#ff4081',
        confirmButtonText: 'Great ',
        buttons: {
          cancel: {
            text: "Cancelar",
            value: null,
            visible: true,
            className: "btn btn-danger",
            closeModal: true,
          },
          confirm: {
            text: "Aceptar",
            value: true,
            visible: true,
            className: "btn btn-primary",
            closeModal: true
          }
        }
      }).then(function (result) {
    	  if(result){
    		  $("#active-form").submit();
    	  }
      });
}
SAUA.activeEvals = function(idInput, idValue){
	$("#" + idInput, "#active-form").val(idValue);
	swal({
		title: 'Activar Evaluaciones docentes y Autoevaluaciones',
		text: "Al activar las evaluaciones, el estudiante podrá contestarlas aunque el periodo haya finalizado. ¿Está seguro de activarlas?",
		icon: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#3f51b5',
		cancelButtonColor: '#ff4081',
		confirmButtonText: 'Great ',
		buttons: {
			cancel: {
				text: "Cancelar",
				value: null,
				visible: true,
				className: "btn btn-danger",
				closeModal: true,
			},
			confirm: {
				text: "Aceptar",
				value: true,
				visible: true,
				className: "btn btn-primary",
				closeModal: true
			}
		}
	}).then(function (result) {
		if(result){
			$("#active-form").submit();
		}
	});
}
SAUA.desactiveEvals = function(idInput, idValue){
	$("#" + idInput, "#desactive-form").val(idValue);
	swal({
		title: 'Desactivar Evaluaciones docentes y Autoevaluaciones',
		text: "Al desactivar las evaluaciones, el estudiante ya no podrá contestarlas, hasta que se inicie el periodo de evaluaciones. ¿Está seguro de desactivarla?",
		icon: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#3f51b5',
		cancelButtonColor: '#ff4081',
		confirmButtonText: 'Great ',
		buttons: {
			cancel: {
				text: "Cancelar",
				value: null,
				visible: true,
				className: "btn btn-danger",
				closeModal: true,
			},
			confirm: {
				text: "Aceptar",
				value: true,
				visible: true,
				className: "btn btn-primary",
				closeModal: true
			}
		}
	}).then(function (result) {
		if(result){
			$("#desactive-form").submit();
		}
	});
}
SAUA.groupAssign = function(idInput, idValue, idInputTwo, idProgram) {
	$('#order-listing-two tbody tr').slice(0).remove();
	$.ajax({
		url : '/api/any/groups/find/'+idProgram,
		method : 'GET',
		cache : false,
		contentType : 'application/json',
		dataType : 'json',
		success : function(data){
			  $("#groups-req").empty();
			  $("#groups-req").append('<option value="-1">Seleccione</option>');
		      $.each(data,function(key,registro) {
			        $("#groups-req").append('<option value='+registro[0]+'>'+registro[1]+'</option>');
			      });        
			    },
			    error: function(data) {
			    	console.log("Error al cargar datos.");
			    }
	});
	
	$("#exampleModal-4").modal("show");
	$("#student-register").val(idValue);
	$("#studentRegister").val(idValue);
};

SAUA.uapsRequiredForGroup =  function(){
	$('#order-listing-two tbody tr').slice(0).remove();
	$.ajax({
		url : '/api/admin/groups/offeredclassrequired/find/'+$("#groups-req option:selected").val(),
		method : 'GET',
		cache : false,
		contentType : 'application/json',
		dataType : 'json',
		success : function(data){
			//$('#required-uaps').show();
        	$.each(data,function(key,registro) {
	        	$("#order-listing-two").append("<tr>"
	        			+"<td>"+registro[0]+"</td>"
	        			+"<td>"+registro[1]+"</td>");
		     });
		},
		error: function(data) {
			console.log("Error al cargar los datos");
		}
	});
}

SAUA.groupList = function(idInput, idValue, name, description) {
	$.ajax({
		url : '/api/admin/uaps/findbygroup/'+idValue,
		method : 'GET',
		cache : false,
		contentType : 'application/json',
		dataType : 'json',
		success : function(data){
			  $("#uaps").empty();
			  $("#uaps").append('<option value="SN">Seleccione</option>');
		      $.each(data,function(key,registro) {
			        $("#uaps").append('<option value='+registro[0]+'>'+registro[1]+'</option>');
			      });        
			    },
			    error: function(data) {
			    	console.log("Error al cargar datos.");
			    }
	});
	
	$("#exampleModal-4").modal("show");
	$("#idGroup").val(idValue);
	$("#id-group").val(description);
};

SAUA.grouptopromote = function(idInput, idValue) {
	$.ajax({
		url : '/api/admin/groups/findbypromote/'+idValue,
		method : 'GET',
		cache : false,
		contentType : 'application/json',
		dataType : 'json',
		success : function(data){
			  $("#groups").empty();
			  $("#groups").append('<option value="SN">Seleccione</option>');
			  if(data.length == 0){
				swal({
				        title: 'Siguiente grupo no encontrado.',
				        text: "El siguiente grupo relacionado a este, aun no esta dado de alta.",
				        icon: 'warning',
				        showCancelButton: true,
				        confirmButtonColor: '#3f51b5',
				        cancelButtonColor: '#ff4081',
				        confirmButtonText: 'Great ',
				        buttons: {
				          cancel: {
				            text: "Aceptar",
				            value: null,
				            visible: true,
				            className: "btn btn-danger",
				            closeModal: true,
				          }
				      }
				  }).then(function (result) {
			    	  $("#exampleModal-4").modal("hide");
			    	  return;
			      });
			  }
		      $.each(data,function(key,registro) {
			        $("#groups").append('<option value='+registro[0]+'>'+registro[1]+'</option>');
			      });        
			    },
			    error: function(data) {
			    	console.log("Error al cargar datos.");
			    }
	});
	
	$("#exampleModal-4").modal("show");
	$("#idGroup").val(idValue);
};

SAUA.promote = function(){
	var incomplete = false;
	 if ($("#groups option:selected").val() == 'SN') {
	   incomplete = true;
	 }
	 
 	if (incomplete) {
	  swal({
	        title: 'Informac\u00F3in incompleta',
	        text: "Debe seleccionar un grupo.",
	        icon: 'warning',
	        showCancelButton: true,
	        confirmButtonColor: '#3f51b5',
	        cancelButtonColor: '#ff4081',
	        confirmButtonText: 'Great ',
	        buttons: {
	          cancel: {
	            text: "Aceptar",
	            value: null,
	            visible: true,
	            className: "btn btn-danger",
	            closeModal: true,
	          }
	      }
	  })
      return;
  	}else {	
		$.ajax({
		  headers:
	        { 'X-CSRF-TOKEN': $('#csrf').val(),
	          'Accept': 'application/json',
	          'Content-Type': 'application/json'                    
	        },
			url : '/api/admin/groups/promote',
			method : 'post',
			cache : false,
			contentType : 'application/json',
			dataType : 'json',
			data : JSON.stringify({
				"idGroup" : $("#idGroup").val(),
				"idGroupPromote" : $("#groups option:selected").val()
			}),
			success : function(data){
	        	console.log("Success!!!!");
	        	$("#exampleModal-4").modal('hide');
	        	swal({
	                title: '',
	                text: 'Alumnos promovidos correctamente!!',
	                icon: 'success',
	                button: {
	                  text: "Ok",
	                  value: true,
	                  visible: true,
	                  className: "btn btn-primary"
	                }
	              }).then(function() {
	            	    window.location.reload();
	              });
			},
			error: function(data) {
				console.log("Error!!!!");
				swal({
			        title: '',
			        text: data.responseText,
			        icon: 'error',
			        button: {
			          text: "Ok",
			          value: true,
			          visible: true,
			          className: "btn btn-warning"
			        }
			      });
				$("#exampleModal-4").modal('hide');
			}
		});
	}
}

SAUA.printList = function(){
	var incomplete = false;
	  if ($("#uaps option:selected").val() == 'SN') {
	    incomplete = true;
	  }
	
	if (incomplete) {
		  swal({
		        title: 'Informac\u00F3in incompleta',
		        text: "Debe seleccionar una UAP.",
		        icon: 'warning',
		        showCancelButton: true,
		        confirmButtonColor: '#3f51b5',
		        cancelButtonColor: '#ff4081',
		        confirmButtonText: 'Great ',
		        buttons: {
		          cancel: {
		            text: "Aceptar",
		            value: null,
		            visible: true,
		            className: "btn btn-danger",
		            closeModal: true,
		          }
		      }
		  })
	      return;
	  }else {	
		$.ajax({
		  headers:
	        { 'X-CSRF-TOKEN': $('#csrf').val(),
	          'Accept': 'application/json',
	          'Content-Type': 'application/json'                    
	        },
			url : '/api/admin/lists/studentsbyuap',
			method : 'post',
			cache : false,
			data : JSON.stringify({
				"idGroup" : $("#idGroup").val(),
				"idUap" : $("#uaps option:selected").val(),
				"filename" : $("#id-group").val() +' - '+ $("#uaps option:selected").text()
			}),
			xhrFields: {
	            responseType: 'blob'
	        },
	        success: function (data, status, response) {
	            var filename = "";
	            var disposition = response.getResponseHeader('Content-Disposition');
	            if (disposition && disposition.indexOf('attachment') !== -1) {
	                var filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
	                var matches = filenameRegex.exec(disposition);
	                if (matches != null && matches[1]) filename = matches[1].replace(/['"]/g, '');
	            }
	            let a = document.createElement('a');
	            a.href = window.URL.createObjectURL(data);
	            a.download = filename;
	            document.body.append(a);
	            a.click();
				data = null;
	        },
			error: function(data) {
				console.log("Error!!!!");
				swal({
			        title: '',
			        text: 'Ocurri\u00F3 un error al generar lista.',
			        icon: 'error',
			        button: {
			          text: "Ok",
			          value: true,
			          visible: true,
			          className: "btn btn-warning"
			        }
			      });
				$("#exampleModal-4").modal('hide'); 
			}
		}).done(function() {
		  console.log("Success!!!!");
	    	$("#exampleModal-4").modal('hide');
	    	swal({
	            title: '',
	            text: 'Lista generada correctamente!!',
	            icon: 'success',
	            button: {
	              text: "Ok",
	              value: true,
	              visible: true,
	              className: "btn btn-primary"
	            }
	          });
			
		});
	}
}

SAUA.documentdownload = function(idInput, idValue){
	$.ajax({
	  headers:
        { 'X-CSRF-TOKEN': $('#csrf').val(),
          'Accept': 'application/json',
          'Content-Type': 'application/json'                    
        },
		url : '/api/any/download/document/'+idValue,
		method : 'get',
		cache : false,
		xhrFields: {
            responseType: 'blob'
        },
        success: function (data, status, response) {
            var filename = "";
            var disposition = response.getResponseHeader('Content-Disposition');
            if (disposition && disposition.indexOf('attachment') !== -1) {
                var filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
                var matches = filenameRegex.exec(disposition);
                if (matches != null && matches[1]) filename = matches[1].replace(/['"]/g, '');
            }
            let a = document.createElement('a');
            a.href = window.URL.createObjectURL(data);
            a.download = filename;
            document.body.append(a);
            a.click();
        },
		error: function(data) {
			swal({
		        title: '',
		        text: 'No se pudo descargar archivo.',
		        icon: 'error',
		        button: {
		          text: "Ok",
		          value: true,
		          visible: true,
		          className: "btn btn-warning"
		        }
		      });
		}
	}).done(function() {
	  console.log("Success!!!!");
	});
}

SAUA.papeletadownload = function(){
	$.ajax({
	  headers:
        { 'X-CSRF-TOKEN': $('#csrf').val(),
          'Accept': 'application/json',
          'Content-Type': 'application/json'                    
        },
		url : '/api/any/download/papeleta',
		method : 'get',
		cache : false,
		xhrFields: {
            responseType: 'blob'
        },
        success: function (data, status, response) {
            var filename = "";
            var disposition = response.getResponseHeader('Content-Disposition');
            if (disposition && disposition.indexOf('attachment') !== -1) {
                var filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
                var matches = filenameRegex.exec(disposition);
                if (matches != null && matches[1]) filename = matches[1].replace(/['"]/g, '');
            }
            let a = document.createElement('a');
            a.href = window.URL.createObjectURL(data);
            a.download = filename;
            document.body.append(a);
            a.click();
        },
		error: function(data) {
			swal({
		        title: '',
		        text: 'No se pudo descargar papeleta.',
		        icon: 'error',
		        button: {
		          text: "Ok",
		          value: true,
		          visible: true,
		          className: "btn btn-warning"
		        }
		      });
		}
	}).done(function() {
	  console.log("Success!!!!");
	});
}

SAUA.tutorpapeletadownloadByStudent = function(idInput, idValue){
	$.ajax({
	  headers:
        { 'X-CSRF-TOKEN': $('#csrf').val(),
          'Accept': 'application/json',
          'Content-Type': 'application/json'                    
        },
		url : '/api/tutor/download/papeleta/'+idValue,
		method : 'get',
		cache : false,
		xhrFields: {
            responseType: 'blob'
        },
        success: function (data, status, response) {
            var filename = "";
            var disposition = response.getResponseHeader('Content-Disposition');
            if (disposition && disposition.indexOf('attachment') !== -1) {
                var filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
                var matches = filenameRegex.exec(disposition);
                if (matches != null && matches[1]) filename = matches[1].replace(/['"]/g, '');
            }
            let a = document.createElement('a');
            a.href = window.URL.createObjectURL(data);
            a.download = filename;
            document.body.append(a);
            a.click();
        },
		error: function(data) {
			var errorType = "No se pudo descargar papeleta.";
			if(data.status == '501'){
				errorType = "El usuario no es un alumno.";
			}
			if(data.status == '502'){
				errorType = "El usuario no esta autorizado para imprimir papeleta.";
			}
			if(data.status == '503'){
				errorType = "Carga acad\u00E9mica no autorizada.";
			}
			swal({
		        title: '',
		        text: errorType,
		        icon: 'error',
		        button: {
		          text: "Ok",
		          value: true,
		          visible: true,
		          className: "btn btn-warning"
		        }
		      });
		}
	}).done(function() {
	  console.log("Success!!!!");
	});
}

SAUA.admpapeletadownloadByStudent = function(idInput, idValue){
	$.ajax({
	  headers:
        { 'X-CSRF-TOKEN': $('#csrf').val(),
          'Accept': 'application/json',
          'Content-Type': 'application/json'                    
        },
		url : '/api/admin/download/papeleta/'+idValue,
		method : 'get',
		cache : false,
		xhrFields: {
            responseType: 'blob'
        },
        success: function (data, status, response) {
            var filename = "";
            var disposition = response.getResponseHeader('Content-Disposition');
            if (disposition && disposition.indexOf('attachment') !== -1) {
                var filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
                var matches = filenameRegex.exec(disposition);
                if (matches != null && matches[1]) filename = matches[1].replace(/['"]/g, '');
            }
            let a = document.createElement('a');
            a.href = window.URL.createObjectURL(data);
            a.download = filename;
            document.body.append(a);
            a.click();
        },
		error: function(data) {
			var errorType = "No se pudo descargar papeleta.";
			if(data.status == '501'){
				errorType = "El usuario no es un alumno.";
			}
			if(data.status == '502'){
				errorType = "El usuario no esta autorizado para imprimir papeleta.";
			}
			if(data.status == '503'){
				errorType = "Carga acad\u00E9mica no autorizada.";
			}
			swal({
		        title: '',
		        text: errorType,
		        icon: 'error',
		        button: {
		          text: "Ok",
		          value: true,
		          visible: true,
		          className: "btn btn-warning"
		        }
		      });
		}
	}).done(function() {
	  console.log("Success!!!!");
	});
}

SAUA.papeletadownloadMasMat = function(idInput, idValue){
	$.ajax({
	  headers:
        { 'X-CSRF-TOKEN': $('#csrf').val(),
          'Accept': 'application/json',
          'Content-Type': 'application/json'                    
        },
		url : '/api/admin/download/papeletasMasMat/'+idValue,
		method : 'get',
		cache : false,
		xhrFields: {
            responseType: 'blob'
        },
        success: function (data, status, response) {
            var filename = "";
            var disposition = response.getResponseHeader('Content-Disposition');
            if (disposition && disposition.indexOf('attachment') !== -1) {
                var filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
                var matches = filenameRegex.exec(disposition);
                if (matches != null && matches[1]) filename = matches[1].replace(/['"]/g, '');
            }
            let a = document.createElement('a');
            a.href = window.URL.createObjectURL(data);
            a.download = filename;
            document.body.append(a);
            a.click();
        },
		error: function(data) {
			var errorType = "No se pudo descargar papeleta.";
			if(data.status == '501'){
				errorType = "El usuario no es un alumno.";
			}
			if(data.status == '502'){
				errorType = "El usuario no esta autorizado para imprimir papeleta.";
			}
			if(data.status == '503'){
				errorType = "Carga acad\u00E9mica no autorizada.";
			}
			swal({
		        title: '',
		        text: errorType,
		        icon: 'error',
		        button: {
		          text: "Ok",
		          value: true,
		          visible: true,
		          className: "btn btn-warning"
		        }
		      });
		}
	}).done(function() {
	  console.log("Success!!!!");
	});
}
SAUA.papeletadownloadMasVesp = function(idInput, idValue){
	$.ajax({
	  headers:
        { 'X-CSRF-TOKEN': $('#csrf').val(),
          'Accept': 'application/json',
          'Content-Type': 'application/json'                    
        },
		url : '/api/admin/download/papeletasMasVesp/'+idValue,
		method : 'get',
		cache : false,
		xhrFields: {
            responseType: 'blob'
        },
        success: function (data, status, response) {
            var filename = "";
            var disposition = response.getResponseHeader('Content-Disposition');
            if (disposition && disposition.indexOf('attachment') !== -1) {
                var filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
                var matches = filenameRegex.exec(disposition);
                if (matches != null && matches[1]) filename = matches[1].replace(/['"]/g, '');
            }
            let a = document.createElement('a');
            a.href = window.URL.createObjectURL(data);
            a.download = filename;
            document.body.append(a);
            a.click();
        },
		error: function(data) {
			var errorType = "No se pudo descargar papeleta.";
			if(data.status == '501'){
				errorType = "El usuario no es un alumno.";
			}
			if(data.status == '502'){
				errorType = "El usuario no esta autorizado para imprimir papeleta.";
			}
			if(data.status == '503'){
				errorType = "Carga acad\u00E9mica no autorizada.";
			}
			swal({
		        title: '',
		        text: errorType,
		        icon: 'error',
		        button: {
		          text: "Ok",
		          value: true,
		          visible: true,
		          className: "btn btn-warning"
		        }
		      });
		}
	}).done(function() {
	  console.log("Success!!!!");
	});
}

SAUA.uapsAssingToStudent = function(idInput, idValue){

			$.ajax({
				  headers:
			        { 'X-CSRF-TOKEN': $('#csrf').val(),
			          'Accept': 'application/json',
			          'Content-Type': 'application/json'                    
			        },
					url : '/api/admin/programs/uapsassign/'+idValue,
					method : 'get',
					cache : false,
					contentType : 'application/json',
					dataType : 'json',
					success : function(data){
			        	console.log("Success!!!!");
			        	swal({
			                title: 'Asignación realizada correctamente',
			                text: 'Uaps asignadas correctamente a todos los grupos del programa educativo seleccionado!!',
			                icon: 'success',
			                button: {
			                  text: "Ok",
			                  value: true,
			                  visible: true,
			                  className: "btn btn-primary"
			                }
			              }).then(function() {
			            	    window.location.reload();
			              });
					},
					error: function(data) {
						console.log("Error!!!!");
						swal({
					        title: 'No se realizó la asignación de uaps a los grupos',
					        text: data.responseText,
					        icon: 'error',
					        button: {
					          text: "Ok",
					          value: true,
					          visible: true,
					          className: "btn btn-warning"
					        }
					      });
					}
				});
	  
}
SAUA.uapsAssingToGroupStudent = function(idInput, idValue){

			$.ajax({
				  headers:
			        { 'X-CSRF-TOKEN': $('#csrf').val(),
			          'Accept': 'application/json',
			          'Content-Type': 'application/json'                    
			        },
					url : '/api/admin/groups/assinguapsallgroup/'+idValue,
					method : 'get',
					cache : false,
					contentType : 'application/json',
					dataType : 'json',
					success : function(data){
			        	console.log("Success!!!!");
			        	swal({
			                title: 'Asignación realizada correctamente',
			                text: 'Uaps asignadas correctamente a todos los alumnos del grupo seleccionado!!',
			                icon: 'success',
			                button: {
			                  text: "Ok",
			                  value: true,
			                  visible: true,
			                  className: "btn btn-primary"
			                }
			              }).then(function() {
			            	    window.location.reload();
			              });
					},
					error: function(data) {
						console.log("Error!!!!");
						swal({
					        title: 'No se realizó la asignación de uaps al grupo',
					        text: data.responseText,
					        icon: 'error',
					        button: {
					          text: "Ok",
					          value: true,
					          visible: true,
					          className: "btn btn-warning"
					        }
					      });
						
					}
				});
	  
}

SAUA.statisticsUAPs = function(){
	$.ajax({
	  headers:
        { 'X-CSRF-TOKEN': $('#csrf').val(),
          'Accept': 'application/json',
          'Content-Type': 'application/json'                    
        },
		url : '/api/admin/download/statistics/uaps',
		method : 'get',
		cache : false,
		xhrFields: {
            responseType: 'blob'
        },
        success: function (data, status, response) {
            var filename = "";
            var disposition = response.getResponseHeader('Content-Disposition');
            if (disposition && disposition.indexOf('attachment') !== -1) {
                var filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
                var matches = filenameRegex.exec(disposition);
                if (matches != null && matches[1]) filename = matches[1].replace(/['"]/g, '');
            }
            let a = document.createElement('a');
            a.href = window.URL.createObjectURL(data);
            a.download = filename;
            document.body.append(a);
            a.click();
        },
		error: function(data) {
			var errorType = "No se pudo generar el reporte.";
			if(data.status == '502'){
				errorType = "El usuario no esta autorizado para imprimir este reporte.";
			}
			swal({
		        title: '',
		        text: errorType,
		        icon: 'error',
		        button: {
		          text: "Ok",
		          value: true,
		          visible: true,
		          className: "btn btn-warning"
		        }
		      });
		}
	}).done(function() {
	  console.log("Success!!!!");
	});
}

SAUA.selectQuestionsByCategory =  function(){	
	var selectQuestions = new Array();
	var category = $("#categories option:selected").val().split("-");
	$('#pagination-demo').twbsPagination('destroy');
	$('#pagination-demo').twbsPagination({
        totalPages: category[1],
        visiblePages: 5,
        next: 'Siguiente',
        prev: 'Anterior',
        last: '&Uacute;ltima',
        first:'Primera',
        onPageClick: function (event, page) {
		var t = document.getElementById('order-listing-two');
			
			var i = 1;
			$('#order-listing-two').find('tbody tr').each(function(index) {
				  if( $(this).find("input[type='checkbox']").prop('checked') ) {
					selectQuestions.push($(t.rows[i].cells[0]).text());
				  }else{
					  selectQuestions.forEach(function(selectQuestion, index) {
						if($(t.rows[i].cells[0]).text() == selectQuestion){
							delete selectQuestions[index];
						}
					  });
				  }
				 i++;
			});
			
        	if(page > 0){
            $.ajax({
      		  headers:
      	        { 'X-CSRF-TOKEN': $('#csrf').val(),
      	          'Accept': 'application/json',
      	          'Content-Type': 'application/json'
      	        },
      			url : '/api/admin/questions/list-page',
      			method : 'post',
      			cache : false,
      			dataType : 'json',
      			data : JSON.stringify({
      				"categoryQuestion" : category[0],
      				"page" : page,
					"idsQuestionsSelected": Array.from(new Set(selectQuestions))
      			}),
      	        success: function (data) {
				$('#order-listing-two tbody tr').slice(0).remove();
				totalPages = data.totalPages;
				selectQuestions = data.idsQuestions;
				console.log("Preguntas seleccionadas:"+selectQuestions);
				//$("#q-selected").html(data.idsQuestions);
				//$("#q-selected").attr("value", data.idsQuestions);
	        	$.each(data.result,function(key,registro) {
					$("#order-listing-two").append("<tr>"
							+"<td>"+registro.id+"</td>"
		        			+"<td>"+registro.text+"</td>"
							+"<td>"+registro.questionType+"</td>"
		        			+"<td><input type='checkbox' "+registro.body+" onclick='checkedInput("+registro.id+",this)'></td>");
			     });
		        },
      			error: function(data) {
      				console.log("Error al cargar datos");
      			}
      		});
        }
        }
    });
	
	
}
SAUA.evaldownloadByTeacher = function(idInput, idValue){
	$.ajax({
	  headers:
        { 'X-CSRF-TOKEN': $('#csrf').val(),
          'Accept': 'application/json',
          'Content-Type': 'application/json'                    
        },
		url : '/api/admin/download/evalDocente/'+idValue,
		method : 'get',
		cache : false,
		xhrFields: {
            responseType: 'blob'
        },
        success: function (data, status, response) {
            var filename = "";
            var disposition = response.getResponseHeader('Content-Disposition');
            if (disposition && disposition.indexOf('attachment') !== -1) {
                var filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
                var matches = filenameRegex.exec(disposition);
                if (matches != null && matches[1]) filename = matches[1].replace(/['"]/g, '');
            }
            let a = document.createElement('a');
            a.href = window.URL.createObjectURL(data);
            a.download = filename;
            document.body.append(a);
            a.click();
        },
		error: function(data) {
			var errorType = "No se pudo descargar papeleta.";
			if(data.status == '501'){
				errorType = "El usuario no es un alumno.";
			}
			if(data.status == '502'){
				errorType = "El usuario no esta autorizado para imprimir papeleta.";
			}
			if(data.status == '503'){
				errorType = "Carga acad\u00E9mica no autorizada.";
			}
			swal({
		        title: '',
		        text: errorType,
		        icon: 'error',
		        button: {
		          text: "Ok",
		          value: true,
		          visible: true,
		          className: "btn btn-warning"
		        }
		      });
		}
	}).done(function() {
	  console.log("Success!!!!");
	});
}


SAUA.evaluationTeacher = function(idInput, idValue) {
	$("#" + idInput, "#evaluation-teacher-form").val(idValue);
	$("#evaluation-teacher-form").submit();
};

SAUA.saveTeacherEvaluation = function(){
	var list = new Array();
	var containEmpty = false;
	 let select=document.querySelectorAll('select') 
	  select.forEach(function(sel) {
		if(sel.value == 0) {
			list.push(sel.id+"-empty");
		}else {
			list.push(sel.id+"-"+sel.value);
		}
	});
	for(x in list){
	  if(list[x].includes('-empty')){
			containEmpty = true;
		}
	}
	
	if($("#answ-text").val()==""){
		containEmpty = true;
	}
	
	console.log("val-text:"+$("#answ-text").attr('name')+"-"+$("#answ-text").val());
	list.push($("#answ-text").attr('name')+"-"+$("#answ-text").val());
	console.log("Empty contains:"+containEmpty);
	if(containEmpty){
		swal({
	        title: 'Faltan opciones por seleccionar',
	        icon: 'error',
	        button: {
	          text: "Ok",
	          value: true,
	          visible: true,
	          className: "btn btn-warning"
	        }
	      });
	}else{
		$.ajax({
		  headers:
	        { 'X-CSRF-TOKEN': $('#csrf').val(),
	          'Accept': 'application/json',
	          'Content-Type': 'application/json'                    
	        },
			url : '/api/student/inquest/saveteachereval',
			method : 'post',
			cache : false,
			contentType : 'application/json',
			dataType : 'json',
			data : JSON.stringify({
				"studentRegister" : $("#register").val(),
				"answersl" : list,
				"studentChargeId" : $("#studentChargeId").val(),
				"eval" : "TEACHER"
			}),
			success : function(data){
	        	console.log("Success!!!!");
	        	swal({
	                title: 'Informaci\u00F3n guardada correctamente.',
	                text: 'Gracias por tu participaci\u00F3n',
	                icon: 'success',
	                button: {
	                  text: "Ok",
	                  value: true,
	                  visible: true,
	                  className: "btn btn-primary"
	                }
	              }).then(function() {
						window.location.href = "/student/evaluation";
	              });
			},
			error: function(data) {
				console.log("Error!!!!");
				swal({
			        title: '',
			        text: data.responseText,
			        icon: 'error',
			        button: {
			          text: "Ok",
			          value: true,
			          visible: true,
			          className: "btn btn-warning"
			        }
			      });
			}
		});
	}




};

SAUA.autoEvaluation = function(idInput, idValue) {
	$("#" + idInput, "#autoEvaluation-form").val(idValue);
	$("#autoEvaluation-form").submit();
}

SAUA.saveAutoEvaluation = function(){
	var list = new Array();
	var containEmpty = false;
	 let select=document.querySelectorAll('select') 
	  select.forEach(function(sel) {
		if(sel.value == 0) {
			list.push(sel.id+"-empty");
		}else {
			list.push(sel.id+"-"+sel.value);
		}
	});
	
	$("input[type=text]").each(function(){
		if(this.id != 'ans-specify-24-43'){
			if(this.value == "") {
				list.push(this.id+"-empty");
			}else {
				list.push(this.id+"-"+this.value);
			}
		}
	});
	
	$("input[type=radio]:checked").each(function(){
		if(this.id == '24-43'){
			if($("#ans-specify-24-43").val()==''){
				list.push("24-empty");
			}else{
				list.push("24-"+$("#ans-specify-24-43").val());
			}
		}else if(this.id == '24-42'){
			list.push("24-No");
		}else if(this.name == '29'){
			list.push("29-"+this.value);
		}else{
			list.push(this.id+"-"+this.value);
		}
	});
	
	for(x in list){
	  if(list[x].includes('-empty')){
			containEmpty = true;
		}
	}
	console.log("list:"+list);
	console.log("Empty contains:"+containEmpty);
	if(containEmpty){
		swal({
	        title: 'Faltan opciones por seleccionar',
	        icon: 'error',
	        button: {
	          text: "Ok",
	          value: true,
	          visible: true,
	          className: "btn btn-warning"
	        }
	      });
	}else{
		$.ajax({
		  headers:
	        { 'X-CSRF-TOKEN': $('#csrf').val(),
	          'Accept': 'application/json',
	          'Content-Type': 'application/json'                    
	        },
			url : '/api/student/inquest/saveteachereval',
			method : 'post',
			cache : false,
			contentType : 'application/json',
			dataType : 'json',
			data : JSON.stringify({
				"studentRegister" : $("#register").val(),
				"answersl" : list,
				"studentChargeId" : $("#studentChargeId").val(),
				"eval" : "AUTO"
			}),
			success : function(data){
	        	console.log("Success!!!!");
	        	swal({
	                title: 'Informaci\u00F3n guardada correctamente.',
	                text: 'Gracias por tu participaci\u00F3n',
	                icon: 'success',
	                button: {
	                  text: "Ok",
	                  value: true,
	                  visible: true,
	                  className: "btn btn-primary"
	                }
	              }).then(function() {
						window.location.href = "/student/evaluation";
	              });
			},
			error: function(data) {
				console.log("Error!!!!");
				swal({
			        title: '',
			        text: data.responseText,
			        icon: 'error',
			        button: {
			          text: "Ok",
			          value: true,
			          visible: true,
			          className: "btn btn-warning"
			        }
			      });
			}
		});
	}
};

SAUA.search = function() {
	$('#form-eval').submit();
}

SAUA.clear = function() {
	$('#searchBy').val('');
	$('#searchByValue').val('');
	$('#form-eval').submit();
}

SAUA.answerinquest = function(idInput, idValue){
	$("#" + idInput, "#answer-form").val(idValue);
	$("#answer-form").submit();
}

SAUA.generateevaluationreportxls = function(){
		$.ajax({
		  headers:
	        { 'X-CSRF-TOKEN': $('#csrf').val(),
	          'Accept': 'application/json',
	          'Content-Type': 'application/json'                    
	        },
			url : '/api/admin/exportEvalsTeachersAll/'+ $("#periods option:selected").val(),
			method : 'get',
			cache : false,
			xhrFields: {
	            responseType: 'blob'
	        },
	        success: function (data, status, response) {
	            var filename = "";
	            var disposition = response.getResponseHeader('Content-Disposition');
	            if (disposition && disposition.indexOf('attachment') !== -1) {
	                var filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
	                var matches = filenameRegex.exec(disposition);
	                if (matches != null && matches[1]) filename = matches[1].replace(/['"]/g, '');
	            }
	            let a = document.createElement('a');
	            a.href = window.URL.createObjectURL(data);
	            a.download = filename;
	            document.body.append(a);
	            a.click();
	        },
			error: function(data) {
				swal({
			        title: '',
			        text: 'No se pudo descargar el reporte.',
			        icon: 'error',
			        button: {
			          text: "Ok",
			          value: true,
			          visible: true,
			          className: "btn btn-warning"
			        }
			      });
			}
		}).done(function() {
		  console.log("Success!!!!");
		  $('#periodModal').modal('hide');
		  swal({
	        title: '',
	        text: 'Reporte generado correctamente.',
	        icon: 'success',
	        button: {
	          text: "Ok",
	          value: true,
	          visible: true,
	          className: "btn btn-success"
	        }
	      });
		});
}

SAUA.generateevaluationreport = function(){
	$.ajax({
	  headers:
        { 'X-CSRF-TOKEN': $('#csrf').val(),
          'Accept': 'application/json',
          'Content-Type': 'application/json'                    
        },
		url : '/api/any/download/evaluationvoucher',
		method : 'get',
		cache : false,
		xhrFields: {
            responseType: 'blob'
        },
        success: function (data, status, response) {
            var filename = "";
            var disposition = response.getResponseHeader('Content-Disposition');
            if (disposition && disposition.indexOf('attachment') !== -1) {
                var filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
                var matches = filenameRegex.exec(disposition);
                if (matches != null && matches[1]) filename = matches[1].replace(/['"]/g, '');
            }
            let a = document.createElement('a');
            a.href = window.URL.createObjectURL(data);
            a.download = filename;
            document.body.append(a);
            a.click();
        },
		error: function(data) {
			swal({
		        title: '',
		        text: 'No se pudo descargar el reporte de evaluación.',
		        icon: 'error',
		        button: {
		          text: "Ok",
		          value: true,
		          visible: true,
		          className: "btn btn-warning"
		        }
		      });
		}
	}).done(function() {
	  console.log("Success!!!!");
	  swal({
	        title: '',
	        text: 'Reporte generado correctamente.',
	        icon: 'success',
	        button: {
	          text: "Ok",
	          value: true,
	          visible: true,
	          className: "btn btn-success"
	        }
	      });
	});
}
