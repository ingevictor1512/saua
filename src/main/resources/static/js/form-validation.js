(function($) {
  'use strict';
  $(function() {
    // validate the comment form when it is submitted
    $("#commentForm").validate({
      errorPlacement: function(label, element) {
        label.addClass('mt-2 text-danger');
        label.insertAfter(element);
      },
      highlight: function(element, errorClass) {
        $(element).parent().addClass('has-danger')
        $(element).addClass('form-control-danger')
      }
    });
    // validate signup form on keyup and submit
    $("#signupForm").validate({
      rules: {
        firstname: "required",
        lastname: "required",
        secondlastname: "required",
        username: {
          required: true,
          minlength: 2
        },
        password: {
          required: true,
          minlength: 8
        },
        verifyPassword: {
          required: true,
          minlength: 8,
          equalTo: "#password"
        },
        email: {
          required: true,
          email: true
        },
        register:{
        	required: true,
        	minlength: 8
        },
        curp_input: {
        	required: true,
        	minlength: 18,
        },
        topic: {
          required: "#newsletter:checked",
          minlength: 2
        },
        agree: "required"
      },
      messages: {
        firstname: "Ingresa tu nombre",
        lastname: "Ingresa tu apellido paterno",
        secondlastname: "Ingresa tu apellido materno",
        username: {
          required: "Ingresa un nombre de usuario",
          minlength: "Tu nombre de usuario debe tener un m\u00EDnimo de 2 caracteres"
        },
        password: {
          required: "Ingresa una contrase\u00F1a",
          minlength: "Tu contrase\u00F1a debe tener un m\u00EDnimo de 8 caracteres."
        },
        verifyPassword: {
          required: "Ingresa una contrase\u00F1a",
          minlength: "Tu contrase\u00F1a debe tener un m\u00EDnimo de 8 caracteres.",
          equalTo: "Las contrase\u00F1as no coinciden."
        },
        register: {
        	required: "Ingresa tu matr\u00EDcula",
        	minlength: "Tu matr\u00EDcula debe tener un m\u00EDnimo de 8 caracteres"
        },
        curp_input: {
        	required: "Ingresa tu CURP",
        	minlength: "Tu CURP debe tener un m\u00EDnimo de 18 caracteres"
        },
        email: "Ingresa un e-mail v\u00E1lido.",
        agree: "Acepta los t\u00E9rminos y condiciones",
        topic: "Please select at least 2 topics"
      },
      errorPlacement: function(label, element) {
        label.addClass('mt-2 text-danger');
        label.insertAfter(element);
      },
      highlight: function(element, errorClass) {
        $(element).parent().addClass('has-danger')
        $(element).addClass('form-control-danger')
      }
    });
    // propose username by combining first- and lastname
    $("#username").focus(function() {
      var firstname = $("#firstname").val();
      var lastname = $("#lastname").val();
      if (firstname && lastname && !this.value) {
        this.value = firstname + "." + lastname;
      }
    });
    //code to hide topic selection, disable for demo
    var newsletter = $("#newsletter");
    // newsletter topics are optional, hide at first
    var inital = newsletter.is(":checked");
    var topics = $("#newsletter_topics")[inital ? "removeClass" : "addClass"]("gray");
    var topicInputs = topics.find("input").attr("disabled", !inital);
    // show when newsletter is checked
    newsletter.on("click", function() {
      topics[this.checked ? "removeClass" : "addClass"]("gray");
      topicInputs.attr("disabled", !this.checked);
    });
  });
})(jQuery);